({
    // on Load of page fetch the list of FOR codes, Impact category and project status
    doInit: function(component, event, helper) {
        helper.fetchFROcodes(component, 'FROCodes', 'frocodes');
        helper.fetchProjectStatus(component, 'Current Status', 'projectStatus');
        helper.fetchImpactCategory(component, 'Category', 'impactCategory');
    },
    
    // on edit, get the project Id and related project details, populate in UI
    initLoad: function(component, event, helper) {         
        var recordIdIs;
        var urlString = window.location.href;
        if (urlString.indexOf("create-new-project") > -1) {
            var url = window.location;
            
            if (url.search) {
                recordIdIs = url.search.split("=")[1];
            } else {
                recordIdIs = null;
            }
        }
        if (recordIdIs != null) {
            component.set("v.recordId", recordIdIs);
            
            var getProjectDetailsAction = component.get("c.getProjectDetails");
            getProjectDetailsAction.setParams({
                recordId: recordIdIs
                
            });
            //Setting the Callback
            getProjectDetailsAction.setCallback(this, function(a) {
                //get the response state
                var state = a.getState();
                
                //check if result is successfull
                if (state == "SUCCESS") {
                    var result = a.getReturnValue();
                    if (!$A.util.isUndefined(result)) {
                        console.log(result);
                        if(result.IsEdit){
                            document.title= 'Edit Project - RMIT Researcher Portal'
                        }
                        component.set("v.projectRecord", result);
                        component.set("v.selectedForCodes", result.forCodes ? result.forCodes.split(";") : []);
                        component.set("v.selectedImpList", result.impactCategories ? result.impactCategories.split(";") : []);
                        component.set("v.pjtKeywords", result.keywords ? result.keywords.split(";") : []);
                        
                        if (result.accesss != undefined || (result.IsRMProject && result.accesss != undefined)) {
                            document.querySelector('input[name=options][data-value="' + result.accesss + '"]').checked = true;
                           /* if (result.IsRMProject && result.accesss != undefined && result.IsConfidential) {
                                document.querySelector('input[name=options][data-value="RMIT Public"]').disabled = true;
                            }
                            if(result.IsRMProject && result.accesss != undefined && result.isAccessFieldEditable == true){
                               // document.querySelector('input[name=options][data-value="RMIT Public"]').disabled = true;
                            }*/
                            if(result.IsRMProject && result.accesss != undefined && result.isAccessFieldEditable == false){
                                document.querySelector('input[name=options][data-value="RMIT Public"]').disabled = true;
                                document.querySelector('input[name=options][data-value="Members"]').disabled = true;
                            }
                        } else if ((result.IsRMProject && result.accesss == undefined) || (result.IsRMProject && result.accesss == "Private")) {
                            document.querySelector('input[name=options][data-value="Members"]').checked = true;
                        }
                        
                    }
                } else if (state == "ERROR") {
                    alert('Error in calling server side action');
                }
            });
            
            //adds the server-side action to the queue        
            $A.enqueueAction(getProjectDetailsAction);
            
        }
        else{
            component.set("v.projectRecord", []);
        }
    },
        //start date validation
    startdateUpdate: function(component, event, helper) {
        $('.slds-align_absolute-center').attr("type", "button");
        var startDateVal = component.find("startDate").get("v.value");
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if (dd < 10) {
            dd = '0' + dd;
        }
        // if month is less then 10, then append 0 before date    
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.startDate") != undefined && component.get("v.enddate") != undefined) {
            component.set("v.startdateValidationError", false);
            component.set("v.emptyStartdateValidationError", false);
            component.set("v.enddateValidationError", false);
        } else if (component.find("enddate").get("v.value") != '' && component.find("startDate").get("v.value") >= component.find("enddate").get("v.value")) {
            component.set("v.startdateValidationError", false);
            component.set("v.enddateValidationError", true);
            component.set("v.emptyStartdateValidationError", false);
        } else {
            component.set("v.startdateValidationError", false);
            component.set("v.emptyStartdateValidationError", false);
            component.set("v.enddateValidationError", false);
        }
    },
    
    //End Date Validation - should be greater that start date, and start date shouldnot be empty
    endDateUpdate: function(component, event, helper) {
        $('.slds-align_absolute-center').attr("type", "button");
        var startDateField = component.get("v.startDate");
        if (component.find("enddate").get("v.value") != '' && component.find("enddate").get("v.value") <= component.find("startDate").get("v.value")) {
            component.set("v.enddateValidationError", true);
            component.set("v.emptyStartdateValidationError", false);
        } else if ((component.find("startDate").get("v.value") == "" || component.find("startDate").get("v.value") == null) && component.find("enddate").get("v.value") != '') {
            component.set("v.emptyStartdateValidationError", true);
            component.set("v.enddateValidationError", false);
        } else {
            component.set("v.enddateValidationError", false);
            component.set("v.emptyStartdateValidationError", false);
        }
    },
    // method call to toggle impact category dropdown and on edit based on data check the checkbox
    toggleDropdown: function(component, event, helper) {
        var impDrpdwn = $("#myDropdown");
        if ((impDrpdwn.css("display") === "none") || (impDrpdwn.css("display") === "")) {
            impDrpdwn.css("display", "block");
            helper.toggleArrowState();
            var checkBoxes = $("#myDropdown input");
            var selImpList = component.get("v.selectedImpList");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (selImpList.includes(checkBoxes[i].name)) {
                    checkBoxes[i].checked = true;
                }
            }
        } else {
            impDrpdwn.css("display", "none");
            helper.toggleArrowState();
        }
        $(document).click(function(e) {
            if ($(e.target).closest('#myInput').length === 0 && $(e.target).closest('#myDropdown').length === 0 && $(e.target).closest('#searchboxArrow').length === 0 && $(e.target).closest('#impSelClose').length === 0) {
                $('#myDropdown').toggle(false);
                helper.toggleArrowState();
            }
        }); 
    },
    
    //function call to display the list of data for dropdown and filter based on input data entered
    filterFunction: function(component, event, helper) {
            var input, filter, a, i;
            $("#myDropdown").css("display", "block");
            input = $("#myInput").val();
            filter = input.toUpperCase();
            a = $("#myDropdown li");
            for (i = 0; i < a.length; i++) {
                if (a[i].textContent.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
    },
    
    // Function call to collect all the selected data
    CheckboxSelect: function(component, event, helper) {
        var selectedList = [];
        var checkBoxes = $("#myDropdown input");
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked) {
                var value = checkBoxes[i].name;
                selectedList.push(value);
            }
        }
        component.set("v.selectedImpList", selectedList);
    },
    
    // method call when use deselect impact category checkbox
    delSelected: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var id = selectedItem.dataset.id;
        // $("li #" + id).css("display", 'none');
        
        var checkBoxes = $('input');
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].name == id) {
                checkBoxes[i].checked = false;
            }
        }
        var selectedImpactCategories = component.get("v.selectedImpList");
        
        var projectData = new Object();
        projectData.impactCategory = selectedImpactCategories;
        var index = projectData.impactCategory.indexOf(id);
        if (index > -1) {
            projectData.impactCategory.splice(index, 1);
        }
        component.set("v.selectedImpList", projectData.impactCategory);
        
    },
    
    // method call when use deselect For codes checkbox
    delForSelected: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var id = selectedItem.dataset.id;
        $("#" + id).css("display", 'none');
        
        var checkBoxes = $('input');
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].name == id) {
                checkBoxes[i].checked = false;
            }
        }
        var selectedForCodesList = component.get("v.selectedForCodes");
        
        var projectData = new Object();
        projectData.forCodes = selectedForCodesList;
        var index = projectData.forCodes.indexOf(id);
        if (index > -1) {
            projectData.forCodes.splice(index, 1);
        }
        component.set("v.selectedForCodes", projectData.forCodes);
        
    },
    // method call to toggle For codes dropdown and on edit based on data check the checkbox
    toggleForDropdown: function(component, event, helper) {
        var forDrpdwn = $("#forDropdown");
        if ((forDrpdwn.css("display") === "none") || (forDrpdwn.css("display") === "")) {
            forDrpdwn.css("display", "block");
            helper.toggleforArrowState();
            var checkBoxes = $("#forDropdown input");
            var selForList = component.get("v.selectedForCodes");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (selForList.includes(checkBoxes[i].name)) {
                    checkBoxes[i].checked = true;
                }
            }
        } else {
            forDrpdwn.css("display", "none");
            helper.toggleforArrowState();
        }
        $(document).click(function(e) {
            if ($(e.target).closest('#frocodes').length === 0 && $(e.target).closest('#forDropdown').length === 0 && $(e.target).closest('#forsearchboxArrow').length === 0 &&  $(e.target).closest('#forSelClose').length === 0) {                
                $('#forDropdown').toggle(false);
                helper.toggleforArrowState();
            }
        }); 
    },
    //function call to display the list of data for dropdown and filter based on input data entered
    filterForFunction: function(component, event, helper) {
            var input, filter, ul, li, a, i;
            $("#forDropdown").css("display", "block");
            input = $("#frocodes").val();
            filter = input.toUpperCase();
            a = $("#forDropdown li");
            for (i = 0; i < a.length; i++) {
                if (a[i].textContent.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
    },
    
    // Function call to collect all the selected data
    CheckboxForSelect: function(component, event, helper) {
        var selectedForList = [];
        var forCheckBoxes = $("#forDropdown input");
        for (var i = 0; i < forCheckBoxes.length; i++) {
            if (forCheckBoxes[i].checked) {
                var value = forCheckBoxes[i].name;
                selectedForList.push(value);
            }
        }
        component.set("v.selectedForCodes", selectedForList);
    },
    
    //Remove project title empty error message on keyup
    removeTitleErrorMsg: function(component, event, helper) {
        component.set("v.emptyPrjTitle", false);
    },
    
    //Remove project summary empty error message on keyup
    removeSummaryErrorMsg: function(component, event, helper) {
        component.set("v.emptyPrjSummary", false);
        
    },
    //Method call on click of save button and check validation
    buttonClick: function(component, event, helper) {        
        if ((component.get("v.projectRecord").length ==0) ||
            (component.get("v.projectRecord") && component.get("v.projectRecord").IsRMProject == false)) {
            var prjTitle = component.find("projecttitle").get("v.value");
            var prjSummary = component.find("projectSummary").get("v.value");
            var radioSelectedValue = $('input[name="options"]:checked').data('value');
            var isStartDateError = component.get("v.startdateValidationError");
     	    var isEndDateError = component.get("v.enddateValidationError");
        	var isEmptyStartdateValidationError = component.get("v.emptyStartdateValidationError");
            
            if ((prjTitle == "" || prjTitle == undefined) && (prjSummary == "" || prjSummary == undefined)) {
                $A.util.addClass(component.find("projecttitle"), "slds-has-error");
                $A.util.addClass(component.find("projectSummary"), "slds-has-error");
                component.set("v.emptyPrjTitle", true); 
                component.set("v.emptyPrjSummary", true);
                event.preventDefault();            
            } else
                if ((prjTitle == "" || prjTitle == undefined) && (prjSummary != "" || prjSummary != undefined)) {
                    component.set("v.emptyPrjTitle", true);
                    $A.util.addClass(component.find("projecttitle"), "slds-has-error");
                    event.preventDefault();
                } else if ((prjSummary == "" || prjSummary == undefined) && (prjTitle != "" || prjTitle != undefined)) {
                    component.set("v.emptyPrjSummary", true);
                    $A.util.addClass(component.find("projectSummary"), "slds-has-error");
                    event.preventDefault();
                } 
                 else  if (isEmptyStartdateValidationError ==true || isEndDateError == true || isStartDateError ==true) {
            		event.preventDefault();
       				 } 
                    else {
                        if (component.get("v.projectRecord").length != 0) {
                            var prevPjtAccess = component.get("v.projectRecord").accesss;
                            var accessMap = {
                                Private: $A.get("$Label.c.privateAccessMsg"),
                                Members: $A.get("$Label.c.MemberAccessMsg"),
                                RMITPublic: $A.get("$Label.c.RMITPublicAccessMsg")
                            }
                            if (prevPjtAccess !== radioSelectedValue) {
                                component.set("v.confMessage", accessMap[radioSelectedValue.replace(/ /g, '')]);
                                $('#confirm').modal({});
                            } else {
                                helper.saveHelper(component, event, helper);
                                $('#confirm').modal('hide');
                            }
                        } else {
                            helper.saveHelper(component, event, helper);
                            $('#confirm').modal('hide');
                        }
                    }
        }
        else {
             if ((component.get("v.projectRecord").length ==0) ||
            (component.get("v.projectRecord") && component.get("v.projectRecord").IsRMProject == true)) {
                            var prevPjtAccess = component.get("v.projectRecord").accesss;
                  			var radioSelectedValue = $('input[name="options"]:checked').data('value');
                            var accessMap = {
                                Private: $A.get("$Label.c.privateAccessMsg"),
                                Members: $A.get("$Label.c.MemberAccessMsg"),
                                RMITPublic: $A.get("$Label.c.RMITPublicAccessMsg")
                            }
                            if (prevPjtAccess !== radioSelectedValue) {
                                component.set("v.confMessage", accessMap[radioSelectedValue.replace(/ /g, '')]);
                                $('#confirm').modal({});
                            } else {
                                helper.saveHelper(component, event, helper);
                                $('#confirm').modal('hide');
                            }
                        }
        }
        
    },
    // Method call on click of cancel button
    cancelClick: function(component, event, handler) {
        var urlEvent = $A.get("e.force:navigateToURL");
        if (component.get("v.projectRecord.IsEdit")) {
            urlEvent.setParams({
                "url": "/project-details?recordId=" + component.get("v.recordId")
            });
            urlEvent.fire();
        } else {
            urlEvent.setParams({
                "url": "/"
            });
            urlEvent.fire();
        }
    },
    // Method call on click of popup cancel button and reset the project access back to database data
    cancelSave: function(component) {
        $('#confirm').modal('hide');
        var prevPjtAccess = component.get("v.projectRecord").accesss;
        document.querySelector('input[name=options][data-value="' + prevPjtAccess + '"]').checked = true;
    },
    // Method call on click  of save button in popup
    confirmSave: function(component, event, helper) {
        helper.saveHelper(component, event, helper);
        $("#confirm").modal('hide');
    },
    
    //when user types keyword and clicks enter in the input box
    enterKeyWord: function(component, event, helper) {
        if (event.keyCode === 13) {
            event.preventDefault();
            helper.addKeywords(component);
        }
    },
    
    //Method call on click of add button
    addKeywordBtnClick: function(component, event, helper) {
        event.preventDefault();
        helper.addKeywords(component);
    },
    
    // Method call to delete selected keywords
    delkeywords : function(component, event, helper){
        var selectedItem = event.currentTarget;
        var id = selectedItem.dataset.id;
        $("#" + id).css("display", "none");
        var selectedkeywords = component.get("v.pjtKeywords");
        
        var projectData = new Object();
        projectData.keywords = selectedkeywords;
        var index = projectData.keywords.indexOf(id);
        if (index > -1) {
            projectData.keywords.splice(index, 1);
        }
        component.set("v.pjtKeywords", projectData.keywords);
    },
    keyEnterFunction :function(component, event, helper){
        if(event.keyCode ===13){
            event.preventDefault();
        }
    }
    
})