({
	doInit : function(component, event, helper) {
		var action = component.get("c.createCampaign");
        action.setParams({
            "Id": component.get("v.recordId")
        });
         action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue == 'true'){
                    component.set('v.successCreate', 'Campaign Created');
                }
                $A.get("e.force:refreshView").fire();
            } else {
                var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Error Occured",
                        "message": "An error occured while creating campaign"
                    });
                resultsToast.fire();
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	}
})