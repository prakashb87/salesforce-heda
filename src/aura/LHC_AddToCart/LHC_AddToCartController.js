({
    doInit : function(component, event, helper) {
        console.log('document.referrer: ' + document.referrer);
        localStorage.setItem("marketplace-referrer", document.referrer);
        var action = component.get('c.getHeight');
        action.setCallback(this, function(response){
            
            var result = response.getReturnValue();
            var state = response.getState();
            if(state ==='SUCCESS'){
                component.set("v.height",result);
            }else{
                console.log('Error While Fetching Height');
            }                
        });
        $A.enqueueAction(action);  
        
        var flag = false;
        var action = component.get('c.userLoginCheck');
        console.log('<<A>>');
        
        action.setCallback(this, function(response){
            
            var result = response.getReturnValue();
            console.log('usertype--->'+result);
            var state = response.getState();
            
            if(result !='Guest'){
                console.log('<<B>>');
                
                component.set("v.Spinner", true);
                
                helper.getErrorURL(component);
                
                //ECB-3811: Updated the Code to make it IE 11 compatible :Starts
                var courseName = component.searchParam("coursename");
                var productReference = component.searchParam("PRDref");
                var sessionStartDate;
                var sessionEndDate;
                var courseOfferingId = '';
                //ECB-4424:Tech Story -- Modify AddtoCart Page controller for new products/programs
                //ProgramPlan parameter for the Program is not included here.
                console.log('<<C>>');
                var deliveryMode;
                var productLine = component.searchParam('productLine');
                var courseType = component.searchParam('courseType');
                if(courseType == 'Course' || courseType == 'course')
                    courseOfferingId = component.searchParam("courseoffrID");
                var programPlan;       
                
                if(component.searchParam("SStartdate") == 'null'){
                    sessionStartDate = '';
                }else{
                    sessionStartDate = component.searchParam("SStartdate");
                }
                if(component.searchParam("SEnddate") == 'null'){
                    sessionEndDate = '';
                }else{
                    sessionEndDate = component.searchParam("SEnddate");
                }
                
                
                //ECB-4424:Tech Story -- Modify AddtoCart Page controller for new products/programs
                if(component.searchParam("deliveryMode")== 'null'){
                    deliveryMode = '';
                }
                else{
                    deliveryMode = component.searchParam('deliveryMode'); 
                }
                
                if(component.searchParam("programPlan")== 'null'){
                    programPlan = '';
                }
                else{
                    programPlan = component.searchParam('programPlan'); 
                }
                
                // Condition Check Suggested Done
                console.log('<<D>>');
                //if(courseName != null && productLine != null && courseType != null && productReference != null){          
                console.log('CN ' + courseName);
                if(courseName || productLine || courseType || productReference ){// && productLine != null && courseType != null && productReference != null){          
                    //Old call      
                    console.log('<<E>>');
                    flag = true;           
                    var action = component.get('c.addProducttoBasket');
                    action.setParams({
                        courseName : courseName,
                        courseOfferingId : courseOfferingId,
                        productReference : productReference,
                        sessionEndDate : sessionEndDate,
                        sessionStartDate : sessionStartDate,
                        //ECB-4424:Tech Story -- Modify AddtoCart Page controller for new products/programs
                        deliveryMode :deliveryMode,
                        productLine :productLine,
                        courseType :courseType,
                        programPlan :programPlan
                    });
                    
                    action.setCallback(this, function(a){
                        
                        console.log('State==============>'+a.getReturnValue().Status);
                        if(a.getReturnValue().Status=='Pass'){
                            var urlEvent = $A.get("e.force:navigateToURL");
                            urlEvent.setParams({
                                "url": "/viewcart"
                                
                            });
                            urlEvent.fire();
                            component.set("v.Spinner", false);
                        }else if(a.getReturnValue().Status === "Fail"){
                            
                            // ECB -3762 Error Message - Rishbah :Starts
                            console.log('ERROR in delete');
                            var msg = a.getReturnValue().Message;
                            console.log('Message-->'+msg);
                            var errorURL = component.get("v.ViewCartErrorURL")+'&Message='+msg;
                            window.open(errorURL,'_top');
                            component.set("v.Spinner", false);
                            // ECB -3762 Error Message - Rishbah :Ends
                        }
                    });
                    
                    $A.enqueueAction(action);                    
                    // ECB-4614 Method -- Starts 
                    
                    
                    // ECB-4614 Method  -- Ends
                }
                if(flag == false)
                {
                    console.log('<<F>>');
                    helper.cartCreation(component);  
                } 
                
            }
            else{
                // When the User is Not Logged in Redirecting to the VF Page with the URL Stored as message.
                
                var action = component.get('c.getCommunityURL');
                
                action.setCallback(this, function(response){
                    
                    var result = response.getReturnValue();
                    var state = response.getState();
                    
                    if(state ==='SUCCESS'){
                        
                        component.set('v.vfHost',result);
                    }else{
                        console.log('Error While Fetching Domain Name');
                    }
                }); 
                
                $A.enqueueAction(action);  
                
            }
            
        });
        $A.enqueueAction(action);
    },
    
    //ECB-3811
    searchParam : function(component, event, helper) {
        
        
        var params = event.getParam('arguments');
        var name;
        if(params){
            name = params.param1;
        }
        
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        
    }
    
})