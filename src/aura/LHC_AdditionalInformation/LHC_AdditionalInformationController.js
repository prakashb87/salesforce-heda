({
    doInit : function(component, event, helper) {
        
        helper.doInit(component,helper);
        
    },
    
    NavigateToTnC : function (component, event, helper) {
        
        helper.homeFieldsValidation(component,event,helper);
        
        
    },
    
    viewCart : function(component, event, helper) {
        
        console.log('Enroll');
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/viewcart"
            
        });
        urlEvent.fire();
    },
    nextButtonValidation : function(component,event,helper){
        helper.getNextButton(component);
    }
})