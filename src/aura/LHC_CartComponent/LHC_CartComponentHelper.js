({
    jsLoad : function(component,event,helper) 
    {
        window.history.pushState(null, null, '/MarketPlace/s/viewcart');
        
    },
    
    getDeliveryMode : function(component) 
    {	
        
        var action = component.get('c.getDeliveryMode');
        //component.set("v.Spinner", true); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('response:::'+response.getReturnValue());
                component.set("v.deliveryModeWrapperList",response.getReturnValue());
                //component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    getUpdatedBasketDetails : function(component) 
    {
        
        var action = component.get('c.getresult');
        
        action.setCallback(this, function(response) {
            var courseList = component.get("v.ProductBasket").Courses;
            console.log(courseList.length);
            var totalPrice = 0; 
            if(!$A.util.isEmpty(courseList)){
                for(var i=0;i<courseList.length;i++){
                    console.log(courseList[i].Price);
                    totalPrice = totalPrice+Number(courseList[i].Price); 
                    console.log(totalPrice);
                }
                
            } 
            
            
            var result = response.getReturnValue();
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log(result.TotalPrice);
                component.set("v.ProductBasket.TotalPrice",result.TotalPrice);
                var disAmt = (totalPrice - Number(result.TotalPrice)).toFixed(2); 
                console.log('disAmt:::'+disAmt);
                if(disAmt > 0){   
                    component.set("v.promoCodeDiscountAmt",disAmt);
                    $A.util.addClass(component.find('promodiv'),'showDiv');
                    $A.util.removeClass(component.find('promodiv'),'hideDiv');
                    component.set("v.Spinner", false);
                    component.set("v.message",'Promo code successfully applied');
                    component.set("v.isOpen", true);
                    console.log('pass');
                }else{
                    console.log('error');
                    component.set("v.Spinner", false);
                    component.set("v.message","Invalid code. The entered promo code is invalid, or is no longer available");
                    component.set("v.isOpen", true);
                   /* $A.util.addClass(component.find('promodiv'),'hideDiv');
                    $A.util.removeClass(component.find('promodiv'),'showDiv');
                    
                    console.log('tot::::'+localStorage.getItem('prevTotalPrice')); 
                    component.set("v.ProductBasket.TotalPrice",totalPrice);*/
                }
                
                
            }
            
            
        });
        $A.enqueueAction(action);
    },
    navigate : function(component,event,helper) {
        var checkbox;
        checkbox=component.find("IndividualtandC").get("v.checked");
        console.log('check::::'+checkbox);
        if(checkbox == true){
            var action = component.get("c.getCitizenshipStatus");
            action.setCallback(this, function(response){
                var result = response.getReturnValue();
                var flag = result.Status;
                var state = response.getState();
                console.log('result::::'+result);
                if(result == true){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/additional-info",
                    });
                    urlEvent.fire(); 
                }else{
                    
                    var finalProductBasket = component.get("v.ProductBasket");

                    $A.util.removeClass(component.find('tcerror'),'showDiv'); 
                    $A.util.addClass(component.find('tcerror'),'hideDiv'); 
                    if(finalProductBasket.TotalPrice == '0.00'){
                        var basketNumber = finalProductBasket.BasketNumber;
                        var action = component.get("c.confirmEnroll");
                        action.setParams({
                            "receiptNo" : "n/a",
                            "basketNo"  : basketNumber
                        }); 
                        
                        
                        action.setCallback(this, function(response){
                            var result = response.getReturnValue();
                            var flag = result.Status;
                            var state = response.getState();
                            if (state === "SUCCESS") {
                                
                                if(flag === "Pass"){
                                    var urlEvent = $A.get("e.force:navigateToURL");
                                    urlEvent.setParams({
                                        "url": "/successpage",
                                        
                                    });
                                    urlEvent.fire(); 
                                }else{
                                    var errors = response.getError();
                                }
                                component.find("accept").set("v.disabled", "false"); 
                            }
                        });
                        $A.enqueueAction(action);
                    }else{
                        var finalProductBasket = component.get("v.ProductBasket");
                        
                        var orderId = finalProductBasket.BasketNumber;
                        var amount = finalProductBasket.TotalPrice;
                        var action = component.get("c.getEmail");
                        action.setCallback(this, function(response){
                            var state = response.getState();
                            var email= response.getReturnValue();
                            if(state === 'SUCCESS'){
                                //ECB-3702 Open OneStop link in same window
                                /* var urlEvent1 = $A.get("$Label.c.PAYNOW_ONESTOP_URL")+'&MPB='+orderId+'&EMAIL='+email+'&UNITAMOUNTINCTAX='+amount;
                                window.open(urlEvent1,'_top'); */
                                
                                var action = component.get("c.getOnestopURL");
                               
                                
                                action.setCallback(this, function(response) {
                                    var state = response.getState();
                                    var url = response.getReturnValue();
                                    if (state === "SUCCESS") {
                                        if(response.getReturnValue() !=null){
                                            /*var urlEvent1 = url+'&MPB='+orderId+'&EMAIL='+email+'&UNITAMOUNTINCTAX='+amount;
                                            window.open(urlEvent1,'_top');*/
                                            //self.location = url+'&MPB='+orderId+'&EMAIL='+email+'&UNITAMOUNTINCTAX='+amount;
											//ECB-5656
                                        	self.location = url+'&MPB='+orderId+'&UNITAMOUNTINCTAX='+amount;
 
                                        }                    
                                    }    
                                });
                                $A.enqueueAction(action);
                            }else{
                            }
                            component.find("accept").set("v.disabled", "false");   
                        });
                        $A.enqueueAction(action);
                        
                    }  
                }
            })
        }
        else{
            $A.util.addClass(component.find('tcerror'),'showDiv'); 
            $A.util.removeClass(component.find('tcerror'),'hideDiv'); 
        } 
        $A.enqueueAction(action);
    },
    
    //added for 5766
    updateCartDetails : function(component) 
   {
       console.log('update');
       var action = component.get('c.removePromoCode');
       action.setParams({
           "promoCode" : component.get("v.promocode"),
           
       });
       action.setCallback(this, function(response) {
           var result = response.getReturnValue();
           var state = response.getState();
           console.log(result);
           
            if (state === "SUCCESS") {
                console.log(result.Status);
                if(result.Status=='Pass'){
                    console.log(result.TotalPrice);
                   component.set("v.ProductBasket.TotalPrice",result.TotalPrice);
                   $A.util.addClass(component.find('promodiv'),'hideDiv');
                   $A.util.removeClass(component.find('promodiv'),'showDiv');
                }
            }
           
        });
        $A.enqueueAction(action); 
   }    
    
    
})
