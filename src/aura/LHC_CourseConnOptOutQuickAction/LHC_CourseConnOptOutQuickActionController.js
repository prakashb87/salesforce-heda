({
	doInit : function(component, event, helper) {
		 var dismissActionPanel = $A.get("e.force:closeQuickAction");
	   dismissActionPanel.fire();
        var recordId = component.get("v.recordId");
        console.log('recordId:::::'+recordId);

        var action = component.get("c.setCourseConnStatus");

        action.setParams({"courseConnId" : recordId}); 

        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                
                if(response.getReturnValue() == 'Error1'){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "",
                        "message": $A.get("$Label.c.Status_Already_Exists"),
                        "type": "Error"
                    });
                    resultsToast.fire();
                    
                }else if(response.getReturnValue() == 'Error2'){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "",
                        "message": $A.get("$Label.c.Status_Cannot_be_Edited"),
                        "type": "Error"
                    });
                    resultsToast.fire();
                }else{
                   $A.get('e.force:refreshView').fire();
                   
                   
                }
            } 

        });
        $A.enqueueAction(action);

        
	},
    
    closeQuickAction: function(component, event, helper) {
        
       var dismissActionPanel = $A.get("e.force:closeQuickAction");
	   dismissActionPanel.fire();

    }
})