({
	doInit : function(component, event, helper) {
		helper.processCredlyIntegration(component);
		console.log("LC_CredlyCourseConnectionQuickActionController.doInit: exited");
	},

	closeQuickAction: function(component, event, helper) {
        
        $A.get("e.force:closeQuickAction").fire();

    }

})