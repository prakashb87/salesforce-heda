({
    myDetails : function(component, event, helper) {
        
        var action = component.get('c.getMyDetails');
        
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
            var state = response.getState();
            
            
            if(state ==='SUCCESS'){
                component.set('v.MyDetails',result);
            }else{
                var errors = response.getError();
                console.log('Error in Details');
            }
            
        });
        $A.enqueueAction(action);
    },
    
    purchaseHistory : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/purchasehistory"
            
        });
        urlEvent.fire();     
    }, 
    
    myProgress : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myproduct"
            
        });
        urlEvent.fire();     
    }, 
  
    // ECB-4962 
    navigation : function(component, event, helper) {
        
        var action = component.get('c.getServiceAndSupportURL');
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() !=null){
                    var shoppingURL = response.getReturnValue();
                    window.open(shoppingURL,'_blank'); 
                    //self.location = shoppingURL;
                }                    
            }    
        });  
        
        $A.enqueueAction(action);
        
    },
    
    
})