({
    myProduct : function(component, event, helper) {
	   
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myproduct"
            
        });
        urlEvent.fire();
    },
    
    myPurchaseHistory: function(component,event,helper){
      
        var action = component.get('c.getMyPurchaseDetails');
       
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
   		    var state = response.getState();
            console.log('result>>>>'+result);
            
            if(state ==='SUCCESS'){
                component.set('v.PurchaseDetails',result);
            }else{
                var errors = response.getError();
                console.log(errors);
                console.log('Error in Purchase Details');
            }
    
        });
        $A.enqueueAction(action);
        
    },
    //Redirection to My Details
  
     myDetails : function(component, event, helper) {
	   
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
           "url": "/mydetails"
           
        });
        urlEvent.fire();     
    },  
    
      
   
})