({
    successPageRedirect : function(component) {
        console.log('inside helper');
        var action = component.get("c.getSuccessPageURL");
        
        action.setCallback(this, function(a){
            var state = a.getState();
            if (a.getState() === "SUCCESS"){
                component.set("v.successUrl", a.getReturnValue());
                component.set("v.isReady", true);
            }else{
                console.log("Failed>>> getSuccessPageURL");
            }
        });
        
        $A.enqueueAction(action);
        
    }, // New Code here
})