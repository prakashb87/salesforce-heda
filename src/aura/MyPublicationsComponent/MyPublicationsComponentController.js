({
    //on launch of ethics page, create table and get the ethics related to the user. sort ethics by Pending status
    onLoad: function(component, event, helper) {
        helper.getcheckInternalDualAccessUser(component, event, helper);
        component.set("v.pageCount",1);
        component.set("v.isFilterApplied", false);
        $('.totalcontainer')[0].style.display ="none";
        helper.fetchPublicationStatusValue(component, event, helper);
        helper.fetchPublicationCategoryValue(component, event, helper);
        var getPublicationsListAction = component.get("c.getPublicationListWrapper");
        
        getPublicationsListAction.setCallback(this, function(response) {
            
            if (response.getState() == "SUCCESS") {
                var fullResult = JSON.parse(response.getReturnValue());
                if(component.get('v.isInternalDualAccessUser') == true){
                        fullResult = [];
                    	fullResult.totalPublications =0;
                        component.set("v.publicationsList", fullResult);
                    }
                var filteredData = []
                let resultList = fullResult.publicationWrapperList && fullResult.publicationWrapperList.length >0 ?
                    fullResult.publicationWrapperList:[]
                resultList.forEach((item)=>{
                    var newItem = Object.assign({}, item);
                    let filteredOutlet = newItem.outlet ? newItem.outlet.replace(/\n/ig, ' '):''
                    newItem.outlet = filteredOutlet
                    filteredData.push(newItem)
                })
                var result = {
                    totalPublications:fullResult.totalPublications,
                    publicationWrapperList:filteredData
                }
                var dataSet = filteredData;
                var returnData = [];
                $.each(dataSet, function(index, value) {
                    var rowData = []; 
                    rowData.push(value.publicationEcode != undefined ? value.publicationEcode : '');
                    rowData.push(value.year != undefined ? value.year : '');
                    rowData.push(value.category != undefined ? value.category : '');
                    rowData.push(value.title != undefined ? value.title : '');
                    rowData.push(value.outlet != undefined ? value.outlet : '');
                    rowData.push(value.authors != undefined ? value.authors : '');
                    rowData.push(value.status != undefined ? value.status : '');
                    rowData.push(value.publicationId !=undefined ? value.publicationId : '');
                    returnData.push(rowData);
                });               
                component.set("v.publicationsList", result);
                component.set("v.publicationsTotalCount", result.totalPublications);
                component.set("v.publicationsCurrentPageCount", 25);
                if(result.totalPublications <= 25){
                    component.set("v.publicationsCurrentPageCount", result.totalPublications);
                }
                if(result.totalPublications > 0){
                     $('.totalcontainer')[0].style.display ="block";
                }
                jQuery("document").ready(function() {
                    var table = $('#myPublicationsList').DataTable({
                        data: returnData,
                        "pageLength": 25,
                        "language": {
                             "emptyTable":  $A.get("$Label.c.Youdonothaveanypublicationstoview")
                        },
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [
                            {
                            title: $A.get("$Label.c.contractCode")
                        }, {
                            title: $A.get("$Label.c.Year")
                        }, {
                            title: $A.get("$Label.c.Category")
                        }, {
                            title: $A.get("$Label.c.Title")
                        }, {
                            title: $A.get("$Label.c.Outlet")
                        }, {
                            title: $A.get("$Label.c.Authors")
                        }, {
                            title: $A.get("$Label.c.Status")
                        }],
                        "info": false,
                        "dom": 'frt',
                        "drawCallback": function(oSettings) {
                            // Show or hide "Load more" button based on whether there is more data available
                            $('#btnLoadMore').toggle(this.api().page.hasMore());
                            oSettings.oLanguage.sEmptyTable = $A.get("$Label.c.Youdonothaveanyfilteredpublicationstoview")
                          //  $A.get("$Label.c.Youdonothaveanyfilteredpublicationstoview")
                        },                       
                        aaSorting : [],
                        "columnDefs": [
                            {
                                targets: 5,
                                "orderable": false
                            },
                            {
                                targets: [4,5],
                                "render": function(data, type, full, meta, row) {
                                    return '<div class="truncate"><a class="pjtTitleColor" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '" >' + data + '</a>';
                                },
                            },
                             {
                                targets: 3,
                                "render": function(data, type, full, meta, row) {
                                    return '<div class="truncate"><a class="rmit-blue-color rmit-blue-color_hover" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '" href="' + '/Researcherportal/s/publications-details-page?recordId=' + full[7] + '">' + data + '</a>';
                                },
                            },
                          
                        ]
                            
                            });
                            
                            // Handle click on "Load more" button
                            $('#btnLoadMore').on('click', function() {
                            // Load more data
                            table.page.loadMore();
                            var currentPage = component.get("v.pageCount");
                            currentPage++;
                            component.set("v.pageCount",currentPage);
                            let totalPublications = component.get("v.publicationsTotalCount");
                            helper.updatePublicationsCount(component,totalPublications, currentPage);
                            });
                            });
                            }
                            });
                            $A.enqueueAction(getPublicationsListAction);
                            $("document").ready(function() {
            var offset = 250;
            
            var duration = 300;
            
            $(window).scroll(function() {
                if ($(this).scrollTop() > offset) {
                    $("#back-to-top").fadeIn(duration);
                } else {
                    $("#back-to-top").fadeOut(duration);
                }
                
            });
            
            $("#back-to-top").click(function(a) {
                
                a.preventDefault();
                
                $("html, body").animate({
                    scrollTop: 0
                }, duration);
                
                return false;
            });
            
            $('[data-toggle="tooltip"]').tooltip();
        }); 
},
 // method call to toggle impact category dropdown and on edit based on data check the checkbox
    toggleDropdown: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        
        var targetId = targetCmp.id;
        
        var type = targetCmp.dataset.type;
        
        var topicDropdown = $('#' + type + 'Dropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
            
        } else {
            topicDropdown.css("display", "none");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
        }
        
        $(document).click(component, function(e) {
            if ($(e.target).closest('#categoryfilter').length === 0 && $(e.target).closest('#categoryDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#categoryDropdown').is(":visible")) {
                $('#categoryDropdown').toggle(false);
                helper.toggleArrowState('#categoryDropdown', '#arrowcategory');
                var topic = $('#categoryfilter').data('selected');
                $('#categoryfilter').val(topic);
            }         
            else if ($(e.target).closest('#statusfilter').length === 0 && $(e.target).closest('#statusDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#statusDropdown').is(":visible")) {
                $('#statusDropdown').toggle(false);
                helper.toggleArrowState('#statusDropdown', '#arrowstatus');
                var topic = $('#statusfilter').data('selected');
                $('#statusfilter').val(topic);
            }
            
        });
    },
    selectTopic: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        var topic = targetCmp.dataset.value;
        var targetId = targetCmp.parentElement.id;
        var statusval = $('#statusfilter').val();
        var typeval = $('#categoryfilter').val(); 
        var publications = component.get("v.publicationsList").publicationWrapperList;
        if (targetId === 'categoryDropdown') {
            component.set("v.selectedCategory", topic);
            $('#categoryfilter').data('selected', topic);
            $('#categoryfilter').val(topic);
            $('#categoryDropdown').toggle(false);
            helper.toggleArrowState('#categoryDropdown', '#arrowcategory');
            var typeval = $('#categoryfilter').val(); 
            helper.fetchFilteredPublications(typeval,statusval,publications,component);
        } else if (targetId === 'statusDropdown') {
            component.set("v.selectedStatus", topic);
            $('#statusfilter').data('selected',topic);
            $('#statusfilter').val(topic);
            $('#statusDropdown').toggle(false);
            helper.toggleArrowState('#statusDropdown', '#arrowstatus');  
            var statusval = $('#statusfilter').val();
            helper.fetchFilteredPublications(typeval,statusval,publications,component);
        }        
    }
});