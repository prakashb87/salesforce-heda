({
   addRows: function(tableId, dataset) {
       var table = $(tableId).ready().DataTable();       
        if (!Array.isArray(dataset))
            dataset = [dataset];

        var data = [];
        dataset.forEach(function(milestone) {
            var temp = [];
            temp.push(milestone["publicationEcode"] != undefined ? milestone["publicationEcode"] : '');
            temp.push(milestone["year"] != undefined ? milestone["year"] : '');
            temp.push(milestone["category"] != undefined ? milestone["category"] : '');
            temp.push(milestone["title"] != undefined ? milestone["title"] : '');
            temp.push(milestone["outlet"] != undefined ? milestone["outlet"] : '');
            temp.push(milestone["authors"] != undefined ? milestone["authors"] : '');
            temp.push(milestone["status"] != undefined ? milestone["status"] : '');
            temp.push(milestone["publicationId"] != undefined ? milestone["publicationId"] : '');

            data.push(temp);
        });

        //table.order.neutral().draw(); 
        table.rows.add(data).draw();
       
    },
	// change arrow up when impact category dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function(dropdownId, arrowId) {
         if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },
    
    fetchFilteredPublications : function(category,status,publications,component){
        var filtermilestones =   $.map(publications,function(val,key){
            if((val.category == category || category == "") || category == $A.get("$Label.c.All")) {
                if(val.status == status){
                    return val;
                }
                else if(status == $A.get("$Label.c.All")){
                    return val;
                }
            }  
        });
        $('.totalcontainer')[0].style.display ="block";
        component.set("v.isFilterApplied", true);
        component.set("v.publicationsCurrentPageCount", filtermilestones.length);
        component.set("v.publicationsTotalCount", filtermilestones.length);
        var currentPage = component.get("v.pageCount");
        this.updatePublicationsCount(component,filtermilestones.length, currentPage);
        if(filtermilestones.length == 0){
           $('.totalcontainer')[0].style.display ="none";
        }
        $('#myPublicationsList').DataTable().clear();
        this.addRows("#myPublicationsList", filtermilestones);
    },
    //Method call to fetch Impact category list
    fetchPublicationStatusValue: function(component, fieldName, elementId) {
        var action = component.get("c.getPublicationStatusValue");
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var filterByStatus = response.getReturnValue();
                filterByStatus.push($A.get("$Label.c.All"))
                filterByStatus.sort(); 
                component.set("v.filterByStatus", filterByStatus);  
                component.set("v.selectedStatus", filterByStatus[0]);                
            }
        });
        $A.enqueueAction(action);
    },
    //Method call to fetch Category list
    fetchPublicationCategoryValue : function(component, fieldName, elementId) {
        var action = component.get("c.getPublicationCategoryValue");
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var filterByCategory = response.getReturnValue();
                filterByCategory.push($A.get("$Label.c.All"))
                filterByCategory.sort();
                component.set("v.filterByCategory", filterByCategory); 
        		component.set("v.selectedCategory", filterByCategory[0]);
            }
        });
        $A.enqueueAction(action);
    },
     updatePublicationsCount : function(component, publications, currentPage){
        if(publications <= currentPage*25){
            component.set("v.publicationsCurrentPageCount", publications)
        }
        else{
            component.set("v.publicationsCurrentPageCount", currentPage*25);
        }  
    },
    getcheckInternalDualAccessUser : function(component, event, helper){
        var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
        
        getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
					component.set('v.isInternalDualAccessUser', result);
                }
            }        
        });
          $A.enqueueAction(getcheckInternalDualAccessUserAction);
    }
})