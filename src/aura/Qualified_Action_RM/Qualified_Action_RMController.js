({
	handleRecordUpdated : function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log("Record is loaded successfully. RETURMNNNNN");
            var val = component.get("v.simpleLead.Event_type__c");
            var val1 = component.get("v.simpleLead.Email");
            var val2 = component.get("v.simpleLead.matched_school__c");
            //console.log(val+','+val1+','+val2);
            
            if(val2 == "" || val2 == null){
                component.set("v.missingVal1",'Please enter matched school.');
                component.set("v.processRender",false);
            } 
            if(val1 == "" || val1 == null){
                component.set("v.missingVal2",'Please enter email.');
                component.set("v.processRender",false);
            } 
            if(val == "" || val == null){
                component.set("v.missingVal3",'Please enter Event type.');
                component.set("v.processRender",false);
            }
            if(component.get("v.processRender")){
                
                console.log("Inside Apex Call4 - Changed");
                var action = component.get("c.eventConversionQualified");
                action.setParams({
                    "id": component.get("v.recordId")
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        var returnValue = response.getReturnValue();
                        console.log(returnValue);
                        if(returnValue.startsWith("701")){
                        //if(returnValue.startsWith("003")){
                            var urlEvent = $A.get("e.force:navigateToURL");
                            urlEvent.setParams({
                                "url": "/"+returnValue
                            });
                            urlEvent.fire();
                            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        	dismissActionPanel.fire();
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Success!",
                                "message": "The record has been updated successfully.",
                                "type": "success"
                            });
                            toastEvent.fire();
                        }
                        $A.get("e.force:refreshView").fire();
                    } 
                });
                $A.enqueueAction(action);
            }
        }else if(eventParams.changeType === "ERROR"){
            console.log("Error while loading");
            console.log('Error: ' + component.get("v.leadError"));
		}
    }
	
})