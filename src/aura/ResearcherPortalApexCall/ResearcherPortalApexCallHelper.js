({
    /*
     * This method will call the server side action and will execute callback method
     * it will also show error if generated any
     * @param component (required) - Calling component
     * @param method (required) - Server side methos name
     * @param successCallback (required) - Callback function to be executed on server response
     * @param errorCallback (required) - Callback function to be executed on server response
     * @param params (optional) - parameter values to pass to server
     * @param needToHold (optional) - parameter values to use back on success
     * @param setStorable(optional) - if true, action response will be stored in cache
     *
    */
    callApexServer : function(component, helper, controllerMethod, successCallback, errorCallback, params, needToHold, setStorable) {
        var action = component.get(controllerMethod);
        //Set params if any
        if (params) {
            action.setParams(params);
        }
        if(setStorable){
            actions.setStorable();
        }

        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // pass returned value to callback function
                successCallback(component,helper,response.getReturnValue(), needToHold);
            } else if (state === "ERROR") {
                // generic error handler
                var errors = response.getError();
                errorCallback(component,errors);
            }
        });

        $A.enqueueAction(action);
    },
})