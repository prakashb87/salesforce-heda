({
    doInit: function(component, event, helper) {
        var urlParams = {};
        if (window.location.search) {
            urlParams = window.location.search.substr(1).split('&').map(function(param) {
                return param.split('=').map(function(part) {
                    return decodeURIComponent(part).replace(/\+/g, ' ');
                });
            }).reduce(function(params, param) {
                params[param[0]] = param[1];
                return params;
            }, {});
        }

        component.set('v.tagPath', urlParams.tags);

        if (urlParams.tags) {
            component.set('v.title', urlParams.tags.substr(urlParams.tags.lastIndexOf('/') + 1));
        } else {
            component.set('v.title', 'All Content');
        }
    }
})