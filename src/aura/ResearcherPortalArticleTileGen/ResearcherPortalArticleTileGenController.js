({
    onInit: function(component, event, helper) {
        var pixelPosition = component.get('v.contentAttrs').pixelPosition || '';
        var pixelPositionClasses = [];

        if (pixelPosition.indexOf('top') !== -1) {
            pixelPositionClasses.push('pixel_top');
        } else if (pixelPosition.indexOf('bottom') !== -1) {
            pixelPositionClasses.push('pixel_bottom');
        }

        if (pixelPosition.indexOf('left') !== -1) {
            pixelPositionClasses.push('pixel_left');
        } else if (pixelPosition.indexOf('right') !== -1) {
            pixelPositionClasses.push('pixel_right');
        }

        component.set('v.pixelPositionClasses', pixelPositionClasses.join(' '));
        component.set('v.hasPixel', (pixelPositionClasses.length === 2));
    },

    handleClick: function (component, event, helper) {
        helper.handleClick(component, event, helper);
    }
})