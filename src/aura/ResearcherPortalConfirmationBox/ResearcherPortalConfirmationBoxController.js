({
    doInit: function(component, event, helper) {
         var modalId = component.get('v.modalId')
         console.log(modalId)
    },
    showModalHandler : function(component, event) {
        var params = event.getParam('arguments');
        var modalId = component.get('v.modalId')
        console.log('modalId', modalId)
        component.set('v.modalIndex', params.index)
        if(params.param1 === 'SHOW'){
            $("#"+modalId).modal('show');
        }
        if(params.param1 === 'HIDE'){
            $("#"+modalId).modal('hide');
        }
    },
    callPrimaryAction:function(component, event) {
        var parentObj = component.get("v.parent");
        var modalId = component.get('v.modalId')
        var index = component.get('v.modalIndex')
    	parentObj.parentModalMethod(modalId, index);
    }
})