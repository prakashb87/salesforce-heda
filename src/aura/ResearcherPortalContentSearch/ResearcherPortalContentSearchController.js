({
    doInit: function(component, event, helper) {
        component.set('v.loadState', 'init');

        component.set('v.pageInfo', {
            currentType: '',
        });

        var urlParams = {};
        if (window.location.search) {
            urlParams = window.location.search.substr(1).split('&').map(function(param) {
                return param.split('=').map(function(part) {
                    return decodeURIComponent(part).replace(/\+/g, ' ');
                });
            }).reduce(function(params, param) {
                params[param[0]] = param[1];
                return params;
            }, {});
        }

        var term = urlParams.q;
        component.set('v.term', urlParams.q);

        var pageSize = component.get('v.pageSize');

        helper.callGetKeywords(component, function(error, keywords) {
            component.set('v.keywords', keywords);

            var lowerTerm = term.toLowerCase();

            var splitTerms = lowerTerm.split(' ').filter(function(t) { return t.length > 0; });
            var displayedKeywords = keywords.map(function(keyword) { // rank keywords
                var validPathPart = keyword.toLowerCase().substr(keyword.indexOf('/', 1));
                return {
                    path: keyword,
                    label: keyword.substr(keyword.lastIndexOf('/') + 1),
                    matches: splitTerms.reduce(function(c, t) {
                        if (validPathPart.indexOf(t) !== -1) return c + 1;
                        return c;
                    }, 0)
                };
            }).filter(function(keyword) {
                return keyword.matches > 0;
            }).sort(function(keywordA, keywordB) {
                if (keywordA.matches < keywordB.matches) return 1;
                if (keywordA.matches > keywordB.matches) return -1;
                return 0;
            }).filter(function(keyword, i) {
                return i < 6;
            });

            component.set('v.displayedKeywords', displayedKeywords);
        });

        helper.callGetResults(component, term, function(error, results) {
            var totalResults = results.searchData.length;

            var resultsByType = {
                'ResearcherPortalFAQ': [],
                'ResearcherPortalArticle': [],
            };

            if (totalResults == 0) {
                component.set('v.loadState', 'noResults');
                component.set('v.resultsByType', resultsByType);
                return;
            }

            component.set('v.loadState', 'loading');

            var idToType = {};
            results.searchData.forEach(function(result) {
                resultsByType[result.Content_Type__c].push(result);
                idToType[result.Content__c] = result.Content_Type__c;
            });

            component.set('v.results', results.searchData);
            component.set('v.resultsByType', resultsByType);
            component.set('v.idToType', idToType);
            component.set('v.pageInfo', {
                firstResult: 1,
                lastResult: totalResults > pageSize ? pageSize : totalResults,
                totalResults: totalResults,
                currentPage: 1,
                numPages: Math.ceil(totalResults / pageSize),
                currentType: '',
            });

            var loadPageAction = component.get('c.loadPage');
            loadPageAction.setParams({
                component: component,
                helper: helper,
            });
            $A.enqueueAction(loadPageAction);
        });
    },

    loadPage: function(component, event, helper) {
        var pageInfo = component.get('v.pageInfo');
        var indexStart = pageInfo.firstResult - 1;
        var indexEnd = pageInfo.lastResult - 1;
        var resultSet = component.get('v.resultsByType')[pageInfo.currentType];
        if (!resultSet) {
            resultSet = component.get('v.results');
        }

        var idList = resultSet.filter(function(result, i) {
            return i >= indexStart && i <= indexEnd;
        }).map(function(result) {
            return result.Content__c;
        });

        component.set('v.loadState', 'loading');
        helper.callGetResultPage(component, idList, function(error, bundles) {
            component.set('v.loadState', 'loaded');

            var idToType = component.get('v.idToType');

            var bundleMap = {};
            bundles.forEach(function(bundle) {
                bundleMap[bundle.originId] = bundle;
            });

            component.set('v.resultPage', idList.map(function(originId) {
                return {
                    originId: originId,
                    contentType: idToType[originId],
                    contentAttrs: bundleMap[originId].contentAttributes.en_US.reduce(function(contentAttrs, attr) {
                        contentAttrs[attr.name] = attr.value;
                        return contentAttrs;
                    }, {}),
                }
            }));
        });
    },

    setContentType: function(component, event, helper) {
        var currentType = event.currentTarget.dataset.type;
        var pageSize = component.get('v.pageSize');
        var allResults = component.get('v.results');
        var resultsByType = component.get('v.resultsByType');

        var totalResults;
        if (resultsByType[currentType]) {
            totalResults = resultsByType[currentType].length;
        } else {
            totalResults = allResults.length;
        }

        component.set('v.pageInfo', {
            firstResult: 1,
            lastResult: totalResults > pageSize ? pageSize : totalResults,
            totalResults: totalResults,
            currentPage: 1,
            numPages: Math.ceil(totalResults / pageSize),
            currentType: currentType,
        });

        var loadPageAction = component.get('c.loadPage');
        loadPageAction.setParams({
            component: component,
            helper: helper,
        });
        $A.enqueueAction(loadPageAction);
    },

    setPage: function(component, event, helper) {
        var newPage = parseInt(event.currentTarget.dataset.page, 10);
        var pageInfo = component.get('v.pageInfo');
        var pageSize = component.get('v.pageSize');

        pageInfo.currentPage = newPage;
        pageInfo.firstResult = (newPage - 1) * pageSize + 1;
        pageInfo.lastResult = pageInfo.totalResults > pageSize ? pageInfo.firstResult + pageSize - 1 : pageInfo.totalResults;

        component.set('v.pageInfo', pageInfo);

        var loadPageAction = component.get('c.loadPage');
        loadPageAction.setParams({
            component: component,
            helper: helper,
        });
        $A.enqueueAction(loadPageAction);
    },

    navigateToList: function(component, event, helper) {
        var tag = event.currentTarget.dataset.tag;
        var targetUrl = '/article-list?tags=' + encodeURIComponent(tag);

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ url: targetUrl });
        urlEvent.fire();
    },

    navigateToDetail: function(component, event, helper) {
        var articleId = event.currentTarget.dataset.article;
        var targetUrl = '/article?id=' + encodeURIComponent(articleId);

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ url: targetUrl });
        urlEvent.fire();
    }
})