({  
    loadPubDetails : function(component, event, helper) {
       var recordIdIs;
        var urlString = window.location.href;
        if (urlString.indexOf("contract-details") > -1) {
            var url = window.location;
            
            if (url.search) {
                recordIdIs = url.search.split("=")[1];
            } else {
                recordIdIs = null;
            }
        }
        if (recordIdIs != null) {
            component.set("v.recordId", recordIdIs);
      		helper.getContractDetails(component, event, helper, recordIdIs);
        }
    }
})