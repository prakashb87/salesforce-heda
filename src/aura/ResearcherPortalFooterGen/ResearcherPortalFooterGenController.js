/**
 * Created by rhyanmitchell on 2019-04-18.
 */
({
    doInit: function(component, event, helper) {
        var contentAttrs = component.get('v.contentAttrs');

        // Set custom background colour
        var customColour = contentAttrs.customColour;
        var poundHexRegEx = new RegExp('(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)');
        var hexRegEx = new RegExp('(^[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)');

        if (poundHexRegEx.test(customColour)) {
            customColour = customColour;
        } else if (hexRegEx.test(customColour)) {
            customColour = '#' + customColour;
        } else {
            customColour = '';
        }
        component.set('v.customColour', customColour);

        // Set render status
        var render = contentAttrs.render === 'true';
        var dashboardExclusive = contentAttrs.dashboardExclusive === 'true';
        if (dashboardExclusive && render) {
            render = false;
            var dashboardPathnames = ['/Researcherportal/s/','/Researcherportal/s/Dashboard','/Researcherportal/s/Home'];
            var i = dashboardPathnames.length;
            while (i--) {
                if (window.location.pathname === dashboardPathnames[i]) {
                    render = true;
                    break;
                } else {
                    render = false;
                }
            }
        }
        component.set("v.render", render);
    }
})