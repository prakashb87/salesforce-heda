({
	// change arrow up when disciple area dropdown is shown and arrow down when dropdown is closed
    fetchUserDetails:function(component, event, helper){
      var fetchUserAction = component.get("c.fetchUser");
        
        fetchUserAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log(storeResponse);
                // set current user information on userInfo attribute
                component.set("v.userInfo", storeResponse);
                component.set("v.userImage", storeResponse.SmallPhotoUrl)
                
            }
        }); 
         $A.enqueueAction(fetchUserAction);
    },
    toggleforArrowState: function(dropdownId, arrowId) {
         if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },
    checkSandbox : function(component, event) {
        var checkSandboxAction = component.get("c.checkIsSandbox");
        
        checkSandboxAction.setCallback(this, function(response) {
            if(response.getState() === "SUCCESS") {
                var isSandbox = response.getReturnValue();
                console.log('@@@@@isSandbox ::'+isSandbox);
                component.set("v.isSandbox",isSandbox);
            }
        });
        
        $A.enqueueAction(checkSandboxAction);
    },
    getPhotoUrl:function(component, event, helper, photoBlob, imageType){
        var imgaction = null
        imgaction = component.get("c.updateUserProfilePhoto");
        var oldUserPic = component.get("v.userImage");
        imgaction.setParams({
            'usrPhoto': photoBlob,
            'imageType':imageType
        });
        imgaction.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.isLoading",false);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                var imgUrl = storeResponse
                console.log(imgUrl);
                component.set("v.userImage",'');
                //helper.fetchUserDetails(component, event, helper)
                window.location.reload()
                setTimeout(function(){
                    component.set("v.userImage",imgUrl);
                },20);
                $('.profile-pic').attr("src",imgUrl)
            } else {
                console.log("Image upload failed");
                component.set("v.userImage",oldUserPic);
            }
        });
        setTimeout(function(){
                    component.set("v.userImage",'');
                },20);
        $A.enqueueAction(imgaction);
        
    }
})