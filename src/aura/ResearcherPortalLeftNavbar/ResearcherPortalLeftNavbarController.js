({
    //to navigate to respective on left nav item selection
    goToNewPage : function(component, event, helper) {
        
        //Start :: Added by Subhajit :: 245/233 :: 11/14/2018
        var candidaturecentreNONPROD_URL = $A.get("$Label.c.Candidature_Centre_URL");
        var candidaturecentrePROD_URL = $A.get("$Label.c.Candidature_Centre_PROD_URL");
        var hdrsupervisionNONPROD_URL = $A.get("$Label.c.hdrsupervision_URL");
        var hdrsupervisionPROD_URL = $A.get("$Label.c.hdrsupervision_PROD_URL");
        //Start :: Added by Subhajit :: 245/233 :: 11/14/2018
        
        var urlEvent = $A.get("e.force:navigateToURL");
        
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        
        var targetUrl;
        console.log('@@@@@id_str ===>'+id_str);
        switch(id_str){
            case "myprojects":
                targetUrl = "/view-all-project?tab=";
                break;
            case "ethics":
                targetUrl = "/view-my-ethics?tab=";
                break;
            case "funding":
                targetUrl = "/view-funding-details?tab=";
                break;
            case "mymilestones":
                targetUrl = "/view-my-milestones?tab=";
                break;
            case "publications":
                targetUrl = "/view-my-publications?tab=";
                break;
            case "contracts":
                targetUrl = "/view-my-contracts?tab=";
                break;
                //Start :: Modified by Subhajit :: 245/233 :: 11/14/2018    
            case "candidature":
                //console.log('@@@@@isSandbox'+component.get("v.isSandbox") +'==>'+candidaturecentreNONPROD_URL);
                if(component.get("v.isSandbox"))
                    targetUrl = candidaturecentreNONPROD_URL;
                else
                    targetUrl = candidaturecentrePROD_URL;
                break;
                
            case "hdrsupervision":
                //console.log('@@@@@isSandbox'+component.get("v.isSandbox") +'==>'+hdrsupervisionNONPROD_URL);
                if(component.get("v.isSandbox"))
                    targetUrl = hdrsupervisionNONPROD_URL;
                else
                    targetUrl = hdrsupervisionPROD_URL;
                break;
                //End :: Modified  by Subhajit :: 245/233 :: 11/14/2018
            default:
                targetUrl = "/";
                id_str = "";
        }
        
        //Start :: Modified by Subhajit :: 245/233 :: 11/14/2018  
        if(id_str=='candidature' || id_str=='hdrsupervision')
        {
            console.log('@@@@@targetUrl ===>'+targetUrl);
            window.open(targetUrl, '_blank');
        }
        else
        {
            targetUrl = targetUrl + id_str;
            
            urlEvent.setParams({
                "url" : targetUrl
            });
            
            urlEvent.fire();  
        }
        //End :: Modified by Subhajit :: 245/233 :: 11/14/2018  
    },
	
    //on launch, make default myprojects tab active and apply the border and selection color
	onInit : function(component, event, helper) {
        helper.getcheckInternalDualAccessUser(component, event, helper);
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName , i , tabType;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            if (sParameterName[0] === 'tab') { //lets say you are looking for param name - firstName
                tabType = (sParameterName[1] === undefined) ? 'Not found' : sParameterName[1];
            }
        }
        
        var leftNavlinkId = undefined;
        
        switch (tabType){
        	case "myprojects":
        		leftNavlinkId = "myprojectsLink";
        		break;
        	case "mymilestones":
        		leftNavlinkId = "mymilestonesLink";
        		break;
        	case "funding":
        		leftNavlinkId = "fundingLink";
        		break;
        	case "ethics":
        		leftNavlinkId = "ethicsLink";
        		break;
        	case "publications":
        		leftNavlinkId = "publicationsLink";
        		break;
        	case "hdrsupervision":
        		leftNavlinkId = "hdrsupervisionLink";
        		break;
            case "collaboration":
        		leftNavlinkId = "collaborationLink";
        		break;
        	case "contracts":
        		leftNavlinkId = "contractsLink";
        		break;
        	case "candidature":
        		leftNavlinkId = "candidatureCentreLink";
        		break;
        }
                
        
        if(leftNavlinkId != undefined){
        	var cmpTarget = component.find(leftNavlinkId);
        	$A.util.addClass(cmpTarget, "tabActive");
        }
        
        var getMilestonesCountAction = component.get("c.getLeftNavWrapper");
        
        getMilestonesCountAction.setCallback(this, function(response) {
        	if(response.getState() === "SUCCESS") {
        		var count = response.getReturnValue().milestoneCount;
        		component.set("v.milestonesNotification",parseInt(count));
        	}
        });
        
        $A.enqueueAction(getMilestonesCountAction);
        
        //Start :: Added by Subhajit :: 245/233 :: 11/14/2018
        helper.checkSandbox(component, event);
        //End :: Added by Subhajit :: 245/233 :: 11/14/2018
        
    }
})