({
    doInit: function(component, event, helper) {
        var tagPath = component.get('v.tagPath');
        var keyword = tagPath.substring(tagPath.lastIndexOf('/') + 1);
        component.set('v.keyword', keyword);
    },

    goToFaqsPage: function(component, event, helper) {
        var tagPath = component.get('v.tagPath');

        var urlEvent = $A.get("e.force:navigateToURL");
        if (tagPath) {
            urlEvent.setParams({url: '/faqs?tags=' + encodeURIComponent(tagPath)});
        } else {
            urlEvent.setParams({url: '/faqs'});
        }
        urlEvent.fire();
    },

    goToArticlePage: function(component, event, helper) {
        var tagPath = component.get('v.tagPath');

        var urlEvent = $A.get("e.force:navigateToURL");
        if (tagPath) {
            urlEvent.setParams({url: '/article-list?tags=' + encodeURIComponent(tagPath)});
        } else {
            urlEvent.setParams({url: '/article-list'});
        }
        urlEvent.fire();
    }
})