/*******************************************************************************
 * Purpose: This clone of menubar component is to implement story RPORW-97
 * Created by: Bowen
 * on 26/10/2018
 ******************************************************************************/
({
    //to navigate to respective tab based on item selection
	goToNewPage : function(component, event, helper) {

		var urlEvent = $A.get("e.force:navigateToURL");

		var ctarget = event.currentTarget;
		var id_str = ctarget.dataset.value;
        
		urlEvent.setParams({
			"url" : "/"
		});

		urlEvent.fire();
	},

    goToLifecyclePage : function(component, event, helper) {

        var urlEvent = $A.get("e.force:navigateToURL");

        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;

        urlEvent.setParams({
            "url" : "/" + id_str
        });

        urlEvent.fire();
    },
	
	onRender : function(component, event, helper) {

	},
	
    //Based on user selection apply active style, by default dashboard will be selected
	doInit : function(component, event) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName, i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            if (sParameterName[0] === 'tab') { //lets say you are looking for param name - firstName
                sParameterName[1] === undefined ? 'Not found' : sParameterName[1];
            }
        }
        
        var siteUrl = window.location.pathname.split("/s/")[1];
        
        var linkId = undefined;
        
        switch (siteUrl){
        	case "dashboard":
        		linkId = "dashboardLink";
        		break;
        	case "fund":
        		linkId = "fundingopportunitiesLink";
        		break;
        	case "idea":
        		linkId = "translationLink";
        		break;
        	case "plan":
        		linkId = "toolsLink";
        		break;
        	case "conduct":
        		linkId = "engagementLink";
        		break;
            case "needHelp":
        		linkId = "needHelpLink";
        		break;
        	case "communicate":
        		linkId = "communicateLink";
        		break;
        	case "impact":
        		linkId = "impactLink";
        		break;
        }
        
        if(window.location.pathname === "/Researcherportal/s/")
        	linkId = "dashboardLink";
        if(linkId != undefined){
        	var cmpTarget = component.find(linkId);
        
        	$A.util.addClass(cmpTarget, "tabActive");
        } 
    }

})