({
    // On Load of details page, fetch the selected project details and store it in object to display related data in UI.
    initLoad: function(component, event, helper) {

        var recordId;
        var urlString = window.location.href;
        if (urlString.indexOf("project-details") > -1) {
            var url = window.location;

            if (url.search) {
                recordId = url.search.split("=")[1];
            } else {
                recordId = null;
            }
        }
        component.set("v.recordId", recordId);
    },
    handleApplicationEvent: function(cmp, event) {
        var message = event.getParam("recEvent");
        alert(message);
        // set the handler attributes based on event data
        cmp.set("v.messageFromEvent", message);
        // var numEventsHandled = parseInt(cmp.get("v.numEvents")) + 1;
        // cmp.set("v.numEvents", numEventsHandled);
    },
    goBackBtn: function(cmp, event, helper) {
        if(window.localStorage){
            let isPublicProject = cmp.get("v.isPublicProject")
            if(isPublicProject){
                window.localStorage.setItem('PREV_URL', 'PROJECT_DETAIL')
            }
        }
        window.history.back(-1)
    },
    /* Create Associated ethics table and add Ethics table with relevant fields in the page.
       Load related Ethics if there are ethics already present for the selected project.
    */
    enableDataTable: function(component, event, helper) { 
        helper.getcheckInternalDualAccessUser(component, event, helper); 
        if(window.localStorage){
            let prevUrl = window.localStorage.getItem('PREV_URL')
            if(prevUrl == 'PUBLIC_VIEW'){
                component.set("v.isPublicProject",true)
                window.localStorage.removeItem('PREV_URL')
            }
            if(prevUrl != 'PUBLIC_VIEW'){
                window.localStorage.removeItem('SEARCH_CRITERIA')
                window.localStorage.removeItem('PREV_URL')
            }
        }
           $("#addEthic").on('shown.bs.modal', function(){
                $(this).find('input[type="search"]').focus();
            });
    
    	var tableAddEthic = $('#addEthicsList').DataTable({
             "data": [],
             "language": {
                 "emptyTable": "No ethics application has been selected."
             },
             "dom": '<"top"i>rt<"bottom"><"clear">',
             "columns": [{
                 title: $A.get("$Label.c.ApplicationTitle")
             }, {
                 title: $A.get("$Label.c.Status")
             }, {
                 title: $A.get("$Label.c.ApprovedDate")
             }, {
                 title: $A.get("$Label.c.ExpiryDate")
             }, {
                 title: ""
             }],
             "info": false,
             "searching": false,
             "ordering": false,
             "dom": 'frt',
             "columnDefs": [{
                 targets: 0,
                 className: "table-addEthic-column",
                 'render': function(data, type, full, meta) {
                     return "<div class='truncate' data-container='body' data-toggle='tooltip' data-placement='top' title='" + data + "'>" + data + "</div>";
                 }
             }, {
                 targets: 1,
                 className: "table-addEthic-status",
             }, {
                 targets: 4,
                 'render': function(data, type, full, meta) {
                     return "<a id='removeItem' class='removeItemLink'>Remove</a>";
                 }
             }, {
                 targets: [2, 3],
                 "render": function(data, type, full, meta, row) {
                     if (type === 'display' || type === 'filter') {
                         if (data !== '')
                             return moment(data).format('DD/MM/YYYY');
                         else
                             return '';
                     }
                     
                     return data;
                 }
             }]
             
         });

        $('#addEthicsList tbody').on('click', 'td', function() {
            if ($(this).children('a').attr('id') == 'removeItem') {
                var index = tableAddEthic.cell(this).index().row;
                var selectedEthics = component.get("v.selectedEthics");
                var serResult = component.get("v.server_result");
                serResult.push(selectedEthics[index]);
                serResult = serResult.sort(function(x,y){ 
                        var a = String(x.ethicsTitle).toUpperCase(); 
                        var b = String(y.ethicsTitle).toUpperCase(); 
                        if (a > b){
                            return 1 
                        }                             
                        if (a < b) {
                            return -1 
                                   }
                                
                       return 0; 
                    }); 
                component.set("v.server_result", serResult);
                selectedEthics.splice(index, 1);
                tableAddEthic.row(index).remove().draw();
                component.set("v.selectedEthics", selectedEthics); 
                $('#searchInput').focus();
            }
        });

        var recordId = component.get("v.recordId");

        var test = component.get("c.getProjectDetails");
        test.setParams({
            recordId: recordId
        });

        //Setting the Callback
        test.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
                var forCodes = result.forCodes ? result.forCodes.split(";") : [];
                var impactCategories = result.impactCategories ? result.impactCategories.split(";") : [];
                var keywords = result.keywords ? result.keywords.split(";") : [];
                component.set("v.pjtKeywords", keywords);
                component.set("v.FoRCodes", forCodes);
                component.set("v.ImpactCategory", impactCategories);
                component.set("v.projectRecord", result);
                    var isRMPublic = (result.accesss == $A.get("$Label.c.RMITPublic")) ? true : false;
                    if(!result.IsRMProject){
                        $('#linkedContract').css("display", "none");
                        $('#linkedMilestones').css("display", "none");                        
                    }      
                    if(!isRMPublic){
                        $('#chatterBox').css("display", "none");
                    }
                    if(isRMPublic && !result.isProjectMember){
                       $('#linkedContract').css("display", "none");
                       $('#associateEthics').css("display", "none");
                       $('#expectedimpact').css("display", "none");
                       $('#impactDetailsTitle').css("display", "none");
                       $('#linkedMilestones').css("display", "none"); 
                    }
                }              

            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(test);
        
         var getEthicsAction = component.get("c.getProjectEthicsWrapper");

                getEthicsAction.setParams({
                    projectId: recordId
                });
                getEthicsAction.setCallback(this, function(a) {
                    var dataSet = [];
                    var state = a.getState();
                    console.log(state)
                    if (state == "SUCCESS") {
                        var dataSet = a.getReturnValue();
                        console.log(dataSet)
                        if (!$A.util.isUndefined(dataSet)) {
                            var pendArr = dataSet.filter(function(el) {
                                if (el.status === "Pending") {
                                    return el
                                }
                            });
                            var nonPendArr = dataSet.filter(function(el) {
                                if (el.status !== "Pending") {
                                    return el
                                }
                            });
                            dataSet = pendArr.concat(nonPendArr);

                            component.set("v.ethics", dataSet);
                            var table = $('#ethicList').DataTable({
                                "data": [],
                                "language": {
                                    "emptyTable": $A.get("$Label.c.Noethicsapplicationshavebeenassociatedwiththisproject")
                                },
                                "pageLength": 4,
                                "dom": '<"top"i>rt<"bottom"><"clear">',
                                "columns": [{
                                    title: $A.get("$Label.c.ApplicationTitle")
                                }, {
                                    title: $A.get("$Label.c.Status")
                                }, {
                                    title: $A.get("$Label.c.ApprovedDate")
                                }, {
                                    title: $A.get("$Label.c.ExpiryDate")
                                }, {
                                    title: ""
                                }],
                                "info": false,
                                "order": [],
                                "searching": false,
                                "dom": 'frt',
                                "drawCallback": function() {
                                    // Show or hide "Load more" button based on whether there is more data available
                                    $('#btn-load-more').toggle(this.api().page.hasMore());
                                },
                                "columnDefs": [{
                                    targets: 0,
                                    className: "table-title-column",
                                    'render': function(data, type, full, meta) {
                                        return "<div class='truncate' data-container='body' data-toggle='tooltip' data-placement='top' title='" + data + "'>" + data + "</div>";
                                    }
                                }, {
                                    targets: 1,
                                    className: "table-ethic-column",
                                }, {
                                    targets: 4,
                                    orderable: false,
                                    'render': function(data, type, full, meta) {
                                        if (full[4] === 'true' && component.get("v.isInternalDualAccessUser") == false) {
                                            return "<i id='deleteBtn' class='fa fa-trash-o deleteIcon' aria-hidden='true' data-toggle='modal' data-target='#deleteConfirmation'></i>";
                                        } else {
                                            return "";
                                        }
                                        //                                return "<i id='deleteBtn' class='fa fa-trash-o deleteIcon' aria-hidden='true' data-toggle='modal' data-target='#deleteConfirmation'></i>";
                                    }
                                }, {
                                    targets: [2, 3],
                                    "render": function(data, type, full, meta, row) {
                                        if (type === 'display' || type === 'filter') {
                                            if (data !== '')
                                                return moment(data).format('DD/MM/YYYY');
                                            else
                                                return '';
                                        }

                                        return data;
                                    }
                                }]

                            });
                            

                            $('#btn-load-more').on('click', function() {
                                table.page.loadMore();
                            });

                            $('#ethicList tbody').on('click', 'td', function() {
                                if ($(this).children('i').attr('id') == 'deleteBtn') {
                                    $('#deleteConfirmation .modal-body #passedValue').val(table.cell(this).index().row);
                                }
                            });
                            helper.addRows(component, "#ethicList", dataSet, true);
                        }
                    } else {
                        $('#no-ethics-alert').show();
                    }
                });

                //adds the server-side action to the queue        
                $A.enqueueAction(getEthicsAction);
         helper.getLinkedContacts(component, event, helper,recordId);  
         helper.getLinkedMilestones(component, event, helper,recordId);  		        
    },
    // Close the add ethics modal and clear the selected ethics.
    closeModal: function(component, event, helper) {
        helper.clearValues(component);

        var table = $('#addEthicsList').DataTable();
        table.clear().draw();

    },
    // close the modal popup
    cancelDelete: function(component) {
        $('#deleteConfirmation').modal('hide');
    },
    //delete selected ethics and send the data to backend and refresh the datatable data.
    deleteMember: function(component, event) {

       // $('#deleteConfirmation').modal('hide');

        var recordId = component.get("v.recordId");

        var index = $('#passedValue').val();

        var ethics = component.get("v.ethics");

        var selectedEthic = ethics[index];

        //Function to delete the member in SF needs to be implemented
        var deleteEthicAction = component.get("c.deleteEthicsProject");
        deleteEthicAction.setParams({
            ethicsId: selectedEthic["ethicsId"],
            projectId: recordId
        });

        //Setting the Callback
        deleteEthicAction.setCallback(this, function(a) {

            //get the response state
            var state = a.getState();

            //check if result is successfull
            if (state == "SUCCESS") {
                ethics.splice(index, 1);
                component.set("v.ethics", ethics);

                var table = $('#ethicList').DataTable();
                table.row(index).remove().draw();

            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(deleteEthicAction);
    },

    // When user enter more than 2 characters on searchbox input, fetch backend data and  display the list
    searchQuery: function(component, event, helper) {
        //var inputKeyword = component.get("v.searchKeyword");
        //$('#addEthic').modal('show');
        var inputKeyword = $("#searchInput").val();
         $('.add-ethic-search').toggle(true);
        if (inputKeyword.length > 2) {
            helper.searchEthics(component, event, helper, inputKeyword,false);          

        } 
        else if(inputKeyword.length == 0){
             helper.searchEthics(component, event, helper, inputKeyword,true);             
        }
        else {
            component.set("v.server_result", []);
        }
        
         $(document).click(component, function(e) {
            if ($(e.target).closest('.add-ethic-search').length === 0 && $('.add-ethic-search').is(":visible") && $("#searchInput").is(':focus') === false) {
                $('.add-ethic-search').toggle(false);
            }    
            
        });

    },
    // On ethics list selection, add the selected one to the associated ethics list table   
    selectEthic: function(component, event, helper) {
        var targetCmp = event.currentTarget;

        var index = targetCmp.dataset.indexvar;

        var selectedEthics = component.get("v.selectedEthics");

        var projectEthics = component.get("v.ethics");

        var server_result = component.get("v.server_result");

        var item = server_result[index];

        var indexSe = selectedEthics.findIndex(function(member) {
            return member.ethicsId === item.ethicsId;
        });

        var indexPro = projectEthics.findIndex(function(member) {
            return member.ethicsId === item.ethicsId;
        });
        
      //  var selEthic = selectedEthics.

        if (indexSe == -1 && indexPro == -1) {
            selectedEthics.push(item);

            component.set("v.selectedEthics", selectedEthics);

            helper.addRows(component, "#addEthicsList", item, true);
        }

        component.set("v.server_result", []);
        //component.set("v.searchKeyword", '');
         var inputKeyword =   $("#searchInput").val("");
         helper.searchEthics(component, event, helper, "" ,true);          
        
    },

    // Remove the selected ethics and refresh the table data.
    removeSeletedEthic: function(component) {
        var targetCmp = event.currentTarget;
        var index = targetCmp.dataset.index;

        var selectedEthics = component.get("v.selectedEthics");

        if (index != undefined) {
            selectedEthics.splice(index, 1);

            var table = $('#addEthicsList').DataTable();
            table.row(index).remove().draw();
        }

        component.set("v.selectedEthics", selectedEthics);
    },
    // save the selected Ethics to backend for the project and display the same in the associated ethics list in UI. 
    saveEthics: function(component, event, helper) {
        var recordId, idList = [];

        recordId = component.get("v.recordId");

        var selectedEthics = component.get("v.selectedEthics");

        selectedEthics.forEach(function(ethic) {
            idList.push(ethic["ethicsId"]);
        });

        var saveEthicsAction = component.get("c.addEthicsProjectRecords");
        saveEthicsAction.setParams({
            ehicsIdList: idList,
            projectId: recordId
        });

        //Setting the Callback
        saveEthicsAction.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
               // $('#addEthic').modal('hide');
                component.set("v.selectedEthics", []);
                helper.redrawTable(component, "#ethicList", result);

                var table = $('#addEthicsList').DataTable();
                table.clear().draw();

            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(saveEthicsAction);
    }
})