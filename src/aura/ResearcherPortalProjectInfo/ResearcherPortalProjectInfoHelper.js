({
    //Methd call to clear the values on close of modal popup
    clearValues: function(component) {
        //component.set("v.searchKeyword", "");
        $('#searchInputMember').val("");
        component.set("v.selectedMembers", []);
        component.set("v.server_result", []);
    },
    
    // To get the projectId based on project selection from the URL
    getRecordID: function(component) {
        var recordId;
        var urlString = window.location.href;
        if (urlString.indexOf("project-details") > -1) {
            var url = window.location;
            
            if (url.search) {
                recordId = url.search.split("=")[1];
            } else {
                recordId = null;
            }
            
            return recordId;
        }
        
        return null;
    },
    
    /* Method call to get the project details which includes project members.
    Date should be formatted to dd/mm/yyyy
    Based on the project access, respective message needs to be shown on hover
    Check whether external contact exists or not.
    */
    getProjectInfo : function(component, event, helper) {
        var recordId = this.getRecordID(component);
        
        var getProjectDetailAction = component.get("c.getProjectDetails");
        getProjectDetailAction.setParams({
            recordId: recordId
        });
        //Setting the Callback
        getProjectDetailAction.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
                    var pjtEndDate = result.projectEndDate != undefined ? this.formatDate(result.projectEndDate) : '';
       			    var pjtStartDate = result.projectStartDate != undefined ? this.formatDate(result.projectStartDate): '';    
                    result.agreementStartDate = result.agreementStartDate != undefined ? this.formatDate(result.agreementStartDate) : '';
                    var accessMap = {
                        Private: $A.get("$Label.c.PrivateAccess"),
                        Members: $A.get("$Label.c.MembersAccess"),
                        RMITPublic: $A.get("$Label.c.RMITPublicAccess")
                    }
                    
                    var msg = "";
                    if (result.accesss != undefined){
                        msg = accessMap[result.accesss.replace(/ /g, '')];
                    }
                    
                    result.accessMsg = 'This project will be visible to the following groups: \n' + msg;
                    var isext = result.projectMembers.filter(function(data) {
                        if (data.IsExternalContact === true) {
                            return data
                        }
                    });
                    result.projectMembers =  this.sortProjectMembers(component,event,helper,result.projectMembers);  
                    component.set("v.IsExternalContactExists", isext);
                    component.set("v.projectRecord", result);   
                    component.set("v.pjtStartDate", pjtStartDate);
     			    component.set("v.pjtEndDate", pjtEndDate);
                    component.set("v.groupStatus", result.groupStatus);
                    component.set("v.projectMembers", result.projectMembers);
                }
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(getProjectDetailAction);        
    },
    
    formatDate: function(dateString) {
        
        var date = new Date(dateString);
        
        var day = date.getDate();
        var month = date.getMonth()+1;
        var year = date.getFullYear();
        if(day<10){
            day = '0'+day;
        }
        
        if(month<10){
            month = '0'+month;
        }
        
        return day+'/'+month+'/'+year;
    },
    
    updateProjectDetails :function(component){
        var projectRecord = component.get("v.projectRecord");
        var prjStartdate = component.find("startDate").get("v.value");
        var prjEnddate   = component.find("enddate").get("v.value");     
        var projectId = component.get("v.projectRecord").projId;
        var isStartDateError = component.get("v.startdateValidationError");
        var isEndDateError = component.get("v.enddateValidationError");
        var isEmptyStartdateValidationError = component.get("v.emptyStartdateValidationError"); 
        
        if (isEmptyStartdateValidationError ==true || isEndDateError == true || isStartDateError ==true) {
            event.preventDefault();
        } 
        else {
            var projectData = new Object();
            projectData.projectStartDate = prjStartdate;
            projectData.projectEndDate = prjEnddate;
            projectData.groupStatus = $('#statusdropdown').val();
            var updateProjectDetailAction = component.get("c.updatestatusvalueList");
            updateProjectDetailAction.setParams({
                "onSubmitJSON" : JSON.stringify(projectData),
                projectId : projectId
            });
            updateProjectDetailAction.setCallback(this,function(response) {            
                var state = response.getState();
                if(state == "SUCCESS"){
                    component.set("v.modalErrorBox", false)
                    var result = response.getReturnValue();
                    this.updatepjtDates(component,result);
                    component.find("editKeyDetailsPopup").hideModalBox('editKeyDetailsPopup');
            } else if(state == "ERROR"){
                    component.set("v.modalErrorBox", true)
            }
            
        });
        $A.enqueueAction(updateProjectDetailAction);   
    }
    },
    
    checkMemberfromResponse :function(component,member, temp, helper){
        var selectedMembers = component.get("v.selectedMembers");
        var projectMembers = component.get("v.projectMembers");
        var pjtType = component.get("v.projectRecord").IsRMProject;
        if(pjtType == true){
            var isSelectedMember = selectedMembers.findIndex(function(item){
                return item.contactId === member.contactId
            });
            
            if(isSelectedMember ==-1)
            {
                var isProjectMember = projectMembers.findIndex(function(item){
                    return (item.contactId === member.contactId && isSelectedMember == -1)
                    
                });
                
                // project member not there with this contact id add 
                if(isProjectMember == -1)
                {
                    temp.push(member);                
                }
                else
                {
                    if(helper.checkforCIDelegate(component, member) == true){
                        temp.push(member)
                    }  
                }
            }           
        }
        else{
            if (selectedMembers.findIndex(function(item) {
                return item.contactId === member.contactId;
            }) == -1 && projectMembers.findIndex(function(item) {
                return item.contactId === member.contactId;
            }) == -1) {
                temp.push(member);
            }
        }
    },
    
    checkforCIDelegate :function(component, member){        
        var projectMembers = component.get("v.projectMembers");
        var status =false;
        var ciIndex= projectMembers.findIndex(function(item){
            return (item.contactId === member.contactId && item.memberRole == 'CI')            
        });
        
        // 1. project member exists with CI no need to add
        if(ciIndex !=-1){
            return false;
        }
        
        // 2.project member exists with CI and researcher no need to add
        var memArr = [];
        projectMembers.forEach(function(item) {
            if(member.contactId === item.contactId){
                memArr.push(item)
            }
        })
        if(memArr.length >1){
            return false;
        }
        
        if(memArr[0].memberRole ==$A.get("$Label.c.Researcher") && memArr[0].IsRMMember == true){
            return true;
        }
        else {
            return false;
        }
    },
    updatepjtDates :function(component,result){       
        var projectRecord = component.get("v.projectRecord");
        var newProjectRecord = Object.assign(projectRecord, result)
        component.set("v.projectRecord", newProjectRecord);
        var pjtEndDate = result.projectEndDate != undefined ? this.formatDate(result.projectEndDate) : '';
        var pjtStartDate = result.projectStartDate != undefined ? this.formatDate(result.projectStartDate): '';       
        setTimeout(function(){
        	component.set("v.pjtStartDate", pjtStartDate);
       	 	component.set("v.pjtEndDate", pjtEndDate);
        	component.set("v.groupStatus", result.groupStatus);
        },10);
    },
    sortProjectMembers : function(component, event, helper, data){
  	var sortedProjectMembers = data;
        var finalarr = [], CIDel = [], res = [], PCI =[], CI = [];
        Object.keys(sortedProjectMembers).forEach(key => {
            var member = sortedProjectMembers[key];
            if(member.memberRole =="CI" && member.IsRMMember ==true){
                PCI.push(member);
            }
            else if(member.RMPosition =="CI" && member.IsRMMember ==false){
                CI.push(member);
            }
            else if(member.RMPosition =="CI Delegate" && member.IsRMMember ==false){
                CIDel.push(member);
            }
            else{
                res.push(member);
      	  	}       
    });
    finalarr = finalarr.concat(PCI,CI,CIDel,res);
    return finalarr;
},
 getcheckInternalDualAccessUser : function(component, event, helper){
    var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
    
    getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
        var state = a.getState();
        if (state == "SUCCESS") {
            var result = a.getReturnValue();
            if (!$A.util.isUndefined(result)) {  
                component.set('v.isInternalDualAccessUser', result);
            }
        }        
    });
    $A.enqueueAction(getcheckInternalDualAccessUserAction);
}
})
      
