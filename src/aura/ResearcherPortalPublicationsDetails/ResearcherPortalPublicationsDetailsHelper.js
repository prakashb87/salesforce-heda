({
    strCheck : function(component, event, helper, str, keywordsarray, splCharacter){
        var tempcount=0;     
        for(var i =0; i<=splCharacter.length; i++){
            if(str.includes(splCharacter[i])){
                tempcount++;
                var count = str.split(splCharacter[i]);
                for(var j =0;j< count.length; j++)
                {
                    this.strCheck(component, event, helper, count[j], keywordsarray,splCharacter) 
                }
                break
            }            
        }
        if(tempcount==0)
        {
            keywordsarray.push(str.trim());
            component.set("v.keywordsArray", keywordsarray);
        }          
    },    
    //Method call to fetch publication details
    getPublicationsDetails : function(component, event, helper, recordIdIs) {       
        var action = component.get("c.getPublicationDetails");
        action.setParams({
            publicationId: recordIdIs,
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result)
                component.set("v.publicationDetails", result);
                //var splCharacter = $A.get("$Label.c.keywordSpecialCharacter");
                // splCharacter = splCharacter.split("$");
                // var keywordsarray = [];
                // this.strCheck(component, event, helper, result.keywords, keywordsarray, splCharacter);
                var forCodes = result.forcodes ? result.forcodes.split(";") : [];
                component.set("v.FoRCodes", forCodes);
                var outletDetails = [
                    {
                        key:$A.get("$Label.c.Category"),
                        value:result.category != undefined ? result.category:'-'
                    },
                    {
                        key:$A.get("$Label.c.subcategory"),
                        value:result.subCategory != undefined ? result.subCategory:'-'
                    },
                    {
                        key:$A.get("$Label.c.Outlet"),
                        value:result.outlet != undefined ? result.outlet:'-'
                    },
                    {
                        key:$A.get("$Label.c.issn"),
                        value:result.iSSN != undefined ? result.iSSN:'-'
                    },
                    {
                        key:$A.get("$Label.c.publisher"),
                        value:result.publisher != undefined ? result.publisher:'-'
                    },
                    {
                        key:$A.get("$Label.c.placeofpublication"),
                        value:result.placeofPublication != undefined ? result.placeofPublication:'-'
                    },
                    {
                        key:$A.get("$Label.c.conference"),
                        value:result.conferenceName != undefined ? result.conferenceName:'-'
                    },
                    {
                        key:$A.get("$Label.c.conferencelocation"),
                        value:result.conferenceLocation != undefined ? result.conferenceLocation:'-'
                    },
                    {
                        key:$A.get("$Label.c.conferencedates"),
                        value:result.eventDate != undefined ? result.eventDate:'-'
                    },
                    {
                        key:$A.get("$Label.c.editors"),
                        value:result.editors != undefined ? result.editors:'-'
                    }                    
                ];                
                component.set("v.outletDetails", outletDetails);
                var furtherDetails = [
                    {
                        key:$A.get("$Label.c.edition"),
                        value:result.edition != undefined ? result.edition:'-'
                    },
                    {
                        key:$A.get("$Label.c.volume"),
                        value:result.volume != undefined ? result.volume:'-'
                    },
                    {
                        key:$A.get("$Label.c.issue"),
                        value:result.issueNumber != undefined ? result.issueNumber:'-'
                    },
                    {
                        key:$A.get("$Label.c.startpage"),
                        value:result.startPage != undefined ? result.startPage:'-'
                    },
                    {
                        key:$A.get("$Label.c.endpage"),
                        value:result.endPage != undefined ? result.endPage:'-'
                    },
                    {
                        key:$A.get("$Label.c.totalpages"),
                        value:result.totalPages != undefined ? result.totalPages:'-'
                    },
                    {
                        key:$A.get("$Label.c.medium"),
                        value:result.medium != undefined ? result.medium:'-'
                    },
                    {
                        key:$A.get("$Label.c.copyright"),
                        value:result.copyright != undefined ? result.copyright:'-'
                    }
                ];                
                component.set("v.furtherDetails", furtherDetails);
                var rankingDetails = [
                    {
                        key:$A.get("$Label.c.sjr"),
                        value:result.sjrQuartile != undefined ? result.sjrQuartile:'-'
                    },
                    {
                        key:$A.get("$Label.c.sjrpercentile"),
                        value:result.sjrPercentile != undefined ? result.sjrPercentile:'-'
                    }
                ];                
                component.set("v.rankingDetails", rankingDetails);
                
                var linksDetails = [
                     {
                        key:$A.get("$Label.c.electroniclocation"),
                        value:result.electronicLocation != undefined ? result.electronicLocation:'-'
                    },
                    {
                        key:$A.get("$Label.c.scopusID"),
                        value:result.scopusID != undefined ? result.scopusID:'-'
                    },
                    {
                        key:$A.get("$Label.c.webofScienceID"),
                        value:result.webofScienceID != undefined ? result.webofScienceID:'-'
                    },
                    {
                        key:$A.get("$Label.c.dateDepositedtoRepository"),
                        value:result.dateDepositedtoRepository != undefined ? this.formatDate(result.dateDepositedtoRepository):'-'
                    }                     
                ];                
                component.set("v.linksDetails", linksDetails);
            }
        });
        $A.enqueueAction(action);
    },
        formatDate: function(dateString) {
    
    	var date = new Date(dateString);
    	
    	var day = date.getDate();
    	var month = date.getMonth()+1;
    	var year = date.getFullYear();
    	if(day<10){
    		day = '0'+day;
    	}
    	
    	if(month<10){
    		month = '0'+month;
    	}
    	
    	return day+'/'+month+'/'+year;
    }
});