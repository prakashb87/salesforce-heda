({
    //Method call to fetch publication details
    getKeyPublicationsDetails : function(component, event, helper, recordIdIs) {
        var action = component.get("c.getPublicationDetails");
        action.setParams({
            publicationId: recordIdIs,
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.publicationKeyDetails", result);
                var status = result.status != undefined ? result.status:'-';
                var publicationEcode = result.publicationEcode != undefined ? result.publicationEcode:'-';
                var year = result.year != undefined ? result.year:'-';
                component.set("v.status", status); 
                component.set("v.publicationEcode", publicationEcode);
                component.set("v.year", year);
                if(result.rmsMembers.length >0){
                    var authorsList = result.rmsMembers;
                    authorsList = authorsList.sort(function(x,y){
                        var a = String(x.personnelType).toUpperCase(); 
                        var b = String(y.personnelType).toUpperCase(); 
                        if (a > b){
                            return 1 
                        }                             
                        if (a < b) {
                            return -1 
                        }
                        
                        return 0; 
                    });
                    component.set("v.rmsMembers", authorsList);
                }
            }
        });
        $A.enqueueAction(action);
    }
})