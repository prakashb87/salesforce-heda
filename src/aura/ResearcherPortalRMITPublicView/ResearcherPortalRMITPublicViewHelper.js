({
    //backend method call to get the list of public projects on inital load with tab selected and search text entered
    //resetting the array mapped to UI before calling.
    getRMITPublicProjectsList : function(component, event, helper, searchText, tabSelected) {
        component.set("v.dataSet", []);
        component.set("v.searchCompleted", true);
        var getLinkedContactsAction = component.get("c.getMyResearchProjectPublicListView");
        let searchBoxValue = document.getElementById('search-input')  ?  document.getElementById('search-input').value :''
        let searchBoxTrimVal = searchBoxValue == "" ? searchBoxValue : searchBoxValue.trim();
        getLinkedContactsAction.setParams({
            searchText :  searchText,
            tabSelected : tabSelected
        });
        
        getLinkedContactsAction.setCallback(this, function(a) {
            var state = a.getState();
            component.set("v.searchCompleted", false);
            if (state == "SUCCESS") {
                var result= a.getReturnValue();
                var dataSet = result.publicProjectList;                  
                console.log(result)
                if (!$A.util.isUndefined(dataSet)) {
                    this.fetchRMITPublicPrjts(component, event, helper, dataSet, result, searchBoxTrimVal);
                }
            }            
        });        
        $A.enqueueAction(getLinkedContactsAction);
    },
    getIndex:function(ID){
        var index
        if(ID && ID.split('-') && ID.split('-').length>0){
            index = ID.split('-')[0]
        }
        return index
    },
    //Method call on search button with validation ie., minimum 2 characters.
    searchProject:function(component, event, helper){
        component.set("v.dataSet", []);
        component.set("v.totalDataSet", []);
        component.set("v.pageCount",1);
        component.set("v.prevPageCount", 0) 
        $("#searchError").css("display","none");
        $("#search-input").css("border-color",  "");
        var selectedTabId = document.querySelectorAll('.active-tab')[0].id;
        var selectedTab = (selectedTabId =='filter-R') ? 'RM': ((selectedTabId == 'filter-U') ?'UC' : 'ALL');
        let searchBoxValue = document.getElementById('search-input').value;
        let searchBoxTrimVal = searchBoxValue == "" ? searchBoxValue : searchBoxValue.trim();
        // Remove/Comment 2nd if and uncomment 1st if for searchbox empty search with FOR codes
       //if((searchBoxTrimVal.length>2) ||(searchBoxTrimVal.length == 0 && component.get("v.selectedForCodes").length >0)){
        
        if(searchBoxTrimVal.length >=1 && searchBoxTrimVal.length <=2){
            $("#searchError").css("display","block");
            $("#search-input").css("border-color",  "red");
        } else{
            var obj = {
                searchText : searchBoxTrimVal,
                tabSelected : selectedTab,
                pageNo : component.get("v.pageCount"),
                forCodeDetailSearch : component.get("v.selectedForCodes"),
                impactCodeDetailSearch : component.get("v.selectedImpactCodes")
            }
            
            var jsonParams = JSON.stringify(obj);
            component.set("v.searchCriteria", jsonParams)
            this.getNextRMITPublicProjectsList(component,event,helper,jsonParams, searchBoxTrimVal);
        }
        
    },
    //method call on click on search, tabs to get the matching records
    getNextRMITPublicProjectsList : function(component, event, helper, obj, searchBoxTrimVal) {
        component.set("v.dataSet", []);
        component.set("v.searchCompleted", true);
        var getLinkedContactsAction = component.get("c.getRMITPublicResearchProjectsFiltered");
        
        getLinkedContactsAction.setParams({
            params  :obj
        });
        
        getLinkedContactsAction.setCallback(this, function(a) {
            var state = a.getState();
            component.set("v.searchCompleted", false);
            if (state == "SUCCESS") {
                var result= a.getReturnValue();
                var dataSet = result.publicProjectList;
                console.log(dataSet);
                if (!$A.util.isUndefined(dataSet)) {
                    this.fetchRMITPublicPrjts(component, event, helper, dataSet, result, searchBoxTrimVal);
                }
            }            
        });        
        $A.enqueueAction(getLinkedContactsAction);
    },
    //Data parsing based on the response and sorting primary contact as first followed by CI, CI Delegate, members.
    fetchRMITPublicPrjts:function(component, event, helper, dataSet, result, searchBoxTrimVal){
        dataSet.forEach(function(data){
            if(data.projectMembersAndRoles.length >0){
                var authorsList = data.projectMembersAndRoles;
                var finalarr = [], CIarr = [], CIDel = [], res = [], hdr = [], noaccess =[], PCI =[];
                authorsList.forEach(function(member) {
                    if(member.Primary_Contact_Flag__c){
                        finalarr.push(member);
                    }
                    else if(member.MemberRole__c =="CI" && member.RM_Member__c ==true){
                        PCI.push(member);
                    }
                        else if(member.MemberRole__c =="CI" && member.RM_Member__c ==false){
                            CIDel.push(member);
                        }
                            else if(member.MemberRole__c =="Researcher"){
                                res.push(member);
                            }
                                else if(member.MemberRole__c =="HDR"){
                                    hdr.push(member);
                                }
                                    else{
                                        noaccess.push(member);
                                    }                   
                    
                });
                if(PCI.length > 0){
                    finalarr = finalarr.concat(PCI);
                }
                if(CIDel.length>0){
                    finalarr = finalarr.concat(CIDel);
                }
                if(res.length>0){
                    res.forEach(function(member) {
                        if (CIDel.findIndex(function(item) {
                            return item.Contact__c === member.Contact__c;
                        }) == -1) {
                             finalarr = finalarr.concat(member);
                    }
                    });
                }
                if(hdr.length > 0){
                    finalarr = finalarr.concat(hdr)
                }
                if(noaccess.length > 0){
                    finalarr = finalarr.concat(noaccess)
                }
                data.projectMembersAndRoles = finalarr;                                             
                component.set("v.totalDataSet", dataSet);
                component.set("v.actualDataSet", dataSet);                         
            } else {
                component.set("v.totalDataSet", dataSet);
                component.set("v.actualDataSet", dataSet);
            }            
            component.set("v.projectsLength", dataSet.length);  
            component.set("v.totalProjectCount", result.totalPublicProjects);             
        });
         component.set("v.hasMore", result.hasMoreRecords);
        this.updateDataSet(component, event, helper, false);
    },
    //Method call to make selected tab active and call method to get respective data
    filterTab : function(component, event, helper, id, tab){
        component.set("v.dataSet", []);
        component.set("v.totalDataSet",[]);
        component.set("v.pageCount",1); 
        component.set("v.prevPageCount", 0);
        //$('.nav-item.active-tab').removeClass('active-tab');
        let activeElm = document.querySelectorAll('.nav-item.active-tab')
        if(activeElm && activeElm.length){
            activeElm[0].classList.remove('active-tab')
        }
        document.getElementById(id).classList.add('active-tab');
        let searchBoxValue = document.getElementById('search-input').value;
        let searchBoxTrimVal = searchBoxValue == "" ? searchBoxValue : searchBoxValue.trim();
        if(searchBoxTrimVal.length>2 || searchBoxTrimVal.length ==0){
            var obj = {
                searchText : searchBoxTrimVal,
                tabSelected : tab,
                pageNo : component.get("v.pageCount"),
                forCodeDetailSearch : component.get("v.selectedForCodes"),
                impactCodeDetailSearch : component.get("v.selectedImpactCodes")
            } 
            var jsonParams = JSON.stringify(obj);
            component.set("v.searchCriteria",jsonParams)
            this.getNextRMITPublicProjectsList(component,event,helper,jsonParams, searchBoxTrimVal);
        }
    },
    //method call on click on search, tabs, and load more to get the matching records
    getloadmoreRMITPublicProjectsList : function(component, event, helper, obj) {
        let searchBoxValue = document.getElementById('search-input').value;
        let searchBoxTrimVal = searchBoxValue == "" ? searchBoxValue : searchBoxValue.trim();
        component.set("v.prevPageCount", component.get("v.dataSet").length);
        component.set("v.dataSet", []);
        component.set("v.searchCompleted", true);
        var getLinkedContactsAction = component.get("c.getNextSetOfRMITPublicResearchProjects");
        
        getLinkedContactsAction.setParams({
            params  :  obj
        });
        
        getLinkedContactsAction.setCallback(this, function(a) {
            var state = a.getState();
            component.set("v.searchCompleted", false);
            if (state == "SUCCESS") {
                var result= a.getReturnValue();
                var dataSet = result.publicProjectList;
                console.log(dataSet)
                if (!$A.util.isUndefined(dataSet)) {
                    this.fetchRMITPublicPrjts(component, event, helper, dataSet, result, searchBoxTrimVal);
                }
            }            
        });        
        $A.enqueueAction(getLinkedContactsAction);
    },
    
    getRecordSizeLimit : function(component, event, helper){
        var getRecordSizeLimitAction = component.get("c.getRecordSizeLimit");
        getRecordSizeLimitAction.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "SUCCESS") {
                var result= a.getReturnValue();  
                if (!$A.util.isUndefined(result)) {
                    console.log(result);
                    component.set("v.offsetLimit", result);
                }
            }            
        });        
        $A.enqueueAction(getRecordSizeLimitAction);
    },
    getFORCodes : function(component, event, helper, selectedForCodes) {
        var getForCodesAction = component.get("c.getFORValues");
        
        getForCodesAction.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "SUCCESS") {
                var result= a.getReturnValue();  
                if (!$A.util.isUndefined(result)) {
                    component.set("v.FoRCodes", result);
                    component.set("v.ActualFoRCodes", result);
                    if(selectedForCodes){
                        setTimeout(function(){
                            helper.setForCodes(selectedForCodes)
                        },500)
                    }
                }
            }            
        });        
        $A.enqueueAction(getForCodesAction);
    },
    getImpactCategory : function(component, event, helper, selectedImpactCodes) {
        var getImpactCategorysAction = component.get("c.getImpactCategory");
        
        getImpactCategorysAction.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "SUCCESS") {
                var result= a.getReturnValue();  
                if (!$A.util.isUndefined(result)) {
                    var data = result && result.length>0 ? result: []
                    component.set("v.impactCategories", data);
                    if(selectedImpactCodes){
                        setTimeout(function(){
                            helper.setImpactCodes(selectedImpactCodes)
                        },500)

                    }
                }
            } else {
                component.set("v.impactCategories", []);
            }            
        });        
        $A.enqueueAction(getImpactCategorysAction);
    },
    // change arrow up when impact category dropdown is shown and arrow down when dropdown is closed 
    toggleArrowState: function(dropdownId, arrowId) {
        if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },
    filterList:function(component, event, helper, inputId, dropdownId){
        var input, filter, ul, li, a, i;
            $(dropdownId).css("display", "block");
            input = $(inputId).val();
            filter = input.toUpperCase();
            a = $(dropdownId+" li");
            for (i = 0; i < a.length; i++) {
                if (a[i].textContent.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
    },
    resetFilter:function(component, event, helper, type){
        if(type = 'FOR_CODES'){
            var selectedForList = [];
            var forCheckBoxes = $("#forcodesDropdown input");
            for (var i = 0; i < forCheckBoxes.length; i++) {
                forCheckBoxes[i].checked = false
            }
            component.set("v.selectedForCodes", selectedForList);
            component.set("v.selstatusListCount", selectedForList.length);
            if(selectedForList.length){
                $('input#forcodesfilter').attr('placeholder','')
            } else {
               $('input#forcodesfilter').attr('placeholder','No Selection')
            }            
        } 
        if(type = 'IMPACT_CODES'){
            var selectedImpactCodeList = [];
            var impactCheckBoxes = $("#impactcategoryDropdown input");
            for (var i = 0; i < impactCheckBoxes.length; i++) {
                    impactCheckBoxes[i].checked = false
            }
            component.set("v.selectedImpactCodes", selectedImpactCodeList);
            component.set("v.selstatusImpactListCount", selectedImpactCodeList.length);
            if(selectedImpactCodeList.length){
                $('input#impactcategoryfilter').attr('placeholder','')
            } else {
               $('input#impactcategoryfilter').attr('placeholder','No Selection')
            }
        }
        return []
    },
    addHighlightText:function(component, event, helper, str){
        if(str){
            var className = "highlightText"
            let main = document.querySelectorAll('.contextHighlight')
            main.forEach(function(item){
                let text = item.innerHTML
                var regex = str.split(' ');
                regex = regex.filter(function(char){
                    return char !== '';
                });
                regex = regex.join('|');
                regex = regex.replace(/[-[\]{}()*+?.,\\^$]/g, "\\$&");
                var matcher = new RegExp(regex, 'gi');

                var result =  text.replace(matcher, function wrapper (match) {
                    return "<span class='highlightText'>" + match + "</span>"
                })
                item.innerHTML = result
            })
        }

    },
    removeHighlightText:function(component, event, helper){
        let highlightList = document.querySelectorAll('.highlightText')
        if(highlightList && highlightList.length>0){
            highlightList.forEach(function(item){
                $(item).replaceWith(item.innerHTML)
            })
        }
    },
    updateDataSet:function(component, event, helper, isButtonClick){

        var pageDataSet =  component.get("v.dataSet");
        var totalDataSet = component.get("v.totalDataSet");
        var pageCount = component.get("v.pageCount");
        var hasMore = component.get("v.hasMore");
        var prevCount = component.get("v.prevPageCount");
        let searchBoxValue = $('#search-input').val();
        let searchBoxTrimVal = searchBoxValue == "" ? searchBoxValue : searchBoxValue.trim();
        if(isButtonClick){
            pageCount++;
            component.set("v.pageCount",pageCount);
        }	
        if(totalDataSet.length > prevCount+7 || hasMore){
            component.set("v.showLoadMore", true);
        }
        
        else{
            component.set("v.showLoadMore", false);
        }
        pageDataSet =  totalDataSet.slice(0,prevCount+7);
        component.set("v.dataSet", pageDataSet);
        component.set("v.projectsLength", pageDataSet.length);
        component.set("v.prevPageCount", pageDataSet.length);
        setTimeout(function(){
            helper.addHighlightText(component, event, helper, searchBoxTrimVal);
        },500)
    },
    setLocalStorage:function(name, obj){
        if(window.localStorage){
            window.localStorage.setItem(name, obj);
        }
    },
    setForCodes:function(selectedForCodes){
        if(selectedForCodes && selectedForCodes.length>0){
            $('input#forcodesfilter').attr('placeholder','')
        }
        var forCheckBoxes = $("#forcodesDropdown input");
        for (var i = 0; i < forCheckBoxes.length; i++) {
            if (forCheckBoxes[i].name) {
                var checkValue = (forCheckBoxes[i].name).toLowerCase().split("-")[0].trim()
                if(selectedForCodes.includes(checkValue)){
                    forCheckBoxes[i].checked = true
                }
            }
        }
    },
    setImpactCodes:function(selectedImpactCodes){
        if(selectedImpactCodes && selectedImpactCodes.length>0){
            $('input#impactcategoryfilter').attr('placeholder','')
        }
        var impactCheckBoxes = $("#impactcategoryDropdown input");
        for (var i = 0; i < impactCheckBoxes.length; i++) {
            if (impactCheckBoxes[i].name) {
                var checkValue = (impactCheckBoxes[i].name).toLowerCase().split("-")[0].trim();
                if(selectedImpactCodes.includes(checkValue)){
                    impactCheckBoxes[i].checked = true
                }
            }
        }
    },
       successisCreateProjectApplicableAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            console.log("isCreateProjectApplicable", result)
            if(result == true){
                $('#createPjtBtnAllPjts').css("display","block");
            }
            else{
                $('#createPjtBtnAllPjts').css("display","none");
            }
        }
    },
     errorAction:function(component, helper, error){
        if(error && errors[0] && errors[0].message){
            throw new Error("Error" + errors[0].message);
        }
    },
})