({
    errorAction:function(component, helper, error){
        component.set('v.submitLoader', false);
        component.set("v.serverError", true)
        if(error && errors[0] && errors[0].message){
            throw new Error("Error" + errors[0].message);
        }
    },
    successSubtopicAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            component.set('v.actualSupportTopicList', result)
            helper.setSubTopic(component, result)
        }
    },
    successQueryRelatedAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            let newResult = result ? result.reverse():result
            component.set('v.queryTopics', newResult)
        }
    },
    successLocationAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            component.set('v.locationData', result)
        }
    },
    successContractsAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            console.log(result)
            component.set('v.contractsList', result)
            let seperatorList = ["contractTitle", "contractCode", "status"]
            let seperatorNameList = ["Title", "Code", "Status"]
            let classesList = ["width50", "width30", "width20"]
            let idName = "contractId"
            let type="contracts"
            let attrList=["v.formatContractsList", "v.contractsListCallCompleted"]
            helper.createCustomList(component, helper, result, type, idName, seperatorList, seperatorNameList, classesList, attrList)
        }
    },
    successEthicsAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            console.log(result)
            component.set('v.ethicsList', result)
            let data = helper.removeDuplicates(component, result, "ethicsId")
            let seperatorList = ["ethicsTitle", "type", "status"]
            let seperatorNameList = ["Title", "Type", "Status"]
            let classesList = ["width50", "width30", "width20"]
            let idName = "ethicsId"
            let type="ethics"
            let attrList=["v.formatEthicsList", "v.ethicsListCallCompleted"]
            helper.createCustomList(component, helper, data, type, idName, seperatorList, seperatorNameList, classesList, attrList)
        }
    },
    successMilesthonesAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            console.log(result)
            let data = result && result.milestoneWrapperList ? result.milestoneWrapperList :[]
            component.set('v.milestonesList', data)
            let seperatorList = ["name", "type", "description"]
            let seperatorNameList = ["Name", "Type", "Description"]
            let classesList = ["width50", "width25", "width25"]
            let idName = "milestoneId"
            let type="milestones"
            let attrList=["v.formatMilestonesList", "v.milestonesListCallCompleted"]
            helper.createCustomList(component, helper, data, type, idName, seperatorList, seperatorNameList, classesList, attrList)

        }
    },
    successPublicationAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            console.log(result)
            let data = result && result.publicationWrapperList ? result.publicationWrapperList :[]
            component.set('v.publicationsList', data)
            let seperatorList = ["title", "category", "outlet"]
            let seperatorNameList = ["Title", "Category", "Outlet"]
            let classesList = ["width50", "width25", "width25"]
            let idName = "publicationId"
            let type="publications"
            let attrList=["v.formatPublicationsList", "v.publicationsListCallCompleted"]
            helper.createCustomList(component, helper, data, type, idName, seperatorList, seperatorNameList, classesList, attrList)
        }
    },
    successProjectsAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            helper.createCustomProjectList(component, helper, result)
        }
    },
    createCustomProjectList:function(component, helper, result){
        if(result && result.lstResearchProject && result.lstResearchProject.length){
            let arr = []
            let data = result.lstResearchProject
            component.set('v.projectsList', data)
            data.forEach(function(item,index){
                if(item.projectStartDate){
                    let spiltStartData = item.projectStartDate.split('-')
                    item.projectStartDate = spiltStartData[2]+'/'+spiltStartData[1]+'/'+spiltStartData[0]
                }
                if(item.projectEndDate){
                    let spiltEndData = item.projectEndDate.split('-')
                    item.projectEndDate = spiltEndData[2]+'/'+spiltEndData[1]+'/'+spiltEndData[0]
                }
                let newObj = new Object()
                newObj.id = item.projId
                newObj.title = item.projectTitle
                newObj.seperator1 = item.projectStartDate ? item.projectStartDate : '-'
                newObj.seperator2 = item.projectEndDate ? item.projectEndDate : '-'
                if(item.projectTitle && item.projId){
                    arr.push(newObj)
                }
            })
            component.set('v.formatProjectsList', arr)
        }

    },
    createCustomList:function(component, helper, result, type, idName, seperatorList, seperatorNameList, classesList, attrList){
        let arr =[]
        if(result && result.length>0){
            result.forEach(function(item, index){
                let newObj = new Object()
                newObj.id = type+index+'-'+item[idName]
                newObj.seperator1 = item[seperatorList[0]] ? item[seperatorList[0]] : ' -'
                newObj.seperator2 = item[seperatorList[1]] ? item[seperatorList[1]] : ' -'
                newObj.seperator3 = item[seperatorList[2]] ? item[seperatorList[2]] : ' -'
                newObj.seperator1_Name = seperatorNameList[0]
                newObj.seperator2_Name = seperatorNameList[1]
                newObj.seperator3_Name = seperatorNameList[2]
                newObj.seperator1_class = classesList[0]
                newObj.seperator2_class = classesList[1]
                newObj.seperator3_class = classesList[2]
                newObj.checked = false
                if(item[idName]){
                    arr.push(newObj)
                }
            })
        }
        component.set(attrList[0], arr)
        component.set(attrList[1], true)
    },
    setSubTopic:function(component, result){
        var data = JSON.parse(result);
        var allTopics = [], subTopics = [], topicsDict = [];
        for(var i = 0; i <Object.keys(data).length; i++){
            var subTopics = [];
            allTopics.push(Object.keys(data)[i]);
            var sub = Object.values(data)[i];
            for(var j = 0;j<sub.length; j++){
                subTopics.push(sub[j]);
            }
            topicsDict.push({value: subTopics, key: Object.keys(data)[i]});

        }
        allTopics = allTopics.sort(function(x,y){
            var a = String(x).toUpperCase();
            var b = String(y).toUpperCase();
            if (a > b){
                return 1
            }
            if (a < b) {
                return -1
            }
            return 0;
        });
        component.set("v.topicsDict",  topicsDict);
        component.set("v.supportTopics",  allTopics);
        component.set("v.actualsupportTopics",  allTopics);
        component.set("v.actualtopicsDict",  topicsDict);
    },


    // on close of dropdown hide the list of projects
    restoreProjectList: function() {
        var a, i;
        a = $("#topicDropdown li, #subtopicDropdown li");
        for (i = 0; i < a.length; i++) {
            a[i].style.display = "";
        }
    },
    // change arrow up when dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function(dropdownId, arrowId) {
        if ($(dropdownId).is(':visible')) {
            $(arrowId).removeClass('fa-angle-down');
            $(arrowId).addClass('fa-angle-up');
        } else {
            $(arrowId).removeClass('fa-angle-up');
            $(arrowId).addClass('fa-angle-down');
        }
    },

    //Function call to update the respective subTopic for the selected Topic
    getSubTopicsFromTopic : function(component, event, helper,topic){
        var dict = component.get("v.topicsDict");
        var subtopics = dict.filter(function(el) {
            if (el.key === topic) {
                return el
            }
        });
        component.set("v.subTopics", subtopics[0].value.sort());
        component.set("v.actualsubTopics", subtopics[0].value.sort());
    },
    successSupportPageAction:function(component, helper, result, caseAttachments){
        if (!$A.util.isUndefined(result)) {
            if(result){
                var caseId = result
                var fileSendCount =0;
                let numberOfFiles= caseAttachments && caseAttachments.length > 0 ? caseAttachments.length :0
                if(numberOfFiles >0){
                    helper.sendFile(component, helper, caseId, fileSendCount, numberOfFiles, caseAttachments)
                } else {
                    let params = {
                        "caseid": caseId
                    }
                    helper.callApexServer(component,helper,"c.sendResearchSupportEmailNotification",helper.successEmailNotificationAction,helper.errorAction, params)
                }
            }
        }
    },
    sendFile: function(component, helper, caseId, fileSendCount, numberOfFiles, caseAttachments){
        if(numberOfFiles>fileSendCount){
            let formData= caseAttachments[fileSendCount];
            helper.callServer(component, helper, formData, caseId, fileSendCount, numberOfFiles, caseAttachments)
        } else{
            let params = {
                "caseid": caseId
            }
            helper.callApexServer(component,helper,"c.sendResearchSupportEmailNotification",helper.successEmailNotificationAction,helper.errorAction, params)

        }
    },
    successEmailNotificationAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            component.set('v.submitLoader', false)
            component.set("v.submittedSuccess", true);
        }
    },

    callServer: function(component, helper, formData, caseId, fileSendCount, numberOfFiles, caseAttachments){
        var submitRemainingFiles = component.get("c.attachFilesToCaseRecord");
        submitRemainingFiles.setParams({
            caseAttachments : JSON.stringify(formData),
            caseId:caseId
        });
        submitRemainingFiles.setCallback(this,function(response) {
            var state = response.getState();
            component.set('v.submitLoader', false)
            if(state == "SUCCESS"){
                fileSendCount = fileSendCount+1
                helper.sendFile(component, helper, caseId, fileSendCount, numberOfFiles, caseAttachments)
                component.set("v.submittedSuccess", true);
            } else if(state == "ERROR"){
                console.log('Error in calling server side action');
                component.set('v.submitLoader', false);
                component.set("v.serverError", true)
            }

        });
        $A.enqueueAction(submitRemainingFiles);
    },
    setAccordionSubTextCount:function(component, accordionStatus, type, attributeName){
        if(accordionStatus == 'INACTIVE'){
            let accordionList =type+"List"
            component.find(accordionList).resetMultiSelectSearchBox(accordionList)
            component.find(accordionList).getMultiSelectBoxSelectedIds(accordionList, function(selectedList){
                let accordionSubCountText = selectedList.length ? selectedList.length+' selected' :''
                component.set(attributeName, accordionSubCountText)
            })
        }
        if(accordionStatus == 'ACTIVE'){
            component.set(attributeName, "")
        }
    },
    removeDuplicates:function(component, myArr, prop) {
        let arr =[]
        let arrId =[]
        myArr.forEach(function(item){
            if(arrId.indexOf(item[prop]) == -1){
                arrId.push(item[prop])
                arr.push(item)
            }
        })
        return arr
    }
})