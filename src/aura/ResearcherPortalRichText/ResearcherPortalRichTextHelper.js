({
    initAccordions: function(root) {
        var accordionItems = $('.accordion-item');
        
        if (!accordionItems.length) return;
        
        accordionItems.each(function(i, e) {
            $('.accordion-item-header', e).append('<div class="accordion-icon"><i class="fa fa-chevron-down"></i></div>');
            
            var accordion = $(e);
            if (accordion.is('.first')) {
                var accordionId = accordion.attr('data-accordion');

                if (accordion.prev().is('h1,h2,h3,h4,h5,h6')) {
                    accordion.prev().addClass('accordion-section-header');
                }
                
                if ($('[data-accordion=' + accordionId + ']', root).length > 1) {
                    $('<div class="accordion-collapse-all">')
                        .append('<a href="#">Collapse all sections <i class="fa fa-chevron-circle-up"></i></a></div>')
                        .attr('data-accordion', accordionId)
                        .insertBefore(accordion);
                }
            }
        });
        
        $(root).on('click', '.accordion-item-header', function(evt) {
            evt.preventDefault();
            
            var accordionItem = $(evt.target).parents('.accordion-item');

            accordionItem.toggleClass('expanded');
            $('.fa', accordionItem).toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
        });
        
        $(root).on('click', '.accordion-collapse-all a', function(evt) {
            evt.preventDefault();
            
            var accordionId = $(evt.target).parent().attr('data-accordion');
            var accordions = $('[data-accordion=' + accordionId + ']', root);
            accordions.each(function(i, e) {
                $(e).removeClass('expanded');
                $('.fa', e).removeClass('fa-chevron-up').addClass('fa-chevron-down');
            });
        });
    }
});