({    
    
    //on launch of page, create table. Load related list of last modified 10 projects and update the table.
    enableDataTable: function(component, event, helper) {
        component.set("v.selstatusListCount", 0);  
        var getStatusListAction = component.get("c.getGroupStatusSelectOptions");
        getStatusListAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var statuslist = response.getReturnValue();  
                statuslist.splice(4, 1);
                component.set("v.statusList",statuslist);
            }
        });
        $A.enqueueAction(getStatusListAction);
        
         $('#createPjtBtnAllPjts').hide();
         helper.callApexServer(component,helper,"c.isCreateProjectApplicable",helper.successisCreateProjectApplicableAction,helper.errorAction)
         $('#example').DataTable()
        var getMyProjectsAction = component.get("c.getMyResearchProjectListView");        
        getMyProjectsAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var projects = response.getReturnValue();
                if (!$A.util.isUndefined(projects.lstResearchProject)) {
                    /* if(projects.lstResearchProject.length > 25){
                        $("#btn-load-more").css("display", "block");
                    } */
                    var filteredData = []
                    let resultList = projects.lstResearchProject && projects.lstResearchProject.length >0 ?
                        projects.lstResearchProject:[]
                    resultList.forEach((item)=>{
                        	var newItem = Object.assign({}, item);
                            let filteredTitle = newItem.projectTitle ? newItem.projectTitle.replace(/\n/ig, ' '):''
                            newItem.projectTitle = filteredTitle
                            filteredData.push(newItem)                            
                    })
                    
                    component.set("v.projects", filteredData);
                    
                    //$.fn.dataTable.moment("DD/MM/YYYY");
                    var table = $('#myProjectList').DataTable({
                        "data": [],
                        "language": {
                            "emptyTable": $A.get("$Label.c.Youdonothaveanyprojectsavailableforviewing")
                        },
                        "pageLength": 25,
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [{
                            title: " "
                        }, {
                            title: $A.get("$Label.c.ProjectTitle")
                        }, {
                            title: $A.get("$Label.c.Status")
                        },{
                            title: $A.get("$Label.c.Primary_Contact")
                        }, {
                            title: $A.get("$Label.c.Start_Date")
                        }, {
                            title: $A.get("$Label.c.Project_End_Date")
                        }],
                        "info": false,
                        "order": [[ 2, "asc" ],[1,"asc"]],
                        "searching": false,                        
                        "dom": 'frt',
                        "drawCallback": function() {
                            // If there is some more data
                            if(this.api() && this.api().page && this.api().page.info){
                                var info = this.api().page.info()
                                if(info.length <info.recordsTotal){
                                    $('#btn-load-more').toggle(true);
                                } else {
                                    $('#btn-load-more').toggle(false);
                                }
                            } 
                        },
                        "columnDefs": [{
                            targets: 1,
                            "render": function(data, type, full, meta, row) {
                                return '<div class="truncate"><a class="rmit-blue-color rmit-blue-color_hover title-text" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '" href="' + '/Researcherportal/s/project-details?recordId=' + full[6] + '">' + data + '</a>';
                                
                            }
                        }, {
                            targets: 0,
                            width: "10px",
                            "contentPadding": "mmm",
                            bSortable: false,
                            "render": function(data, type, full, meta, row) {
                                console.log(full)
                                if (full[7] != true) {
                                    return '<div><span class="badge badge-pill rp-project-badge" data-toggle="tooltip" data-placement="top" title="Researcher Portal Project"><i class="fa fa-lightbulb-o" aria-hidden="true">' + data + '</i></span></div>';
                                } else {
                                    return '<div><span class="badge badge-pill badge-danger rm-project-badge">R' + data + '</span></div>';
                                }
                            }
                        }, {
                            targets: [4,5],
                            "render": function(data, type, full, meta, row) {
                                if(type === 'display' || type === 'filter'){
                                    if(data !== '')
                                        return moment(data).format('DD/MM/YYYY') ;
                                    else
                                        return '';
                                }
                                
                                return data;
                            }
                        },
                         {
                            targets: 2,                            
                            "render": function(data, type, full, meta, row) {                                
                                 var accessMap = {
                                    Active: $A.get("$Label.c.ActiveMsg"),
                                    AwaitingOutcome: $A.get("$Label.c.AwaitingoutcomeMsg"),
                                    Unsuccessful: $A.get("$Label.c.UnsuccessfulMsg"),
                                    AwardedContractInProgress: $A.get("$Label.c.AwardedMsg"), 
                                    Closed: $A.get("$Label.c.ClosedMsg")                                     
                                }
                                 //console.log(accessMap[data.replace(/[(,), ]/gi, '')]);
                                return '<span class="slds-form-element__label title-text">'+ data +'</span><span class="nav-item dropdown pjtacceess hideItem"><a class="helpimg" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-question-circle-o"></i></a><div class="dropdown-menu dropdown-menu-top pjtaccess_text shadow" aria-labelledby="navbarDropdown"><span class="dropdown-item d-flex flex-row"><div class="d-block"><span>'+accessMap[data.replace(/[(,), ]/gi, '')]+'</span></div> </span></div></span>';
                               
                            }
                        }]
                        
                    });
                    
                    $.fn.dataTable.Api.register('order.neutral()', function() {
                        return this.iterator('table', function(s) {
                            s.aaSorting.length = 0;
                            s.aiDisplay.sort(function(a, b) {
                                return a - b;
                            });
                            s.aiDisplayMaster.sort(function(a, b) {
                                return a - b;
                            });
                        });
                    });
                    
                    if(projects.lstResearchProject.length === 0){
                        $('#btn-load-more').toggle(false);
                    }
                    // Handle click on "Load more" button
                   var settings = table.settings()
                        // Handle click on "Load more" button
                        $('#btn-load-more').on('click', function() {
                            settings[0]._iDisplayLength = settings[0]._iDisplayLength + 25
                            table.draw()
                     });
                    helper.addRows("#myProjectList", filteredData);
                }
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(getMyProjectsAction);
        
        $("document").ready(function() {
            var offset = 250;
            
            var duration = 300;
            
            $(window).scroll(function() {
                if ($(this).scrollTop() > offset) {
                    $("#back-to-top").fadeIn(duration);
                } else {
                    $("#back-to-top").fadeOut(duration);
                }
                
            });
            
            $("#back-to-top").click(function(a) {
                
                a.preventDefault();
                
                $("html, body").animate({
                    scrollTop: 0
                }, duration);
                
                return false;
            });
            
            $('[data-toggle="tooltip"]').tooltip();
        }); 
        
        
    },
  
    // to navigate to create new project page on click of create project button
    goToCreateProjectPage: function() {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/create-new-project"
        });
        
        urlEvent.fire();
    },
 // method call to toggle impact category dropdown and on edit based on data check the checkbox
    toggleDropdown: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        
        var targetId = targetCmp.id;
        
        var type = targetCmp.dataset.type;
        
        $('#' + targetId).val('');
        
        var topicDropdown = $('#' + type + 'Dropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleArrowState();
            
        } else {
            topicDropdown.css("display", "none");
            helper.toggleArrowState();
        }
        
        $(document).click(component, function(e) {
            if ($(e.target).closest('#typefilter').length === 0 && $(e.target).closest('#typeDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#typeDropdown').is(":visible")) {
                $('#typeDropdown').toggle(false);
                helper.toggleArrowState();
                var topic = $('#typefilter').data('selected');
                $('#typefilter').val(topic);
            }
            
        });
    },
     // Function call to collect all the selected data
    CheckboxSelect: function(component, event, helper) {
        var selectedList = [];
        var checkBoxes = $("#typeDropdown input");
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked) {
                var value = checkBoxes[i].name;
                selectedList.push(value);
            }
        }
        component.set("v.selstatusList", selectedList);  
        component.set("v.selstatusListCount", selectedList.length);  
        helper.fetchFilteredPjtList(selectedList,component.get("v.projects"))
    },
})