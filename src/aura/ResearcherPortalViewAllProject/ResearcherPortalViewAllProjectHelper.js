({
	getRecordID: function(component) {
        var recordId;
        var urlString = window.location.href;
        if (urlString.indexOf("project-details") > -1) {
             var url = window.location;

            if (url.search) {
                recordId = url.search.split("=")[1];
            } else {
                recordId = null;
            }
            
            return recordId;
        }
        
        return null;
    },
    //update/add the table values based on the response
    addRows : function(tableId, dataset) {
		var table = $(tableId).ready().DataTable();
		if(!Array.isArray(dataset))
			dataset = [dataset];
			
		var data = [];
        dataset.forEach(function(project) {
        	var temp = [];

        	temp.push("");
            temp.push(project["projectTitle"] != undefined ? project["projectTitle"]:'');
            temp.push(project["groupStatus"] != undefined ? project["groupStatus"]:'');
            temp.push(project["projectCIName"] != undefined ? project["projectCIName"]:'');
            temp.push(project["projectStartDate"] != undefined ? project["projectStartDate"] :'');
            temp.push(project["projectEndDate"] != undefined ? project["projectEndDate"] :'');
            temp.push(project["projId"] != undefined ? project["projId"]:'');
            temp.push(project["isRMProject"] != undefined ? project["isRMProject"]:'');

            data.push(temp);
       });
       
       //table.order.neutral().draw();
       
       table.rows.add(data).order([[2, "asc"],[1,"asc"]]).draw();
	},
	
    //method call to get the next set of projects on click of load more button
	getMoreProjects: function(component, size) {
		var getMoreProjectsAction = component.get("c.getMyResearchProjects");

        getMoreProjectsAction.setCallback(this, function(a) {
            var state = a.getState();

            if (state == "SUCCESS") {
                var projects = a.getReturnValue();
                if (!$A.util.isUndefined(projects.lstResearchProject)) {
  
                    var temp = component.get("v.projects");
                    
                    var temp = temp.concat(projects.lstResearchProject);
                    
                    component.set("v.projects", temp);
                    
                    this.addRows("#myProjectList", projects.lstResearchProject);
                }
            }
        });
        
        $A.enqueueAction(getMoreProjectsAction);
	},
        // change arrow up when disciple area dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function() {
        if ($('#typeDropdown').is(':visible')) {
            $('#arrowtype').removeClass('fa-angle-down');
            $('#arrowtype').addClass('fa-angle-up');
        } else {
            $('#arrowtype').removeClass('fa-angle-up');
            $('#arrowtype').addClass('fa-angle-down');
        }
    },
    
    fetchFilteredPjtList : function(status,pjtsList){
        var filterpjtList =   $.map(pjtsList,function(val,key){
            if(status.length ==0){
                return val;
            }
            if(status.includes(val.groupStatus)) {
                    return val;
            }
        });
       $('#myProjectList').DataTable().clear();
       this.addRows("#myProjectList", filterpjtList);
    },
    successisCreateProjectApplicableAction:function(component, helper, result){
        if (!$A.util.isUndefined(result)) {
            console.log("isCreateProjectApplicable", result)
            if(result == true){
                $('#createPjtBtnAllPjts').css("display","block");
            }
            else{
                $('#createPjtBtnAllPjts').css("display","none");
            }
        }
    },
     errorAction:function(component, helper, error){
        if(error && errors[0] && errors[0].message){
            throw new Error("Error" + errors[0].message);
        }
    }
})