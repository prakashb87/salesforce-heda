({
 	//on launch of ethics page, create table and get the ethics related to the user. sort ethics by Pending status
    onLoad: function(component, event, helper) {
        helper.getcheckInternalDualAccessUser(component, event, helper);
        var dataSet = [];
        var Search = '';
        var getEthicsListAction = component.get("c.getEthicsList");
        getEthicsListAction.setParams({
            "SearchText": Search,
        });

        getEthicsListAction.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                dataSet = response.getReturnValue();
                console.log(dataSet);
                if(component.get('v.isInternalDualAccessUser') == true){
                    dataSet = [];
                    component.set("v.ethicsList", dataSet);
                }
                var pendArr = dataSet.filter(function(el) {
                    if (el.status === "Pending") {
                        return el
                    }
                });
                var nonPendArr = dataSet.filter(function(el) {
                    if (el.status !== "Pending") {
                        return el
                    }
                });
                dataSet = pendArr.concat(nonPendArr);
                var returnData = [];
                $.each(dataSet, function(index, value) {
                    var rowData = [];
                    rowData.push(value.ethicsTitle != undefined ? value.ethicsTitle : '');
                    rowData.push(value.type != undefined ? value.type : '');
                    rowData.push(value.status != undefined ? value.status : '');
                    rowData.push(value.position != undefined ? value.position : '');
                    rowData.push(value.projectTitle != undefined ? value.projectTitle : '');
                    rowData.push(value.approvedDate != undefined ? value.approvedDate : '');
                    rowData.push(value.expiryDate != undefined ? value.expiryDate : '');
                    rowData.push(value.projectId != undefined ? value.projectId : '');

                    returnData.push(rowData);

                });
                component.set("v.ethicsList", returnData);
                //Date Filtering Code Ends      
                jQuery("document").ready(function() {
                    var table = $('#myEthicsList').DataTable({
                        data: returnData,
                        "pageLength": 25,
                        "language": {
                            "emptyTable": $A.get("$Label.c.YoudonothaveanyEthics")
                        },
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [{
                            title: $A.get("$Label.c.ApplicationTitle")
                        }, {
                            title: $A.get("$Label.c.Type")
                        }, {
                            title: $A.get("$Label.c.Status")
                        }, {
                            title: $A.get("$Label.c.position")
                        }, {
                            title: $A.get("$Label.c.ProjectTitle")
                        }, {
                            title: $A.get("$Label.c.ApprovedDate")
                        }, {
                            title: $A.get("$Label.c.ExpiryDate")
                        }],
                        "info": false,
                        "dom": 'frt',
                        "drawCallback": function() {
                            // If there is some more data
                            /*if ($('#btnLoadMore').is(':visible')) {
                                // Scroll to the "Load more" button
                                $('html, body').animate({
                                    scrollTop: $('#btnLoadMore').offset().top
                                }, 1000);
                            }*/

                            // Show or hide "Load more" button based on whether there is more data available
                            $('#btnLoadMore').toggle(this.api().page.hasMore());
                        },
                        "aaSorting": [],
                        "columnDefs": [{
                            targets: [0,3],
                            "render": function(data, type, full, meta) {
                                return '<div class="truncate"><a data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '"</a>' + data + '</div>'
                            },
                        }, {
                            targets: 4,
                            "render": function(data, type, full, meta, row) {
                                return '<div class="truncate"><a class="rmit-blue-color rmit-blue-color_hover" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '" href="' + '/Researcherportal/s/project-details?recordId=' + full[7] + '">' + data + '</a>';
                            },
                        },
                           {
                        	targets: [5,6],
                        	"render": function(data, type, full, meta, row) {
                        		if(type === 'display' || type === 'filter'){
                        			if(data !== '')
                        				return moment(data).format('DD/MM/YYYY') ;
                        			else
                        				return '';
                        		}
                        		
                        		return data;
                            }
                        }]

                    });

                    // Handle click on "Load more" button
                    $('#btnLoadMore').on('click', function() {
                        // Load more data
                        table.page.loadMore();
                    });
                });
            }
        });
        $A.enqueueAction(getEthicsListAction);
    },
    
    //Search ethics when user types and clicks enter in the input box
    enterSearchRecord: function(component, event, helper) {
        if (event.keyCode === 13) {
            helper.searchFunction(component);
        }
    },
    
    //Method call on click of search button
    searchRecord: function(component, event, helper) {
        helper.searchFunction(component);
    }
})