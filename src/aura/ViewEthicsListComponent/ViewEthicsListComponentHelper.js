({
    // Method call when user search for particular ethics to get the data from backend and sort by Pending status
    searchFunction : function(component, fieldName, elementId) {
        var dataSet = [];  
        var Search = $('#mySearchText').val() || $("#myMobileSearchText").val();
        if(Search.length === 0){
            var searchtEthicsAction = component.get("c.getEthicsList");
        }
        else{
            var searchtEthicsAction = component.get("c.searchtEthicsList");
        }
        searchtEthicsAction.setParams({
            "searchText": Search
        });
        
        searchtEthicsAction.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                dataSet  = response.getReturnValue();
                console.log(dataSet);
                if(component.get('v.isInternalDualAccessUser') == true){
                    dataSet = [];
                    component.set("v.ethicsList", dataSet);
                }
                var pendArr = dataSet.filter(function (el) {
                    if(el.status ==="Pending"){ return el}
                });
                var nonPendArr = dataSet.filter(function (el) {
                    if(el.status!=="Pending"){ return el}
                });
                dataSet = pendArr.concat(nonPendArr);
                //Data Filtering Code
                var returnData = [];
                $.each(dataSet,function(index,value){ 
                    var rowData = [];
                    rowData.push(value.ethicsTitle != undefined ? value.ethicsTitle : '');
                    rowData.push(value.type != undefined ? value.type : '');
                    rowData.push(value.status != undefined ? value.status : '');
                    rowData.push(value.position != undefined ? value.position : '');
                    rowData.push(value.projectTitle != undefined ? value.projectTitle : '');
                    rowData.push(value.approvedDate != undefined ? value.approvedDate : '');                  
                    rowData.push(value.expiryDate != undefined ? value.expiryDate: '');
                    
                    returnData.push(rowData);  
                });
                component.set("v.ethicsList", returnData);
                $('#myEthicsList').DataTable().clear().rows.add(returnData).draw();
            }
            
        });
        
        $A.enqueueAction(searchtEthicsAction);
    },
        getcheckInternalDualAccessUser : function(component, event, helper){
        var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
        
        getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
					component.set('v.isInternalDualAccessUser', result);
                }
            }        
        });
          $A.enqueueAction(getcheckInternalDualAccessUserAction);
    }
})