({
 	//on launch of ethics page, create table and get the ethics related to the user. sort ethics by Pending status
    onLoad: function(component, event, helper) {
        helper.getcheckInternalDualAccessUser(component, event, helper);
        helper.fetchContractResearchTypeValue(component, event, helper);
        helper.fetchContractStatusValue(component, event, helper);
        var dataSet = [];
        var getContractsListAction = component.get("c.getAllResearcherContractRecsInfo");
        getContractsListAction.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                dataSet = response.getReturnValue();
                console.log(dataSet);
                if(component.get('v.isInternalDualAccessUser') == true){
                    dataSet = [];
                    component.set("v.Allcontracts", dataSet);
                    component.set("v.contractsList", dataSet);
                }
                component.set("v.Allcontracts", dataSet);
                helper.loadMyContracts(dataSet, component, event);                
            }
        });
        $A.enqueueAction(getContractsListAction);   
         $("document").ready(function() {
            var offset = 250;
            
            var duration = 300;
            
            $(window).scroll(function() {
                if ($(this).scrollTop() > offset) {
                    $("#back-to-top").fadeIn(duration);
                } else {
                    $("#back-to-top").fadeOut(duration);
                }
                
            });
            
            $("#back-to-top").click(function(a) {
                
                a.preventDefault();
                
                $("html, body").animate({
                    scrollTop: 0
                }, duration);
                
                return false;
            });
            
            $('[data-toggle="tooltip"]').tooltip();
        }); 
    },
     // method call to toggle status and type dropdown
    toggleDropdown: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        
        var targetId = targetCmp.id;
        var type = targetCmp.dataset.type;        
        var topic = $('#' + targetId).data('selected');
        $('#' + targetId).val(topic);
        
        var topicDropdown = $('#' + type + 'Dropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
            
        } else {
            topicDropdown.css("display", "none");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
        }
        
        $(document).click(component, function(e) {
            if ($(e.target).closest('#typefilter').length === 0 && $(e.target).closest('#typeDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#typeDropdown').is(":visible")) {
                $('#typeDropdown').toggle(false);
                helper.toggleArrowState('#typeDropdown', '#arrowtype');
                var topic = $('#typefilter').data('selected');
                $('#typefilter').val(topic);
            }         
            else if ($(e.target).closest('#statusfilter').length === 0 && $(e.target).closest('#statusDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#statusDropdown').is(":visible")) {
                $('#statusDropdown').toggle(false);
                helper.toggleArrowState('#statusDropdown', '#arrowstatus');
                var topic = $('#statusfilter').data('selected');
                $('#statusfilter').val(topic);
            }
            
        });
    },
    //method call on select from dropdown and call filter method based on input selection
    selectTopic: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        var topic = targetCmp.dataset.value;
        var targetId = targetCmp.parentElement.id;
        var statusval = $('#statusfilter').val();
        var typeval = $('#typefilter').val(); 
        var contracts = component.get("v.Allcontracts");
        if (targetId === 'typeDropdown') {
            component.set("v.selectedCategory", topic);
            $('#typefilter').data('selected', topic);
            $('#typefilter').val(topic);
            $('#typeDropdown').toggle(false);
            helper.toggleArrowState('#typeDropdown', '#arrowtype');
            var typeval = $('#typefilter').val(); 
            helper.fetchFilteredContracts(statusval,typeval,contracts);
        } else if (targetId === 'statusDropdown') {
            component.set("v.selectedStatus", topic);
            $('#statusfilter').data('selected',topic);
            $('#statusfilter').val(topic);
            $('#statusDropdown').toggle(false);
            helper.toggleArrowState('#statusDropdown', '#arrowstatus');  
            var statusval = $('#statusfilter').val();
            helper.fetchFilteredContracts(statusval,typeval,contracts);
        }        
    }
})