({
    doInit: function(component, event, helper) {
        var intervalid = window.setInterval(
                $A.getCallback(function() {
                    helper.buttonDsblStatus(component, event, helper);
                    helper.commStatus(component, event, helper);
                    helper.commLoadhDt(component, event, helper);
                }), 1000
            );
        component.set('v.clearinterval', intervalid);
 
    },
    /*
    deltaLoadc: function(component, event, helper) {
        helper.deltaLoadh(component, event, helper);

    },*/

    commPrdc: function(component, event, helper) {
        helper.commPrdh(component, event, helper);
    },
    destoryCmp : function (component, event, helper) {
        clearInterval(component.get('v.clearinterval'));
    }
    /*pricApic: function(component, event, helper) {
        helper.pricApih(component, event, helper);

    }*/

})