({
    /*deltaLoadh: function(component, event, helper) {
        var action = component.get("c.deltaLoad");
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue == false) {
                component.set("v.showFlag", false);
                component.set("v.showMsg", "There is some issue in Delta Load...");
            } else {
                component.set("v.showFlag", true);
                component.set("v.showMsg", "Delta Load in process");

            }
        });
        $A.enqueueAction(action);

    },*/
    commStatus: function(component, event, helper) {
        //var action = component.get("c.deltaLoadStatus");
        var action1 = component.get("c.commProdStatus");
        //var action2 = component.get("c.pricingLoadStatus");
        /*action.setCallback(this, function(a) {
            var state = a.getState();
            if (state == 'SUCCESS') {
                component.set('v.deltaStatus', "Last Delta Load Status: " + a.getReturnValue());
                if (a.getReturnValue() == 'Completed') {
                    component.set("v.showMsg", "Delta Load completed");
                } else {
                    component.set('v.isDisabledDelta', true);
                }
            }
        });
        $A.enqueueAction(action);*/


        action1.setCallback(this, function(a1) {
            var state = a1.getState();
            if (state == 'SUCCESS') {
                component.set('v.commProdStatus', "Last commercial product load status: " + a1.getReturnValue());
                if (a1.getReturnValue() == 'Completed') {
                    component.set("v.showMsg1", "Commercial product sync completed");
                } else {
                    component.set('v.isDisabledCommercialProd', true);
                }
            }
        });
        $A.enqueueAction(action1);


        /*action2.setCallback(this, function(a2) {
            var state = a2.getState();
            if (state == 'SUCCESS') {
                component.set('v.pricingApiStatus', "Last Pricing Api call Status: " + a2.getReturnValue());
                if (a2.getReturnValue() == 'Completed') {
                    component.set("v.showMsg2", "Pricing API in process");
                } else {
                    component.set('v.isDisabledPricingApi', true);
                }
            }
        });
        $A.enqueueAction(action2);*/

    },
      buttonDsblStatus: function(component, event, helper) {
        //var action = component.get("c.deltaLoadDisablt");
        //var action1 = component.get("c.pricApiLoadDisablt");
        var action2 = component.get("c.commLoadDisablt");
       /* component.set("v.isDisabledDelta", true);
        action.setCallback(this, function(a) {
            if (a.getReturnValue() == true) {
                    component.set('v.isDisabledButton', true);
                } else {
                    
                alert('delta disabled false');
                    component.set('v.isDisabledButton', false);
                }
        });
        $A.enqueueAction(action);
        
        action1.setCallback(this, function(a) {
            if (a.getReturnValue() == true) {
                    component.set('v.isDisabledPricingApi', true);
                } else {
                    component.set('v.isDisabledPricingApi', false);
                }
        });
        $A.enqueueAction(action1);*/
            
        
        action2.setCallback(this, function(a) {
            if (a.getReturnValue() == true) {
                    component.set('v.isDisabledCommercialProd', true);
                } else {
                    component.set('v.isDisabledCommercialProd', false);
                }
        });
        $A.enqueueAction(action2);
    },
    commLoadhDt: function(component, event, helper) {

        //var action = component.get("c.deltaLoadDt");
        var action1 = component.get("c.commProdDt");
        /*var action2 = component.get("c.pricingApiDt");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state == 'SUCCESS') {
                component.set('v.deltaDt', "Last Delta Load Run: " + a.getReturnValue());
                console.log(a.getReturnValue());
            }
        });
        $A.enqueueAction(action);*/
        action1.setCallback(this, function(a1) {
            var state = a1.getState();
            if (state == 'SUCCESS') {
                component.set('v.commDt', "Last commercial product sync run: " + a1.getReturnValue());
                console.log(a1.getReturnValue());
            }
        });
        $A.enqueueAction(action1);
        /*
        action2.setCallback(this, function(a2) {
            var state = a2.getState();
            if (state == 'SUCCESS') {
                component.set('v.pricDt', "Last Pricing API Run: " + a2.getReturnValue());
                console.log(a2.getReturnValue());
            }
        });
        $A.enqueueAction(action2);*/
    },
    commPrdh: function(component, event, helper) {
        component.set("v.isDisabledCommercialProd", true);
        var action = component.get("c.commPrdSync");
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue == false) {
                component.set("v.showFlag1", false);
                component.set("v.showMsg1", "There is some issue in commercial product sync");
            } else {
                component.set("v.showFlag1", true);
                // component.set("v.isDisabledCommercialProd", false);
                component.set("v.showMsg1", "Commercial product sync is in process");
            }
        });
        $A.enqueueAction(action);
    },
    /*pricApih: function(component, event, helper) {
        component.set("v.isDisabledPricingApi", true);
        var action = component.get("c.pricingAPI");
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue == false) {
                component.set("v.showFlag2", false);
                component.set("v.showMsg2", "There is some issue in PricAPI Call...");
            } else {
                component.set("v.showFlag2", true);
                //component.set("v.isDisabledPricingApi", false);
                component.set("v.showMsg2", "Pricing Api in process");
            }
        });
        $A.enqueueAction(action);
    },*/
    enableButtontime: function(component, event, helper) {
        var action = component.get("c.buttonEnablT");
    	action.setCallback(this, function(a) {
            if (a.getReturnValue() == true) {
                   // component.set("v.isDisabledPricingApi", false);
        			component.set("v.isDisabledCommercialProd", false);
        			//component.set("v.isDisabledDelta", false);
                } else { 
                   // alert('button disabled true');
                   // component.set("v.isDisabledPricingApi", true);
        			component.set("v.isDisabledCommercialProd", true);
        			//component.set("v.isDisabledDelta", true);
                }
            });
        $A.enqueueAction(action);
    }
})