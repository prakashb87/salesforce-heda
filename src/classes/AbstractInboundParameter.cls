/**
 * Created by mmarsson on 1/03/19.
 * Abstract implementation of IInbound Parameter.
 * Will be used as base processing for the classes as the VO objects musn't have the logic in them.
 * PLEASE DO NOT TOUCH. IF NEEDED EXTEND.
 */

public with sharing virtual class AbstractInboundParameter implements IInboundParameter{

    private List<String> mandatoryFields;


    public void initiateParameter(){ 
        this.setMandatoryFields();
    }

    public virtual void setMandatoryFields(){
        this.mandatoryFields = new List <String>();

    }

    public void addMandatoryField(String field){

        this.mandatoryFields.add(field);
    }

    public virtual void validateParameters(){
        //Ensure there are mandatory fields, otherwise waste of time.
        if (mandatoryFields==null){
            initiateParameter();
        }

        if(this.mandatoryFields.isEmpty()){
            return;
        }

        Map<String,Object>  valueByParameterName = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(this));
        for(String mandField : mandatoryFields){
            if(!valueByParameterName.containsKey(mandField) || (valueByParameterName.get(mandField)==null)){
               //Configures and sends the exception.
                WebserviceException e = new WebserviceException('Mandatory Parameter '+mandField+ ' not present in payload.',RestContext.request.requestBody);
                e.parameter = mandField;
                e.exceptiontype = WebserviceException.WebservixceExceptionType.MANDATORYPARAMETERMISSING;
                e.payload = RestContext.request.requestBody;
                e.errorCode = '02';
                throw e;
            }
        }
    }

    public List<String> getMandatoryFields(){
        return mandatoryFields;
    }

    public Object getFieldValueByName(String fieldName){
        //Might be worth to add the JSON as a field. (food for thought)
        Map<String,Object> thisObject = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(this));
        system.debug('thisObject===>'+thisObject);
        return thisObject.get(fieldName);
    }


}