/*********************************************************************************
 *** @ClassName         : ActivatorLeadAutoConvert_Scheduler_Test
 *** @Author            : Shubham Singh 
 *** @Requirement       : Test class to test batch Batch_ActivatorLeadAutoConvert
 *** @Created date      : 2019-05-13
 *** @Reference         : RM-2720
 *** @Modified by       : shubham singh
 *** @Modified Date     : -
 **********************************************************************************/
@isTest
private class ActivatorLeadAutoConvert_Scheduler_Test {
    //Affiliation role
    static final String ACTIVATORROLE = 'Activator Member';
    //Affiliation status
    static final String CURRENTSTATUS = 'Current';
    //Create test data to process
    //create contact and lead records
    @testSetup static void insertTestContactandTestLead() {
        //Affiliation Activator recordType
        Id activatorRegistrationRTOfAffiliation = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByDeveloperName().get('Activator').getRecordTypeId();
        //Add account
        Account accnt = new Account();
        accnt.Name = 'TestAccount';
        insert accnt;
        //get Account record External ID
        String externalId = CustomSettingsService.getActivatorExternalId();
        //Add Activator account
        Account activator = new Account();
        activator.Name = 'Activator';
        activator.External_Unique_ID__c = externalId;
        insert activator;
        //add contact reccords with student and staff id
        List < Contact > lstContact = new List < Contact > ();
        for (integer i = 0; i < 150; i++) {
            Contact con = new Contact();
            con.LastName = 'Test ' + i;
            con.AccountID = accnt.ID;
            con.FirstName = 'Schedule';
            if (i < 50) {
                con.Enumber__c = 'ABCDEF0011' + i;
            } else {
                con.Student_ID__c = 'ABCDEF0011' + i;
            }
            lstContact.add(con);
        }
        insert lstContact;
        List < contact > insertedContact = [select id from contact];
        system.assertEquals(150, insertedContact.size());
        //Add lead records with student and staff id
        List < Lead > lstLead = new List < Lead > ();
        Id recordTypeID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Activator_Registration').getRecordTypeId();
        for (integer i = 0; i < 200; i++) {
            Lead testLead = new Lead();
            testLead.LastName = 'Test ' + i;
            testLead.FirstName = 'Schedule Lead';
            testLead.Company = 'TestLead';
            testLead.Type_of_registration__c = 'Student';
            if (i < 50) {
                testLead.Type_of_registration__c = 'Staff';
            } 
            testLead.status = 'Curious';
            testLead.MobilePhone = '48378490730';
            testLead.Email = i + 'metestlead@me.com';
            if (i >= 100 && i < 150) {
                testLead.Student_Number__c = 'SABCDEF0011' + i;
            } else {
                testLead.Student_Number__c = 'ABCDEF0011' + i;
            }
            testLead.RecordTypeID = recordTypeID;
            lstLead.add(testLead);
        }
        insert lstLead;
        List < Lead > insertedLead = [select id from Lead];
        system.assertEquals(200, insertedLead.size());
        //insert affiliation records with contact to check the affiliations should not be created for these contact
        hed__Affiliation__c hedaAffilliation;
        list<hed__Affiliation__c> insertAffiliation = new list<hed__Affiliation__c>();
        for (integer i = 0; i < 10; i++) {
            hedaAffilliation = new hed__Affiliation__c();
            hedaAffilliation.RecordTypeId = activatorRegistrationRTOfAffiliation;
            hedaAffilliation.hed__Contact__c = lstContact[i].ID;
            hedaAffilliation.hed__Account__c = accnt.Id;
            hedaAffilliation.hed__Role__c = ACTIVATORROLE;
            hedaAffilliation.hed__Status__c = CURRENTSTATUS;
            hedaAffilliation.hed__StartDate__c = System.today();
            insertAffiliation.add(hedaAffilliation);
        }
        insert insertAffiliation;
        list<hed__Affiliation__c> lstAffiliation = [select id from hed__Affiliation__c];
        system.assertEquals(10,lstAffiliation.size());
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * to test the schedule class by calling it
     * ------------------------------------------------------------------------------------------------
     * @author   Shubham Singh 
     * @method    testScheduledJob
     * @param     -
     * @return    -
     * @Reference RM-2720
     * -----------------------------------------------------------------------------------------------+
     */
    static testMethod void testScheduledJob() {
        // Because this is a test, job executes
        // immediately after Test.stopTest().
        String cronExp = '0 1 * * * ? *';
        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ActivatorLeadAutoConvert Test',
            cronExp,
            new ActivatorLeadAutoConvert_Scheduler());
        Test.stopTest();
        CronTrigger jobRunning = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger where Id =: jobId];
        system.assertEquals(jobRunning.ID, jobId);
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * to test the batch class
     * ------------------------------------------------------------------------------------------------
     * @author   Shubham Singh 
     * @method    methodActivatorLeadAutoConvertTest
     * @param     -
     * @return    -
     * @Reference RM-2720
     * -----------------------------------------------------------------------------------------------+
     */
    static testMethod void methodActivatorLeadAutoConvertTest() {
        Test.startTest();
        Batch_ActivatorLeadAutoConvert batchLeadAutoConvert = new Batch_ActivatorLeadAutoConvert();
        Database.executeBatch(batchLeadAutoConvert, 200);
        Test.stopTest();
        List < Lead > convertedStatusTrue = [select id from Lead where isConverted = true];
        List < Lead > convertedStatusFalse = [select id from Lead where isConverted = False];
        List < hed__Affiliation__c > lstAffiliation = [Select id from hed__Affiliation__c];
        //number of affiliaiton should match to the number of leads converted
        system.assertEquals(150, lstAffiliation.size());
        //lead converted
        System.assertEquals(150, convertedStatusTrue.size());
        //leads which are not converted
        System.assertEquals(50, convertedStatusFalse.size());
    }
}