/**
* -------------------------------------------------------------------------------------------------+
* @description This class controls manual Activator lead conversion feature
* --------------------------------------------------------------------------------------------------
* @author         Dinesh Kumar
* @version        0.1
* @created        05-08-2019
* @modified       05-14-2019
* ------------------------------------------------------------------------------------------------+
*/
public without sharing class ActivatorLeadConversionController {
   
    private static final String CONTACTSTUDENT = 'Validation error on Contact: This contact has a student affiliation, therefore, the following fields cannot be amended; First Name, Last Name, Middle Name, Preferred Name, Gender, Mailing Address, Fax.';
    private static String returnValue='false';
    //Affiliation role
    static final String ACTIVATORROLE = 'Activator Member';
    //Affiliation status
    static final String CURRENTSTATUS = 'Current';
    
    @AuraEnabled
    public static boolean verifyProfile(){
        Boolean result =false;
        Profile pfl = new Profile();
        if(Schema.sObjectType.Profile.fields.Name.isAccessible()){
            pfl = [SELECT Id FROM Profile Where Name =: 'RMIT Activator User'];
        }
        if(string.IsNotBlank(pfl.Id))
        {
            if(UserInfo.getProfileId().equalsIgnoreCase(pfl.Id)){
                result = true;
            }
        }
        /*
        Boolean result = false;
        Profile pfl = [SELECT Id FROM Profile Where Name =: 'RMIT Activator User'];
        if(UserInfo.getProfileId().equalsIgnoreCase(pfl.Id)){
            result = true;
        }
        */
        return result;
    }
    
    /**
    * ------------------------------------------------------------------------------------------------------------+
    * @description Method to Convert Activator Lead
    * ----------------------------------------------------------------------------------------------------------
    * @method    NAME    convertActivatorLead
    * @param     idforConversion   String as combination of AccountId, ContactId, LeadId
    * @return    returnValue    string
    * ------------------------------------------------------------------------------------------------------------+
    */
      
    @AuraEnabled
    public static string convertActivatorLead(Sobject idforConversion){
        Lead processLead = (lead)idforConversion;
        Id activatorAcctId;
        List <Account> actAccount = new List <Account>([SELECT Id FROM Account Where Name = 'Activator' and External_Unique_ID__c =:  CustomSettingsService.getActivatorExternalId() LIMIT 1]);

        List<Contact> contacts = new List<Contact> ([SELECT Id, AccountId FROM Contact WHERE Id=:processLead.Matched_Contact__c Limit 1]);
        Database.LeadConvert lc = new Database.LeadConvert();
        if(!contacts.isEmpty()){
            lc.setContactId(contacts[0].Id);
            lc.setAccountId(contacts[0].AccountId);
        }
        
        lc.setDoNotCreateOpportunity(true);       
        lc.setLeadId(processLead.Id);
        
        if(processLead.OwnerId.getSObjectType() != User.SObjectType) {
            lc.setOwnerId(System.UserInfo.getUserId());
        }
        
        assignLeadIdConvertStatus(lc);
        Database.LeadConvertResult lcr = Database.convertLead(lc, allowduplicate());
        if(lcr.isSuccess()){
            ActivatorLeadConversionController.createActivatorAccountAffiliation(lcr.getContactId(), processLead.RecordTypeId);
            returnValue= 'true,'+lcr.getContactId() +','+ lcr.getContactId() ;
            
        }else{
            throwAuraError(lcr.getErrors());
        }
        return returnValue;
    }
    
    /**
    * ------------------------------------------------------------------------------------------------------------+
    * @description Method to assign Lead Id Convert Status
    * ----------------------------------------------------------------------------------------------------------
    * @method    NAME    assignLeadIdConvertStatus
    * @param     Database.LeadConvert   LeadRecord
    * @return    returnValue    void
    * ------------------------------------------------------------------------------------------------------------+
    */
    private static void assignLeadIdConvertStatus(Database.LeadConvert lc){
    
        List<LeadStatus> convertStatus = new List<LeadStatus>([SELECT 
                                                                Id, 
                                                                MasterLabel 
                                                                FROM LeadStatus 
                                                                WHERE IsConverted=true 
                                                                LIMIT 1]);
        
        lc.setConvertedStatus(convertStatus[0].MasterLabel);
    }
    
    
    /**
    * ------------------------------------------------------------------------------------------------------------+
    * @description Method to allow duplicate records
    * ----------------------------------------------------------------------------------------------------------
    * @method    NAME    allowduplicate
    * @param     null
    * @return    returnValue    Database.DMLOptions
    * ------------------------------------------------------------------------------------------------------------+
    */    
    private static  Database.DMLOptions allowduplicate(){
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        return dml;
    }
    
    
    /**
    * ------------------------------------------------------------------------------------------------------------+
    * @description Method for throw Aura Error
    * ----------------------------------------------------------------------------------------------------------
    * @method    NAME    throwAuraError
    * @param     List<Database.Error> exception
    * @return    returnValue    Void
    * ------------------------------------------------------------------------------------------------------------+
    */    
    private static void throwAuraError(List<Database.Error> errorInput){
        for(Database.Error dbe : errorInput) {
            String errorMessage = dbe.getMessage();  
            if(errorMessage.equalsIgnoreCase(CONTACTSTUDENT)){
                List<String> errorMsg = errorMessage.split(':');
                AuraHandledException e = new AuraHandledException(errorMsg[1]);
                e.setMessage(errorMsg[1]);
                throw e;
            }
            
        }
        
    }   
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description Activator Account and Affilation Creation Method : RM-2698
    * --------------------------------------------------------------------------------------------------------------
    * @method    NAME    createActivatorAccountAffiliation
    * @param     accId
    * @param     contId
    * @param     leadRecordTypeId
    * ---------------------------------------------------------------------------------------------------------------+
    */
    private static void createActivatorAccountAffiliation (Id contId, Id leadRecordTypeId){
        String extId = CustomSettingsService.getActivatorExternalId();
        boolean isExist=false;         
        List <Account> actAccount = new List <Account>([SELECT Id FROM Account Where Name = 'Activator' and External_Unique_ID__c =: extId LIMIT 1]);
        Id recTypeAffActId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Activator').getRecordTypeId();
        Id recTypeAccountActId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();

        List<hed__Affiliation__c> headAff = new List<hed__Affiliation__c> ([SELECT ID FROM hed__Affiliation__c WHERE hed__Contact__c=:contId AND hed__Account__c=:actAccount[0].Id]);
        isExist=!headAff.isEmpty();
        if(Schema.sObjectType.hed__Affiliation__c.fields.hed__Contact__c.isCreateable() && !isExist){
            hed__Affiliation__c hedaAffilliationact = new hed__Affiliation__c();
            hedaAffilliationact.RecordTypeId = recTypeAffActId;
            hedaAffilliationact.hed__Contact__c = contId;
            hedaAffilliationact.hed__Account__c = actAccount[0].Id;
            hedaAffilliationact.hed__Role__c = ACTIVATORROLE;
            hedaAffilliationact.hed__Status__c = CURRENTSTATUS;
            hedaAffilliationact.hed__StartDate__c = System.today();
            insert hedaAffilliationact;
        }        
    }
    
    
    
    /*******************/
}