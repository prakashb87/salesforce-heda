/********************************************************************
// Purpose              : Method for Fetching Details For the Customer Additional Additional Information Page
// Author               : Capgemini [Kiran Kumar]
// Parameters           :
// Returns              : List<hed__Address__c>
//JIRA Reference        : ECB-4360, ECB-4425 & ECB-4436:  Additional Information
//********************************************************************/

public without sharing class AdditionalInformationController {
     
    @AuraEnabled
    public static csapi_BasketRequestWrapper.BasketInfoResponse getCourseNames() {
        
        csapi_BasketRequestWrapper.BasketInfoResponse response = new  csapi_BasketRequestWrapper.BasketInfoResponse();
        csapi_BasketRequestWrapper.BasketInfoRequest request = new csapi_BasketRequestWrapper.BasketInfoRequest();        
        response = csapi_BasketExtension.getBasketInfo(request); 
        
        return response; 
    }
    
    @AuraEnabled
    public static Contact  getContactDetails() {
        
        User user = [select id, ContactId, name from user where id = :UserInfo.getUserId()];
        
        Contact con = [SELECT Id, name, FirstName, LastName, Birthdate, hed__Gender__c FROM Contact  where id = :user.contactId];
        
        Contact contactList = [SELECT
                               Name, FirstName, LastName,
                               hed__Current_Address__r.Mailing_Address__c,
                               hed__Preferred_Email__c,
                               hed__PreferredPhone__c,
                               Preferred_Phone_Number__c,
                               Email,
                               Birthdate,
                               hed__Gender__c, Citizenship_Status__c, Email_Subscription__c,
                               Legal_First_Name__c, Legal_Last_Name__c
                               FROM Contact WHERE id = :con.Id
                              ];
        
        System.debug('Contact Details---->' + contactList);
        return contactList;
    }
    
    @AuraEnabled
    public static List<String> getJobArea() {
        
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = hed__Address__c.Job_Area__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f : ple) {
            options.add(f.getLabel());
        }
        //Returning Job Area picklist values
        return options;
    }
    
    @AuraEnabled
    public static List<String> getCitizenship() {
        
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Contact.Citizenship_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f : ple) {
            options.add(f.getLabel());
        }
        //Returning citizenship status picklist values
        return options;
    }
    
    @AuraEnabled
    public static hed__Address__c getAddressDetails() {
        
        User user = [select id, ContactId, name from user where id = :UserInfo.getUserId()];
        
        List<hed__Address__c> add = [SELECT Id, Name,
                                     hed__Address_Type__c,
                                     hed__MailingStreet__c,
                                     hed__MailingCity__c,
                                     hed__MailingState__c,
                                     hed__MailingCountry__c,
                                     hed__MailingPostalCode__c,
                                     Phone__c, Last_Name__c, First_Name__c, Email__c, Company_Name__c, Job_Area__c
                                     FROM hed__Address__c where hed__Parent_Contact__c = :user.ContactId and hed__Address_Type__c = 'Home'
                                     ORDER BY Effective_Date__c DESC limit 1];
        
        if (add.size() > 0) {
            System.debug('Home Adress--------->' + add);
            //returns if record existed
            return add[0];
        } else {
            hed__Address__c homeAddress = new hed__Address__c(hed__MailingStreet__c = '',
                                                              hed__MailingCity__c = '',
                                                              hed__MailingState__c = '',
                                                              hed__MailingCountry__c = '',
                                                              hed__MailingPostalCode__c = '',
                                                              Phone__c = '',
                                                              Last_Name__c = '',
                                                              First_name__c = '',
                                                              Email__c = '',
                                                              Company_Name__c = '',
                                                              Job_Area__c = '');
            //returns blank record if there is no record
            return homeAddress;
        }
        
    }
    
    @AuraEnabled
    public static hed__Address__c upsertHomeAddressDetails(hed__Address__c homeAddress) {
    	if (!Schema.sObjectType.User.isAccessible()) {
			throw new DMLException();
		}
        User user = [select id, ContactId, name from User where id = :UserInfo.getUserId()];
        System.debug('homeAddress----------->' + homeAddress);
        homeAddress.hed__Address_Type__c = 'Home';
        homeAddress.hed__Parent_Contact__c = user.ContactId;
        homeAddress.Effective_Date__c = System.today();
        Database.upsert(homeAddress);
        
        /*Contact con = [SELECT Id,cscrm__Address__c FROM Contact WHERE Id = :user.ContactId];
        con.cscrm__Address__c = homeAddress.Id;
        Database.upsert(con);*/
        
        return homeAddress;
    }
    
    @AuraEnabled
    public static hed__Address__c getBillingAddressDetails() {
        User user = [select id, ContactId, name from user where id = :UserInfo.getUserId()];
        
        List<hed__Address__c> add = [SELECT Id, Name,
                                     hed__Address_Type__c,
                                     hed__MailingStreet__c,
                                     hed__MailingCity__c,
                                     hed__MailingState__c,
                                     hed__MailingCountry__c,
                                     hed__MailingPostalCode__c,
                                     Phone__c, Last_Name__c, First_Name__c, Email__c, Company_Name__c, Job_Area__c
                                     FROM hed__Address__c where hed__Parent_Contact__c = :user.ContactId and hed__Address_Type__c = 'Billing'
                                     ORDER BY Effective_Date__c DESC limit 1];
        
        if (add.size() > 0) {
            System.debug('Billing Address--------->' + add);
            return add[0];
        } else {
            hed__Address__c billingAddress = new hed__Address__c(hed__MailingStreet__c = '',
                                                                 hed__MailingCity__c = '',
                                                                 hed__MailingState__c = '',
                                                                 hed__MailingCountry__c = '',
                                                                 hed__MailingPostalCode__c = '',
                                                                 Phone__c = '',
                                                                 Last_Name__c = '',
                                                                 First_name__c = '',
                                                                 Email__c = '',
                                                                 Company_Name__c = '',
                                                                 Job_Area__c = '');
            return billingAddress;
        }
    }
    
    @AuraEnabled
    public static hed__Address__c upsertBillingAddressDetails(hed__Address__c billingAddress) {
    	if (!Schema.sObjectType.User.isAccessible()) {
			throw new DMLException();
		}
        User user = [select id, ContactId, name from User where id = :UserInfo.getUserId()];
        System.debug('billingAddress----------->' + billingAddress);
        billingAddress.hed__Address_Type__c = 'Billing';
        billingAddress.hed__Parent_Contact__c = user.ContactId;
        billingAddress.Effective_Date__c = System.today();
        Database.upsert(billingAddress);
        return billingAddress;
    }
    
    @AuraEnabled
    public static Contact upsertContactDetails(Contact contactUpdate) {
    	if (!Schema.sObjectType.User.isAccessible()) {
			throw new DMLException();
		}
        User user = [select id, ContactId, name from User where id = :UserInfo.getUserId()];
        Database.upsert(contactUpdate);
        return contactUpdate;
    }
    
    //ECB-4425 CitizenshipStatus Field show/hide based on citizenship_required__c flag value in account and course
   /* @AuraEnabled
    public static Boolean getCitizenshipStatus() {
        
        Boolean flag = false;
        
        List<String> courseNameList = new List<String>();
        List<String> programNameList = new List<String>();
        
        csapi_BasketRequestWrapper.BasketInfoResponse basketresponse = new  csapi_BasketRequestWrapper.BasketInfoResponse();
        csapi_BasketRequestWrapper.BasketInfoRequest basketrequest = new csapi_BasketRequestWrapper.BasketInfoRequest();
        
        basketresponse=csapi_BasketExtension.getBasketInfo(basketrequest);
        system.debug('basketresponse:::'+basketresponse.Courses);
        
        for(csapi_BasketRequestWrapper.csapi_CourseInfo response : basketresponse.Courses){
            if(response.CourseType == 'Course'){
                courseNameList.add(response.CourseName);
            }else if(response.CourseType == 'Program'){
                programNameList.add(response.CourseName);
            }
        }
        
        List<hed__Course__c> courseCitizenshipStatus = [SELECT Id, Name,RecordType.DeveloperName ,Citizenship_Required__c from hed__Course__c
                                                        WHERE Name IN :courseNameList];
        
        List<Account> programCitizenshipStatus = [SELECT Id, Name,RecordType.DeveloperName ,Citizenship_Required__c from Account
                                                  WHERE Name IN :programNameList];
        
        User user = [select id, ContactId, name from user where id = :UserInfo.getUserId()];
        
        Contact con = [SELECT Id, Citizenship_Status__c FROM Contact  where id = :user.contactId];
        
        if (con.Citizenship_Status__c == null || con.Citizenship_Status__c == '') {
            
            System.debug('Citizenship status null');
            
            for (hed__Course__c course : courseCitizenshipStatus) {
                if (course.Citizenship_Required__c == true && (course.RecordType.DeveloperName =='X21CC' || course.RecordType.DeveloperName =='Award_Course')) {
                    flag = true;
                }
            }
            
            for (Account acc : programCitizenshipStatus) {
                if (acc.Citizenship_Required__c == true) {
                    flag = true;
                }
            }
        }
        
        list<hed__Address__c> addressList = [SELECT ID FROM hed__Address__c WHERE hed__Parent_Contact__c =: con.Id];
        if(addressList.size() > 0){
            flag = true;
        }
        return flag;
        
    }*/
/********************************************************************
// Purpose              : To get the Email address of the logged in user
// Author               : Capgemini [Rishabh]
// Parameters           : null
//  Returns             : String
//JIRA Reference        : ECB-142 Redirect and URL passes purchase info to Onestop
//********************************************************************/
    
    @AuraEnabled
    public static String getEmail(){
        String userEmail=UserInfo.getUserEmail();
        return userEmail;
    }
    
    @AuraEnabled
    public static csapi_BasketRequestWrapper.BasketInfoResponse getresult(){
        
        csapi_BasketRequestWrapper.BasketInfoResponse response =new  csapi_BasketRequestWrapper.BasketInfoResponse();
        csapi_BasketRequestWrapper.BasketInfoRequest request = new csapi_BasketRequestWrapper.BasketInfoRequest();
        
        response=csapi_BasketExtension.getBasketInfo(request);
        system.debug('response:::'+response);
        return response;
    }
    
   /********************************************************************
    Purpose              : 1. Method to call the confirm enroll method ofProduct Basket controller 
    Author               : Capgemini [Shreya]
    Parameters           : String receiptNo,String basketNo
    Returns              : csapi_BasketRequestWrapper.SyncBasketResponse
    JIRA Reference        : ECB-4515 
    ********************************************************************/  
    
    @AuraEnabled
    public static csapi_BasketRequestWrapper.SyncBasketResponse confirmEnroll(String receiptNo,String basketNo){
        csapi_BasketRequestWrapper.SyncBasketResponse response = ProductBasketController.confirmEnroll(receiptNo,basketNo);
        return response;
    }
    
     
    
}