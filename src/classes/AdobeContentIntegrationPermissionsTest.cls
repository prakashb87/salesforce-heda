/**
 * @description This test class checks if the integration user has
 * sufficient access to the required Salesforce objects and fields.
 *
 * This test will catch any permissions misalignments - if the 
 * integration user cannot access a single field or object required,
 * then the entire Salesforce to IPaaS to Adobe integration will
 * fail.
 *
 * Any changes to the Adobe integration data elements should be 
 * updated in this class to catch any unforeseen permission issues 
 * e.g. during a code merge or deployment
 */
@isTest
public with sharing class AdobeContentIntegrationPermissionsTest {

    @TestSetup
    static void setupData(){
        
        // If the integration user's profile is changed, then the below permission should be updated
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        // As of writing, the Integration User uses the System Administrator profile

        User integrationUser = new User();
        integrationUser.firstName = 'Integration';
        integrationUser.lastName = 'User';
        integrationUser.email = 'integration000@testclass.mock.com';
        integrationUser.username = 'integration000@testclass.mock.com';
        integrationUser.IsActive = true;
        integrationUser.Alias = 'testuser';
        integrationUser.TimeZoneSidKey = 'Australia/Sydney'; 
        integrationUser.EmailEncodingKey = 'UTF-8'; 
        integrationUser.LanguageLocaleKey = 'en_US'; 
        integrationUser.LocaleSidKey = 'en_AU';
        integrationUser.ProfileId = profileId;

        insert integrationUser;
    }

    @isTest
    static void testIntegrationUserAccess() {
        List<User> integrationUsers = [SELECT Id FROM User WHERE username = 'integration000@testclass.mock.com'];
        System.assertEquals(1, integrationUsers.size(), 'More than 1 integration user identified in test class');
        // Verify user has access to all the required objects and fields
        System.runAs(integrationUsers[0]) {
            testProgramAccess();
            testProgramPlanAccess();
            testPlanReqAccess();
            testProgramIndPartAccess();
            testProgramTestimonialAcccess();
            testCourseAccess();
            testCourseOfferingAccess();
            testCourseOfferSessionAccess();
            testCourseRelationshipAccess();
            testCourseTestimonialAcccess();
            testCourseIndustPartAccess();
        }
    }

    private static void testProgramAccess() {
        System.assertEquals(true, Schema.sObjectType.Account.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Name.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Status__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Publish_Date__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ParentId.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Recognition__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Tools_and_Materials__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Additional_Information__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Instructor__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Product_line__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Tagline__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Overview__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Image_URL__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Video_URL__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Skills__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Concepts__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Assessment__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Effort__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Points__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Prerequisites__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Delivery_Mode__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Industry__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Skills__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Interest_Area__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.ECB_Program_Enquiry_Email__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Market_Place_Partner_Logo_URL__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Partner_Description_for_Market_Place__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Website.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Phone.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Email__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Effort_per_week__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Account.fields.Enquiry_Only__c.isAccessible());
    }

    private static void testProgramPlanAccess() {
        System.assertEquals(true, Schema.sObjectType.hed__Program_Plan__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Program_Plan__c.fields.hed__Account__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Program_Plan__c.fields.hed__Is_Primary__c.isAccessible());
    }

    private static void testPlanReqAccess() {
        System.assertEquals(true, Schema.sObjectType.hed__Plan_Requirement__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Plan_Requirement__c.fields.hed__Program_Plan__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Plan_Requirement__c.fields.hed__Course__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Plan_Requirement__c.fields.hed__Sequence__c.isAccessible());
    }

    private static void testProgramIndPartAccess() {
        System.assertEquals(true, Schema.sObjectType.Program_Industry_Partner__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Program_Industry_Partner__c.fields.Program__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Program_Industry_Partner__c.fields.Industry_Partner__c.isAccessible());
    }

    private static void testProgramTestimonialAcccess() {
        System.assertEquals(true, Schema.sObjectType.Program_Testimonial__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Program_Testimonial__c.fields.Name.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Program_Testimonial__c.fields.Testimonial__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Program_Testimonial__c.fields.Status__c.isAccessible());
    }

    private static void testCourseAccess() {
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Name.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Tagline__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Overview__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Image_URL__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Video_URL__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Skills_Acquired__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Concepts__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Assessment__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Effort__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Points__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Course_Prerequisites__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Mode_of_Delivery__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Industry__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Skills__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Interest_Area__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Capability_Home__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Enquiry_Only__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Enquiry_Email__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Status__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Publish_Date__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Price__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Pricing_Model__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Recognition__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Tools_and_Materials__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Additional_Information__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Instructor__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.hed__Account__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Product_Line__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course__c.fields.Effort_per_week__c.isAccessible());
    }

    private static void testCourseOfferingAccess() {
        System.assertEquals(true, Schema.sObjectType.hed__Course_Offering__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course_Offering__c.fields.hed__Start_Date__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course_Offering__c.fields.hed__End_Date__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course_Offering__c.fields.Status__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.hed__Course_Offering__c.fields.Last_Day_to_Add__c.isAccessible());
    }

    private static void testCourseOfferSessionAccess() {
        System.assertEquals(true, Schema.sObjectType.Course_Offering_Session__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Offering_Session__c.fields.Start_Time__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Offering_Session__c.fields.End_Time__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Offering_Session__c.fields.Hours__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Offering_Session__c.fields.Venue__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Offering_Session__c.fields.Status__c.isAccessible());
    }

    private static void testCourseRelationshipAccess() {
        System.assertEquals(true, Schema.sObjectType.Course_Relationship__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Relationship__c.fields.Course__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Relationship__c.fields.Related_Course__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Relationship__c.fields.Sequence__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Relationship__c.fields.Status__c.isAccessible());
    }

    private static void testCourseTestimonialAcccess() {
        System.assertEquals(true, Schema.sObjectType.Course_Testimonial__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Testimonial__c.fields.Name.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Testimonial__c.fields.Testimonial__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Testimonial__c.fields.Status__c.isAccessible());
    }

    private static void testCourseIndustPartAccess() {
        System.assertEquals(true, Schema.sObjectType.Course_Industry_Partner__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Industry_Partner__c.fields.Course_id__c.isAccessible());
        System.assertEquals(true, Schema.sObjectType.Course_Industry_Partner__c.fields.Industry_Partner__c.isAccessible());
    }
}