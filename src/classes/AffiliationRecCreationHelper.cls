/*****************************************************************************************************
*** @Class             : AffiliationRecCreationHelper
*** @Author            : Avinash Machineni
*** @Requirement       : when a course connection is created, link them to affiliation records 
*** @Created date      : 04/06/2018
*** @JIRA ID           : ECB-3748

*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is future class to send course ID to IPASS API.Its called from apex schduler class scheduledCourse
*****************************************************************************************************/ 

public with sharing class AffiliationRecCreationHelper {
    
    public static User usr;
    
    //********************************************************************
    // Purpose             : Creating affiliation records                               
    // Author              : Capgemini [Avinash]
    // Parameters           : list<String>
    //  Returns             : Map<id,hed__Affiliation__c>
    //JIRA Reference        : ECB-3748 - Affilation Creation on Successful purchase of 21CC courses 
    //********************************************************************/ 
    public static Map<String,Id> createaffilaitions(list<String> studentIds, Set<String> courseType) {
        
        List<Contact> contactswithAffs;
        Set<String> checkAff;
        List<hed__Affiliation__c> newaffiliations =  new List<hed__Affiliation__c>();
        Map<id,hed__Affiliation__c> affs = new  Map<id,hed__Affiliation__c>();
        Map<String,Id> affiliationMap = new Map<String,Id>();
        
        List<Account>  accountList = new List<Account>();
        if(Schema.sObjectType.Account.isAccessible()){
            accountList = [Select Name, Id FROM Account WHERE (Name = '21CC' OR Name = 'RMIT Online' OR Name = 'RMIT Training')];
        }
        
        
        // Map Created as per ECB-5038
        Map<String, Id> accountMap = new Map<String, Id>();
        
        if(!accountList.isEmpty()){
            for(Account acc: accountList) {
                accountMap.put(acc.Name, acc.Id); 
            }            
        }
        
        usr = [Select id, Name, ContactId from User where id =: UserInfo.getUserId() LIMIT 1];
        
        //Added this for test class
        if(Test.isRunningTest()){
            usr = TestDataFactoryUtil.testusr;     
        }
        //Contact with affiliation
        if(Schema.sObjectType.Contact.isAccessible()){
            contactswithAffs = [SELECT Id, Name, AccountId, Student_ID__c, (select id, hed__Contact__c,hed__Account__c,hed__Account__r.Name from hed__Affl_Accounts__r Where hed__Contact__c =: usr.ContactId) FROM Contact where id =: usr.ContactId];
        }
        
        Set<String> accountName = new Set<String>();
        
        for(Contact con: contactswithAffs){
            for(hed__Affiliation__c aff : con.hed__Affl_Accounts__r){
      String v = aff.hed__Contact__c+aff.hed__Account__r.Name;
                accountName.add(aff.hed__Account__r.Name);
        affiliationMap.put(v,aff.Id);
            }
        }
        
        for(Contact cont: contactswithAffs){ 
            if(cont.hed__Affl_Accounts__r.size() == 0){
                //Change as per ECB-5038
                if(!courseType.isEmpty()){
                    newaffiliations = createNewAffilaitions(accountMap,courseType,cont);
                }  
            }else{
                if(Cont.hed__Affl_Accounts__r.size() > 0){
                    //Added For ECB-5038
                    if(!courseType.isEmpty()){
                        checkAff = checkAffiliations(accountName,courseType);
                    }
                    if(!checkAff.isEmpty()){
                        newaffiliations = createNewAffilaitions(accountMap,checkAff,cont);
                    }
                }
            }
        }
        
        
        if(newaffiliations.size() > 0){
            try{
                Database.insert(newaffiliations);
            }Catch(Exception e){
                ApplicationErrorLogger.logError(e);
            }
        }
        
        for(hed__Affiliation__c affiliation: newaffiliations){
    String v = affiliation.hed__Contact__c+affiliation.hed__Account__r.Name;
            affs.put(affiliation.hed__Contact__c, affiliation);
            affiliationMap.put(v, affiliation.id);
        }
        System.debug('AffMap-->'+affiliationMap);
        return affiliationMap;
        
    }
    
    public static List<hed__Affiliation__c> createNewAffilaitions(Map<String, Id> accountMap, Set<String> courseType,Contact cont){
        
        List<hed__Affiliation__c> affiliationList = new List<hed__Affiliation__c>();      
        hed__Affiliation__c affiliation = new hed__Affiliation__c();
        
        affiliation.hed__Contact__c = cont.Id;
        affiliation.hed__Role__c = 'Student';
        affiliation.hed__Status__c = 'Current';
        affiliation.recordtypeid  = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        for (String s : courseType){
            affiliation.hed__Account__c = accountMap.get(s); 
        }
        
        affiliationList.add(affiliation);
        return affiliationList;
    }
    
    public Static Set<String> checkAffiliations(Set<String> accountName, Set<String> courseType){
        Set<String> result = new Set<String>();
        for(String s: courseType){
            if(accountName.contains(s)){
                System.debug('duplicate');
            }else{
                result.add(s);
            }
        }
        return result;
    }
}