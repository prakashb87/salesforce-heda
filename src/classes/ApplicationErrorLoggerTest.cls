/********************************************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 04/06/2018     ****************************************************/               
/***   This is the Test Class for ApplicationErrorLogger Class     **************************************************************/                                           
/********************************************************************************************************************************/
@IsTest
@SuppressWarnings('PMD.AvoidHardcodingId')
public class ApplicationErrorLoggerTest {
	
    static testMethod void testCanvasFirstErrorLogMethod() {
                 
        //Create an Integration Log record
       // This object is used because it won't have anything blocking the creation
       // e.g. validation rules, triggers, etc...
        String courseConnId = '0065D000002i2Rz';
        String errorType = 'Canvas Integration';
        Course_Connection_Life_Cycle__c cclyf = new Course_Connection_Life_Cycle__c();
        cclyf.Course_Connection__c = Id.valueOf(courseConnId);
        Id recordID = cclyf.Course_Connection__c;
       
        CourseConnCanvasResponseWrapper.Response ccrw = new CourseConnCanvasResponseWrapper.Response();
        ccrw.courseConnectionId= String.valueOf(recordID);
        ccrw.result='test';
        ccrw.html_url='http://test';
        ccrw.errorMessage='test error message';
        
        Map<Id, CourseConnCanvasResponseWrapper.Response> canvasres = new Map<Id, CourseConnCanvasResponseWrapper.Response>();
        canvasres.put(recordID, ccrw);
                
		ApplicationErrorLogger.logError(cclyf, errorType, canvasres);

		List<RMErrLog__c> dbResults = [Select ID, Record_Id__c, Error_Type__c,
		                               Error_Payload__c, Error_Code__c from RMErrLog__c];

		System.assertEquals(1, dbResults.size());
		//System.assertEquals(errorId, dbResults[0].Record_Id__c);
    }    
    static testMethod void testSAMSFirstErrorLogMethod() {
                 
        //Create an Integration Log record
       // This object is used because it won't have anything blocking the creation
       // e.g. validation rules, triggers, etc...
       
        String errorType = 'SAMS Integration';
        Course_Connection_Life_Cycle__c cclyf = new Course_Connection_Life_Cycle__c();
        cclyf.Course_Connection__c=Id.valueOf('0065D000002i2Rz');
        Id recordID = cclyf.Course_Connection__c;
       
        JsonCourseWrapper.IpaasResponseWrapper jcrw = new JsonCourseWrapper.IpaasResponseWrapper();
        jcrw.courseConnectionId=recordID;
        jcrw.result='test';
        jcrw.errorCode='test';
        jcrw.errorText='test error message';
        
        Map<String, JsonCourseWrapper.IpaasResponseWrapper> ipaasResWrp = new Map<String, JsonCourseWrapper.IpaasResponseWrapper>();
        ipaasResWrp.put(jcrw.courseConnectionId, jcrw);
       
		ApplicationErrorLogger.logError(cclyf, errorType, ipaasResWrp);

		List<RMErrLog__c> dbResults = [Select ID, Record_Id__c, Error_Type__c,
		                               Error_Payload__c, Error_Code__c from RMErrLog__c];

		System.assertEquals(1, dbResults.size());
		//System.assertEquals(errorId, dbResults[0].Record_Id__c);
    }    

    static testMethod void testSecondLogErrorLogMethod() {       
        String  num= '12345';
		String errMessage = 'Invalid Data';
		Integer errNum = 101;
        Datetime errTime = DateTime.now(); 
        String errType = 'Payload Error';
        
        Exception ex = new DMLException();
        ex.getCause();
        
		//ApplicationErrorLogger.logError(num, ex ,ex.getCause(), errNum, errMessage, errTime, errType);
		ApplicationErrorLogger.logError(ex);
		
		List<RMErrLog__c> dbResults = [Select ID, Error_Cause__c, Error_Line_Number__c,
		                               Error_Message__c, Error_Time__c, Error_Type__c from RMErrLog__c];

		System.assertEquals(1, dbResults.size());
		System.assertEquals(errTime, dbResults[0].Error_Time__c);
        
    }

/*
    static testMethod void testThirdLogErrorLogMethod() {
*/        
        /* 
        Create an Integration Log record
        This object is used because it won't have anything blocking the creation
        e.g. validation rules, triggers, etc...
        */
/*        
        Integration_Logs__c randomRecord = new Integration_Logs__c();
        insert randomRecord;
        
        ApplicationErrorLogger.logError( 'TestIntegration', 'TestClassName', 'TestErrorCauase', '100', 22, 
            'TestErrorMeaasege', 'Test Payload', 'TestErrorType', randomRecord.Id, false, false, false,'Testmethod()' );

        List<RMErrLog__c> dbResults = [Select ID, Record_Id__c, Error_Type__c,
		                               Error_Payload__c, Error_Code__c,Business_Function_Name__c from RMErrLog__c];

		System.assertEquals(1, dbResults.size());
		System.assertEquals('TestIntegration', dbResults[0].Business_Function_Name__c);
    }
*/
    
    static testMethod void testAutoPuchaseErrorLoggerMethod() {
        ApplicationErrorLogger.logError('Auto Purchase','Student ID=1111 & Micro-Credential Name=AAAA','TestErrorMessage');
         List<RMErrLog__c> dbResults = [Select Error_Type__c,Business_Function_Name__c,Record_Id__c,Error_Message__c from RMErrLog__c];

		System.assertEquals(1, dbResults.size());
		System.assertEquals('Auto Purchase', dbResults[0].Business_Function_Name__c);
    }
}