public with sharing class ApplicationTouchPointsUpdate {
    //update the touchpoints
    public void beforeUpdateApplication(List < Application__c > newApplicationRecord, List < Application__c > oldApplicationRecord) {
        try {    
            List < Application__c > applicationRecords;
            if (newApplicationRecord != null && newApplicationRecord.size() > 0) {
                applicationRecords = new List < Application__c > (newApplicationRecord);
            } else if (oldApplicationRecord != null && oldApplicationRecord.size() > 0) {
                applicationRecords = new List < Application__c > (oldApplicationRecord);
            }
            
            Set < ID > relatedLead = new Set < ID > ();
            //store lead related to application record
            for (Application__c newApplication: applicationRecords) {
                if (newApplication.Applicant_Name__c != null && newApplication.Program_Application_Status__c != null) {
                    relatedLead.add(newApplication.Applicant_Name__c);
                }
            }
            updateApplicationMethod(relatedLead, applicationRecords);

        } catch (DmlException ex) {
            System.debug('The following exception has occurred: ' + ex.getMessage());
        }
    }
    private void updateApplicationMethod(Set <ID> relatedLeadId, List < Application__c > applicationRecords) {
        List < Lead > leadlist = new List < Lead > ();
        map<ID, Task> mapAppTask = new map<ID,Task>();
        map<ID, CampaignMember> mapAppCampaign = new map<ID, CampaignMember> ();
        //get task related to lead
        for(Task task:[SELECT WhoId, Subject, CreatedDate, TaskSubtype 
                       FROM Task WHERE WHOID IN : relatedLeadId
                       ORDER BY CreatedDate DESC]){
                       	if(!mapAppTask.containsKey(task.WhoId)){
                       		mapAppTask.put(task.WhoId, task);
                       	}
                           
                       }
        //get campaignmember related to lead
        for(CampaignMember cm:[SELECT LeadId, CreatedDate, Campaign.Name 
                               FROM CampaignMember 
                               WHERE LeadId IN: relatedLeadId
                               ORDER BY CreatedDate DESC]){
                               	if(!mapAppCampaign.containsKey(cm.LeadId)){
                                   mapAppCampaign.put(cm.LeadId, cm);               
                               }
                               }
        finalUpdateMethod(applicationRecords, mapAppTask, mapAppCampaign);
    }
  
    private void finalUpdateMethod(List < Application__c > applicationRecords, Map < Id, Task> mapAppTask, Map < Id, CampaignMember> mapAppCampaign) {
        List < Application__c > lstapplicationUpdate = new List < Application__c > ();
        Lead leadRecord;
        CampaignMember leadCampMember;
        Task leadTask;
         for (Application__c newApplication: applicationRecords) {
             leadTask= new Task();
             leadCampMember= new CampaignMember();
             if(mapAppTask.containsKey(newApplication.Applicant_Name__c)){
                 leadTask=mapAppTask.get(newApplication.Applicant_Name__c);
             }
             if(mapAppCampaign.containsKey(newApplication.Applicant_Name__c)){
                 leadCampMember=mapAppCampaign.get(newApplication.Applicant_Name__c);
             }
             updateApplicationRecords(leadTask,leadCampMember,newApplication);
         }

    }
   
    public void updateApplicationRecords(Task leadTask,CampaignMember leadCampMember,Application__c newApplication){
        
       TouchpointWrapper tp = new TouchpointWrapper();
       
        if(leadTask!=null){
            tp.strTouchpoint = leadTask.TaskSubtype;
            tp.dtTouchpoint=leadTask.createdDate;
            tp.strSubject=leadTask.Subject;
        }
        if(leadCampMember.Id != null){
            tp.strTouchpoint='Campaign';
            tp.dtTouchpoint= leadCampMember.CreatedDate;
            tp.strSubject=leadCampMember.Campaign.Name;
        }

        if(leadCampMember!=null && leadTask!=null && leadCampMember.CreatedDate > leadTask.CreatedDate){
            tp.strTouchpoint='Campaign';
            tp.dtTouchpoint= leadCampMember.CreatedDate;
            tp.strSubject=leadCampMember.Campaign.Name;
        }
        if(leadCampMember!=null && leadTask!=null && leadCampMember.CreatedDate < leadTask.CreatedDate){
            tp.strTouchpoint = leadTask.TaskSubtype;
            tp.dtTouchpoint=leadTask.createdDate;
            tp.strSubject=leadTask.Subject;
        }
        updateApplicationBasedOnCreatedDate(tp,newApplication); 
            
        
    }
    
    private void updateApplicationBasedOnCreatedDate(TouchpointWrapper touchPoint,Application__c applicationUpdate) {
        if (applicationUpdate.Program_Application_Status__c == 'Application' ) {
            applicationUpdate.Application_Conversion_Touchpoint__c = touchPoint.strTouchpoint;
            applicationUpdate.Application_Conversion_Touchpoint_Date__c = touchPoint.dtTouchpoint;
            applicationUpdate.Application_ConversionTouchpoint_Subject__c = touchPoint.strSubject;
        }else if (applicationUpdate.Program_Application_Status__c == 'Enrolment') {
            applicationUpdate.Enrolment_Conversion_Touchpoint__c = touchPoint.strTouchpoint;
            applicationUpdate.Enrolment_Conversion_Touchpoint_Date__c = touchPoint.dtTouchpoint;
            applicationUpdate.Enrolment_Conversion_Touchpoint_Subject__c = touchPoint.strSubject;
        }else if (applicationUpdate.Program_Application_Status__c == 'Acceptance') {
            applicationUpdate.Acceptance_Conversion_Touchpoint__c = touchPoint.strTouchpoint;
            applicationUpdate.Acceptance_Conversion_Touchpoint_Date__c = touchPoint.dtTouchpoint;
            applicationUpdate.Acceptance_Conversion_Touchpoint_Subject__c = touchPoint.strSubject;
        }
           
    }
}