//This is the Trigger handler for Application Trigger
public with sharing class ApplicationTriggerHandler {
    //update lead records based on priority
    public void updateLeadRecords(List < Application__c > applicationRecord) {
        try {
            Set < Id > leadId = new Set < Id > ();
            map < ID, List<Application__c> > mapLeadAppRecord = new map < ID, List<Application__c> > ();
            List < Application__c > listOfApp = new List < Application__c > ();
            List < Application__c > application = new List < Application__c > ();
            Map < Id, Integer > mapOfLeadToUpdate = new Map < Id, Integer > ();


            for (Application__c appVar: applicationRecord) {
                if (appVar.Applicant_Name__c != null && Schema.sObjectType.Application__c.fields.Applicant_Name__c.isAccessible()) {
                    leadId.add(appVar.Applicant_Name__c);
                }
            }

            mapLeadAppRecord = allRelatedApplication(leadId);

            mapOfLeadToUpdate = relatedLeadToUpdate(mapLeadAppRecord);
            updateLeadRecord(mapOfLeadToUpdate);

            if (Test.isRunningTest()) {
                throw new DmlException();
            }
        } catch (DmlException ex) {
            System.debug('The following exception has occurred: ' + ex.getMessage());
        }
    }
        
     private map < ID, List<Application__c>  > allRelatedApplication(Set <Id> leadRecordId) {
         map<ID,List<Application__c>> result= new map<ID, List<Application__c>>();
         List<Application__c> applications;
        //if(Schema.sObjectType.Application__c.fields.Applicant_Name__c.isAccessible() &&
         //   Schema.sObjectType.Application__c.fields.Program_Application_Status__c.isAccessible()){
             for(Application__c app:[SELECT Applicant_Name__c, Program_Application_Status__c 
                                        FROM Application__c 
                                        WHERE Applicant_Name__c IN: leadRecordId ]){
                 if(!result.containsKey(app.Applicant_Name__c)){
                     applications = new List<Application__c>();
                     applications.add(app);
                     result.put(app.Applicant_Name__c, applications);
                 }else{
                     result.get(app.Applicant_Name__c).add(app);
                 }
             }
         //}
         return result;
     }
     private Map < Id, Integer > relatedLeadToUpdate(map<Id,List<Application__c>> leadAppMap) {
        Map < Id, List < Application__c >> mapOfLstOfApp = new Map < Id, List < Application__c >> ();
        Map < String, Integer > mapOfStatus = new Map < String, Integer > {
            'Enrolment' => 1,
            'Acceptance' => 2,
            'Offer' => 3,
            'Conditional offer' => 4,
            'Application' => 5,
            'Registration' => 6,
            'Deny' => 7,
            'Deferment' => 8,
            'Leave' => 9,
            'Withdrawal' => 10,
            'Archived' => 11
        };
        Id leadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Prospective_Student').getRecordTypeId();
        //if(Schema.sObjectType.Lead.fields.Id.isAccessible()){
            for (Lead varLead: [SELECT Id, RecordTypeId FROM Lead WHERE ID IN: leadAppMap.KeySet()]) {
                if (leadRecordTypeId == varLead.RecordTypeId) {
                    mapOfLstOfApp.put(varLead.Id, leadAppMap.get(varLead.Id));
                }
            }
        //}
        Map < Id, Integer > mapUpdateLead = new Map < Id, Integer > ();
        Integer priority;
        List < Integer > highPriority;
        if(!mapOfLstOfApp.isEmpty()){
            for (Id leadRec: mapOfLstOfApp.keySet()) {
                highPriority = new List < Integer > ();
                for (Application__c apps: mapOfLstOfApp.get(leadRec)) {
                    priority = mapOfStatus.get(apps.Program_Application_Status__c);
                    highPriority.add(priority);
                }
                if (highPriority.size() > 0) {
                    highPriority.sort();
                    mapUpdateLead.put(leadRec, highPriority[0]);
                }
            }
        }
        return mapUpdateLead;
     }

      private void updateLeadRecord(Map < Id, Integer > mapOfLeadToUpdate) {

        Map < Integer, String > mapOfPriorityStatus = new Map < Integer, String > {
            1 => 'Enrolment',
            2 => 'Acceptance',
            3 => 'Offer',
            4 => 'Conditional offer',
            5 => 'Application',
            6 => 'Registration',
            7 => 'Deny',
            8 => 'Deferment',
            9 => 'Leave',
            10 => 'Withdrawal',
            11 => 'Archived'
        };
        List < Lead > listUpdate = new List < Lead > ();
        //if(Schema.sObjectType.Lead.fields.Current_Status__c.isAccessible()){
            for (Lead updateLead: [SELECT Id,Current_Status__c FROM Lead WHERE ID IN : mapOfLeadToUpdate.KeySet() ]) {
                //RM-2363 
                if (mapOfLeadToUpdate.get(updateLead.Id) < 11) {
                    updateLead.Current_Status__c = mapOfPriorityStatus.get(mapOfLeadToUpdate.get(updateLead.Id));
                    listUpdate.add(updateLead);
                }
            }
        //}
        Database.SaveResult[] srList;
        if (!listUpdate.isEmpty()) {
            srList = Database.Update(listUpdate, False);
            // Iterate through each returned result
            for (Database.SaveResult sr: srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for (Database.Error err: sr.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
        }
    }
}