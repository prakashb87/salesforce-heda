@isTest
public class BatchJobInvokeControllerTest {        
    @isTest  
    static void testloadCourseDetailsTestSuccess() {
        
        //create test data
	 	string myJsonBody = '{"id": "ID-dev-20180718T035647828","result": "SUCCESS","code": "200","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sent successfully to Exchange rmit-publisher-api-sfdc-dev"}';
        String testObjDt;
         
		//TestDataFactoryUtil.deltaTestSuccess();

        //Test.setMock(HttpCalloutMock.class, new iPaasDeltaLoadCalloutServiceMock());
        
        //Custom Setting data making
        List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();
    
        Config_Data_Map__c timeDisabled = new Config_Data_Map__c();
        timeDisabled.name = 'DisabledDuration_TimeAttribute';
        timeDisabled.Config_Value__c = '5';
        configurations.add(timeDisabled);
    
        Config_Data_Map__c timeStartE = new Config_Data_Map__c();
        timeStartE.name = 'EnabledDuration_TimeStart';
        timeStartE.Config_Value__c = '9';
        configurations.add(timeStartE);
    
        Config_Data_Map__c timeEndE = new Config_Data_Map__c();
        timeEndE.name = 'EnabledDuration_TimeEnd';
        timeEndE.Config_Value__c = '17';
        configurations.add(timeEndE);
        
        insert configurations;
		
		Test.startTest();
		/*Boolean deltaLoad = BatchJobInvokeController.deltaLoad();
        testObjDt = BatchJobInvokeController.deltaLoadDt();
        Boolean v = BatchJobInvokeController.deltaLoadDisablt();
        Boolean v1 = BatchJobInvokeController.pricApiLoadDisablt();*/
        Boolean v2 = BatchJobInvokeController.commLoadDisablt();
        system.assertEquals(false, v2);
        
        String commDt = BatchJobInvokeController.commProdDt();
        String commStatus = BatchJobInvokeController.commProdStatus();
       /* String pricDt = BatchJobInvokeController.pricingApiDt();
        String pricStatus = BatchJobInvokeController.pricingLoadStatus();*/
		Test.stopTest();

		//verify outbound request record in SF
		/*list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
		system.assertEquals(1, dbResults.size());
        System.debug('dbResults++++'+dbResults);

		//verify 
		system.assertEquals(myJsonBody, dbResults[0].Request_Response__c);

		list<Integration_Logs__c> dbResults2 = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Outbound Service'];
		system.assertEquals(1, dbResults2.size());
        System.debug('dbResults2++++'+dbResults2);*/
	}
    /*
    @isTest static void testloadCourseDetailsTestError() {
        //create test data
	 	string responsebody = '{"id": "ID-dev-20180718T035647828","result": "ERROR","code": "400","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sending Failed"}';

		TestDataFactoryUtil.deltaTestError();

        Test.setMock(HttpCalloutMock.class, new iPaasDeltaLoadCalloutServiceMock2());
		
		Test.startTest();
		//DeltaLoadDummyClass.sendCourseAsync();
		BatchJobInvokeController.deltaLoad();
		Test.stopTest();
 
		//verify outbound request record in SF
		list<Integration_Logs__c> intloglist = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
		system.assertEquals(1, intloglist.size());
        System.debug('intloglist++++'+intloglist);

		System.debug('Database Result+++++'+ intloglist[0].Request_Response__c);
        System.debug('responsebody+++++'+responsebody);
		system.assertEquals(responsebody, intloglist[0].Request_Response__c);

		list<Integration_Logs__c> listinteglog = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Outbound Service'];
		system.assertEquals(1, listinteglog.size());
        System.debug('listinteglog++++'+listinteglog);
	}


    public class iPaasDeltaLoadCalloutServiceMock implements HttpCalloutMock {
        // implement http mock callout
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "SUCCESS","code": "200","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sent successfully to Exchange rmit-publisher-api-sfdc-dev"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    public class iPaasDeltaLoadCalloutServiceMock2 implements HttpCalloutMock {
        // implement http mock callout
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "ERROR","code": "400","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sending Failed"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }
        
    }*/
    static testMethod void commPrdSyncTestCases1() {
         
		// Create test data
		Id courseRecordTypeId = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
		system.debug('21CC recordTypeId ' + courseRecordTypeId);
		
		Account department = New Account(name = 'RMITU');
		Insert department;

		hed__Term__c term = New hed__Term__c();
		term.hed__Account__c = department.Id;
		Insert term;

		hed__course__c course = New hed__course__c();
		course.Name = 'Test Course';
		course.hed__Account__c = department.Id;
		course.recordTypeId = courseRecordTypeId;
		Insert course;

		system.debug('**** debug course creation ' + course);

		hed__Course_Offering__c courseOffering1 = New hed__Course_Offering__c();
		courseOffering1.name = 'Course Offering ID 1';
		courseOffering1.hed__course__c = course.Id;
		courseOffering1.hed__course__r = course;
		courseOffering1.Status__c = 'Active';
		courseOffering1.hed__Term__c = term.Id;
		courseOffering1.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering1.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering1;

		hed__Course_Offering__c courseOffering2 = New hed__Course_Offering__c();
		courseOffering2.name = 'Course Offering ID 2';
		courseOffering2.hed__course__c = course.Id;
		courseOffering2.Status__c = 'Inactive';
		courseOffering2.hed__Term__c = term.Id;
		courseOffering2.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering2.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering2;

		hed__Course_Offering__c courseOffering3 = New hed__Course_Offering__c();
		courseOffering3.name = 'Course Offering ID 3';
		courseOffering3.hed__course__c = course.Id;
		courseOffering3.Status__c = 'Active';
		courseOffering3.hed__Term__c = term.Id;
		courseOffering3.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering3.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering3;

		// Read data from DB
		list<hed__Course_Offering__c> testDataFromDB = [SELECT Id, name, hed__course__c,
		                              hed__Start_Date__c,
		                              hed__End_Date__c,
		                              hed__course__r.name,
		                              //hed__course__r.price__c,
		                              hed__course__r.recordTypeId,
		                              status__c 
		                              FROM hed__Course_Offering__c 
		                              WHERE hed__course__r.recordTypeId = :courseRecordTypeId];
         Test.startTest();
         Boolean testObj = BatchJobInvokeController.commPrdSync1();
         Test.stopTest();
         System.assertEquals(true, testObj);
    }
    
       static testMethod void commPrdSyncTestCases2() {
         
		// Create test data
		
		List<Account> accountsToCreate = new List<Account>();
		Account account21CC = New Account(name = '21CC');
		accountsToCreate.add(account21CC);

		Account accountRMITOnline = New Account(name = 'RMIT Online');
		accountsToCreate.add(accountRMITOnline);

		insert accountsToCreate;

		List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();
        Config_Data_Map__c account21CCCustomSetting = new Config_Data_Map__c();
        account21CCCustomSetting.Name = 'AccountID_21CC';
        account21CCCustomSetting.Config_Value__c = account21CC.id; 
        configurations.add(account21CCCustomSetting);

        Config_Data_Map__c accountRMITOnlineCustomSetting = new Config_Data_Map__c();
        accountRMITOnlineCustomSetting.Name = 'AccountID_RMITOnline';
        accountRMITOnlineCustomSetting.Config_Value__c = accountRMITOnline.id; 
        configurations.add(accountRMITOnlineCustomSetting);

		insert configurations;

		List<cspmb__Add_On_Price_Item__c> priceItems = new List<cspmb__Add_On_Price_Item__c>();
        cspmb__Add_On_Price_Item__c addOn1 = new cspmb__Add_On_Price_Item__c();
		addOn1.name = 'All';
		priceItems.add(addOn1);

		cspmb__Add_On_Price_Item__c addOn2 = new cspmb__Add_On_Price_Item__c();
		addOn2.name = 'Assessment Only';
		priceItems.add(addOn2);


		cspmb__Add_On_Price_Item__c addOn3 = new cspmb__Add_On_Price_Item__c();
		addOn3.name = 'Content Only';
		priceItems.add(addOn3);

		insert priceItems;
           
         Test.startTest();
         Boolean testObj = BatchJobInvokeController.commPrdSync2();
         Test.stopTest();
         System.assertEquals(true, testObj);
    }
  
 
}