/**
* -------------------------------------------------------------------------------------------------+
* @description This test class validates the Close of Enrolment batch class
* --------------------------------------------------------------------------------------------------
* @author         Sajitha Deivasikamani
* @version        0.0
* @created        2019-03-13
* @modified       2019-03-14
* @modified by    Anupam Singhal
* @Reference      RM-2539
* -------------------------------------------------------------------------------------------------+
*/

@isTest
public class Batch_CloseOfEnrolment_Test {
    
    @testSetup
    private static void batchData(){        
        List<Lead> leadList = new List<Lead>();
        List<hed__Course__c> courseList = new List<hed__Course__c>();
        List<hed__Course_Offering__c> courseOffList = new List<hed__Course_Offering__c>();
        List<hed__Course_Offering__c> courseOffList1 = new List<hed__Course_Offering__c>();
        Id leadRT = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student - RMIT Online').RecordTypeId;
        Id departmentRMITOrt = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        
        Account departmentRMITO = new Account(Name='RMIT Online',RecordTypeId=departmentRMITOrt);
        insert departmentRMITO;
        
        Account termAccount = new Account(Name='Used for Term session',RecordTypeId=departmentRMITOrt);
        insert termAccount;
        
        hed__Term__c termsession = new hed__Term__c(hed__Account__c = termAccount.id);
        insert termsession;
        //Uncomment the record type when deploying to SIT/UAT
        Id courseRMITORT = Schema.SObjectType.hed__Course__c.RecordTypeInfosByName.get('RMIT Online').RecordTypeId;
        for(Integer i=0;i<15;i++){
            hed__Course__c courseRec = new hed__Course__c();
            courseRec.Name = 'Test course Name'+i;
            courseRec.hed__Course_ID__c = 'Test cId'+i;
            courseRec.hed__Account__c = departmentRMITO.Id;
            //Uncomment the record type when deploying to SIT/UAT
            courseRec.RecordTypeId = courseRMITORT;
            courseList.add(courseRec);
        }
        	 insert courseList;
        
        for(Integer i=0;i<10;i++){
            Datetime dt = Datetime.now().addDays(5);
            Datetime dt1 = dt.addDays(10);            
            hed__Course_Offering__c courseOffRec = new hed__Course_Offering__c();
            courseOffRec.hed__Course__c = courseList[i].Id;
            courseOffRec.hed__Term__c = termsession.Id;           
            courseOffRec.hed__Start_Date__c = dt.date();
            courseOffRec.Last_Day_to_Add__c = dt1.date();
            courseOffList.add(courseOffRec);
        }
        
        for(Integer i=0;i<5;i++){
            Datetime dt = Datetime.now();
            Datetime dt1 = dt.addDays(3);            
            hed__Course_Offering__c courseOffRec = new hed__Course_Offering__c();
            courseOffRec.hed__Course__c = courseList[i].Id;
            courseOffRec.hed__Term__c = termsession.Id;           
            courseOffRec.hed__Start_Date__c = dt.date();
            courseOffRec.Last_Day_to_Add__c = dt1.date();
            courseOffList.add(courseOffRec);
        }
        for(Integer i=10;i<15;i++){
            Datetime dt = Datetime.now().addDays(-2);
            Datetime dt1 = dt.addDays(1);            
            hed__Course_Offering__c courseOffRec = new hed__Course_Offering__c();
            courseOffRec.hed__Course__c = courseList[i].Id;
            courseOffRec.hed__Term__c = termsession.Id;           
            courseOffRec.hed__Start_Date__c = dt.date();
            courseOffRec.Last_Day_to_Add__c = dt1.date();
            courseOffList.add(courseOffRec);
        }
        
        insert courseOffList;
        
        for(Integer i=0;i<15;i++){
            Lead leadRec = new Lead();
            leadRec.FirstName = 'SAJD First Name'+i;
            leadRec.LastName = 'SAJD Last Name_'+i;
            leadRec.Company = 'Test Company'+i;
            leadRec.Interested_Course_ID__c = 'Test cId'+i;
            leadRec.Course__c = courseList[i].Id;
            leadRec.RecordTypeId = leadRT;
            leadRec.Portfolio__c = 'Future Skills';                       
            leadList.add(leadRec);
        }   
          insert leadList; 
           
        for(Integer i=0;i<10;i++){
            Datetime dt = Datetime.now().addDays(3);
            Datetime dt1 = dt.addDays(5);            
            hed__Course_Offering__c courseOffRec = new hed__Course_Offering__c();
            courseOffRec.hed__Course__c = courseList[i].Id;
            courseOffRec.hed__Term__c = termsession.Id;           
            courseOffRec.hed__Start_Date__c = dt.date();
            courseOffRec.Last_Day_to_Add__c = dt1.date();
            courseOffList1.add(courseOffRec);
        }
        insert courseOffList1;
        
    }
    private static void createCustomSetting(){
        RMITO_Lead_Auto_Convert__c newCustomSettingValues = new RMITO_Lead_Auto_Convert__c();
        newCustomSettingValues.BatchExceptionEmailAddresses__c = 'test@test.com';
        newCustomSettingValues.BatchFinishEmailAddresses__c = 'test@test.com';
        newCustomSettingValues.Name = 'COEBatchConfiguration';
        newCustomSettingValues.EnableSendEmailBatchException__c = true;
        newCustomSettingValues.EnableSendEmailBatchFinish__c = true;
        newCustomSettingValues.BatchRunFrequency__c = '1';
        newCustomSettingValues.CourseConnectionProcessedFlag__c = false;
        insert newCustomSettingValues;
    }
    
    static testMethod void checkBatch(){
        createCustomSetting();
        Test.startTest();        
        Batch_CloseOfEnrolment batchEnrol = new Batch_CloseOfEnrolment();
        Database.executeBatch(batchEnrol);         
        Test.stopTest();        
        DateTime actualDate = [SELECT Close_of_Enrolment__c FROM Lead WHERE LastName = 'SAJD Last Name_0' LIMIT 1].Close_of_Enrolment__c;
        Id courseId = [SELECT Course__c FROM Lead WHERE LastName = 'SAJD Last Name_0'  LIMIT 1].Course__c;
        DateTime expectedDate = [SELECT hed__Start_Date__c FROM hed__Course_Offering__c Where hed__Course__c =: courseId ORDER BY hed__Course__c, Last_Day_To_Add__c, hed__Start_Date__c LIMIT 1].hed__Start_Date__c;
        System.assertEquals(expectedDate, actualDate,'The Close of Enrolment Dates are not Equal');
    }
}