/*********************************************************************************
*** @Class			    : Batch_CreateUpdateAffiliations_Helper
*** @Author		        : Mehreen Mansoor
*** @Requirement     	: Batch for Creating Alumni Affiliations for existing students
*** @Created date    	: 01/03/2019
**********************************************************************************/
/*****************************************************************************************
*** @About Class
*** This is a helper class for Batch_CreateUpdateAffiliations batch job to create Alumni affiliations
*** This batch job will run once to create Alumni Affiliations
*****************************************************************************************/

public with sharing class Batch_CreateUpdateAffiliations_Helper {
    List<Contact> contactList; 
    public static Map<Id,List <hed__Program_Enrollment__c> > contactToEnrolMap = new Map<Id,List <hed__Program_Enrollment__c>>();
    public static Map<Id,List <hed__Affiliation__c> > contactToAffilMap = new Map<Id,List <hed__Affiliation__c>>();
    public static Map < String, hed__Affiliation__c > ukandaff = new Map < String, hed__Affiliation__c > ();
    public static List<hed__Affiliation__c> lstAffToUpdate= new List<hed__Affiliation__c>();
    public static Id alumnirecodTypeID = Schema.SObjectType.hed__Affiliation__c.RecordTypeInfosByName.get('Alumni').RecordTypeId;
    public static Id studentrecodTypeID = Schema.SObjectType.hed__Affiliation__c.RecordTypeInfosByName.get('Student').RecordTypeId;
    static final String COMPLETED = 'CM';
    static final String ACTIVE = 'AC';
    static final String LEAVE = 'LA';
    static final List<String> ALL_STATUS = new list<String> {'CM','AC','LA'}; 
        public Batch_CreateUpdateAffiliations_Helper (List <Contact> scope) {
            contactList = new List<Contact>();
            contactList = scope;
        }
    
    /*The method creates Alumni Affiliations between students and schools*/
    public void affiliations (){
        getAllContactsEnrols(contactList);
        getAllContactsAffils(contactList);
        
        for(Id id: contactToEnrolMap.keyset())
        {
            
            List <hed__Program_Enrollment__c> listEnrls=  contactToEnrolMap.get (id);
            if(listEnrls !=null){
                getEnrolMaps(listEnrls,id);
            }
            
            
            
            
        }
        
        
        if (!ukandaff.isEmpty()) {
            System.debug('Map of Values to be upserted:------> \t\t\t' + ukandaff.size());
            System.debug(JSON.serializePretty(ukandaff.values()));
            Database.upsert(ukandaff.values(),hed__Affiliation__c.PE_Affilliation_Unique_Key__c,false);
            update lstAffToUpdate;
        }
    }
    
    public static void createUpdateAlumniAffil(Map <id,id> mapAccountContact,Map<id,hed__Program_Enrollment__c> pE,Map <id,id> mapAccountContactActive) {
        
        for(Id id: mapAccountContact.keyset()){
            hed__Affiliation__c affiliation = new hed__Affiliation__c();
            string accountParentId=id;
            string contactRecordid=mapAccountContact.get(id);
            string getAff = accountParentId + '' + contactRecordid;
            affiliation.hed__Account__c = accountParentId;
            affiliation.hed__Contact__c = contactRecordid;
            affiliation.hed__StartDate__c = pE.get(id).Effective_Date__c;
            affiliation.PE_Affilliation_Unique_Key__c = accountParentId + 'Alumni' + contactRecordId;
            affiliation.hed__Role__c = 'Alumni';
            affiliation.RecordTypeId = alumnirecodTypeID;
            affiliation.hed__Status__c = 'Current';
            if (affiliation.PE_Affilliation_Unique_Key__c != null) {
                ukandaff.put(affiliation.PE_Affilliation_Unique_Key__c, affiliation);
            }
            
            /*updates Student Affiliation between same school and contact and sets end date as program completion date of the first completed program*/
            updateStudAffiliation(accountParentId,contactRecordid,pE.get(id).Effective_Date__c,mapAccountContactActive); 
        }
    }
    public static void updateStudAffiliation( string accountParentId, string contactRecordid,date endDate,Map <id,id> mapAccountContactActive) {
        /*Check if student has current enrolment with that school*/
        
        /*Only update student affiliation for that school and contact for which you created Alumin Affiliation if a student is not currently enrolled in any program */
        hed__Affiliation__c studAffil=new hed__Affiliation__c();
        if(!mapAccountContactActive.containsKey(accountParentId)){
            
            studAffil=getStudAffil(accountParentId,contactRecordid);
            
            if(studAffil!=null){
                studAffil.hed__Status__c = 'Former';
                
                studAffil.hed__EndDate__c =endDate;
                
                lstAffToUpdate.add(studAffil);}
            
        }
        
    }
    
    
    
    
    public static void getAllContactsEnrols(List <contact> contactList){
        
        for(hed__Program_Enrollment__c enrl:[select id, hed__Contact__c,Action_Date__c,Academic_Program__c,hed__Account__r.ParentId,Effective_Date__c, Program_Value__c, Program_Action__c, Program_Status__c,hed__Graduation_Year__c from hed__Program_Enrollment__c where hed__Contact__c  IN :contactlist and Program_Status__c IN:ALL_STATUS and hed__Account__r.ParentId !=null order by hed__Account__r.ParentId,Effective_Date__c ASC])
        {
            if (!contactToEnrolMap.containsKey(enrl.hed__Contact__c)) {
                
                List <hed__Program_Enrollment__c> enrolments = new List<hed__Program_Enrollment__c>();
                enrolments.add(enrl);
                
                contactToEnrolMap.put(enrl.hed__Contact__c,enrolments);
            }else
            {
                
                contactToEnrolMap.get(enrl.hed__Contact__c).add(enrl);
            }
            
        }
    }
    
    public static void getAllContactsAffils(List <contact> contactList){
        for( hed__Affiliation__c hedAff : [SELECT
                                           Id,
                                           hed__Contact__c,
                                           hed__Account__c,
                                           PE_Affilliation_Unique_Key__c
                                           FROM hed__Affiliation__c
                                           WHERE RecordTypeId =: studentrecodTypeID
                                           AND hed__Contact__c IN : contactlist
                                           AND hed__Contact__c != null
                                           AND hed__Account__c != null
                                          ]){
                                              if (!contactToAffilMap.containsKey(hedAff.hed__Contact__c)) {
                                                  
                                                  List<hed__Affiliation__c> affiliations = new List<hed__Affiliation__c>();
                                                  affiliations.add(hedAff);
                                                  
                                                  contactToAffilMap.put(hedAff.hed__Contact__c,affiliations);
                                              }else
                                              {
                                                  
                                                  contactToAffilMap.get(hedAff.hed__Contact__c).add(hedAff);
                                              }
                                              
                                          }
    }
    
    public static boolean checkIfStudAffilExists(id account,id contact)
    {
        List<hed__Affiliation__c> studAffil=new List<hed__Affiliation__c>();
        
        boolean exists=false;
        studAffil= contactToAffilMap.get(contact);
        if (studAffil !=null){
            for (hed__Affiliation__c affil :  studAffil){
                {
                    
                    String strId = Id.valueOf(affil.hed__Account__c);
                    if(strId==account){
                        exists=true;
                        
                        
                        
                    }}}}return exists;
    }
    
    
    
    public static hed__Affiliation__c getStudAffil(id account,id contact)
    {
        List<hed__Affiliation__c> studAffil=new List<hed__Affiliation__c>();
        hed__Affiliation__c studAffiliation=new hed__Affiliation__c();
        
        studAffil= contactToAffilMap.get(contact);
        if (studAffil !=null){
            for (hed__Affiliation__c affil :  studAffil){
                {
                    
                    String strId = Id.valueOf(affil.hed__Account__c);
                    if(strId==account){
                        studAffiliation=affil;
                        
                        
                    }}}}
        return studAffiliation;
    }
    
    
    
    
    public static void getEnrolMaps(List <hed__Program_Enrollment__c> listEnrls,id con){
        
        
        Map < id, id> mapAccountContact=new Map <id,id>();
        Map < id, id> mapAccountContactActive=new Map <id,id>();
        Map < id,hed__Program_Enrollment__c> mapAccountEnrol=new Map<id,hed__Program_Enrollment__c>();
        for (hed__Program_Enrollment__c progs :  listEnrls){
            
            if(progs.Program_Status__c==COMPLETED && checkIfStudAffilExists(progs.hed__Account__r.ParentId,con)==true){
                
                if(!mapAccountEnrol.containsKey(progs.hed__Account__r.ParentId)){
                    
                    mapAccountEnrol.put(progs.hed__Account__r.ParentId,progs);
                }
                if(!mapAccountContact.containsKey(progs.hed__Account__r.ParentId)){
                    mapAccountContact.put(progs.hed__Account__r.ParentId,con);
                    
                }}
            if(progs.Program_Status__c==ACTIVE || progs.Program_Status__c==LEAVE ){
                mapAccountContactActive.put(progs.hed__Account__r.ParentId,con);
            }
            
            
            
            
        }
        createUpdateAlumniAffil(mapAccountContact,mapAccountEnrol,mapAccountContactActive);
        
    }}