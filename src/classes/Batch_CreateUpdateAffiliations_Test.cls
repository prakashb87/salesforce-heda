/*******************************************************************************
 *** @TestClassName     : Batch_CreateUpdateAffiliations_Test
 *** @Author		    : Mehreen Mansoor
 *** @Requirement     	: Create Alumni affiliations for Completed program enrolments 
 *** @Created date    	: 04/03/2019
 ********************************************************************************/

@isTest
public class Batch_CreateUpdateAffiliations_Test {
    	@testSetup 
    	static void setupData() {
		List<Account> programList = new List<Account>();
		List<hed__Program_Enrollment__c> programEnrolmentList = new List<hed__Program_Enrollment__c>();
		List<hed__Course_Enrollment__c> courseEnrolmentList = new List<hed__Course_Enrollment__c>();
            List<hed__Affiliation__c> studaffil=new List<hed__Affiliation__c>();
		id institutioRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId();
		id programRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
             Id studentrecodTypeID = Schema.SObjectType.hed__Affiliation__c.RecordTypeInfosByName.get('Student').RecordTypeId;
		
		Account institution = new Account(
			name = 'RMITU',
			recordtypeid = institutioRecordTypeId
		);
		
		insert institution;
		
		Account school = new Account(
			name = 'School',
			recordtypeid = institutioRecordTypeId
		);
		
		insert school;
		
		for (Integer i = 0; i<10; i++) {
			programList.add(new Account(
				Name = 'Program'+String.valueof(i),
				AccountNumber = String.valueof(14120+i),
				Effective_Date__c = Date.today()-30,
				parentid = school.id,
				recordtypeid = programRecordTypeId,
				Academic_Institution__c = institution.id
			));
		
			programList.add(new Account(
				Name = 'Program'+String.valueof(i),
				AccountNumber = String.valueof(14120+i),
				Effective_Date__c = Date.today(),
				parentid = school.id,
				recordtypeid = programRecordTypeId,
				Academic_Institution__c = institution.id
			));
			
			programList.add(new Account(
				Name = 'Program'+String.valueof(i),
				AccountNumber = String.valueof(14120+i),
				Effective_Date__c = Date.today()-10,
				parentid = school.id,
				recordtypeid = programRecordTypeId,
				Academic_Institution__c = institution.id
			));
			
			
		}
		
		insert programList;
		
		List<Contact> studentList = new List<Contact>();
		
		for (Integer i = 0; i<10; i++) {
		    studentList.add(new Contact(
		    	firstname = 'firstname',
		    	lastname = 'lastname',
		    	Student_ID__c = string.valueOf(i)
		    ));
		} 
		
		insert studentList;
		
		for(Contact con : studentList){
			programEnrolmentList.add(new hed__Program_Enrollment__c(
				hed__Account__c = programlist[0].id,
				Effective_Date__c = Date.today()-30,
				Program_Status__c='CM',
				hed__Contact__c=con.id
			));
		}
		
		
		
		insert programEnrolmentList;
            
            for(Contact con : studentList){
			studaffil.add(new hed__Affiliation__c(
				hed__Account__c = school.id,
				
				RecordTypeId = studentrecodTypeID,
				hed__Contact__c=con.id
			));
		}
		
		
		
		insert studaffil;
		
	}
    
    @isTest
    static void testCleanUp(){
    
	    id institutioRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId();
		id programRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
        id alumnirecodTypeID = Schema.SObjectType.hed__Affiliation__c.RecordTypeInfosByName.get('Alumni').RecordTypeId;
		
	    system.assertEquals(30,[SELECT count() FROM Account WHERE recordTypeId = : programRecordTypeId],'Programs were not created');
	    system.assertEquals(10,[SELECT count() FROM hed__Program_Enrollment__c],'Program Enrolment were not created');
	    system.assertEquals(10,[SELECT count() FROM Contact where firstname = 'firstname'],'Students were not created');

	    Test.startTest();
	    
        DataBase.executeBatch(new Batch_CreateUpdateAffiliations(),200);
	    
	    Test.stopTest();
	    
	    system.assertEquals(10,[SELECT count() FROM hed__Affiliation__c WHERE recordTypeId = : alumnirecodTypeID],'Affiliations not created');
	   
    }
}