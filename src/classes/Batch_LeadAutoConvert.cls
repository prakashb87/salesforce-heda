/**
* -------------------------------------------------------------------------------------------------+
* This batch matches the Lead and Contact, converts the matched lead
* --------------------------------------------------------------------------------------------------
* @author         Anupam Singhal
* @version        1.0
* @created        2019-02-04
* @modified       2019-02-04
* @modified by    Sajitha
* @Reference      RM-2397,RM-2669
* -------------------------------------------------------------------------------------------------+
*/
public class Batch_LeadAutoConvert implements Database.Batchable<sObject>, Schedulable {
    
        
    public Database.QueryLocator start(Database.BatchableContext contactContext){ 
        String queryString = 'SELECT '+
            'FirstName,'+
            'LastName,'+
            'Id,'+
            'Email,'+ 
            'Mobile_Formatted2__c,'+
            'CreatedDate,'+
            'Mobile_Formatted__c, '+
            'RecordTypeId '+ 
            'FROM Lead '+
            'WHERE RecordType.DeveloperName = \'Prospective_Student_RMIT_Online\' '+
            'AND isConverted = false '+
            'AND (Email != null OR Mobile_Formatted2__c != null OR Mobile_Formatted__c != null ) '+           
            'AND Portfolio__c != \'Future Degrees\' '+ //Refer-RM-2669
            'AND SAMS_Program_name__c = null'; //Refer-RM-2669
        return Database.getQueryLocator(queryString);       
    }    
    
    public void execute(Database.BatchableContext contactContext,List <Lead> scope )
    {
        Batch_LeadAutoConvert_helper.processMatching(scope);   
    }
    public void finish(Database.BatchableContext contactContext){
        RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues('BatchNotificationConfig');
        AsyncApexJob a = [SELECT Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:contactContext.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = csRmito.BatchFinishEmailAddresses__c.split(',');
        mail.setToAddresses(toAddresses);
        mail.setSubject('Match Merge Batch ' + a.Status);
        mail.setPlainTextBody('Records processed ' + a.TotalJobItems +   'with '+ a.NumberOfErrors + ' failures.');    
        if(a.NumberOfErrors > 0 && csRmito.EnableSendEmailBatchFinish__c == true){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    public void execute(SchedulableContext ctx){
        System.debug('Hi There!');
    }  
}