/**
* -------------------------------------------------------------------------------------------------+
* @description This test class validates the Lead Auto convert batch helper class
* --------------------------------------------------------------------------------------------------
* @author         Sai Venkata Ajay Kumar Potti
* @version        0.0
* @created        2019-04-17
* @modified       2019-04-22
* @modified by    Anupam Singhal
* @Reference      RM-2401
* -------------------------------------------------------------------------------------------------+
*/
@isTest
public class Batch_LeadAutoConvert_Test {
    private static final Id RMITOLEADRT = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student - RMIT Online').RecordTypeId;
    static testMethod void checkBatch() {
        
        List <Contact> students = new List <Contact>();
        List <hed__Course_Enrollment__c> courseConnections = new List<hed__Course_Enrollment__c>();
        List <Lead> enquiries = new List <Lead>();
        List <hed__Term__c> termsessions = new List <hed__Term__c>();
        List <hed__Course__c> courses = new List<hed__Course__c>();
        List <hed__Course_Offering__c> courseOfferings = new List <hed__Course_Offering__c>();
        List<Id> leadid = new List<id>();
        
        
        Id departmentRMITOrt = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        id courseRMITORt = Schema.SObjectType.hed__Course__c .RecordTypeInfosByName.get('RMIT Online').RecordTypeId;
        
        createCustomSetting();
        
        students = createStudentData();
        
        Account departmentRMITO = new Account(Name='RMIT Online',RecordTypeId=departmentRMITOrt);
        insert departmentRMITO;
        
        Account termAccount = new Account(Name='Used for Term session',RecordTypeId=departmentRMITOrt);
        insert termAccount;
        //
        hed__Course__c course12 = new hed__Course__c(Name='Test RMITO Course',hed__Account__c=departmentRMITO.Id,RecordTypeId=courseRMITORt);
        insert course12;
        
        
        hed__Term__c termsession = new hed__Term__c(hed__Account__c = termAccount.id);
        insert termsession;
        
        hed__Course_Offering__c co = new hed__Course_Offering__c(Name='Test CO 1',hed__Course__c = course12.id, hed__Term__c = termsession.Id);
        insert co;
        
        enquiries = createMobileMatchLeadData();
        System.debug('Mobile_Lead_Data'+enquiries);
        for(Lead addId: enquiries){
        	leadid.add(addId.Id);
        }
        for(Integer j=0;j<2;j++){
            Lead enquiry = new Lead();
            enquiry.FirstName = 'Student x fname'+j;
            enquiry.LastName = 'Student x lname'+j;
            enquiry.Company = 'X Lname inc'+j;
            enquiry.RecordTypeId = RMITOLEADRT;
            enquiry.LeadSource = 'Webform';
            enquiry.Email = 'studentfnamelname'+j+'@rmittest.com';
			enquiry.Portfolio__c = 'Future Skills';
            enquiry.CreatedDate = DateTime.newInstance(2019,01,12);
            leadid.add(enquiry.Id);
            enquiries.add(enquiry);
        }
        for(Integer j=2;j<4;j++){
            Lead enquiry = new Lead();
            enquiry.FirstName = 'Student x fname'+j;
            enquiry.LastName = 'Student x lname'+j;
            enquiry.Company = 'X Lname inc'+j;
            enquiry.RecordTypeId = RMITOLEADRT;
            enquiry.LeadSource = 'Webform';
            enquiry.Portfolio__c = 'Future Skills';
            enquiry.Email = 'studentfnamelnamedonotConvert'+j+'@rmittest.com';
            enquiry.CreatedDate = DateTime.newInstance(2019,01,12);
            leadid.add(enquiry.Id);
            enquiries.add(enquiry);
        }
        
        //Positive scenario leads for Accredited Lead conversion ie Portfolio != Future Degrees and SAMS Program Name ==  null
        /*for(Integer k=40;k<50;k++){
            Lead accrLeadPositive = new Lead();
            accrLeadPositive.FirstName = 'Student accrp fname'+k;
            accrLeadPositive.LastName = 'Student accrp lname'+k;
            accrLeadPositive.Company = 'X Lname inc'+k;
            accrLeadPositive.RecordTypeId = RMITOLEADRT;
            accrLeadPositive.LeadSource = 'Webform';
            accrLeadPositive.Email = 'accrpfnamelname'+k+'@rmittest.com';
            accrLeadPositive.Portfolio__c = 'Future Skills';
            accrLeadPositive.CreatedDate = DateTime.newInstance(2019,01,12);
            leadid.add(accrLeadPositive.Id);
            enquiries.add(accrLeadPositive);//----
        }*/
        
        //Negative scenario leads for Accredited Lead conversion ie Portfolio = Future Degrees and SAMS Program Name ==  null
        for(Integer x=4;x<6;x++){
            Lead accrLeadNegative= new Lead();
            accrLeadNegative.FirstName = 'Student x fname'+x;
            accrLeadNegative.LastName = 'Student x lname'+x;
            accrLeadNegative.Company = 'X Lname inc'+x;
            accrLeadNegative.RecordTypeId = RMITOLEADRT;
            accrLeadNegative.LeadSource = 'Webform';
            accrLeadNegative.Email = 'accrnfnamelname'+x+'@rmittest.com';
            accrLeadNegative.Portfolio__c = 'Future Skills';
            accrLeadNegative.CreatedDate = DateTime.newInstance(2019,01,12);
            //accrLeadNegative.Portfolio__c='Future Degrees';	
            accrLeadNegative.Course__c = course12.Id;
            leadid.add(accrLeadNegative.Id);
            enquiries.add(accrLeadNegative);
        }
        
        //Positive scenario leads for Accredited Lead conversion ie SAMS Program Name !=  null
        for(Integer y=6;y<8;y++){
            Lead accrLeadNegative= new Lead();
            accrLeadNegative.FirstName = 'Student x fname'+y;
            accrLeadNegative.LastName = 'Student x lname'+y;
            accrLeadNegative.Company = 'X Lname inc'+y;
            accrLeadNegative.RecordTypeId = RMITOLEADRT;
            accrLeadNegative.LeadSource = 'Webform';
            accrLeadNegative.Email = 'samsfnamelname'+y+'@rmittest.com';
            accrLeadNegative.Portfolio__c = 'Future Degrees';
            accrLeadNegative.CreatedDate = DateTime.newInstance(2019,01,12);
            accrLeadNegative.SAMS_Program_name__c=departmentRMITO.Id;	
            leadid.add(accrLeadNegative.Id);
            enquiries.add(accrLeadNegative);
        }
        
        insert enquiries;
        
        for(Contact rmitoStudent: students){
            hed__Course_Enrollment__c courseConnection = new hed__Course_Enrollment__c();
            courseConnection.hed__Contact__c = rmitoStudent.Id;
            //courseConnection.Product_Line__c = 'RMIT Online';
            courseConnection.CreatedDate = System.now();
            courseConnection.hed__Course_Offering__c = co.Id;
            courseConnections.add(courseConnection);
    }
        insert courseConnections;
        
        test.startTest();
        Batch_LeadAutoConvert blac = new Batch_LeadAutoConvert();
        Id batchId = Database.executeBatch(blac);    
        test.stopTest();
        for(Lead convertedLead: [SELECT isConverted FROM Lead WHERE Id IN: leadid]){
            System.AssertEquals(true,convertedLead.isConverted,'Leads were not connverted');
        }
        
        for(Lead convertedLead: [SELECT isConverted FROM Lead WHERE Id IN: leadid AND isConverted = false]){
            System.AssertEquals(false,convertedLead.isConverted,'Leads were connverted');
        }
           
    }
    
    private static List<Lead> createMobileMatchLeadData(){
    	List<Lead> enquiries = new List<Lead>();
    	Lead accrLeadEmailMatch1 = new Lead();
        accrLeadEmailMatch1.FirstName = 'Lead Email match fname';
        accrLeadEmailMatch1.LastName = 'Lead Email lname';
        accrLeadEmailMatch1.Company = 'X Lname inc';
        accrLeadEmailMatch1.RecordTypeId = RMITOLEADRT;
        accrLeadEmailMatch1.LeadSource = 'Webform';
        accrLeadEmailMatch1.Email = 'testEmailContact@rmittest.com';
        accrLeadEmailMatch1.Phone = '989898989';
        accrLeadEmailMatch1.Portfolio__c = 'Future Skills';
        accrLeadEmailMatch1.SAMS_Program_name__c = null;
        accrLeadEmailMatch1.isConverted = false;
        accrLeadEmailMatch1.CreatedDate = DateTime.newInstance(2019,01,12);
        //leadid.add(accrLeadEmailMatch1.Id);
        enquiries.add(accrLeadEmailMatch1);
        
        Lead accrLeadMobileFormattedMatch = new Lead();
        accrLeadMobileFormattedMatch.FirstName = 'Lead Email match fname';
        accrLeadMobileFormattedMatch.LastName = 'Lead Email lname';
        accrLeadMobileFormattedMatch.Company = 'X Lname inc';
        accrLeadMobileFormattedMatch.RecordTypeId = RMITOLEADRT;
        accrLeadMobileFormattedMatch.LeadSource = 'Webform';        
        accrLeadMobileFormattedMatch.Phone = '449898989';
        accrLeadMobileFormattedMatch.Portfolio__c = 'Future Skills';
        accrLeadMobileFormattedMatch.SAMS_Program_name__c = null;
        accrLeadMobileFormattedMatch.isConverted = false;
        accrLeadMobileFormattedMatch.CreatedDate = DateTime.newInstance(2019,01,12);
        //leadid.add(accrLeadEmailMatch1.Id);
        enquiries.add(accrLeadMobileFormattedMatch);
        
        Lead accrLeadMobileMatch2 = new Lead();
        accrLeadMobileMatch2.FirstName = 'Lead Email match fname';
        accrLeadMobileMatch2.LastName = 'Lead Email lname';
        accrLeadMobileMatch2.Company = 'X Lname inc';
        accrLeadMobileMatch2.RecordTypeId = RMITOLEADRT;
        accrLeadMobileMatch2.LeadSource = 'Webform';
        accrLeadMobileMatch2.Email = 'testContact@rmittest.com';
        accrLeadMobileMatch2.Portfolio__c = 'Future Skills';
        accrLeadMobileMatch2.Phone = '9898989890';
        accrLeadMobileMatch2.SAMS_Program_name__c = null;
        accrLeadMobileMatch2.isConverted = false;
        accrLeadMobileMatch2.CreatedDate = DateTime.newInstance(2019,01,12);
        //leadid.add(accrLeadMobileMatch2.Id);
        enquiries.add(accrLeadMobileMatch2);
        
        Lead accrLeadMobileMatch3 = new Lead();
        accrLeadMobileMatch3.FirstName = 'Lead Email match fname';
        accrLeadMobileMatch3.LastName = 'Lead Email lname';
        accrLeadMobileMatch3.Company = 'X Lname inc';
        accrLeadMobileMatch3.RecordTypeId = RMITOLEADRT;
        accrLeadMobileMatch3.LeadSource = 'Webform';
        accrLeadMobileMatch3.Email = 'testContact123@rmittest.com';
        accrLeadMobileMatch3.Portfolio__c = 'Future Skills';
        accrLeadMobileMatch3.Phone = '9898989800';
        accrLeadMobileMatch3.SAMS_Program_name__c = null;
        accrLeadMobileMatch3.isConverted = false;
        accrLeadMobileMatch3.CreatedDate = DateTime.newInstance(2019,01,12);
        //leadid.add(accrLeadMobileMatch3.Id);
        enquiries.add(accrLeadMobileMatch3);
        
        Lead accrLeadMobileMatch4 = new Lead();
        accrLeadMobileMatch4.FirstName = 'Lead negative fname';
        accrLeadMobileMatch4.LastName = 'Lead lname';
        accrLeadMobileMatch4.Company = 'X Lname inc5';
        accrLeadMobileMatch4.RecordTypeId = RMITOLEADRT;
        accrLeadMobileMatch4.LeadSource = 'Webform';
        accrLeadMobileMatch4.Email = 'testContact1243@rmittest.com';
        accrLeadMobileMatch4.Portfolio__c = 'Future Degrees';
        accrLeadMobileMatch4.Phone = '9898960';
        accrLeadMobileMatch4.SAMS_Program_name__c = null;
        accrLeadMobileMatch4.isConverted = false;
        accrLeadMobileMatch4.CreatedDate = DateTime.newInstance(2019,01,12);
        //leadid.add(accrLeadMobileMatch4.Id);
        //
        Lead accrLeadMobileMatch5 = new Lead();
        accrLeadMobileMatch5.FirstName = 'Lead negative fname';
        accrLeadMobileMatch5.LastName = 'Lead lname';
        accrLeadMobileMatch5.Company = 'X Lname inc5';
        accrLeadMobileMatch5.RecordTypeId = RMITOLEADRT;
        accrLeadMobileMatch5.LeadSource = 'Webform';
        accrLeadMobileMatch5.Email = 'testContact1243@rmittest.com';
        accrLeadMobileMatch5.Portfolio__c = 'Future Degrees';
        accrLeadMobileMatch5.Phone = '+045677680';
        accrLeadMobileMatch5.SAMS_Program_name__c = null;
        accrLeadMobileMatch5.isConverted = false;
        accrLeadMobileMatch5.CreatedDate = DateTime.newInstance(2019,01,12);
        enquiries.add(accrLeadMobileMatch5);
        return enquiries;
    }
    
    private static List<Contact> createStudentData(){
    	List <Contact> students = new List <Contact>();
    	
    	Contact cnt1 = new Contact();
        cnt1.FirstName = 'Test Email Contact';
        cnt1.LastName = 'Email Match Test';
        cnt1.Email = 'testEmailContact@rmittest.com';
        students.add(cnt1);
        
        Contact cnt2 = new Contact();
        cnt2.FirstName = 'Test Mobile Contact';
        cnt2.LastName = 'Mobile Match Test';
        cnt2.Email = 'testMobileContact@rmittest.com';
        cnt2.Mailing_Phone__c = '9898989890';
        students.add(cnt2);
        
        Contact cnt3 = new Contact();
        cnt3.FirstName = 'Test Mobile Cnt2';
        cnt3.LastName = 'Mobile Match Test2';
        //cnt3.Email = 'testMobileContact@rmittest.com';
        cnt3.Mailing_Phone__c = '9898989800';
        students.add(cnt3);
        
        Contact cnt4 = new Contact();
        cnt4.FirstName = 'Test Mobile Cnt2';
        cnt4.LastName = 'Mobile Match Test2';
        //cnt3.Email = 'testMobileContact@rmittest.com';
        cnt4.MobilePhone = '045677680';
        students.add(cnt4);
        
        Contact cnt6 = new Contact();
        cnt6.FirstName = 'Test Email Contact';
        cnt6.LastName = 'Email Match Test';
        cnt6.MobilePhone = '61449898989';
        //cnt6.Email = 'testEmailContact@rmittest.com';
        students.add(cnt6);
        
        for(Integer i=0;i<2;i++){
            Contact student = new Contact();
            student.FirstName = 'Student 01 fname'+i;
            student.LastName = 'Student 01 lname'+i;
            student.Email = 'studentfnamelname'+i+'@rmittest.com';
            students.add(student);
        } 
        
        for(Integer i=2;i<4;i++){
            Contact student = new Contact();
            student.FirstName = 'Student 02 fname'+i;
            student.LastName = 'Student 02 lname'+i;
            student.Home_Email__c = 'studentfnamelname'+i+'@rmittest.com';
            students.add(student);
        }
        
        for(Integer i=4;i<6;i++){
            Contact student = new Contact();
            student.FirstName = 'Student 03 fname'+i;
            student.LastName = 'Student 03 lname'+i;
            student.Work_Email__c = 'studentfnamelname'+i+'@rmittest.com';
            students.add(student);
        }
        
        for(Integer i=6;i<8;i++){
            Contact student = new Contact();
            student.FirstName = 'Student 04 fname'+i;
            student.LastName = 'Student 04 lname'+i;
            student.Other_Email__c = 'studentfnamelname'+i+'@rmittest.com';
            students.add(student);
        }    
           
        for(Integer i=8;i<10;i++){
            Contact student = new Contact();
            student.FirstName = 'Student 05 fname'+i;
            student.LastName = 'Student 05 lname'+i;
            student.Other_Email__c = 'accrpfnamelname'+i+'@rmittest.com';
            students.add(student);
        }
        
        /*for(Integer i=50;i<60;i++){
            Contact student = new Contact();
            student.FirstName = 'Student 06 fname'+i;
            student.LastName = 'Student 06 lname'+i;
            student.Other_Email__c = 'accrnfnamelname'+i+'@rmittest.com';
            students.add(student);
    }
        
        for(Integer i=10;i<12;i++){
            Contact student = new Contact();
            student.FirstName = 'Student 07 fname'+i;
            student.LastName = 'Student 07 lname'+i;
            student.Other_Email__c = 'samsfnamelname'+i+'@rmittest.com';
            students.add(student);
        }*/
        
        insert students;
        System.debug('Number_of_Contacts_Created'+students.size());
        return students;
    }
    
    
    private static void createCustomSetting(){
        RMITO_Lead_Auto_Convert__c newCustomSettingValues = new RMITO_Lead_Auto_Convert__c();
        newCustomSettingValues.BatchExceptionEmailAddresses__c = 'test@test.com';
        newCustomSettingValues.BatchFinishEmailAddresses__c = 'test@test.com';
        newCustomSettingValues.Name = 'BatchNotificationConfig';
        newCustomSettingValues.EnableSendEmailBatchException__c = true;
        newCustomSettingValues.EnableSendEmailBatchFinish__c = true;
        newCustomSettingValues.BatchRunFrequency__c = '1';
        newCustomSettingValues.CourseConnectionProcessedFlag__c = false;
        insert newCustomSettingValues;
    }
    
    static testMethod void checkSendEmail(){
        createCustomSetting();
        CalloutException e = new CalloutException();
        e.setMessage('This is a constructed exception for testing and code coverage');
        Test.StartTest();
        Batch_LeadAutoConvert_helper.sendEmail(e);
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.assertEquals(1, invocations, 'An email has not been sent');
    }
}