/**
* -------------------------------------------------------------------------------------------------+
* This helper processes the Contact records match them based on the Email Address and
* converts the matched lead
* --------------------------------------------------------------------------------------------------
* @author         Anupam Singhal
* @version        0.0
* @created        2019-02-04
* @modified       2019-04-22
* @modified by    Anupam Singhal
* @Reference      RM-2397,RM-2401
* -------------------------------------------------------------------------------------------------+
*/
public with sharing class Batch_LeadAutoConvert_helper {
	static final String PRODUCTLINERMITO = System.Label.Product_Line_RMITO;//Contains the Value "RMIT Online"
	static final String CONVERTMASTERLABEL = 'Qualified'; 
    private static Map<String,List <Lead>> mapemailandLead = new Map<String,List <Lead>>();
    private static Map<String,hed__Course_Enrollment__c> mapEmailCC = new Map<String,hed__Course_Enrollment__c>();
    private static Map<String,hed__Course_Enrollment__c> mapMobileCC = new Map<String,hed__Course_Enrollment__c>();
    private static Map<String,hed__Course_Enrollment__c> mapMobileFormattedCC = new Map<String,hed__Course_Enrollment__c>();
    private static List < Database.LeadConvert > leadstobeconverted = new List < Database.LeadConvert >();
    private static Database.LeadConvert lc;
    private static String convertEmailCase;
    private static String convertMobileNumber;
    private static String trimEmail;
    private static List<Lead> sameLeads;
    private static Lead leadRecord;
    private static Set<Lead> distinctLeads = new Set<Lead>();
    private static Map<Id,Lead> mapMobileLeadList = new Map<Id,Lead>();
    private static RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues('BatchNotificationConfig');
    
    private static Map<Id,hed__Course_Enrollment__c> updateCC = new  Map<Id,hed__Course_Enrollment__c>();
	
    public static void processMatching(List<Lead> scope){
        
        for(Lead iterateLead: scope){
            distinctLeads.add(iterateLead);
            
            if(String.isNotBlank(iterateLead.Mobile_Formatted2__c) || String.isNotBlank(iterateLead.Mobile_Formatted__c)){
                mapMobileLeadList.put(iterateLead.id,iterateLead);
            }
            if(String.isNotBlank(iterateLead.Email)){
                convertEmailCase = iterateLead.Email;
        	trimEmail = convertEmailCase.toLowerCase().trim();
            
                if(!mapemailandLead.containsKey(iterateLead.Email)){
        		sameLeads = new List<Lead>();
        		sameLeads.add(iterateLead);
        		
        		mapemailandLead.put(trimEmail,sameLeads);
        	}else{
                    string convertcase2 = iterateLead.Email;
        		mapemailandLead.get(trimEmail).add(iterateLead);
        	}
            }
        }
        fetchMatchingRecords();
    }
    public static void fetchMatchingRecords(){
        
       	for(hed__Course_Enrollment__c iterateCC: [SELECT 
                                                                 CreatedDate,
                                                                 hed__Contact__c,
                                              hed__Contact__r.All_Email_Field__c,
                                                  hed__Contact__r.Mobile_Formatted2__c,
                                                  hed__Contact__r.Mobile_Formatted__c,
                                              hed__Contact__r.AccountId,
                                              hed__Contact__r.OwnerId
                                                                 FROM hed__Course_Enrollment__c 
                                                  WHERE Product_Line__c =: PRODUCTLINERMITO 
                                                  AND isProcessed__c = false 
                                                  AND (hed__Contact__r.All_Email_Field__c != null 
                                                  OR hed__Contact__r.Mobile_Formatted2__c != null) 
                                                  ORDER BY CreatedDate ASC]){
                                                      if(String.isNotBlank(iterateCC.hed__Contact__r.All_Email_Field__c)){
                                                  for(String addEmail: parseString(iterateCC.hed__Contact__r.All_Email_Field__c)){
                                                      mapEmailCC.put(addEmail.toLowerCase(),iterateCC);
                                                                     }
    }
    
                                                      if(String.isNotBlank(iterateCC.hed__Contact__r.Mobile_Formatted2__c)){
                                                          mapMobileCC.put(iterateCC.hed__Contact__r.Mobile_Formatted2__c,iterateCC);
                                                          
                                                      }
                                                      if(String.isNotBlank(iterateCC.hed__Contact__r.Mobile_Formatted__c)){
                                                          mapMobileFormattedCC.put(iterateCC.hed__Contact__r.Mobile_Formatted__c, iterateCC);
                                                      }
                                                      
                                                  }
        processsEmailMatching();        
    }
    
    private static void processsEmailMatching(){
        for(String matchLeadEmail: mapemailandLead.keySet()){
            if(mapEmailCC.containsKey(matchLeadEmail)){
		            for(Lead lead: mapemailandLead.get(matchLeadEmail)){
		            	if(mapEmailCC.get(matchLeadEmail).CreatedDate > lead.CreatedDate){
                                   lc = new Database.LeadConvert();
			                lc.setLeadId(lead.Id);
                                       lc.convertedStatus = CONVERTMASTERLABEL;
                    lc.setAccountId(mapEmailCC.get(matchLeadEmail).hed__Contact__r.AccountId);
                    lc.setContactId(mapEmailCC.get(matchLeadEmail).hed__Contact__c);
			                lc.setOwnerId(mapEmailCC.get(matchLeadEmail).hed__Contact__r.OwnerId);
                                       lc.setDoNotCreateOpportunity(true);
                        mapEmailCC.get(matchLeadEmail).isProcessed__c = csRmito.CourseConnectionProcessedFlag__c;
			                updateCC.put(lead.Id,mapEmailCC.get(matchLeadEmail));
                    leadstobeconverted.add(lc);
                        mapMobileLeadList.remove(lead.Id);
                }
                                   }
            }
        }
        processsMobileFormatted2Matching();
    }
    
    private static void processsMobileFormatted2Matching(){
        for(Id matchLeadMobile: mapMobileLeadList.keySet()){           
            //String mapMobileCCKeyValue = mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c;
            if(mapMobileCC.containsKey(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c)){
                leadRecord = new Lead();
                leadRecord = mapMobileLeadList.get(matchLeadMobile);
                if(mapMobileCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c).CreatedDate > leadRecord.CreatedDate){
                    lc = new Database.LeadConvert();
                    lc.setLeadId(leadRecord.Id);
                    lc.convertedStatus = CONVERTMASTERLABEL;
                    lc.setAccountId(mapMobileCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c).hed__Contact__r.AccountId);
                    lc.setContactId(mapMobileCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c).hed__Contact__c);
                    lc.setOwnerId(mapMobileCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c).hed__Contact__r.OwnerId);
                    lc.setDoNotCreateOpportunity(true);
                    mapMobileCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c).isProcessed__c = csRmito.CourseConnectionProcessedFlag__c; //Customsetting false
                    updateCC.put(leadRecord.Id,mapMobileCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted2__c));
                    leadstobeconverted.add(lc);
                    mapMobileLeadList.remove(matchLeadMobile);
                }
            }
        }
        //processConversion(leadstobeconverted,updateCC);
        processsMobileFormattedMatching();
    }
    
    private static void processsMobileFormattedMatching(){
        for(Id matchLeadMobile: mapMobileLeadList.keySet()){
            if(mapMobileFormattedCC.containsKey(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted__c)){
                leadRecord = new Lead();
                leadRecord = mapMobileLeadList.get(matchLeadMobile);
                if(mapMobileFormattedCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted__c).CreatedDate > leadRecord.CreatedDate){
                    lc = new Database.LeadConvert();
                    lc.setLeadId(leadRecord.Id);
                    lc.convertedStatus = CONVERTMASTERLABEL;
                    lc.setAccountId(mapMobileFormattedCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted__c).hed__Contact__r.AccountId);
                    lc.setContactId(mapMobileFormattedCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted__c).hed__Contact__c);
                    lc.setOwnerId(mapMobileFormattedCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted__c).hed__Contact__r.OwnerId);
                    lc.setDoNotCreateOpportunity(true);
                    mapMobileFormattedCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted__c).isProcessed__c = csRmito.CourseConnectionProcessedFlag__c; //Customsetting false
                    updateCC.put(leadRecord.Id,mapMobileFormattedCC.get(mapMobileLeadList.get(matchLeadMobile).Mobile_Formatted__c));
                    leadstobeconverted.add(lc);
                    mapMobileLeadList.remove(matchLeadMobile);
                }
            }
                               }
        processConversion(leadstobeconverted,updateCC);
        }
    
    private static void processConversion(List<Database.LeadConvert> convertLeads,Map<Id,hed__Course_Enrollment__c> updateCCflag){
        List<hed__Course_Enrollment__c> finalCCupdate = new List<hed__Course_Enrollment__c>();
    	
        
        Database.DMLOptions allowDuplicate = new Database.DMLOptions();
        
        allowDuplicate.DuplicateRuleHeader.AllowSave = true;
        try{
            Database.LeadConvertResult[] lcrList = Database.convertLead(convertLeads, allowDuplicate);
            for(Database.LeadConvertResult lcr : lcrList){
                if(updateCCflag.containsKey(lcr.getLeadId())){
                    finalCCupdate.add(updateCCflag.get(lcr.getLeadId()));
                }
                System.debug('LCR Success'+lcr.isSuccess());
            }
            
            if(!finalCCupdate.isEmpty()){
                Database.update(finalCCupdate,allowDuplicate);
            }
            if(Test.isRunningTest()){
                CalloutException e = new CalloutException();
                e.setMessage('This is a constructed exception for testing and code coverage');
                throw e;
            }
        }catch(Exception e){
        	System.debug('The Following exception has occured: \t'+e.getMessage()+'\n'+e.getCause());
            sendEmail(e);    
        }
    }
    
    private static List<String> parseString(String toProcess){
        String removedCharatEnd = toProcess.removeEndIgnoreCase(',');
        String removeWhiteSpace = removedCharatEnd.replaceAll('\\s+', '');
        String [] splitEmails = removeWhiteSpace.split(',');
        return splitEmails;
    }
    
    public static void sendEmail(Exception e){
        //RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues('BatchNotificationConfig');
    	if(csRmito.EnableSendEmailBatchException__c == true){
			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			String[] toaddresses = csRmito.BatchExceptionEmailAddresses__c.split(',');
			email.setToAddresses(toaddresses);
			email.setSubject(System.Label.Batch_exception_email_subject);
            String finalMessage = System.Label.Batch_exception_email_string +'\n'+ e.getCause()+'\n'+e.getMessage()+'\n'+e.getStackTraceString();
            email.setHtmlBody(finalMessage);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    	}
    }
}