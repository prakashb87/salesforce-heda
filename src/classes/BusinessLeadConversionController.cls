/**
* -------------------------------------------------------------------------------------------------+
* @description This class controls B2B lead conversion feature
* --------------------------------------------------------------------------------------------------
* @author         Anupam Singhal
* @version        0.1
* @created        2019-04-04
* @modified       -
* ------------------------------------------------------------------------------------------------+
*/
public without sharing class BusinessLeadConversionController {
    private static final String OPPORTUNITYERROR = 'One or more leads couldn\'t be converted. If doNotCreateOpportunity is true, opportunityName and opportunityId must be null.';
    private static final String QUEUEEROR = 'Converted objects can only be owned by users.  If the lead is not owned by a user, you must specify a user for the Owner field.';
    private static final String CONTACTSTUDENT = 'Validation error on Contact: This contact has a student affiliation, therefore, the following fields cannot be amended; First Name, Last Name, Middle Name, Preferred Name, Gender, Mailing Address, Fax.';
    private static String returnValue='false';
    /**
    * ------------------------------------------------------------------------------------------------------------+
    * @description Method to Convert Business Lead : RM-2000
    * ----------------------------------------------------------------------------------------------------------
    * @method    NAME    convertBusinessLead
    * @param     idforConversion   String as combination of AccountId,ContactId,OpportunityId,donot Create checkbox,LeadId
    * @return    returnValue    string
    * ------------------------------------------------------------------------------------------------------------+
    */    
      
    @AuraEnabled
    public static string convertBusinessLead(Sobject idforConversion){
        Lead processLead = (lead)idforConversion;
        Id opportunityId;
        Id businessAcctId;
        List<Contact> contacts = new List<Contact> ([SELECT Id, AccountId FROM Contact WHERE Id=:processLead.Matched_Contact__c Limit 1]);
        Database.LeadConvert lc = new Database.LeadConvert();
        if(string.isNotBlank(processLead.Matched_Business_Account__c)){
            businessAcctId = processLead.Matched_Business_Account__c;
            lc.setAccountId(businessAcctId);
        } 
        
        if(!contacts.isEmpty()){
            lc.setContactId(contacts[0].Id);
            lc.setAccountId(contacts[0].AccountId);
            businessAcctId = createBusinessAccount(businessAcctId,processLead.Company);
        }            
        if(string.isNotBlank(processLead.Matched_Opportunity__c)){
            opportunityId=Id.valueOf(processLead.Matched_Opportunity__c);            
        }         
        
        if(string.isNotBlank(processLead.Opportunity_Name__c)){
            lc.setOpportunityName(processLead.Opportunity_Name__c);
        }  
        
        assignLeadIdConvertStatus(processLead,lc);
        Database.LeadConvertResult lcr = Database.convertLead(lc, allowduplicate());
        if(lcr.isSuccess()){
            opportunityId= String.isBlank(lcr.getOpportunityId())?opportunityId:lcr.getOpportunityId();
            businessAcctId= businessAcctId==NULL?lcr.getAccountId():businessAcctId;
            businessAcctId=lcr.getAccountId()==businessAcctId?lcr.getAccountId():businessAcctId;
            
            BusinessLeadConversionController.createBusinessAccountAffiliation(businessAcctId,lcr.getContactId(), processLead.RecordTypeId);
            BusinessLeadConversionController.reparentingOpportunity(businessAcctId,lcr.getContactId(),opportunityId);
            returnValue= 'true,'+lcr.getContactId() +','+ businessAcctId +',' + opportunityId;
        }else{
            throwAuraError(lcr.getErrors());
        }
        return returnValue;
    }
    
    private static void assignLeadIdConvertStatus(Lead processLead,Database.LeadConvert lc){
    	lc.setDoNotCreateOpportunity(processLead.Don_t_create_a_new_opportunity_upon_Lead__c);       
        lc.setLeadId(processLead.Id);
    
    	List<LeadStatus> convertStatus = new List<LeadStatus>([SELECT 
														        Id, 
														        MasterLabel 
														        FROM LeadStatus 
														        WHERE IsConverted=true 
														        LIMIT 1]);
        
        lc.setConvertedStatus(convertStatus[0].MasterLabel);
    }
    
    private static  Database.DMLOptions allowduplicate(){
    	Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        return dml;
    }
    
    private static String createBusinessAccount(String businessAcctId,String companyName){
        Id businessAccountRecTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        if(string.isBlank(businessAcctId)){
            Account bAc = new Account ( Name= companyName, RecordTypeId = businessAccountRecTypeId );
            Database.insert(bAc);
            businessAcctId = bAc.Id;
        }
        return businessAcctId;
    }
    
    private static void throwAuraError(List<Database.Error> errorInput){
        for(Database.Error dbe : errorInput) {
            String errorMessage = dbe.getMessage();
            if(errorMessage.equalsIgnoreCase(OPPORTUNITYERROR)){
                AuraHandledException e = new AuraHandledException(Label.Opportunity_error_for_conversion);
			    e.setMessage(Label.Opportunity_error_for_conversion);
			    throw e;
            }
            
            if(errorMessage.equalsIgnoreCase(QUEUEEROR)){
                AuraHandledException e = new AuraHandledException(Label.Owner_error_for_conversion);
			    e.setMessage(Label.Owner_error_for_conversion);
			    throw e;
            }
            
            if(errorMessage.equalsIgnoreCase(CONTACTSTUDENT)){
                List<String> errorMsg = errorMessage.split(':');
                AuraHandledException e = new AuraHandledException(errorMsg[1]);
			    e.setMessage(errorMsg[1]);
			    throw e;
            }
            
        }
        
    }   
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description Business Account and Affilation Creation Method : RM-2000
    * --------------------------------------------------------------------------------------------------------------
    * @method    NAME    createBusinessAccountAffiliation
    * @param     accId
    * @param     contId
    * @param     leadRecordTypeId
    * ---------------------------------------------------------------------------------------------------------------+
    */
    private static void createBusinessAccountAffiliation (Id accId, Id contId, Id leadRecordTypeId)
    {
        boolean isExist=false;
        Id leadBusinessAccountRecTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Business Lead').getRecordTypeId();  
        Id leadActivatorRecTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Activator Registration').getRecordTypeId();
        List <Account> actAccount = new List <Account>([SELECT Id FROM Account Where Name = 'Activator' LIMIT 1]);  
        Id recTypeAffId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Business').getRecordTypeId();
        Id recTypeAffActId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Activator').getRecordTypeId();
        //if(Schema.sObjectType.Account.fields.Id.isAccessible()){
            //actAccount = ;
        //}
        List<hed__Affiliation__c> headAff = new List<hed__Affiliation__c> ([SELECT ID FROM hed__Affiliation__c WHERE hed__Contact__c=:contId AND hed__Account__c=:accId]);
       // if(Schema.sObjectType.hed__Affiliation__c.isAccessible() && Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.Account.isAccessible()){
            //headAff=;   
        //}
        
        isExist=!headAff.isEmpty();
        //creating business affiliation
        if(Schema.sObjectType.hed__Affiliation__c.fields.hed__Contact__c.isCreateable() && !isExist){
            hed__Affiliation__c hedaAffilliation = new hed__Affiliation__c();
            hedaAffilliation.RecordTypeId = recTypeAffId;			
            hedaAffilliation.hed__Contact__c = contId;        
            hedaAffilliation.hed__Account__c = accId;
            insert hedaAffilliation;
            if(leadRecordTypeId==leadActivatorRecTypeId){
                hed__Affiliation__c hedaAffilliationact = new hed__Affiliation__c();
                hedaAffilliationact.RecordTypeId = recTypeAffActId;
                hedaAffilliationact.hed__Contact__c = contId;
                hedaAffilliationact.hed__Account__c = actAccount[0].Id;
                insert hedaAffilliationact;
            }
        }        
    }
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description Opportunity Reparenting Method : RM-2000
    * --------------------------------------------------------------------------------------------------------------
    * @method    NAME    reparentingOpportunity
    * @param     accountId    Matched Account Id, Conveted Contact Id/Matched Contact Id, Converted Business Lead Record
    * 					 Matched Opportunity Record Id/Converted Opportunity Record Id
    * @param     contactId
    * @param     oppId	
    * ---------------------------------------------------------------------------------------------------------------+
    */
    private static void reparentingOpportunity (Id accountId,Id contactId, Id oppId)
    {
        List<Opportunity> opportunities = new List<Opportunity>([SELECT AccountId, Contact__c, Assign_To_Contact__c FROM Opportunity WHERE Id =: oppId ]); 
        //if(Schema.sObjectType.Opportunity.fields.Contact__c.isAccessible()){
            //opportunities=;
        //}
        if(!Schema.sObjectType.Opportunity.isUpdateable()){
            return;
        }
        if(!opportunities.isEmpty()){
            Opportunity opp = opportunities[0];
            if(opp.AccountId != accountId){
                opp.AccountId = accountId;
                opp.Contact__c = contactId;
                Database.update(opp);
            }else{
                insertOpportunityRoles(contactId,opp);
            }
        }
    }
    
    /**
    * --------------------------------------------------------+
    * @description used to create opportunity contact
    * roles: RM-2000
    * --------------------------------------------------------+
    * @method    insertOpportunityRoles
    * @param     contactId
    * @param     opp
    * --------------------------------------------------------+
    */
    private static void insertOpportunityRoles(Id contactId,Opportunity opp){
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        if(!Schema.sObjectType.OpportunityContactRole.isCreateable()){
            return;  
        }
        oppContactRoles=[SELECT ID FROM OpportunityContactRole WHERE ContactID=:contactId];
        if(oppContactRoles.isEmpty()){
            OpportunityContactRole newContactRole = new OpportunityContactRole(ContactId=contactId,OpportunityId=opp.Id, IsPrimary=false);
            Database.insert(newContactRole);
        }
    }
}