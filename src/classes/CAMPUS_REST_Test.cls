/*********************************************************************************
*** @TestClassName     : CAMPUS_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Location_Rest web service apex class.
*** @Created date      : 27/08/2018
**********************************************************************************/
@isTest
public class CAMPUS_REST_Test {
static testMethod void myUnitTest() {
        Account acc= new Account();
        acc.Name = 'RMITU';
        insert acc;
    	
    	Location__c loc = new Location__c();
    	loc.Name = 'Test Loc';
    	loc.location__C = 'VNU';
    	insert loc;
    
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Campus';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","campus": "VNMNU","effectiveDate": "1901-01-01","status": "A","description": "Vietnam National University","shortDescription": "VNU","location": "VNU","validLocationCode": "VNU"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = CAMPUS_REST.upsertCampus();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Campus';
        
        //creating test data
        String JsonMsg = '{"institution": "","campus": "","effectiveDate": "1901-01-01","status": "A","description": "Vietnam National University","shortDescription": "VNU","location": "VNU","validLocationCode": "VNU"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = CAMPUS_REST.upsertCampus();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = CAMPUS_REST.upsertCampus();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        Account acc= new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        Campus__c camp = new Campus__c();
        
        camp.Name = 'Campus Test';
        
        camp.Effective_Date__c = Date.valueOf('1902-05-01');
        
        camp.Institution__c = acc.id;
        
        //camp.Campus__c = 'VNMNU';
        
        insert camp;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Campus';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","campus": "VNMNU","effectiveDate": "1901-01-01","status": "A","description": "Vietnam National University","shortDescription": "VNU","location": "VNU","validLocationCode": "VNU"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = CAMPUS_REST.upsertCampus();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void testFutureMethod() {
        Account acc= new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        Location__c loc = new Location__c();
    	loc.Name = 'VNU';
    	loc.location__C = 'VNU123';
    	insert loc;
        
        Campus__c camp = new Campus__c();
        
        camp.Name = 'Campus Test';
        
        camp.Effective_Date__c = Date.valueOf('1902-05-01');
        
        camp.Institution__c = acc.id;
        
        camp.Valid_Location_Code__c = 'VNU';
        
        //camp.Campus__c = 'VNMNU';
        
        insert camp;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Campus';
        //creating test data
        String JsonMsg = '{"institution": "RMITU","campus": "Campus Test","effectiveDate": "3019-01-01","status": "A","description": "Vietnam National University","shortDescription": "VNU","location": "VNU","validLocationCode": "VNU"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = CAMPUS_REST.upsertCampus();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}