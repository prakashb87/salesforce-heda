/*********************************************************************************
*** @ClassName         : COURSEOFFERING_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 30/08/2018
*** @Modified by       : Anupam Singhal
*** @modified date     : 20/09/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/CourseOffering')
global with sharing class COURSEOFFERING_REST {
    
    public String courseCode;
    public String effectiveDate;
    public String courseOfferingNumber;
    public String institution;
    public String academicgroup;
    public String subject;
    public String catalogueNumber;
    public String campus;
    public String academicOrganisation;
    public String academicCareer;
    public String courseApproved;
    public String allowCourseToBeScheduled;
    public String fieldOfEducationCode;
    public String courseCoordinator;
    //public String studentId;
    
    @HttpPost
    global static Map < String, String > upsertCourseOffering() {
        //to create map of effective date        
        map < String,Date > updateEffectiveDate = new map < String,Date >(); 
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Course offering
        list < hed__Course__c > upsertCourseOffering = new list < hed__Course__c > ();
        //course list
        list<Course_List__c> course = new list<Course_List__c>();
        //effective date
        list<Date> addEffectiveDate = new list<Date>();
        //Academic Organization 
        list<Account> academicOrg = new list<Account>();
        //getting course Id
        list < String > addCourseID = new list < String > ();
        //getting courseOffering Field to be translated
        list < String > addCourseOfferNumber = new list < String > ();
        //get instituion
        list < String > listInstitution = new list < String > ();
        //Add Academic organization to get from account
        list < String > addAcademicOrg = new list < String > ();
        //To get courseCoordinator from StudentId 
        list<String> addContactId = new list<String>();
        // field of education
        list<Field_of_education__c> foe = new list<Field_of_education__c>();
        // courseCoordinator
        list<Contact>  courseCoordinator = new list<Contact>();
        //To get field Of Education Code  
        list<String> addFieldOfEducationCode = new list<String>();
        //hed__Course__c
        hed__Course__c courseOffering = new hed__Course__c();
        try {
            
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            system.debug('reqStr-->' + reqStr);
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List < COURSEOFFERING_REST > postString = (List < COURSEOFFERING_REST > ) System.JSON.deserialize(reqStr, List < COURSEOFFERING_REST > .class);
            
            //store institution
            Account institution = new Account();
            
            if (postString != null) {
                for (COURSEOFFERING_REST courseOfferingrest: postString) {
                    if (courseOfferingrest.courseCode != null && (courseOfferingrest.courseCode).length() > 0 && courseOfferingrest.courseOfferingNumber != null && (courseOfferingrest.courseOfferingNumber).length() > 0 && courseOfferingrest.institution != null && (courseOfferingrest.institution).length() > 0) {
                        listInstitution.add(courseOfferingrest.institution);
                        addCourseID.add(courseOfferingrest.courseCode);
                        addCourseOfferNumber.add(courseOfferingrest.courseOfferingNumber);
                        if(courseOfferingrest.academicOrganisation != null && (courseOfferingrest.academicOrganisation).length() > 0 )
                        {
                            addAcademicOrg.add(courseOfferingrest.academicOrganisation);
                        }
                        //need to confirm the name of JSON enumber             
                        if(courseOfferingrest.courseCoordinator != null && (courseOfferingrest.courseCoordinator).length() > 0)
                        {
                            addContactId.add(courseOfferingrest.courseCoordinator);
                        }
                        if(courseOfferingrest.fieldOfEducationCode != null && (courseOfferingrest.fieldOfEducationCode.length() > 0))
                        {
                            addFieldOfEducationCode.add(courseOfferingrest.fieldOfEducationCode);
                        }
                        if(courseOfferingrest.effectiveDate != null && (courseOfferingrest.effectiveDate).length() > 0){
                            updateEffectiveDate.put(courseOfferingrest.courseCode+''+courseOfferingrest.courseOfferingNumber,Date.ValueOf(courseOfferingrest.effectiveDate));
                            addEffectiveDate.add(Date.valueOf(courseOfferingrest.effectiveDate));
                        }
                    } else {
                        //throw error
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                    
                }
            }
            //getting course record
            if(addEffectiveDate != null && addEffectiveDate.size() > 0 && addCourseID != null && addCourseID.size() > 0)
            {
                course = [select id,Description__c from Course_List__c where Name =: addCourseID[0] and Effective_Date__c =: addEffectiveDate limit 1];
            }
            //getting the old courseOffering for update
            list < hed__Course__c > previousCourseOffering = new list < hed__Course__c > ();
            if (addCourseID != null && addCourseID.size() > 0 && addCourseOfferNumber != null && addCourseOfferNumber.size() > 0) {
                previousCourseOffering = [select id,hed__Course_ID__c,hed__Account__c,hed__Account__r.Name,Effective_date__c,Course_Offer_Number__c,Academic_Group__c,Subject__c,Catalog_Number__c,Campus__c,Academic_Organization__c, Academic_Organization__r.Name,Career__c,Course_Approved__c,Allow_Course_To_Be_Scheduled__c,Field_Of_Education_Code__c,Field_of_Education_Description__c,Course_Coordinator__c,Course_List__c
                                          from hed__Course__c where hed__Course_ID__c IN : addCourseID and Course_Offer_Number__c IN: addCourseOfferNumber limit 1
                                         ];
            }
            
            //map to update the previous Translation service
            map < String, hed__Course__c > oldCourseOffering = new map < String, hed__Course__c > ();
            for (hed__Course__c prevCourse : previousCourseOffering) {
                if(prevCourse.hed__Course_ID__c != null && prevCourse.Course_Offer_Number__c != null){
                    String uniqKey = prevCourse.hed__Course_ID__c + '' + prevCourse.Course_Offer_Number__c;
                    if (updateEffectiveDate != null && updateEffectiveDate.get(uniqKey) != null) {               
                        if(updateEffectiveDate.get(uniqKey) <= System.today() && updateEffectiveDate.get(uniqKey) > prevCourse.Effective_Date__c ){
                            oldCourseOffering.put(uniqKey, prevCourse);
                        }else if(updateEffectiveDate.get(uniqKey) > System.today()){
                            caseMatching(postString, previousCourseOffering);
                        }else if(updateEffectiveDate.get(uniqKey) < prevCourse.Effective_Date__c ){
                           responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend; 
                        }
                    }else{
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                }
            }
           
            //Store institution record
            if (listInstitution != null && listInstitution.size() > 0)
            {
                institution = [select id from account where Name =: listInstitution[0] Limit 1];
            }
            //Get academic organization record from account
            if(addAcademicOrg != null && addAcademicOrg.size() > 0)
            {
                academicOrg = [select id from Account where AccountNumber__c =: addAcademicOrg limit 1];
            }
            if(addContactId != null && addContactId.size() > 0)
            {
                courseCoordinator =  [select id from contact where Enumber__c =: addContactId];
            }
            if(addFieldOfEducationCode != null && addFieldOfEducationCode.size() > 0)
            {
                foe = [select id,Description__c from Field_of_education__c where name =: addFieldOfEducationCode limit 1];
            }
            //insert or update the courseOffering
            for (COURSEOFFERING_REST courseOfferingRest: postString) {
                String key = courseOfferingRest.courseCode + '' + courseOfferingRest.courseOfferingNumber;
                courseOffering = new hed__Course__c();
                courseOffering.Name = course != null && course.size() > 0 ? course[0].Description__c : null;
                courseOffering.hed__Account__c = institution != null ? institution.id : null;
                courseOffering.hed__Course_ID__c    =   courseOfferingRest.courseCode;
                courseOffering.Effective_date__c    =   courseOfferingRest.effectiveDate != null && (courseOfferingRest.effectiveDate).length () > 0 ? Date.ValueOf(courseOfferingRest.effectiveDate) : null;
                courseOffering.Course_Offer_Number__c   =   courseOfferingRest.courseOfferingNumber;
                courseOffering.Academic_Group__c    =   courseOfferingRest.academicgroup;
                courseOffering.Subject__c   =   courseOfferingRest.subject;
                courseOffering.Catalog_Number__c    =   courseOfferingRest.catalogueNumber;
                courseOffering.Campus__c    =   courseOfferingRest.campus;
                courseOffering.Academic_Organization__c =   academicOrg != null && academicOrg.size() > 0 ? academicOrg[0].ID : null;
                courseOffering.Career__c    =   courseOfferingRest.academicCareer;
                courseOffering.Course_Approved__c   =   courseOfferingRest.courseApproved;
                courseOffering.Allow_Course_To_Be_Scheduled__c  =   courseOfferingRest.allowCourseToBeScheduled;
                courseOffering.Field_Of_Education_Code__c   =   foe != null && foe.size() > 0 ? foe[0].ID : null;
                courseOffering.Field_of_Education_Description__c = foe != null && foe.size() > 0 ? foe[0].Description__c : null;
                courseOffering.Course_Coordinator__c  =   courseCoordinator != null && courseCoordinator.size() > 0 ? courseCoordinator[0].ID : null;
                courseOffering.Course_List__c = course != null && course.size() > 0 ? course[0].Id : null;
                courseOffering.CourseOffering_key__c = key;
                if (oldCourseOffering.size() > 0 && oldCourseOffering.get(key) != null) {
                    if(oldCourseOffering.get(key).Effective_Date__c <= courseOffering.Effective_date__c ){
                        courseOffering.ID = oldCourseOffering.get(key).ID;    
                    }else{
                        courseOffering = null;
                    }
                    
                }
                if(courseOffering!=null){
                    upsertCourseOffering.add(courseOffering);    
                }
                
            }
            system.debug('upsertCourseOffering-->' + upsertCourseOffering );
            //upsert courseOffering
            if (upsertCourseOffering != null && upsertCourseOffering.size() > 0 && Date.ValueOf(postString[0].effectiveDate) <= System.today()) {
                upsert upsertCourseOffering CourseOffering_key__c;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Course Offering was not well defined, amigo');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
    
    public static void caseMatching(List<COURSEOFFERING_REST> postString, List<hed__Course__c> courseOffRecords){
        //Future Change variables
        List<Course_Offering_Future_Change__c> ListFutChange = new List<Course_Offering_Future_Change__c>();
        Course_Offering_Future_Change__c FutChangeRec = new Course_Offering_Future_Change__c();
        
        string effDateDB = null;
        if (courseOffRecords[0].Effective_Date__c != Null){
            effDateDB = String.valueOf(courseOffRecords[0].Effective_Date__c);
        }
        
        string effDateJSON = null;
        if (postString[0].effectiveDate.length() > 0){
            effDateJSON = postString[0].effectiveDate;                           
        }
        
        if(effDateDB != effDateJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Effective_Date__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].effectiveDate;
            ListFutChange.add(FutChangeRec);
        }
        
        
        string accIntitutionDB = null;
        if (courseOffRecords[0].hed__Account__c != Null){
            accIntitutionDB = courseOffRecords[0].hed__Account__r.Name;
        }
        
        string accIntitutionJSON = null;
        if (postString[0].institution.length() > 0){
            accIntitutionJSON = postString[0].institution;                           
        }
        
        if(accIntitutionDB != accIntitutionJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('hed__Account__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = accIntitutionJSON != null? [SELECT Id FROM ACCOUNT WHERE Name =: accIntitutionJSON].size()>0?[SELECT Id FROM ACCOUNT WHERE Name =: accIntitutionJSON].Id: accIntitutionJSON:accIntitutionJSON;
            ListFutChange.add(FutChangeRec);
        }
        //courseOfferingrest.courseCode+''+courseOfferingrest.courseOfferingNumber
        string acadGroupDB = null;
        if (courseOffRecords[0].Academic_Group__c != Null){
            acadGroupDB = courseOffRecords[0].Academic_Group__c;
        }
        
        string acadGroupJSON = null;
        if (postString[0].academicgroup.length() > 0){
            acadGroupJSON = postString[0].academicgroup;                           
        }
        
        if(acadGroupDB != acadGroupJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Academic_Group__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].academicgroup;
            ListFutChange.add(FutChangeRec);
        }
        
        string subjectDB = null;
        if (courseOffRecords[0].Subject__c != Null){
            subjectDB = courseOffRecords[0].Subject__c;
        }
        
        string subjectJSON = null;
        if (postString[0].subject.length() > 0){
            subjectJSON = postString[0].subject;                           
        }
        
        if(subjectDB != subjectJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Subject__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].subject;
            ListFutChange.add(FutChangeRec);
        }
        
        string catNumDB = null;
        if (courseOffRecords[0].Catalog_Number__c != Null){
            catNumDB = courseOffRecords[0].Catalog_Number__c;
        }
        
        string catNumJSON = null;
        if (postString[0].catalogueNumber.length() > 0){
            catNumJSON = postString[0].catalogueNumber;                           
        }
        
        if(catNumDB != catNumJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Catalog_Number__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].catalogueNumber;
            ListFutChange.add(FutChangeRec);
        }
        
        string campusDB = null;
        if (courseOffRecords[0].Campus__c != Null){
            campusDB = courseOffRecords[0].Campus__c;
        }
        
        string campusJSON = null;
        if (postString[0].campus.length() > 0){
            campusJSON = postString[0].campus;                           
        }
        
        if(campusDB != campusJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Campus__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].campus;//To insert the Campus's sf id in the field
            ListFutChange.add(FutChangeRec);
        }
        
        string acadOrgDB = null;
        if (courseOffRecords[0].Academic_Organization__c != Null){
            acadOrgDB = courseOffRecords[0].Academic_Organization__r.Name;
        }
        
        string acadOrgJSON = null;
        if (postString[0].academicOrganisation.length() > 0){
            acadOrgJSON = postString[0].academicOrganisation;                           
        }
        
        if(acadOrgDB != acadOrgJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Academic_Organization__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = acadOrgJSON != null? [SELECT Id FROM ACCOUNT WHERE AccountNumber__c =: acadOrgJSON LIMIT 1].size()>0?[SELECT Id FROM ACCOUNT WHERE AccountNumber__c =: acadOrgJSON LIMIT 1].Id: acadOrgJSON: acadOrgJSON;
            ListFutChange.add(FutChangeRec);
        }
        
        string careerDB = null;
        if (courseOffRecords[0].Career__c != Null){
            careerDB = courseOffRecords[0].Career__c;
        }
        
        string careerJSON = null;
        if (postString[0].academicCareer.length() > 0){
            careerJSON = postString[0].academicCareer;                           
        }
        
        if(careerDB != careerJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Career__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].academicCareer;
            ListFutChange.add(FutChangeRec);
        }
        
        string courseApproveDB = null;
        if (courseOffRecords[0].Course_Approved__c != Null){
            courseApproveDB = courseOffRecords[0].Course_Approved__c;
        }
        
        string courseApproveJSON = null;
        if (postString[0].courseApproved.length() > 0){
            courseApproveJSON = postString[0].courseApproved;                           
        }
        
        if(courseApproveDB != courseApproveJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Course_Approved__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].courseApproved;
            ListFutChange.add(FutChangeRec);
        }
        
        string allowCourseSchedDB = null;
        if (courseOffRecords[0].Allow_Course_To_Be_Scheduled__c != Null){
            allowCourseSchedDB = courseOffRecords[0].Allow_Course_To_Be_Scheduled__c;
        }
        
        string allowCourseSchedJSON = null;
        if (postString[0].allowCourseToBeScheduled.length() > 0){
            allowCourseSchedJSON = postString[0].allowCourseToBeScheduled;                           
        }
        
        if(allowCourseSchedDB != allowCourseSchedJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Allow_Course_To_Be_Scheduled__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = postString[0].allowCourseToBeScheduled;
            ListFutChange.add(FutChangeRec);
        }
        
        string foeDB = null;
        if (courseOffRecords[0].Field_Of_Education_Code__c != Null){
            foeDB = courseOffRecords[0].Field_Of_Education_Code__r.Name;
        }
        
        string foeJSON = null;
        if (postString[0].fieldOfEducationCode.length() > 0){
            foeJSON = postString[0].fieldOfEducationCode;                           
        }
        
        if(foeDB != foeJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Field_Of_Education_Code__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = foeJSON != null?[SELECT Id From Field_of_education__c WHERE Name=: foeJSON].size()>0?[SELECT Id From Field_of_education__c WHERE Name=: foeJSON].Id:foeJSON:foeJSON;
            ListFutChange.add(FutChangeRec);
        }
        
        string courseCoordDB = null;
        if (courseOffRecords[0].Course_Coordinator__c != Null){
            courseCoordDB = courseOffRecords[0].Course_Coordinator__r.Student_ID__c;
        }
        
        string courseCoordJSON = null;
        if (postString[0].courseCoordinator.length() > 0){
            courseCoordJSON = postString[0].courseCoordinator;                           
        }
        
        if(courseCoordDB != courseCoordJSON){
            FutChangeRec = COURSEOFFERING_REST.createFutChange('Course_Coordinator__c');
            FutChangeRec.Parent_Lookup__c = courseOffRecords[0].Id;
            FutChangeRec.Value__c = courseCoordJSON != null? [select id from contact where Enumber__c =: courseCoordJSON LIMIT 1].size()>0?[select id from contact where Enumber__c =: courseCoordJSON LIMIT 1].Id:courseCoordJSON:courseCoordJSON;
            ListFutChange.add(FutChangeRec);
        }
        
        if(ListFutChange != null && ListFutChange.size() > 0){
            Database.insert(ListFutChange);
        }
        //Course_List__c
    }
    
    public static Map<String, Schema.DescribeFieldResult> getFieldMetaData(Schema.DescribeSObjectResult dsor, Set<String> fields) {
            
            // the map to be returned with the final data
            Map<String,Schema.DescribeFieldResult> finalMap = 
                new Map<String, Schema.DescribeFieldResult>();
            // map of all fields in the object
            Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
            
            // iterate over the requested fields and get the describe info for each one. 
            // add it to a map with field name as key
            for(String field : fields){
                // skip fields that are not part of the object
                if (objectFields.containsKey(field)) {
                    Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                    // add the results to the map to be returned
                    finalMap.put(field, dr); 
                }
            }
            return finalMap;
        }
    
    public static Course_Offering_Future_Change__c createFutChange(String str){
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        
        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        
        response.addHeader('Content-Type', 'application/json');
        List < COURSEOFFERING_REST > postString = (List < COURSEOFFERING_REST > ) System.JSON.deserialize(reqStr, List < COURSEOFFERING_REST > .class);
        Course_Offering_Future_Change__c planFu = new Course_Offering_Future_Change__c();
        Set<String> fields = new Set<String>{str};
            
            Map<String, Schema.DescribeFieldResult> finalMap = 
            COURSEOFFERING_REST.getFieldMetaData(hed__Course__c.getSObjectType().getDescribe(), fields);
        
        // only print out the 'good' fields
        for (String field : new Set<String>{str}) {
            planFu.API_Field_Name__c = finalMap.get(field).getName();
            planFu.Field__c = finalMap.get(field).getLabel();
        }
        
        for (COURSEOFFERING_REST checkDate : postString) {
            string jsonDate = checkDate.effectiveDate;
            planFu.Effective_Date__c = Date.valueOf(jsonDate);
        }
        
        //planFu.Applied__c = true;
        
        return planFu;
    }
}