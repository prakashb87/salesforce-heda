/*********************************************************************************
*** @TestClassName     : COURSEOFFERING_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the COURSEOFFERING_REST web service apex class.
*** @Created date      : 14/09/2018
**********************************************************************************/
@isTest
public class COURSEOFFERING_REST_Test {
    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/CourseOffering';
        
        //creating test data
        String JsonMsg = '{"courseCode": "009342","effectiveDate": "2001-01-01","courseOfferingNumber": 1,"institution": "RMITU","academicgroup": "BHSN","subject": "CONV","catalogueNumber": "MR999","campus": "","academicOrganisation": "185","academicCareer": "RSCH","courseApproved": "A","allowCourseToBeScheduled": "Y","fieldOfEducationCode": "","courseCoordinator": ""}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COURSEOFFERING_REST.upsertCourseOffering();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/CourseOffering';
        
        //creating test data
        String JsonMsg = '{"courseCode": "","effectiveDate": "2001-01-01","courseOfferingNumber": 1,"institution": "RMITU","academicgroup": "BHSN","subject": "CONV","catalogueNumber": "MR999","campus": "","academicOrganisation": "185","academicCareer": "RSCH","courseApproved": "A","allowCourseToBeScheduled": "Y","fieldOfEducationCode": "","courseCoordinator": ""}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COURSEOFFERING_REST.upsertCourseOffering();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = COURSEOFFERING_REST.upsertCourseOffering();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        Account acnt = new Account();
        acnt.Name = 'Account Number';
        acnt.AccountNumber = '185' ;
        insert acnt;
        
        Contact con = new contact();
        con.LastName = 'Tes con';
        con.Student_ID__c = '12345';
        insert con;
        
        Field_of_education__c foe = new Field_of_education__c();
        foe.name = '589';
        foe.Effective_date__c = Date.valueOf('2001-05-01');
        insert foe;
        
        Course_List__c courseList = new Course_List__c();
        //need to comment if changed to auto number;
        courseList.Name = '009342';
        courseList.Effective_Date__c = Date.valueOf('2001-05-01');
        insert courseList;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'Test course';
        course.hed__Course_ID__c = '009342';
        course.Course_Offer_Number__c = '1';
        course.hed__Account__c = acc.iD;
        insert course;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/CourseOffering';
        
        //creating test data
        String JsonMsg = '{"courseCode": "009342","effectiveDate": "2001-05-01","courseOfferingNumber": 1,"institution": "RMITU","academicgroup": "BHSN","subject": "CONV","catalogueNumber": "MR999","campus": "","academicOrganisation": "185","academicCareer": "RSCH","courseApproved": "A","allowCourseToBeScheduled": "Y","fieldOfEducationCode": "589","courseCoordinator": "12345"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COURSEOFFERING_REST.upsertCourseOffering();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}