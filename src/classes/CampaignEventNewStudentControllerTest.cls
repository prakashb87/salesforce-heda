@isTest
public class CampaignEventNewStudentControllerTest 
{
    static Campaign newCampaign;
    static Account newAcc;
    static Lead newLead;
    static CampaignMember newCampaignMember;
    private static Id prospectiveRecordType = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
    
    static void setup()
    {
        newAcc = new Account();
        newAcc.Name = 'Test Account';
        insert newAcc;
        
        newCampaign = new Campaign();
        newCampaign.Name = 'Test Campaign';
        newCampaign.School__c = newAcc.Id;
        insert newCampaign;
        
    }
    
    static testMethod void testCreateNewStudent()
    {
        setup();
        
        PageReference pageRef = Page.Event_Student;
        pageRef.getParameters().put('refcampaign', newCampaign.Id);
        pageRef.getParameters().put('schoolid', newAcc.Id);
        pageRef.getParameters().put('schoolname', newAcc.Name);
        Test.setCurrentPage(pageRef);
        
        newLead = new Lead();
        newLead.RecordTypeId = prospectiveRecordType;
        newLead.FirstName = 'Sangsom';
        newLead.LastName = 'HongThong';
        newLead.Company = 'Test Account';
        newLead.matched_school__c = newAcc.Id;
        newLead.Phone = '12345678';
        newLead.MobilePhone = '1234556677';
        newLead.PostCode__c = '50190';
        newLead.Description = 'Test Description';
        newLead.Email = 'test@test.com';
        newLead.I_am_currently__c = 'Studying at another uni/TAFE';
        newLead.Residency_status__c = 'Australian citizen';
        //newLead.Select_an_area_you_are_interested_in__c = 'Health and biomedical sciences';
        insert newLead;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        CampaignEventNewStudentController testCreateNewStudent = new CampaignEventNewStudentController(sc);
        testCreateNewStudent.FirstName = 'Maki';
        testCreateNewStudent.LastName =  'Salmon';
        testCreateNewStudent.Email =  'salmon@test.com';
        testCreateNewStudent.Mobile = '0999818181';
        testCreateNewStudent.getIAmCurrently();
        testCreateNewStudent.getSelectAreaInterested();
        testCreateNewStudent.getResidencyStatus();
        
        testCreateNewStudent.createNewStudent();
    }
    
    static testMethod void testCreateNewStudent_Negative()
    {
        setup();
        
        PageReference pageRef = Page.Event_Student;
        Test.setCurrentPage(pageRef);
        
        newLead = new Lead();
        newLead.RecordTypeId = prospectiveRecordType;
        newLead.FirstName = 'Sangsom';
        newLead.LastName = 'HongThong';
        newLead.Company = 'Test Account';
        newLead.matched_school__c = newAcc.Id;
        newLead.Phone = '12345678';
        newLead.MobilePhone = '1234556677';
        newLead.Description = 'Test Description';
        newLead.Email = 'test@test.com';
        insert newLead;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        CampaignEventNewStudentController testCreateNewStudent = new CampaignEventNewStudentController(sc);
        
        testCreateNewStudent.createNewStudent();
    }
}