/*****************************************************************
Name: CanvasEnrollmentRequest
Author: Capgemini [Shreya]
Purpose: Handle Canvas Integration  for all the course connection records which are created via embedded batch credential job.
Jira Reference : ECB-3900,ECB-5545
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           02/04/2019         Initial Version
********************************************************************/

public without sharing class CanvasEnrollmentRequest implements ICanvasEnrollmentRequest {
    
    private CourseConnCanvasResponseWrapper calloutResponse;
    private String jSONString;
    private HTTPResponse response;
       
    public void sendEnrollDetailToCanvas(CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody){
        String endpoint = ConfigDataMapController.getCustomSettingValue('CanvasEndPointURL'); //https://procs-canvas-v1-dev.npe.integration.rmit.edu.au/api/enrolment
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CanvasClientId'));
        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CanvasClientSecretId'));
        req.setHeader('Content-Type','application/json');
        req.setMethod('POST');
        jSONString = JSON.serialize(canvasEnrollmentRequestBody);
        req.setbody(jSONString);
        req.setTimeout(120000);
        Http http = new Http();
        
        
        response = http.send(req);
        insertIntegrationLogs(jSONString,response.getBody());
        
        if(response.getStatusCode()!= 200){
            throw new CanvasRequestCalloutException('Canvas Response not successful. Status Code: ' + response.getStatus() + ' Response: ' + response.getBody());
        }
           
        System.debug('Response Code ---------->'+response.getStatusCode()+'\nResponse Code ---------->'+response.getStatus());
        try{
            this.calloutResponse = (CourseConnCanvasResponseWrapper) JSON.deserializeStrict(response.getBody(), CourseConnCanvasResponseWrapper.class);
        }catch(Exception ex){
            throw new CanvasRequestCalloutException('Canvas Response not successful. Status Code: ' + response.getStatus() + ' Response: ' + response.getBody());
        }

    }
         
    public Map<Id, CourseConnCanvasResponseWrapper.Response> getCanvasEnrollResByCourseConnId() {
        if (calloutResponse == null) {
            throw new ArgumentException('HTTP callout request for Canvas enrollment needs to be executed first.');
        }
        Map<Id, CourseConnCanvasResponseWrapper.Response> canvasEnrollResByCourseConnId = new Map<Id, CourseConnCanvasResponseWrapper.Response>();
        for(CourseConnCanvasResponseWrapper.Response calloutResponse : calloutResponse.payload.enrolmentResponse) {
           canvasEnrollResByCourseConnId.put(Id.valueOf(calloutResponse.courseConnectionId), calloutResponse);
        }
        return canvasEnrollResByCourseConnId;
    }
    
    public Integer getTotalNumberOfFailedEnrollments() {
        if (calloutResponse == null) {
            throw new ArgumentException('HTTP callout request for Canvas enrollment needs to be executed first.');
        }
        Integer failCount = 0;
        for(CourseConnCanvasResponseWrapper.Response calloutResponse : calloutResponse.payload.enrolmentResponse) {
            if(calloutResponse.result!=ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess')){
                failCount = failCount + 1;
            }
        }
        return failCount;
    }
    
    private void insertIntegrationLogs(String reqBody, String responseBody){
        IntegrationLog requestLog = new IntegrationLog();
        requestLog.setRequestBody(reqBody);
        requestLog.setType('Canvas_IPaaS');
        requestLog.setResponseBody(responseBody);
        requestLog.log();
    }
    
}