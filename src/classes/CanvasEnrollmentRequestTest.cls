/*****************************************************************
Name: CanvasEnrollmentRequest
Author: Capgemini 
Purpose: Test Class of CanvasEnrollmentRequest for JIRA ID ECB 3900
*****************************************************************/
/*==================================================================
History
--------
Version   Author          Date              Detail
1.0      Shreya           25/06/2018         Initial Version
********************************************************************/
@isTest
public class CanvasEnrollmentRequestTest {
    
     @testSetup
    static void testData() {
        List<Config_Data_Map__c> config = new List<Config_Data_Map__c>();
        
        Config_Data_Map__c endpoint = new Config_Data_Map__c();
        endpoint.name = 'CanvasEndPointURL';
        endpoint.Config_Value__c = 'https://procs-canvas-v1-dev.npe.integration.rmit.edu.au/api/enrolment';
        config.add(endpoint);
        
        Config_Data_Map__c clientId = new Config_Data_Map__c();
        clientId.name = 'CanvasClientId';
        clientId.Config_Value__c = '12345redt';
        config.add(clientId);
        
        Config_Data_Map__c secretId = new Config_Data_Map__c();
        secretId.name = 'CanvasClientSecretId';
        secretId.Config_Value__c = 'ertret546';
        config.add(secretId);
        
        Config_Data_Map__c supportEmail = new Config_Data_Map__c();
        supportEmail.name = 'ECB Support Email';
        supportEmail.Config_Value__c = 'test@gmail.com';
        config.add(supportEmail);
        
        Config_Data_Map__c emailSub = new Config_Data_Map__c();
        emailSub.name = 'EmbedErrorEmailSubject';
        emailSub.Config_Value__c = 'Embed Error';
        config.add(emailSub);
        
        Config_Data_Map__c canvasAccountId = new Config_Data_Map__c();
        canvasAccountId.name = 'CourseConnectionCanvasAccountId';
        canvasAccountId.Config_Value__c = '59';
        config.add(canvasAccountId);
               
        Config_Data_Map__c canvasType = new Config_Data_Map__c();
        canvasType.name = 'CourseConnectionCanvastype';
        canvasType.Config_Value__c = 'Student Enrollment';
        config.add(canvasType);
        
        Config_Data_Map__c canvasEnrollState = new Config_Data_Map__c();
        canvasEnrollState.name = 'CourseConnectionCanvasenrolmentstate';
        canvasEnrollState.Config_Value__c = 'active';
        config.add(canvasEnrollState);
        
        Config_Data_Map__c canvasNotify = new Config_Data_Map__c();
        canvasNotify.name = 'CourseConnectionCanvasnotify';
        canvasNotify.Config_Value__c = 'false';
        config.add(canvasNotify);
        
        Config_Data_Map__c canvasStage = new Config_Data_Map__c();
        canvasStage.Name = 'CourseConnectionLifeCycleStageCanvas';
        canvasStage.Config_Value__c = 'Post to Canvas';
        config.add(canvasStage);
        
        Config_Data_Map__c canvasInitiated = new Config_Data_Map__c();
        canvasInitiated.Name = 'CourseConnectionLifeCycleInitiated';
        canvasInitiated.Config_Value__c = 'Initiated';
        config.add(canvasInitiated);
        
        Config_Data_Map__c canvasError = new Config_Data_Map__c();
        canvasError.Name = 'CourseConnectionLifeCycleError';
        canvasError.Config_Value__c = 'Error';
        config.add(canvasError);
        
        Config_Data_Map__c canvasSuccess = new Config_Data_Map__c();
        canvasSuccess.Name = 'CourseConnectionLifeCycleSuccess';
        canvasSuccess.Config_Value__c = 'Success';
        config.add(canvasSuccess);
        
        Insert config;
        
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Organization').getRecordTypeId();  
        Id courseEnrollRecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        
        Account acc = new Account(name = 'RMIT Online', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        insert acc;
        
        Account accountRec = new Account(Name='Test Partner',RecordTypeId=accountRecordTypeId);
        Insert accountRec;
        
        //Creating Address test data
        hed__Address__c addr = new hed__Address__c();
        addr.hed__MailingStreet__c = 'abc';
        addr.hed__MailingStreet2__c = 'xyz';
        addr.hed__MailingCity__c= 'Test';
        addr.hed__MailingState__c= 'Mel';
        addr.hed__MailingPostalCode__c= '0121';
        addr.hed__MailingCountry__c = 'Aus';
        addr.Last_Name__c = 'test';
        Insert addr;
        
        Contact con = new Contact();
        con.LastName='TestCon';
        con.Student_ID__c='1235';
        con.Email='mail@mail.com';
        con.hed__UniversityEmail__c = 'mail@mail.com';
        con.hed__Current_Address__c=addr.Id;
        con.hed__Preferred_Email__c = 'SAMS/SAP University Email';
        Insert con;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'test Course';
        course.hed__Account__c = acc.id;
        course.Status__c='Active';
        course.SIS_Course_Id__c='1254';
        course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
        Insert course;
        
        hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = acc.id);
        insert term;
        
        hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
        crsoffer.name = 'test courseoffer';
        crsoffer.hed__Course__c = course.Id;
        crsoffer.hed__Term__c = term.id;
        crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
        crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
        Insert crsoffer; 
        
        hed__Course_Enrollment__c ce = new hed__Course_Enrollment__c();
        ce.hed__Contact__c = con.Id;
        ce.hed__Course_Offering__c = crsoffer.Id;
        ce.hed__Status__c = 'Current';
        ce.Enrolment_Status__c = 'E';
        ce.Canvas_Integration_Processing_Complete__c = false;
        ce.SAMS_Integration_Processing_Complete__c = true;
        ce.Source_Type__c = 'MKT-Batch';
        Insert ce;
        
        List<Course_Connection_Life_Cycle__c> clifecycleList = new List<Course_Connection_Life_Cycle__c>();
        
        Course_Connection_Life_Cycle__c cl1 = new Course_Connection_Life_Cycle__c();
        cl1.Course_Connection__c= ce.Id;
        cl1.Stage__c  = 'Post to Canvas';
        cl1.Status__c  = 'Not Started';
        
        clifecycleList.add(cl1);
        
        Insert clifecycleList;
    } 
    
    
    static testmethod void testBlankRequestJSON(){
        Test.setMock(HttpCalloutMock.class, new canvasCalloutMockError());
        try {
            Test.startTest();
                CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody = new CourseConnCanvasRequestWrapper();
                CanvasEnrollmentRequest req = new CanvasEnrollmentRequest();
                req.sendEnrollDetailToCanvas(canvasEnrollmentRequestBody);
                req.getCanvasEnrollResByCourseConnId();
                req.getTotalNumberOfFailedEnrollments();
                System.assert(false);
             Test.stopTest();    
        } catch (Exception e) {
            System.assert(true);
        }
    }
    
    static testmethod void testCanvasSuccess(){
        Test.setMock(HttpCalloutMock.class, new canvasCalloutMockPositive());
        try {

            List<hed__Course_Enrollment__c> cenrollRec = [SELECT Id,hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c,hed__Contact__r.Student_ID__c,hed__Contact__r.Name,hed__Contact__r.Email FROM hed__Course_Enrollment__c];
            List<CourseConnCanvasRequestWrapper.EnrolDetails> enrolDetailList = new List<CourseConnCanvasRequestWrapper.EnrolDetails>();
            CourseConnCanvasRequestWrapper.EnrolDetails enroll = new CourseConnCanvasRequestWrapper.EnrolDetails();
            enroll.courseConnectionId = cenrollRec[0].Id;
            enroll.courseOfferingId = cenrollRec[0].hed__Course_Offering__c;
            enroll.sisCourseId = cenrollRec[0].hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c;
            enrolDetailList.add(enroll);
            CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody = new CourseConnCanvasRequestWrapper();           
            canvasEnrollmentRequestBody.studentId = cenrollRec[0].hed__Contact__r.Student_ID__c;
            canvasEnrollmentRequestBody.studentName = cenrollRec[0].hed__Contact__r.Name;
            canvasEnrollmentRequestBody.studentEmail = cenrollRec[0].hed__Contact__r.Email;
            canvasEnrollmentRequestBody.enrolmentDetails = enrolDetailList ;
            
            Test.startTest();
                CanvasEnrollmentRequest req = new CanvasEnrollmentRequest();
                req.sendEnrollDetailToCanvas(canvasEnrollmentRequestBody);
                System.assert(true);
            Test.stopTest();     
        } catch (CanvasRequestCalloutException e) {
            // If callout was not successful, an exception would be thrown
            System.assert(false);
        } catch (Exception e) {
            System.assert(false);
        }
    }
    
    static testmethod void testCanvasSuccessEnrollResults(){
        Test.setMock(HttpCalloutMock.class, new canvasCalloutMockPositive());
        try {
 
            List<hed__Course_Enrollment__c> cenrollRec = [SELECT Id,hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c,hed__Contact__r.Student_ID__c,hed__Contact__r.Name,hed__Contact__r.Email FROM hed__Course_Enrollment__c];
            List<CourseConnCanvasRequestWrapper.EnrolDetails> enrolDetailList = new List<CourseConnCanvasRequestWrapper.EnrolDetails>();
            CourseConnCanvasRequestWrapper.EnrolDetails enroll = new CourseConnCanvasRequestWrapper.EnrolDetails();
            enroll.courseConnectionId = cenrollRec[0].Id;
            enroll.courseOfferingId = cenrollRec[0].hed__Course_Offering__c;
            enroll.sisCourseId = cenrollRec[0].hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c;
            enrolDetailList.add(enroll);
            CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody = new CourseConnCanvasRequestWrapper();           
            canvasEnrollmentRequestBody.studentId = cenrollRec[0].hed__Contact__r.Student_ID__c;
            canvasEnrollmentRequestBody.studentName = cenrollRec[0].hed__Contact__r.Name;
            canvasEnrollmentRequestBody.studentEmail = cenrollRec[0].hed__Contact__r.Email;
            canvasEnrollmentRequestBody.enrolmentDetails = enrolDetailList ;
            
            Test.startTest();
                CanvasEnrollmentRequest req = new CanvasEnrollmentRequest();
                req.sendEnrollDetailToCanvas(canvasEnrollmentRequestBody);
                Map<Id, CourseConnCanvasResponseWrapper.Response> courseEnrollResultMap = req.getCanvasEnrollResByCourseConnId();
                system.assert(courseEnrollResultMap.size()>0,true);
                system.assertEquals('Success',courseEnrollResultMap.get(cenrollRec[0].Id).result);        
            Test.stopTest();     
        } catch (CanvasRequestCalloutException e) {
            // If callout was not successful, an exception would be thrown
            System.assert(false);
        } catch (Exception e) {
            System.assert(false);
        }
    }
    
    static testmethod void testCanvasBlankResponse1(){
        
        try {
            Test.startTest();
                CanvasEnrollmentRequest req = new CanvasEnrollmentRequest();
                req.getCanvasEnrollResByCourseConnId();
                req.getTotalNumberOfFailedEnrollments();
            Test.stopTest();     
        } catch (ArgumentException e) {
            // If canvas response is null
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }
    
    static testmethod void testCanvasBlankResponse2(){
        
        try {
            Test.startTest();
                CanvasEnrollmentRequest req = new CanvasEnrollmentRequest();
                req.getTotalNumberOfFailedEnrollments();
            Test.stopTest();     
        } catch (ArgumentException e) {
            // If canvas response is null
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }
    
    static testmethod void testCanvasFailCount(){
        Test.setMock(HttpCalloutMock.class, new canvasCalloutMockNegative());
        try {
            List<hed__Course_Enrollment__c> cenrollRec = [SELECT Id,hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c,hed__Contact__r.Student_ID__c,hed__Contact__r.Name,hed__Contact__r.Email FROM hed__Course_Enrollment__c];
            List<CourseConnCanvasRequestWrapper.EnrolDetails> enrolDetailList = new List<CourseConnCanvasRequestWrapper.EnrolDetails>();
            CourseConnCanvasRequestWrapper.EnrolDetails enroll = new CourseConnCanvasRequestWrapper.EnrolDetails();
            enroll.courseConnectionId = cenrollRec[0].Id;
            enroll.courseOfferingId = cenrollRec[0].hed__Course_Offering__c;
            enroll.sisCourseId = cenrollRec[0].hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c;
            enrolDetailList.add(enroll);
            CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody = new CourseConnCanvasRequestWrapper();           
            canvasEnrollmentRequestBody.studentId = cenrollRec[0].hed__Contact__r.Student_ID__c;
            canvasEnrollmentRequestBody.studentName = cenrollRec[0].hed__Contact__r.Name;
            canvasEnrollmentRequestBody.studentEmail = cenrollRec[0].hed__Contact__r.Email;
            canvasEnrollmentRequestBody.enrolmentDetails = enrolDetailList ;
            Test.startTest();
                
                CanvasEnrollmentRequest req = new CanvasEnrollmentRequest();
                req.sendEnrollDetailToCanvas(canvasEnrollmentRequestBody);
                Integer failCount = req.getTotalNumberOfFailedEnrollments();
                system.assertEquals(1,failCount);   
             Test.stopTest();    
        } catch (Exception e) {
            System.assert(true);
        }
    }
    
    static testmethod void canvasError(){
        Test.setMock(HttpCalloutMock.class, new calloutMockError());
        try {
            List<hed__Course_Enrollment__c> cenrollRec = [SELECT Id,hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c,hed__Contact__r.Student_ID__c,hed__Contact__r.Name,hed__Contact__r.Email FROM hed__Course_Enrollment__c];
            List<CourseConnCanvasRequestWrapper.EnrolDetails> enrolDetailList = new List<CourseConnCanvasRequestWrapper.EnrolDetails>();
            CourseConnCanvasRequestWrapper.EnrolDetails enroll = new CourseConnCanvasRequestWrapper.EnrolDetails();
            enroll.courseConnectionId = cenrollRec[0].Id;
            enroll.courseOfferingId = cenrollRec[0].hed__Course_Offering__c;
            enroll.sisCourseId = cenrollRec[0].hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c;
            enrolDetailList.add(enroll);
            CourseConnCanvasRequestWrapper canvasEnrollmentRequestBody = new CourseConnCanvasRequestWrapper();           
            canvasEnrollmentRequestBody.studentId = '12345';
            canvasEnrollmentRequestBody.studentName = cenrollRec[0].hed__Contact__r.Name;
            canvasEnrollmentRequestBody.studentEmail = cenrollRec[0].hed__Contact__r.Email;
            canvasEnrollmentRequestBody.enrolmentDetails = enrolDetailList ;
            Test.startTest();
                
                CanvasEnrollmentRequest req = new CanvasEnrollmentRequest();
                req.sendEnrollDetailToCanvas(canvasEnrollmentRequestBody);

             Test.stopTest();    
        } catch (Exception e) {
            System.assert(true);
        }
    }
    
    private class canvasCalloutMockPositive implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            List<hed__Course_Enrollment__c> cenroll = [Select Id,hed__Contact__r.Student_ID__c from hed__Course_Enrollment__c where hed__Contact__r.Student_ID__c = '1235'];
            String jsonBody ='{"payload":{"studentId":"1235","enrolmentResponse":[{"courseConnectionId":"'+cenroll[0].id+'","result":"Success","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":""}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    private class canvasCalloutMockNegative implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            List<hed__Course_Enrollment__c> cenroll = [Select Id,hed__Contact__r.Student_ID__c from hed__Course_Enrollment__c where hed__Contact__r.Student_ID__c = '1235'];
            String jsonBody ='{"payload":{"studentId":"1235","enrolmentResponse":[{"courseConnectionId":"'+cenroll[0].id+'","result":"Error","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":"Course doesnot exist"}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    
    private class canvasCalloutMockError implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"payload":{"studentId":"551235","enrolmentResponse":[{}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatus('Bad Request');
            response.setStatusCode(404);
            return response;
        }
        
    }
    
    public class calloutMockError implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {              
            HttpResponse response = new HttpResponse();
            String jsonBody = '{"id": "ID-devqa-e503a961-6dc9-4d8f-b4ee-c2d723cafd4d","result": "ERROR","code": "500", "payload": "Error occured while getting User details from Canvas. (User not present)"}';
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatus('Success');
            response.setStatusCode(200);
            return response;
        }
        
    }
}