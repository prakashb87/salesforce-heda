/*-----------------------------------------------------------------------
File name:      CaseTriggerHandler.cls
Author:         SF HEDA Team
Company:        Capgemini  
Description:    
Test Class:     CaseTrigger_Test.cls
Created Date :  16/08/2017
History

<Date>            <Authors Name>            <Brief Description of Change>
-----------------------------------------------------------------------*/

public with sharing class CaseTriggerHandler {

    public void OnBeforeInsert(Case[] newObjects){
        set<Id> setCon = new set<Id>();     
        map<Id,Case> mapCase = new map<Id,Case>();
        map<Id,contact> conList = new map<Id,contact>();
        for(case objCase : newObjects)
        {        
            setCon.add(objCase.ContactId);
        }
        conList =new map<Id,Contact>([select Id, First_in_family_at_university__c, Early_highschool_leaver__c, Previous_low_GPA__c, Employed_fulltime_or_time_poor__c,
                    Unemployed_or_unskilled__c, Course_engagement_level__c, Lives_20km_or_further_from_RMIT__c from contact where Id in : setCon ]);
        for (case caseObj: newObjects)
        {
            if (caseObj.ContactId <> null)
            {                
                if(caseObj.Status <> null && (caseObj.Status == Label.InProgressStatus|| caseObj.Status == Label.NewStatus|| caseObj.Status == Label.EscalatedStatus||caseObj.Status == Label.OnHoldStatus))
                {                    
                    caseObj.First_in_family_at_university__c= conList.get(caseObj.ContactId).First_in_family_at_university__c  ;
                    caseObj.Early_highschool_leaver__c = conList.get(caseObj.ContactId).Early_highschool_leaver__c;
                    caseObj.Previous_low_GPA__c = conList.get(caseObj.ContactId).Previous_low_GPA__c  ;
                    caseObj.Employed_fulltime_or_time_poor__c = conList.get(caseObj.ContactId).Employed_fulltime_or_time_poor__c  ;
                    caseObj.Unemployed_or_unskilled__c = conList.get(caseObj.ContactId).Unemployed_or_unskilled__c  ;
                    caseObj.Course_engagement_level__c = conList.get(caseObj.ContactId).Course_engagement_level__c ;
                    caseObj.Lives_20km_or_further_from_RMIT__c = conList.get(caseObj.ContactId).Lives_20km_or_further_from_RMIT__c ;
                    
                }
                
            }
        }
    }
    public void OnAfterInsert(Case[] newObjects){
    set<Id> setCon = new set<Id>();
    map<Id,Contact> lstConUpdate = new map<Id,Contact>();
    map<Id,Case> mapCase = new map<Id,Case>();
    map<Id,contact> conList = new map<Id,contact>();
    for(case objCase : newObjects)
    {
        setCon.add(objCase.ContactId);
    }
    conList =new map<Id,Contact>([select Id, First_in_family_at_university__c, Early_highschool_leaver__c, Previous_low_GPA__c, Employed_fulltime_or_time_poor__c,
                Unemployed_or_unskilled__c, Course_engagement_level__c, Lives_20km_or_further_from_RMIT__c from contact where Id in : setCon ]);
    for (case caseObj: newObjects)
    {
        if (caseObj.ContactId <> null)
        {
            if(caseObj.Status <> null && (caseObj.Status == Label.InProgressStatus|| caseObj.Status == Label.NewStatus|| caseObj.Status == Label.EscalatedStatus||caseObj.Status == Label.OnHoldStatus))
            {
                conList.get(caseObj.ContactId).High_Risk__c = true; 
                lstConUpdate.put(caseObj.ContactId,conList.get(caseObj.ContactId));
            }
        }
    }
    if(lstConUpdate.size()>0)
    {
        database.update(lstConUpdate.values(),false);
    }

   }

    public void OnAfterUpdate(map<Id,Case> oldObjects, Case[] updatedObjects){
        set<Id> setCon = new set<Id>();
        map<Id,Contact> lstConUpdate = new map<Id,Contact>();
        map<Id,Case> mapCase = new map<Id,Case>();
        map<Id,contact> conList = new map<Id,contact>();
        for(case objCase : updatedObjects)
        {
            if(objCase.ContactId != null){
            	setCon.add(objCase.ContactId);
            }
        }
        conList =new map<Id,Contact>([select Id, First_in_family_at_university__c, Early_highschool_leaver__c, Previous_low_GPA__c, Employed_fulltime_or_time_poor__c,
                    Unemployed_or_unskilled__c, Course_engagement_level__c, Lives_20km_or_further_from_RMIT__c from contact where Id in : setCon ]); 
        mapCase = new map<Id,Case>([select id, Status from Case where ContactId in : setCon and (Status = 'In Progress' or Status = 'Escalated' OR Status = 'On Hold' OR Status = 'New')]);
        for (Case caseObj: updatedObjects)
        {
            if (caseObj.ContactId <> null)
            { 
                if(caseObj.First_in_family_at_university__c != oldObjects.get(caseObj.Id).First_in_family_at_university__c ||
                    caseObj.Early_highschool_leaver__c != oldObjects.get(caseObj.Id).Early_highschool_leaver__c ||
                    caseObj.Previous_low_GPA__c != oldObjects.get(caseObj.Id).Previous_low_GPA__c ||
                    caseObj.Employed_fulltime_or_time_poor__c != oldObjects.get(caseObj.Id).Employed_fulltime_or_time_poor__c ||
                    caseObj.Unemployed_or_unskilled__c != oldObjects.get(caseObj.Id).Unemployed_or_unskilled__c ||
                    caseObj.Course_engagement_level__c != oldObjects.get(caseObj.Id).Course_engagement_level__c ||
                    caseObj.Lives_20km_or_further_from_RMIT__c != oldObjects.get(caseObj.Id).Lives_20km_or_further_from_RMIT__c ||                 
                    caseObj.Assessed_risk_level__c != oldObjects.get(caseObj.Id).Assessed_risk_level__c ) 
                    {                
                        conList.get(caseObj.ContactId).First_in_family_at_university__c = caseObj.First_in_family_at_university__c;
                        conList.get(caseObj.ContactId).Early_highschool_leaver__c = caseObj.Early_highschool_leaver__c;
                        conList.get(caseObj.ContactId).Previous_low_GPA__c = caseObj.Previous_low_GPA__c;
                        conList.get(caseObj.ContactId).Employed_fulltime_or_time_poor__c = caseObj.Employed_fulltime_or_time_poor__c;
                        conList.get(caseObj.ContactId).Unemployed_or_unskilled__c = caseObj.Unemployed_or_unskilled__c;
                        conList.get(caseObj.ContactId).Course_engagement_level__c = caseObj.Course_engagement_level__c;
                        conList.get(caseObj.ContactId).Lives_20km_or_further_from_RMIT__c = caseObj.Lives_20km_or_further_from_RMIT__c;
                        conList.get(caseObj.ContactId).Assessed_risk_level__c = caseObj.Assessed_risk_level__c;
                        lstConUpdate.put(caseObj.ContactId,conList.get(caseObj.ContactId));
                    }
                  if(caseObj.Status <> null && (caseObj.Status == Label.InProgressStatus|| caseObj.Status == Label.NewStatus|| caseObj.Status == Label.EscalatedStatus||caseObj.Status == Label.OnHoldStatus))
                  {
                       if(caseObj.Status != oldObjects.get(caseObj.Id).Status )
                        {  
                            conList.get(caseObj.ContactId).High_Risk__c = true; 
                            lstConUpdate.put(caseObj.ContactId,conList.get(caseObj.ContactId));
                        }
                  }
                  if(caseObj.Status <> null && caseObj.Status == Label.ClosedStatus && caseObj.Status != oldObjects.get(caseObj.Id).Status )
                     {
                        if(mapCase.size()>0)
                        {
                            conList.get(caseObj.ContactId).High_Risk__c = true; 
                        }
                        else
                        {
                           conList.get(caseObj.ContactId).High_Risk__c = false; 
                             
                        }
                        lstConUpdate.put(caseObj.ContactId,conList.get(caseObj.ContactId));                     
                     }
                }
            }
            if(lstConUpdate.size()>0)
            {
                database.update(lstConUpdate.values(),false);
            }
    }
        
    
}