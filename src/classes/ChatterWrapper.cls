/**************************************************************
Purpose    : The wrapper class is for displaying the chatter comments on the portal
Created by : Prakash 15/03/2019
History    :
*************************************************************/

public class ChatterWrapper implements Comparable{

        @AuraEnabled public String userName { get; set; } 
        @AuraEnabled public String timeStamp { get; set; }
        @AuraEnabled public DateTime sortTimeStamp { get; set; }
        @AuraEnabled public String comment { get; set; }
        @AuraEnabled public String userEmail { get; set; }
        @AuraEnabled public String userId { get; set; }
        @AuraEnabled public String photoURL { get; set; }
        @AuraEnabled public String feedId { get; set; }
        @AuraEnabled public String userRole { get; set; }
        @AuraEnabled public String feedItemId { get; set;}
        @AuraEnabled public List<ChatterWrapper.feedComments> feedComments { get; set; }
        
        public class feedComments{
            @AuraEnabled public ChatterWrapper feedComment { get; set; }
        }
    
        public Integer compareTo(Object compareTo) {
                ChatterWrapper compareToComment = (ChatterWrapper )compareTo;
                if (sortTimeStamp  == compareToComment.sortTimeStamp ){ return 0; }
                if (sortTimeStamp  < compareToComment.sortTimeStamp  ){ return 1; }
                return -1;        
        }
    
}