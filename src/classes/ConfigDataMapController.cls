/**    
 * @author Resmi Ramakrishnan                    
 * @date 18/05/2018               
 * @description This class is using get value from Config_Data_Map__c CustomSetting
 */
public class ConfigDataMapController {

    public static String getCustomSettingValue( String customSettingName) {
		Config_Data_Map__c configDataMapObj = Config_Data_Map__c.getValues(customSettingName);
        if (configDataMapObj != null) {
            return configDataMapObj.Config_Value__c;
        } else {
            return null;
        }
    }

}