/*****************************************************************************************************
*** @Class             : CourseConnCanvasRequestWrapper
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending course SIS Id, Course Offering Id's
*** @Created date      : 13/06/2018
*** @JIRA ID           : ECB-367
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to create a request structure to send to Ipaas
*****************************************************************************************************/
public class CourseConnCanvasRequestWrapper 
{
    
    public String studentId;
    public String studentName;
    public String studentEmail;
    public String accountId;
    public String type;
    public String enrollment_state;
    public String notify;
    public List<EnrolDetails> enrolmentDetails = new List<EnrolDetails>();
    
    public Class EnrolDetails
    {
        public Id courseConnectionId;
        public String courseOfferingId;
        public String sisCourseId;

    }
    public static CourseConnCanvasRequestWrapper parse(String json) 
    {
        return (CourseConnCanvasRequestWrapper ) System.JSON.deserialize(json, CourseConnCanvasRequestWrapper.class);
    }
    
}