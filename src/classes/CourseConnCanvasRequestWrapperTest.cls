/*****************************************************************************************************
*** @Class             : CourseConnCanvasRequestWrapperTest
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending course SIS Id, Course Offering Id's
*** @Created date      : 13/06/2018
*** @JIRA ID           : ECB-367
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** Test class to create a canvas request structure to send to Ipaas
*****************************************************************************************************/

@IsTest
public class CourseConnCanvasRequestWrapperTest {
	
       
	static testMethod void testParse() {
		String json = '{'+
		''+
		'	\"studentId\":\"34\",'+
		'	\"studentName\":\"abc\",'+
		'	\"studentEmail\":\"'+Label.Email+'\",'+
		'	\"accountId\":\"34\",'+
		'	\"enrolmentDetails\":[  '+
		'	  {  '+
		'		 \"courseConnectionId\":\"\",'+
		'		 \"courseOfferingId\":\"'+Label.CourseOfferingID+'\"'+
		'	  },'+
		'	  {  '+
		'		 \"courseConnectionId\":\"\",'+
		'		 \"courseOfferingId\":\"'+Label.CourseOfferingID+'\"'+
		'	  }'+
		'	]'+
		'}';	
		CourseConnCanvasRequestWrapper obj = CourseConnCanvasRequestWrapper.parse(json);
		System.assert(obj != null);
	}
}