/*****************************************************************************************************
*** @Class             : CourseConnCanvasResponseWrapper
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending course SIS Id, Course Offering Id's
*** @Created date      : 13/06/2018
*** @JIRA ID           : ECB-367
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to create a response structure that is received from Ipaas
*****************************************************************************************************/

public class CourseConnCanvasResponseWrapper 
{

	public String id;
	public String result;
	public String code;
	public String application;
	public String provider;
	public ResponseInfo payload;
	
	public class ResponseInfo 
    {
		public String studentId;
		public List<Response> enrolmentResponse;
	}
	
	public class Response 
    {
		public String courseConnectionId;
		public String result;
		public String html_url;
		public String errorMessage;
	}

		
	public static CourseConnCanvasResponseWrapper parse(String json) 
    {
		return (CourseConnCanvasResponseWrapper) System.JSON.deserialize(json, CourseConnCanvasResponseWrapper.class);
	}
}