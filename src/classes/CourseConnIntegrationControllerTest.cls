@isTest
private class CourseConnIntegrationControllerTest {

	/*
	@isTest(SeeAllData=true)
	static void test_sendCourseConn_hasError() {

		hed__Course_Enrollment__c cc = new hed__Course_Enrollment__c();
		insert cc;

		Course_Connection_Life_Cycle__c cclc = New Course_Connection_Life_Cycle__c();
		cclc.Course_Connection__c = cc.id;
		cclc.Stage__c = 'Post to SAMS';
		cclc.Status__c = 'Error';
		insert cclc;

        Test.setMock(HttpCalloutMock.class, new SAMSHttpCalloutMock());


        Test.startTest();   
        CourseConnectionIntegrationController.sendCourseConn(cc.id);
        Test.stopTest(); 
	}
	*/


	private class SAMSHttpCalloutMock implements HttpCalloutMock {
		// Implement this interface method
		public HTTPResponse respond(HTTPRequest request) {
			// Create a fake response
			HttpResponse response = new HttpResponse();
			response.setHeader('Content-Type', 'application/json');
			response.setBody('{ "id" : "ID-dev-6r87t87587" , "result" : "success", "code" : "200", "application" : "rmit-system-api-sfdc", "provider" : "SAMS", "payload" : [ { "courseConnectionId": "a3e5D000000NUNd", "result": "Success", "errorCode": "", "errorText": "" } ] }');
			response.setStatusCode(200);
			return response;
		}
	}


}