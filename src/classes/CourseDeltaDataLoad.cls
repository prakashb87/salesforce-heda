/*****************************************************************************************************
*** @Class             : CourseDeltaDataLoad
*** @Author            : Praneet Vig
*** @Requirement       : scheduler to sends newly created or updated courseIds to IPAAS API 
*** @Created date      : 21/02/2018
*** @JIRA ID           : ECB-30
*** @LastModifiedBy    : Rishabh Anand
*** @Created date      : 26/09/2018
*** @JIRA ID           : ECB-4431
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is future class to send course ID to IPASS API.Its called from apex schduler class scheduledCourse
*****************************************************************************************************/ 
public without sharing class CourseDeltaDataLoad {
    
    /********************************************************************
// Purpose              : Send delta load course data to IPASS                             
// Author               : Capgemini [Avinash] / Rishabh
// Parameters           : null
// Returns              : void
//JIRA Reference        : ECB-3718 - Update existing delta trigger for passing product cataloge details to iPaaS  and ECB - 4431 Implement program sync delta batch job in SF  
//********************************************************************/ 
    @future(callout=true)
    public static void sendCourseAsync(){
        
        Integration_Logs__c requestLogObjectToInsert = new Integration_Logs__c();
        Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c(); 
        
        DateTime dt = System.today()-1;
        System.debug('TTTTT: Yesterdays Date Time = '+dt);
        
        List<CourseWrapper> courses = new List<CourseWrapper>();
        Map<String,String> parentCourseMap= new Map<String,String>();
        List<hed__Course__c> courseList;
        List<hed__Course_Offering__c> courseOfferingList;
        
        //Query For Course List Last Modified Date > Yesterday And Course Record type Either 21CC or RMIT Online 
        //Added RMIT_Training record type to the all th course related queries to resolve ECB-4654: Date: 25-Oct-18:: Upendra
        if(Schema.sObjectType.hed__Course__c.isAccessible()){
            courseList = [SELECT Assessment__c,Capability_Home__c,Concepts__c,Course_Prerequisites__c,Effort__c,Enquiry_Email__c,Enquiry_Only__c,Id,Image_URL__c,Industry__c,Mode_of_Delivery__c,Name,Overview__c,Points__c,Price__c,Pricing_Model__c,Publish_Date__c,Skills_Acquired__c,Skills__c,Subject__c,Tagline__c,Video_URL__c FROM hed__Course__c WHERE   (Product_Line__c = '21cc Credential' OR Product_Line__c ='RMIT Online' OR Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        }
        System.debug('List of Courses modified = '+ courseList);
     
        //Query For Course Relationship List Last Modified Date > Yesterday And Related Course Course Record type Either 21CC or RMIT Online
        List<Course_Relationship__c> courseRelationshipList = [SELECT Course__c,Course__r.Id,Course__r.Name,Related_Course__c,Related_Course__r.Id,Related_Course__r.Name FROM Course_Relationship__c WHERE (Related_Course__r.Product_Line__c ='21cc Credential' OR Related_Course__r.Product_Line__c ='RMIT Online' OR Related_Course__r.Product_Line__c ='RMIT Training') and (Course__r.Product_Line__c ='21cc Credential' OR Course__r.Product_Line__c ='RMIT Online' OR Course__r.Product_Line__c ='RMIT Training') and Course__c != null];
        System.debug('List Of Related Courses--->'+courseRelationshipList);
        for( Course_Relationship__c courseRelObj : courseRelationshipList ) 
        {
            parentCourseMap.put( String.valueOf(courseRelObj.Course__c), courseRelObj.Course__r.Name);
        }

        
        //Query For CourseOffering List Last Modified Date > Yesterday And Course Record type Either 21CC or RMIT Online
        if(Schema.sObjectType.hed__Course_Offering__c.isAccessible()){
            courseOfferingList =  [SELECT hed__End_Date__c,hed__Start_Date__c,Id,Name,Status__c,hed__Course__r.Id,hed__Course__r.Name FROM hed__Course_Offering__c WHERE (hed__Course__r.Product_Line__c ='21cc Credential'  OR hed__Course__r.Product_Line__c ='RMIT Online' OR hed__Course__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        }
        System.debug('List of Courses Offerings modified = '+courseOfferingList);
        
        List<Course_Testimonial__c> courseTestimonialList;
        //Query For Course Testimonial List Last Modified Date > Yesterday And Course Record type Either 21CC or RMIT Online
        if(Schema.sObjectType.Course_Testimonial__c.isAccessible()){
            courseTestimonialList = [SELECT Id,Name,Status__c,Testimonial__c,Course__r.Id,Course__r.Name FROM Course_Testimonial__c  WHERE (Course__r.Product_Line__c ='21cc Credential'  OR Course__r.Product_Line__c ='RMIT Online' OR Course__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        }
        System.debug('List of Courses Testimonials modified = '+courseTestimonialList); 
        
        List<Course_Offering_Session__c> courseOfferingSessionList;
        //Query For Course Offering Session List Last Modified Date > Yesterday And Course Record type Either 21CC or RMIT Online
        if(Schema.sObjectType.Course_Offering_Session__c.isAccessible()){
            courseOfferingSessionList =  [SELECT End_Time__c,Hours__c,Id,Start_Time__c,Status__c,Venue__c,Course_Offering__r.hed__Course__r.Id,Course_Offering__r.hed__Course__r.Name FROM Course_Offering_Session__c WHERE (Course_Offering__r.hed__Course__r.Product_Line__c ='21cc Credential'  OR Course_Offering__r.hed__Course__r.Product_Line__c ='RMIT Online' OR Course_Offering__r.hed__Course__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        }
        //System.debug('List of Courses Offerings modified = ' + courseOfferingSessionList);
        
        List<Course_Industry_Partner__c> courseIndustryPartnerList;
        // Quering the List  of Course Industry Partner Records And Course Record type Either 21CC or RMIT Online
        if(Schema.sObjectType.Course_Industry_Partner__c.isAccessible()){
             courseIndustryPartnerList =[Select Id, Industry_Partner__c, Course_Id__r.Id, Course_Id__r.Name,Course_Id__r.RecordType.DeveloperName from Course_Industry_Partner__c WHERE (Course_Id__r.Product_Line__c ='21cc Credential'  OR Course_Id__r.Product_Line__c ='RMIT Online' OR Course_Id__r.Product_Line__c ='RMIT Training')];
        }
        System.debug('All List of Courses Industry Partners ='+courseIndustryPartnerList);
        
        Set<id> crsindprtnr = new Set<id>();
        map<id, id> partnerCourseId = new map<id,id>();
        map<id, string> partnerCourseName = new map<id, string>();
        
        if( !courseIndustryPartnerList.IsEmpty())   
        {
            for(Course_Industry_Partner__c ciprtnr : courseIndustryPartnerList)
            {
                crsindprtnr.add(ciprtnr.Industry_Partner__c);
                partnerCourseId.put(ciprtnr.Industry_Partner__c, ciprtnr.Course_Id__r.Id);
                partnerCourseName.put(ciprtnr.Industry_Partner__c, ciprtnr.Course_Id__r.name);
            }
        }
        
        List <Account> accList;
        //Changes on Account Object - Example If Account record name changes that changes will be reflecting on course industry partner object 
        if(Schema.sObjectType.Account.isAccessible()){ 
            accList = [SELECT Market_Place_Partner_Logo_URL__c,Name,Partner_Description_for_Market_Place__c,Status__c from Account WHERE Id IN: crsindprtnr AND LastModifiedDate > :dt];
        }
        System.debug('Account List=' + accList);
        if(!accList.IsEmpty())
        {
            for(Account acc : accList)
            { 
                if( acc != null )
                {
                    CourseWrapper crswpr = new CourseWrapper();
                    crswpr.ObjectName = 'coursePartnerAccount';
                    crswpr.key.put('courseId',partnerCourseId.get(acc.id));
                    crswpr.key.put('courseName',partnerCourseName.get(acc.id));
                    crswpr.key.put('coursePartnerAccountId',acc.id);
                   
                    courses.add(crswpr);
                }
            }
        }
        // Changes direcly on Course Industry partner record - if record directly updated or new record added 
        List<Course_Industry_Partner__c> courseIndPartnerModified =[Select Id, Industry_Partner__c, Course_Id__r.Id, Course_Id__r.Name,Course_Id__r.RecordType.DeveloperName from Course_Industry_Partner__c WHERE (Course_Id__r.Product_Line__c ='21cc Credential'  OR Course_Id__r.Product_Line__c ='RMIT Online' OR Course_Id__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        System.debug('List of Courses Industry Partners Modified ='+ courseIndPartnerModified );
        if(!courseIndPartnerModified.IsEmpty())
        {
            for(Course_Industry_Partner__c ccInParModified : courseIndPartnerModified)
            {
                if( ccInParModified != null )
                {
                    CourseWrapper crswpr = new CourseWrapper();
                    crswpr.ObjectName = 'coursePartnerAccount';
                    crswpr.key.put('courseId', ccInParModified.Course_Id__r.Id);
                    crswpr.key.put('courseName', ccInParModified.Course_Id__r.Name);
                    crswpr.key.put('coursePartnerAccountId', ccInParModified.Industry_Partner__c);
                    
                    courses.add(crswpr);  
                }
            } 
        }
        
        Id academicProgramRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
        
        //Query For Program List Last Modified Date > Yesterday and Record Type = Academic Program
        List<Account> listOfProgramLastmodified = [SELECT Name,ECB_Program_Assessment__c,ECB_Program_Concepts__c,ECB_Program_Delivery_Mode__c,ECB_Program_Effort__c,ECB_Program_Enquiry_Email__c,ECB_Program_Image_URL__c,ECB_Program_Industry__c,ECB_Program_Interest_Area__c,ECB_Program_Overview__c,ECB_Program_Points__c,ECB_Program_Prerequisites__c,ECB_Program_Skills__c,ECB_Program_Tagline__c,ECB_Program_Video_URL__c,ParentId,Status__c FROM Account WHERE LastModifiedDate > :dt AND RecordTypeId =:academicProgramRecordTypeId AND (Product_Line__c = '21cc Credential' OR Product_Line__c ='RMIT Online' OR Product_Line__c ='RMIT Training')];
        System.debug('List of Program modified = '+ listOfProgramLastmodified);  
        
        //Query For Program Plan List Last Modified Date > Yesterday  and Program Record Type = Academic Program
        List<hed__Program_Plan__c> programPlanList=  [SELECT hed__Account__c,hed__Account__r.Name,hed__Account__r.Id,hed__Is_Primary__c,Id FROM hed__Program_Plan__c WHERE hed__Account__r.RecordType.DeveloperName ='Academic_Program' AND (hed__Account__r.Product_Line__c = '21cc Credential' OR hed__Account__r.Product_Line__c ='RMIT Online' OR hed__Account__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        System.debug('List of Program Plans Modified = '+programPlanList);  
        
        //Query For Program Requirement List Last Modified Date > Yesterday and Program Record Type = Academic Program
        List<hed__Plan_Requirement__c> planReqirementList=  [SELECT hed__Course__c,hed__Program_Plan__r.hed__Account__r.Name,hed__Program_Plan__r.hed__Account__r.Id,hed__Program_Plan__c,hed__Sequence__c,Id FROM hed__Plan_Requirement__c WHERE hed__Program_Plan__r.hed__Account__r.RecordType.DeveloperName ='Academic_Program' AND (hed__Program_Plan__r.hed__Account__r.Product_Line__c = '21cc Credential' OR hed__Program_Plan__r.hed__Account__r.Product_Line__c ='RMIT Online' OR hed__Program_Plan__r.hed__Account__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        System.debug('List of Program Plans Modified = '+planReqirementList);
        
        List<Program_Testimonial__c> programTestimonialList;
        //Query For Program Testimonial List Last Modified Date > Yesterday and Program Record Type = Academic Program
        if(Schema.sObjectType.Program_Testimonial__c.isAccessible()){
            programTestimonialList=  [SELECT Id,Name,Status__c,Testimonial__c,Program__r.Name,Program__r.Id FROM Program_Testimonial__c WHERE Program__r.RecordType.DeveloperName = 'Academic_Program' AND (Program__r.Product_Line__c = '21cc Credential' OR Program__r.Product_Line__c ='RMIT Online' OR Program__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        }
        System.debug('List of Program Testimonial Modified = '+programTestimonialList); 
        
        List<Program_Industry_Partner__c> industryPlanList;
        //Query For Program Industry Partner List  WHERE RecordType.DeveloperName = 'Academic Program'
        if(Schema.sObjectType.Program_Industry_Partner__c.isAccessible()){
            industryPlanList=  [SELECT Industry_Partner__c,Program__c,Program__r.Id,Program__r.Name FROM Program_Industry_Partner__c WHERE    Program__r.RecordType.DeveloperName = 'Academic_Program' AND (Program__r.Product_Line__c = '21cc Credential' OR Program__r.Product_Line__c ='RMIT Online' OR Program__r.Product_Line__c ='RMIT Training')];
        }
        System.debug('List of Industry Plans Modified = '+industryPlanList);    
        
        Set<id> programIndustryPartnerId = new Set<id>();
        map<id, id> partnerProgramId = new map<id,id>();
        map<id, string> partnerProgramName = new map<id, string>();
        
        
        if( !industryPlanList.IsEmpty())   
        {
            for(Program_Industry_Partner__c programIndustry : industryPlanList)
            {
                programIndustryPartnerId.add(programIndustry.Industry_Partner__c);
                partnerProgramId.put(programIndustry.Industry_Partner__c, programIndustry.Program__r.Id);
                partnerProgramName.put(programIndustry.Industry_Partner__c, programIndustry.Program__r.name);
            }
        }
        
        //Changes on Account Object - Example If Account record name changes that changes will be reflecting on Program industry partner object 
        List <Account> accountList = [SELECT Market_Place_Partner_Logo_URL__c,Name,Partner_Description_for_Market_Place__c,Status__c from Account WHERE Id IN: programIndustryPartnerId AND LastModifiedDate > :dt];
        System.debug('Account List=' + accountList);
        if(!accountList.IsEmpty())
        {
            for(Account acc : accountList)
            { 
                if( acc != null )
                {
                    CourseWrapper crswpr = new CourseWrapper();
                    crswpr.ObjectName = 'programPartnerAccount';
                    crswpr.key.put('programId',partnerProgramId.get(acc.id));
                    crswpr.key.put('programName',partnerProgramName.get(acc.id));
                    crswpr.key.put('programPartnerAccountId',acc.id);
                   
                    courses.add(crswpr);
                }
            }
        } 
        
        List<Program_Industry_Partner__c> industryPlanListModified;  
        // Changes directly on Program Industry partner record - if record directly updated or new record added
        if(Schema.sObjectType.Program_Industry_Partner__c.isAccessible()){
            industryPlanListModified=  [SELECT Industry_Partner__c,Program__c, Program__r.RecordType.DeveloperName,Program__r.Id,Program__r.Name FROM Program_Industry_Partner__c WHERE Program__r.RecordType.DeveloperName = 'Academic_Program' AND (Program__r.Product_Line__c = '21cc Credential' OR Program__r.Product_Line__c ='RMIT Online' OR Program__r.Product_Line__c ='RMIT Training') AND LastModifiedDate > :dt];
        }
        if(!industryPlanListModified.IsEmpty())
        {
            for(Program_Industry_Partner__c ccInParModified : industryPlanListModified)
            {
                if( ccInParModified != null )
                {
                    CourseWrapper crswpr = new CourseWrapper();
                    crswpr.ObjectName = 'programPartnerAccount';
                    crswpr.key.put('programId', ccInParModified.Program__r.Id);
                    crswpr.key.put('programName', ccInParModified.Program__r.Name);
                    crswpr.key.put('programPartnerAccountId', ccInParModified.Industry_Partner__c);
                   
                    courses.add(crswpr);
                    
                }
                
            }
            
        }       
        
        //Adding Courses to the CourseWrapper 
        CourseWrapper cw = New CourseWrapper();
        if(!courseList.isEmpty())
        {
            for(hed__Course__c c : courseList )
            {
                cw = new CourseWrapper();
                cw.ObjectName = 'course';
                cw.key.put('courseId',c.Id);
                cw.key.put('courseName',c.Name);
                courses.add(cw);
            }
        }
        
        //Adding Course Offering to the CourseWrapper 
        if(!courseOfferingList.isEmpty())
        {
            for(hed__Course_Offering__c co : courseOfferingList)
            {
                CourseWrapper cow = new CourseWrapper();
                cow.ObjectName = 'courseOffering';
                cow.key.put('courseId',co.hed__Course__r.Id);
                cow.key.put('courseName',co.hed__Course__r.Name);
                cow.key.put('courseOfferingId',co.Id);
                courses.add(cow);
            }
        }
        
        //Adding Course Testimonial to the CourseWrapper 
        if(!courseTestimonialList.isEmpty())
        {
            for(Course_Testimonial__c ct : courseTestimonialList)
            {
                CourseWrapper crswp = new CourseWrapper();
                crswp.ObjectName = 'courseTestimonial';
                crswp.key.put('courseId',ct.Course__r.Id);
                crswp.key.put('courseName',ct.Course__r.Name);
                crswp.key.put('testimonialId',ct.Id);
                courses.add(crswp);
            }  
        } //data for industry partner    
        if(!listOfProgramLastmodified.isEmpty())
        {
            for(Account program : listOfProgramLastmodified )
            {
                CW= new CourseWrapper();
                cw.ObjectName = 'program';
                cw.key.put('programId',program.Id);
                cw.key.put('programName',program.Name);
                courses.add(cw);
            }
        }
        System.debug('>> programPlanList size>>> '+programPlanList.size());
        System.debug('>> programPlanList >>> '+programPlanList);
        if(!programPlanList.isEmpty())
        {
            for(hed__Program_Plan__c programplan : programPlanList )
            {
                CW= new CourseWrapper();
                cw.ObjectName = 'programPlan';
                cw.key.put('programId',programplan.hed__Account__r.Id);
                cw.key.put('programName',programplan.hed__Account__r.Name);
                cw.key.put('programPlanId',programplan.Id);
                courses.add(cw);
            }
        }
        
        System.debug('>> planReqirementList size>>> '+planReqirementList.size());
        System.debug('>> planReqirementList >>> '+planReqirementList);
       
        // code block commeneted to change the JSON structure to resolve ECB-4639: Date: 23-Oct-18:: Upendra
        /* if(!planReqirementList.isEmpty())
        {
            for(hed__Plan_Requirement__c programRequirement : planReqirementList )
            {
                CW= new CourseWrapper();
                cw.ObjectName = 'planRequirement';
                cw.key.put('programId',programRequirement.hed__Program_Plan__r.hed__Account__r.Id);
                cw.key.put('programName',programRequirement.hed__Program_Plan__r.hed__Account__r.Name);
                cw.key.put('programPlanId',programRequirement.hed__Program_Plan__r.Id);
                cw.key.put('planRequirementId',programRequirement.Id);
                courses.add(cw);
            }
        }*/
        
        //Updated JSON structure code block starts :: ECB-4639: Date: 23-Oct-18:: Upendra
        if(!planReqirementList.isEmpty())
        {
            for(hed__Plan_Requirement__c programRequirement : planReqirementList )
            {
                CW= new CourseWrapper();
                cw.ObjectName = 'programPlan';
                cw.key.put('programId',programRequirement.hed__Program_Plan__r.hed__Account__r.Id);
                cw.key.put('programName',programRequirement.hed__Program_Plan__r.hed__Account__r.Name);
                cw.key.put('programPlanId',programRequirement.hed__Program_Plan__r.Id);
                courses.add(cw);
            }
        }
        //Updated JSON structure code block ends :: ECB-4639
        
        if(!programTestimonialList.isEmpty())
        {
            for(Program_Testimonial__c programTestimonial : programTestimonialList )
            {
                CW= new CourseWrapper();
                cw.ObjectName = 'programTestimonial';
                cw.key.put('programId',programTestimonial.Program__r.Id);
                cw.key.put('programName',programTestimonial.Program__r.Name);
                cw.key.put('programTestimonialId',programTestimonial.Id);
                courses.add(cw);
            }
        }
        
        if(!courseOfferingSessionList.isEmpty())
        {
            for(Course_Offering_Session__c ct : courseOfferingSessionList)
            {
                CourseWrapper crswp = new CourseWrapper();
                crswp.ObjectName = 'courseOfferingSession';
                crswp.key.put('courseId',ct.Course_Offering__r.hed__Course__r.Id);
                crswp.key.put('courseName',ct.Course_Offering__r.hed__Course__r.Name);
                crswp.key.put('courseOfferingId',ct.Course_Offering__r.Id);
                crswp.key.put('courseOfferingSessionId',ct.Id);
                courses.add(crswp);
            }
        }
        //Adding Related Courses to the CourseWrapper 
        /*if(!courseRelationshipList.isEmpty())
        {
            for(Course_Relationship__c c : courseRelationshipList )
            {
                CW= new CourseWrapper();
                cw.ObjectName = 'relatedCourses';
                cw.key.put('courseId',c.Related_Course__r.Id);
                cw.key.put('courseName',c.Related_Course__r.Name);
                courses.add(cw);
            }
        } */

        if( !parentCourseMap.IsEmpty())
        {
           for( String cID : parentCourseMap.keySet())
           {
                CW= new CourseWrapper();
                CW.ObjectName = 'relatedCourses';
                CW.key.put('courseId', cID);
                CW.key.put('courseName',parentCourseMap.get(cID));
                courses.add(CW);
            }
                    
        }
        
        
        
        // HTTP Request Creation
        String reqJSONString = JSON.serialize(courses);
        System.debug('Serialized list of courses into JSON format for Delta Load: ' + reqJSONString);
        System.debug('Serialized list of courses into JSON format for Delta Load 2: ' + JSON.serializePretty(courses));
        String endpoint = ConfigDataMapController.getCustomSettingValue('IpassAPI'); 
        system.debug('>>> endpoint>> '+endpoint);
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IpassClientID')); 
        req.setHeader('client_secret',ConfigDataMapController.getCustomSettingValue('IpassCientSecret')); 
        req.setHeader('Content-Type','application/json');
        req.setMethod('POST');
        req.setbody(reqJSONString);
        
        Http http = new Http();
        HTTPResponse response;
        try {
            requestLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('Product_Catalog',reqJSONString, 'Outbound Service','',false);
            response = http.send(req);
            responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'Product_Catalog',response.getBody(), 'Acknowledgement','',false);
        } catch(Exception ex) {
            system.debug('Exception------'+ex);
        }   
        
        System.debug('response:::'+response);
        
        if (requestLogObjectToInsert != null) {
            insert requestLogObjectToInsert;        
        }
        if( responseLogObjectToInsert != null ) {
            if (Schema.sObjectType.Integration_Logs__c.isCreateable()){ 
                insert responseLogObjectToInsert;
            }   
        }
        
        //Creating RMErrorLog incase iPaaS returns an error
        if(response.getStatusCode() != 200)
        {
            RMErrLog__c errlog = new RMErrLog__c();
            errlog.Error_Cause__c = ConfigDataMapController.getCustomSettingValue('IpassErrorCause'); 
            errlog.Error_Message__c = ConfigDataMapController.getCustomSettingValue('DeltaLoadErrorMsg'); 
            errlog.Error_Type__c = ConfigDataMapController.getCustomSettingValue('IpassErrorType');
            errlog.Error_Payload__c = ConfigDataMapController.getCustomSettingValue('IpassErrorPayLoad');
            
            system.debug('errlog----------------'+errlog);
            try{
                if (Schema.sObjectType.RMErrLog__c.isCreateable()){ 
                    Insert errlog;
                }   
            }
            catch(DmlException ex){
                system.debug('Exception:::'+ex);
            }    
        }
        
    }
    
}