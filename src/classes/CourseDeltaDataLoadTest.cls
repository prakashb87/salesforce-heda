/*****************************************************************************************************
 *** @Class             : CourseDeltaDataLoadTest
 *** @CreatedBy         : Avinash Machineni
 *** @Created date      : 29/03/2018
 *** @JIRA ID           : ECB-290
*** @LastModifiedBy     : Avinash Machineni
 *** @Created date      : 28/05/2018
 *** @JIRA ID           : ECB-3718
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to test the functionality CourseDeltaDataLoad using test data
 *****************************************************************************************************/ 
@isTest
private class CourseDeltaDataLoadTest {
    /********************************************************************
    // Purpose              : Creating Scheduled Job for test class                             
    // Author               : Capgemini [Avinash]
    // Parameters           : null
    //  Returns             : void
    //JIRA Reference        : ECB-3718 - Test Method for creating a scheduled job	
    //********************************************************************/ 
    /*
    public static void schedulerTest() 
    {
        String CRON_EXP = '0 15 17 ? * MON-FRI';
        
        ProductCatalogueScheduler p = new ProductCatalogueScheduler();
        
        String jobId = System.schedule('Product Data sync',  CRON_EXP, p);
        CronTrigger ct = [SELECT Id, state, PreviousFireTime, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
    }*/
	/********************************************************************
    // Purpose              : Passing records for testing success response                           
    // Author               : Capgemini [Avinash]
    // Parameters           : null
    //  Returns             : void
    //JIRA Reference        : ECB-3718 - Test Method for Updating existing delta trigger for passing product cataloge details to iPaaS  
    //********************************************************************/ 
	
    @isTest static void testloadCourseDetailsTestSuccess() {
        //schedulerTest();
        //create test data
	 	string myJsonBody = '{"id": "ID-dev-20180718T035647828","result": "SUCCESS","code": "200","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sent successfully to Exchange rmit-publisher-api-sfdc-dev"}';

		TestDataFactoryUtilRefOne.deltaTestSuccess();

        Test.setMock(HttpCalloutMock.class, new iPaasDeltaLoadCalloutServiceMock());
		
		Test.startTest();
		CourseDeltaDataLoad.sendCourseAsync();
		Test.stopTest();

		//verify outbound request record in SF
		list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
		system.assertEquals(1, dbResults.size());
        System.debug('dbResults++++'+dbResults);

		//verify 
		system.assertEquals(myJsonBody, dbResults[0].Request_Response__c);

		list<Integration_Logs__c> dbResults2 = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Outbound Service'];
		system.assertEquals(1, dbResults2.size());
        System.debug('dbResults2++++'+dbResults2);
	}
    
	/********************************************************************
    // Purpose              : Passing records for testing error response                            
    // Author               : Capgemini [Avinash]
    // Parameters           : null
    //  Returns             : void
    //JIRA Reference        : ECB-3718 - Test Method for Updating existing delta trigger for passing product cataloge details to iPaaS  
    //********************************************************************/ 
    
    @isTest static void testloadCourseDetailsTestError() {
		//schedulerTest();
        //create test data
	 	string responsebody = '{"id": "ID-dev-20180718T035647828","result": "ERROR","code": "400","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sending Failed"}';

		TestDataFactoryUtilRefOne.deltaTestError();

        Test.setMock(HttpCalloutMock.class, new iPaasDeltaLoadCalloutServiceMock2());
		
		Test.startTest();
		CourseDeltaDataLoad.sendCourseAsync();
		Test.stopTest();

		//verify outbound request record in SF
		list<Integration_Logs__c> intloglist = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
		system.assertEquals(1, intloglist.size());
        System.debug('intloglist++++'+intloglist);

		System.debug('Database Result+++++'+ intloglist[0].Request_Response__c);
        System.debug('responsebody+++++'+responsebody);
		system.assertEquals(responsebody, intloglist[0].Request_Response__c);

		list<Integration_Logs__c> listinteglog = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Outbound Service'];
		system.assertEquals(1, listinteglog.size());
        System.debug('listinteglog++++'+listinteglog);
	}


    public class iPaasDeltaLoadCalloutServiceMock implements HttpCalloutMock {
        // implement http mock callout
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "SUCCESS","code": "200","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sent successfully to Exchange rmit-publisher-api-sfdc-dev"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    public class iPaasDeltaLoadCalloutServiceMock2 implements HttpCalloutMock {
        // implement http mock callout
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "ERROR","code": "400","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sending Failed"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }
        
    }
	
}