@isTest(seeAllData=false)
public class CourseEnrollmentObserverTest {
    
    static testmethod void  enrollContactsTest() {
        
        TestDataFactoryUtil.test();
        Test.startTest();
        
        List<Id> servicesId=new List<Id>();
        for(csord__Service__c service:[SELECT Id from csord__Service__c]){
            servicesId.add(service.Id);
        }
        CourseEnrollmentObserver.enrollContacts(servicesId);
        System.assert([SELECT Id FROM hed__Course_Enrollment__c].size() > 0);
        Test.stopTest();
        
    }
    
}