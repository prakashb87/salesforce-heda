/* 
   @Author <Pawan Srivastava>
   @name <CourseEnrollmentTriggerHandler>
   @CreateDate <16/10/2018>
   @Description <This class handles request from hed__Course_Enrollment__c object trigger> 
   @Version <1.0>
   */
public with sharing class CourseEnrollmentTriggerHandler {

    /**
    * @Author       Pawan Srivastava
    * @Name         doAfterUpdate
    * @Date         16/10/2018
    * @Description  This method handles after insert requests from hed__Course_Enrollment__c object trigger 
    * @Param        List<hed__Course_Enrollment__c> courseEnrolments
    * @return       NA
    */  
    public static void doAfterUpdate(List<hed__Course_Enrollment__c> courseEnrolment , Map<Id,hed__Course_Enrollment__c> mapOldCourseEnrolment){ 
      
        set<String> enrolls = new set<String>();
        Set<String> setCourses = new Set<String>();
        String fedId = '';
        for (hed__Course_Enrollment__c enroll : courseEnrolment) {
            enrolls.add(enroll.Id);
        }
        List<User> contactsUser = new List<User>();
		//if(
        contactsUser = [select Id, ContactId, FederationIdentifier from User where ContactId = :courseEnrolment.get(0).hed__Contact__c];
		
        if(contactsUser.size() >0){
            fedId = contactsUser[0].FederationIdentifier;
            System.debug('Fed Id = ' + fedId);
                    
		
			for (hed__Course_Enrollment__c enrollment : [SELECT 
															Id,hed__Status__c,
															hed__Contact__r.Owner.FederationIdentifier,
															hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c,
															hed__Course_Offering__r.hed__Course__r.hed__Account__r.Name,
															hed__Course_Offering__r.Name
														FROM
															hed__Course_Enrollment__c
														WHERE
															Id IN :enrolls AND (hed__Status__c = :ConfigDataMapController.getCustomSettingValue('OptOut') OR
															hed__Status__c = :ConfigDataMapController.getCustomSettingValue('Cancelled') OR
															hed__Status__c = :ConfigDataMapController.getCustomSettingValue('Transferred'))
														])   
													{  
																									 
				hed__Course_Enrollment__c oldEnrollment = mapOldCourseEnrolment.get(enrollment.Id);
				
					System.debug('Account Name = ' + enrollment.hed__Course_Offering__r.hed__Course__r.hed__Account__r.Name);
					System.debug('oldEnrollment = ' + oldEnrollment);
					System.debug('oldEnrollment Status  = ' + oldEnrollment.hed__Status__c );
					System.debug('Custom Settings = ' + ConfigDataMapController.getCustomSettingValue('OptOut') + ' ' + ConfigDataMapController.getCustomSettingValue('Cancelled') + ' ' + ConfigDataMapController.getCustomSettingValue('Transferred'));                                                    
				   if (enrollment.hed__Course_Offering__r.hed__Course__r.hed__Account__r.Name == ConfigDataMapController.getCustomSettingValue('RMITOnline') && 
				   (oldEnrollment != null && oldEnrollment.hed__Status__c != enrollment.hed__Status__c &&
					(enrollment.hed__Status__c == ConfigDataMapController.getCustomSettingValue('OptOut') || enrollment.hed__Status__c == ConfigDataMapController.getCustomSettingValue('Cancelled') || enrollment.hed__Status__c == ConfigDataMapController.getCustomSettingValue('Transferred')))
				   ){
						System.Debug('old Status-->'+oldEnrollment.hed__Status__c + ' new status -->'+ enrollment.hed__Status__c);
						//setCourses.add(enrollment.hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c);
						setCourses.add(enrollment.hed__Course_Offering__r.Name);
						//CourseEnrollmentHelper.iPassOpenEdxApiUnEnroll(enrollment.hed__Contact__r.Owner.FederationIdentifier,setCourses);               
				}           
			}

			if(!setCourses.isEmpty() && fedId != null)
			{
				CourseEnrollmentHelper.iPassOpenEdxApiUnEnroll(fedId,setCourses); 
			}				
				 
		}
	}


}