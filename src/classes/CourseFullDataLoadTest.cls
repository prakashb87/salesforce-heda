/*****************************************************************
Name: CourseFullDataLoadTest 
Author: Capgemini[Shreya]
Purpose: Test Class For CourseFullDataLoad
JIRA Reference : ECB-3247 - The Full load for the Event trigger not catered to
*****************************************************************/
/*==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Shreya Barua            29/05/2018         Initial Version
********************************************************************/

@isTest
public class CourseFullDataLoadTest {
    
  @isTest static void loadCourseDetailsTest(){
        
        Test.startTest();
        
        TestDataFactoryUtilRefTwo.testfullLoad();
        
        //Querying data from test datasetup method 
        List<hed__Course__c> courseList = [Select hed__Course_ID__c, Name,Id from hed__Course__c WHERE RecordType.DeveloperName = 'X21CC'];
        system.assert(courseList.size()>0,true); 
          
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
        
        CourseFullDataLoad.loadCourseDetails();
        HTTPRequest req = new HTTPRequest();
        HTTPMockCallout obj = new HTTPMockCallout();
        HTTPResponse resp = obj.respond(req);
        System.assertEquals(404, resp.getStatusCode());

        
 
        Test.stopTest();
    }
       
}