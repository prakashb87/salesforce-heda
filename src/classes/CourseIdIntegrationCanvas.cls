/*****************************************************************************************************
*** @Class             : CourseIdIntegrationCanvas
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending course SIS Id's and record id's
*** @Created date      : 24/05/2018
*** @JIRA ID           : ECB-3251
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used integrate the records from Salesforce and IPaas for sending course Id's
*****************************************************************************************************/
public without sharing class CourseIdIntegrationCanvas {
    
    @AuraEnabled
    public static String returntoIpass(Id incomingId, String ssid)
    {
        system.debug('TTTT:returntoIpass()' + incomingId );
        String permissionErrorMessge = '';
        if( CheckBusinessAdminUser.getUserProfileName() == '21CC Standard User')
        {
           permissionErrorMessge = System.Label.Error2; // Error2 message to identify that the user does not have the required permission
        }
        else
        {
            
        
        Integration_Logs__c requestLogObjectToInsert = new Integration_Logs__c();
        Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c();
        
        JSONGenerator gen = JSON.createGenerator(true);
        
        gen.writeStartObject();
        //gen.writeIdField('sf_record_id', incomingId);
        //gen.writeStringField('sis_course_id', ssid);
        gen.writeEndObject();
        
        String jstring = gen.getAsString();
        
        String endpoint = ConfigDataMapController.getCustomSettingValue('CanvasCourseOfferingEndPointURL')+''+ssid+'?record_id='+incomingId;
        system.debug('endpoint: ' + endpoint); //Ming
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CanvasCourseOfferingClientId'));
        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CanvasCourseOfferingClientSecretId'));
        req.setMethod('GET');
        req.setTimeout(30000);
        Http http = new Http();
        HTTPResponse response;
        try
        {
            requestLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'Canvas_IPaaS',endpoint, 'Outbound Service','',false);
            response = http.send(req);
            responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'Canvas_IPaaS',response.getBody(), 'Acknowledgement','',false);
            
            
        }
        catch (exception e) 
        {
            System.debug('Error Message------------'+e.getMessage());
            ApplicationErrorLogger.logError(e);
        }
        
        System.debug('Json String-------->'+gen.getAsString());
        
        System.debug('Response code'+response);
        try
        {
            if( requestLogObjectToInsert != null)
            {
                insert requestLogObjectToInsert;        
            }
            if( responseLogObjectToInsert != null )
            {
                insert responseLogObjectToInsert;
            }
        }
        catch (exception e) 
        {
            ApplicationErrorLogger.logError(e);
        }
        
       
    }
    //return jstring;
    return permissionErrorMessge;
               
    }   
}