/*****************************************************************************************************
 *** @Class             : CoursePrerequisiteTriggerHandler
 *** @Author            : Ming Ma
 *** @Purpose 	        : tigger logic of Course Prerequisite object
 *** @Created date      : 21/02/2018
 *** @JIRA ID           : ECB-141 & ECB-3224
 *****************************************************************************************************/

public with sharing class CoursePrerequisiteTriggerHandler {
	public static void handleBeforeInsert(List<Course_Prerequisite__c> records) {
    	checkDuplication(records);
        cannotAddItself(records);
 	}
    
    public static void handleBeforeUpdate(List<Course_Prerequisite__c> records) {
    	checkDuplication(records);
        cannotAddItself(records);
 	}
    
    @TestVisible
    private static void checkDuplication(List<Course_Prerequisite__c> records) {
        if (Schema.sObjectType.Course_Prerequisite__c.isAccessible()) {
	        List<Course_Prerequisite__c> existingRecords = [Select Id, course__c, Prerequisite__c
			             from Course_Prerequisite__c];
	        
	        for (Course_Prerequisite__c record : records) {
	            for (Course_Prerequisite__c existingRecord : existingRecords) {
	                if (existingRecord.Course__c == record.Course__c && existingRecord.Prerequisite__c == record.Prerequisite__c) {
	                    record.addError('Cannot add duplicate course prerequisite');
	                }
	            }
	        }
        }
    }
    
    @TestVisible
    private static void cannotAddItself(List<Course_Prerequisite__c> records) {
        for (Course_Prerequisite__c record : records) {
            if (record.Course__c == record.Prerequisite__c) {
                record.addError('Cannot add itself as a course prerequisite');
            }
           
        }
    }
}