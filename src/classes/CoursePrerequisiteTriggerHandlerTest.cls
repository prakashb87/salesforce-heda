/*****************************************************************************************************
 *** @Class             : CoursePrerequisiteTriggerHandlerTest
 *** @Author            : Ming Ma
 *** @Purpose 	        : test calss for CoursePrerequisiteTriggerHandler
 *** @Created date      : 21/02/2018
 *** @JIRA ID           : ECB-141 & ECB-3224
 *****************************************************************************************************/

@isTest
public class CoursePrerequisiteTriggerHandlerTest {
    
    @isTest static void testCheckDuplicationInsert() {
        Account department1 = New Account();
        department1.name = 'School of Business';
        insert department1;
        
        hed__Course__c course1 = New hed__Course__c ();
        course1.name = 'Course 1';
        course1.hed__Account__c = department1.Id;
        insert course1;
        
        hed__Course__c course2 = New hed__Course__c ();
        course2.name = 'Course 2';
        course2.hed__Account__c = department1.Id;
        insert course2;
        
        Course_Prerequisite__c p1 = New Course_Prerequisite__c();
        p1.Course__c = course1.Id;
        p1.Prerequisite__c = course2.Id;
        insert p1;
        
        Test.startTest();
        try{
       
            Course_Prerequisite__c p2 = New Course_Prerequisite__c();
            p2.Course__c = course1.Id;
            p2.Prerequisite__c = course2.Id;
            insert p2;
        } catch(Exception e) {
        	Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot add duplicate course prerequisite') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
        }

       Test.StopTest();
    }
    
    @isTest static void testCheckDuplicationUpdate() {
        Account department1 = New Account();
        department1.name = 'School of Business';
        insert department1;
        
        hed__Course__c course1 = New hed__Course__c ();
        course1.name = 'Course 1';
        course1.hed__Account__c = department1.Id;
        insert course1;
        
        hed__Course__c course2 = New hed__Course__c ();
        course2.name = 'Course 2';
        course2.hed__Account__c = department1.Id;
        insert course2;
        
        hed__Course__c course3 = New hed__Course__c ();
        course3.name = 'Course 3';
        course3.hed__Account__c = department1.Id;
        insert course3;
        
        Course_Prerequisite__c p1 = New Course_Prerequisite__c();
        p1.Course__c = course1.Id;
        p1.Prerequisite__c = course2.Id;
        insert p1;
        
        Course_Prerequisite__c p2 = New Course_Prerequisite__c();
        p2.Course__c = course1.Id;
        p2.Prerequisite__c = course3.Id;
        insert p2;
        
        Test.startTest();
        try{
            p2.Prerequisite__c = course2.Id;
            update p2;     
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot add duplicate course prerequisite') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }

        Test.StopTest();
    }
    
    @isTest static void testCannotAddItselfInsert() {
        Account department1 = New Account();
        department1.name = 'School of Business';
        insert department1;
        
        hed__Course__c course1 = New hed__Course__c ();
        course1.name = 'Course 1';
        course1.hed__Account__c = department1.Id;
        insert course1;
      
        Test.startTest();
        try{
           	Course_Prerequisite__c p1 = New Course_Prerequisite__c();
            p1.Course__c = course1.Id;
            p1.Prerequisite__c = course1.Id;
            insert p1;
        } catch(Exception e) {
        	Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot add itself as a course prerequisite') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
        }

		Test.StopTest();
    }
    
    
    @isTest static void testCannotAddItselfUpdate() {
        Account department1 = New Account();
        department1.name = 'School of Business';
        insert department1;
        
        hed__Course__c course1 = New hed__Course__c ();
        course1.name = 'Course 1';
        course1.hed__Account__c = department1.Id;
        insert course1;
        
        hed__Course__c course2 = New hed__Course__c ();
        course2.name = 'Course 2';
        course2.hed__Account__c = department1.Id;
        insert course2;
        
        Course_Prerequisite__c p1 = New Course_Prerequisite__c();
        p1.Course__c = course1.Id;
        p1.Prerequisite__c = course2.Id;
        insert p1;
      
        Test.startTest();
        try{
           	p1.Prerequisite__c = course1.Id;
        } catch(Exception e) {
        	Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot add itself as a course prerequisite') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
        }

		Test.StopTest();
    }
}