/*****************************************************************
Name: CourseTransactionRecordControllerTest
Author: Capgemini [Shreya]
Purpose: Test Class For CourseTransactionController
JIRA Reference : ECB-4416 :  Build : Recording Transactions in Course Transaction object                 
*****************************************************************/
/*==================================================================
History 
--------
Version   Author            Date              Detail
1.0       Shreya            5/09/2018         Initial Version
********************************************************************/
@isTest
public class CourseTransactionControllerTest {
    
    @isTest
    public static void createCourseTransactionRecordTest(){
        
        Test.startTest();
        TestDataFactoryUtil.testCoureseTransaction();
        TestDataFactoryUtil.dummycustomsetting();
        
        //Querying data from test datasetup method
        Set<Id> connIdList = new Set<Id>();
        for(hed__Course_Enrollment__c cenroll :[ Select Id from hed__Course_Enrollment__c]){
            connIdList.add(cenroll.Id);
        }
        system.assert(connIdList!=null,true);
        
        CourseTransactionController.createCourseTransactionRecord(connIdList);
        List<Course_Transaction__c> ctransacList = [SELECT Id FROM Course_Transaction__c WHERE Course_Connection__c IN :connIdList];
        system.assert(ctransacList!=null,true);
        
        Test.stopTest();
    }
    
    
    

}