/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 04/12/2018     ****************************/               
/***   Trigger Helper :When Course Transaction records manually created, get service reference from Course 
 ***           Connection or Program Enrolment object and update it on Course Transaction Record  *******/ 
/********************************************************************************************************/
public without sharing class CourseTransactionUpdateTriggerHelper 
{
     public static void updateCourseTransactionRecord(Set<Id> courseTransactionIdSet)
     {
       
       List<Course_Transaction__c> courseTransactionList = new List <Course_Transaction__c>(); 
       List<Course_Transaction__c> courseTransactionListToUpdate = new List <Course_Transaction__c>();
       if(Schema.sObjectType.Course_Transaction__c.isAccessible()){
           courseTransactionList = [SELECT id,Generated_by__c,Course_Connection__c,Course_Connection__r.Service__c,Program_Enrollment__c,Program_Enrollment__r.Service__c FROM Course_Transaction__c where id IN:courseTransactionIdSet];
       } 
        
        if(!courseTransactionList.isEmpty()){
            courseTransactionListToUpdate.addAll(getUpdatedCourseTransactionList(courseTransactionList));
        } 
        
        if( !courseTransactionListToUpdate.isEmpty()) {
            if (Schema.sObjectType.Course_Transaction__c.isUpdateable()){ 
                update courseTransactionListToUpdate;
            }    
        }

     }
     
     public static List<Course_Transaction__c> getUpdatedCourseTransactionList(List<Course_Transaction__c> courseTransactionList){
        
        List<Course_Transaction__c> courseTransactionListToUpdate = new List <Course_Transaction__c>(); 
        for (Course_Transaction__c ctObj : courseTransactionList){           
            if( ctObj.Generated_by__c == 'User' ){
                if( String.IsNotBlank(ctObj.Course_Connection__c)){
                    System.debug('Course Connection Service Id =' + ctObj.Course_Connection__r.Service__c);
                    ctObj.Service__c = ctObj.Course_Connection__r.Service__c;
                    courseTransactionListToUpdate.add(ctObj);
                    
                }
                else if( String.IsNotBlank(ctObj.Program_Enrollment__c)){
                      System.debug('Program Enrollment Service Id =' + ctObj.Program_Enrollment__r.Service__c);
                      ctObj.Service__c = ctObj.Program_Enrollment__r.Service__c;
                      courseTransactionListToUpdate.add(ctObj);
                }
               
            }                     
        }
        
        return courseTransactionListToUpdate ;
     }
}