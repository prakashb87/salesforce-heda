/*
  ***************************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         :09/10/2018 
  * @description  :Once user clicks on the "start button" the user should be enrolled in the course 
  				   offering that they have selected  
  ***************************************************************************************************************/
public without sharing class CreateEnrollmentForSectectedOffering 
{
	public static Boolean createCourseConnectionForSelectedOffering(Id serviceId, Id courseOffId, Id programEnrollId )
	{
		System.debug('RRRR-Entering function:createCourseConnectionForSelectedOffering');
	    
        Id courseEnrollRecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();   
	    
        hed__Program_Enrollment__c pgmEnrollObj = new hed__Program_Enrollment__c();
	      pgmEnrollObj = [SELECT id,hed__Contact__c from hed__Program_Enrollment__c where id =: programEnrollId LIMIT 1];

        List<hed__Course_Enrollment__c> selectedCCList = checkExistingCourseConnection(courseEnrollRecordTypeId,pgmEnrollObj,courseOffId,serviceId);
	    
        if(selectedCCList.isEmpty())
        {
            // Creating Course Connection record for enrollment
            hed__Course_Enrollment__c courseCCObj = new hed__Course_Enrollment__c();
            courseCCObj.RecordTypeId = courseEnrollRecordTypeId ; 
            courseCCObj.hed__Contact__c = pgmEnrollObj.hed__Contact__c; 
            courseCCObj.hed__Course_Offering__c = courseOffId;
            courseCCObj.hed__Program_Enrollment__c = programEnrollId;

            courseCCObj.Service__c= serviceId; 
            if(System.isBatch())
            { 
                courseCCObj.Source_Type__c = ConfigDataMapController.getCustomSettingValue('MKTBatch');//MKT-Batch  
            }
            else
            {
                courseCCObj.Source_Type__c = ConfigDataMapController.getCustomSettingValue('MKT');//MKT
            }    
            courseCCObj.Admission_Type__c = ConfigDataMapController.getCustomSettingValue('AdmissionType');//Existing RMIT Student
            courseCCObj.hed__Status__c = ConfigDataMapController.getCustomSettingValue('EnrollmentStatus');//current
            courseCCObj.Batch_Processed__c = false; 
            courseCCObj.Enrolment_Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE');//E
            

            //Creating List of course Connection to create course transaction and course connection life cycle
            Set<Id> IncomingCourseEnrolments = new Set<Id>();
            Database.SaveResult sr = Database.insert(courseCCObj,false);            
            System.debug('Save Result'+sr);
            if(sr.isSuccess()) 
            {                                    
                IncomingCourseEnrolments.add(sr.getId());                                                           
            }
            System.debug('IncomingCourseEnrolments list=' + IncomingCourseEnrolments);
            if(!IncomingCourseEnrolments.isEmpty())
            {
                 // Creating Course Transaction record.
                 CourseTransactionController.createCourseTransactionRecord( IncomingCourseEnrolments );
                 //Creating Course Connection life cycle record on course connection record.
                 CreateRecordsOnCourseConnectionLifeCycle.createCourseConnection( IncomingCourseEnrolments );
            }
            return true;
        }
        else
        {
            System.debug('For the selected course offering active Course Connection is Existing');
            return false;
        }
       
	    return false;
	}

    public static List<hed__Course_Enrollment__c> checkExistingCourseConnection(Id courseEnrollRecordTypeId,hed__Program_Enrollment__c pgmEnrollObj,String courseOffId, String serviceId)
    {
       List<hed__Course_Enrollment__c> selectExistingCCList =[SELECT id from hed__Course_Enrollment__c 
                                                              where RecordTypeId =:courseEnrollRecordTypeId and
                                                              hed__Contact__c =: pgmEnrollObj.hed__Contact__c and
                                                              hed__Course_Offering__c = :courseOffId and 
                                                              hed__Program_Enrollment__c = :pgmEnrollObj.id and 
                                                              Service__c =:serviceId and
                                                              hed__Status__c = :ConfigDataMapController.getCustomSettingValue('EnrollmentStatus') and
                                                              Enrolment_Status__c = :ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE')]; 

        return selectExistingCCList;                                                         
    }
}