/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 17/05/2018     ****************************/               
/*** This class is using to  automatically populate records on CourseConnection Life Cycle Object *******/ 
/********************************************************************************************************/

public without sharing class CreateRecordsOnCourseConnectionLifeCycle 
{
    public static void createCourseConnection(Set<Id> courseConnectionIds  )
    {   
        System.debug('CCId-->'+courseConnectionIds);
        List<hed__Course_Enrollment__c> courseConnectionList = new List<hed__Course_Enrollment__c>();
        courseConnectionList = [SELECT id,hed__Course_Offering__r.Name,hed__Course_Offering__r.hed__Course__r.RecordType.Name,hed__Program_Enrollment__c from hed__Course_Enrollment__c where id IN:courseConnectionIds];
        
        
        List<Life_Cycle__c>lifeCycleList = new List<Life_Cycle__c>();
        lifeCycleList = [SELECT id,Course_Type__c,Stage__c from Life_Cycle__c];
        
        List<Course_Connection_Life_Cycle__c> ccLifeCycleList = new  List<Course_Connection_Life_Cycle__c>();
        
        for( hed__Course_Enrollment__c ccObj :courseConnectionList ) 
        {
            String ccRecordType = ccObj.hed__Course_Offering__r.hed__Course__r.RecordType.Name;
            System.debug('AAAA=' + ccRecordType );
            System.debug('Inside Method--->'+lifeCycleList);
            for (Life_Cycle__c lifeCycleObj : lifeCycleList )
            {
                Course_Connection_Life_Cycle__c newCCLifeCycleObj = new Course_Connection_Life_Cycle__c();
                String courseType = lifeCycleObj.Course_Type__c;
                List<String> courseTypeSplit = courseType.split(';');
                system.debug('aaa'+ courseTypeSplit );
                if (courseTypeSplit.contains(ccRecordType))
                {                   
                    newCCLifeCycleObj.Course_Connection__c =  ccObj.id;
                    newCCLifeCycleObj.Stage__c = lifeCycleObj.Stage__c;
                    
                    if( newCCLifeCycleObj.Stage__c == ConfigDataMapController.getCustomSettingValue(ConstantUtil.COURSECONNECTION_LIFECYCLE_CREATED))
                    {
                        newCCLifeCycleObj.Status__c = ConfigDataMapController.getCustomSettingValue(ConstantUtil.COURSECONNECTION_LIFECYCLE_SUCCESS);
                    }
                    else
                    {
                        newCCLifeCycleObj.Status__c = ConfigDataMapController.getCustomSettingValue(ConstantUtil.COURSECONNECTION_LIFECYCLE_NOTSTARTED);
                    }
                    
                }
              ccLifeCycleList.add(newCCLifeCycleObj);   
                System.debug('life Cycle-->'+ccLifeCycleList);
            }
            
        }
        
        if( !ccLifeCycleList.isEmpty())
        {            
            Database.SaveResult[] srList = Database.insert(ccLifeCycleList ,false);
            Set<Id> ccifeCycleIdSet = new Set<Id>();
            for(Database.SaveResult sr : srList)
            {
                if (sr.isSuccess()) 
                {
                   ccifeCycleIdSet.add(sr.getId()) ;                   
                }
            } system.debug('Id---<'+ccifeCycleIdSet);
            //get all success set records
           
            
            if(!ccifeCycleIdSet.isEmpty())
            {
                List<Course_Connection_Life_Cycle__c> ccLCSuccessList = new List <Course_Connection_Life_Cycle__c>();
                Set<Id> successCCSet = new Set<Id>();
                //ccLCSuccessList = [SELECT id,Course_Connection__c from Course_Connection_Life_Cycle__c where id IN: ccifeCycleIdSet AND Stage__c != :ConfigDataMapController.getCustomSettingValue('EdxLifeCycleStage')]; //ECB-4438
                ccLCSuccessList = [SELECT id,Course_Connection__c from Course_Connection_Life_Cycle__c where id IN: ccifeCycleIdSet]; //ECB-4438

                for (Course_Connection_Life_Cycle__c cclcObj :ccLCSuccessList )
                {
                    if( cclcObj.Course_Connection__c != null)
                    {
                        successCCSet.add(cclcObj.Course_Connection__c);  
                    }
                                      
                }
                system.debug('successCCSet=' +successCCSet);
                if( successCCSet.size() == courseConnectionIds.size())
                {
                    system.debug( '@@@@@=' + successCCSet);
                    system.debug( '######' + courseConnectionIds);
                    
                    Set<Id> ccSetFor21CC = new Set<Id>();
                    Set<String> coSetForRMITO = new Set<String>();
                    Set<String> ccIDSetForRMITO = new Set<String>();
                    Set<Id> ccSetForRMITTraining = new Set<Id>();
                    for( hed__Course_Enrollment__c ccObj: courseConnectionList )
                    {
                        if( ccObj.hed__Course_Offering__r.hed__Course__r.RecordType.Name == '21CC')
                        {
                           ccSetFor21CC.add(ccObj.id); 
                        }
                        else if( ccObj.hed__Course_Offering__r.hed__Course__r.RecordType.Name == 'RMIT Online')
                        {
                            coSetForRMITO.add(ccObj.hed__Course_Offering__r.Name);
                            ccIDSetForRMITO.add(ccObj.id);
                        }
                        else if( ccObj.hed__Course_Offering__r.hed__Course__r.RecordType.Name == 'RMIT Training')
                        {
                            ccSetForRMITTraining.add(ccObj.id);
                        }
                        else if( ccObj.hed__Program_Enrollment__c != null)
                        {
                            coSetForRMITO.add(ccObj.hed__Course_Offering__r.Name);
                            ccIDSetForRMITO.add(ccObj.id);

                        }

                    }
                    // below code is for - Calling send course connection details to SAMS method            
                    if(!Test.isRunningTest()) 
                    {
                        if(!System.isBatch())
                        {
                            if( !ccSetFor21CC.isEmpty())
                            {
                                CourseConnectionDetailIntegration.sendCourseConn( ccSetFor21CC );
                                // Added for ECB-367 (Post to Canvas)
                                CourseConnectionIntegrationtoCanvas.sendCourseConnIds( ccSetFor21CC );
                            }
                            if( !ccSetForRMITTraining.isEmpty())
                            {
                                
                                // Added for ECB-4854 (Post to Canvas)
                                CourseConnectionIntegrationtoCanvas.sendCourseConnIds( ccSetForRMITTraining );
                            }

                            //Sending RMITO courseConnection to OpenEdx
                            if( !coSetForRMITO.isEmpty() && !ccIDSetForRMITO.isEmpty())
                            {
                                String ccId  = (new list<String>(ccIDSetForRMITO) )[0] ;
                                system.debug('ccId=' + ccId);
                                String federationId = getFederationId(ccId);
                                system.debug('Federation ID =' + federationId);
                                system.debug('OpenEdxList =' + coSetForRMITO.size());
                               
                                CourseEnrollmentHelper.iPassOpenEdxApiEnrollCall(federationId,coSetForRMITO );
                            }
                        }
                    }
                }
                else
                {
                    System.debug('Error: For all course connections, CourseConnection LifeCycle stage records not created ');
                }
            }
        }
        
    }
    public static string getFederationId(String courseConnectionId)
    {
        String federationId ='';
        User userObj = new User();
        hed__Course_Enrollment__c ccForFederationId = [SELECT id, hed__Contact__c from hed__Course_Enrollment__c where id =:courseConnectionId LIMIT 1];
        if( ccForFederationId != null )
        {
            userObj = [SELECT ContactId,FederationIdentifier FROM User where ContactId  = :ccForFederationId.hed__Contact__c and FederationIdentifier != null];
        }
        if( userObj != null )
        {
            if( !String.isBlank(userObj.FederationIdentifier))
            {
                federationId = userObj.FederationIdentifier;
            }
            
            else 
            {
               federationId = '';
            }
        }
        return federationId;

    }
       
}