/**************************************************************
Purpose: To To create Non Funded Researcher Portal Projects
History:
Created by Ankit Bhagat on 05/09/2018
*************************************************************/
public class CreateResearcherPortalProjectController {
    
    /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
         
    /*****************************************************************************************
    // Purpose : This method used to Create Project and call the helper method
    // Parameters : String jsonProjectData, Id recordIdfromcontroller
    // Developer : Ankit
    // Created Date : 05/09/2018
    //****************************************************************************************/
    @AuraEnabled
    public static void createProject(String jsonProjectData, Id recordIdfromcontroller) {

        try{
            
            CreateProjectHelper.createResearcherProject(jsonProjectData, recordIdfromcontroller);
        }catch (Exception ex) {
            
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('get Fields Error' + ex.getMessage());
        }
    }
    
    /*****************************************************************************************
    // Purpose : This method used to get the fields API Names
    // Parameters : Void
    // Developer : Ankit
    // Created Date : 05/09/2018
    //****************************************************************************************/    
    public static Map<String,String> getfieldsAPINames(){
        Map<String,String> fieldsMap = new Map<String,String>{'Current Status'=>'Portal_Project_Status__c', 'FROCodes' => 'For_Four_Digit_Detail__c', 
                                                              'Category' => 'Impact_Category__c'};
        return fieldsMap;
    }

    /*****************************************************************************************
    // Purpose : This method used to get the Objects API Names
    // Parameters : Void
    // Developer : Ankit
    // Created Date : 05/09/2018
    //****************************************************************************************/    
    public static Map<String,String> getObjectsAPINames(){
        Map<String,String> fieldsMap = new Map<String,String>{'ResearcherPortalProject'=>'ResearcherPortalProject__c' ,
                                                              'ProjectClassification'=>'Research_Project_Classification__c'};
        return fieldsMap;
    }

    /*****************************************************************************************
    // Purpose : This method used to get select Options
    // Parameters : String objObject, string fld
    // Developer : Ankit
    // Created Date : 05/09/2018
    //****************************************************************************************/    
    @AuraEnabled 
    public static List < String > getselectOptions(String objObject, string fld) { 
        List < String > picklistValues = new List<String>();
        
        try{
            String fieldAPIName = getfieldsAPINames().get(fld);            
            String objObject1 = getObjectsAPINames().get(objObject);
            sObject obj = Schema.getGlobalDescribe().get(objObject1).newSObject();
            picklistValues = SobjectUtils.getselectOptions(obj,fieldAPIName );
            
        } catch(Exception ex){
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            
        }
        return picklistValues;
        
    }
    
    /*****************************************************************************************
    // Purpose : To retrieve project detail from the ProjectDetailsWrapper
    // Parameters : Id recordId
    // Developer : Ankit
    // Created Date : 05/09/2018
    //****************************************************************************************/    
    @AuraEnabled 
    public static projectDetailsWrapper.ResearchProjectDetailData getProjectDetails(Id recordId){
        projectDetailsWrapper.ResearchProjectDetailData projectDetailsRecords = 
            new projectDetailsWrapper.ResearchProjectDetailData();
        
        try
        {
            projectDetailsRecords = ResearchProjectHelper.getProjectDetailsHelper(recordId); 
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);                                                         //added by Ali
        }
        catch(exception ex)
        {
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            
        }
        return projectDetailsRecords;
    }
    
}