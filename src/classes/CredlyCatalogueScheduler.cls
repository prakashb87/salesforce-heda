/*****************************************************************************************************
 *** @Class             : CredlyCatalogueScheduler
 *** @Author            : Avinash Machineni
 *** @Requirement       : scheduler to schedule Course Connection IDs
 *** @Created date      : 20/06/2018
 *** @JIRA ID           : ECB-2446
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used as a scheduler to schedule Course Connection IDs  to Ipass API.
 *****************************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class CredlyCatalogueScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		//RR.....Future Class Fix......starts
		CredlyBadgeCompletionBatch batchObj = new CredlyBadgeCompletionBatch();
		// ECB-4293
		//database.executebatch( batchObj,5);
		database.executebatch( batchObj, 1);
		//RR.....Future Class Fix......Ends

	}
}