/*****************************************************************************************************
 *** @Class             : CredlyCourseConnectionIntController
 *** @Author            : Avinash Machineni
 *** @Purpose           : Controller for retry quick action lightning component
 *** @Created date      : 18/05/2018
 *** @JIRA ID           : ECB-2466
 *****************************************************************************************************/


public without sharing class CredlyCourseConnectionIntController {
    @AuraEnabled
    public static void updateCourseConn(String courseConnectionId) { 
        System.debug('CredlyBadgeCompletion.updateCourseConn entered');

        // need to check if there is an error or not
        boolean hasError = false;
        if(!Schema.sObjectType.Course_Connection_Life_Cycle__c.isAccessible())
        {
           throw new DMLException();           		
        }
        List<Course_Connection_Life_Cycle__c> dbResults = [Select Id, Course_Connection__c from Course_Connection_Life_Cycle__c
                where  Course_Connection__c = : courseConnectionId AND Stage__c = : ConfigDataMapController.getCustomSettingValue('CredlyCCLifeCycleStage') AND Status__c = :   ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError')];

        system.debug('Course_Connection_Life_Cycle__c records count: ' + dbResults.size());

        if (dbResults.size() == 1) {
            hasError = true;
        }

        system.debug('hasError: ' + hasError);

        if (hasError == true) {
        	if(!Schema.sObjectType.hed__Course_Enrollment__c.isAccessible())
            {
               throw new DMLException();           		
            }
            hed__Course_Enrollment__c courseConnRecord  = [SELECT Id ,
                                      hed__Contact__c,
                                      hed__Contact__r.hed__UniversityEmail__c,
                                      hed__Course_Offering__r.hed__Course__r.Credly_Badge_Id__c,
                                      Grade_Date__c,
                                      hed__Status__c
                                      FROM hed__Course_Enrollment__c
                                      WHERE hed__Status__c = : ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionStatus') AND
                                              Enrolment_Status__c = : ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionEnrolmentStatus') AND
                                                      hed__Course_Offering__r.hed__Course__r.RecordType.DeveloperName = : ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionType') AND Id = : courseConnectionId];

            Set<Id> contactIdSet = new Set<Id>();
            contactIdSet.add(courseConnRecord.hed__Contact__c);

            if (contactIdSet != null) {
                CredlyBadgeCompletion.updateCourseConn(contactIdSet);
            }
        }

    }
}