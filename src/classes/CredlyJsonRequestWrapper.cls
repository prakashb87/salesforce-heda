/*****************************************************************************************************
 *** @Class             : CredlyJsonRequestWrapper
 *** @Author            : Avinash Machineni / Rishabh Anand
 *** @Requirement       : scheduler to send Course Connections associated with the contact 
 *** @Created date      : 13/06/2018
 *** @JIRA ID           : ECB-2446
 *** @LastModifiedBy	: Rishabh Anand
 *** @LastModified date : 13/06/2018
 
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to create a request structure to send Course Connection ID and badge ID to Ipass API. 
 *****************************************************************************************************/ 
public class CredlyJsonRequestWrapper 
{
    
    public String email;
    public List<BadgeDetails> badgeDetails = new List<BadgeDetails>();
    
     public class BadgeDetails
     {
         public Id courseConnectionId;
         public String badgeId;
     }
    
    public static CredlyJsonRequestWrapper parse(String json) 
    {
		return (CredlyJsonRequestWrapper) System.JSON.deserialize(json, CredlyJsonRequestWrapper.class);
	}
    
}