/*****************************************************************************************************
 *** @Class             : CredlyJsonResponseWrapper
 *** @Author            : Avinash Machineni / Rishabh Anand
 *** @Requirement       : scheduler to send Course Connections associated with the contact
 *** @Created date      : 13/06/2018
 *** @JIRA ID           : ECB-2446
 *** @LastModifiedBy	: Rishabh Anand
 *** @LastModified date : 13/06/2018
 
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
*** This class is used to create a response structure to receive Course Connection ID and badge ID from Ipass API. 
 *****************************************************************************************************/ 

public class CredlyJsonResponseWrapper {

	public String email ;
	public String result;
	public String message;
    public List<BadgeResponse> badgeResponse;
	
	public class BadgeResponse {
		public Id courseConnectionId;
		public String badgeId;
		public Date issueDate;
		public  String message;
		
	}
	
    public static CredlyJsonResponseWrapper parse(String json) 
    {
		return (CredlyJsonResponseWrapper) System.JSON.deserialize(json, CredlyJsonResponseWrapper.class);
	}

}