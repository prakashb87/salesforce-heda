@SuppressWarnings('PMD.AvoidGlobalModifier')
global with sharing class CustomButtonSyncBasket extends csbb.CustomButtonExt {
	/*public static final String errorMsg = '{"status":"error","title":"Error",'
                                            + '"text":"Basket must be in status Valid."}';
  
    public String performAction (String basketId) {
        
        set<String> setValidStatus = new set<String> {'Valid', 'Is Valid'};
        cscfga__Product_Basket__c basket;
        PageReference oppPage;
        
        if(Schema.sObjectType.cscfga__Product_Basket__c.isAccessible()){
        	basket = [SELECT Id, cscfga__Basket_Status__c,
                                                csordtelcoa__Synchronised_with_Opportunity__c,
                                                csbb__Synchronised_With_Opportunity__c,
                                                cscfga__Opportunity__c,
                                                cscfga__Opportunity__r.Id, 
                                                cscfga__Total_Price__c
                                            FROM cscfga__Product_Basket__c 
                                            WHERE Id = :basketId];

        	oppPage = new ApexPages.StandardController(basket.cscfga__Opportunity__r).view();
        
        
          
        	doConfirmEnrollment(basket);
                  
        	
        }
        return '{"status":"ok","redirectURL":"' + oppPage.getUrl() + '"}';
        
    }

    @TestVisible
    private void doConfirmEnrollment(cscfga__Product_Basket__c basket) {
        
        Opportunity opp = basket.cscfga__Opportunity__r;
        set<String> setBasketToUnSync = new set<String> ();
        set<String> setBasketToSync = new set<String> { basket.Id };

        list<cscfga__Product_Basket__c> allBaskets = [SELECT Id, cscfga__Basket_Status__c,
                                            csordtelcoa__Synchronised_with_Opportunity__c,
                                            csbb__Synchronised_With_Opportunity__c,
                                            cscfga__Opportunity__r.Id, 
                                            cscfga__Total_Price__c
                                        FROM cscfga__Product_Basket__c 
                                        WHERE cscfga__Opportunity__c = :opp.Id];
                       
        
        
        for (cscfga__Product_Basket__c tmpBasket: allBaskets) { 
            setBasketToUnSync.add(tmpBasket.Id);
            if (tmpBasket.Id == basket.Id) { 
                tmpBasket.csordtelcoa__Synchronised_with_Opportunity__c = true;
                tmpBasket.csbb__Synchronised_With_Opportunity__c = true;
            } else {
                tmpBasket.csordtelcoa__Synchronised_with_Opportunity__c = false;
                tmpBasket.csbb__Synchronised_With_Opportunity__c = false;
            }
        }
        
        ProductUtility.DeleteHardOLIs(setBasketToUnSync);
        ProductUtility.CreateOLIs(setBasketToSync);
        update allBaskets;
        
        UpdateCustomOpportunityStatus.changeCustomopportunityStatus(basket.Id);
    }*/
}