/*****************************************************************
Name: CustomCommunitesLoginController
Author: Capgemini [Rishabh]
*****************************************************************/
/*==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Rishabh Anand           27/07/2018         Initial Version
********************************************************************/
public class CustomCommunitesLoginController {

    public static PageReference redirectURL(){
        
        PageReference pgref = new PageReference(ConfigDataMapController.getCustomSettingValue('CASLoginPage'));
       
        pgref.setRedirect(true);
        
        return pgref;
    }
}