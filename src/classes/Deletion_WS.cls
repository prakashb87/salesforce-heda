/*********************************************************************************
*** @ClassName         : Deletion_WS
*** @Author            : Shubham Singh 
*** @Requirement       : RM-1788,RM-1979
*** @Created date      : 25/09/2018
*** @Modified by       : Shubham Singh 
*** @Modified date     : 16/10/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Delete')
global with sharing class Deletion_WS {
    
    //json object name for delete
    public String objectName;
    
    public List<key> key;
    //json values
    public class key {
        public String studentId;
        public String effectiveDate;
        public String addressType;
        public String nameType;
        public String EMAILTYPE;
        public String phoneType;
        public String fundSourceCode;
        public String HecsExemptStatusCode;
        public String setId;
        public string groups;
        public String location;
        public String groupName;
        public String code;
        public String category;
        public String value;
        public String institution;
        public String campus;
        public String academicCareer;
        public String session;
        public String classnumber;
        public String academicProgram;
        public string term;
        public String courseCode;
        public String studentCareerNumber;
        public String effectiveSequence;
        public String plan;
        public String planSequence;
        public String courseOfferingNumber;
        public String studentPersonalDetail;
        public String studentEmail;
        public String studentAddress;
        public String studentPhone;
        public String studentName;
        public String studentAccountFlag;
        public String school;
        public String college;
        public String academicPlan;
    }
    
    @HttpPost
    global static Map<String,String> deleterecord() {
        
        //to strore respone to send
        Map<String,String> responsetoSend = new Map<String,String>();
        
        //to store object name
        String strObjectName = '';
        
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        try{
            
            String reqStrOne    = request.requestBody.toString();
            system.debug('postString IPASS reqStrOne-----------------> '+reqStrOne);
            //replace the object as object is a system variable name
            String reqStrTwo    = reqStrOne.replaceFirst('"object"', '"objectName"');
            
            String reqStrThree  = reqStrTwo.replaceFirst('"group"', '"groups"');
            
            String reqStrFour   = reqStrThree.replaceFirst('"student.PersonalDetail"', '"studentPersonalDetail"');
            
            String reqStrFive   = reqStrFour.replaceFirst('"student.EMAIL"', '"studentEmail"');
            
            String reqStrSix    = reqStrFive.replaceFirst('"student.Address"', '"studentAddress"');
            
            String reqStrSeven  = reqStrSix.replaceFirst('"student.Phone"', '"studentPhone"');
            
            String reqStrEight  = reqStrSeven.replaceFirst('"student.name"', '"studentName"');
            
            String reqStrNine  = reqStrEight.replaceFirst('"student.AccountFlag"', '"studentAccountFlag"');
            
            //adding [ in the JSON
            String reqStrten   = reqStrNine.replace('{','[{');
            
            //adding ] in the JSON
            String reqStrFinal  = reqStrten.replace('}','}]');
            
            
            response.addHeader('Content-Type', 'application/json');
            
            List<Deletion_WS> postString = (List<Deletion_WS>) System.JSON.deserialize(reqStrFinal, List<Deletion_WS>.class);
            
            system.debug('postString IPASS-----------------> '+postString);
            Deletion_WS deleteRecord;
            
            //store object name to delete and its data
            for(Deletion_WS dws: postString){
                strObjectName = dWs.objectName;
                deleteRecord = dws;
            }
            
            //send the record and object name
            if(deleteRecord != null && strObjectName != null && strObjectName.length() > 0)
            {
                responsetoSend = keyVlaues(deleteRecord ,strObjectName);
            }
            
            //returm response if everything wents fine
            return responsetoSend;
            
        }catch(Exception excpt){
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
    }
    
    public static Map<String,String> keyVlaues(Deletion_WS postString,string strObjectName){     
        
        Map<String,String> responsetoSend = new Map<String,String>();
        //to check the delete type
        map<string,string> checkDeleteType = new map<string,string>();
        //to store variable and its value from JSON
        map<string,string> keyvalue = new map<string,string>();       
        //to store api name of fields 
        map<string,string> apiname = new map<string,string>();
        
        try{
            //record to delete
            Delete_Object_Record__mdt [] deleteObjectRecord = [SELECT id,Field_API_Name__c, JSON_Field_Name__c, JSON_Object_Name__c ,Object_API_Name__c FROM Delete_Object_Record__mdt];        
            
            //type of delete soft or hard
            DeleteMetaData__mdt [] deleteMetaData = [select id,MasterLabel,Operation__c from DeleteMetaData__mdt];
            
            String[] arrOfStr;
            
            String deleteQuery = '' ;
            //store the JSON variable and its value
            for(Deletion_WS.key kw : postString.key){          
                String str = kw + '';
                str = str.remove('key');
                str = str.remove('[');
                str = str.remove(']');
                str = str.remove(':');
                arrOfStr = str.split(',');
            }
            
            String objectApiName = '';
            
            String jsonObjectName = '';
            
            
            //separate the key and its value and add it in keyvalue map
            for(integer i = 0 ; i < arrOfStr.size() ; i ++){
                string st = arrOfStr[i].normalizeSpace();
                String []value = st.split('=');
                if(!(String.isBlank(value[1]) || value[1].equals('null'))){
                    keyvalue.put(value[0].toUppercase(),value[1]);
                    
                }            
            }
            
            //check the number of fields and store api name,object api name and json object name
            Integer count = 0;
            for(Delete_Object_Record__mdt fund : deleteObjectRecord){
                if(fund.JSON_Object_Name__c != null && fund.JSON_Object_Name__c.equalsIgnoreCase(strObjectName)){
                    objectApiName = fund.Object_API_Name__c;
                    jsonObjectName = fund.JSON_Object_Name__c;
                    apiname.put(fund.JSON_Field_Name__c,fund.Field_API_Name__c);
                    count++;
                }
            }
            //Checking delete type for delete operation
            for(DeleteMetaData__mdt deleteData : deleteMetaData){
                if(jsonObjectName != null && jsonObjectName.equalsIgnoreCase(deleteData.MasterLabel))
                   { checkDeleteType.put(deleteData.MasterLabel.toUpperCase(),deleteData.Operation__c);}
            }
            
            //calling delete methods w.r.t there value
            if(checkDeleteType != null && checkDeleteType.size() > 0){
                if(jsonObjectName != null && checkDeleteType.get(jsonObjectName.toUpperCase()) != null && checkDeleteType.get(jsonObjectName.toUpperCase()).equalsIgnoreCase('Hard')){
                    responsetoSend = doDeleteHard(deleteObjectRecord,strObjectName,keyvalue ,objectApiName, apiname, count);
                }else if(jsonObjectName != null && checkDeleteType.get(jsonObjectName.toUpperCase()) != null && checkDeleteType.get(jsonObjectName.toUpperCase()).equalsIgnoreCase('Soft')){
                    responsetoSend = doDeleteSoft(deleteObjectRecord,strObjectName,keyvalue,objectApiName, apiname,count);
                }
            }
            //error criteria
            if(responsetoSend != null && responsetoSend.size() > 0)
            {
                return responsetoSend;
            }
            else{
                responsetoSend = throwError();
                return responsetoSend;
            }
        }catch(Exception excpt){
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    public static Map<String,String> doDeleteHard(Delete_Object_Record__mdt[] getObjectRecord,String strObjectName, Map<string,string> keyValue,String strObjectApiName, Map<string,string> fieldApiName,Integer countOfRecords){
        
        String deleteQuery = '' ;
        
        String baseQuery = '';
        
        String baseQueryTwo = '';
        
        String whereQuery = '';
        
        Map<String,String> responsetoSend = new Map<String,String>();
        
        system.debug('----HARD DELETE------');
        
        try{   
            Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
            
            List<Sobject> hardDeleteRecord;
            
            Schema.DisplayType fielddataType;
            
            Schema.SObjectType objectSchema;
            
            map < string, string > mapDataType = new map < string, string > ();
            
            //field map of an object to store its fields
            Map < String, Schema.SObjectField > fieldMap; 
            
            //get object schema
            if(strObjectApiName != null && strObjectApiName.length() > 0 && schemaMap != null && schemaMap.size() > 0)
             {  
                objectSchema = schemaMap.get(strObjectApiName);
             } 
            
            //get field map
            if(objectSchema != null) 
            {   
                fieldMap = objectSchema.getDescribe().fields.getMap();
            }
            //Loop to iteratre through field names
            for (String fieldName: fieldMap.keySet()) {
                fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                string dataType = fielddataType+'';
                mapDataType.put(fieldName.toUpperCase(),dataType);
            }
            
            //Base query for every hard delete object
            if(strObjectApiName != null && strObjectApiName.length() > 0)
            {
                baseQuery = 'Select id from '+strObjectApiName + ' where ' ;
            }
            
            //to match field count
            Integer checkCount = 0;
            
            //create query for deletion
            for(integer i = 0;i < getObjectRecord.size() ; i++){
                if(getObjectRecord[i].JSON_Object_Name__c != null && getObjectRecord[i].JSON_Object_Name__c.equalsIgnoreCase(strObjectName)){
                    
                    if(getObjectRecord[i].JSON_Field_Name__c != null && keyValue.get(getObjectRecord[i].JSON_Field_Name__c.toUpperCase()) != null){
                        checkCount++;
                        //strObjectApiName = getObjectRecord[i].Object_API_Name__c;
                        if((mapDataType.get(getObjectRecord[i].Field_API_Name__c.toUpperCase()).equalsIgnoreCase('Date'))){
                            whereQuery = fieldApiName.get(getObjectRecord[i].JSON_Field_Name__c)+'= ' + keyValue.get(getObjectRecord[i].JSON_Field_Name__c.toUpperCase()) +'';                        
                        }else{
                            whereQuery = fieldApiName.get(getObjectRecord[i].JSON_Field_Name__c)+'= \'' + keyValue.get(getObjectRecord[i].JSON_Field_Name__c.toUpperCase()) +'\'';                        
                        }
                        
                        baseQueryTwo = baseQueryTwo + whereQuery;
                        if(countOfRecords > 1 && checkCount != countOfRecords){
                            baseQueryTwo = baseQueryTwo + ' and ';
                        }
                    }
                }
            }
            
            //final delete query
            if(baseQuery != null && baseQuery.length() > 0 && baseQueryTwo != null && baseQueryTwo.length() > 0 &&  baseQueryTwo != null && baseQueryTwo.length() > 0 )
            {
                deleteQuery = baseQuery + baseQueryTwo + ' Limit 1';
            }
            //Get record to delete
            if(deleteQuery != null && deleteQuery.length() > 0)
            {
                hardDeleteRecord = Database.query(deleteQuery);  
            }          
            
            //delete the record
            if(hardDeleteRecord != null && hardDeleteRecord.size () > 0)
            {
                Database.delete(hardDeleteRecord,false);
            }
            
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            
            return responsetoSend;
            
        }catch(Exception excpt){
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
    }
    public static Map<String,String> doDeleteSoft(Delete_Object_Record__mdt[] getObjectRecord,String strObjectName, Map<string,string> keyValue,String strObjectApiName, Map<string,string> fieldApiName,Integer countOfRecords){
        system.debug('----SOFT DELETE------');
        
        Map<String,String> responsetoSend = new Map<String,String>();
        try{
            String fieldsToQuery = '';
            
            Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
            
            map < string, string > mapDataType = new map < string, string > ();
            
            Schema.DisplayType fielddataType;
            
            List<Sobject> recordToUpdate;
            
            Map < String, Schema.SObjectField > fieldMap;
            
            Map < String, String > finalMap = new Map < String, String > ();
            
            List<SObject> softUpdateRecord = new List<SObject>();
            
            List<SObject> softUpdateAddressRecord = new List<SObject>();
            
            Sobject object_instance ;
            
            Schema.SObjectType objectSchema ;
            
            //get obejct schema
           if(strObjectApiName != null && strObjectApiName.length() > 0 && schemaMap != null && schemaMap.size() > 0)
           {
             objectSchema = schemaMap.get(strObjectApiName); 
           }
            
            //field map of an object to store its fields
            if(objectSchema != null) 
            {   
                fieldMap = objectSchema.getDescribe().fields.getMap();                      
            }
            if(strObjectApiName != null && strObjectApiName.length() > 0 )
            {
                object_instance = Schema.getGlobalDescribe().get(strObjectApiName).newSObject() ;
            }
            //Loop to iteratre through field names
            for (String fieldName: fieldMap.keySet()) {
                fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                string dataType = fielddataType+'';
                finalMap.put(fieldName.toUpperCase(), fieldName);
                mapDataType.put(fieldName.toUpperCase(), dataType);
            }
            
            
            if(!strObjectApiName.equalsIgnoreCase('Account')){
                for(String fieldName : fieldMap.keyset()){
                    if(fieldsToQuery == null || fieldsToQuery == ''){
                        fieldsToQuery = fieldName;
                    }else{
                        fieldsToQuery = fieldsToQuery + ', ' + fieldName;
                    }
                }
            }else {
                //for any other account
                fieldsToQuery = 'Id , Status__c,Effective_Date__c ';
            }
            
            String softDeleteQuery = '' ;
            String baseQuery = '';
            String baseQueryTwo = '';
            String whereQuery = '';
            
            //base query for update
            baseQuery = 'select ' + fieldsToQuery + ' from ' + strObjectApiName + ' where ' ;
            
            Integer checkCount = 0;
            //create query for deletion
            for(integer i = 0;i < getObjectRecord.size() ; i++){
                if(getObjectRecord[i].JSON_Object_Name__c != null && getObjectRecord[i].JSON_Object_Name__c.equalsIgnoreCase(strObjectName)){
                    if(getObjectRecord[i].JSON_Field_Name__c != null && keyValue.get(getObjectRecord[i].JSON_Field_Name__c.toUpperCase()) != null){
                        checkCount++;
                        
                        //strObjectApiName = getObjectRecord[i].Object_API_Name__c;
                        if(mapDataType.get(getObjectRecord[i].Field_API_Name__c.toUpperCase())  != null && (mapDataType.get(getObjectRecord[i].Field_API_Name__c.toUpperCase()).equalsIgnoreCase('Date'))){
                            whereQuery = fieldApiName.get(getObjectRecord[i].JSON_Field_Name__c)+'= ' + keyValue.get(getObjectRecord[i].JSON_Field_Name__c.toUpperCase()) +'';                        
                        }else{
                            whereQuery = fieldApiName.get(getObjectRecord[i].JSON_Field_Name__c)+'= \'' + keyValue.get(getObjectRecord[i].JSON_Field_Name__c.toUpperCase()) +'\'';                        
                        }
                        baseQueryTwo = baseQueryTwo + whereQuery;
                        if(countOfRecords > 1 && checkCount != countOfRecords){
                            baseQueryTwo = baseQueryTwo + ' and ';
                        }
                    }
                }
            }
            system.debug('baseQueryTwo------'+baseQueryTwo);
            
            if(baseQuery != null && baseQuery.length() > 0 && baseQueryTwo != null && baseQueryTwo.length() > 0 &&  baseQueryTwo != null && baseQueryTwo.length() > 0 )
            {
                softDeleteQuery = baseQuery + baseQueryTwo +' Limit 1';
            }
            
            system.debug('softDeleteQuery------'+softDeleteQuery);
            //Schema.SObjectType sObjectType = objectRecords.getSObjectType();
            //Schema.SObjectType sObjectType = (SObjectType)strObjectApiName;
            //Query record
            if(softDeleteQuery != null && softDeleteQuery.length() > 0){
                recordToUpdate = Database.query(softDeleteQuery);
            }
            system.debug('recordToUpdate------'+recordToUpdate);
            String IdOfContact;
            //to update contact reocrd
            for(Sobject record : recordToUpdate){
                object_instance = record;
                if(strObjectName.equalsIgnoreCase('studentPersonalDetail') || strObjectName.equalsIgnoreCase('studentEmail')|| strObjectName.equalsIgnoreCase('studentPhone') || strObjectName.equalsIgnoreCase('studentName')) { system.debug('strObjectName------'+strObjectName);
                    if(strObjectName.equalsIgnoreCase('studentPersonalDetail')) {object_instance.put('hed__Gender__c','null'); }                  
                    if(strObjectName.equalsIgnoreCase('studentEmail') && keyValue.get('EMAILTYPE') != null && keyValue.get('EMAILTYPE').equalsIgnoreCase('CAMP')){object_instance.put('hed__Preferred_Email__c','');object_instance.put('hed__UniversityEmail__c','');}
                    if(strObjectName.equalsIgnoreCase('studentEmail') && keyValue.get('EMAILTYPE') != null && keyValue.get('EMAILTYPE').equalsIgnoreCase('BUSN')){object_instance.put('hed__Preferred_Email__c','');object_instance.put('hed__WorkEmail__c','');}
                    if(strObjectName.equalsIgnoreCase('studentEmail') && keyValue.get('EMAILTYPE') != null && keyValue.get('EMAILTYPE').equalsIgnoreCase('HOME')){object_instance.put('hed__Preferred_Email__c','');object_instance.put('Email',' ');}
                    if(strObjectName.equalsIgnoreCase('studentEmail') && keyValue.get('EMAILTYPE') != null && keyValue.get('EMAILTYPE').equalsIgnoreCase('Alternate')){object_instance.put('hed__Preferred_Email__c','');object_instance.put('hed__AlternateEmail__c','');}
                    if(strObjectName.equalsIgnoreCase('studentEmail') && keyValue.get('PHONETYPE') != null && keyValue.get('EMAILTYPE').equalsIgnoreCase('PART')){object_instance.put('hed__Preferred_Email__c','');object_instance.put('SAMS_Partner_Email__c','');}
                    if(strObjectName.equalsIgnoreCase('studentPhone') && keyValue.get('PHONETYPE') != null && keyValue.get('PHONETYPE').equalsIgnoreCase('HOME')){object_instance.put('hed__PreferredPhone__c','');object_instance.put('HomePhone','');}
                    if(strObjectName.equalsIgnoreCase('studentPhone') && keyValue.get('PHONETYPE') != null && keyValue.get('PHONETYPE').equalsIgnoreCase('WKPL')){object_instance.put('hed__PreferredPhone__c','');object_instance.put('hed__WorkPhone__c','');}
                    if(strObjectName.equalsIgnoreCase('studentPhone') && keyValue.get('PHONETYPE') != null && keyValue.get('PHONETYPE').equalsIgnoreCase('MOBL')){object_instance.put('hed__PreferredPhone__c','');object_instance.put('MobilePhone','');}
                    if(strObjectName.equalsIgnoreCase('studentPhone') && keyValue.get('PHONETYPE') != null && keyValue.get('PHONETYPE').equalsIgnoreCase('MAIL')){object_instance.put('hed__PreferredPhone__c','');object_instance.put('Mailing_Phone__c','');}
                    if(strObjectName.equalsIgnoreCase('studentPhone') && keyValue.get('PHONETYPE') != null && keyValue.get('PHONETYPE').equalsIgnoreCase('ARES')){object_instance.put('hed__PreferredPhone__c','');object_instance.put('Australian_Residence_phone__c','');}	
                    if(strObjectName.equalsIgnoreCase('studentPhone') && keyValue.get('PHONETYPE') != null && keyValue.get('PHONETYPE').equalsIgnoreCase('FAX')){object_instance.put('hed__PreferredPhone__c','');object_instance.put('Fax','');}	
                    if(strObjectName.equalsIgnoreCase('studentPhone') && keyValue.get('PHONETYPE') != null && keyValue.get('PHONETYPE').equalsIgnoreCase('AGNT')){object_instance.put('hed__PreferredPhone__c','');object_instance.put('Agent_Phone__c','');}
                    if(strObjectName.equalsIgnoreCase('studentName')  && keyValue.get('NAMETYPE') != null  && keyValue.get('NAMETYPE').equalsIgnoreCase('PRI')){object_instance.put('Salutation','null');object_instance.put('Middle_Name__c','');object_instance.put('FirstName','');object_instance.put('LastName','No name');}	
                    if(strObjectName.equalsIgnoreCase('studentName')  && keyValue.get('NAMETYPE') != null  && keyValue.get('NAMETYPE').equalsIgnoreCase('PRF')){object_instance.put('Preferred_First_Name__c','');object_instance.put('Preferred_Name__c','');}
                }else if(strObjectName.equalsIgnoreCase('studentAddress')){
                     	object_instance.put('hed__Parent_Contact__c',null);
                        object_instance.put('hed__MailingCountry__c','');
                        object_instance.put('hed__MailingCity__c','');
                        object_instance.put('hed__MailingStreet__c','');
                        object_instance.put('hed__MailingStreet2__c','');
                        object_instance.put('Mailing_Street_3__c' ,'');
                        object_instance.put('Mailing_Street_4__c' ,'');
                        object_instance.put('hed__MailingState__c','');
                        object_instance.put('hed__MailingPostalCode__c' ,'');
                        object_instance.put('Effective_Date__c',null);
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('HOME')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('MAIL')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('AGNT')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('ARES')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('BUSN')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('WKPL')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('BILL')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('FAX')) {object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('MOBL')){object_instance.put('hed__Address_Type__c','');}
                        if(strObjectName.equalsIgnoreCase('studentAddress') && keyValue.get('ADDRESSTYPE') != null && keyValue.get('ADDRESSTYPE').equalsIgnoreCase('OTH')) {object_instance.put('hed__Address_Type__c','');}
                  		softUpdateAddressRecord.add(object_instance);
                }else if(strObjectName.equalsIgnoreCase('studentAccountFlag') && record.get('NSI_status__c') != null && record.get('NSI_status__c') == True){
                    object_instance.put('NSI_status__c',False);
                }
                if(strObjectName.equalsIgnoreCase('AcademicPlan')){
                    object_instance.put('Effective_Status__c','Inactive');
                    object_instance.put('Effective_Date__c',null);
                }else if(strObjectApiName.equalsIgnoreCase('Account')){
                    object_instance.put('Status__c','Deactive'); 
                    object_instance.put('Effective_Date__c',null);
                }else if(strObjectApiName.equalsIgnoreCase('Contact')){
                    object_instance.put('Status__c','Inactive');
                }
                else{
                    object_instance.put('Status__c','Inactive');
                    object_instance.put('Effective_Date__c',null);
                }
                softUpdateRecord.add(object_instance);
            }
            system.debug('object_instance------'+object_instance);
            //to update address record based on contact id
            //List <hed__Address__c> existingAddress;
            //if(IdOfContact != null && IdOfContact.length() > 0)
            //    existingAddress = [Select id, hed__Parent_Contact__c, hed__Address_Type__c, Effective_Date__c, hed__MailingPostalCode__c, hed__MailingStreet__c FROM hed__Address__c Where hed__Parent_Contact__c =: IdOfContact ];
            
            //system.debug('existingAddress------'+existingAddress);
            
            //Update the address values
            /*if(recordToUpdate != null && recordToUpdate.size () > 0){
                for(Sobject hedAdd :  recordToUpdate){
                    if(strObjectName.equalsIgnoreCase('studentAddress')){
                       
                }
            }*/
            system.debug('softUpdateRecord------'+softUpdateRecord);
             system.debug('softUpdateAddressRecord------'+softUpdateAddressRecord);
            //update 
            if(softUpdateRecord != null && softUpdateRecord.size() > 0 && softUpdateAddressRecord.size() == 0){
                Database.update(softUpdateRecord);    
            }else if(softUpdateRecord != null && softUpdateRecord.size() > 0 && softUpdateAddressRecord != null && softUpdateAddressRecord.size() > 0 ){
                Database.update(softUpdateRecord);    
                Database.update(softUpdateAddressRecord);
            }else{
                //If exception occurs then return this message to IPASS
                responsetoSend = throwError();
                return responsetoSend;
            }  
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            
            return responsetoSend;
        }catch(Exception excpt){
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Deletion of record did not take place, amigo');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}