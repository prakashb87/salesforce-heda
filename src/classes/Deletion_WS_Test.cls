/*********************************************************************************
*** @TestClassName     : Deletion_WS_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Deletion_WS web service apex class.
*** @Created date      : 03/10/2018
**********************************************************************************/
@isTest
public class Deletion_WS_Test {
    static testMethod void coverSoftDelete() {
        contact con = new contact();
        con.Student_ID__c = '3456004';
        con.LastName = 'Test';
        insert con;
        //con.hed__Address_Type__c = 'HOME';
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Delete';
        
        //creating test data
        String JsonMsg = '{"object": "student","key": {"studentId": "3456004"}}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Deletion_WS.deleterecord();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverSoftDeleteAddress() {
        contact con = new contact();
        con.Student_ID__c = '3456005';
        con.LastName = 'Test';
        con.Effective_Date__c = Date.valueOf('2013-06-16');
        insert con;
        
        hed__Address__c addRess = new hed__Address__c();
        addRess.hed__Parent_Contact__c = con.Id;
        addRess.hed__Address_Type__c = 'HOME';
        addRess.Effective_Date__c = Date.valueOf('2013-06-16');
        insert addRess;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Delete';
        
        //creating test data
        String JsonMsg = '{"object": "student.Address","key": {"studentId": "3456005","addressType": "HOME","effectiveDate": "2013-06-16"}}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Deletion_WS.deleterecord();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverSoftDeletePersonalEmail() {
        contact con = new contact();
        con.Student_ID__c = '3456006';
        con.LastName = 'Test';
        con.Effective_Date__c = Date.valueOf('2013-06-16');
        insert con;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Delete';
        
        //creating test data
        String JsonMsg = '{"object": "student.EMAIL","key": {"studentId": "3456006","EMAILTYPE": "Home"}}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Deletion_WS.deleterecord();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverHardDelete() {
        
        Field_of_Study__c fos = new Field_of_Study__c();
        fos.Effective_date__c = Date.valueOf('2001-01-01');
        fos.Name = 'ASCE';
        fos.Field_of_Study_Code__c = '040399';
        insert fos;

        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Delete';
        
        //creating test data
        String JsonMsg = '{"object": "FieldOfStudy","key": {"group": "ASCE","code": "040399","effectiveDate": "2001-01-01"}}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Deletion_WS.deleterecord();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = Deletion_WS.deleterecord();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
  
}