/**
 * @description This abstract factory creates the required dependencies to process 
 * a new customer registration for ECB (Marketplace)
 */
public class ECBRegistrationAbstractFactory implements RegistrationAbstractFactory {

    /**
     * @description Creates the Google captcha instance
     */
    public CaptchaVerificationRequest createCaptcha() {
        return new GoogleCaptchaVerificationRequest();
    }

    /**
     * @description Creates the Passport Idp user status request instance
     */
    public IdentityProviderUserStatusReq createIdpUserStatusReq(String email) {
        return new PassportIdentityProviderUserStatusReq(email);
    }

    /**
     * @description Creates the Passport Idp user creation request instance
     */
    public IdentityProviderUserRequest createIdpUserCreateReq() {
        return new PassportIdentityProviderUserRequest(new PassportIdentityUserValidator());
    }

    /**
     * @description Creates the Passport Idp user delete request instance
     */
    public IdentityProviderUserDeleteRequest createIdpUserDeleteReq() {
        return new PassportIdentityProviderDeleteRequest(); // for now
    }
    
}