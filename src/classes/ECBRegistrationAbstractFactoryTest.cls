/**
 * @description Test class for ECBRegistrationAbstractFactory
 */
@isTest
public class ECBRegistrationAbstractFactoryTest {
    
    @isTest
    static void createCaptchaTest() {
        ECBRegistrationAbstractFactory factory = new ECBRegistrationAbstractFactory();
        System.assertNotEquals(null, factory.createCaptcha());
    }


    @isTest
    static void createIdpUserStatusReqTest() {
        ECBRegistrationAbstractFactory factory = new ECBRegistrationAbstractFactory();
        System.assertNotEquals(null, factory.createIdpUserStatusReq('test@test.com'));
    }


    @isTest
    static void createIdpUserCreateReqTest() {
        ECBRegistrationAbstractFactory factory = new ECBRegistrationAbstractFactory();
        System.assertNotEquals(null, factory.createIdpUserCreateReq());
    }
}