public without sharing class EmailSuccessController {

     //ECB-4581 : Instructions After Registration Details have been Submitted - Rishabh
    @AuraEnabled
    public static boolean resendConfirmationEmail(String contactId) {
        system.debug('cid--->'+contactId);
        Boolean result = false;
        if (contactId != Null) {
            UserRegistrationEmail userReg = new UserRegistrationEmail(contactId); 
            result=userReg.userRegistrationEmail();
            system.debug('Res--.'+result);
        }
        return result;
    }
}