/*********************************************************************************
 *** @Class             : EmailtoCaseHandlerBatch
 *** @Author            : Shubham Singh
 *** @Requirement       : Batch for Updading values Of case on Contact
 *** @Created date      : 10/01/2018
 **********************************************************************************/
/*****************************************************************************************
 *** @About Class
 *** This class is a Batch class that is used to update or delete the previous records of case
 *****************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class EmailtoCaseHandlerBatch implements Database.Batchable < sObject > , Database.Stateful {
    // Method to query and pass the records of case to the execute Method.
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'Select Id,Subject,ParentId,suppliedEmail,origin,contactId,Description from case where origin =\'Email\' AND SuppliedEmail !=NUll '
        );
    }

    global void execute(Database.BatchableContext bc, List <case >scope) {
        // process each batch of records
        set < Id > caseId = new set < Id > ();
        List < Id > attachCaseId = new List < Id > ();
        Map < id,case >delSpamCaseId = new Map < id,case >();
        Map < id,case >delCaseId = new Map < id,case >();
        Map < Id, Id > mergeCase = new Map < Id, Id > ();
        List < Spam_Mails__c > mcs = Spam_Mails__c.getall().values();
        List < Contact > vCon = new List < Contact > ();
        List <case >newcase = new List <case >();
        for (case c : scope) {
        if (mcs != null ) {
            for (Integer j = 0; j < mcs.size(); j++) {
                String emailAddress = c.SuppliedEmail;
                if (emailAddress != null) {
                    if (emailAddress.contains(mcs[j].Spam_Mails__c)) {
                        delSpamCaseId.put(c.Id, c);
                        }
                    }
                }
            }
        }
        //Identify those cases which are reply to the contact previous case and merge them into one and delete the previous ones
        List < Task > insertTask = new List < Task > ();
        for (Case cs: scope) {
            if (cs.ContactId != null && cs.Subject != null) {
                for (Case cse: scope) {
                    if (cs.ContactId == cse.ContactId && cs.Id != cse.Id && cse.Subject != null) {
                        String parentSubject = cs.Subject;
                        String childSubject = cse.Subject;
                        if (parentSubject != null && childSubject != null) {
                            if (childSubject.startsWithIgnoreCase('Re: ') || childSubject.startsWithIgnoreCase('Fwd: ')) {
                                childSubject = childSubject.toUpperCase();                                
                                if (childSubject.contains(parentSubject.toUpperCase())) {
                                    //to get parentcase id
                                    mergecase.put(cse.id, cs.id);
                                    delCaseId.put(cse.id, cse);
                                    //for query on attachment
                                    caseId.add(cse.id);
                                    Task newTask = new Task();
                                    newTask.Description = cse.Description;
                                    newTask.subject = cse.subject;
                                    newTask.status = Label.TaskStatus;
                                    newTask.whatId = cs.id;
                                    insertTask.add(newTask);
                                    //insert seprately
                                }
                            }
                        }
                    }
                }
            }
        }
        if (insertTask.size() > 0 && insertTask != null) {
            Database.insert(insertTask,false);
        }
        List < Attachment > attch = [SELECT Id, Name, body, ParentId, ContentType FROM Attachment where ParentId IN: caseId];
        List < ContentVersion > lstContentVersionID = new List < ContentVersion > ();
        List < ContentDocumentLink > lstContentDocumentLink = new List < ContentDocumentLink > ();
        If(attch != null && attch.size() > 0) {
            ContentVersion cv = new ContentVersion();
            for (Attachment a: attch) {
                cv = new ContentVersion();
                cv.ContentLocation = 'S';
                cv.PathOnClient = a.Name;
                cv.Origin = 'H';
                cv.Title = a.Name;
                cv.VersionData = a.Body;
                attachCaseId.add(a.ParentId);
                lstContentVersionID.add(cv);
            }
            if (lstContentVersionID.size() > 0 && lstContentVersionID != null) {
                Database.insert(lstContentVersionID,false);
            }
            List<Id> ContentDocumentIdList = new List<Id>();
                if(lstContentVersionID != null && lstContentVersionID.size() > 0){
                    for(Integer i = 0; i < lstContentVersionID.size(); i++){
                        ContentDocumentIdList.add(lstContentVersionID[i].ContentDocumentId);
                }
            }
            ContentDocumentLink cdl = new ContentDocumentLink();
            for (Integer i = 0; i < lstContentVersionID.size(); i++) {
                cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = ContentDocumentIdList.isempty() ? cdl.ContentDocumentId : ContentDocumentIdList[i];
                cdl.ShareType = 'V';
                Id parentcaseId = mergecase.get(attachCaseId[i]);
                cdl.LinkedEntityId = parentcaseId;
                lstContentDocumentLink.add(cdl);
            }
            if (lstContentDocumentLink.size() > 0 && lstContentDocumentLink != null) {
                Database.insert(lstContentDocumentLink,false);
            }
        }
        //Delete all cases created from spam emails
        if (delSpamCaseId != null && delSpamCaseId.size() > 0) {
            Database.delete(delSpamCaseId.values(), False);
        }
        //Delete all case which are reply of the previous case.
        if (delCaseId != null && delCaseId.size() > 0) {
            Database.delete(delCaseId.values(), False);
        }
    }
    //Method of Impemented class
    global void finish(Database.BatchableContext bc) {
       system.debug('EmailtoCaseHandlerBatch/finish');
    }
}