/*********************************************************************************
 *** @Class             : EmailtoCaseHandlerTest
 *** @Author            : Shubham Singh
 *** @Requirement       : Test class for EmailtoCaseHandle
 *** @Created date      : 10/01/2018
 **********************************************************************************/
/*****************************************************************************************
 *** @About Class
 *** This class is a test class for EmailtoCaseHandler class
 *****************************************************************************************/
@isTest
public class EmailtoCaseHandlerTest {
  public static testMethod void testEmailtoCaseHandler(){
      ////Method to test that spam emails are not inserted
      Messaging.InboundEmail email = new Messaging.InboundEmail();
      Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
      Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();       
      email.subject = 'Test Subject';
      email.fromName = 'test student';
      email.plainTextBody = 'Hello, this a test email body. For testing purposes only.';
      email.fromAddress = 'testaddress@gmail.com';
      email.htmlBody = 'Test html body';
      Spam_Mails__c em = new Spam_Mails__c();
      em.Name = 'Test Setting';
      em.Spam_Mails__c = 'gmail.com';
      insert em;
      EmailtoCaseHandler emailtocase = new EmailtoCaseHandler();
      Test.startTest();
        result = emailtocase.handleInboundEmail(email, envelope);
      Test.stopTest();
      List<case> lstcase = [select id,suppliedEmail from case where suppliedEmail =:email.fromAddress limit 1];
      System.assertEquals(0, lstcase.size());
  }
  public static testMethod void testEmailtoCaseHandler1(){
       //Method to test the insertion of case if there is contact with email as incoming mail
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();       
        email.subject = 'Test Subject';
        email.fromName = 'test student';
        email.plainTextBody = 'Hello, this a test email body. For testing purposes only.';
        email.fromAddress = 'testaddress@gmail.com';
        email.htmlBody = 'Test html body';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        Contact con1 = new Contact();
        con1.LastName = 'Test Contact';
        con1.Latest_Email__c = 'testaddress@gmail.com';
        insert con1;
        List<Contact> lcon = [select id,name from contact where Latest_Email__c = 'testaddress@gmail.com'] ;
        System.assertEquals(1,lcon.size());
        EmailtoCaseHandler emailtocase = new EmailtoCaseHandler();
        Test.startTest();
            result = emailtocase.handleInboundEmail(email, envelope);
        Test.stopTest();
        List<case> lstcase = [select id,suppliedEmail from case where suppliedEmail =:email.fromAddress limit 1];
        System.assertEquals(1, lstcase.size());
  }
    public static testmethod void testEmailtoCaseHandler2(){
        //Method to test the insertion of case if there is no contact related to the incoming mail
        Messaging.InboundEmail emails = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelopespam = new Messaging.InboundEnvelope();
        Messaging.InboundEmailResult resultspam = new Messaging.InboundEmailResult();   
        emails.subject = 'Testing Subject';
        emails.fromName = 'New Student';
        emails.plainTextBody = 'Hello, this a test email body. For testing purposes only.';
        emails.fromAddress = 'testaddressnew@gmail.com';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        emails.binaryAttachments = binaryAttachments ;
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        emails.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        EmailtoCaseHandler emailtocasenew = new EmailtoCaseHandler();
        Test.startTest();
            resultspam = emailtocasenew.handleInboundEmail(emails, envelopespam);
        Test.stopTest();
        List<case> lstcase = [select id,suppliedEmail from case where suppliedEmail =:emails.fromAddress limit 1];
        System.assertEquals(1, lstcase.size());
    }
    public static testmethod void testEmailtoCaseHandler3(){
        //Method to test the insertion of case in the contact with attachment
        Messaging.InboundEmail emailspam = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelopespam = new Messaging.InboundEnvelope();
        Messaging.InboundEmailResult resultspam = new Messaging.InboundEmailResult();   
        emailspam.subject = 'Testing Subject';
        emailspam.fromName = 'New Student';
        emailspam.plainTextBody = 'Hello, this a test email body. For testing purposes only.';
        emailspam.fromAddress = 'testaddressnew@gmail.com';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        emailspam.binaryAttachments = binaryAttachments ;
             // add an Text atatchment
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        emailspam.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        Contact con = new Contact();
        con.LastName = 'Student';
        con.FirstName = 'New';
        con.Latest_Email__c = 'test@gmail.com';
        insert con;
        EmailtoCaseHandler emailtocasenew = new EmailtoCaseHandler();
        Test.startTest();
            resultspam = emailtocasenew.handleInboundEmail(emailspam, envelopespam);
        Test.stopTest();
        List<case> lstcase = [select id,suppliedEmail from case where ContactId =: con.Id limit 1];
        System.assertEquals(1, lstcase.size());
    }
    public static testmethod void testEmailtoCaseHandler4(){
        //Method to test insertion of task in case if their is a contact that has case with same subject as incoming mail
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();   
        email.subject = 'Re: Testing Subject';
        email.fromName = 'New Student';
        email.plainTextBody = 'Hello, this a test email body. For testing purposes only.';
        email.fromAddress = 'testaddressnew@gmail.com';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        Contact con = new Contact();
        con.LastName = 'Student';
        con.FirstName = 'New';
        con.Latest_Email__c = 'testaddressnew@gmail.com';
        insert con;
        Case cas = new Case();
        cas.Subject = 'Testing Subject';
        cas.ContactId = con.id;
        cas.Description = email.plainTextBody;
        insert cas;
        EmailtoCaseHandler emailtocasenew = new EmailtoCaseHandler();
        Test.startTest();
            result = emailtocasenew.handleInboundEmail(email, envelope);
        Test.stopTest();
        List<Task> lstTask = [select id from Task where whatId =:cas.Id limit 1];
        System.assertEquals(1, lstTask.size()); 
    }
    public static testmethod void testEmailtoCaseHandler5(){
        //Method to test the insertion of case if there is no contact related to the incoming mail
        Messaging.InboundEmail emails = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelopespam = new Messaging.InboundEnvelope();
        Messaging.InboundEmailResult resultspam = new Messaging.InboundEmailResult();   
        emails.subject = 'Testing Subject';
        emails.plainTextBody = 'Hello, this a test email body. For testing purposes only.';
        emails.fromAddress = 'testaddressnew@gmail.com';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        emails.binaryAttachments = binaryAttachments ;
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        emails.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        EmailtoCaseHandler emailtocasenew = new EmailtoCaseHandler();
        Test.startTest();
            resultspam = emailtocasenew.handleInboundEmail(emails, envelopespam);
        Test.stopTest();
        List<case> lstcase = [select id,suppliedEmail from case where suppliedEmail =:emails.fromAddress limit 1];
        System.assertEquals(1, lstcase.size());    
    }
    public static testmethod void testEmailtoCaseHandler6(){
      //to send while sending reply for the same scenario but this time a case will be there
      case c = new case();
      c.Priority = Label.CasePriority;
      c.Status =  Label.CaseStatus;
      c.Subject = 'Test Subject';
      c.Origin = Label.CaseOrigin;
      c.SuppliedEmail = 'testaddress@gmail.com';
      insert c;
      Messaging.InboundEmail email = new Messaging.InboundEmail();
      Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
      Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();       
      email.subject = 'Re: Test Subject';
      email.fromName = 'test student';
      email.plainTextBody = 'Hello, this a test email body. For testing purposes only.';
      email.fromAddress = 'testaddress@gmail.com';
      email.htmlBody = 'Test html body';
      EmailtoCaseHandler emailtocase = new EmailtoCaseHandler();
      Test.startTest();
        result = emailtocase.handleInboundEmail(email, envelope);
      Test.stopTest();
      List<case> lstcase = [select id,suppliedEmail from case where suppliedEmail =:email.fromAddress limit 1];
      System.assertEquals(1, lstcase.size());
    }
}