@isTest
public class EmbeddedCanvasEnrollmentServiceTest {
    
    @testSetup
    static void createTestData() {
        List<Config_Data_Map__c> configurations= new List<Config_Data_Map__c>();
        
        Config_Data_Map__c canvasAccountId = new Config_Data_Map__c();
        canvasAccountId.name = 'CourseConnectionCanvasAccountId';
        canvasAccountId.Config_Value__c = '59';
        configurations.add(canvasAccountId);
        
        
        Config_Data_Map__c canvasType = new Config_Data_Map__c();
        canvasType.name = 'CourseConnectionCanvastype';
        canvasType.Config_Value__c = 'Student Enrollment';
        configurations.add(canvasType);
        
        Config_Data_Map__c canvasEnrollState = new Config_Data_Map__c();
        canvasEnrollState.name = 'CourseConnectionCanvasenrolmentstate';
        canvasEnrollState.Config_Value__c = 'active';
        configurations.add(canvasEnrollState);
        
        Config_Data_Map__c canvasNotify = new Config_Data_Map__c();
        canvasNotify.name = 'CourseConnectionCanvasnotify';
        canvasNotify.Config_Value__c = 'false';
        configurations.add(canvasNotify);
        
        Config_Data_Map__c canvasStage = new Config_Data_Map__c();
        canvasStage.Name = 'CourseConnectionLifeCycleStageCanvas';
        canvasStage.Config_Value__c = 'Post to Canvas';
        configurations.add(canvasStage);
        
        Config_Data_Map__c canvasInitiated = new Config_Data_Map__c();
        canvasInitiated.Name = 'CourseConnectionLifeCycleInitiated';
        canvasInitiated.Config_Value__c = 'Initiated';
        configurations.add(canvasInitiated);
        
        Config_Data_Map__c canvasError = new Config_Data_Map__c();
        canvasError.Name = 'CourseConnectionLifeCycleError';
        canvasError.Config_Value__c = 'Error';
        configurations.add(canvasError);
        
        Config_Data_Map__c canvasSuccess = new Config_Data_Map__c();
        canvasSuccess.Name = 'CourseConnectionLifeCycleSuccess';
        canvasSuccess.Config_Value__c = 'Success';
        configurations.add(canvasSuccess);
        
        insert configurations;
        
        
        Account acc = new Account(name = 'RMIT Online', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        insert acc;
        
        Contact con = new Contact();
        con.LastName='TestCon';
        con.Student_ID__c='1235';
        con.Email='mail@mail.com';
        con.hed__UniversityEmail__c = 'mail@mail.com';
        con.hed__Preferred_Email__c = 'SAMS/SAP University Email';
        Insert con;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'test Course';
        course.hed__Account__c = acc.id;
        course.Status__c='Active';
        course.SIS_Course_Id__c='1254';
        course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
        Insert course;
        
        hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = acc.id);
        insert term;
        
        hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
        crsoffer.name = 'test courseoffer';
        crsoffer.hed__Course__c = course.Id;
        crsoffer.hed__Term__c = term.id;
        crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
        crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
        Insert crsoffer; 
        
        hed__Course_Enrollment__c ce = new hed__Course_Enrollment__c();
        ce.hed__Contact__c = con.Id;
        ce.hed__Course_Offering__c = crsoffer.Id;
        ce.hed__Status__c = 'Current';
        ce.Enrolment_Status__c = 'E';
        ce.Canvas_Integration_Processing_Complete__c = false;
        ce.SAMS_Integration_Processing_Complete__c = true;
        ce.Source_Type__c = 'MKT-Batch';
        Insert ce;
        
        Course_Connection_Life_Cycle__c ccLifeCyc = new Course_Connection_Life_Cycle__c();
        ccLifeCyc.Status__c='Not Started';
        ccLifeCyc.Stage__c='Post to Canvas';
        ccLifeCyc.Course_Connection__c = ce.Id;
        insert ccLifeCyc;
 
    }
    
    @isTest
    static void nullReqTest() {
        MockSuccessSingleCanvasEnrollReq canvasReq = new MockSuccessSingleCanvasEnrollReq();
        EmbeddedCanvasEnrollmentService embeddedCanvasEnrolSer = new EmbeddedCanvasEnrollmentService(canvasReq);
        
        try {
            embeddedCanvasEnrolSer.sendCourseConnIds(null);
            System.assert(false);
        } catch (QueryException e) {
            System.assert(true);    // no exception expected!
        } catch (Exception e) {
            System.assert(false);        // nothing else should fail
        }
    }
    
    @isTest
    static void successSingleEnrollReqTest() {
        MockSuccessSingleCanvasEnrollReq canvasReq = new MockSuccessSingleCanvasEnrollReq();
        EmbeddedCanvasEnrollmentService embeddedCanvasEnrolSer = new EmbeddedCanvasEnrollmentService(canvasReq);
        
        
        Test.startTest();
        try {
            List<hed__Course_Enrollment__c> enrollmentList = [SELECT Id,Canvas_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c LIMIT 1];
            Set<Id> courseConnections = new Set<Id> {enrollmentList[0].Id};
            embeddedCanvasEnrolSer.sendCourseConnIds(courseConnections);
            System.assertEquals(embeddedCanvasEnrolSer.failedCount,0);
            System.assertEquals(embeddedCanvasEnrolSer.updatedCourseConnList[0].Canvas_Integration_Processing_Complete__c,true);
            System.assertEquals(embeddedCanvasEnrolSer.ccLifeCycleList[0].Status__c,'Success');
            
            
        } catch (Exception e) {
            system.debug('Exception:::'+e);
            System.assert(false);        // nothing else should fail
        }
        Test.StopTest();
    }
    
    @isTest
    static void failedSingleEnrollReqTest() {
        MockFailedSingleCanvasEnrollReq canvasReq = new MockFailedSingleCanvasEnrollReq();
        EmbeddedCanvasEnrollmentService embeddedCanvasEnrolSer = new EmbeddedCanvasEnrollmentService(canvasReq);
        
        
        Test.startTest();
        try {
            Id enrollmentId = [SELECT Id FROM hed__Course_Enrollment__c LIMIT 1].Id;
            Set<Id> courseConnections = new Set<Id> {enrollmentId};
            embeddedCanvasEnrolSer.sendCourseConnIds(courseConnections);
            
        } catch (CanvasRequestCalloutException e) {
            // E.g. integration is not working, so expecting an callout exception
            System.assert(true);
            System.assertEquals(embeddedCanvasEnrolSer.failedCount,1);
            System.assertEquals(embeddedCanvasEnrolSer.ccLifeCycleList[0].Status__c,'Error');
        } catch (Exception e) {
            System.assert(false);
        }
        Test.StopTest();
    }
    
    @isTest
    static void canvasErrorResponseTest() {
        MockFailedSingleCanvasEnrollReq canvasReq = new MockFailedSingleCanvasEnrollReq();
        EmbeddedCanvasEnrollmentService embeddedCanvasEnrolSer = new EmbeddedCanvasEnrollmentService(canvasReq);
        
        
        Test.startTest();
        try {
            List<Course_Connection_Life_Cycle__c> lifeCycleList = [SELECT Id,Course_Connection__c,Course_Connection__r.id,Stage__c,Status__c FROM Course_Connection_Life_Cycle__c];
            embeddedCanvasEnrolSer.ccLifeCycleList = new List<Course_Connection_Life_Cycle__c>();
            embeddedCanvasEnrolSer.ccLifeCycleList.addAll(lifeCycleList);
            embeddedCanvasEnrolSer.updateLifeCycleStatusToInitiated();
            System.assertEquals(embeddedCanvasEnrolSer.failedCount,1);
            System.assertEquals(embeddedCanvasEnrolSer.ccLifeCycleList[0].Status__c,'Error');
        } catch (CanvasRequestCalloutException e) {
            // E.g. integration is not working, so expecting an callout exception
            System.assert(false);
        } catch (Exception e) {
            system.debug('Exception::::'+e);
            System.assert(false);
        }
        Test.StopTest();
    }
    
    @isTest
    static void nullCourseConnsForContacts() {
        MockSuccessSingleCanvasEnrollReq canvasReq = new MockSuccessSingleCanvasEnrollReq();
        EmbeddedCanvasEnrollmentService embeddedCanvasEnrolSer = new EmbeddedCanvasEnrollmentService(canvasReq);
        try {
            List<Contact> result = embeddedCanvasEnrolSer.getContactsForCourseEnrollments(null, null);
            System.assert(true);
        } catch (QueryException e) {
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }
    
    @isTest
    static void nullCourseEnrollIdsTest() {
        MockSuccessSingleCanvasEnrollReq canvasReq = new MockSuccessSingleCanvasEnrollReq();
        EmbeddedCanvasEnrollmentService embeddedCanvasEnrolSer = new EmbeddedCanvasEnrollmentService(canvasReq);
        
        try {
            embeddedCanvasEnrolSer.queryCCLifeCycleRec(null);
            System.assert(false);
        } catch (QueryException e) {
            System.assert(true);    // no exception expected!
        } catch (Exception e) {
            System.assert(false);        // nothing else should fail
        }
    }
    
    @isTest
    static void failCountTest() {
        MockSuccessSingleCanvasEnrollReq canvasReq = new MockSuccessSingleCanvasEnrollReq();
        EmbeddedCanvasEnrollmentService embeddedCanvasEnrolSer = new EmbeddedCanvasEnrollmentService(canvasReq);
        
        try {
            embeddedCanvasEnrolSer.failedCount = 5;
            embeddedCanvasEnrolSer.getFailedCount();
            System.assertEquals(embeddedCanvasEnrolSer.failedCount,5);
        } catch (QueryException e) {
            System.assert(true);    // no exception expected!
        } catch (Exception e) {
            System.assert(false);        // nothing else should fail
        }
    }
}