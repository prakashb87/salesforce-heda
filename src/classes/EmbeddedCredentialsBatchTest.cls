@isTest(seeAllData=false)
private class EmbeddedCredentialsBatchTest {
    static Account accDepartment;
    static Account accAdministrative;
    
    static hed__Course__c course;
    static hed__Course_Offering__c courseOffering;

    static User testRunUser;
    static csapi_Message__c csInstanceBasketCreatedSuccessfully;
    static csapi_Message__c csInstanceBasketCreationFailed;
    
    static void setupTestData() {
        
        Account accDepartment = TestUtility.createTestAccount(true
            , 'Department Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        
        Account accAdministrative = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
        
        List<String> lstParams = new List<String> {'Test', 'Con'};
        Contact con = TestUtility.createTestContact(true
            , lstParams
            , accAdministrative.Id);
        
        testRunUser = TestUtility.createUser('Customer Community Login User - RMIT', false);
        testRunUser.ContactId = con.Id;
        insert testRunUser;
        
        /* string testRunUserName = Label.TestRunUser;
        
        testRunUser = [SELECT Id, ContactId, name, UserRoleId FROM User WHERE UserName = :testRunUserName limit 1]; */

        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;

        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute Definition'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0 
            , cscfga__is_significant__c = true);
        insert attrDef;

        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;

        course = new hed__Course__c(Name = 'Course 001'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course;

        hed__Course__c course2 = new hed__Course__c(Name = 'Course 002'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course2;
        
        courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
            , hed__Course__c = course.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today());
        insert courseOffering;

        hed__Course_Offering__c courseOffering2 = new hed__Course_Offering__c(Name = 'Course Offering 002'
            , hed__Course__c = course2.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today()
            , Census_Date__c = system.today().addDays(3));
        insert courseOffering2;

        hed__Course_Enrollment__c courseEnrolment = new hed__Course_Enrollment__c(Enrolment_Status__c = System.Label.Embedded_Course_Enrollment_Status 
            , hed__Course_Offering__c = courseOffering2.Id
            , hed__Contact__c = testRunUser.ContactId);
        insert courseEnrolment;

        Course_Offering_Relationship__c courseOffRel = new Course_Offering_Relationship__c(Course_Offering__c=courseOffering.Id
            , Enrolment_Trigger_Date__c = Date.today().addDays(-3)
            , Related_Course_Offering__c = courseOffering2.Id
            , Status__c = 'Active'
            , RecordTypeId = Schema.SObjectType.Course_Offering_Relationship__c.getRecordTypeInfosByName().get('Embedded').getRecordTypeId());
        insert courseOffRel;

        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name = course.Name
            , Course_Program__c = 'Course'
            , Course_Offering_ID__c = courseOffering.Id
            , cspmb__Product_Definition_Name__c = 'Test definition'
            , cspmb__Effective_Start_Date__c = courseOffering.hed__Start_Date__c
            , cspmb__Effective_End_Date__c = courseOffering.hed__End_Date__c);
        insert priceItem;

        cspmb__Price_Item__c priceItemRel = new cspmb__Price_Item__c(Name = course2.Name
            , Course_ID__c=course2.Id
            , Course_Program__c = 'Course'
            , Course_Offering_ID__c = courseOffering2.Id
            , cspmb__Product_Definition_Name__c = 'Test definition'
            , cspmb__Effective_Start_Date__c = courseOffering2.hed__Start_Date__c
            , cspmb__Effective_End_Date__c = courseOffering2.hed__End_Date__c);
        insert priceItemRel;
        
        Embedded_Credentials_Trigger_Date_Setup__c triggerDateSetupTest=new Embedded_Credentials_Trigger_Date_Setup__c();
        triggerDateSetupTest.Name='DEFAULT_TRIGGER_DATE_SETUP';
        triggerDateSetupTest.To_Current_Date_Offset__c=1;
        triggerDateSetupTest.From_Current_Date_Offset__c=4;
        triggerDateSetupTest.Use_Date_Offset__c=true;
        insert triggerDateSetupTest;

        csapi_Message__c csInstanceAddSuccess = new csapi_Message__c();         
        csInstanceAddSuccess.Name = 'CSAPI_PROD_ADDED_SUCCESSFULLY';        
        csInstanceAddSuccess.Status__c = 'Pass';
        csInstanceAddSuccess.Message__c = 'CSAPI_PROD_ADDED_SUCCESSFULLY';
        csInstanceAddSuccess.Code__c = 'S001';
        insert csInstanceAddSuccess;
        
        csapi_Message__c csInstanceDelSuccess = new csapi_Message__c();         
        csInstanceDelSuccess.Name = 'CSAPI_PROD_DELETED_SUCCESSFULLY';        
        csInstanceDelSuccess.Status__c = 'Pass';
        csInstanceDelSuccess.Message__c = 'CSAPI_PROD_DELETED_SUCCESSFULLY';
        csInstanceDelSuccess.Code__c = 'S002';
        insert csInstanceDelSuccess;
        
        csapi_Message__c csInstanceInfoSuccess = new csapi_Message__c();         
        csInstanceInfoSuccess.Name = 'CSAPI_BASKET_REFRESHED_SUCCESSFULLY';        
        csInstanceInfoSuccess.Status__c = 'Pass';
        csInstanceInfoSuccess.Message__c = 'CSAPI_BASKET_REFRESHED_SUCCESSFULLY';
        csInstanceInfoSuccess.Code__c = 'S003';
        insert csInstanceInfoSuccess;
        
        csapi_Message__c csInstanceSyncSuccess = new csapi_Message__c();         
        csInstanceSyncSuccess.Name = 'CSAPI_BASKET_SYNCED_SUCCESSFULLY';        
        csInstanceSyncSuccess.Status__c = 'Pass';
        csInstanceSyncSuccess.Message__c = 'CSAPI_BASKET_SYNCED_SUCCESSFULLY';
        csInstanceSyncSuccess.Code__c = 'S004';
        insert csInstanceSyncSuccess;

        csapi_Message__c csInstanceAddFail = new csapi_Message__c();         
        csInstanceAddFail.Name = 'CSAPI_PROD_ADD_FAIL';        
        csInstanceAddFail.Status__c = 'Fail';
        csInstanceAddFail.Message__c = 'CSAPI_PROD_ADD_FAIL';
        csInstanceAddFail.Code__c = 'S005';
        insert csInstanceAddFail;

        csapi_Message__c csInstanceAlreadyPurchased = new csapi_Message__c();         
        csInstanceAlreadyPurchased.Name = 'CSAPI_COURSE_ALREADY_PURCHASED';        
        csInstanceAlreadyPurchased.Status__c = 'Fail';
        csInstanceAlreadyPurchased.Message__c = 'CSAPI_COURSE_ALREADY_PURCHASED';
        csInstanceAlreadyPurchased.Code__c = 'S006';
        insert csInstanceAlreadyPurchased;
        
        csInstanceBasketCreatedSuccessfully = new csapi_Message__c();         
        csInstanceBasketCreatedSuccessfully.Name = 'CSAPI_BASKET_CREATED_SUCCESSFULLY';        
        csInstanceBasketCreatedSuccessfully.Status__c = 'Fail';
        csInstanceBasketCreatedSuccessfully.Message__c = 'CSAPI_BASKET_CREATED_SUCCESSFULLY';
        csInstanceBasketCreatedSuccessfully.Code__c = 'S007';
        insert csInstanceBasketCreatedSuccessfully;
        
        csInstanceBasketCreationFailed = new csapi_Message__c();         
        csInstanceBasketCreationFailed.Name = 'CSAPI_BASKET_CREATION_FAIL';        
        csInstanceBasketCreationFailed.Status__c = 'Fail';
        csInstanceBasketCreationFailed.Message__c = 'CSAPI_BASKET_CREATION_FAIL';
        csInstanceBasketCreationFailed.Code__c = 'S008';
        insert csInstanceBasketCreationFailed ;
    }
    
    @isTest 
    static void testEmbeddedCredentials() {
        setupTestData();
        System.runAs(testRunUser) {
            Test.StartTest();
            EmbeddedCredentialsBatch embCredBatch = new EmbeddedCredentialsBatch();
            Database.executeBatch(embCredBatch); 
            Test.StopTest();
            
            system.debug('testRunUser.Id >> ' + testRunUser.Id);
            list<cscfga__Product_Basket__c> baskets = [select Id, Name from cscfga__Product_Basket__c where OwnerId = :testRunUser.Id];
            //system.debug('baskets.size() >> ' + baskets.size());
            //system.debug('after test spc: ' + [select Id, Name, Basket_Number__c, OwnerId from cscfga__Product_Basket__c where OwnerId = :testRunUser.Id]);
            //system.debug('after test all: ' + [select Id, Name, Basket_Number__c, OwnerId from cscfga__Product_Basket__c]);
            system.assertEquals(baskets.size(), 1);
        }   
    }
    
    @isTest 
    static void testEmbeddedCredentialsFailure() {
        setupTestData();
        delete csInstanceBasketCreatedSuccessfully;
        delete csInstanceBasketCreationFailed;
        System.runAs(testRunUser) {
            Test.StartTest();
            EmbeddedCredentialsBatch embCredBatch = new EmbeddedCredentialsBatch();
            Database.executeBatch(embCredBatch); 
            Test.StopTest();
            
            system.debug('testRunUser.Id >> ' + testRunUser.Id);
            list<cscfga__Product_Basket__c> baskets = [select Id, Name from cscfga__Product_Basket__c where OwnerId = :testRunUser.Id];
            //system.debug('baskets.size() >> ' + baskets.size());
            //system.debug('after test spc: ' + [select Id, Name, Basket_Number__c, OwnerId from cscfga__Product_Basket__c where OwnerId = :testRunUser.Id]);
            //system.debug('after test all: ' + [select Id, Name, Basket_Number__c, OwnerId from cscfga__Product_Basket__c]);
            system.assertEquals(baskets.size(), 0);
        }   
    }
}