/*****************************************************************
Name: EmbeddedCredentialsLaunchBatchV2_0 
Author: Capgemini [Parveen Kaushik]
Jira Reference : ECB-5530 (Master), ECB-5794, ECB-5795, ECB-5800, ECB-5801

Purpose: This batch class provides the initiation point for Embeds processing. As part of 
a complete redesign of the Embeds process with a view to optimize the performance and 
throughput; we are eliminating the unnecessary steps and records creation (Oppty, Prod Basket etc).
The new process will simply generate direct Course Enrollment records for eligible students and
pass it over to SAMS/Canvas for micro-cred enrollments.

Sequence of Steps in the new Process:
1. The first step in the overall process is to identify valid relationships i.e. Find all students that are
enrolled in an award course with an embedded credentials (micro-cred). 
2. Validate that the course isn't already purchased (means that the micro-cred is already enrolled previously)  
3. Generate Course Connection Records and Connection Lifecycle records
4. Launch Canvas Integration & SAMS batch processes in a chained fashion

History
--------
Version   Author            Date              Detail
1.0       Parveen           07/06/2019        Initial Version
********************************************************************/

@SuppressWarnings('PMD')
public class EmbeddedCredentialsLaunchBatchV2_0 implements Database.Batchable<Id> {
            
    list<csapi_BasketRequestWrapper.AddProductRequest> lstAddMicroCredentialRequest;
    
    set<id> microCourseOfferIds; //Micro-credentials
    set<id> embedCourseOfferIds; //Award Course offerings
    set<id> setStudentIdsAssigned;

    map<id, Id> mapMicroCredentialsToParentCourse; //contains Course Id for a given micro-cred Course Offering for validations

    map<id, set<id>> mapRelatedCourseToCourseSet;
    map<id, set<id>> mapStudentToMicroCredentialsSet; //Student to Micro-credentials
    map<id, set<id>> mapStudentToEmbededCredentialsSet; //Sudent to Award Course Offering
    map<Id, cspmb__Price_Item__c> mapCourseToPriceItemObj;
	Set<String> studentCourseValidateSet;

    public EmbeddedCredentialsLaunchBatchV2_0() {
		initRelatedCourseToMicroCredentialsMap();
        initStudentToMicroCredentialsMap(microCourseOfferIds);
        initStudentToEmbededCredentialsMap(embedCourseOfferIds);
		validateAlreadyEnrolledEmbeds();
    }
    
    public Iterable<Id> start(Database.BatchableContext ctx) {
        return new List<Id>(setStudentIdsAssigned);
    }

    public void execute(Database.BatchableContext bc, List<Id> studentIds) {
        
        system.debug('EmbeddedCredentialsLaunchBatchV2_0 | EXECUTE  student count:' + studentIds.size());
        List<hed__Course_Enrollment__c> ccList = new List<hed__Course_Enrollment__c>();
		Id studentRecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
		
        for (Id studentId : studentIds) {
            for (id embededCredId: mapStudentToEmbededCredentialsSet.get(studentId)) {
                for (id microCredId: mapRelatedCourseToCourseSet.get (embededCredId)) {
                    if ((!mapStudentToMicroCredentialsSet.containsKey(studentId) 
                        || !mapStudentToMicroCredentialsSet.get(studentId).contains(microCredId))) {

						//Validate for already enrolled embeds
						String studentCourseKey = studentId + '-' + mapMicroCredentialsToParentCourse.get(microCredId);
						System.debug('studentCourseKey '+ studentCourseKey);
						if(studentCourseValidateSet.contains(studentCourseKey)){
							System.debug('Enrollment already exists for Contact '+ studentId + '  and Embeds Course '+mapMicroCredentialsToParentCourse.get(microCredId));
						} else {
							hed__Course_Enrollment__c ccRecord = preprareCourseConnectionRecord(studentId, microCredId, studentRecordTypeId);
							System.debug('EmbeddedCredentialsLaunchBatchV2_0 | Generated: '+ccRecord);
							ccList.add(ccRecord);
						}
                    }
                }
			}
        }
		
		if(ccList.size() > 0){
			insert ccList;
		}
        system.debug('EmbeddedCredentialsLaunchBatchV2_0 | EXECUTE  Course Connection count:' + ccList.size());
		
		Set<Id> ccIdSet = new Set<Id>();
		for(hed__Course_Enrollment__c ccRecord: ccList){
			ccIdSet.add(ccRecord.Id);
		}
		
		/*
			Explicitly reuse and call below method to automatically create stage records on CourseConnection Life Cycle Object
			- In its current state, this method will not initiate SAMS and Canvas integration when called from Batch or Test context
		*/
        CreateRecordsOnCourseConnectionLifeCycle.createCourseConnection(ccIdSet);
		
    }
	
    public void finish(Database.BatchableContext bc) {
        system.debug('EmbeddedCredentialsLaunchBatchV2_0 >> FINISH');
        
        if (!Test.isRunningTest()) {
			EmbeddedSyncBasketIntegrationBatch nextBatch = new EmbeddedSyncBasketIntegrationBatch();  
			Database.executeBatch(nextBatch, 1);
        }
    }
	
	/* HELPER METHODS */
    void initRelatedCourseToMicroCredentialsMap() {
        /// 1. condition: Today >= Trigger Date and Trigger Date < Census Date and Today =< census date
        /// 2. condition: Today == Trigger Date and Trigger Date >= Census Date
        
        Date dtToday = Date.today();
        mapMicroCredentialsToParentCourse = new map<id, Id>();
        
		mapRelatedCourseToCourseSet = new map<id, set<id>>();
        microCourseOfferIds = new set<id>();   
        for (Course_Offering_Relationship__c cor: [select id
                , Course_Offering__c
				, Course_Offering__r.hed__Course__c
                , Related_Course_Offering__c
                , Related_Course_Offering__r.Census_Date__c
                , Status__c
                , Enrolment_Trigger_Date__c
                , Enrolment_Census_Date__c
                , RecordType.Name
            from 
                Course_Offering_Relationship__c 
            where
                Status__c = 'Active' and
                RecordType.Name = 'Embedded' and 
                ((Enrolment_Trigger_Date__c = :dtToday) or
                 (Enrolment_Trigger_Date__c < :dtToday and Enrolment_Census_Date__c >= :dtToday))]) {
                
            if ((cor.Enrolment_Trigger_Date__c == dtToday && cor.Enrolment_Trigger_Date__c >= cor.Enrolment_Census_Date__c) || 
                (cor.Enrolment_Trigger_Date__c <= dtToday && cor.Enrolment_Trigger_Date__c < cor.Enrolment_Census_Date__c)) {
                
                if (!mapRelatedCourseToCourseSet.containsKey(cor.Related_Course_Offering__c)) {  
                    mapRelatedCourseToCourseSet.put(cor.Related_Course_Offering__c, new set<id>());
                }
                mapRelatedCourseToCourseSet.get(cor.Related_Course_Offering__c).add(cor.Course_Offering__c);
                microCourseOfferIds.add(cor.Course_Offering__c);
				
				mapMicroCredentialsToParentCourse.put(cor.Course_Offering__c, cor.Course_Offering__r.hed__Course__c);
            }
        }
        
        embedCourseOfferIds = mapRelatedCourseToCourseSet.keySet();
        
        system.debug('EmbeddedCredentialsLaunchBatchV2_0|mapRelatedCourseToCourseSet >> ' + mapRelatedCourseToCourseSet);
        system.debug('EmbeddedCredentialsLaunchBatchV2_0|embedCourseOfferIds >> ' + embedCourseOfferIds);
        system.debug('EmbeddedCredentialsLaunchBatchV2_0|microCourseOfferIds >> ' + microCourseOfferIds);
    }

    
    void initStudentToMicroCredentialsMap(set<id> microCourseOfferIds) {
        
        mapStudentToMicroCredentialsSet = new map<id, set<id>>();
        for (hed__Course_Enrollment__c enrollment: [select Id
                , hed__Contact__c
                , hed__Course_Offering__c
            from 
                hed__Course_Enrollment__c 
            where 
                hed__Course_Offering__c in :microCourseOfferIds and
                (hed__Status__c = :System.Label.Course_Assessment_Complete_Status or hed__Status__c = :System.Label.Course_Current_Status)]) {
            
            if (!mapStudentToMicroCredentialsSet.containsKey(enrollment.hed__Contact__c)) {
                mapStudentToMicroCredentialsSet.put(enrollment.hed__Contact__c, new set<id>());
            }
            mapStudentToMicroCredentialsSet.get(enrollment.hed__Contact__c).add(enrollment.hed__Course_Offering__c);
        }
        
        system.debug('EmbeddedCredentialsLaunchBatchV2_0|mapStudentToMicroCredentialsSet >> ' + mapStudentToMicroCredentialsSet);
    }
    
    void initStudentToEmbededCredentialsMap(set<Id> embedCourseOfferIds) {
        
        mapStudentToEmbededCredentialsSet = new map<id, set<id>>();
        for (hed__Course_Enrollment__c enrollment: [select Id
                , hed__Contact__c
                , hed__Course_Offering__c
                , hed__Course_Offering__r.hed__Course__c
                , hed__Course_Offering__r.hed__Course__r.Name
            from 
                hed__Course_Enrollment__c 
            where                               
                Enrolment_Status__c = :System.Label.Embedded_Course_Enrollment_Status and 
                hed__Course_Offering__c in :embedCourseOfferIds]) {
            
            if (!mapStudentToEmbededCredentialsSet.containsKey(enrollment.hed__Contact__c)) {
                mapStudentToEmbededCredentialsSet.put(enrollment.hed__Contact__c, new set<id>());
            }
            mapStudentToEmbededCredentialsSet.get(enrollment.hed__Contact__c).add(enrollment.hed__Course_Offering__c);              
        }
        
        setStudentIdsAssigned = mapStudentToEmbededCredentialsSet.keySet();
        
        system.debug('EmbeddedCredentialsLaunchBatchV2_0|mapStudentToEmbededCredentialsSet.keySet()>> ' + mapStudentToEmbededCredentialsSet.keySet());
    }
    
    
	
	public hed__Course_Enrollment__c preprareCourseConnectionRecord(Id contactId, Id courseOfferingId, Id recordTypeId){
		hed__Course_Enrollment__c ccRecord = new hed__Course_Enrollment__c();
		ccRecord.RecordTypeId = recordTypeId ; 
		ccRecord.hed__Contact__c = contactId; 
		ccRecord.hed__Course_Offering__c = courseOfferingId;
		ccRecord.Source_Type__c = ConfigDataMapController.getCustomSettingValue('MKTBatch'); 
		ccRecord.Admission_Type__c = ConfigDataMapController.getCustomSettingValue('AdmissionType');
		ccRecord.hed__Status__c = ConfigDataMapController.getCustomSettingValue('EnrollmentStatus');
		ccRecord.Enrolment_Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE');
		ccRecord.Batch_Processed__c = false; 
		return ccRecord;
	}	
	
	public void validateAlreadyEnrolledEmbeds(){
		//The code below checks for already purchased/enrolled course connections - Similar to csapi_BasketExtension.isCourseAlreadyPurchased()
		List<AggregateResult> courseEnrolAggrResult = [select hed__Contact__c, hed__Course_Offering__r.hed__Course__r.Id courseId, COUNT(Id) 
                            from hed__Course_Enrollment__c  
							where hed__Contact__c IN :setStudentIdsAssigned
                            and (hed__Status__c = :System.Label.Course_Assessment_Complete_Status OR hed__Status__c = :System.Label.Course_Current_Status)
                            and hed__Course_Offering__r.hed__Course__r.Id IN :mapMicroCredentialsToParentCourse.values()							
                            group by hed__Course_Offering__r.hed__Course__r.Id, hed__Contact__c];

		studentCourseValidateSet = new Set<String>();
		for(AggregateResult agr: courseEnrolAggrResult){
			String studentCourseKey = agr.get('hed__Contact__c') + '-' + agr.get('courseId');
			if(!studentCourseValidateSet.contains(studentCourseKey)){
				studentCourseValidateSet.add(studentCourseKey);
			}
		}
		
		System.debug('validateAlreadyEnrolledEmbeds | studentCourseValidateSet: '+studentCourseValidateSet);
	}
}