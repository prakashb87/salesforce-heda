/*****************************************************************
Name: EmbeddedInvoiceGenerationTest 
Author: Capgemini [Shreya]
Purpose: Test Class of EmbeddedInvoiceGenerationController
*****************************************************************/

@isTest
public class EmbeddedInvoiceGenerationTest {
    
    @testSetup
    static void createTestData() {
        List<Config_Data_Map__c> configurations= new List<Config_Data_Map__c>();
        
        Config_Data_Map__c invoiceName = new Config_Data_Map__c();
        invoiceName.name = 'InvoiceAttName';
        invoiceName.Config_Value__c = 'Invoice';
        configurations.add(invoiceName);      
        
        insert configurations;
        
        
        Account acc = new Account(name = 'RMIT Online', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        insert acc;
        
        hed__Address__c addr = new hed__Address__c();
        addr.hed__MailingStreet__c = 'abc';
        addr.hed__MailingStreet2__c = 'test street2';
        addr.hed__MailingCity__c = 'kol';
        addr.hed__MailingState__c = 'wb';
        addr.hed__MailingPostalCode__c = '1234';
        addr.hed__MailingCountry__c = 'Ind';
        addr.Last_Name__c= 'bb';
        Insert addr;
        
        Contact con = new Contact();
        con.LastName='TestCon';
        con.hed__Current_Address__c = addr.Id;
        con.Student_ID__c='1235';
        con.Email='mail@mail.com';
        con.hed__UniversityEmail__c = 'mail@mail.com';
        con.hed__Preferred_Email__c = 'SAMS/SAP University Email';
        
        Insert con;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'test Course';
        course.hed__Account__c = acc.id;
        course.Status__c='Active';
        course.SIS_Course_Id__c='1254';
        course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
        Insert course;
        
        hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = acc.id);
        insert term;
        
        hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
        crsoffer.name = 'test courseoffer';
        crsoffer.hed__Course__c = course.Id;
        crsoffer.hed__Term__c = term.id;
        crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
        crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
        Insert crsoffer; 
        
        cscfga__Product_Basket__c prodbasket = new cscfga__Product_Basket__c ();
         prodbasket.Name = 'New Basket';
         prodbasket.cscfga__Total_Price__c = 100.00;  
         Insert prodbasket ;
         
         //Creating Order test data
        csord__Order_Request__c req= new csord__Order_Request__c(Name='testOrder',csord__Module_Name__c='ModuleTest',csord__Module_Version__c='10',csord__Process_Status__c='requested',csord__Request_DateTime__c=System.now());
        insert req;
        
       //Creating Subscription test data
        csord__Subscription__c sub= new csord__Subscription__c(name= 'SubTest', csord__Identification__c='testIdentification',csord__Order_Request__c=Req.id); 
        insert sub;
        
        //Creating Product Configuration Test Data
        cscfga__Product_Configuration__c prodconfig = new cscfga__Product_Configuration__c();
        prodconfig.Name='ABCD Test';
        prodconfig.cscfga__Quantity__c=1;   
        insert prodconfig;  
        
        csord__Service__c servic= new csord__Service__c();
        servic.Name='Test service';
        servic.csord__Identification__c='TestIdentify';
        servic.csordtelcoa__Main_Contact__c = con.Id;
        servic.csord__Subscription__c=sub.id;
        servic.csord__Order_Request__c=req.id;
        servic.csord__Status__c='StatusRequest';
        servic.csordtelcoa__Product_Configuration__c=prodconfig.id;
        servic.csordtelcoa__Product_Basket__c = prodbasket.Id;
        servic.Course_Offering__c = crsoffer.Id;
        Insert servic;
        
        hed__Course_Enrollment__c ce = new hed__Course_Enrollment__c();
        ce.hed__Contact__c = con.Id;
        ce.hed__Course_Offering__c = crsoffer.Id;
        ce.hed__Status__c = 'Current';
        ce.Enrolment_Status__c = 'E';
        ce.Canvas_Integration_Processing_Complete__c = false;
        ce.SAMS_Integration_Processing_Complete__c = true;
        ce.Source_Type__c = 'MKT-Batch';
        Insert ce;
        

    }
   
    @isTest
    static void successInvoiceCreation() {
        List<csord__Service__c> serList = [SELECT Id FROM csord__Service__c ];
        List<Id> serIdList = new List<Id>();
        for(csord__Service__c ser : serList ){
            serIdList.add(ser.Id);
        }
       Test.startTest();
       try { 
           EmbeddedInvoiceGenerationController.getServices(serIdList);
           List<Payment__c> payRec = [SELECT Id FROM Payment__c];
           system.assert(payRec.size()>0,true);
           Attachment attRec = [SELECT Id FROM Attachment WHERE ParentId = :payRec[0].Id];
           system.assert(attRec!=null,true);
           
        }catch(Exception ex){
            system.assert(false);
        } 
        Test.stopTest();  
    }
    
    @isTest
    static void nullInvoiceCreation() {
        
       Test.startTest();
       try { 
           EmbeddedInvoiceGenerationController.getServices(null);
           
        }catch(Exception ex){
            system.assert(true);
        } 
       Test.stopTest();    
    }
}