/*****************************************************************
Name: EmbeddedSAMSIntegrationBatchTest 
Author: Capgemini [Shreya]
Purpose: Test Class of EmbeddedSAMSIntegrationBatchTest 
*****************************************************************/

@isTest
public class EmbeddedSAMSIntegrationBatchTest {
     
    static testmethod void testSAMSSuccess(){
        EmbedSamsDataFactoryUtil.testEmbeds();
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new samsCalloutMock());
            EmbeddedSamsIntegrationBatch bc = new EmbeddedSamsIntegrationBatch();  
            Database.executeBatch(bc,10);
                     
        Test.stopTest(); 
        
        List<hed__Course_Enrollment__c> courseConnList = [Select id, Name,SAMS_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c ];
        system.debug(courseConnList);
        system.assert(courseConnList.size()>0,true);
        //system.assertEquals(courseConnList[0].SAMS_Integration_Processing_Complete__c,true );
       
    }
    
    static testmethod void testSAMSFailure(){
        EmbedSamsDataFactoryUtil.testEmbeds();     
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new samsCalloutMockFailure());
            EmbeddedSamsIntegrationBatch bc = new EmbeddedSamsIntegrationBatch();  
            Database.executeBatch(bc,10);
                     
        Test.stopTest(); 
        
        List<hed__Course_Enrollment__c> courseConnList = [Select id, Name,SAMS_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c ];
        system.debug(courseConnList);
        system.assert(courseConnList.size()>0,true);
        
    }
    
    public class samsCalloutMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            List<hed__Course_Enrollment__c> cenroll = [Select Id,hed__Contact__r.Student_ID__c from hed__Course_Enrollment__c];
            String jsonBody ='{"payload": [{"courseConnectionId": "'+cenroll[0].Id+'","result": "Success","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[1].Id+'","result": "Error","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[2].Id+'","result": "Success","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[3].Id+'","result": "Success","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[4].Id+'","result": "Error","errorCode": "","errorText": ""}';
            jsonBody+=']}';
           
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    
    public class samsCalloutMockFailure implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {              
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setStatus('Bad Request');
            response.setStatusCode(404);
            return response;
        }
        
    }
}