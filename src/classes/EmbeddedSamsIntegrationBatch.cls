/*****************************************************************
Name: EmbeddedSamsIntegrationBatch 
Author: Capgemini [Shreya]
Purpose: Handle PDF generation,SAMS Integration  for all the course connection records which are created via embedded batch credential job.
Jira Reference : ECB-3900,ECB-4125,ECB-5545,ECB-5549
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           02/07/2018         Initial Version
********************************************************************/
public class EmbeddedSamsIntegrationBatch implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts {

  public String querystring ;
  public List<hed__Course_Enrollment__c> courseConnList = new List<hed__Course_Enrollment__c>();
  @testVisible public Integer failCount = 0;
  
  public Database.QueryLocator start(Database.BatchableContext bc) {
          //ECB-5545: Added SAMS_Integration_Processing_Complete__c check in the query : Shreya
        querystring = 'Select id, Name,Service__c,Batch_Processed__c  FROM hed__Course_Enrollment__c WHERE Source_Type__c = \'MKT-Batch\' AND SAMS_Integration_Processing_Complete__c = false ';  
        querystring += 'AND (hed__Status__c = \'Current\' OR hed__Status__c = \'Enrolled\')';     
        return Database.getQueryLocator(querystring);
    }
    
    public void execute(Database.BatchableContext bc, List<hed__Course_Enrollment__c> scope){
       List<Id> serviceIdList = new List<Id>();
       Set<Id> courseConnIdSet = new Set<Id>();       
       system.debug('scope::::'+scope);
       
       if(!scope.isEmpty()){
           system.debug('scope not empty');
           courseConnList.addAll(scope);
           for(hed__Course_Enrollment__c courseconn : scope){
               //serviceIdList.add(courseconn.Service__c);
               courseConnIdSet.add(courseconn.Id);
           }
       }
       
      //SAMS Integration
      try{
            SAMSEnrollmentRequest enrollReq = new SAMSEnrollmentRequest();
            EmbeddedSAMSEnrollmentService samsCon = new EmbeddedSAMSEnrollmentService(enrollReq);
            samsCon.sendCourseConn(courseConnIdSet);
            //counting on the integration failure
            failCount = failcount+samsCon.getFailedCount();
       }catch(Exception ex){
           ApplicationErrorLogger.logError(ex);
       }
      
      
         
          
    }
    
    public void finish(Database.BatchableContext bc) {
         system.debug('Batch Job Executed');
         system.debug('courseConnList>>>>'+courseConnList);
         system.debug('failCount :::'+failCount );
         //ECB-5549
         if(failCount > 0){
             Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
             String[] toAddresses = new list<string> {ConfigDataMapController.getCustomSettingValue('ECB Support Email')};
             String subject = ConfigDataMapController.getCustomSettingValue('EmbedErrorEmailSubject');
             email.setToAddresses(toAddresses);
             email.setSubject(subject);
             String emailBody = 'SAMS Enrollment Failure on '+ System.Today() + '\n';
             emailBody += 'No of records errored : '+failCount;
             email.setPlainTextBody(emailBody);           
            
             Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
         }    
         
         List<Id> serviceIdList = new List<Id>(); 
         
         if(!courseConnList.isEmpty()){
             for(hed__Course_Enrollment__c courseconn : courseConnList){
                
                 serviceIdList.add(courseconn.Service__c);
             }
         }   
         
         try{
             //PDF generation
            EmbeddedInvoiceGenerationController.getServices(serviceIdList); 
         }catch(Exception ex){
             system.debug('Exception:::'+ex);
         } 
                
        
    }
    
}