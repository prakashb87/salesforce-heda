/*****************************************************************
Name: EmbeddedSyncBasketIntegrationBatchTest 
Author: Capgemini 
Purpose: Test Class of EmbeddedSyncBasketIntegrationBatch for JIRA ID ECB 3900
*****************************************************************/
/*==================================================================
History
--------
Version   Author          Date              Detail
1.0      Shreya           25/06/2018         Initial Version
********************************************************************/
@isTest
public class EmbeddedSyncBasketIntegrationBatchTest {
     
      @testSetup
    static void testData() {
        List<Config_Data_Map__c> config = new List<Config_Data_Map__c>();
        
        Config_Data_Map__c endpoint = new Config_Data_Map__c();
        endpoint.name = 'CanvasEndPointURL';
        endpoint.Config_Value__c = 'https://procs-canvas-v1-dev.npe.integration.rmit.edu.au/api/enrolment';
        config.add(endpoint);
        
        Config_Data_Map__c clientId = new Config_Data_Map__c();
        clientId.name = 'CanvasClientId';
        clientId.Config_Value__c = '12345redt';
        config.add(clientId);
        
        Config_Data_Map__c secretId = new Config_Data_Map__c();
        secretId.name = 'CanvasClientSecretId';
        secretId.Config_Value__c = 'ertret546';
        config.add(secretId);
        
        Config_Data_Map__c supportEmail = new Config_Data_Map__c();
        supportEmail.name = 'ECB Support Email';
        supportEmail.Config_Value__c = 'test@gmail.com';
        config.add(supportEmail);
        
        Config_Data_Map__c emailSub = new Config_Data_Map__c();
        emailSub.name = 'EmbedErrorEmailSubject';
        emailSub.Config_Value__c = 'Embed Error';
        config.add(emailSub);
        
        Config_Data_Map__c canvasAccountId = new Config_Data_Map__c();
        canvasAccountId.name = 'CourseConnectionCanvasAccountId';
        canvasAccountId.Config_Value__c = '59';
        config.add(canvasAccountId);
               
        Config_Data_Map__c canvasType = new Config_Data_Map__c();
        canvasType.name = 'CourseConnectionCanvastype';
        canvasType.Config_Value__c = 'Student Enrollment';
        config.add(canvasType);
        
        Config_Data_Map__c canvasEnrollState = new Config_Data_Map__c();
        canvasEnrollState.name = 'CourseConnectionCanvasenrolmentstate';
        canvasEnrollState.Config_Value__c = 'active';
        config.add(canvasEnrollState);
        
        Config_Data_Map__c canvasNotify = new Config_Data_Map__c();
        canvasNotify.name = 'CourseConnectionCanvasnotify';
        canvasNotify.Config_Value__c = 'false';
        config.add(canvasNotify);
        
        Config_Data_Map__c canvasStage = new Config_Data_Map__c();
        canvasStage.Name = 'CourseConnectionLifeCycleStageCanvas';
        canvasStage.Config_Value__c = 'Post to Canvas';
        config.add(canvasStage);
        
        Config_Data_Map__c canvasInitiated = new Config_Data_Map__c();
        canvasInitiated.Name = 'CourseConnectionLifeCycleInitiated';
        canvasInitiated.Config_Value__c = 'Initiated';
        config.add(canvasInitiated);
        
        Config_Data_Map__c canvasError = new Config_Data_Map__c();
        canvasError.Name = 'CourseConnectionLifeCycleError';
        canvasError.Config_Value__c = 'Error';
        config.add(canvasError);
        
        Config_Data_Map__c canvasSuccess = new Config_Data_Map__c();
        canvasSuccess.Name = 'CourseConnectionLifeCycleSuccess';
        canvasSuccess.Config_Value__c = 'Success';
        config.add(canvasSuccess);
        
        Insert config;
        
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Organization').getRecordTypeId();  
        Id courseEnrollRecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        
        Account acc = new Account(name = 'RMIT Online', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        insert acc;
        
        Account accountRec = new Account(Name='Test Partner',RecordTypeId=accountRecordTypeId);
        Insert accountRec;
        
        //Creating Address test data
        hed__Address__c addr = new hed__Address__c();
        addr.hed__MailingStreet__c = 'abc';
        addr.hed__MailingStreet2__c = 'xyz';
        addr.hed__MailingCity__c= 'Test';
        addr.hed__MailingState__c= 'Mel';
        addr.hed__MailingPostalCode__c= '0121';
        addr.hed__MailingCountry__c = 'Aus';
        addr.Last_Name__c = 'test';
        Insert addr;
        
        Contact con = new Contact();
        con.LastName='TestCon';
        con.Student_ID__c='1235';
        con.Email='mail@mail.com';
        con.hed__UniversityEmail__c = 'mail@mail.com';
        con.hed__Current_Address__c=addr.Id;
        con.hed__Preferred_Email__c = 'SAMS/SAP University Email';
        Insert con;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'test Course';
        course.hed__Account__c = acc.id;
        course.Status__c='Active';
        course.SIS_Course_Id__c='1254';
        course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
        Insert course;
        
        hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = acc.id);
        insert term;
        
        hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
        crsoffer.name = 'test courseoffer';
        crsoffer.hed__Course__c = course.Id;
        crsoffer.hed__Term__c = term.id;
        crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
        crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
        Insert crsoffer; 
        
        hed__Course_Enrollment__c ce = new hed__Course_Enrollment__c();
        ce.hed__Contact__c = con.Id;
        ce.hed__Course_Offering__c = crsoffer.Id;
        ce.hed__Status__c = 'Current';
        ce.Enrolment_Status__c = 'E';
        ce.Canvas_Integration_Processing_Complete__c = false;
        ce.SAMS_Integration_Processing_Complete__c = true;
        ce.Source_Type__c = 'MKT-Batch';
        Insert ce;
        
        List<Course_Connection_Life_Cycle__c> clifecycleList = new List<Course_Connection_Life_Cycle__c>();
        
        Course_Connection_Life_Cycle__c cl1 = new Course_Connection_Life_Cycle__c();
        cl1.Course_Connection__c= ce.Id;
        cl1.Stage__c  = 'Post to Canvas';
        cl1.Status__c  = 'Not Started';
        
        clifecycleList.add(cl1);
        
        Insert clifecycleList;
    } 
    
    static testmethod void testCanvasSucess(){
                      
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new canvasCalloutMockPositive());
            EmbeddedSyncBasketIntegrationBatch bc = new EmbeddedSyncBasketIntegrationBatch();  
            Database.executeBatch(bc,1);
            
            /*String CRON_EXP = '0 15 17 ? * MON-FRI';
            EmbeddedSyncBasketIntegrationScheduler em = new EmbeddedSyncBasketIntegrationScheduler();
            String jobId = System.schedule('Embedded Sync',  CRON_EXP, em);   */      
        Test.stopTest();   
        
        List<hed__Course_Enrollment__c> courseConnList = [Select id, Name,Canvas_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c where hed__Contact__r.Student_ID__c = '1235'];
        system.debug(courseConnList);
        system.assert(courseConnList.size()>0,true);
        system.assertEquals(bc.failCount,0);
 
            
    }
    
     static testmethod void testCanvasFailure(){  
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new canvasCalloutMockError());
            EmbeddedSyncBasketIntegrationBatch bc = new EmbeddedSyncBasketIntegrationBatch();  
            Database.executeBatch(bc,1);
                     
        Test.stopTest(); 
        List<RMErrLog__c> errList = [SELECT Id FROM RMErrLog__c ];
        system.assertEquals(errList.size()>0,true);
        
    }
  
    static testmethod void testCanvasError(){

        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new canvasCalloutMockNegative());
            EmbeddedSyncBasketIntegrationBatch bc = new EmbeddedSyncBasketIntegrationBatch();  
            Database.executeBatch(bc,1);
                
        Test.stopTest(); 
        
       List<hed__Course_Enrollment__c> courseConnList = [Select id, Name,Canvas_Integration_Processing_Complete__c FROM hed__Course_Enrollment__c where hed__Contact__r.Student_ID__c = '1235'];
        system.debug(courseConnList);
        system.assert(courseConnList.size()>0,true);
        
    }
    
     public class canvasCalloutMockPositive implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {          
            List<hed__Course_Enrollment__c> cenroll = [Select Id,hed__Contact__r.Student_ID__c from hed__Course_Enrollment__c where hed__Contact__r.Student_ID__c = '1235'];
            String jsonBody ='{"payload":{"studentId":"1235","enrolmentResponse":[{"courseConnectionId":"'+cenroll[0].id+'","result":"Success","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":""}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    public class canvasCalloutMockNegative implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {           
            List<hed__Course_Enrollment__c> cenroll = [Select Id,hed__Contact__r.Student_ID__c from hed__Course_Enrollment__c where hed__Contact__r.Student_ID__c = '1235'];
            String jsonBody ='{"payload":{"studentId":"1235","enrolmentResponse":[{"courseConnectionId":"'+cenroll[0].id+'","result":"Error","html_url":"https://rmit.test.instructure.com/courses/20445/users/12356","errorMessage":"Course doesnot exist"}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    public class canvasCalloutMockError implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"payload":{"studentId":"551235","enrolmentResponse":[{}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatus('Bad Request');
            response.setStatusCode(404);
            return response;
        }
        
    }
    
 
}