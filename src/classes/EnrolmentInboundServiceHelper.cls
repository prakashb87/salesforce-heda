/*********************************************************************************
 *** @ClassName         : EnrolmentInboundServiceHelper
 *** @Author            : Shubham Singh 
 *** @Requirement       : Helper class to EnrolmentInboundService for creating sets with their respective values
 *** @Created date      : 21/03/2019
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 01/04/2019
 **********************************************************************************/
public with sharing class EnrolmentInboundServiceHelper {
    //wrapper class to create of collection sets to store the respective values 
    public class wrapperEnrolmentInboundServiceHelper {
        public Set < String > courseConnectionUniqueKey = new Set < String > ();
        public Set < String > studentId = new Set < String > ();
        public Set < String > termSet = new Set < String > ();
        public Set < String > institution = new Set < String > ();
        public Set < String > hecsName = new Set < String > ();
        public Set < String > academicProgram = new Set < String > ();
        public Set < String > acdemicCareer = new Set < String > ();
        public Set < String > studentCareerNumber = new Set < String > ();
        public Set < String > classNumber = new Set < String > ();
        public Set < String > fundSource = new Set < String > ();
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Adding elements to the respective sets to process
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    createSetOfWrapper
     * @param     List < InboundParameterImplementations.EnrolmentParameters > parameterList
     * @return    wrapperEnrolmentInboundServiceHelper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static wrapperEnrolmentInboundServiceHelper createSetOfWrapper(List < InboundParameterImplementations.EnrolmentParameters > parameterList) {
        wrapperEnrolmentInboundServiceHelper wrapper = new wrapperEnrolmentInboundServiceHelper();
        //getting values from parameter list and adding into the respective variables to process the logic 
        for (InboundParameterImplementations.EnrolmentParameters parameter: parameterList) {
            wrapper.studentId.add(parameter.studentId);
            wrapper.institution.add(parameter.institution);
            wrapper.acdemicCareer.add(parameter.academicCareer);
            wrapper.studentCareerNumber.add(String.valueOf(parameter.studentCareerNumber));
            wrapper.termSet.add(parameter.term);
            wrapper.classNumber.add(String.valueOf(parameter.classNumber));
            wrapper.hecsName.add(parameter.hecsExemptionStatusCode);
            wrapper.academicProgram.add(parameter.academicProgram);
            wrapper.fundSource.add(parameter.fundingSourceCode);
            wrapper.courseConnectionUniqueKey.add(parameter.academicCareer + parameter.classNumber + parameter.term + parameter.studentId + parameter.institution);
        }
        return wrapper;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Create course connection record based on the JSON values and return
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    processConnectionRecords
     * @param     hed__Course_Enrollment__c courseConnection, InboundParameterImplementations.EnrolmentParameters parameter
     * @return    hed__Course_Enrollment__c
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static hed__Course_Enrollment__c processConnectionRecords(hed__Course_Enrollment__c courseConnection, InboundParameterImplementations.EnrolmentParameters parameter) {

        courseConnection.Class_Number__c = String.valueOf(parameter.classNumber);
        courseConnection.Session__c = parameter.session;
        courseConnection.Academic_Career__c = parameter.academicCareer;
        if (parameter.status != null) {
            courseConnection.hed__Status__c = parameter.status;
            courseConnection.Enrolment_Status__c = parameter.status;
        }
        courseConnection.Status_Reason__c = parameter.statusReason;
        courseConnection.Last_Enrolment_Action__c = parameter.lastAction;
        courseConnection.Last_Action_Reason__c = parameter.lastActionReason;
        if (String.isNotBlank(parameter.addDate)) {
            courseConnection.Enrolment_Add_Date__c = Date.ValueOf(parameter.addDate);
        }
        if (String.isNotBlank(parameter.dropDate)) {
            courseConnection.Enrolment_Drop_Date__c = Date.ValueOf(parameter.dropDate);
        }
        courseConnection.Units_Taken__c = String.ValueOf(parameter.unitsTaken);
        courseConnection.Progress_Units__c = parameter.progressUnits != null ? parameter.progressUnits : null;
        courseConnection.Billing_Units__c = parameter.billingUnits != null ? parameter.billingUnits : null;
        courseConnection.Grading_Basis__c = parameter.gradingBasis;
        if (String.isNotBlank(parameter.gradingBasisDate)) {
            courseConnection.Grading_Basis_Date__c = Date.ValueOf(parameter.gradingBasisDate);
        }
        courseConnection.Official_Grade__c = parameter.officialGrade;
        courseConnection.Grade_Input__c = parameter.gradeInput;
        if (String.isNotBlank(parameter.gradeDate)) {
            courseConnection.Grade_Date__c = Date.ValueOf(parameter.gradeDate);
        }
        courseConnection.Career__c = String.valueOf(parameter.studentCareerNumber);
        courseConnection.Term__c = parameter.term;
        if (String.isNotBlank(parameter.statusDate)) {
            courseConnection.Status_Date__c = Date.ValueOf(parameter.statusDate);
        }
        return courseConnection;
    }
}