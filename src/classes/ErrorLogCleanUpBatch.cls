/*****************************************************************************************************
 *** @Class             : ErrorLogCleanUpBatch
 *** @Author            : Praneet Vig
 *** @Requirement       : Class is created to Clean Error Log records in error log object
 *** @Created date      : 07/06/2018
 *** @JIRA ID           : ECB-3768
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** Class is created to Clean Error Log records in error log object.
 *****************************************************************************************************/ 
public class ErrorLogCleanUpBatch implements Database.Batchable<sObject> {
public Database.QueryLocator start(Database.BatchableContext bc) {
     //configurable value taken from custom settings
    list<Config_Data_Map__c> valuelst=new list<Config_Data_Map__c>();
      valuelst=  [ select Config_Value__c from Config_Data_Map__c where name='ErrorLogTimeline' Limit 1];
     Config_Data_Map__c value =valuelst.size() >0 ? valuelst[0] :new Config_Data_Map__c();
    string query= 'SELECT Id from RMErrLog__c limit 0' ;
    if(valuelst.size()>0){
   	
     Date date2 = system.today();
    
      Date date1 = System.Today() - Integer.valueOf(value.Config_Value__c);
         //Query Error log Records
        query= 'SELECT Id from RMErrLog__c WHERE DAY_ONLY(CreatedDate) <=:' + 'date1';
     
     
    }
     return Database.getQueryLocator(query);  
   
    
}
    public void execute(Database.BatchableContext bc, List<RMErrLog__c> scope2){ 
         try{
    delete scope2;
             system.debug('log deleted successfully');
            
    }
        catch(exception e){
            system.debug('log not deleted');
        }
    }
    public void finish(Database.BatchableContext bc) {
        system.debug('Batch Job Executed');
    }
    
}