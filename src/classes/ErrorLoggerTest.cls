/*****************************************************************************************************
 *** @Class             : ErrorLoggerTest
 *** @Author            : Ming Ma
 *** @Purpose 	        : Log error into RMErrLog__c object
 *** @Created date      : 18/05/2018
 *** @JIRA ID           : ECB-2955
 *****************************************************************************************************/


@isTest
private class ErrorLoggerTest {

	/*@isTest static void test_logError() {
		String errorId = '0015D00000GZ2fqQAD';
		String errorType = 'SAMS Integration';
		String errorPayload = '{"courseConnectionId" : "0065D000002i2Rz", "studentId" : "3711131", "courseId": "00000009914", "courseOfferingId":"59416", "enrolStatus":"R"}';
		String errorCode = '101';
		String errorMessage = 'Invalid Student Id';

		ErrorLogger.logError(errorId, errorType, errorPayload, errorCode, errorMessage);

		List<RMErrLog__c> dbResults = [Select ID, Record_Id__c, Error_Type__c,
		                               Error_Payload__c, Error_Code__c from RMErrLog__c];

		System.assertEquals(1, dbResults.size());
		System.assertEquals(errorId, dbResults[0].Record_Id__c);

	}*/


	/*
    @isTest static void test_sendErrorEmailAlert()
    {
        Config_Data_Map__c setting = new Config_Data_Map__c();
		setting.Name = 'ECB Support Email';
		setting.Config_Value__c = 'ming.ma2@rmit.edu.au';
		
		insert setting;
		
        Test.StartTest();
        ErrorLogger.sendErrorEmailAlert('0065D000002i2Rz');
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations, 'An email has not been sent');
    }
	*/


}