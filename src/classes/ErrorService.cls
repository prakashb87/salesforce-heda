/**
 * Created by mmarsson on 4/03/19.
 */

public with sharing class ErrorService {

    public static final Map <String, String> ERROR_CODE_TABLE = New Map<String,String>{'01'=>'Invalid Payload','02'=>'Invalid Parameter',
    '03'=>'Unexpected empty List','04'=>'Insertion/Update Error', '05'=>'Object not found'};




       public static RMErrLog__c logWebserviceError(String errorCode,String errorMessage ){
        RMErrLog__c err = new RMErrLog__c();
        err.Error_Message__c = errorMessage;
        err.Error_Code__c =errorCode;
        err.Error_Cause__c = ERROR_CODE_TABLE.get(errorCode);
        err.Error_Time__c = System.now();
        err.Error_Type__c = 'Integration';
        err.Logged_In_User__c = Userinfo.getUserId();
        return err;
    }


    public static void logFromExceptionWithAttachment(Exception e, String errorCode, Blob payload){
        RMErrLog__c errorlog = logError(e,errorCode);
        saveError(errorlog);
        addPayloadToError(errorlog, payload);
    }

    public static RMErrLog__c logFromWebserviceErrorWithAttachment(WebserviceException e){
        RMErrLog__c errorlog = logError(e);
        saveError(errorlog);
        addPayloadToError(errorlog, e.payload);
        return errorlog;
    }
     

    public static RMErrLog__c logError(WebserviceException e ){

        RMErrLog__c err = new RMErrLog__c();
        err.Error_Code__c = e.errorCode;
        err.Error_Cause__c = ERROR_CODE_TABLE.get(e.errorCode);
        err.Error_Line_Number__c = e.getLineNumber();
        err.Error_Message__c = e.getMessage();
        err.Error_Time__c = System.now();
        err.Error_Type__c = 'Integration';
        err.Logged_In_User__c = Userinfo.getUserId();
        err.Business_Function_Name__c =e.businesString ;
        err.Error_Payload__c = e.getStackTraceString();

        return err;
    }

    public static RMErrLog__c logError(Exception e , String errorCode){

        RMErrLog__c err = new RMErrLog__c();
        err.Error_Code__c = errorCode;
        err.Error_Cause__c = ERROR_CODE_TABLE.get(errorCode);
        err.Error_Line_Number__c = e.getLineNumber();
        err.Error_Message__c = e.getMessage();
        err.Error_Time__c = System.now();
        err.Error_Type__c = 'Integration';
        err.Logged_In_User__c = Userinfo.getUserId();
        err.Error_Payload__c = e.getStackTraceString();

        return err;
    }

    public static void saveError(RMErrLog__c errorLog){
        if (Schema.sObjectType.RMErrLog__c.isCreateable()){
            Insert errorLog;

        }else{
            System.debug('Who thinks we should have an exception here?');
            //Should throw exception??? Opinions...
        }



    }


    public static void addPayloadToError(RMErrLog__c errorLog, Blob payload){

            FileAttachmentToObject.createFileForSObject('Payload',errorLog.Id, payload);

    }

}