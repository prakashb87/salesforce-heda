/*********************************************************************************
*** @ClassName         : FIELD_OF_EDUCATION_REST
*** @Author            : Dinesh Kumar
*** @Requirement       : 
*** @Created date      : 14/08/2018
*** @Modified by       : Anupam Singhal
*** @modified date     : 12/10/2018
**********************************************************************************/

@RestResource(urlMapping = '/HEDA/v1/FieldOfEducation')
global with sharing class FIELD_OF_EDUCATION_REST {
    public String code;
    public String effectiveDate;
    public String effectiveStatus;
    public String description;
    public String shortDescription;
    
    @HttpPost
    global static Map <String, String> upsertFieldOfEducation() {
        //final response to send
        Map <String, String> responsetoSend = new Map <String,String>();
        //upsert Field Of Education
        List<Field_of_education__c> listUpsertFieldEducation = new List<Field_of_education__c>();
        //to check the effective date
        map < String,Date > updateEffectiveDate = new map < String,Date >();
        
        //Field of Education source
        Field_of_education__c upsertFieldOfEducation;
        try {     
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List<FIELD_OF_EDUCATION_REST> postString = (List<FIELD_OF_EDUCATION_REST>) System.JSON.deserialize(reqStr, List <FIELD_OF_EDUCATION_REST>.class);
            system.debug('postString from IPaaS ------>'+postString); 
            List<String> updateFieldEducation = new List<String>();
            if (postString != null) {
                for (FIELD_OF_EDUCATION_REST fieldEducation : postString) {
                    if (fieldEducation.code != null && (fieldEducation.code).length() > 0) {
                        updateFieldEducation.add(fieldEducation.code);
                        if(fieldEducation.effectiveDate != null && (fieldEducation.effectiveDate).length() > 0)
                        {
                            updateEffectiveDate.put(fieldEducation.code,Date.ValueOf(fieldEducation.effectiveDate));
                        }
                    }else{
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                    
                }
            }
            //getting the old fieldEducation for update
            List<Field_of_education__c> listFieldEducation = new List <Field_of_education__c>();
            if (updateFieldEducation != null && updateFieldEducation.size() > 0 ) {
                listFieldEducation = [Select Id, Name, Description__c, Effective_date__c, Effective_status__c, Short_description__c From Field_of_education__c where Name IN: updateFieldEducation];        }
            
           //map to update the old Field Education
           Map<String, ID> oldFieldEducation = new Map<String, ID>();
           map < String,  Field_of_education__c> stringFOEMap = new map < String, Field_of_education__c> ();
           for (Field_of_education__c fe: listFieldEducation) {
               String getEffDate = fe.Name+'';
               if(updateEffectiveDate != null && updateEffectiveDate.get(getEffDate) != null){
                    if(updateEffectiveDate.get(getEffDate) <= System.today() && fe.Effective_date__c <= updateEffectiveDate.get(getEffDate)){
                        oldFieldEducation.put(fe.Name, fe.ID);
                        stringFOEMap.put(fe.Name,fe);    
                    }else if(fe.Effective_date__c > updateEffectiveDate.get(getEffDate)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend; 
                }
           }
            
            //insert or update the Field Education
            for (FIELD_OF_EDUCATION_REST fse: postString) {
                
                upsertFieldOfEducation = new Field_of_education__c();
                upsertFieldOfEducation.Name = fse.code;
                upsertFieldOfEducation.Effective_date__c = Date.valueOf(fse.effectiveDate);
                upsertFieldOfEducation.Effective_status__c = fse.effectiveStatus != null && fse.effectiveStatus.length() >0? fse.effectiveStatus == 'A'?'Active' : 'Inactive':null;
                upsertFieldOfEducation.Short_description__c = fse.shortDescription;
                upsertFieldOfEducation.Description__c = fse.description;
                if (oldFieldEducation.size() > 0 && oldFieldEducation.get(fse.Code) != null){
                    if (stringFOEMap.get(fse.Code).Effective_date__c <= upsertFieldOfEducation.Effective_date__c ){
                        upsertFieldOfEducation.ID = oldFieldEducation.get(fse.Code);
                    }else{
                        upsertFieldOfEducation=null;
                    }
                }
                
                if(upsertFieldOfEducation !=null)
                {
                    listUpsertFieldEducation.add(upsertFieldOfEducation);
                }
            }
            if (listUpsertFieldEducation != null && listUpsertFieldEducation.size() > 0  && upsertFieldOfEducation.Effective_date__c <= System.today()) {
                upsert listUpsertFieldEducation Name;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
       } catch (Exception excpt) {
           //If exception occurs then return this message to IPASS
           responsetoSend = throwError();
           return responsetoSend;
       }
        }
        
        public static Map < String, String > throwError() {
            //to store the error response
            Map < String, String > errorResponse = new Map < String, String > ();
            errorResponse.put('message', 'Oops! Student was not well defined, amigo.');
            errorResponse.put('status', '404');
            //If error occurs then return this message to IPASS
            return errorResponse;
        }
    }