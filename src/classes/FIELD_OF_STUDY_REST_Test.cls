/*********************************************************************************
*** @TestClassName     : FIELD_OF_STUDY_REST_Test
*** @Author            : Shubham
*** @Requirement       : To Test the FIELD_OF_EDUCATION_REST web service apex class.
*** @Created date      : 20/08/2018
**********************************************************************************/

@isTest
public class FIELD_OF_STUDY_REST_Test {
    static testMethod void myUnitTest() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FieldOfStudy';
        
        //creating test data
        String JsonMsg = '{"groups": "ASCE","code": "040399","effectiveDate": "2001-01-01","effectiveStatus": "A","description": "Building NEC","shortDescription": "BldgNEC"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FIELD_OF_STUDY_REST.upsertFieldOfStudy();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FieldOfStudy';
        
        //creating test data
        String JsonMsg = '{"groups": "","code": "040399","effectiveDate": "2001-01-01","effectiveStatus": "A","description": "Building NEC","shortDescription": "BldgNEC"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FIELD_OF_STUDY_REST.upsertFieldOfStudy();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = FIELD_OF_STUDY_REST.upsertFieldOfStudy();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        
        Field_of_Study__c fos = new Field_of_Study__c();
        
        fos.Name = 'ASCE';
        fos.Field_of_Study_Code__c = '040399';
        fos.Effective_Date__c = Date.ValueOf('2008-01-01');
        insert fos;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/FieldOfStudy';
        
        //creating test data
        String JsonMsg = '{"groups": "ASCE","code": "040399","effectiveDate": "2001-01-01","effectiveStatus": "A","description": "Building NEC","shortDescription": "BldgNEC"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = FIELD_OF_STUDY_REST.upsertFieldOfStudy();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}