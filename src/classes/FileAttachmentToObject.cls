/*********************************************************************************
*** @ClassName         : FileAttachmentToObject
*** @Author            : Dinesh Kumar 
*** @Requirement       : Using Anonymous apex console execution, a user must be able to:
						 Provide a Filename, an object Id and a Blob, and the system should 
						 add a file to the Record with that Id, with the filename provided, 
						 and the .txt extension with the string value of the blob 
						 (Blob must be converted to text) as the file contents.
*** @Created date      : 25/02/2019
*** @Modified by       : Dinesh Kumar
*** @modified date     : 01/03/2019
**********************************************************************************/

public with sharing class FileAttachmentToObject {
    
    
    //This method will take three parameter as fileName(String), parentId(Record Id) and
    //fileContent(Blob). This method is responsible to create file under relatedlist of Record 
    //as a attachment
	public static void createFileForSObject(String fileName, Id parentId, Blob fileContent){
       
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = fileName;
        contentVersion.PathOnClient = contentVersion.Title + '.txt';
        contentVersion.VersionData = fileContent;
        
        try {
        	if (Schema.sObjectType.ContentVersion.fields.Title.IsCreateable()){
        		insert contentVersion;	
        	} 
        	       
        } catch (Exception e) {
            system.debug('ErrorLogger.logError exception occured: ' + e.getMessage());
        }
        Integer count = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion.Id].size();
        if(count > 0){
            contentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion.Id];
        }
        
        ContentDocumentLink contentLink = new ContentDocumentLink();
        contentLink.ContentDocumentId = contentVersion.ContentDocumentId;
        contentLink.ShareType = 'I';
        contentLink.LinkedEntityId = parentId;
 
        try {
        	if (Schema.sObjectType.ContentDocumentLink.fields.ShareType.IsCreateable()){
        		insert contentLink;	
        	} 
        	      
        } catch (Exception e) {
            system.debug('ErrorLogger.logError exception occured: ' + e.getMessage());
        }

    }
    
}