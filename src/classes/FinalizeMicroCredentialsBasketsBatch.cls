public class FinalizeMicroCredentialsBasketsBatch implements Database.Batchable<csapi_BasketRequestWrapper.SyncBasketRequest> {

    list<csapi_BasketRequestWrapper.SyncBasketRequest> requests;

    public FinalizeMicroCredentialsBasketsBatch (list<csapi_BasketRequestWrapper.SyncBasketRequest> syncRequests) {
        this.requests = syncRequests;
    }
    
    public Iterable<csapi_BasketRequestWrapper.SyncBasketRequest> start(Database.BatchableContext ctx) {
        return this.requests;
    }

    public void execute(Database.BatchableContext bc, list<csapi_BasketRequestWrapper.SyncBasketRequest> syncRequests) {
        
        csapi_BasketRequestWrapper.SyncBasketRequest request = syncRequests[0];
        
        Exception ee;
        System.Savepoint sp = Database.setSavepoint();
        try {
            csapi_BasketExtension.syncBasket(request);
            system.debug('FinalizeMicroCredentialsBasketsBatch|execute|request >> ' + request);
        } catch (Exception e) {
            Database.rollback(sp);
            ee = e;
        } finally {
            if (ee != null) {
                
                //
                // log exception message, cause, line and other details
                //            
                ApplicationErrorLogger.logError('Finalize Basket' //String BusinessFunctionName
                    , 'Student ID=' + request.UserId//String FailedRecordId,
                    , ee.getMessage());   //String ErrorMessage    
            }
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        system.debug('FinalizeCredentialsBasketsBatch >> finish');
        
        EmbeddedSyncBasketIntegrationBatch nextBatch = new EmbeddedSyncBasketIntegrationBatch();  
        Database.executeBatch(nextBatch, 1);
    }

}