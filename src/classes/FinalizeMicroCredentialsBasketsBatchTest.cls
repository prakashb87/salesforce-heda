@isTest
private class FinalizeMicroCredentialsBasketsBatchTest {
    
    static Account accDepartment;
    static Account accAdministrative;
    static User u;
    
    static hed__Course__c course;
    static hed__Course_Offering__c courseOffering;

    static void setupTestData() {
        
        Account accDepartment = TestUtility.createTestAccount(true
            , 'Department Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        
        Account accAdministrative = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
        
        List<String> lstParams = new List<String>{'Test', 'Con'};
        Contact con = TestUtility.createTestContact(true
            , lstParams
            , accAdministrative.Id);
        
        u = TestUtility.createUser('Customer Community Login User - RMIT', false);
        u.ContactId = con.Id;
        insert u;
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;

        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute Definition'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0 
            , cscfga__is_significant__c = true);
        insert attrDef;

        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;

        course = new hed__Course__c(Name = 'Course 001'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course;

        hed__Course__c course2 = new hed__Course__c(Name = 'Course 002'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course2;
        
        courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
            , hed__Course__c = course.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today());
        insert courseOffering;

        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name = course.Name
            , Course_Program__c = 'Course'
            , Course_Offering_ID__c = courseOffering.Id
            , cspmb__Product_Definition_Name__c = 'Test definition'
            , cspmb__Effective_Start_Date__c = courseOffering.hed__Start_Date__c
            , cspmb__Effective_End_Date__c = courseOffering.hed__End_Date__c);
        insert priceItem;

        csapi_Message__c csInstanceAddSuccess = new csapi_Message__c();         
        csInstanceAddSuccess.Name = 'CSAPI_PROD_ADDED_SUCCESSFULLY';        
        csInstanceAddSuccess.Status__c = 'Pass';
        csInstanceAddSuccess.Message__c = 'CSAPI_PROD_ADDED_SUCCESSFULLY';
        csInstanceAddSuccess.Code__c = 'S001';
        insert csInstanceAddSuccess;
        
        csapi_Message__c csInstanceDelSuccess = new csapi_Message__c();         
        csInstanceDelSuccess.Name = 'CSAPI_PROD_DELETED_SUCCESSFULLY';        
        csInstanceDelSuccess.Status__c = 'Pass';
        csInstanceDelSuccess.Message__c = 'CSAPI_PROD_DELETED_SUCCESSFULLY';
        csInstanceDelSuccess.Code__c = 'S002';
        insert csInstanceDelSuccess;
        
        csapi_Message__c csInstanceInfoSuccess = new csapi_Message__c();         
        csInstanceInfoSuccess.Name = 'CSAPI_BASKET_REFRESHED_SUCCESSFULLY';        
        csInstanceInfoSuccess.Status__c = 'Pass';
        csInstanceInfoSuccess.Message__c = 'CSAPI_BASKET_REFRESHED_SUCCESSFULLY';
        csInstanceInfoSuccess.Code__c = 'S003';
        insert csInstanceInfoSuccess;
        
        csapi_Message__c csInstanceSyncSuccess = new csapi_Message__c();         
        csInstanceSyncSuccess.Name = 'CSAPI_BASKET_SYNCED_SUCCESSFULLY';        
        csInstanceSyncSuccess.Status__c = 'Pass';
        csInstanceSyncSuccess.Message__c = 'CSAPI_BASKET_SYNCED_SUCCESSFULLY';
        csInstanceSyncSuccess.Code__c = 'S004';
        insert csInstanceSyncSuccess;

        csapi_Message__c csInstanceSyncFail = new csapi_Message__c();         
        csInstanceSyncFail.Name = 'CSAPI_BASKET_SYNC_FAIL';        
        csInstanceSyncFail.Status__c = 'Fail';
        csInstanceSyncFail.Message__c = 'CSAPI_BASKET_SYNC_FAIL';
        csInstanceSyncFail.Code__c = 'S007';
        insert csInstanceSyncFail;

        csapi_Message__c csInstanceAddFail = new csapi_Message__c();         
        csInstanceAddFail.Name = 'CSAPI_PROD_ADD_FAIL';        
        csInstanceAddFail.Status__c = 'Fail';
        csInstanceAddFail.Message__c = 'CSAPI_PROD_ADD_FAIL';
        csInstanceAddFail.Code__c = 'S005';
        insert csInstanceAddFail;

        csapi_Message__c csInstanceAlreadyPurchased = new csapi_Message__c();         
        csInstanceAlreadyPurchased.Name = 'CSAPI_COURSE_ALREADY_PURCHASED';        
        csInstanceAlreadyPurchased.Status__c = 'Fail';
        csInstanceAlreadyPurchased.Message__c = 'CSAPI_COURSE_ALREADY_PURCHASED';
        csInstanceAlreadyPurchased.Code__c = 'S006';
        insert csInstanceAlreadyPurchased;
    }
    
    @isTest 
    static void testBasketSync() {
        
        setupTestData();
        
        System.runAs(u) {
        
            Test.startTest();
    
            csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
            details.CourseName = course.Name;
            details.CourseType = 'Course';
            details.ProductReference = 'Test definition';
            details.CourseOfferingId = courseOffering.Id;
            details.SessionStartDate = string.valueOf(courseOffering.hed__Start_Date__c);
            details.SessionEndDate = string.valueOf(courseOffering.hed__End_Date__c);
            
            csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
            request.Course = details;
            csapi_BasketExtension.addProduct(request);
            
            csapi_BasketRequestWrapper.SyncBasketRequest request2 = new csapi_BasketRequestWrapper.SyncBasketRequest();
            
            list<csapi_BasketRequestWrapper.SyncBasketRequest> requests=new list<csapi_BasketRequestWrapper.SyncBasketRequest>();
            requests.add(request2);
            
            FinalizeMicroCredentialsBasketsBatch finalMicroCr=new FinalizeMicroCredentialsBasketsBatch(requests);
            Database.executeBatch(finalMicroCr); 
            
            Test.stopTest();
            
            cscfga__Product_Basket__c basket = [select Id, Name, csordtelcoa__Synchronised_with_Opportunity__c, csbb__Synchronised_With_Opportunity__c 
                from cscfga__Product_Basket__c 
                where OwnerId = :u.Id limit 1];
        
            system.debug('basket >> ' + basket);
            
            system.assertEquals(basket.csordtelcoa__Synchronised_with_Opportunity__c, true);
        }
    }
    
    @isTest 
    static void testBasketSyncFailed() {
        
        Test.startTest();
        
        csapi_BasketRequestWrapper.SyncBasketRequest request2 = new csapi_BasketRequestWrapper.SyncBasketRequest();
        
        list<csapi_BasketRequestWrapper.SyncBasketRequest> requests = new list<csapi_BasketRequestWrapper.SyncBasketRequest>();
        requests.add(request2);
        
        FinalizeMicroCredentialsBasketsBatch finalMicroBatch = new FinalizeMicroCredentialsBasketsBatch(requests);
        Database.executeBatch(finalMicroBatch); 
        
        Test.stopTest();
        
        RMErrLog__c errorLog = [select Id, Class_Name__c, Method_Name__c from RMErrLog__c limit 1];
        system.assertNotEquals(errorLog, null);
    }
    
}