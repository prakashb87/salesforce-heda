@isTest
public class Form1_EnterLeadControllerTest 
{
    //static Account newAcc;
    static Lead newLead;
    private static Id leadRecordType = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Business Lead').RecordTypeId;
    
    
    static testMethod void testCreateNewStudent()
    {
        PageReference pageRef = Page.GPLR;
        Test.setCurrentPage(pageRef);
        
        newLead = new Lead();
        newLead.RecordTypeId = leadRecordType;
        newLead.FirstName = 'Thomas';
        newLead.LastName = 'Huebner';
        newLead.Company = 'Test Account';
        newLead.Email = 'test@test.com';
        newLead.College2__c = 'College of Business';
        newLead.School__c = 'College of Business Office';
        newLead.Subject_Expert_SE_Consulted__c = 'Yes';
        newLead.Subject_Expert_Name__c = 'Collaborative Research Training (Higher Degrees by Research) - Mary Goodman, Senior Project Officer, School of Graduate Research';
        newLead.Reason_for_not_Consulting_SE__c = 'Reason_for_not_Consulting_SE';
        newLead.HOS_Senior_Executive_informed__c = 'Yes';
        newLead.If_yes_please_provide_their_name_and_ti__c = 'If_yes_please_provide_their_name_and_ti';
        newLead.Reason_for_not_Contacting_HOS_Senior_Exe__c = 'Reason_for_not_Contacting_HOS_Senior_Exe';
        newLead.Proposed_Partner_Institution__c = 'Proposed_Partner_Institution';
        newLead.Current_RMIT_partner__c = 'Yes';
        newLead.If_Yes_briefly_describe_relationship__c = 'If_Yes_briefly_describe_relationship';
        newLead.RMIT_Staff_Title__c = 'Mr.';
        newLead.RMIT_Staff_Name__c = 'RMIT_Staff_Name';
        newLead.RMIT_Staff_Position__c = 'RMIT_Staff_Position';
        newLead.RMIT_Staff_Email__c = 'RMIT_Staff_Email@test.com';
        newLead.RMIT_Staff_Phone__c = 'RMIT_Staff_Phone';
        newLead.Type_of_Planned_Activity__c = 'Research Agreements';
        newLead.Proposed_Activity_Agreement_Start_Date__c = date.parse('10/6/2016');
        newLead.Level_of_Urgency__c = 'Level_of_Urgency';
        newLead.Rationale__c = 'Rationale';
        newLead.Expected_Deliverables__c = 'Expected_Deliverables';
        
        insert newLead;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        Form1_EnterLeadController  testCreateNewStudent = new Form1_EnterLeadController(sc);
        testCreateNewStudent.FirstName = 'Norman';
        testCreateNewStudent.LastName =  'Sinn';
        testCreateNewStudent.Email =  'Norman@test.com';
        //testCreateNewStudent.Mobile = '0999818181';
        
        testCreateNewStudent.Proposed_Partner_Institution=testCreateNewStudent.FirstName + testCreateNewStudent.LastName;
        
        testCreateNewStudent.getCollege();
        testCreateNewStudent.getSchool();
        testCreateNewStudent.getSubject_Expert_SE_Consulted();
        testCreateNewStudent.getHOS_Senior_Executive_informed();
        testCreateNewStudent.getCurrent_Partner();
        testCreateNewStudent.getRMIT_Staff_Title();
        testCreateNewStudent.getProposed_Partner_Title();
        testCreateNewStudent.getCountryOption();
        testCreateNewStudent.getType_of_Planned_Activity();
        testCreateNewStudent.getHoS_Contacted();
        testCreateNewStudent.getSE_Supports_proposal();
        testCreateNewStudent.getSubject_Expert_Name();
        
        
        testCreateNewStudent.AttachedFileName = 'Unit Test Attachment';
        testCreateNewStudent.AttachedFileBody = Blob.valueOf('Unit Test Attachment Body');
        
        testCreateNewStudent.createNewStudent();
        
        List<Lead> newlyCreatedLead=[select id from Lead where email='Norman@test.com'];
        System.assertEquals(1, newlyCreatedLead.size());
    }
    
   
    static testMethod void testCreateNewStudentNegative()
    {
        PageReference pageRef = Page.GPLR;
        Test.setCurrentPage(pageRef);
        
        newLead = new Lead();
        newLead.RecordTypeId = leadRecordType;
        newLead.FirstName = 'Shane';
        newLead.LastName = 'Oliver';
        newLead.Company = 'Test Account';
        newLead.Email = 'test@test.com';
        insert newLead;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        Form1_EnterLeadController testCreateNewStudent = new Form1_EnterLeadController(sc);
        
        testCreateNewStudent.createNewStudent();

        List<Lead> newlyCreatedLead=[select id from Lead where email='test@test.com'];
        System.assertNotEquals(2, newlyCreatedLead.size());
    }
}