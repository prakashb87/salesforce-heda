@isTest
public class Form2_EnterLeadControllerTest 
{
    static Lead newLead;
    private static Id leadRecordType = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Business Lead').RecordTypeId;
    
    
    static testMethod void testCreateNewStudent()
    {
        PageReference pageRef = Page.Intvisit;
        Test.setCurrentPage(pageRef);
        
        newLead = new Lead();
        newLead.RecordTypeId = leadRecordType;
        newLead.FirstName = 'Thomas';
        newLead.LastName = 'Huebner';
        newLead.Company = 'Test Account';
        newLead.Email = 'test@test.com';
        newLead.Past_visits_to_RMIT_When_What_purpose__c = 'Past_visits_to_RMIT_When_What_purpose';
        newLead.proposed_Date__c =  date.parse('10/6/2016');
        newLead.Purpose_Main_Areas_of_Interest__c = 'Deutsche Musik';
       // newLead.Proposed_Time__c = '9am';
        newLead.Proposed_Outcomes__c = 'Proposed_Outcomes';
        newLead.Academic_Focus__c = 'Musik';
        newLead.Number_of_visitors__c = 10;
        newLead.Specific_RMIT_divisions_to_meet__c = 'Specific_RMIT_divisions_to_meet';
        newLead.Names_Roles_of_visitors__c = 'Names_Roles_of_visitors';
        newLead.Current_RMIT_partner__c = 'Yes';
        newLead.Invitation_Letter_required__c = 'No';
        newLead.Informal_Links_with_RMIT__c = 'Informal_Links_with_RMIT';
        
        insert newLead;
        System.assertEquals(newLead.Email,'test@test.com');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        Form2_EnterLeadController  testCreateNewStudent = new Form2_EnterLeadController(sc);
        testCreateNewStudent.leadWrapper.standredFieldsWrapper.FirstName = 'Norman';
        testCreateNewStudent.leadWrapper.standredFieldsWrapper.LastName =  'Sinn';
        testCreateNewStudent.leadWrapper.standredFieldsWrapper.Email =  'Norman@test.com';
        testCreateNewStudent.getProposedTime();
        testCreateNewStudent.getCurrentRMITpartner();
        testCreateNewStudent.getInvitationLetterrequired();
        testCreateNewStudent.getSalutation();
        testCreateNewStudent.getPurposeMainAreasofInterest();
        testCreateNewStudent.getAcademicFocus();
        testCreateNewStudent.getCountryOption();
        
        testCreateNewStudent.leadWrapper.standredFieldsWrapper.AttachedFileName = 'Unit Test Attachment';
        testCreateNewStudent.leadWrapper.standredFieldsWrapper.AttachedFileBody = Blob.valueOf('Unit Test Attachment Body');
        
        testCreateNewStudent.createNewStudent();
    }
    
    static testMethod void testCreateNewStudentNegative()
    {
        PageReference pageRef = Page.Intvisit;
        Test.setCurrentPage(pageRef);
        
        newLead = new Lead();
        newLead.RecordTypeId = leadRecordType;
        newLead.FirstName = 'Shane';
        newLead.LastName = 'Oliver';
        newLead.Company = 'Test Account';
        newLead.Email = 'test@test.com';
        insert newLead;
        System.assertEquals(newLead.Email,'test@test.com');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
        Form2_EnterLeadController testCreateNewStudent = new Form2_EnterLeadController(sc);
        testCreateNewStudent.createNewStudent();
    }
}