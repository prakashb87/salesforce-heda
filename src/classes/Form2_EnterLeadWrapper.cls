/*******************************************
Purpose: Form2 Enter Lead Wrapper
History:
Created by Shalu on 27/02/19
*******************************************/
@SuppressWarnings('PMD.VariableNamingConventions')
public with sharing class Form2_EnterLeadWrapper {
	
	public String Past_visits_to_RMIT_When_What_purpose {get; set;}
	public Date Proposed_Date {get; set;}
    public String Proposed_Outcomes {get; set;}
    public Integer Number_of_visitors {get; set;}
    public String Specific_RMIT_divisions_to_meet {get; set;}
    public String Names_Roles_of_visitors {get; set;}
    public String Informal_Links_with_RMIT {get; set;}    
    public String Position {get; set;}
    
    public Form2_EnterLeadStndFieldsWrapper standredFieldsWrapper {get;set;} 
    
     public Form2_EnterLeadWrapper()
     {
     	standredFieldsWrapper = new Form2_EnterLeadStndFieldsWrapper();
    	this.Past_visits_to_RMIT_When_What_purpose = '';
	    this.Proposed_Outcomes='';
	    this.Number_of_visitors = 0;
	    this.Specific_RMIT_divisions_to_meet ='';
	    this.Names_Roles_of_visitors = '';
	    this.Informal_Links_with_RMIT='';
        this.Position = '';
        this.Proposed_Date = null;
    }
    
}