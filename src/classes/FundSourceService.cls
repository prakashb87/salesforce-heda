/*********************************************************************************
*** @Class			    : FundSourceInboundService
*** @Author		        : Marcello Marsson
*** @Requirement     	: FundSource WebService Service Class 
*** @Created date    	: 26/03/2019
**********************************************************************************/
/*****************************************************************************************
*** @About Class
*** FundSource WebService Service Class 
*****************************************************************************************/

public with sharing class FundSourceService {

    public static List<Fund_Source__c> getFundSourceByName(set<String>fundSourceNames){

        if(Schema.sObjectType.Fund_Source__c.isAccessible()){
           return [SELECT Name, Id,Effective_Date__c,Description__c  FROM Fund_Source__c WHERE Name IN :fundSourceNames];
        }
        return new List<Fund_Source__c>();
    }


    public static Map<String,Fund_Source__c> getFundsourceMapByName(List<Fund_Source__c>fundSourceList){
        Map<String,Fund_Source__c> fundSourceByName = new Map <String,Fund_Source__c>();
        if(fundSourceList==null){
        return fundSourceByName;
        }

        for(Fund_Source__c fundSource : fundSourceList){
            fundSourceByName.put(fundSource.Name,fundSource);
        }
        return fundSourceByName;
    }
}