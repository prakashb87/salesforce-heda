/*****************************************************************
Name: GeneratePDFController
Author: Capgemini 
Purpose: To create dynamic content for Invoice pdf
JIRA Reference : ECB-102 : Generate a PDF invoice and reference in payment table to order
                 ECB-3241 : Consume receipt ID from OneStop and add to payment table and PDF
*****************************************************************/
/*==================================================================
History 
--------
Version   Author            Date              Detail
1.0       Shreya            08/05/2018         Initial Version
********************************************************************/


public without sharing class  GeneratePDFController {
    // Without sharing since the user that invokes the PDF generation can
    // be a communities user which does not have any sharing capabilities
   
   public Map<Id,List<cscfga__Attribute__c>> prodConfigIdAttributeMap;
   public List<GeneratePDFController.CourseDetailWrapper> courseWrapperList {get;set;}
   public String studentName {get;set;}
   public String studentId {get;set;}
   public String studentAddress {get;set;}
   public String basketNumber {get;set;}
   public Decimal totalPrice {get;set;}
   public String prodConfigIds;
   public String receiptNo {get;set;}
   public String todayDate{get;set;}
   public Boolean displayPaymentReference {get;set;}
   public String street {get;set;}
   public String city {get;set;}
   public String cityStatePOCode{get;set;}
   public String statePostalCode {get;set;}
   public String country {get;set;}
   public Date dat {get;set;} 
   public String invoiceABN{get;set;}
   
   //constructor
   public GeneratePDFController()
   {
       system.debug('constructor');
       //getPDFContent();
       //ECB-4253:Shreya  - Added null checks for the parameter received from url
       system.debug('studentName:::'+studentName);
       if( !String.IsBlank(Apexpages.currentPage().getParameters().get('invoiceABN')))
       {
         invoiceABN = Apexpages.currentPage().getParameters().get('invoiceABN');
       }
       else
       {
            invoiceABN = '';
       }
        if(Apexpages.currentPage().getParameters().get('studentName')!='null')
        {
            studentName = Apexpages.currentPage().getParameters().get('studentName');
        }
        else
        {
            studentName = '';
        }   
        
        if(Apexpages.currentPage().getParameters().get('studentId')!='null')
        { 
            studentId = Apexpages.currentPage().getParameters().get('studentId');
        }
        else
        {
            studentId = '';
        }  
        
        /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf:Starts : 25/07/2018 **/  
        if(Apexpages.currentPage().getParameters().get('street')!='null')
        {
            street = Apexpages.currentPage().getParameters().get('street');
        }
        else
        {
            street = '';
        }
        
        if(Apexpages.currentPage().getParameters().get('cityStatePOCode')!='null')
        {
            cityStatePOCode = Apexpages.currentPage().getParameters().get('cityStatePOCode');
        }
        else
        {
            cityStatePOCode = '';
        }
        
        if(Apexpages.currentPage().getParameters().get('country')!='null')
        {
            country = Apexpages.currentPage().getParameters().get('country');
        }
        else
        {
            country = '';
        }
        
         /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf: Ends: 25/07/2018 **/
         
        if(Apexpages.currentPage().getParameters().get('basketNumber')!='null')
        {
            basketNumber = Apexpages.currentPage().getParameters().get('basketNumber');
        }
        else
        {
            basketNumber = '';
        }    
        prodConfigIds = Apexpages.currentPage().getParameters().get('prodConfigIds');
        
        /*if(Apexpages.currentPage().getParameters().get('totalPrice')!='null')
        {
            totalPrice = Apexpages.currentPage().getParameters().get('totalPrice');
        }
        else
        {
            totalPrice = '';
        } */
        
        if(Apexpages.currentPage().getParameters().get('receiptNo')!='null')
        {  
            if(Apexpages.currentPage().getParameters().get('receiptNo') == 'n/a')
            { 
                displayPaymentReference = false;
            }
            else
            {
                receiptNo = Apexpages.currentPage().getParameters().get('receiptNo');
                 displayPaymentReference = true; 
            }    
        }
        else
        {
            receiptNo = '';
        }    
        
        //ECB-4253 - Updated date format to dd/mm/yyyy
        String monthVal = System.TODAY().month() + 1 > 10 ? String.valueOf(System.TODAY().month()) : '0'+System.TODAY().month() ;
        todayDate= (System.TODAY().day()+'/'+monthVal +'/'+System.TODAY().year()).trim();
        system.debug('studentName::::'+studentName+'studentId:::'+studentId+'studentAddress:::'+studentAddress+'basketNumber:::'+basketNumber+'prodConfigIds::'+prodConfigIds);
        
         dat= System.TODAY();
   }
   
    /********************************************************************
    // Purpose              : GetProduct Configuration Ids and query related Attribute records                             
    // Author               : Capgemini [Shreya]
    // Parameters           : null
    //  Returns             : List<GeneratePDFCOntroller.CourseDetailWrapper>
    //JIRA Reference        : ECB-102 : Generate a PDF invoice and reference in payment table to order
    //********************************************************************/
    
      
   public List<GeneratePDFCOntroller.CourseDetailWrapper> getPDFContent(){

        
        List<Id> productConfigIdList = new List<id>();
        if(prodConfigIds!=null && prodConfigIds!=''){
            productConfigIdList = prodConfigIds.split(',');
        }    
        
        system.debug('productConfigIdList::::'+productConfigIdList);
        
        //Querying the related Attributes of Product Configuration
        List<cscfga__Attribute__c>  attributeList = [SELECT cscfga__Value__c,Name,cscfga__Product_Configuration__c,cscfga__Product_Configuration__r.cscfga__Product_Definition__r.name FROM cscfga__Attribute__c 
                                                     WHERE cscfga__Product_Configuration__c in :productConfigIdList and 
                                                     Name IN ('CourseName','CredentialType','Fees','Price','SessionEndDate','SessionStartDate','GST Applicable')];
        
        //Map to store the Product Configuration Id and its related Attributes List
        prodConfigIdAttributeMap = new Map<Id,List<cscfga__Attribute__c>>();
        List<cscfga__Attribute__c> attrList;
        
        if(!attributeList.isEmpty())
        {
            for(cscfga__Attribute__c attr : attributeList)
            {
                if(prodConfigIdAttributeMap.containsKey(attr.cscfga__Product_Configuration__c))
                {
                    attrList = prodConfigIdAttributeMap.get(attr.cscfga__Product_Configuration__c);
                    attrList.add(attr); 
                    prodConfigIdAttributeMap.put(attr.cscfga__Product_Configuration__c,attrList);
                    
                }
                else
                {
                    attrList = new List<cscfga__Attribute__c>();
                    attrList.add(attr);
                    prodConfigIdAttributeMap.put(attr.cscfga__Product_Configuration__c,attrList);
                }
            }
        }    
        
        system.debug('prodConfigIdAttributeMap:::::'+prodConfigIdAttributeMap);
        List<GeneratePDFCOntroller.CourseDetailWrapper> courseWrapperList = new List<GeneratePDFCOntroller.CourseDetailWrapper>();
        
        if(!productConfigIdList.isEmpty())
        {
            totalPrice = 0.00;
            for(Id confidid : productConfigIdList)
            {
                CourseDetailWrapper attrObj = new CourseDetailWrapper();
                attrObj = createCourseWrapperList(confidid);
                // starts:new invoice change to include GST
                if (attrObj.gstApplicable =='Yes' && !String.isBlank(attrObj.fees))
                 {
                    attrObj.gstAmount = (Decimal.ValueOf(attrObj.fees)-(Decimal.ValueOf(attrObj.fees)/1.1)).setScale(2);
                    attrObj.feesWithoutGST = (Decimal.ValueOf(attrObj.fees)-attrObj.gstAmount).setScale(2);
                 }
                 if (attrObj.gstApplicable =='No' && !String.isBlank(attrObj.fees))
                 {
                    attrObj.feesWithoutGST = (Decimal.ValueOf(attrObj.fees)).setScale(2);
                    attrObj.gstAmount=0.00;
                 }
                 // Ends:new invoice change to include GST  
                courseWrapperList.add(attrObj);
                if(!String.isBlank(attrObj.fees))
                {
                    totalPrice  = (totalPrice + Decimal.ValueOf(attrObj.fees)).setScale(2);
                }
                else
                {
                    totalPrice  = totalPrice ;
                }                                
                  

            }
        }    
        
        system.debug('courseWrapperList::::'+courseWrapperList);
        if(!courseWrapperList.isEmpty()){
            return courseWrapperList;
        }
        else
        {
            return null;
        }    
        
         
   }
   
   /********************************************************************
    // Purpose              : Create CourseDetailWrapper record based on the records returned from Attribute object                         
    // Author               : Capgemini [Shreya]
    // Parameters           : Id prodConfigId
    //  Returns             : GeneratePDFCOntroller.CourseDetailWrapper
    //JIRA Reference        : ECB-102 : Generate a PDF invoice and reference in payment table to order
    //********************************************************************/
   
   
   public GeneratePDFCOntroller.CourseDetailWrapper createCourseWrapperList(Id prodConfigId){
        system.debug('prodConfigId:::'+prodConfigId);
        //Wrapper List to store the course details
        List<GeneratePDFCOntroller.CourseDetailWrapper> courseWrapperList = new List<GeneratePDFCOntroller.CourseDetailWrapper>();
        GeneratePDFCOntroller.CourseDetailWrapper attrObj = new GeneratePDFCOntroller.CourseDetailWrapper();
        
       
        if(prodConfigIdAttributeMap.containsKey(prodConfigId))
        {
            for(cscfga__Attribute__c attr : prodConfigIdAttributeMap.get(prodConfigId))
            {
                system.debug('attr:::'+attr);         
                if(attr.Name==ConfigDataMapController.getCustomSettingValue('CourseName'))
                { //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                    attrObj.courseName = attr.cscfga__Value__c;         
                }
                else if(attr.Name==ConfigDataMapController.getCustomSettingValue('Fees'))
                { //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                    attrObj.fees = attr.cscfga__Value__c;   
                    attrObj.feesInDecimal = (Decimal.ValueOf(attrObj.fees)).setScale(2);
                }
                else if(attr.Name==ConfigDataMapController.getCustomSettingValue('Price'))
                {
                    attrObj.fees = attr.cscfga__Value__c; 
                    attrObj.feesInDecimal = (Decimal.ValueOf(attrObj.fees)).setScale(2);
                }
                else if(attr.Name==ConfigDataMapController.getCustomSettingValue('SessionStartDate'))
                { //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                    if(attr.cscfga__Value__c!=null && attr.cscfga__Value__c!='')
                    {
                        List<String> startDateList = attr.cscfga__Value__c.split('-');
                        attrObj.sessionStartDate = startDateList[2]+'/'+startDateList[1]+'/'+startDateList[0];  //ECB-4253:Shreya- Updated date format to dd/mm/yyyy
                    } 
                    else
                    {
                        attrObj.sessionStartDate = todayDate;
                    }    
                }
                else if(attr.Name==ConfigDataMapController.getCustomSettingValue('SessionEndDate'))
                { //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                    attrObj.sessionEndDate = attr.cscfga__Value__c;     
                }
                else if(attr.Name==ConfigDataMapController.getCustomSettingValue('CredentialType'))
                { //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                    attrObj.courseType = attr.cscfga__Value__c; 
                }
                else if(attr.Name==ConfigDataMapController.getCustomSettingValue('gstApplicable'))
                {
                    attrObj.gstApplicable = attr.cscfga__Value__c; 
                   
                
                }
            }
        }    
        system.debug('attrObj::::'+attrObj);
        
        courseWrapperList.add(attrObj);
        
        return attrObj;
   }
   
   //Wrapper Class for Course List
   public class CourseDetailWrapper{
        
        public String courseName {get;set;}
        public String fees {get;set;}
        public String sessionStartDate {get;set;}
        public String sessionEndDate {get;set;}
        public String courseType {get;set;}   
        public String gstApplicable {get;set;} 
        public Decimal gstAmount{get;set;}  
        public Decimal feesWithoutGST{get;set;}
        public Decimal feesInDecimal{get;set;} 
   }
   
}