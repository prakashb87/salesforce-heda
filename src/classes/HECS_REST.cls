/*********************************************************************************
*** @ClassName         : HECS_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 14/08/2018
*** @Modified by       : Shubham Singh 
*** @modified date     : 14/08/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/HECS')
global with sharing class HECS_REST {
    
    public String HecsExemptStatusCode;
    public String effectiveDate;
    public String effectiveStatus;
    public String Description;
    public String ShortDescription;
    public String HecsCodeType;
    public String LoanType;
    public String CommonwealthAssisted;
    public String LongDescription;
    
    @HttpPost
    global static Map < String, String > upsertHECS() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Hecs
        list < HECS__c > UpsertHECS = new list < HECS__c > ();
        //HECS
        HECS__c hecs;
        
        //to check the effective date
        map < String,Date > updateEffectiveDate = new map < String,Date >();
        try {
            
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List < HECS_REST > postString = (List < HECS_REST > ) System.JSON.deserialize(reqStr, List < HECS_REST > .class);
            //getting hecs code
            list < String > updateHecsCode = new list < String > ();
            
            if (postString != null) {
                for (HECS_REST hecsrest: postString) {
                    if (hecsrest.HecsExemptStatusCode != null && (hecsrest.HecsExemptStatusCode).length() > 0) {
                        updateHecsCode.add(hecsrest.HecsExemptStatusCode);
                        if(hecsrest.effectiveDate != null && (hecsrest.effectiveDate).length() > 0)
                        {
                            updateEffectiveDate.put(hecsrest.HecsExemptStatusCode,Date.ValueOf(hecsrest.effectiveDate));
                        }
                    } else {
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                    
                }
            }
            
            //getting the old HECS for update
            list < HECS__c > listHECS = new list < HECS__c > ();
            if (updateHecsCode.size() > 0 && updateHecsCode != null) {
                //listHECS = [select id, Name, Effective_Date__c, Effective_Status__c, Description__c, Short_Description__c, HECS_Code_Type__c, Loan_Type__c, Commonwealth_assisted_flag__c, Long_Description__c from HECS__c where Name IN: updateHecsCode and Effective_Date__c IN: effectiveDate];
                listHECS = [select id, Name, Effective_Date__c, Effective_Status__c, Description__c, Short_Description__c, HECS_Code_Type__c, Loan_Type__c, Commonwealth_assisted_flag__c, Long_Description__c from HECS__c where Name IN: updateHecsCode];

            }
            
            //map to update the old HECS
            map < String, HECS__c > oldHECS = new map < String, HECS__c > ();
            for (HECS__c hec : listHECS) {
                String getEffDate = hec.Name+'';
                if(updateEffectiveDate != null && updateEffectiveDate.get(getEffDate) != null ){
                    if(updateEffectiveDate.get(getEffDate) <= System.today() && hec.Effective_Date__c <= updateEffectiveDate.get(getEffDate)){
                        oldHECS.put(hec.Name, hec);
                    }else if(hec.Effective_Date__c > updateEffectiveDate.get(getEffDate)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }   
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend; 
                }
            }
            
            //insert or update the HECS
            for (HECS_REST hecsRest: postString) {
                
                hecs = new HECS__c();
                hecs.Name = hecsRest.HecsExemptStatusCode;
                hecs.Effective_Date__c = hecsRest.effectiveDate != null ? Date.valueOf(hecsRest.effectiveDate) : null;
                hecs.Effective_Status__c = hecsRest.effectiveStatus != null && hecsRest.effectiveStatus.length() >0 ? hecsRest.effectiveStatus == 'A' ? 'Active' : 'Inactive' : null;
                hecs.Description__c = hecsRest.Description;
                hecs.Short_Description__c = hecsRest.ShortDescription;
                hecs.HECS_Code_Type__c = hecsRest.HecsCodeType;
                hecs.Loan_Type__c = hecsRest.LoanType;
                hecs.Commonwealth_assisted_flag__c = hecsRest.CommonwealthAssisted != null && hecsRest.CommonwealthAssisted.length() >0 ? hecsRest.CommonwealthAssisted == 'Y' ? True : False : null;
                hecs.Long_Description__c = hecsRest.LongDescription;
                //assigning the fund source id for update criteria : effective date should be less than or equal to previous one
                if (oldHECS.size() > 0 && oldHECS.get(hecsRest.HecsExemptStatusCode) != null) 
                {
                    if(hecsRest.effectiveDate != null && (oldHECS.get(hecsRest.HecsExemptStatusCode)).Effective_Date__c <= hecs.Effective_Date__c )
		    {
		    	hecs.ID = oldHECS.get(hecsRest.HecsExemptStatusCode).ID;
		    }
		    else
		    {
		    	hecs = null;
		    }
                }
                if(hecs !=null)
                {
                    UpsertHECS.add(hecs);
                }
            }
            //upsert hecs
            if (UpsertHECS != null && UpsertHECS.size() > 0 && Date.ValueOf(postString[0].effectiveDate) <= System.today()) {
                upsert UpsertHECS Name;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Student was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}