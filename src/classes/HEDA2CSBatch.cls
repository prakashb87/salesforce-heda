/*****************************************************************************************************
 *** @Author            : Ming Ma
 *** @Purpose 	        : Batch class to sync HEDA courses info into price items in Cloud Sense
 *** @Created date      : 28/06/2018
 *** @JIRA ID           : TBA (Fixes to ECB-168,246,2968)
 *****************************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class HEDA2CSBatch implements Database.Batchable<sObject> {

	private String query;

	Id recordTypeId21CC = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
	Id recordTypeIdRMITOnline = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId();
	Id recordTypeIdRMITTraining = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('RMIT Training').getRecordTypeId();

	global HEDA2CSBatch() {

		query = 'SELECT Id, name, hed__course__c, ' +
		        'hed__Start_Date__c, hed__End_Date__c,' +
		        'hed__course__r.name, ' +
		        'hed__course__r.price__c, ' +
		        'hed__course__r.hed__Account__c, ' +
		        'status__c ' +
		        'FROM hed__Course_Offering__c ' +
		        'WHERE hed__course__r.recordTypeId = \'' + recordTypeId21CC + '\' ' +
		        'OR hed__course__r.recordTypeId = \'' + recordTypeIdRMITOnline + '\''+
		        'OR hed__course__r.recordTypeId = \'' + recordTypeIdRMITTraining + '\'';
		//'WHERE hed__course__r.recordTypeId = \'' + recordTypeId21CC + '\'';


	}

	global Database.QueryLocator start(Database.BatchableContext bc) {
		system.debug('HEDA2CSBatch.start entered');
		system.debug('HEDA2CSBatch.start query ' + query);
		system.debug(Database.getQueryLocator(query));
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		//System.debug('HEDA2CSBatch scope' + scope);
		system.debug('HEDA2CSBatch.execute entered');

		HEDA2CSUtil.transformCourses2PriceItems(scope);
	}

	global void finish(Database.BatchableContext bc) {
		system.debug('HEDA2CSBatch.finish entered');

	}

}