public without sharing class HEDA2CSBatchScheduler implements Schedulable {
	public void execute(SchedulableContext sc) {
		HEDA2CSBatch batchJob = new HEDA2CSBatch();
		Id batchId = Database.executeBatch(batchJob, 30);
	}
}