/*****************************************************************************************************
 *** @Author            : Ming Ma
 *** @Purpose 	        : Test Class
 *** @Created date      : 28/06/2018
 *** @JIRA ID           : TBA (Fixes to ECB-168,246,2968)
 *****************************************************************************************************/

@isTest
private class HEDA2CSBatchTest {

	@isTest
	static void testHEDA2CSBatch() {
		System.debug('*** HEDA2CSBatchTest.test_HEDA2CSBatch');

		// Create test data
		Id courseRecordTypeId21CC = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
		Id courseRecordTypeIdRMITOnline = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId();

		system.debug('21CC recordTypeId ' + courseRecordTypeId21CC);
		
		Account department = New Account(name = 'RMITU');
		Insert department;

		hed__Term__c term = New hed__Term__c();
		term.hed__Account__c = department.Id;
		Insert term;

		hed__course__c course = New hed__course__c();
		course.Name = 'Test Course';
		course.hed__Account__c = department.Id;
		course.recordTypeId = courseRecordTypeId21CC;
		Insert course;

		system.debug('**** debug course creation ' + course);

		hed__Course_Offering__c courseOffering1 = New hed__Course_Offering__c();
		courseOffering1.name = 'Course Offering ID 1';
		courseOffering1.hed__course__c = course.Id;
		courseOffering1.hed__course__r = course;
		courseOffering1.Status__c = 'Active';
		courseOffering1.hed__Term__c = term.Id;
		courseOffering1.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering1.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering1;

		hed__Course_Offering__c courseOffering2 = New hed__Course_Offering__c();
		courseOffering2.name = 'Course Offering ID 2';
		courseOffering2.hed__course__c = course.Id;
		courseOffering2.Status__c = 'Inactive';
		courseOffering2.hed__Term__c = term.Id;
		courseOffering2.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering2.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering2;

		hed__Course_Offering__c courseOffering3 = New hed__Course_Offering__c();
		courseOffering3.name = 'Course Offering ID 3';
		courseOffering3.hed__course__c = course.Id;
		courseOffering3.Status__c = 'Active';
		courseOffering3.hed__Term__c = term.Id;
		courseOffering3.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering3.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering3;

		//Test Data for RMIT Online Course
		hed__course__c course2 = New hed__course__c();
		course2.Name = 'Test Course2 - RMIT Online';
		course2.hed__Account__c = department.Id;
		course2.recordTypeId = courseRecordTypeIdRMITOnline;
		Insert course2;

		system.debug('**** debug course2 creation ' + course2);

		hed__Course_Offering__c courseOfferingRMITOnline = New hed__Course_Offering__c();
		courseOfferingRMITOnline.name = 'Course Offering ID 1';
		courseOfferingRMITOnline.hed__course__c = course2.Id;
		courseOfferingRMITOnline.hed__course__r = course2;
		courseOfferingRMITOnline.Status__c = 'Active';
		courseOfferingRMITOnline.hed__Term__c = term.Id;
		courseOfferingRMITOnline.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOfferingRMITOnline.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOfferingRMITOnline;

		list<hed__Course_Offering__c> testDataFromDB = [SELECT Id, name, hed__course__c,
		                              hed__Start_Date__c,
		                              hed__End_Date__c,
		                              hed__course__r.name,
		                              //hed__course__r.price__c,
		                              hed__course__r.recordTypeId,
		                              status__c 
		                              FROM hed__Course_Offering__c];


		System.debug('*** HEDA2CSBatchTest testDataFromDB.size: ' +  testDataFromDB.size());
		System.debug('*** HEDA2CSBatchTest testDataFromDB: ' +  testDataFromDB);

		Test.startTest();

		HEDA2CSBatch batchJob = new HEDA2CSBatch();
		Id batchId = Database.executeBatch(batchJob, 10);




		Test.stopTest();


		List<cspmb__Price_Item__c> priceItems = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c,
		                           External_Unique_ID__c from cspmb__Price_Item__c];




		//We should only have 3 records after two upsert.
		System.assertEquals(4, priceItems.size());


	}


}