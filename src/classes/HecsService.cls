/*********************************************************************************
 *** @ClassName         : HecsService
 *** @Author            : Shubham Singh 
 *** @Requirement       : HecsService class for methods related to web service version 1.1
 *** @Created date      : 17/03/2019
 *** @Modified by       : Dinesh Kumar 
 *** @modified date     : 02/04/2019
 *** @Reference			: RM-2563 and RM-2580
 **********************************************************************************/
public with sharing class HecsService {
    //wrapper class to create variables to store the respective values     

    /* HecsServiceWrapper [Marcelo 1/4/2019]:
       No need for this Wrapper if it is Wrapping only one var.Keep it simple...
       There is also no reason for not storing the whole HECS__c as the object in the map. You will save AT LEAST 1 extra check if you keep the whole object.

    public class HecsServiceWrapper {
        public List<HECS__c> hecsRecords;
        public Map < String, Id > hecsNameIdMap = new Map < String, Id > ();
    }
  */


    /**
     * -----------------------------------------------------------------------------------------------+
     * Get HECS records and create Map
     * ------------------------------------------------------------------------------------------------
     * @author	  Dinesh Kuamr 
     * @method    getHecsbyNameMap
     * @param     Set < String > hecsExemptionStatusCode
     * @return    map
     * @Reference RM-2563
     * -----------------------------------------------------------------------------------------------+
     */

    //This method should return the map. There is no reason to create a Wrapper with only one parameter.
    public static Map < String, HECS__c > getHecsbyNameMap(Set < String > hecsExemptionStatusCode) {
        Map < String, HECS__c > hecsByName = new Map<String,HECS__c>();

        for (Hecs__c loopHecs: getHecsByName(hecsExemptionStatusCode)) {
            hecsByName.put(loopHecs.Name, loopHecs);
        }
        return hecsByName;
    }
    
    /**
     * -----------------------------------------------------------------------------------------------+
     * Query HECS record
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    getHecsByName
     * @param     Set < String > hecsExemptionStatusCode
     * @return    List
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
     public static List < Hecs__c > getHecsByName(Set < String > hecsExemptionStatusCode) {
       List <HECS__c> hecsList = new List<HECS__c>();
        if (Schema.sObjectType.HECS__c.fields.Name.isAccessible()) {
            hecsList= [SELECT Id, Name, Effective_Date__c FROM HECS__c WHERE name IN: hecsExemptionStatusCode Order by Effective_Date__c];
        }
        return hecsList;
    }

    //[Marcelo 1/4/2019]: This method is not required.
    /**
     * -----------------------------------------------------------------------------------------------+
     * create HECS mab by hecs name
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    getHecsMapByName
     * @param     List<HECS__c> hecsList
     * @return    Map<String,HECS__c> 
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+    
    public static Map < String, HECS__c > getHecsMapByName(List < HECS__c > hecsList) {
        Map < String, HECS__c > hecsByName = new Map < String, HECS__c > ();
        if (hecsList == null) {
            return hecsByName;
        }
        for (HECS__c hecs: hecsList) {
            hecsByName.put(hecs.Name, hecs);
        }
        return hecsByName;
    }
    */
}