/*****************************************************************************************************
 *** @Class             : IDAMResponseWrapper
 *** @Author            : Rishabh Anand
 *** @Requirement       : Response Wrapper Class For IDAM Integration 
 *** @Created date      : 12/09/2018
 *** @JIRA ID           : ECB-4352
 *** @LastModifiedBy	: Rishabh Anand
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to create a response structure to receive response from Ipass API. 
 *****************************************************************************************************/
public class IDAMResponseWrapper {
	
   	public String id;
	public String result;
	public String code;
	public String application;
	public String provider;
	public Payload payload;

	public class Payload {
		public Boolean success;
		public String message;
	}

	
	public static IDAMResponseWrapper parse(String json) {
		return (IDAMResponseWrapper) System.JSON.deserialize(json, IDAMResponseWrapper.class);
	}
}