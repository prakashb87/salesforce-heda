/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 17/09/2018     ****************************/ 
/***   @JIRA ID: ECB-4460                        ********************************************************/          
/*** This class is request wrapper class for creating user in IDAM                                  *****/ 
/********************************************************************************************************/
public class IDAMUserCreationRequestWrapper 
{
	public String firstname;
	public String lastname;
	public String email;
	public String mobile;
	
}