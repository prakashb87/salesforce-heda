/**
 * @description This interface defines methods to create a delete user 
 * in the identity provider
 * @group Identity Management
 */
// Global state is required as the page referencing this class
// will be inside an iframe
@SuppressWarnings('PMD.AvoidGlobalModifier')
global interface IdentityProviderUserDeleteRequest {
    
    /**
     * @description Deletes the user from the identity provider
     * @param email The email address of the user to be deleted
     */
    void deleteUser(String email);
}