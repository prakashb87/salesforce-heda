/**
 * @description This interface describes the methods avalable retrieving
 * the user's details from the Identity Provider
 * @group Identity Management
 */
// Global state is required as the page referencing this class
// will be inside an iframe
@SuppressWarnings('PMD.AvoidGlobalModifier')
global interface IdentityProviderUserStatusReq {

    /**
     * @description Set if integration logging should be enabled. This method is
     * used when logged explicitly required to be disabled, for example when 
     * there are multiple callouts and dml operations cannot be executed before 
     * all callouts have been completed
     * @param isLogging Set this to true if integration logs should be written 
     * to the database
     */
    void setIntegrationLogging(Boolean isLogging);

    /**
     * @description Returns if the user exists in the Identity Provider
     */
    boolean isExistingUser();


    /**
     * @description Returns if the user exists and is also verified in the
     * Identity Provider
     */
    boolean isExistingVerifiedUser();
}