/**
 * @description Exception for any problems with the object's state
 * @group Exceptions
 */
public class IllegalStateException extends Exception {}