/**
 * @description This class provides functionality to log integrations requests and responses.
 * Depending on the configuration, it allows for logging to the Salesforce Debug Log or 
 * optionally to the Integration_Logs__c object
 * @group Logging
 */
public without sharing class IntegrationLog {

    @testVisible private String requestBody;
    @testVisible private String responseBody;
    @testVisible private Boolean isException;
    @testVisible private String recordId;
    @testVisible private String type;
    @testVisible private DateTime logCreatedTime;
    @testVisible private String serviceType;

    /**
     * @description The constructor sets all instance variables to blank. This allows all
     * fields to be optional and prevents any null errors.
     */
    public IntegrationLog() {
        this.requestBody = '';
        this.responseBody = '';
        this.isException = false;
        this.type = '';
        this.serviceType = '';
        this.logCreatedTime = System.now(); 
    }


    /**
     * @description Sets the request body that was sent to an external system 
     * or into Salesforce
     * @param The request body in String format
     */
    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }


    /**
     * @description Sets the response body that was sent by an external system
     * or the response sent from Salesforce
     * @param The reponse body in String format
     */
    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }


    /**
     * @description Set the exception status of the log
     * @param isException Set this value to true if this log occurs during an exception
     */
    public void setIsException(boolean isException) {
        this.isException = isException;
    }


    /**
     * @description Sets the record Id that the integration record is related to
     * @param Record Id in String format
     */
    public void setRecordId(String recordId) {
        // Validate the length of record Id to ensure this field can
        // be stored in the Integration_Log__c object (field has max length 100)
        if(String.isNotBlank(recordId) && recordId.length() > 100) {
            throw new ArgumentException('Invalid record Id parameter value.');
        }
        this.recordId = recordId;
    }

    /**
     * @description Sets the record Id that the integration record is related to
     * @param Record Id in String format
     */
    public void setRecordId(Id recordId) {
        this.recordId = (String)recordId;
    }


    /**
     * @description This field stores the type of error. 
     * @param Type of error in String format
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }


    /**
     * @description This field stores the type of error. 
     * @param Type of error in String format
     */
    public void setType(String type) {
        this.type = type;
    }


    /**
     * @description Logs the instance variables set to the Salesforce debug log and
     * depending on system configuration, to the Integration_Logs__c object
     */
    public void log() {
        logToDebugLog();
        String logSetting = ConfigDataMapController.getCustomSettingValue('LogIntegrationRequestToIntegrationLog');
        if (String.isNotBlank(logSetting) && logSetting == 'true') {
            // If both request and response have been set then both can be logged using a single dml
            if (String.isNotBlank(this.requestBody) && String.isNotBlank(this.responseBody)) {
                logRequestResponseToIntObject();
            } else {
                logToIntegrationObject();
            }
        }
    }

    /**
     * @description Logs the instance variables set to the Salesforce debug log
     */
    public void logToDebugLog() {
        System.debug(LoggingLevel.FINER, '====== START INTEGRATION LOG =======');
        System.debug(LoggingLevel.FINER, 'Request Body: ' + this.requestBody);
        System.debug(LoggingLevel.FINER, 'Response Body: ' + this.responseBody);
        System.debug(LoggingLevel.FINER, 'ServiceType: ' + this.serviceType);
        System.debug(LoggingLevel.FINER, 'type: ' + this.type);
        System.debug(LoggingLevel.FINER, 'recordId: ' + this.recordId);
        System.debug(LoggingLevel.FINER, 'isException: ' + this.isException);
        System.debug(LoggingLevel.FINER, 'logCreatedTime: ' + this.logCreatedTime);
        System.debug(LoggingLevel.FINER, '====== END INTEGRATION LOG =======');
    }


    @testVisible
    private void logToIntegrationObject() {
        if (Schema.sObjectType.Integration_Logs__c.isCreateable()) {
            Integration_Logs__c logRecord = new Integration_Logs__c();
            logRecord.Type__c = this.type;
            /* 
            The Integration_Logs__c object captures both the request and response in a 
            single field and is used. It was built so that one record is created for the request 
            and another record is created for the response.
            To allow for the future possibility of capturing both records in a single record
            this class accepts both request and response in a single Integration_Logs__c record to
            save data storage or to consolidate requests and response for other purposes...
            */ 
            logRecord.Request_Response__c = requestBody + responseBody;
            logRecord.Service_Type__c = this.serviceType;
            logRecord.SObject_ID__c = this.recordId;
            logRecord.Service_Type__c = this.serviceType;
            logRecord.Is_Exception__c = this.isException;
            logRecord.Sent_Received_Date_Time__c = logCreatedTime;
            insert logRecord;
        }
    }


    @testVisible
    private void logRequestResponseToIntObject() {
        if (Schema.sObjectType.Integration_Logs__c.isCreateable()) {
            /* 
            The Integration_Logs__c object captures both the request and response in a 
            single field and is used. It was built so that one record is created for the request 
            and another record is created for the response.
            */ 
            List<Integration_Logs__c> logs = new List<Integration_Logs__c>();

            Integration_Logs__c requestRecord = new Integration_Logs__c();
            requestRecord.Type__c = this.type;
            requestRecord.Request_Response__c = this.requestBody;
            requestRecord.Service_Type__c = 'Outbound Service';
            requestRecord.SObject_ID__c = this.recordId;
            requestRecord.Service_Type__c = this.serviceType;
            requestRecord.Is_Exception__c = this.isException;
            requestRecord.Sent_Received_Date_Time__c = logCreatedTime;
            logs.add(requestRecord);

            Integration_Logs__c responseRecord = new Integration_Logs__c();
            responseRecord.Type__c = this.type;
            responseRecord.Request_Response__c = this.responseBody;
            responseRecord.Service_Type__c = 'Acknowledgement';
            responseRecord.SObject_ID__c = this.recordId;
            responseRecord.Service_Type__c = this.serviceType;
            responseRecord.Is_Exception__c = this.isException;
            responseRecord.Sent_Received_Date_Time__c = logCreatedTime;
            logs.add(responseRecord);

            insert logs;
        }
    }
}