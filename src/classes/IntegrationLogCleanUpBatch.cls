/*****************************************************************************************************
 *** @Class             : IntegrationLogCleanUpBatch
 *** @Author            : Praneet Vig
 *** @Requirement       : Batch to Clean Integration Log records
 *** @Created date      : 07/06/2018
 *** @JIRA ID           : ECB-3768
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is a batch Class used batch to clean Integration log records.
 *****************************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class IntegrationLogCleanUpBatch implements Database.Batchable < sObject > {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //configurable value taken from custom settings
        list<Config_Data_Map__c> valuelst=new list<Config_Data_Map__c>();
         valuelst=  [ select Config_Value__c from Config_Data_Map__c where name='IntegrationLogTimeline' Limit 1];
        Config_Data_Map__c value =valuelst.size() >0 ? valuelst[0] :new Config_Data_Map__c();
        string query= 'SELECT Id from Integration_Logs__c limit 0' ;
		 if(valuelst.size()>0){             
           Date date2 = system.today();
           Date date1 = System.Today() - Integer.valueOf(value.Config_Value__c);
           query= 'SELECT Id from Integration_Logs__c WHERE DAY_ONLY(CreatedDate) <=:' + 'date1';      
         }
       return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List < Integration_Logs__c > scope) {
        try {
            delete scope;
            system.debug('log deleted successfully');
        } catch (Exception e) {
            system.debug('log not deleted');
        }
    }
    global void finish(Database.BatchableContext bc) {
        system.debug('Batch Job Executed');
    }

}