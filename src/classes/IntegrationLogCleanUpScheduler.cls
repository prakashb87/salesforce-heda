/*****************************************************************************************************
 *** @Class             : IntegrationLogCleanUpScheduler
 *** @Author            : Praneet Vig
 *** @Requirement       : scheduler to Clean Integration Log records
 *** @Created date      : 07/06/2018
 *** @JIRA ID           : ECB-3768
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to schedule batch to clean Integration log records.
 *****************************************************************************************************/ 
//Schedule class to run IntegrationLogCleanUpBatch
public without sharing class IntegrationLogCleanUpScheduler Implements Schedulable{
  
    public void execute(SchedulableContext sc){
        
        IntegrationLogCleanUpBatch bc= new IntegrationLogCleanUpBatch();
        ID batchprocessid = Database.executeBatch(bc);
        
    }
}