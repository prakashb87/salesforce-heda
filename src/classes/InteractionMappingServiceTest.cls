/*****************************************
* File: INT_InteractionMappingService_TEST
* Author: Sierra-Cedar
* Description: Unit Tests for
******************************************/
@IsTest
private class InteractionMappingServiceTest {
 @testSetup static void setUpTest() {
        // Create and insert Interaction Mapping Test Data
        List<Interaction_Mapping__c> testMappings = InteractionTestDataFactory.createInteractionmappingsTriggerHandlers();
         List<Interaction_Mapping__c> tmp= new  List<Interaction_Mapping__c>();
        Interaction_Mapping__c interactionFirstNameFromLeadMapping = new Interaction_Mapping__c(
            Target_Object_API_Name__c = 'Interaction__c',
            Source_Object_API_Name__c = 'Lead',
            Source_Field_API_Name__c = 'FirstName',
            Target_Field_API_Name__c = 'First_Name__c',
            Active__c = true
        );
        tmp.add(interactionFirstNameFromLeadMapping);

        Interaction_Mapping__c interactionLastNameFromLeadMapping = new Interaction_Mapping__c(
            Target_Object_API_Name__c = 'Interaction__c',
            Source_Object_API_Name__c = 'Lead',
            Source_Field_API_Name__c = 'LastName',
            Target_Field_API_Name__c = 'Last_Name__c',
            Active__c = true
        );
        tmp.add(interactionLastNameFromLeadMapping);
        insert tmp;
    }

    static testMethod void testCreateContact() {
        // Arrange
        Interaction__c testInteraction = new Interaction__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Contact',
            Interaction_Status__c = 'New'
        );

        Contact c = new Contact(
            FirstName = 'Something',
            LastName = 'Else'
        );

        // Act
        Test.startTest();
        InteractionMappingService interactionMapping = new InteractionMappingService();
        interactionMapping.applyDataToSObject(testInteraction, c);
        Test.stopTest();

        // Assert
        System.assert(c.FirstName == 'Test');
        System.assert(c.LastName == 'Contact');
    }

    static testMethod void testCreateContactExcludedSources() {
        // Arrange
        Interaction__c testInteraction = new Interaction__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Contact',
            Interaction_Status__c = 'New',
            Interaction_Source__c = 'Webform'
        );

        Contact c = new Contact(
            FirstName = 'Something',
            LastName = 'Else'
        );

        // Act
        Test.startTest();
        InteractionMappingService interactionMapping = new InteractionMappingService();
        interactionMapping.applyDataToSObject(testInteraction, c);
        Test.stopTest();

        // Assert
        // Interaction data should NOT be copied over since we exclude Webform Sources
       	System.assert(c.FirstName == 'Test');
       	System.assert(c.LastName == 'Contact');
    }   
    static testMethod void testMapFromLeadSources() {
        // Arrange
        Lead testLead = new Lead(
            FirstName = 'Test',
            LastName = 'Lead',
            Email = 'test@nomail.com',
            Company = 'Lead, Test'
        );

        Interaction__c testInteraction = new Interaction__c(
            First_Name__c = '',
            Last_Name__c = 'Lead',
            Interaction_Status__c = 'New',
            Interaction_Source__c = 'Webform'
        );

        // Act
        Test.startTest();
        InteractionMappingService interactionMapping = new InteractionMappingService();
        interactionMapping.applyDataToInteraction(testLead, testInteraction);
        Test.stopTest();
        // Assert
        System.assertEquals('Test', testInteraction.First_Name__c, 'The first name on Lead should have been copied to the Interaction as per the mappings.');
    }
}