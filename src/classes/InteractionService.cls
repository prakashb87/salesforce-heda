public with sharing class InteractionService {
    
    private static LeadStatus convertStatus {
        get {
            if (convertStatus == null) {
                convertStatus = [SELECT Id, Masterlabel FROM LeadStatus WHERE IsConverted = true LIMIT 1];
            }
            return convertStatus;
        }
        set;
    }

    /*******************************************************************************************************************************
     * @description Debugs SaveResult errors if they happen.
     * @param saveResults, the Database.SaveResult List to check.
     ********************************************************************************************************************************/
    public static void logPossibleErrors(Database.SaveResult[] saveResults) {
        for (Database.SaveResult result: saveResults) {
            if (!result.isSuccess()) {
                System.debug('Save Result Error: ' + result.getErrors() + ' ' + result.getId());
            }
        }
    }

    /**********************************************************************************************************************************
     * @description Debugs UpsertResult errors if they happen.
     * @param saveResults, the Database.UpsertResult List to check.
     **********************************************************************************************************************************/
    public static void logPossibleErrors(Database.UpsertResult[] saveResults) {
            for (Database.UpsertResult result: saveResults) {
                if (!result.isSuccess()) {
                    System.debug('Upsert Error: ' + result.getErrors() + ' ' + result.getId());
                }
            }
        }
        /*******************************************************************************************************************************
         * @description setthe primaryCmapign Id when interaction created against leads
         * @param interactions , list of interaction before insert
         ********************************************************************************************************************************/
    public static void setPrimaryCampaignID(list < Interaction__c > interactions) {
            map < Id, Interaction__c > mapLeadInteraction = new map < Id, Interaction__c > ();
            for (interaction__c interaction: interactions) {
                if (String.isNotBlank(interaction.Lead__c)) {
                    mapLeadInteraction.put(interaction.Lead__c, interaction);
                }

            }
            CampaignMember selectedCampaignMember;
            for (CampaignMember cm: [SELECT leadId, CampaignId, Status, CreatedDate
                    FROM CampaignMember
                    WHERE leadId IN: mapLeadInteraction.keySet()
                    ORDER BY LeadId
                ]) {
                CampaignMember tempCM = cm;
                if (selectedCampaignMember == null || selectedCampaignMember.createdDate > tempCM.createdDate) {
                    selectedCampaignMember = new CampaignMember();
                    selectedCampaignMember = cm;
                }
                Interaction__c interaction = mapLeadInteraction.get(selectedCampaignMember.LeadId);
                interaction.Primary_Campaign__c = selectedCampaignMember.CampaignId;

            }
        }
        /*******************************************************************************************************************************
         * @description Linked created prospect affiliation to the created opportunity
         * @param  interactions , list of interaction before update
         ********************************************************************************************************************************/
    public static void updateOpportunityAffiliation(list < Interaction__c > interactions) {
            map < Id, Interaction__c > mapOppInteraction = new map < Id, Interaction__c > ();
            map < String, id > mapOppUpsertKey = new Map < String, Id > ();
            map < Id, Id > mapOppAff = new map < id, Id > ();
            list < Opportunity > oppToUpdate = new list < Opportunity > ();
            string affilitionKey = '';
            for (interaction__c interaction: interactions) {
                if (String.isNotBlank(interaction.Opportunity__c)) {
                    mapOppInteraction.put(interaction.Opportunity__c, interaction);
                    mapOppUpsertKey.put(interaction.Affiliation_Key__c, interaction.Opportunity__c);
                }
            }
            for (hed__Affiliation__c affiliation: [SELECT Id, upsertKey__c
                    FROM hed__Affiliation__c
                    WHERE upsertKey__c IN: mapOppUpsertKey.keySet()
                ]) {
                mapOppAff.put(mapOppUpsertKey.get(affiliation.upsertKey__c), affiliation.Id);


            }
            for (Opportunity opp: [SELECT Id, Affiliation__c
                    FROM Opportunity
                    WHERE ID IN: mapOppInteraction.keySet()
                ]) {
                if (!mapOppAff.isEmpty() && mapOppAff.containsKey(opp.Id)) {
                    opp.Affiliation__c = mapOppAff.get(opp.Id);
                    oppToUpdate.add(opp);
                }

            }
            if (!oppToUpdate.isEmpty()) {
                Database.update(oppToUpdate, true);
            }
        }
    
    public static void processAllOtherLeads(map<id,list<Lead>> leadForReconversion, List<Interaction__c> interactionToUpdate  ){
         List < Database.LeadConvert > allLeadForConversion = new  List < Database.LeadConvert >();
        if (!leadForReconversion.isEmpty()) {
            map < Id, Interaction__c > mapInteraction = new map < Id, Interaction__c > (interactionToUpdate);

            for (interaction__c intRec: [SELECT Lead__c, Contact__c, Contact_Account_ID__c FROM interaction__c WHERE ID IN: mapInteraction.keySet()]) {
                intRec.recalculateFormulas();
                if (leadForReconversion.containskey(intRec.Lead__c)) {
                    allLeadForConversion.addAll(processLeadReconversion(leadForReconversion.get(intRec.Lead__c), intRec));
                }
            }

            if (!allLeadForConversion.isEmpty()) {
                Database.LeadConvertResult[] leadConvertResults = Database.convertLead(allLeadForConversion, true);
                System.debug('****************** leadConvertResults:' + JSON.serializePretty(leadConvertResults));

            }

        }
        
    }
     /**
     * @description Creates a Database.LeadConvert Object from supplied Interaction record.
     * @param interaction, the Interaction__c to create Database.LeadConvert from.
     * @param leadId, the Id of the Lead to convert.
     * @param createOppty, whether or not to create an Opportunity from the Database.LeadConvert.
     */
    private static Database.LeadConvert createLeadConvert(Id leadId, Interaction__c intRec) {
        // Prepare to convert matching lead

        System.debug('-------->' + intRec);
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadId);
        lc.setOwnerId(UserInfo.getUserId());
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setContactId(intRec.Contact__c);
        lc.setAccountId(intRec.Contact_Account_ID__c);
        lc.setDoNotCreateOpportunity(true);
        return lc;
    }

    private static List < Database.LeadConvert > processLeadReconversion(list < Lead > leads, interaction__c intRec) {
        List < Database.LeadConvert > result = new List < Database.LeadConvert > ();
        for (Lead l: leads) {
            result.add(createLeadConvert(l.id, intRec));
        }
        return result;
    }
    public static  map<id, hed__Affiliation__c> mappedContactAffiliation(map<Id, Contact> mapContact){
		Id affStudentRecordtypeID = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
		 map<id, hed__Affiliation__c> mapResult = new map<id, hed__Affiliation__c>();
		for(hed__Affiliation__c aff:[SELECT hed__Contact__c 
											FROM hed__Affiliation__c 
											WHERE hed__Contact__c IN: mapContact.keySet()
											AND RecordTypeId=:affStudentRecordtypeID]){
			mapResult.put(aff.hed__Contact__c, aff);
		}
		return mapResult;
	}
}