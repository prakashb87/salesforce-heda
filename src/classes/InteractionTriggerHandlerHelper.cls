public without sharing class InteractionTriggerHandlerHelper {
    private List < Opportunity > opportunitiesToUpsert = new List < Opportunity > ();
    private list < Contact > contactsToUpdate = new list < Contact > ();
    private list < hed__Affiliation__c > affiliationsToUpsert = new list < hed__Affiliation__c > ();
    private InteractionMappingService intMappingService {
        get {
            if (intMappingService == null) {
                intMappingService = new InteractionMappingService();
            }
            return intMappingService;
        }
        set;
    }
   
    public map < Id, Interaction__c > processInteractionOnly(List < Interaction__c > interactionToProcess) {
            Id leadRecordtypeID = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Prospective Student').getRecordTypeId();
            Map < Id, Lead > interactionIdToLead = new Map < Id, Lead > ();
            map < Id, Interaction__c > mapResults = new map < Id, Interaction__c > ();
            for (Interaction__c interaction: interactionToProcess) {
                Lead newLead = new Lead(
                    FirstName = interaction.First_Name__c,
                    LastName = interaction.Last_Name__c,
                    MobilePhone = interaction.Mobile_Phone__c,
                    Email = interaction.Email__c,
                    Company = interaction.Last_Name__c + ', ' + interaction.First_Name__c,
                    recordTypeId = leadRecordtypeID);
                intMappingService.applyDataToSObject(interaction, newLead);
                interactionIdToLead.put(interaction.Id, newLead);
            }
            if (!interactionIdToLead.isEmpty()) {
                mapResults = processInteractionToLead(interactionIdToLead.values(), interactionToProcess);
            }
            return mapResults;
        }
     public List < Interaction__c > reprocessInteraction(map < Interaction__c, Database.LeadConvertResult > mapInteractionLCR, map<Id,hed__Affiliation__c> mapConAff) {
        Id oppRecordtypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Inquiry Opportunities').getRecordTypeId();
        list < Interaction__c > interactionToUpdate = new list < Interaction__c > ();
        map<ID,hed__Program_Enrollment__c> mapConPE= new map<ID,hed__Program_Enrollment__c>();
        hed__Program_Enrollment__c programEnrol;
        Database.LeadConvertResult lcr;
        //set<ID> contactIds=fetchContactIds(mapInteractionLCR.values());
        mapConPE=programEnrollments(fetchContactIds(mapInteractionLCR.values()));
        for (Interaction__c interaction: mapInteractionLCR.keySet()) {
            programEnrol=null;
            lcr = mapInteractionLCR.get(interaction);
          
            //updating contact records 
            if (String.isNotBlank(interaction.Contact__c) && !mapConAff.containskey(interaction.Contact__c)) {
                Contact newCont = new Contact(Id = interaction.Contact__c);
                intMappingService.applyDataToSObject(interaction, newCont);
                contactsToUpdate.add(newCont);
            }
            if(mapConPE.containsKey(lcr.getContactId())){
                programEnrol= mapConPE.get(lcr.getContactId());
            }
           
            affiliationsToUpsert.addAll(createUpsertAffilFromInteraction(interaction,programEnrol));
            
            // Create new Opportunity for upsert if Opportunity Key is populated.
            if (!String.isEmpty(interaction.Opportunity_Key__c) && (programEnrol==null || (programEnrol!=null && programEnrol.Admit_Term__c!=interaction.Admit_Term__c))) {
                Opportunity newOppty = new Opportunity(
                    Id = lcr.getOpportunityId(),
                    Name = interaction.Opportunity_Name__c,
                    Inquiry_Date__c = Date.valueOf(interaction.CreatedDate),
                    CloseDate = System.today() + 90,
                    StageName = 'Confirmed Interest',
                    Opportunity_Key__c = lcr.getContactId() + '.' + lcr.getAccountId() + '.'+ oppRecordtypeID +'.'+ interaction.Admit_Term__c,
                    AccountId = lcr.getAccountId(),
                    RecordTypeId = oppRecordtypeID,
                    Assign_To_Contact__c = lcr.getContactId());
                intMappingService.applyDataToSObject(interaction, newOppty);
                interaction.Opportunity__r = newOppty;
                opportunitiesToUpsert.add(newOppty);
            }
            interactionToUpdate.add(interaction);
        }
        insertUpdateAllInteractionRecords();
        return interactionToUpdate;
    }
        /*****************************************************************************************************************************
         * @description Create new Lead record, if there is duplicate get the firs lead and use it for conversion
         * @param  interactionLead- List of Leads , mapInteractionOnly  list of interaction with no related leads
         ****************************************************************************************************************************/
    private map < Id, Interaction__c > processInteractionToLead(List < Lead > interactionLead, List < Interaction__c > mapInteractionOnly) {
        integer indx = 0;
        List < Database.SaveResult > saveResults = Database.insert(interactionLead, false);
        map < Id, Interaction__c > results = new map < Id, Interaction__c > ();
        for (Database.SaveResult sr: saveResults) {
            Interaction__c interaction = mapInteractionOnly[indx];
            if (sr.isSuccess()) {
                Id leadId = sr.getId();
                interaction.Lead__c = leadId;
                results.put(leadId, interaction);
            } else {
                for (Database.Error error: sr.getErrors()) {
                    if (error.getStatusCode() == StatusCode.DUPLICATES_DETECTED) {
                        Database.DuplicateError dupErrorError = (Database.DuplicateError) error;
                        for (Datacloud.MatchResult matchResult: dupErrorError.getDuplicateResult().getMatchResults()) {
                            Lead matchedLead;
                            for (Datacloud.MatchRecord match: matchResult.getMatchRecords()) {
                                matchedLead = (Lead) match.getRecord();
                                interaction.Lead__c = matchedLead.Id;
                                results.put(matchedLead.Id, interaction);
                                break;
                            }
                        }
                    }

                }

            }
            indx++;
        }
        return results;
    }
   
  
        /*****************************************************************************************************************************
         * @description Creates a new Affiliation record for upsert from the data on the Interaction__c. provided.
         * @param interaction, the Interaction__c record to copy data from.
         * @return newAffil, the Affiliation__c record for upsert.
         ****************************************************************************************************************************/
    private  list < hed__Affiliation__c > createUpsertAffilFromInteraction(Interaction__c interaction,hed__Program_Enrollment__c programEnrol) {
        Id affProspectRecordtypeID = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        Id affStudentRecordtypeID = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();

        List < hed__Affiliation__c > affiliations = new List < hed__Affiliation__c > ();
        if(programEnrol==null || (programEnrol!=null &&  interaction.Education_Institution__c != programEnrol.hed__Affiliation__r.hed__Account__c)){
            hed__Affiliation__c newAffil1 = new hed__Affiliation__c(RecordTypeId = affProspectRecordtypeID,
                hed__Role__c = 'Prospect',
                hed__Status__c = 'Current',
                hed__Account__c = interaction.Education_Institution__c,
                hed__Contact__c = interaction.Contact__c,
                UpsertKey__c = interaction.Contact__c + '.' + interaction.Education_Institution__c + '.' + affProspectRecordtypeID + '.Prospect');
            affiliations.add(newAffil1);
        }

        if (String.isNotBlank(interaction.Secondary_School__c)) {
            hed__Affiliation__c newAffil2 = new hed__Affiliation__c(RecordTypeId = affStudentRecordtypeID,
                hed__Role__c = 'Secondary Student',
                hed__Status__c = 'Current',
                hed__Account__c = interaction.Secondary_School__c,
                hed__Contact__c = interaction.Contact__c,
                UpsertKey__c = interaction.Contact__c + '.' + interaction.Secondary_School__c + '.' + affStudentRecordtypeID + '.Secondary Student');
            affiliations.add(newAffil2);
        }
       if (String.isNotBlank(interaction.AgentSponsor__c)) {
            hed__Affiliation__c newAffil3 = new hed__Affiliation__c(RecordTypeId = affProspectRecordtypeID,
                hed__Role__c = 'Agent',
                hed__Status__c = 'Current',
                hed__Account__c = interaction.AgentSponsor__c,
                hed__Contact__c = interaction.Contact__c,
                UpsertKey__c = interaction.Contact__c + '.' + interaction.AgentSponsor__c + '.' + affProspectRecordtypeID + '.Agent');
            affiliations.add(newAffil3);
        }

        return affiliations;

    }
     
   
    private void insertUpdateAllInteractionRecords() {
        List < Interaction__c > interactionFinalUpdate = new List < Interaction__c > ();
        // Update Contacts
        if (!contactsToUpdate.isEmpty()) {
            InteractionService.logPossibleErrors(Database.update(contactsToUpdate, true));
        }
        // Upsert Affiliations using the Upsert_Key__c
        if (!affiliationsToUpsert.isEmpty()) {
            InteractionService.logPossibleErrors(Database.upsert(affiliationsToUpsert, hed__Affiliation__c.UpsertKey__c, false));
        }
        if (!opportunitiesToUpsert.isEmpty()) {
            Database.upsert(opportunitiesToUpsert, Opportunity.Opportunity_Key__c, false);
        }
    }
    private  map<ID,hed__Program_Enrollment__c> programEnrollments(set<ID> contactIds){
         map<ID,hed__Program_Enrollment__c> mapConPE= new  map<Id,hed__Program_Enrollment__c>();
         for(hed__Program_Enrollment__c programEnrol:[SELECT hed__Contact__c, Id, 	
                                                            Admit_Term__c,  
                                                            Enrolment_Date__c,
                                                            hed__Affiliation__c,
                                                            hed__Affiliation__r.hed__Account__c
                                                            FROM hed__Program_Enrollment__c
                                                            WHERE hed__Contact__c IN: contactIds
                                                            AND Enrolment_Date__c!=NULL
                                                            AND Program_Status__c='AD']){
               mapConPE.put(programEnrol.hed__Contact__c,programEnrol);
        }
        return mapConPE;
     }
    private set<ID> fetchContactIds(list<Database.LeadConvertResult> leadConvertResult){
        set<ID> fetchIDs= new set<ID>();
        for(Database.LeadConvertResult lcr:leadConvertResult){
            fetchIDs.add(lcr.getContactId());
        }
        return fetchIDs;
    }
    
   
}