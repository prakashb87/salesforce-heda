/*****************************************************************************************************
*** @Class             : IpassDeleteAccountApiWrapper
*** @Author            : Pawan Srivastava
*** @Requirement       : Integration between Salesforce and IPaaS for sending email Id
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to create a response structure that is received from Ipaas
*****************************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class IpassDeleteAccountApiWrapper 
{

  public String id;
  public String result;
  public String code;
  public String application;
  public String provider;
  public ResponseInfo payload;
  
  public class ResponseInfo {
    public String success;
    public String message;
  }
  
}