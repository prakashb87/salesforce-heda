/*****************************************************************************************************
 *** @Class             : JsonCourseWrapper
 *** @Author            : Avinash Machineni
 *** @Requirement       : To pass the values to CourseConnectionDetailIntegration class
 *** @Created date      : 15/05/2018
 *** @JIRA ID           : ECB-2954
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to pass the values to CourseConnectionDetailIntegration Class
 *****************************************************************************************************/ 


public class JsonCourseWrapper {
      
        public String id;
        public String result;
        public String code;
        public String application;
        public String provider;
        public List<IpaasResponseWrapper> payload;

    public class IpaasResponseWrapper {
        public Id courseConnectionId;
        public String result;
        public String errorCode;
        public String errorText;
    }
    public static JsonCourseWrapper parse(String json) {
        return (JsonCourseWrapper ) System.JSON.deserialize(json, JsonCourseWrapper.class);
    }
}