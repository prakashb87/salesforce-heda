/**
 * Created by marcelowork on 2019-05-07.
 */

public with sharing class LeadService {

    public static final map < ID, Schema.RecordTypeInfo > RECORDTYPEINFOBYID = Lead.sObjectType.getDescribe().getRecordTypeInfosById();


    public static void setDeduplicationField(List<Lead> leadList) {
        for (Lead l : leadList) {

            String arecordType = RECORDTYPEINFOBYID.get(l.RecordTypeId).getDeveloperName();
            switch on arecordType {
                when 'Prospective_Student' {
                    l.Prospective_Student_RT__c = 'Prospective_Student';
                }
                when 'Prospective_Student_RMIT_Online' {
                    l.Prospective_Student_RMITO_RT__c = 'Prospective_Student_RMIT_Online';
                }
                when 'Marketing_Event_Inquiry' {
                    l.Marketing_Event_Enquiry_RT__c ='Marketing_Event_Inquiry';
                }
                when 'Activator_Registration' {
                    l.Activator_Registration_RT__c = 'Activator_Registration';
                }
            }
        }
    }
}
