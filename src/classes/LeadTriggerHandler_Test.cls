/**
* -------------------------------------------------------------------------------------------------+
* This test class validates the LeadTriggerHandler class
* 
* --------------------------------------------------------------------------------------------------
* @author         Anupam Singhal
* @version        0.2
* @created        2019-03-13
* @modified       2019-03-14
* @modified by    Anupam Singhal
* @Reference      RM-2436 
* -------------------------------------------------------------------------------------------------+
*/

@isTest
private class LeadTriggerHandler_Test {
    @testSetup
    static void createTriggerData(){
        Id rmitoLeadrt = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student - RMIT Online').RecordTypeId;
        Id prospectiveLeadrt = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
        Id studentCasert = Schema.SObjectType.Case.RecordTypeInfosByName.get('Student').RecordTypeId;
        Id departmentRMITOrt = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        Id academicProgramRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Program').RecordTypeId;
        //Uncomment the record type when deploying to SIT/UAT
        Id courseRMITORT = Schema.SObjectType.hed__Course__c.RecordTypeInfosByName.get('RMIT Online').RecordTypeId; 
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'RMIT Marketing User' LIMIT 1];
        System.debug('Profile Id RMIT M user'+profileId);
        
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true; 
        

        User usr = new User(LastName = 'Test RMITO',
                            FirstName='User',
                            Alias = 'TRMITOU',
                            Email = 'test.rmito@rmittest.com',
                            Username = 'test.rmito@rmittest.com.sandbox',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        List<Account> accList = new List<Account>();
        Account departmentRMITO = new Account(Name='RMIT Online',RecordTypeId=departmentRMITOrt);
        accList.add(departmentRMITO);
        //insert departmentRMITO;
        
        //RM-2654 Accredited Application
        Account academicProgram= new Account(Name='Program1 Test', RecordTypeId=academicProgramRT, AccountNumber='P1010');
        accList.add(academicProgram);
        //insert academicProgram;
        
        Account termAccount = new Account(Name='Used for Term session',RecordTypeId=departmentRMITOrt);
        accList.add(termAccount);
        
        insert accList;
        
        hed__Term__c termsession = new hed__Term__c(hed__Account__c = accList[2].id);
        insert termsession;
        //Use the one with record type when deploying to SIT
        hed__Course__c course12 = new hed__Course__c(Name='Test RMITO Course',hed__Account__c=accList[0].Id,hed__Course_ID__c='TEST101', RecordTypeId=courseRMITORT);
        //hed__Course__c course12 = new hed__Course__c(Name='Test RMITO Course',hed__Account__c=departmentRMITO.Id,hed__Course_ID__c='TEST101');
        insert course12;
        System.debug('course12 Product Line'+[Select Product_Line__c FROM hed__Course__c].Product_Line__c);
        
        Datetime dt = Datetime.now().addDays(2);
        Datetime dt1 = Datetime.now().addDays(10);
        
        hed__Course_Offering__c courseOffRec = new hed__Course_Offering__c();
        courseOffRec.hed__Course__c = course12.Id;
        courseOffRec.hed__Term__c = termsession.Id;           
        courseOffRec.hed__Start_Date__c = dt.date();
        courseOffRec.Last_Day_to_Add__c = dt1.date();
        insert courseOffRec;
        
        List<Lead> leadForPotentialDups = new List<Lead>();
        
        Lead enquiryWebform0 = new Lead(FirstName='TestUto',
                                       LastName = 'Pia0', 
                                       RecordTypeId = rmitoLeadrt,
                                       LeadSource = 'Webform',
                                       Interested_Course_ID__c = 'TEST101',
                                       Company = 'Test Incorporated0',
                                       Portfolio__c = 'Future Skills');
        leadForPotentialDups.add(enquiryWebform0);
        //insert enquiryWebform0;
        Lead enquiryWebform1 = new Lead(FirstName='TestUto',
                                       LastName = 'Pia1', 
                                       RecordTypeId = rmitoLeadrt,
                                       LeadSource = 'Webform',
                                       Interested_Course_ID__c = 'TEST101',
                                       Company = 'Test Incorporated1',
                                       Portfolio__c = 'Future Skills');
        leadForPotentialDups.add(enquiryWebform1);
        //insert enquiryWebform1;
        
        //RM-2564
          Lead enquiryWebform2 = new Lead(FirstName='TestUto',
                                       LastName = 'Pia2', 
                                       RecordTypeId = rmitoLeadrt,
                                       LeadSource = 'Webform',
                                       Program_Code__c= 'P1010',
                                       Company = 'Test Incorporated2',
                                       Portfolio__c = 'Future Degrees');
        leadForPotentialDups.add(enquiryWebform2);
        //insert enquiryWebform2;
        
        // Leads to test PotentialDuplicates Functionality
        Lead lead1 = new Lead(FirstName='PD',
                                       LastName = 'Test1', 
                                       RecordTypeId = rmitoLeadrt,
                                       LeadSource = 'Webform',
                                       Interested_Course_ID__c = 'TEST101',
                                       Company = 'Test Incorporated0',
                                       Portfolio__c = 'Future Skills',
                             MobilePhone = '1234512345',
                             Email = 'pdtest1@test.com');
        leadForPotentialDups.add(lead1);
         Lead lead3 = new Lead(FirstName='PD',
                                       LastName = 'Test2', 
                                       RecordTypeId = rmitoLeadrt,
                                       LeadSource = 'Webform',
                                       Interested_Course_ID__c = 'TEST102',
                                       Company = 'Test Incorporated0',
                                       Portfolio__c = 'Future Skills',
                             MobilePhone = '1234512222',
                             Email = 'pdtest2@test.com');
        leadForPotentialDups.add(lead3);        
        insert leadForPotentialDups;        
        
        List<Lead> leadForPotentialDups2 = new List<Lead>();
        
        Lead lead2 = new Lead(FirstName='PD',
                                       LastName = 'Test1', 
                                       RecordTypeId = rmitoLeadrt,
                                       LeadSource = 'Webform',
                                       Interested_Course_ID__c = 'TEST101',
                                       Company = 'Test Incorporated0',
                                       Portfolio__c = 'Future Skills',
                             MobilePhone = '1234512345',
                             Email = 'pdtest1@test.com');
        leadForPotentialDups2.add(lead2);
        Lead lead4 = new Lead(FirstName='PD',
                                       LastName = 'Test3', 
                                       RecordTypeId = rmitoLeadrt,
                                       LeadSource = 'Webform',
                                       Interested_Course_ID__c = 'TEST102',
                                       Company = 'Test Incorporated0',
                                       Portfolio__c = 'Future Skills',
                             MobilePhone = '1234512222',
                             Email = 'pdtest1@test.com');
        leadForPotentialDups2.add(lead4);
        insert leadForPotentialDups2;
        
        Lead leadUpdate = new Lead();
        leadUpdate.FirstName='PD';
        leadUpdate.LastName = 'Test3'; 
        leadUpdate.RecordTypeId = rmitoLeadrt;
        leadUpdate.LeadSource = 'Webform';
        leadUpdate.Interested_Course_ID__c = 'TEST102';
        leadUpdate.Company = 'Test Incorporated0';
        leadUpdate.Portfolio__c = 'Future Skills';
        leadUpdate.MobilePhone = '1234512222';
        leadUpdate.Email = 'pdtest1@test.com';       
        insert leadUpdate;
        leadUpdate.MobilePhone = '1234512345';
        database.update(leadUpdate,dml);
        leadUpdate.MobilePhone = '';
        leadUpdate.Email='';
        database.update(leadUpdate, dml);
        
        List<Lead> leadForadmitterm = new List<Lead>();
        //Leads for POtential Duplicates Ends here
        System.runAs(usr){
            //Lead leadPhoneUpdate = [select id, MObilePhone, Email from Lead where id =: leadUpdate.Id ];
            Lead levelofstudy = new Lead(FirstName='TestDefault',
                                         LastName = 'DefaultSegment', 
                                         RecordTypeId = prospectiveLeadrt,
                                         LeadSource = 'Email',
                                         Company = 'Test Incorporated',
                                         Level_of_study__c = 'Postgraduate by coursework',
                                         Portfolio__c = 'Future Skills');
            leadForadmitterm.add(levelofstudy);
            Lead levelofstudy1 = new Lead(FirstName='TestDefault',
                                         LastName = 'DefaultSegment1', 
                                         RecordTypeId = prospectiveLeadrt,
                                         LeadSource = 'Email',
                                         Company = 'Test Incorporated',
                                         Level_of_study__c = 'Bachelor degree',
                                         I_am_currently__c = 'Studying Year 10',
                                         Portfolio__c = 'Future Skills');
            leadForadmitterm.add(levelofstudy1);
            Lead levelofstudy2 = new Lead(FirstName='TestDefault',
                                         LastName = 'DefaultSegment1', 
                                         RecordTypeId = prospectiveLeadrt,
                                         LeadSource = 'Email',
                                         Company = 'Test Incorporated',
                                         Level_of_study__c = 'Bachelor degree',
                                         Start_Semester__c = 'Semester 1, 2019',
                                         Portfolio__c = 'Future Skills');
            leadForadmitterm.add(levelofstudy2);
            Lead levelofstudy3 = new Lead(FirstName='TestDefault',
                                         LastName = 'DefaultSegment1', 
                                         RecordTypeId = prospectiveLeadrt,
                                         LeadSource = 'Email',
                                         Company = 'Test Incorporated',
                                         Level_of_study__c = 'Bachelor degree',
                                         Start_Semester__c = 'Semester 2, 2019',
                                         Portfolio__c = 'Future Skills');
            leadForadmitterm.add(levelofstudy3);
            Lead levelofstudy4 = new Lead(FirstName='TestDefault',
                                         LastName = 'DefaultSegment1', 
                                         RecordTypeId = prospectiveLeadrt,
                                         LeadSource = 'Email',
                                         Company = 'Test Incorporated',
                                         Level_of_study__c = 'Bachelor degree',
                                         I_am_currently__c = 'Studying Year 10',
                                         Start_Semester__c = 'Semester 1, 2019',
                                         Portfolio__c = 'Future Skills');
            leadForadmitterm.add(levelofstudy4);
            insert leadForadmitterm;
        }
        Lead leadConvert = new Lead();
        leadConvert.FirstName='PD';
        leadConvert.LastName = 'Test3'; 
        leadConvert.RecordTypeId = rmitoLeadrt;
        leadConvert.LeadSource = 'Webform';
        leadConvert.Interested_Course_ID__c = 'TEST102';
        leadConvert.Company = 'Test Incorporated0';
        leadConvert.Portfolio__c = 'Future Skills';
        leadConvert.MobilePhone = '1234512222';
        leadConvert.Email = 'pdtest1@test.com';
        insert leadConvert;
        
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadConvert.Id);
        lc.setConvertedStatus('Qualified');
        lc.setDoNotCreateOpportunity(true);
        Database.convertLead(lc);
        
    }
    
    static testMethod void testpopulateCourse() {
        
        Id expected = [SELECT Id FROM hed__Course__c WHERE hed__Course_ID__c = 'TEST101'].Id;
        List <Lead> actual = [SELECT Course__c,Interested_Course_ID__c FROM Lead WHERE Interested_Course_ID__c = 'TEST101'];
        if(!actual.isEmpty()){
            System.assertEquals(expected,actual[0].Course__c);
            System.assertEquals(expected,actual[1].Course__c);
        }
    }
    //RM-2564
    static testMethod void testpopulateProgramNameLookup() {
    	 Id expected=[SELECT ID from Account WHERE AccountNumber='P1010'].Id;
     	 List <Lead> actual = [SELECT SAMS_Program_name__c FROM Lead WHERE Program_Code__c = 'P1010'];
     	 if(!actual.isEmpty()){
     	 	System.assertEquals(expected, actual[0].SAMS_Program_name__c);
     	 }
        LeadUpdateSegment leadSegment= new LeadUpdateSegment();
    
     }
 
}