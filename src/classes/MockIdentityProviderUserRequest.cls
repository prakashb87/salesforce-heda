public class MockIdentityProviderUserRequest implements IdentityProviderUserRequest {

    public void createUser() 
    {
        System.debug('Some user would be created here');
    }

    public String getFederationId()
    {
        return 's7347392';
    }
    public void setFirstName(String firstName)
    {
    	System.debug('Not required.');
    }
    public void setLastName(String lastName)
    {
    	System.debug('Not required.');
    }
    public void setEmail(String email)
    {
    	System.debug('Not required.');
    }
    public void setMobile(String mobile)
    {
    	System.debug('Not required');
    }
    public void setPassword(String password)
    {
    	System.debug('Not required');
    }


}