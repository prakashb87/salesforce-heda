/**
 * @description Mock class for deleting a user in the identity provider. This 
 * mock class is used for testing positive scenarios, e.g. user has been
 * successfully deleted from the identity provider.
 */
public class MockSuccesIdpUserDeleteRequest implements IdentityProviderUserDeleteRequest {

    /**
     * @description Mock method which will mock user deletion
     * @param email Email address of the user to be deleted
     */
    public void deleteUser(String email) {
        System.debug('User with email: ' + email + ' is deleted.');
    }

}