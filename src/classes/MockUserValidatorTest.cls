/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class MockUserValidatorTest {

    @isTest
    static void blankEmailTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isEmailValid(''));
    }
    
    
    @isTest
    static void nullEmailTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isEmailValid(null));
    }


    @isTest
    static void validEmailTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(true, validator.isEmailValid('test.test1@example.com'));
    }
    
    
    @isTest
    static void validFirstNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(true, validator.isFirstNameValid('John'));
    }
    
    
    @isTest
    static void invalidFirstNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isFirstNameValid(null));
    }
    
    
    @isTest
    static void invalidBlankFirstNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isFirstNameValid(''));
    }
    
    
    @isTest
    static void nullBlankFirstNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isFirstNameValid(null));
    }
    
    
    @isTest
    static void validLastNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(true, validator.isLastNameValid('Smith'));
    }
    
    
    @isTest
    static void invalidLastNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isLastNameValid(''));
    }
    
    
    @isTest
    static void invalidBlankLastNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isLastNameValid(''));
    }
    
    
    @isTest
    static void nullBlankLastNameTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isLastNameValid(null));
    }
    
    
    @isTest
    static void validPhoneNumberTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(true, validator.isPhoneNumberValid('+61412936258'));
    }
    
    
    @isTest
    static void invalidBlankPhoneNumberTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isPhoneNumberValid(''));
    }
    
    
    @isTest
    static void invalidNullPhoneNumberTest() {
        MockUserValidator validator = new MockUserValidator();
        System.assertEquals(false, validator.isPhoneNumberValid(null));
    }
}