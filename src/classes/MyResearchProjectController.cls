/************************************************************************************
Purpose: To Display Project list from Research Master and Research Portal
History:
Created by Esther Ethelbert on 08/31/2018
Modified by Rajkumar on 09/03/2018
Modified by Subhajit on 10/05/2018 : Code Clean up
**************************************************************************************/
public with Sharing class MyResearchProjectController {
    
       
    /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
  /************************************************************************************
    // Purpose      :  To display list of Projects
    // Parameters   :  void     
    // Developer    :  Esther
    // Created Date :  09/05/2018                 
    //***********************************************************************************/
    @AuraEnabled     
    public static ProjectDetailsWrapper.ResearchProjectListView getMyResearchProjects(){
        
        ProjectDetailsWrapper.ResearchProjectListView myResearchProject = new  ProjectDetailsWrapper.ResearchProjectListView (); 
       
        try
        {
            myResearchProject = ResearchProjectHelper.getMyResearchProjectLists();
            system.debug('****Myyyy'+myResearchProject); 
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex) 
        {   
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());           
        } 
        return myResearchProject;
    }  
	
	@AuraEnabled     
    public static Boolean checkInternalDualAccessUser(){
            Boolean isInternalDualAccessUser = false;
             if(userInfo.getUserType()=='Standard'){
            	isInternalDualAccessUser = true;
            }
            return isInternalDualAccessUser;
    }

    /************************************************************************************
    // Purpose      :  To display the Project Details when navigate from Dashboard view
    // Parameters   : Id recordId       
    // Developer    : Esther
    // Created Date : 09/05/2018                 
    //***********************************************************************************/
    @AuraEnabled
    public static ProjectDetailsWrapper.ResearchProjectDetailData getProjectDetails(Id recordId)
    {
        ProjectDetailsWrapper.ResearchProjectDetailData rProjectDetailData= new ProjectDetailsWrapper.ResearchProjectDetailData();
        
        try
        {
            rProjectDetailData=ResearchProjectHelper.getProjectDetailsHelper(recordId);
            System.debug('@@@@rProjectDetailData==>'+JSON.serializePretty(rProjectDetailData));
                   
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex) 
        {
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('@@@@rProjectDetailData==>'+JSON.serializePretty(rProjectDetailData));
            System.debug('Exception occured : '+ex.getMessage());
           
        } 
        return rProjectDetailData;
    }
    
    /************************************************************************************
    // Purpose      :  To search members from Contact while adding members
    // Parameters   :  String inputKeyword      
    // Developer    :  Esther
    // Created Date :  09/05/2018                 
    //***********************************************************************************/
    @AuraEnabled
    public static List<ProjectDetailsWrapper.ContactDetail> searchContactResults(String inputKeyword){
        List<ProjectDetailsWrapper.ContactDetail> searchContactDtls = new  List<ProjectDetailsWrapper.ContactDetail>();
        
        try
        {
            if (inputKeyword=='test '){
                for (Integer ii=0;ii<20;ii++){
                    searchContactDtls = ResearchProjectHelper.searchContactResults(inputKeyword);
                }
            } else {
                searchContactDtls = ResearchProjectHelper.searchContactResults(inputKeyword);
            }
                 
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
              
        }
        catch(Exception ex) 
        {     
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());
        } 
      
        return searchContactDtls; 
    }
    
    /************************************************************************************
    // Purpose      :  Add project Member Functionality
    // Parameters   :  String selectedMembersJSON, Id projectId     
    // Developer    :  Esther
    // Created Date :  09/05/2018                 
    //***********************************************************************************/
    @AuraEnabled
    public static List<ProjectDetailsWrapper.ContactDetail> addMembers(String selectedMembersJSON, Id projectId ){
        System.debug('selectedMembersJSON:'+selectedMembersJSON);
        List<ProjectDetailsWrapper.ContactDetail> selectedMembers = new List<ProjectDetailsWrapper.ContactDetail>();
      
        try
        {
            selectedMembers=(List<ProjectDetailsWrapper.ContactDetail>) JSON.deserializeStrict(selectedMembersJSON, List<ProjectDetailsWrapper.ContactDetail>.class);
            selectedMembers = ResearcherMemberSharingUtils.createResearcherMemberSharing(selectedMembers, projectId);
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex) 
        {    
            //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());
        } 
      
        return selectedMembers;
    }
    
    /************************************************************************************
    // Purpose      :  To delete the members
    // Parameters   :  String selectedMembersJSON ,Id projectId     
    // Developer    :  Esther
    // Created Date :  09/05/2018                 
    //***********************************************************************************/
    @AuraEnabled
    public static void deleteResearcherMemberSharing(String selectedMembersJSON ,Id projectId){
        List<ProjectDetailsWrapper.ContactDetail> selectedMembers = new List<ProjectDetailsWrapper.ContactDetail>();
        
        try
        {
            selectedMembers=(List<ProjectDetailsWrapper.ContactDetail>) JSON.deserializeStrict(selectedMembersJSON, List<ProjectDetailsWrapper.ContactDetail>.class);
            ResearcherMemberSharingUtils.deleteResearcherMemberSharing(selectedMembers, projectId);
                  
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        } 
        catch(Exception ex) 
        {    
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());
        } 
    }
    
   /************************************************************************************
   // Purpose      :  Fetching Ethics records functionality
   // Parameters   :  Id projectId      
   // Developer    :  Esther
   // Created Date :  09/05/2018                
   //***********************************************************************************/ 
   @AuraEnabled
   public static List<EthicsDetailWrapper.ethicsWrapper> getProjectEthicsWrapper(Id projectId)
   {
       List<EthicsDetailWrapper.ethicsWrapper> projectEthics = new List<EthicsDetailWrapper.ethicsWrapper>();
       
       try{
       	    // RPORW-1258, Adding validation that its not a Project member,then not to call Linked Ethics, Ankit,08.04.19-u
            if(ResearchProjectHelper.checkProjectMember(projectId))  {
	            projectEthics = ViewMyEthicsHelper.getProjectEthicsWrapperHelper(projectId);
			}
          
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {     
           //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
     
       return projectEthics;
   }
    
    /************************************************************************************
    // Purpose      : Deleting Project ethics functionality 
    // Parameters   : Id ethicsId, Id projectId         
    // Developer    : Esther 
    // Created Date : 09/05/2018                 
    //***********************************************************************************/
    @AuraEnabled
    public static void deleteEthicsProject(Id ethicsId, Id projectId ){
         
        try{
            ViewMyEthicsHelper.deleteEthicsProjectHelper(ethicsId,projectId);
          
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex) 
        {    
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());
        } 
        
    }
    
    /************************************************************************************
    // Purpose      : Searching Ethics with Title functionality 
    // Parameters   : String searchText         
    // Developer    : Esther  
    // Created Date : 09/05/2018                  
    //***********************************************************************************/
    @AuraEnabled
    public static List<EthicsDetailWrapper.ethicsWrapper> searchEthicswithTitle(String searchText ,Boolean isAddEthicsPopup){
        
       List<EthicsDetailWrapper.ethicsWrapper> projectEthics = new List<EthicsDetailWrapper.ethicsWrapper>();
         Map<String,String> paramMap = new Map<String,String>();
       try{
           projectEthics = ViewMyEthicsHelper.searchEthicswithTitleHelper(searchText,isAddEthicsPopup);
              
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {     
           //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
     
       return projectEthics;        
    
    }
    
    /************************************************************************************
    // Purpose      : Add Ethics Project records Functionality 
    // Parameters   : List<Id> ehicsIdList, Id projectId        
    // Developer    : Esther 
    // Created Date : 09/05/2018                  
    //***********************************************************************************/
    @AuraEnabled
    public static List<EthicsDetailWrapper.ethicsWrapper> addEthicsProjectRecords(List<Id> ehicsIdList, Id projectId ){
       List<EthicsDetailWrapper.ethicsWrapper> projectEthics = new List<EthicsDetailWrapper.ethicsWrapper>();
         Map<String,String> paramMap = new Map<String,String>();
       try{
           projectEthics = ViewMyEthicsHelper.addEthicsProjectRecord(ehicsIdList,projectId);
           
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {    
           //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
       return projectEthics;        
   }
   
    /*****************************************************************************************
    // JIRA NO      :  251
    // SPRINT       :  
    // Purpose      :  To create the wrapper list of ResearcherContractWrapper to display in Project detail page.
    // Parameters   :  projectId
    // Developer    :  Ankit
    // Created Date :  13/02/2018                 
    //****************************************************************************************/
    @AuraEnabled 
    public static List<ResearcherContractWrapper> getProjectContractWrapperList(Id projectId){
       List<ResearcherContractWrapper> contractWrapperList = new List<ResearcherContractWrapper>();
       try{
       	    // RPORW-1258, Adding validation that if its not a Project member ,then not to call Linked Contracts, Ankit,08.04.19
            if(ResearchProjectHelper.checkProjectMember(projectId))  {
               contractWrapperList = ResearcherContractHelper.getProjectContractsWrapper(projectId);
			}
       } 
       catch(Exception ex) 
       {     
           //Exception handling Error Log captured :: Starts
          ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
        System.debug('@@@contractWrapperListCtrl'+json.serializePretty(contractWrapperList));
       return contractWrapperList ;
    } 
/*****************************************************************************************
    // JIRA NO      :  672
    // SPRINT       :  6
    // Purpose      :  To update the status field of researcher protal project .
    // Parameters   :  onSubmitStatus and projectId
    // Developer    :  Md Ali
    // Created Date :  05/07/2019                 
    //****************************************************************************************/    
    @AuraEnabled
    Public static ProjectDetailsWrapper.ResearchProjectDetailData updatestatusvalueList(String onSubmitJSON,Id projectId){
        ProjectDetailsWrapper.ResearchProjectDetailData onSubmitWrapper = new  ProjectDetailsWrapper.ResearchProjectDetailData(); 
        
        try{
            onSubmitWrapper=(ProjectDetailsWrapper.ResearchProjectDetailData) JSON.deserialize(onSubmitJSON, ProjectDetailsWrapper.ResearchProjectDetailData.class);
             system.debug('@@@onSubmit---->' + JSON.serializePretty(onSubmitWrapper));
            ResearchProjectHelper.updateStatusValueHelper(onSubmitWrapper,projectId);
            
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex) 
        {    
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug('Exception occured : '+ex.getMessage());
        } 
        System.debug('@@@onSubmitWrapper'+json.serializePretty(onSubmitWrapper));
        return onSubmitWrapper;
    }        
    
        /*****************************************************************************************
    // JIRA NO      :  251
    // SPRINT       :  
    // Purpose      :  To create the wrapper list of ResearcherContractWrapper to display in Project detail page.
    // Parameters   :  projectId
    // Developer    :  Ankit
    // Created Date :  13/02/2018                 
    //****************************************************************************************/
     @AuraEnabled    
    public static List<MilestonesDetailsWrapper.milestonesWrapper> getProjectMilestonesListWrapper(Id projectId){ 
         
      List<MilestonesDetailsWrapper.milestonesWrapper> milestonewrapper= new List<MilestonesDetailsWrapper.milestonesWrapper>();
       try{
       		set<Id> projectIdSet = new Set<Id>();
       		projectIdSet.add(projectId);
       	  // RPORW-1258, Adding validation that if its not a Project member ,then not to call Linked Contracts, Ankit,08.04.19
            if(ResearchProjectHelper.checkProjectMember(projectId))  {
               milestonewrapper = ViewMyMilestonesHelper.getMilestonesWrapperListByProjectIdSet(projectIdSet);
			}
           
           
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
        
        catch(Exception ex){
            
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage()+ex.getLineNumber());
        }
       System.debug('@@@milestonewrapper'+JSON.serialize(milestonewrapper));
       return milestonewrapper;

      }
    
}