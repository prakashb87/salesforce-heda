/*****************************************************************************************************
*** @Class             : OpenEdxResponseWrapper
*** @Author            : Shreya Barua 
*** @Requirement       : Integration between Salesforce and IPaaS for nerolling /unenrolling into openedx
*** @Created date      : 29/11/2018
*** @JIRA ID           : ECB-4635
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to create a response structure that is received from Ipaas
*****************************************************************************************************/

public class OpenEdxResponseWrapper
{

  public String id;
  public String result;
  public String code;
  public String application;
  public String provider;
  public ResponseInfo payload;
  
  public class ResponseInfo {
    public String userId;
    public List<Response> response;
  }
  
  public class Response {
    public String courseId;
    public String result;
    public String errorMessage;
    public String created;
    public String mode;
    public String is_active;
    public CourseDetails course_details;
  }
  
  public class CourseDetails{
    public String course_id;
    public String course_name;
    public String enrollment_start;
    public String enrollment_end;
    public String course_start;
    public String course_end;
    public String invite_only; 
    public List<CourseMode> course_modes;
  }
  
  public class CourseMode{
    public String slug;
    public String name;
    public String min_price;
    public String suggested_prices;
    public String currency_x;
    public String expiration_datetime;
    public String description; 
    public String sku; 
    public String bulk_sku;  
  }
  

    
  public static OpenEdxResponseWrapper parse(String json) {
    return (OpenEdxResponseWrapper) System.JSON.deserialize(json, OpenEdxResponseWrapper.class);
  }
}