public without sharing class OpenEdxUserCreationController {
    
    public static void openEdxUserCreation() {
        List<String> courseNameList = new List<String>();
		List<hed__Course__c> courseRMITOnlineList = new List<hed__Course__c>();
        List<hed__Course__c> course21CCTrainingList = new List<hed__Course__c>();
  		List<Account> rmitOnlinePrograms = new List<Account>();
  		
        csapi_BasketRequestWrapper.BasketInfoResponse basketresponse = new  csapi_BasketRequestWrapper.BasketInfoResponse();
        csapi_BasketRequestWrapper.BasketInfoRequest basketrequest = new csapi_BasketRequestWrapper.BasketInfoRequest();
       
        basketresponse=csapi_BasketExtension.getBasketInfo(basketrequest);
        system.debug('basketresponse:::'+basketresponse);
        
        for(csapi_BasketRequestWrapper.csapi_CourseInfo response : basketresponse.Courses)
        {
            //if(response.CourseType == 'Course')
            //{
                courseNameList.add(response.CourseName);
            //}
        }
      	System.debug('CCList-->'+courseNameList);
        
        Id courseRMITOnlineRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId(); 
        
        // ECB-5382 Starts:added for creating user in canvas.
        Id course21CCRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId(); 
        Id courseTrainingRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Training').getRecordTypeId(); 
        // ECB-5382 Ends:added for creating user in canvas.


        
        if(!courseNameList.isEmpty() && Schema.sObjectType.hed__Course__c.isAccessible()) {
            courseRMITOnlineList = [SELECT Id,Name From hed__Course__c WHERE Name IN :courseNameList AND RecordTypeId=:courseRMITOnlineRecordTypeId];
            // ECB-5382 Starts:added for creating user in canvas.
            course21CCTrainingList = [SELECT Id,Name From hed__Course__c WHERE Name IN :courseNameList AND (RecordTypeId=:course21CCRecordTypeId OR RecordTypeId=:courseTrainingRecordTypeId)];
            // ECB-5382 Ends:added for creating user in canvas.
            
            // Check if any programs were added
            rmitOnlinePrograms = Database.query('SELECT Id FROM Account WHERE Name IN :courseNameList AND Product_Line__c = \'RMIT Online\'');
        }
        //ECB-5382: Below changes are  for PMD Fix 
        invokeIDAMUserCreationMethod(courseRMITOnlineList,course21CCTrainingList, rmitOnlinePrograms);
               
    }
    
    public static void invokeIDAMUserCreationMethod(List<hed__Course__c> courseRMITOnlineList, 
    		List<hed__Course__c> course21CCTrainingList, List<Account> rmitOnlinePrograms) {
        String useremail=UserInfo.getUserEmail();
        System.debug('User Email ='+useremail);
        Set<String> stringSystems = new Set<String>();

        
        if (!String.isBlank(useremail)) {
            if (!courseRMITOnlineList.isEmpty() || !rmitOnlinePrograms.isEmpty()) {                         
                stringSystems.add('OPENEDX');                
            }
            // ECB-5382 Starts:added for creating user in canvas.
            if (!course21CCTrainingList.isEmpty()) {
                stringSystems.add('CANVAS');                
            }
            if (!stringSystems.isEmpty()) {
                idamUserCreationController(useremail, stringSystems);
            }           
            // ECB-5382 Ends: added for creating user in canvas.
        }
    }
    @future(callout = true)
    public static void idamUserCreationController(String email, Set<String> stringSystems )
    {
        // ECB-5382 Starts:added for creating user in canvas.
        Set<LearningManagementSystem> systems = new Set<LearningManagementSystem>();
        if( ! stringSystems.isEmpty()) {
			for( String systemObj : stringSystems) {
				if( systemObj == 'OPENEDX') {
					systems.add(LearningManagementSystem.OPENEDX);
				} else if( systemObj == 'CANVAS') {
    				systems.add(LearningManagementSystem.CANVAS);
  				}              
			}
        }
        
        // ECB-5382 Ends: added for creating user in canvas.

        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(); 
        if(String.isNotBlank(email)) {
            try
            {
                req.createLearningMgmtSystemUser(email, systems); 
            }
            catch(Exception ex)
            {
                system.debug('Exception:::'+ex);
            }
        }
        
    }
    
}