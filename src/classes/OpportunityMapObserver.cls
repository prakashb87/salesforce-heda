@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class OpportunityMapObserver implements csordcb.ObserverApi.IObserver {  
 
    global void execute(csordcb.ObserverApi.Observable o, Object arg) {   
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable)o; 
        list<Id> serviceIds = observable.getServiceIds();
        //do the work
        system.debug('mapOpportunityFieldValues >> ');
        mapOpportunityFieldValues(serviceIds);
    } 
    
    @TestVisible
    private static void mapOpportunityFieldValues(list<Id> serviceIds) {
       
        list<id> lstOpptyIds = new list<id>();
        for (csord__Service__c service: [select id
                , csordtelcoa__Main_Contact__c
                , csordtelcoa__Product_Basket__r.cscfga__Opportunity__c
            from 
                csord__Service__c 
            where 
                id in :serviceIds]) {    
        
            lstOpptyIds.add(service.csordtelcoa__Product_Basket__r.cscfga__Opportunity__c);        
        }

        map<id, Custom_Opportunity__c> mapOpptyIdToCustomOppty = new map<id, Custom_Opportunity__c>();
        for (Custom_Opportunity__c custOppty: [select id
                , Opportunity__c
                , Main_Contact_Id__c 
            from 
                Custom_Opportunity__c 
            where 
                Opportunity__c in: lstOpptyIds]) {
        
            mapOpptyIdToCustomOppty.put(custOppty.Opportunity__c, custOppty);
        }

        list<csord__Service__c> toUpdate = new list<csord__Service__c>();
        for (csord__Service__c service: [select id
                , csordtelcoa__Main_Contact__c
                , csordtelcoa__Product_Basket__r.cscfga__Opportunity__c
            from 
                csord__Service__c 
            where 
                id in :serviceIds]) {
                    
            service.csordtelcoa__Main_Contact__c = mapOpptyIdToCustomOppty.get(service.csordtelcoa__Product_Basket__r.cscfga__Opportunity__c).Main_Contact_Id__c;
            toUpdate.add(service);      
        }
        
        if (toUpdate.size() > 0) {
            update toUpdate;
        }
    }
}