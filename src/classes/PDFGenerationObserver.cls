/*****************************************************************
Name: PDFGenerationObserver
Author: Capgemini 
Purpose: Observer class to genrate Invoice pdf
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           14/05/2018         Initial Version
********************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class PDFGenerationObserver implements csordcb.ObserverApi.IObserver
 {  
    public static String invoiceAttName = '';
    public static List<Attachment> caseAttachmentList;
    public static List<Task> caseTaskList;
    public static Id caseRecordType;
    public static EmailTemplate paymentInvoiceTemplateObj;
    
    /********************************************************************
    // Purpose              : To invoke the generatePDF method                            
    // Author               : Capgemini [Shreya]
    // Parameters           : null
    //  Returns             : void
    //JIRA Reference        : ECB-102 : Generate a PDF invoice and reference in payment table to order
    //********************************************************************/
      
    global void execute(csordcb.ObserverApi.Observable o, Object arg) 
    {   
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable)o; 
        list<Id> serviceIds = observable.getServiceIds();
        system.debug('serviceIds:::'+serviceIds);
        if(!serviceIds.isEmpty() && !System.isBatch())
        {
            generatePDF(serviceIds);
        }    
    } 
    
     /********************************************************************
    // Purpose              : 1. Query all the details from Service Object and generate the pdf
                              2. Get the pdf content and store it in Attachment/Files
                              3. Create a Payment record                              
    // Author               : Capgemini [Shreya]
    // Parameters           : List<Id> serviceIds
    //  Returns             : void
    //JIRA Reference        : ECB-102 : Generate a PDF invoice and reference in payment table to order
                              ECB-3241 : Consume receipt ID from OneStop and add to payment table and PDF
    //********************************************************************/
    @TestVisible
    @future(callout=true)
    public static void generatePDF(List < Id > serviceIds) 
    {
        
            system.debug('serviceId:::'+serviceIds);
                      
            List <csord__Service__c> serviceList = [SELECT id,csordtelcoa__Product_Configuration__r.Id,
                Plan__c,
                Plan__r.hed__Account__r.Academic_Institution__r.ABN__c,
                Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c,
                csordtelcoa__Product_Basket__r.Basket_Number__c,
                csordtelcoa__Product_Basket__r.cscfga__Total_Price__c,
                csordtelcoa__Main_Contact__r.Name,
                csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__Formula_MailingAddress__c,
                csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c,
                csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c,
                csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c,
                csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c,
                csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c,
                csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c,
                csordtelcoa__Main_Contact__r.Student_ID__c,
                csordtelcoa__Main_Contact__r.Email,
                csordtelcoa__Main_Contact__r.id
                FROM csord__Service__c WHERE Id IN: serviceIds
            ];
                         
            system.debug('serviceList:::'+serviceList);
    
            //csord__Service__c service = new csord__Service__c();

            
            String address;            
            String street;
            String city;
            String cityStatePOCode;
            String country;
            
            //Getting contact's address  details to update contact details on Invoice.pdf
            if(!serviceList.isEmpty())
            {
                
                if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c !=null){
                    street = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c + ' ';
                }
                if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c !=null){
                    street += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c + ' ';
                }
                if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c !=null){
                    cityStatePOCode = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c + ' ';
                }   
                if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c !=null){
                    cityStatePOCode += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c + ' ';
                }   
                if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c !=null){
                    cityStatePOCode += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c + ' ';
                }   
                if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c !=null){
                    country = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c + ' ';
                }   
             }                   
        
            //Returning Ivoice pdf name from custom setting 
            invoiceAttName = ConfigDataMapController.getCustomSettingValue('InvoiceAttName'); 

           // Returning payment record associated with the product Basket in order to attach Invoice.pdf
            Payment__c paymentRec = new Payment__c ();
            paymentRec = getpaymentRecord(serviceList);
            
            //Returning  map with key as ABN and value as list of ABN associated with the key ABN
            Map<String, List<csord__Service__c>> abnNewMap= new Map<String,List<csord__Service__c>>();
            abnNewMap = returnABNKeySetValue(serviceList);

            system.debug('Map=' + abnNewMap);
            
            //Getting all unique ABN values 
            Set<String> abnSet=new Set<String>();
            abnSet=abnNewMap.keyset();

            String owEmailAddressId = EmailSending.generateOWEmailId();

            // Getting RMITRequestSupport case record type
            caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RMIT Support Request Case').getRecordTypeId(); 

            // Case List
            List<case> caseList = [select id, contactId, recordtypeID from case where recordtypeId = :caseRecordType AND ContactId = :serviceList[0].csordtelcoa__Main_Contact__r.Id];
            
            //Get PaymentInvoiceTemplateId
            paymentInvoiceTemplateObj = new EmailTemplate();
            paymentInvoiceTemplateObj = [select id, name from emailtemplate where name ='Payment Invoice Template' LIMIT 1];
            
            // List of attacment to insert to payment
            list<Attachment> attPaymentList = new List<Attachment>();

            //List of attachment to insert to case 
            caseAttachmentList = new List<Attachment>();

            //List of task to attach to case
            caseTaskList = new List<Task>();

            for(String abn: abnSet)
            {
                 String prodConfigIdString = '';  
                 String invoiceABN = '';
                 system.debug('RRRR:Each ABN Number=' + abn);
                 if(!String.isBlank(abn) )
                 {
                    invoiceABN = abn;
                 }
                 List<csord__Service__c > abnServiceLisit= new List<csord__Service__c >();
                 abnServiceLisit=abnNewMap.get(abn);
                 System.debug( 'RRR:Service for ABN=' + abnServiceLisit);

                        /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf: Ends: 25/07/2018 **/
                        if (!abnServiceLisit.isEmpty()) 
                        {
                             //service = serviceList[0];
                            for (csord__Service__c ser: abnServiceLisit) 
                            {
                
                                if (ser.csordtelcoa__Product_Configuration__r.Id != null)
                                {
                                    //To store all the prod configuration Id as a string and pass it to the vf page
                                    prodConfigIdString = prodConfigIdString + ser.csordtelcoa__Product_Configuration__r.Id + ',';
                
                                }
                            }
                        }
                        prodConfigIdString.removeEnd(',');
                        system.debug('prodConfigIdString:::::::::' + prodConfigIdString);

                        Attachment att = new Attachment();
                        att =  createPaymentInvoiceAttachmentFiles(abnServiceLisit,street,city,cityStatePOCode,country,prodConfigIdString,paymentRec,invoiceABN);
                        attPaymentList.add(att);
            }
            if( !attPaymentList.IsEmpty())
            {
                insert attPaymentList;

                for( Attachment attObj: attPaymentList)
                {
                    sendPdf(serviceList[0], attObj.body,owEmailAddressId,caseList);
                }
                if( !caseAttachmentList.IsEmpty())
                {
                    insert caseAttachmentList;
                }
                if( !caseTaskList.IsEmpty())
                {
                    insert caseTaskList;
                }
            }
            
    }
   /* public static List <csord__Service_Line_Item__c> serviceLineItemList(List < Id > serviceLineItemsIds) 
    {
                List <csord__Service_Line_Item__c> serviceLineItemsList = [SELECT id,GST__c,csord__Service__c
                FROM csord__Service_Line_Item__c WHERE csord__Service__r.id IN:serviceLineItemsIds];
                return serviceLineItemsList;
    }  */
 
    
    public static Map<String, List<csord__Service__c>> returnABNKeySetValue(List <csord__Service__c> serviceList)
    {
        Map<String, List<csord__Service__c>> abnMap= new Map<String,List<csord__Service__c>>();
           for ( csord__Service__c serv : serviceList )
           {
                List<csord__Service__c> ser=new List<csord__Service__c>();
                List<csord__Service__c> updateSer=new List<csord__Service__c>();

                if(!String.Isblank(serv.Plan__c) && !String.isBlank( serv.Plan__r.hed__Account__r.Academic_Institution__r.ABN__c))
                {
                    
                    if(abnMap.get(serv.Plan__r.hed__Account__r.Academic_Institution__r.ABN__c) ==null )
                    {
                        ser.add(serv);
                        abnMap.put(serv.Plan__r.hed__Account__r.Academic_Institution__r.ABN__c,ser);
                    }
                    else
                    {
                        updateSer=abnMap.get(serv.Plan__r.hed__Account__r.Academic_Institution__r.ABN__c);
                        updateSer.add(serv);
                            
                        abnMap.remove(serv.Plan__r.hed__Account__r.Academic_Institution__r.ABN__c);
                        abnMap.put(serv.Plan__r.hed__Account__r.Academic_Institution__r.ABN__c,updateSer);
                    }
                    
                }
                else if(!String.Isblank(serv.Course_Offering__c) && !String.isBlank(serv.Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c ))
                {
                    if(abnMap.get(serv.Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c)==null)
                    {
                        ser.add(serv);
                        abnMap.put(serv.Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c,ser);
                    }
                    
                    else
                    {
                        updateSer=abnMap.get(serv.Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c);
                        updateSer.add(serv);
                        
                        abnMap.remove(serv.Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c);
                        abnMap.put(serv.Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c,updateSer);

                    }
                }

           }
          
           return abnMap;

    }


    public static Payment__c getpaymentRecord( List <csord__Service__c> serviceList)
    {
        Payment__c paymentRec; 
        try
        { 
            paymentRec = [Select Id,Product_Basket__c,Receipt_Number__c from Payment__c where Product_Basket__c = :serviceList[0].csordtelcoa__Product_Basket__c];
            system.debug('paymentObj::::' + paymentRec.Id);
        }
        catch(QueryException ex)
        {
            system.debug('Exception:::'+ex);
        }
        return paymentRec;  
    }
    public static Attachment createPaymentInvoiceAttachmentFiles( List <csord__Service__c> serviceList,String street,String city,String cityStatePOCode,String country,String prodConfigIdString,Payment__c paymentRec,String invoiceABN)
    {
            //Passing parameters to the InvoicePDF vf page
            PageReference pdf = Page.InvoicePDF;
            
            if(!serviceList.isEmpty())
            {
                pdf.getParameters().put('basketNumber', serviceList[0].csordtelcoa__Product_Basket__r.Basket_Number__c);
                pdf.getParameters().put('studentName', serviceList[0].csordtelcoa__Main_Contact__r.Name);
                pdf.getParameters().put('studentId', serviceList[0].csordtelcoa__Main_Contact__r.Student_ID__c);
                //pdf.getParameters().put('totalPrice', String.valueOf(serviceList[0].csordtelcoa__Product_Basket__r.cscfga__Total_Price__c));
            }
            
            /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf:Starts : 25/07/2018 **/
            if(street!='' && street!=null)
            {
                pdf.getParameters().put('street', street );
            } 
            
            if(cityStatePOCode!='' && cityStatePOCode!=null)
            {
                pdf.getParameters().put('cityStatePOCode', cityStatePOCode);
            } 
            if(country!='' && country!=null)
            {
                pdf.getParameters().put('country', country );
            } 
            
            /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf: Ends: 25/07/2018 **/
            
            if(prodConfigIdString!='' && prodConfigIdString!=null)
            {
                pdf.getParameters().put('prodConfigIds', prodConfigIdString);
            }
            if(paymentRec!=null)
            {
                pdf.getParameters().put('receiptNo', paymentRec.Receipt_Number__c);
            }
            if(paymentRec!=null)
            {
                pdf.getParameters().put('receiptNo', paymentRec.Receipt_Number__c);
            } 
            if(!String.isBlank(invoiceABN))
            {
                pdf.getParameters().put('invoiceABN', inVoiceABN);
            }   
            
            Blob body;
            //getting the dynamic content of pdf
            try
            {
                if(Test.isRunningTest())
                 { 
                      body = blob.valueOf('Unit.Test');
                 }
                 else
                 {
                    body = pdf.getContent();
                 }
                
            } 
            catch (VisualforceException e) 
            {
                body = Blob.valueOf('No Pdf');
            }    
            system.debug('body:::' + body);
            
           
            //Code to save the generated pdf in Payment's Notes & Attachment
            Attachment att = new Attachment();
            if(paymentRec != null)
            {       
                
                att.Body = body;
                att.ParentId = paymentRec.Id;
                att.Name = invoiceAttName;
            }     
           return att;
                 
    }

    public static void sendPdf( csord__Service__c ser, Blob body, String owEmailAddressId, List<Case> caseList)
    {    
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            attach.setContentType('application/pdf');
            attach.setFileName('Payment Invoice.pdf');
            attach.setInline(false);
            attach.Body = body;
    
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            String[] toAddresses = new String[]
             {
                ser.csordtelcoa__Main_Contact__r.Email
            };
            mail.setToAddresses(toAddresses);           
            mail.setOrgWideEmailAddressId(owEmailAddressId);
     
            system.debug('To Address =' + toAddresses.get(0));
            mail.setTargetObjectId(ser.csordtelcoa__Main_Contact__r.id);
            system.debug('Contact Id='+ ser.csordtelcoa__Main_Contact__r.id );
            mail.setTemplateId(paymentInvoiceTemplateObj.id); 
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});        
            mail.setSaveAsActivity(true);
            if(!System.isBatch())
            { 
                try
                {  
                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                }
                catch(exception ex)
                {
                     system.debug('Exception:::'+ex);
                }   
            }
            
        case c = new case ();
        Attachment attachment1 = new Attachment();
        if (caseList.size() == 0)
         {
            //c.RecordTypeId = ConfigDataMapController.getCustomSettingValue('PaymentCaseRecordID'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            c.RecordTypeId = caseRecordType;
            c.Origin = ConfigDataMapController.getCustomSettingValue('PaymentCaseEmail'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            c.Status = ConfigDataMapController.getCustomSettingValue('PaymentCaseStatus'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            c.description = mail.getPlainTextBody(); 
            c.subject = mail.subject;
            c.ContactId = ser.csordtelcoa__Main_Contact__r.Id;
            c.Type = 'MarketPlace';                       
            try
            {
                insert c;
            }
            catch(DmlException ex)
            {
                system.debug('Exception:::'+ex);
            }    
            attachment1.Body = body;
            attachment1.Name = 'invoice';
            attachment1.ParentId = c.id;
            attachment1.ContentType = '.pdf';
            caseAttachmentList.add(attachment1);   
                
        }
         else 
         {
            attachment1.Body = body;
            attachment1.Name = 'invoice';
            attachment1.ParentId = caseList[0].id;
            attachment1.ContentType = '.pdf';
            caseAttachmentList.add(attachment1);  
        }

        Task t = new Task();
        t.subject = mail.subject;
        t.Status = ConfigDataMapController.getCustomSettingValue('PaymentTaskStatus'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
        t.description = mail.getPlainTextBody();
        if(caseList.size() > 0)
        {
            t.WhatId = caseList[0].Id;
        }
        else
        {
           t.WhatId = C.Id;
        }
        t.WhoId=ser.csordtelcoa__Main_Contact__r.Id;       
        caseTaskList.add(t);
    }    
}