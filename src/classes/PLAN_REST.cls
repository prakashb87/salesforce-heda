/*********************************************************************************
 *** @ClassName         : PLAN_REST
 *** @Author            : Dinesh Kumar 
 *** @Requirement       : RM-1608
 *** @Created date      : 29/08/2018
 *** @Modified by       : Shubham Singh
 *** @modified date     : 22/10/2018
 **********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Plan')
global with sharing class PLAN_REST {
    public String institution;
    public String academicPlan;
    public String academicProgram;
    public String effectiveDate;
    public String status;
    public String academicCareer;
    public String academicPlanType;
    public String description;
    public String shortDescription;
    public String firstTermValid;
    public String degree;
    public String transcriptLevel;
    public String ownership_academicOrganisation; //originalfield from json  --> ownership.academicOrganisation
    public String ownership_percentageOwned; //originalfield from json --> ownership.percentageOwned
    public String fieldOfStudyCode;
    public String fieldOfEducationCode;
    public String specialisationCode;
    public String cricosCode;
    public String aqfCode;
    public String fieldOfStudyGroup;


    @HttpPost
    global static Map < String, String > upsertPlan() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert plan
        List < Plan__c > upsertPlanRecord = new List < Plan__c > ();
        //Plan object
        Plan__c plan;
        //Plan__c Record
        Plan__c planRecord;

        //List of JSON STRING Date
        Map < String, Date > updateEffectiveDate = new Map < String, Date > ();

        Map < String, Plan__c > updateOldPLan = new Map < String, Plan__c > ();
        //Map <String,Plan__c> strPlanMap = new Map <String,Plan__c>();

        //Account Object
        Account institution;
        //key to get unique values
        String key;
        try {

            //request and response variable
            RestRequest request = RestContext.request;

            RestResponse response = RestContext.response;

            //no need to add [] in JSON
            String reqStrOne = '[' + request.requestBody.toString() + ']';
            String reqStrTwo = reqStrOne.replaceFirst('"ownership.academicOrganisation"', '"ownership_academicOrganisation"');
            String reqStrFinal =  reqStrTwo.replaceFirst('"ownership.percentageOwned"', '"ownership_percentageOwned"');
            System.debug('Request From SAMS----------------> '+ reqStrFinal);
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List < PLAN_REST > postString = (List < PLAN_REST > ) System.JSON.deserialize(reqStrFinal, List < PLAN_REST > .class);
            
            List<String> listInstitution = new List<String>();
            
            //get Academic Program
            List<String> listAcademicProgram = new List<String>();
            
            //get Academic Plan
            List<String> listAcademicPlan = new List<String>();
            
            //get Academic Organization
            List<String> listAcademicOrganization = new List<String>();
            
            //get FieldOfStudyCode
            List<String> listFieldOfStudyCode = new List<String>();
            
            //get fieldOfEducationCode
            List<String> listfieldOfEducationCode = new List<String>();
            
            //get fieldofStudyGroup
            List<String> listFieldofStudyGroup = new List<String>();
            
            //get check existing plan Records with Account 
            List<Plan__c> checkExistingRecord;
            
            //check existing Institution record on Account
            List<Account> checkExistingAccountRecord;
            
            //get list of exisitng Plan__c object records
            List<Plan__c> lstExisitingPlanRec = new List<Plan__c>();
            
            //String ScurrentDate = Datetime.now().format('yyyy-MM-dd');
            List<Account> acadProgramList = new List<Account>();
            List<Account> acadOrganizationList = new List<Account>();
            List<Account> academicInstitutionList = new List<Account>();
            List<Field_of_Study__c> fieldOfStudyList = new List<Field_of_Study__c>();
            List<Field_of_education__c> fieldofeducationList = new List<Field_of_education__c>();
            String uniqueKey = '';
            
            if (postString != null) {
                for (PLAN_REST planRest: postString) {                    
                    if ((planRest.institution != null && (planRest.institution).length() > 0  && planRest.academicPlan != null && (planRest.academicPlan).length() > 0)){
                        
                        listAcademicPlan.add(planRest.academicPlan);
                        
                        if((planRest.academicProgram).length() > 0){
                            listAcademicProgram.add(planRest.academicProgram);    
                        }
                        
                        if((planRest.ownership_academicOrganisation).length() > 0){
                            listAcademicOrganization.add(planRest.ownership_academicOrganisation);    
                        }
                        
                        if(planRest.fieldOfStudyCode.length() > 0){
                            listFieldOfStudyCode.add(planRest.fieldOfStudyCode);    
                        }
                        if(planRest.fieldOfStudyGroup.length() >0 ){
                            listFieldofStudyGroup.add(planRest.fieldOfStudyGroup);
                        }
                        if(planRest.fieldOfEducationCode.length() > 0){
                            listfieldOfEducationCode.add(planRest.fieldOfEducationCode);    
                        }    
                        
                        if(planRest.institution.length() > 0){ 
                            listInstitution.add(planRest.institution);
                        } 
                        
                        uniqueKey = listInstitution[0]+' '+listAcademicPlan[0];                        
                        updateEffectiveDate.put(uniqueKey,Date.valueOf(planRest.effectiveDate));
                        
                    }else{
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                }
                //get institution from account and Academic Program
                
                
                //Date currentDate;
                
                //Map <String,Plan__c> updateOldPLanEff = new Map <String,Plan__c>();
                
                If(listAcademicProgram != null && listAcademicProgram.size() > 0){
                    acadProgramList = [SELECT Id FROM Account WHERE AccountNumber =: listAcademicProgram[0] AND Effective_Date__c <= TODAY ORDER BY Effective_Date__c DESC LIMIT 1];    
                }
                
                If(listAcademicOrganization != null && listAcademicOrganization.size() > 0){
                    acadOrganizationList = [SELECT Id FROM Account WHERE AccountNumber__c =: listAcademicOrganization[0] AND Effective_Date__c <= TODAY ORDER BY Effective_Date__c DESC LIMIT 1];
                }
                
                If(listInstitution != null && listInstitution.size() > 0){
                    academicInstitutionList = [SELECT Id FROM Account WHERE Name =: listInstitution[0] /*AND Effective_Date__c <= TODAY ORDER BY Effective_Date__c DESC*/ LIMIT 1];
                }
                
                If(listFieldofStudyGroup != null && listFieldofStudyGroup.size() > 0 && listFieldOfStudyCode != null && listFieldOfStudyCode.size() > 0){
                    fieldOfStudyList = [SELECT Id FROM Field_of_Study__c WHERE Field_of_Study_Code__c =: listFieldofStudyGroup[0] AND Name =: listFieldOfStudyCode[0] AND Effective_Date__c <= TODAY ORDER BY Effective_Date__c DESC LIMIT 1];
                }
                
                If(listfieldOfEducationCode != null && listfieldOfEducationCode.size() > 0){
                    fieldofeducationList = [SELECT Id FROM Field_of_education__c WHERE Name =: listfieldOfEducationCode[0] AND Effective_Date__c <= TODAY ORDER BY Effective_Date__c DESC LIMIT 1];
                }   
                if((academicInstitutionList != null  && academicInstitutionList.size() > 0) && (listAcademicPlan != null && listAcademicPlan.size() >0) ){
                    checkExistingRecord = [SELECT Id, Academic_Program__c, Academic_Program__r.Name, Field_of_Study_Code__r.Name, 
                                           Field_of_Education_Code__r.Name, Effective_Date__c, Effective_Status__c, Academic_Career__c, 
                                           Academic_Plan_Type__c, Description__c, Short_Description__c, First_Term_Valid__c, Degree__c, 
                                           Transcript_Level__c,Academic_Organization__c, Percentage_owned__c, Academic_Organization__r.Name, 
                                           Field_of_Study_Code__c, Specialisation_Code__c, CRICOS_Code__c, Field_of_Education_Code__c, 
                                           AQF_Code__c, Field_of_Study_Group__c, Institution__c, Academic_Plan__c FROM Plan__c WHERE Institution__r.Name =: listInstitution[0] AND Academic_Plan__c =: listAcademicPlan[0]];
                    uniqueKey = listInstitution[0]+' '+listAcademicPlan[0];
                }
                system.debug('checkExistingRecord ' + checkExistingRecord);
                if (checkExistingRecord != null && checkExistingRecord.size() > 0) {
                    //if (checkExistingRecord.size() > 0) {
                        //planRecord = [SELECT Id, Institution__c, Academic_Plan__c, Effective_Date__c FROM Plan__c WHERE Institution__r.Name =: listInstitution[0] AND Academic_Plan__c =: listAcademicPlan[0]];
                        for(Plan__c planRecs: checkExistingRecord){
                            if(updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null){
                                if (updateEffectiveDate.get(uniqueKey) <= System.today() &&  updateEffectiveDate.get(uniqueKey) > checkExistingRecord[0].Effective_Date__c ){
                                    updateOldPLan.put(uniqueKey,planRecs);
                                    //strPlanMap.put(uniqueKey,planRecs);//Added by - Rahul
                                }else if(updateEffectiveDate.get(uniqueKey) > System.today()){
                                    //To be inserted in Future Change object
                                    responsetoSend = caseMatching(postString, checkExistingRecord);
                                    system.debug('checkExistingRecord ' + checkExistingRecord);
                                    return responsetoSend;
                                } else if (updateEffectiveDate.get(uniqueKey) < checkExistingRecord[0].Effective_Date__c) {
                                    responsetoSend.put('message', 'OK');
                                    responsetoSend.put('status', '200');
                                    return responsetoSend;
                                }
                            }else{
                                responsetoSend.put('message','OK');
                                responsetoSend.put('status','200');
                                return responsetoSend;
                            }
                        }        
                    }
            }

            //insert or update the Plan
            for (PLAN_REST planRest: postString) {
                plan = new Plan__c();
                plan.Name = planRest.description;
                key = planRest.institution + ' ' + planRest.academicPlan;
                plan.Institution__c = academicInstitutionList.size() > 0 ? academicInstitutionList[0].Id : null;
                plan.Academic_Plan__c = planRest.academicPlan;
                plan.Academic_Program__c = acadProgramList.size() > 0 ? acadProgramList[0].Id : null;
                plan.Effective_Date__c = Date.valueOf(planRest.effectiveDate);
                plan.Effective_Status__c = planRest.status != null && planRest.status.equalsIgnoreCase('A') ? 'Active' : 'Inactive';
                plan.Academic_Career__c = planRest.academicCareer;
                /*if(planRest.academicPlanType == 'PRP'){
                    plan.Academic_Plan_Type__c = 'Default';
                    }else{
                    plan.Academic_Plan_Type__c = 'Default';
                    }*/ //Scope changed RM-1909
                plan.Academic_Plan_Type__c = planRest.academicPlanType;
                //plan.Academic_Plan_Type__c = planRest.academicPlanType == 'PRP'? 'Default':null;//planRest.academicPlanType; Scope Changed
                plan.Description__c = planRest.description;
                plan.Short_Description__c = planRest.shortDescription;
                plan.First_Term_Valid__c = planRest.firstTermValid;
                plan.Degree__c = planRest.degree;
                plan.Transcript_Level__c = planRest.transcriptLevel;
                plan.Academic_Organization__c = acadOrganizationList.size() > 0 ? acadOrganizationList[0].Id : null;               
                plan.Percentage_owned__c = planRest.ownership_percentageOwned;
                plan.Field_of_Study_Code__c = fieldOfStudyList.size() > 0? fieldOfStudyList[0].Id : null;
                plan.Field_of_Education_Code__c = fieldofeducationList.size() > 0 ? fieldofeducationList[0].Id : null;                                
                plan.Specialisation_Code__c = planRest.specialisationCode;
                plan.CRICOS_Code__c = planRest.cricosCode;
                plan.AQF_Code__c = planRest.aqfCode;
                plan.Field_of_Study_Group__c = planRest.fieldOfStudyGroup;
                plan.Plan_key__c = key;

                //currentDate = date.parse(date.today().format());
                
                if(updateOldPLan.size() > 0 && updateOldPLan.get(key) != null){
                    if(updateOldPLan.get(key).Effective_Date__c <= Date.valueOf(planRest.effectiveDate) )
                    {
                        plan.Id = updateOldPLan.get(key).Id;
                    }else{
                        plan = null;
                    }
                }
                if(plan!=null){
			upsertPlanRecord.add(plan);
		}
                system.debug('upsertPlanRecord '+upsertPlanRecord);
            }
            if(upsertPlanRecord !=null && upsertPlanRecord.size()>0 && Date.ValueOf(postString[0].effectiveDate) <= System.today())
            {
                upsert upsertPlanRecord Plan_key__c ;
            } 
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');                      
            return responsetoSend;
            
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;    
        }
        
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Plan was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }

    public static Map < String, String > caseMatching(List < PLAN_REST > planRest, List < Plan__c > pLst) {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        try {
            //List<Plan__c> pLst = new List<Plan__c>();

            List < Plan_Future_Change__c > lstPlanFuChange = new List < Plan_Future_Change__c > ();
            Plan_Future_Change__c planFuChange = new Plan_Future_Change__c();

            string aPDB = null;
            if (pLst[0].Academic_Program__c != Null) {
                aPDB = pLst[0].Academic_Program__r.Name;
            }

            string aPJSON = null;
            if (planRest[0].academicProgram != null && planRest[0].academicProgram.length() > 0) {
                aPJSON = planRest[0].academicProgram;
            }
            List < Account > existingProgramList = [select Id,Name from Account where AccountNumber =: aPJSON Limit 1];
            if (aPDB != null && existingProgramList != null && aPDB != existingProgramList[0].Name) {
                planFuChange = PLAN_REST.createFuPlanChange('Academic_Program__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                
                if (aPJSON != null) {
                    if (existingProgramList.size() > 0) {
                        planFuChange.Value__c = existingProgramList[0].Id;
                    } else {
                        planFuChange.Value__c = null;
                    }
                } else {
                    planFuChange.Value__c = null;
                }
                //planFuChange.Value__c = aPJSON;
                lstPlanFuChange.add(planFuChange);
            }

            string effDateDB = null;
            if (pLst[0].Effective_Date__c != Null) {
                effDateDB = String.valueOf(pLst[0].Effective_Date__c);
            }

            string effDateJSON = null;
            if (planRest[0].effectiveDate != null && planRest[0].effectiveDate.length() > 0) {
                effDateJSON = planRest[0].effectiveDate;
            }

            if (effDateDB != effDateJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Effective_Date__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].effectiveDate;
                lstPlanFuChange.add(planFuChange);
            }

            string effStatusDB = null;
            if (pLst[0].Effective_Status__c != Null) {
                effStatusDB = pLst[0].Effective_Status__c;
            }

            string effStatusJSON = null;
            if (planRest[0].status != null && planRest[0].status.length() > 0) {
                if (planRest[0].status.equalsIgnoreCase('A')) {
                    effStatusJSON = 'Active';
                } else {
                    effStatusJSON = 'Inactive';
                }
                
            }
             system.debug(effStatusDB);
             system.debug((effStatusDB != effStatusJSON));
            system.debug(effStatusJSON);
            if (effStatusDB != effStatusJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Effective_Status__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].status;
                lstPlanFuChange.add(planFuChange);
            }

            string acadCareerDB = null;
            if (pLst[0].Academic_Career__c != Null) {
                acadCareerDB = pLst[0].Academic_Career__c;
            }

            string acadCareerJSON = null;
            if (planRest[0].academicCareer != null && planRest[0].academicCareer.length() > 0) {
                acadCareerJSON = planRest[0].academicCareer;
            }

            if (acadCareerDB != acadCareerJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Academic_Career__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].academicCareer;
                lstPlanFuChange.add(planFuChange);
            }

            string acadPlanTypeDB = null;
            if (pLst[0].Academic_Plan_Type__c != Null) {
                acadPlanTypeDB = pLst[0].Academic_Plan_Type__c;
            }

            string acadPlanTypeJSON = null;
            if (planRest[0].academicPlanType != null && planRest[0].academicPlanType.length() > 0) {
                //acadPlanTypeJSON = planRest[0].academicPlanType;                           
                acadPlanTypeJSON = planRest[0].academicPlanType;
            }

            if (pLst[0].Academic_Plan_Type__c != acadPlanTypeJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Academic_Plan_Type__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].academicPlanType;
                lstPlanFuChange.add(planFuChange);
            }

            string descriDB = null;
            if (pLst[0].Description__c != Null) {
                descriDB = pLst[0].Description__c;
            }

            string descriJSON = null;
            if (planRest[0].description != null && planRest[0].description.length() > 0) {
                descriJSON = planRest[0].description;
            }

            if (descriDB != descriJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Description__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].description;
                lstPlanFuChange.add(planFuChange);
            }

            string shortDescriptionDB = null;
            if (pLst[0].Short_Description__c != Null) {
                shortDescriptionDB = pLst[0].Short_Description__c;
            }

            string shortDescriptionJSON = null;
            if (planRest[0].shortDescription != null && planRest[0].shortDescription.length() > 0) {
                shortDescriptionJSON = planRest[0].shortDescription;
            }

            if (shortDescriptionDB != shortDescriptionJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Short_Description__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].shortDescription;
                lstPlanFuChange.add(planFuChange);
            }

            string firstTermValidDB = null;
            if (pLst[0].First_Term_Valid__c != Null) {
                firstTermValidDB = pLst[0].First_Term_Valid__c;
            }

            string firstTermValidJSON = null;
            if (planRest[0].firstTermValid != null && planRest[0].firstTermValid.length() > 0) {
                firstTermValidJSON = planRest[0].firstTermValid;
            }

            if (firstTermValidDB != firstTermValidJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('First_Term_Valid__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].firstTermValid;
                lstPlanFuChange.add(planFuChange);
            }

            string degreeDB = null;
            if (pLst[0].Degree__c != Null) {
                degreeDB = pLst[0].Degree__c;
            }

            string degreeJSON = null;
            if (planRest[0].degree != null && planRest[0].degree.length() > 0) {
                degreeJSON = planRest[0].degree;
            }

            if (degreeDB != degreeJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Degree__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].degree;
                lstPlanFuChange.add(planFuChange);
            }

            string transcriptLevelDB = null;
            if (pLst[0].Transcript_Level__c != Null) {
                transcriptLevelDB = pLst[0].Transcript_Level__c;
            }

            string transcriptLevelJSON = null;
            if (planRest[0].transcriptLevel != null && planRest[0].transcriptLevel.length() > 0) {
                transcriptLevelJSON = planRest[0].transcriptLevel;
            }

            if (transcriptLevelDB != transcriptLevelJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Transcript_Level__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].transcriptLevel;
                lstPlanFuChange.add(planFuChange);
            }

            string acOrgDB = null;
            if (pLst[0].Academic_Organization__c != Null) {
                acOrgDB = pLst[0].Academic_Organization__r.Name;
            }

            string acOrgJSON = null;
            if (planRest[0].ownership_academicOrganisation != null && planRest[0].ownership_academicOrganisation.length() > 0) {
                acOrgJSON = planRest[0].ownership_academicOrganisation;
            }

            If(acOrgDB != acOrgJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Academic_Organization__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = acOrgJSON != null ? [select Id from Account where AccountNumber__c =: acOrgJSON Limit 1].size() > 0 ? [select Id from Account where AccountNumber__c =: acOrgJSON Limit 1].Id : null : null;
                lstPlanFuChange.add(planFuChange);
            }

            string ownership_percentageOwnedDB = null;
            if (pLst[0].Percentage_owned__c != Null) {
                ownership_percentageOwnedDB = pLst[0].Percentage_owned__c;
            }

            string ownership_percentageOwnedJSON = null;
            if (planRest[0].ownership_percentageOwned != null && planRest[0].ownership_percentageOwned.length() > 0) {
                ownership_percentageOwnedJSON = planRest[0].ownership_percentageOwned;
            }

            if (ownership_percentageOwnedDB != ownership_percentageOwnedJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Percentage_owned__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].ownership_percentageOwned;
                lstPlanFuChange.add(planFuChange);
            }

            string fOSCDB = null;
            if (pLst[0].Field_of_Study_Code__c != Null) {
                fOSCDB = pLst[0].Field_of_Study_Code__r.Name;
            }

            string foscJSON = null;
            if (planRest[0].fieldOfStudyCode != null && planRest[0].fieldOfStudyCode.length() > 0) {
                foscJSON = planRest[0].fieldOfStudyCode;
            }

            if (fOSCDB != foscJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Field_of_Study_Code__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = foscJSON != null ? [select Id from Field_of_Study__c where Name =: foscJSON Limit 1].size() > 0 ? [select Id from Field_of_Study__c where Name =: foscJSON Limit 1].Id : null : null;
                lstPlanFuChange.add(planFuChange);
            }

            string fOECDB = null;
            if (pLst[0].Field_of_Education_Code__c != Null) {
                fOECDB = pLst[0].Field_of_Education_Code__r.Name;
            }

            string fOECJSON = null;
            if (planRest[0].fieldOfEducationCode != null && planRest[0].fieldOfEducationCode.length() > 0) {
                fOECJSON = planRest[0].fieldOfEducationCode;
            }

            if (fOECDB != fOECJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Field_of_Education_Code__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = fOECJSON != null ? [select Id from Field_of_education__c where Name =: fOECJSON Limit 1].size() > 0 ? [select Id from Field_of_education__c where Name =: fOECJSON Limit 1].Id : null : null;
                lstPlanFuChange.add(planFuChange);
            }

            string specialisationCodeDB = null;
            if (pLst[0].Specialisation_Code__c != Null) {
                specialisationCodeDB = pLst[0].Specialisation_Code__c;
            }

            string specialisationCodeJSON = null;
            if (planRest[0].specialisationCode != null && planRest[0].specialisationCode.length() > 0) {
                specialisationCodeJSON = planRest[0].specialisationCode;
            }

            if (specialisationCodeDB != specialisationCodeJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Specialisation_Code__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].specialisationCode;
                lstPlanFuChange.add(planFuChange);
            }

            string cricosCodeDB = null;
            if (pLst[0].CRICOS_Code__c != Null) {
                cricosCodeDB = pLst[0].CRICOS_Code__c;
            }

            string cricosCodeJSON = null;
            if (planRest[0].cricosCode != null && planRest[0].cricosCode.length() > 0) {
                cricosCodeJSON = planRest[0].cricosCode;
            }

            if (cricosCodeDB != cricosCodeJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('CRICOS_Code__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].cricosCode;
                lstPlanFuChange.add(planFuChange);
            }


            string aqfCodeDB = null;
            if (pLst[0].AQF_Code__c != Null) {
                aqfCodeDB = pLst[0].AQF_Code__c;
            }

            string aqfCodeJSON = null;
            if (planRest[0].aqfCode != null && planRest[0].aqfCode.length() > 0) {
                aqfCodeJSON = planRest[0].aqfCode;
            }

            if (aqfCodeDB != aqfCodeJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('AQF_Code__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].aqfCode;
                lstPlanFuChange.add(planFuChange);
            }

            string fieldOfStudyGroupDB = null;
            if (pLst[0].Field_of_Study_Group__c != Null) {
                fieldOfStudyGroupDB = pLst[0].Field_of_Study_Group__c;
            }

            string fieldOfStudyGroupJSON = null;
            if (planRest[0].fieldOfStudyGroup != null && planRest[0].fieldOfStudyGroup.length() > 0) {
                fieldOfStudyGroupJSON = planRest[0].fieldOfStudyGroup;
            }

            if (fieldOfStudyGroupDB != fieldOfStudyGroupJSON) {
                planFuChange = PLAN_REST.createFuPlanChange('Field_of_Study_Group__c');
                planFuChange.Parent_Lookup__c = pLst[0].Id;
                planFuChange.Value__c = planRest[0].fieldOfStudyGroup;
                lstPlanFuChange.add(planFuChange);
            }
            insert lstPlanFuChange;
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
    }

    public static Map < String, Schema.DescribeFieldResult > getFieldMetaData(
        Schema.DescribeSObjectResult dsor, Set < String > fields) {

        // the map to be returned with the final data
        Map < String, Schema.DescribeFieldResult > finalMap =
            new Map < String, Schema.DescribeFieldResult > ();
        // map of all fields in the object
        Map < String, Schema.SObjectField > objectFields = dsor.fields.getMap();

        // iterate over the requested fields and get the describe info for each one. 
        // add it to a map with field name as key
        for (String field: fields) {
            // skip fields that are not part of the object
            if (objectFields.containsKey(field)) {
                Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                // add the results to the map to be returned
                finalMap.put(field, dr);
            }
        }
        return finalMap;
    }

    public static Plan_Future_Change__c createFuPlanChange(String str) {
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        
        //no need to add [] in JSON
        String reqStrOne = '[' + request.requestBody.toString() + ']';
        String reqStrTwo = reqStrOne.replaceFirst('"ownership.academicOrganisation"', '"ownership_academicOrganisation"');
        String reqStrFinal =  reqStrTwo.replaceFirst('"ownership.percentageOwned"', '"ownership_percentageOwned"');
        
        response.addHeader('Content-Type', 'application/json');
        List < PLAN_REST > postString = (List < PLAN_REST > ) System.JSON.deserialize(reqStrFinal, List < PLAN_REST > .class);
        Plan_Future_Change__c planFu = new Plan_Future_Change__c();
        Set < String > fields = new Set < String > {
            str
        };

        Map < String, Schema.DescribeFieldResult > finalMap =
            PLAN_REST.getFieldMetaData(Plan__c.getSObjectType().getDescribe(), fields);

        // only print out the 'good' fields
        for (String field: new Set < String > {
                str
            }) {
            planFu.API_Field_Name__c = finalMap.get(field).getName();
            planFu.Field__c = finalMap.get(field).getLabel();
        }

        for (PLAN_REST checkDate: postString) {
            string jsonDate = checkDate.effectiveDate;
            planFu.Effective_Date__c = Date.valueOf(jsonDate);
        }

        //planFu.Applied__c = true;

        return planFu;
    }
}