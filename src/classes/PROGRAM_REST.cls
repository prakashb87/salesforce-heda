/*********************************************************************************
*** @ClassName         : PROGRAM_REST
*** @Author            : Anupam Singhal
*** @Requirement       : RM-1598
*** @Created date      : 28/08/2018
*** @Modified by       : Anupam Singhal
*** @modified date     : 20/09/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Program')
global with sharing class PROGRAM_REST {
    public String institution;
    public String academicProgram;
    public String effectiveDate;
    public String effectiveStatus;
    public String description;
    public String shortDescription;
    public String academicCareer;
    public String academicCalendarCode;
    public String academicGroup;
    public String academicOrganisation;
    public String defaultAcademicPlan;
    public String defaultCampus;
    public String firstTermValid;
    public String academicLevelRule;
    public String gradingScheme;
    public String gradingBasis;
    public String transcriptLevel;
    public String honorAndAwardDate;
    
    
    @HttpPost
    global static Map < String , String > upsertProgram() {
        //final response to send
        Map < String , String > responsetoSend = new Map < String, String > ();
        List < Account > upsertProgram = new List < Account >();
        List < Account > institutionRecord = new List< Account >();
        List < String > institution = new List < String >();
        List < String > academicProgram = new List < String >();
        List < Account> acadOrgId = new List < Account >();
         
        //get List of Default Campus - RM-1640
        List < String > lstDefaultCampus = new List < String >();
        
        Map < String,Date > updateEffectiveDate = new Map < String,Date >();
        
        Map <String,Id> academicInstitutionId = new Map <String,Id>();
        
        List <String> academicOrganisation = new List <String>();
        
        //RM-1640
        List<Campus__c> lstCampusId = new List<Campus__c>();
        
        
        List<Account> programList;
        String key;
        try{
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            system.debug('reqStr-->' + reqStr);
            List < PROGRAM_REST > postString = (List < PROGRAM_REST > ) System.JSON.deserialize(reqStr, List < PROGRAM_REST > .class);        
            if(postString != null){
                for(PROGRAM_REST programAccount: postString ){
                    if((programAccount.institution != null && (programAccount.institution).length() > 0 ) && (programAccount.academicProgram != null && (programAccount.academicProgram).length() > 0)){
                        institution.add(programAccount.institution);
                        academicProgram.add(programAccount.academicProgram);
                        if(programAccount.academicOrganisation != null && (programAccount.academicOrganisation).length() > 0){
                        	academicOrganisation.add(programAccount.academicOrganisation);
                        }
                        if(programAccount.defaultcampus.length() > 0){
                            lstDefaultCampus.add(programAccount.defaultCampus);    
                        }
                        
                        if(programAccount.effectiveDate != null && (programAccount.effectiveDate).length() > 0 ){//&& Date.valueOf(programAccount.effectiveDate) <= System.today()
                            updateEffectiveDate.put(programAccount.institution+'_'+programAccount.academicProgram,Date.ValueOf(programAccount.effectiveDate));
                        }
                    }else{
                        responsetoSend = throwError();
                        return responsetoSend; 
                    }
                }   
            }
            String uniqueKey = postString[0].institution+'_'+postString[0].academicProgram;
            Date jsonDate = updateEffectiveDate.get(uniqueKey);
            if (lstDefaultCampus != null && lstDefaultCampus.size() > 0){
                lstCampusId = [SELECT Id FROM Campus__c WHERE Name =: lstDefaultCampus[0] LIMIT 1];
            }
            if (institution != null && institution.size() > 0){
                institutionRecord  = [SELECT id FROM ACCOUNT WHERE Name =: institution[0] LIMIT 1];
            }
            system.debug(academicOrganisation);
            system.debug(academicOrganisation.size());
            if(academicOrganisation != null && academicOrganisation.size() > 0){
                acadOrgId = [SELECT Id FROM ACCOUNT WHERE AccountNumber__c =: academicOrganisation[0] LIMIT 1];
            }
                
            if((institution != null && institution.size() > 0)&&(academicProgram != null && academicProgram.size() > 0)){
                programList = [SELECT Id, Effective_Date__c, Status__c, Description,Short_Description__c, Academic_Group__c,
                              Academic_Career__c, Academic_Calendar__c,Composite_Key__c, Academic_Organisation__c, Default_Academic_Plan__c,
                              Default_Campus__c, Default_Campus__r.Name, First_Term_Valid__c, Academic_Level_Rule__c,
                              Grading_Scheme__c, Grading_Basis__c, Transcript_Level__c, Honor_and_Award_Date__c,Academic_Institution__r.Name,Academic_Program__c FROM Account WHERE Academic_Institution__c =: institutionRecord[0].ID AND Academic_Program__c =: academicProgram[0]];
            }
            
            
            Map <String, Account> oldProgramUpdate = new Map <String, Account>();
            
            if(programList != null && programList.size() > 0 ){
               for(Account progList: programList){
                    if(updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null){
                        if (updateEffectiveDate.get(uniqueKey) <= System.today() &&  updateEffectiveDate.get(uniqueKey) >= progList.Effective_Date__c ){
                            oldProgramUpdate.put(uniqueKey,progList);
                        }else if(updateEffectiveDate.get(uniqueKey) > System.today()){
                            //To be inserted in Future Change object
                            caseMatching(postString, programList);
                        }else if(updateEffectiveDate.get(uniqueKey) < progList.Effective_Date__c){
                            responsetoSend.put('message','OK');
                            responsetoSend.put('status','200');
                            return responsetoSend;
                        }
                        
                    }else{
                        responsetoSend.put('message','OK');
                        responsetoSend.put('status','200');
                        return responsetoSend;
                    }    
                }
                
            }
            
            Account program;
            system.debug('postString-->' + postString);
            system.debug('oldProgramUpdate.size()-->' + oldProgramUpdate.size());
            //Insert or update program account
            for(PROGRAM_REST progRest: postString){
                program = new Account();
                key = progRest.institution+'_'+progRest.academicProgram;
                
                if(progRest.description == null || String.isEmpty(progRest.description) || progRest.description == ''){
                    program.Name = progRest.academicProgram;
                }else{
                    //if(oldProgramUpdate.size() == 0 && oldProgramUpdate.get(key) == null){
                        program.Name = progRest.description;
                    //}
                }
                program.Academic_Institution__c = institutionRecord != null && institutionRecord.size() > 0 ? institutionRecord[0].ID : null;
                program.AccountNumber = progRest.academicProgram;
                program.Effective_Date__c = progRest.effectiveDate != null && (progRest.effectiveDate).length()>0 ? Date.valueOf(progRest.effectiveDate) : null;
                program.Status__c = progRest.effectiveStatus == 'A'?'Active':progRest.effectiveStatus == 'I'?'Deactive': null ;
                system.debug(progRest.description);
                program.Description = progRest.description;
                system.debug( program.Description);
                program.Short_Description__c = progRest.shortDescription;
                program.Academic_Career__c = progRest.academicCareer;
                program.Academic_Calendar__c = progRest.academicCalendarCode;
                program.Academic_Group__c = progRest.academicGroup;
                //commented as now we are using the ParentId
                //program.Academic_Organisation__c = progRest.academicOrganisation != null ? progRest.academicOrganisation:null;
                //program.ParentId = progRest.academicOrganisation != null ? progRest.academicOrganisation:null;
                if(acadOrgId != null){
                    program.ParentId = acadOrgId.size() >0 ? acadOrgId[0].Id : null;
                }
                //program.ParentId = acadOrgId.size() >0 ? acadOrgId[0].Id : null;
                program.Default_Academic_Plan__c = progRest.defaultAcademicPlan;
                //RM-1640
                program.Default_Campus__c = lstCampusId.size() > 0 && lstCampusId[0] != null ? lstCampusId[0].Id : null;
                program.First_Term_Valid__c = progRest.firstTermValid;
                program.Academic_Level_Rule__c = progRest.academicLevelRule;
                program.Grading_Scheme__c = progRest.gradingScheme;
                program.Grading_Basis__c = progRest.gradingBasis;
                program.Academic_Program__c = progRest.academicProgram;
                program.Transcript_Level__c = progRest.transcriptLevel;
                program.Honor_and_Award_Date__c =progRest.honorAndAwardDate;
                program.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Program').RecordTypeId;
                program.Program_key__c = key ;
                if(oldProgramUpdate.size() > 0 && oldProgramUpdate.get(key) != null){
                    if(oldProgramUpdate.get(key).Effective_Date__c <= Date.valueOf(progRest.effectiveDate))
                    {
                        program.Id = oldProgramUpdate.get(key).Id;
                    }else{
                        program = null;
                    }
                }
                system.debug('upsertProgram3-->');
                if(program != null){
                    upsertProgram.add(program);               
                }
                system.debug('upsertProgram-->' + upsertProgram );
            }
            
            if(upsertProgram != null && upsertProgram.size() > 0 && Date.valueOf(postString[0].effectiveDate) <= system.today()){
                upsert upsertProgram Program_key__c;
            }
             system.debug(upsertProgram);
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
            
        }catch(Exception excpt){
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Program was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
    
    public static void caseMatching(List<PROGRAM_REST> postString, List<Account> programRecords){
        
        //Future Change variables
        List<Future_Change__c> ListFutChange = new List<Future_Change__c>();
        Future_Change__c FutChangeRec = new Future_Change__c();
        
        
        string effDateDB = null;
        if (programRecords[0].Effective_Date__c != Null){
            effDateDB = String.valueOf(programRecords[0].Effective_Date__c);
        }
        
        string effDateJSON = null;
        if (postString[0].effectiveDate.length() > 0){
            effDateJSON = postString[0].effectiveDate;                           
        }
        
        if(effDateDB != effDateJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Effective_Date__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].effectiveDate;
            ListFutChange.add(FutChangeRec);
        }
        
        string effStatusDB = null;
        if (programRecords[0].Status__c != Null){
            effStatusDB = programRecords[0].Status__c;
        }
        
        string effStatusJSON = null;
        if (postString[0].effectiveStatus.length() > 0){
            if(postString[0].effectiveStatus == 'A'){
                effStatusJSON = 'Active';
            }else if(postString[0].effectiveStatus == 'I'){
                effStatusJSON = 'Deactive';
            }
        }
        
        if(effStatusDB != effStatusJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Status__c');//Error in this field
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = effStatusJSON;
            ListFutChange.add(FutChangeRec);
        }
        
        string DescriptionDB = null;
        if (programRecords[0].Description != Null){
            DescriptionDB = programRecords[0].description;
        }
        
        string DescriptionJSON = null;
        if (postString[0].description.length() > 0){
            DescriptionJSON = postString[0].Description;                           
        }
        
        if(DescriptionDB != DescriptionJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Description');//Standard field
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].description;
            ListFutChange.add(FutChangeRec);
        }
        
        string shortDescriptionDB = null;
        if (programRecords[0].Short_Description__c != Null){
            shortDescriptionDB = programRecords[0].Short_Description__c;
        }
        
        string shortDescriptionJSON = null;
        if (postString[0].shortDescription.length() > 0){
            shortDescriptionJSON = postString[0].shortDescription;                           
        }
        
        if(shortDescriptionDB != shortDescriptionJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Short_Description__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].shortDescription;
            ListFutChange.add(FutChangeRec);
        }
        
        string acadCareerDB = null;
        if (programRecords[0].Academic_Career__c != Null){
            acadCareerDB = programRecords[0].Academic_Career__c;
        }
        
        string acadCareerJSON = null;
        if (postString[0].academicCareer.length() > 0){
            acadCareerJSON = postString[0].academicCareer;                           
        }
        
        if(acadCareerDB != acadCareerJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Academic_Career__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].academicCareer;
            ListFutChange.add(FutChangeRec);
            
        }
        
        string acadCalendarDB = null;
        if(programRecords[0].Academic_Calendar__c != null){
            acadCalendarDB = programRecords[0].Academic_Calendar__c;
        }
        string acadCalendarJSON = null;
        if(postString[0].academicCalendarCode.length() > 0){
            acadCalendarJSON = postString[0].academicCalendarCode;
        }
        if(acadCalendarDB != acadCalendarJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Academic_Calendar__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].academicCalendarCode;
            ListFutChange.add(FutChangeRec);
        }
        
        string acadGroupDB = null;
        if(programRecords[0].Academic_Group__c != null){
            acadGroupDB = programRecords[0].Academic_Group__c;
        }
        string acadGroupJSON = null;
        if(postString[0].academicGroup.length() > 0){
            acadGroupJSON = postString[0].academicGroup;
        }
        if(acadGroupDB != acadGroupJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Academic_Group__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].academicGroup;
            ListFutChange.add(FutChangeRec);
        }
        
        string acadOrgDB = null;
        if(programRecords[0].Academic_Organisation__c != null){
            acadOrgDB = programRecords[0].Academic_Organisation__c;
        }
        string acadOrgJSON = null;
        if(postString[0].academicOrganisation.length() > 0){
            acadOrgJSON = postString[0].academicOrganisation;
        }
        if(acadOrgDB != acadOrgJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('ParentId');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].academicOrganisation;
            ListFutChange.add(FutChangeRec);
        }
        
        string defAcadPlanDB = null;
        if(programRecords[0].Default_Academic_Plan__c != null){
            defAcadPlanDB = programRecords[0].Default_Academic_Plan__c;
        }
        string defAcadPlanJSON = null;
        if(postString[0].defaultAcademicPlan.length() > 0){
            defAcadPlanJSON = postString[0].defaultAcademicPlan;
        }
        if(defAcadPlanDB != defAcadPlanJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Default_Academic_Plan__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].defaultAcademicPlan;
            ListFutChange.add(FutChangeRec);
        }
        
        string defCampusDB = null;
        if(programRecords[0].Default_Campus__c != null){
            defCampusDB = programRecords[0].Default_Campus__r.Name;
        }
        string defCampusJSON = null;
        if(postString[0].defaultCampus.length() > 0){
            defCampusJSON = postString[0].defaultCampus;
        }
        
        if(defCampusDB != defCampusJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Default_Campus__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = defCampusJSON!=null?[SELECT Id FROM Campus__c WHERE Name =: defCampusJSON LIMIT 1].size()>0?[SELECT Id FROM Campus__c WHERE Name =: defCampusJSON LIMIT 1].Id:null:null;
            ListFutChange.add(FutChangeRec);
        }
        
        string ftvDB = null;
        if(programRecords[0].First_Term_Valid__c != null){
            defCampusDB = programRecords[0].First_Term_Valid__c;
        }
        string ftvJSON = null;
        if(postString[0].firstTermValid.length() > 0){
            defCampusJSON = postString[0].firstTermValid;
        }
        if(ftvDB != ftvJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('First_Term_Valid__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].firstTermValid;
            ListFutChange.add(FutChangeRec);
        }
        
        string acadLRDB = null;
        if(programRecords[0].Academic_Level_Rule__c != null){
            acadLRDB = programRecords[0].Academic_Level_Rule__c;
        }
        string acadLRJSON = null;
        if(postString[0].academicLevelRule.length() > 0){
            acadLRJSON = postString[0].academicLevelRule;
        }
        if(acadLRDB != acadLRJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Academic_Level_Rule__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].academicLevelRule;
            ListFutChange.add(FutChangeRec);
        }
        
        string gradeSchemeDB = null;
        if(programRecords[0].Grading_Scheme__c != null){
            gradeSchemeDB = programRecords[0].Grading_Scheme__c;
        }
        string gradeSchemeJSON = null;
        if(postString[0].gradingScheme.length() > 0){
            gradeSchemeJSON = postString[0].gradingScheme;
        }
        if(gradeSchemeDB != gradeSchemeJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Grading_Scheme__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].gradingScheme;
            ListFutChange.add(FutChangeRec);
        }
        
        string gradeBasisDB = null;
        if(programRecords[0].Grading_Basis__c != null){
            gradeBasisDB = programRecords[0].Grading_Basis__c;
        }
        string gradeBasisJSON = null;
        if(postString[0].gradingBasis.length() > 0){
            gradeBasisJSON = postString[0].gradingBasis;
        }
        if(gradeBasisDB != gradeBasisJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Grading_Basis__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].gradingBasis;
            ListFutChange.add(FutChangeRec);
        }
        
        string transScriptLevelDB = null;
        if(programRecords[0].Transcript_Level__c != null){
            transScriptLevelDB = programRecords[0].Transcript_Level__c;
        }
        string transScriptLevelJSON = null;
        if(postString[0].transcriptLevel.length() > 0){
            transScriptLevelJSON = postString[0].transcriptLevel;
        }
        if(transScriptLevelDB != transScriptLevelJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Transcript_Level__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].transcriptLevel;
            ListFutChange.add(FutChangeRec);
        }
        
        string honorAwardDateDB = null;
        if(programRecords[0].Honor_and_Award_Date__c != null){
            honorAwardDateDB = String.valueOf(programRecords[0].Honor_and_Award_Date__c);
        }
        string honorAwardDateJSON = null;
        if(postString[0].transcriptLevel.length() > 0){
            honorAwardDateJSON = postString[0].honorAndAwardDate;
        }
        if(honorAwardDateDB != honorAwardDateJSON){
            FutChangeRec = PROGRAM_REST.createFutChange('Honor_and_Award_Date__c');
            FutChangeRec.Parent_Lookup__c = programRecords[0].Id;
            FutChangeRec.Value__c = postString[0].honorAndAwardDate;
            ListFutChange.add(FutChangeRec);
        }
        
        if(ListFutChange != null && ListFutChange.size() > 0){
            Database.insert(ListFutChange);
        }
    }    
    
    public static Map<String, Schema.DescribeFieldResult> getFieldMetaData(Schema.DescribeSObjectResult dsor, Set<String> fields) {
            
            // the map to be returned with the final data
            Map<String,Schema.DescribeFieldResult> finalMap = 
                new Map<String, Schema.DescribeFieldResult>();
            // map of all fields in the object
            Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
            
            // iterate over the requested fields and get the describe info for each one. 
            // add it to a map with field name as key
            for(String field : fields){
                // skip fields that are not part of the object
                if (objectFields.containsKey(field)) {
                    Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                    // add the results to the map to be returned
                    finalMap.put(field, dr); 
                }
            }
            return finalMap;
        }
    
    public static Future_Change__c createFutChange(String str){
        //request and response variable
        RestRequest request = RestContext.request;
        
        RestResponse response = RestContext.response;
        
        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        
        response.addHeader('Content-Type', 'application/json');
        List < PROGRAM_REST > postString = (List < PROGRAM_REST > ) System.JSON.deserialize(reqStr, List < PROGRAM_REST > .class);
        Future_Change__c planFu = new Future_Change__c();
        Set<String> fields = new Set<String>{str};
            
            Map<String, Schema.DescribeFieldResult> finalMap = 
            PROGRAM_REST.getFieldMetaData(Account.getSObjectType().getDescribe(), fields);
        
        // only print out the 'good' fields
        for (String field : new Set<String>{str}) {
            planFu.API_Field_Name__c = finalMap.get(field).getName();
            planFu.Field__c = finalMap.get(field).getLabel();
        }
        
        for (PROGRAM_REST checkDate : postString) {
            string jsonDate = checkDate.effectiveDate;
            planFu.Effective_Date__c = Date.valueOf(jsonDate);
        }
        
        //planFu.Applied__c = true;
        
        return planFu;
    }
}