@isTest
public class PassportIdentityProviderDeleteReqTest
{
    @TestSetup
    static void makeData(){
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'IDAMIPaaSDeleteAccountError', Config_Value__c = 'iPaaS Delete Account Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'IDAMIPaaSDeleteAccountEndPoint', Config_Value__c = 'https://sys-idm-v1-dev-qa.npe.integration.rmit.edu.au/api/user'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));   
        configDataMaps.add(new Config_Data_Map__c(Name = 'IDAMIpaaSClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));   
        configDataMaps.add(new Config_Data_Map__c(Name = 'IDAMIpaaSClientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));        
        insert configDataMaps;
    }

    static testMethod void testDeleteAccount() {
        //TestDataFactoryUtil.deltaTestSuccess(); 
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new IpassDeleteSuccessMock()); 
        try {
            PassportIdentityProviderDeleteRequest req = new PassportIdentityProviderDeleteRequest();
            req.deleteUser('test@test.com'); 
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
        Test.stopTest();
    }

    static testMethod void testNoMatchDeleteAccount() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AccNotFoundForDeleteMock()); 
        try {
            PassportIdentityProviderDeleteRequest req = new PassportIdentityProviderDeleteRequest();
            req.deleteUser('test@test.com'); 
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
        Test.stopTest();
    }

    static testMethod void testFailDeleteAccount() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DeleteFailureMock()); 
        try {
            PassportIdentityProviderDeleteRequest req = new PassportIdentityProviderDeleteRequest();
            req.deleteUser('test@test.com'); 
            System.assert(false);
        } catch (Exception e) {
            System.assert(true);
        }
        Test.stopTest();
    }
    

    private class DeleteFailureMock implements HttpCalloutMock {
        // implement http mock callout
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            
            String jsonBody = '{"result" : "SUCCESS","provider" : "idm","payload" : { "success" : false,"message" : "Payload error"},"id" : "ID-devqa-50a0814a-af69-4929-a206-4c39bf8e2a9f","code" : "500", "application" : "rmit-system-api-idm"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(500);
            return response;
        }
    }

    private class AccNotFoundForDeleteMock implements HttpCalloutMock {
        // implement http mock callout
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            
            String jsonBody = '{"result" : "SUCCESS","provider" : "idm","payload" : { "success" : false,"message" : "No match was found."},"id" : "ID-devqa-50a0814a-af69-4929-a206-4c39bf8e2a9f","code" : "200", "application" : "rmit-system-api-idm"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }
    
    private class IpassDeleteSuccessMock implements HttpCalloutMock {
        // implement http mock callout
        public HTTPResponse respond(HTTPRequest request)
        {
            // Create a fake response
            
            String jsonBody = '{ "id": "ID-dev-6r87t87587", "result": "success", "code": "200", "application": "rmit-system-api-idam", "provider": "idam", "payload": "" }';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }
}