/**
 * @description This class provides the user validations for creating Passport IDAM users
 */
public class PassportIdentityUserValidator implements UserValidator {
    
    /**
     * @description Validates the email address against the field validation required for IDAM
     * @param email The user's email address
     * @return Returns if the email is valid
     */
    public boolean isEmailValid(String email) {
        if (String.isNotBlank(email) && email.length() <= 46) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @description Validates if the first name mets IDAM's requirements
     * @param firstName The user's first name
     * @return Returns if the name is valid
     */
    public boolean isFirstNameValid(String firstName) {
        return isNameValid(firstName);
	}
	
	
    /**
     * @description Validates if the last name mets IDAM's requirements
     * @param lastName The user's last name
     * @return Returns if the name is valid
     */
    public boolean isLastNameValid(String lastName) {
        return isNameValid(lastName);
    }
	
    
    /**
     * @description Performs basic phone number validation against IDAM's phone number requirements (E164 format)
     * @param phoneNumber The user's phone number
     * @return Returns if the phone number is in a valid format
     */
    public boolean isPhoneNumberValid(String phoneNumber) {
        if (String.isNotBlank(phoneNumber) ) {
            // E164 Regex validation
            return Pattern.matches('^\\+?[1-9]\\d{1,14}$', phoneNumber);
        } else {
            return false;
        }
    }

    
    private boolean isNameValid(String name) {
        if (String.isNotBlank(name) && name.length() <= 30) {
            return true;
        } else {
            return false;
        }
    }
    
}