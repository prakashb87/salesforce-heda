//Wrapper Class for ECB-2686

public class PaymentWrapper {

     @AuraEnabled
     public String Id {get;set;}
     
     @AuraEnabled
     public String  basketNumber{get;set;}
     
     @AuraEnabled
     public Decimal  totalPrice{get;set;}
     
     @AuraEnabled
     public Datetime  dateofPurchase{get;set;}
     
     @AuraEnabled
     public String  attachmentId{get;set;}
     
     @AuraEnabled
     public String paymentId{get;set;}
     
     @AuraEnabled
     public String studentName{get;set;}
     
     @AuraEnabled
     public String studentId{get;set;}
     
     @AuraEnabled
     public String studentAddress{get;set;}
     
     @AuraEnabled
     public String prodConfigIds{get;set;}
     
     @AuraEnabled
     public String receiptNo{get;set;}
     
     @AuraEnabled
     public String pdfURL{get;set;}
     
     
     
    
}