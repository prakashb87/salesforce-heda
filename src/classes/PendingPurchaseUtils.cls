public with sharing class PendingPurchaseUtils {
    @AuraEnabled
    public static csapi_BasketRequestWrapper.AddProductResponse createCartForPendingTransaction(){
        csapi_BasketRequestWrapper.AddProductResponse response = new csapi_BasketRequestWrapper.AddProductResponse();
        Pending_Purchase__c pendingPurchase;
        Id loggedInUserId = UserInfo.getUserId();
        Id loggedInUsersContactId;
        List<User> lstLoggedinUsers = [select ContactId from User where Id =:loggedInUserId];        
        if (!lstLoggedinUsers.isEmpty()) {
            loggedInUsersContactId = lstLoggedinUsers.get(0).ContactId;
            List<Pending_Purchase__c> lstPP = [select Id, Name, Course_Name__c, Course_Offering_Id__c, Course_Type__c, CreatedDate,
                                        Delivery_Mode__c, Product_Line__c, Product_Reference__c, Session_End_Date__c, Session_Start_Date__c, 
                                        Status__c, AddToCart_Url__c, Contact__c 
                                        FROM Pending_Purchase__c
                                        WHERE
                                        Contact__c = :loggedInUsersContactId 
                                        AND Status__c = 'Pending'
                                        ORDER BY CreatedDate DESC LIMIT 1]; 
            System.debug('list-->'+lstPP);
            if (!lstPP.isEmpty()) {
                pendingPurchase = lstPP.get(0);
                System.debug('listInside-->'+pendingPurchase);   
                // response = ProductBasketController.addProducttoBasket(PP.Course_Name__c, PP.Course_Offering_Id__c, PP.Product_Reference__c, PP.Session_End_Date__c,
                               //PP.Session_Start_Date__c, PP.Delivery_Mode__c, PP.Product_Line__c, PP.Course_Type__c, '');
            
                //csapi_BasketRequestWrapper.AddProductResponse response = new csapi_BasketRequestWrapper.AddProductResponse();
                csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
                
                csapi_BasketRequestWrapper.csapi_CourseDetails details =new csapi_BasketRequestWrapper.csapi_CourseDetails();
                details.CourseName = pendingPurchase.Course_Name__c;
                //details.CourseType='21cc Credential';
                details.CourseOfferingId = pendingPurchase.Course_Offering_Id__c;
                details.ProductReference = pendingPurchase.Product_Reference__c;
                //details.SessionStartDate = pendingPurchase.Session_Start_Date__c;
                //details.SessionEndDate = pendingPurchase.Session_End_Date__c;

                if(pendingPurchase.Session_Start_Date__c == 'null' || String.isBlank(pendingPurchase.Session_Start_Date__c)) {
                    details.SessionStartDate = '';
                } else {
                    details.SessionStartDate = pendingPurchase.Session_Start_Date__c;
                }
                if(pendingPurchase.Session_End_Date__c == 'null' || String.isBlank(pendingPurchase.Session_End_Date__c)) {
                    details.SessionEndDate = '';
                } else {
                    details.SessionEndDate = pendingPurchase.Session_End_Date__c;
                }
                //ECB-4424:Tech Story -- Modify AddtoCart Page controller for new products/programs
                // system.debug('courset-->'+CourseType);
                details.courseType = pendingPurchase.Course_Type__c;
                details.deliveryMode = pendingPurchase.Delivery_Mode__c;
                details.productLine = pendingPurchase.Product_Line__c;
                //details.programPlan = programPlan;
                
                system.debug('details:::'+details);
                
                request.Course = details;
                response = csapi_BasketExtension.addProduct(request);
                system.debug('response rec :::'+response);
                // return response;
            }

        }
        return response;   
    }

    @AuraEnabled
    public static String recordPendingTransaction(String contactId, String url){
        String status = 'Fail';
        //List<Pending_Purchase__c> lstPP = new List<Pending_Purchase__c>();
        Pending_Purchase__c pendingPurchase = new Pending_Purchase__c();
        Map<String, String> mapQueryParamAndValue = new Map<String, String>();
        if(contactId != null && url != null){
            //PP.Contact__c = contactId;
            pendingPurchase.AddToCart_Url__c = url;
            url = url.substringAfter('?'); //Get the query strings in the URL after '?'
            List<String> queryParams = url.split('&', -2); // Get a list of individual param and its value Eg. productLine=RMIT Online
            for(String eachParam : queryParams){
                mapQueryParamAndValue.put(eachParam.split('=', 2).get(0), eachParam.split('=', 2).get(1));
            }

            if(!mapQueryParamAndValue.isEmpty()){
                
                pendingPurchase.Contact__c = contactId;
                if(mapQueryParamAndValue.containsKey('productLine')){ 
                    pendingPurchase.Product_Line__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('productLine'),'UTF-8'); 
                }
                if(mapQueryParamAndValue.containsKey('PRDref')){                    
                    pendingPurchase.Product_Reference__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('PRDref'),'UTF-8'); 
                }
                if(mapQueryParamAndValue.containsKey('SEnddate')){                    
                    pendingPurchase.Session_End_Date__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('SEnddate'),'UTF-8'); 
                }
                if(mapQueryParamAndValue.containsKey('SStartdate')){                    
                    pendingPurchase.Session_Start_Date__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('SStartdate'),'UTF-8'); 
                }
                if(mapQueryParamAndValue.containsKey('deliveryMode')){                    
                    pendingPurchase.Delivery_Mode__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('deliveryMode'),'UTF-8'); 
                }
                if(mapQueryParamAndValue.containsKey('courseoffrID')){                   
                    pendingPurchase.Course_Offering_Id__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('courseoffrID'),'UTF-8'); 
                }
                if(mapQueryParamAndValue.containsKey('coursename')){                  
                    pendingPurchase.Course_Name__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('coursename'),'UTF-8'); 
                }
                if(mapQueryParamAndValue.containsKey('courseType')){
                    pendingPurchase.Course_Type__c = EncodingUtil.URLDECODE(mapQueryParamAndValue.get('courseType'),'UTF-8'); 
                }
                insert pendingPurchase;
                status = 'Success';
            }
        }


        return status;
    }   
    
}