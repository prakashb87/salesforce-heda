/*****************************************************************************************************
 *** @Class             : ProductCatalogRefreshBatchApi
 *** @Author            : Pawan Srivastava 
 *** @Requirement       : Batch Class to call ipass End Points 
 *** @Created date      : 24/09/2018
 *** @JIRA ID           : ECB-4489
 *****************************************************************************************************/ 
/*****************************************************************************************************
 *** @About Class
 *** This class is used to send json to Ipass API. 
 *****************************************************************************************************/ 
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class ProductCatalogRefreshBatchApi implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts , Schedulable{  
   
   global Boolean isAnyDataModified;
   global Database.QueryLocator start(Database.BatchableContext bc) {
   
        Set<String> priceitemIds = new Set<String>();
        priceitemIds = csapi_BasketExtension.getActiveProductswithCustomSetting();
        String sObjectQuery = 'SELECT Id, Name FROM cspmb__Price_Item__c WHERE ID IN :priceitemIds';
        
        //Checking if any data modified then invoking Ipass End Point
        if (priceitemIds!= null && priceitemIds.size() > 0) { 
           isAnyDataModified = true;           
        }
        else {
           isAnyDataModified = false;   
        }       
        System.Debug('sObjectQuery-->'+sObjectQuery);
        return Database.getQueryLocator(sObjectQuery);
        
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        System.Debug('scope count-->'+scope.size()+'isAnyDataModified -->' + isAnyDataModified); 
        if (isAnyDataModified) {
          iPassApiCall(); 
        }            
    }
    
    global void finish(Database.BatchableContext bc) 
    {
        System.debug('ProductCatalogRefreshBatchApi batch completed');
    }
    
    global void execute(SchedulableContext sc) {
        ProductCatalogRefreshBatchApi productCatalogbatch = new ProductCatalogRefreshBatchApi();
        Database.executeBatch(productCatalogbatch);
    }
    
    global void iPassApiCall() {               
        
        String requestJSONString; 
        Boolean deltaflag = Boolean.valueof(ConfigDataMapController.getCustomSettingValue('ProductRefreshDeltaFlag'));
              
        CS_Commercial_Product_Refresh_Setting__c productRefreshSetting = CS_Commercial_Product_Refresh_Setting__c.getOrgDefaults();
        if (productRefreshSetting.Last_Modified_Date_Before__c != null) {
            requestJSONString = JSON.serialize(new ProductCatalogRefreshBatch(ConfigDataMapController.getCustomSettingValue('ProductRefreshObjectName'), new ProductCatalogRefreshBatchAPIKey(String.valueof(deltaflag))));  
            requestJSONString = '[' + requestJSONString + ']';
        } else {
            requestJSONString = JSON.serialize(new ProductCatalogRefreshBatch('price', new ProductCatalogRefreshBatchAPIKey(String.valueof(!deltaflag))));
            requestJSONString = '[' + requestJSONString + ']';
        }
        System.debug('requestJSONString -->'+requestJSONString);
        try {      
            String endpoint = ConfigDataMapController.getCustomSettingValue('IpassAPI'); 
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint);
            req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IpassClientID')); 
            req.setHeader('client_secret',ConfigDataMapController.getCustomSettingValue('IpassCientSecret')); 
            req.setHeader('Content-Type','application/json');
            req.setMethod('POST');
            req.setbody(requestJSONString);
    
            Http http = new Http();
            HttpResponse response = http.send(req);          
            System.debug('response---------------'+response.getbody()); 
            
            //Creating RMErrorLog incase iPaaS returns an error
            if(response.getStatusCode() != 200)
            {
               insert(new RMErrLog__c(Error_Cause__c = ConfigDataMapController.getCustomSettingValue('IpassErrorCause'),Error_Message__c = ConfigDataMapController.getCustomSettingValue('ProductRefreshDeltaFlag'),Error_Type__c = ConfigDataMapController.getCustomSettingValue('IpassErrorType'), Error_Payload__c = ConfigDataMapController.getCustomSettingValue('IpassErrorPayLoad'),Method_Name__c = 'iPassApiCall',Class_Name__c='ProductCatalogRefreshBatchApi'));
            }
        }
        Catch (Exception ex) {
           System.debug('response---------------'+ex.getMessage()); 
        }
    }
    
    global class ProductCatalogRefreshBatch {
        
        @AuraEnabled
        public String ObjectName { get; set; }  
       
        @AuraEnabled
        public productCatalogRefreshBatchAPIKey key{ get; set; }  
        
        public productCatalogRefreshBatch (String obj_Name, productCatalogRefreshBatchAPIKey iPass_Key){
            ObjectName = obj_Name;  
            key = iPass_Key;
        }
    }
    
    global class ProductCatalogRefreshBatchAPIKey {
                
        @AuraEnabled
        public String delta; 
        
         public productCatalogRefreshBatchAPIKey(String strDelta) {
            delta = strDelta;
         }     
       
    }
    
}