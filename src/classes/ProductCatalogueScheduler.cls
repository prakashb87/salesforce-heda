/*****************************************************************************************************
 *** @Class             : ProductCatalogueScheduler
 *** @Author            : Praneet Vig
 *** @Requirement       : scheduler to schedule Course ID 
 *** @Created date      : 21/02/2018
 *** @JIRA ID           : ECB-30
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used as a scheduler to schedule Course ID to Ipass API. It works when we schedule using schedule apex.
 *****************************************************************************************************/ 
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class ProductCatalogueScheduler implements Schedulable { 
   global void execute(SchedulableContext SC) {  
        //Calling future class method
        CourseDeltaDataLoad.sendCourseAsync();  
        System.debug('Class Scheduled+++'+ SC);
   }
}