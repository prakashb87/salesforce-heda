/*********************************************************************************
 *** @TriggerTestClass  : ProgramEnrollTriggerTest
 *** @Author		    : Shubham Singh
 *** @Requirement     	: To Test ProgramEnrollTrigger
 *** @Created date    	: 17/11/2017
 **********************************************************************************/
/************************************************************************************
 *** @About Class
 *** This class is a Test class for ProgramEnrollTrigger.It is Tested on four events
 *** (after insert,after update, after delete, after undelete) of ProgramEnrollment. 
 ***********************************************************************************/
@isTest
public class ProgramEnrollTriggerTest {
    //Testing all the four Events
    @isTest static void countofPETestInsert() {
        test.startTest();
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        Contact con = new Contact(lastname = 'Test Contact');
        insert con;
        list < hed__Program_Enrollment__c > progList = new list < hed__Program_Enrollment__c > ();
        hed__Program_Enrollment__c triggerRecord = new hed__Program_Enrollment__c(hed__Contact__c = con.id, Program_Action__c = 'ACTV', Action_Date__c = Date.newInstance(2016, 12, 9), hed__Graduation_Year__c = '2015');
        triggerRecord.hed__Account__c = acc.id;
        progList.add(triggerRecord);

        hed__Program_Enrollment__c triggerRecord1 = new hed__Program_Enrollment__c(hed__Contact__c = con.id, Program_Action__c = 'DISC', Action_Date__c = Date.newInstance(2015, 10, 8), hed__Graduation_Year__c = '2010');
        triggerRecord1.hed__Account__c = acc.id;
        progList.add(triggerRecord1);

        insert progList;
        System.assertNotEquals(con.Number_of_active_program_enrollments__c, 0);

        triggerRecord.Program_Action__c = 'COMP';
        Update triggerRecord;
        System.assertNotEquals(con.Program_Action__c,'DISC');
        Delete triggerRecord;
        Delete triggerRecord1;
        Undelete triggerRecord1;
        test.stopTest();
    }

}