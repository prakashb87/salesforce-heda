/*********************************************************************************
 *** @ClassName         : ProgramEnrolmentService
 *** @Author            : Shubham Singh 
 *** @Requirement       : ProgramEnrolmentService class for methods related to web service version 1.1
 *** @Created date      : 17/03/2019
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 07/05/2019
 **********************************************************************************/
public with sharing class ProgramEnrolmentService {
    //wrapper class to create variables to store the respective values     
    public class ProgramEnrolmentServiceWrapper {
        public Boolean allExist = true;
        public Set < String > notFound = new Set < String > ();
        public Map < String, Id > programEnrolStringMap = new Map < String, Id > ();
        public Set < String > acdemicCareer = new Set < String > ();
        public Set < String > studentId = new Set < String > ();
        public Set < String > studentCareerNumber = new Set < String > ();
        public Set < String > academicProgram = new Set < String > ();
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * check for program enrolment record, if yes then create map with respective values
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    getProgramEnrolment
     * @param     ProgramEnrolmentService.ProgramEnrolmentServiceWrapper wrapper
     * @return    wrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static ProgramEnrolmentServiceWrapper getProgramEnrolment(ProgramEnrolmentService.ProgramEnrolmentServiceWrapper wrapper) {
        List<AggregateResult > resultAggregate = checkAllExistAcademicCareerandReturnAggregate(wrapper.acdemicCareer);
        wrapper.notFound = wrapper.acdemicCareer.clone();
        for (AggregateResult loopResult  : resultAggregate) {
            wrapper.notFound.remove(String.valueOf(loopResult.get('Career__c')));
        }
        if (!wrapper.notFound.isEmpty()) {
            wrapper.allExist = false;
        }
        List < hed__Program_Enrollment__c > listProgramEnrol = new List < hed__Program_Enrollment__c > ();
        listProgramEnrol = queryprogramEnrolment(wrapper);
        if (listProgramEnrol != null) {
            for (hed__Program_Enrollment__c loopVar : listProgramEnrol) {
                String stringVar = loopVar.Institution__r.Name + String.valueOf(loopVar.hed__Contact__r.Student_ID__c) + String.ValueOf(loopVar.Academic_Program__c) + String.valueOf(loopvar.Career__c) + String.ValueOf(loopVar.Student_Career_Number__c);
                wrapper.programEnrolStringMap.put(stringVar, loopVar.Id);
            }
        }
        system.debug('wrapper ' + wrapper);
        return wrapper;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Check for ProgramEnrolment record with their respective career otherwise add in notFound set
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    checkAllExistAcademicCareerandReturnAggregate
     * @param     Set<String> career
     * @return    List<aggregateresult >
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static List<aggregateresult > checkAllExistAcademicCareerandReturnAggregate(Set < String > career) {
        List<AggregateResult > resultAggregate = new List<AggregateResult>();
        if (Schema.sObjectType.hed__Program_Enrollment__c.fields.Career__c.isAccessible()) {
            resultAggregate = [SELECT Count (ID), Career__c FROM hed__Program_Enrollment__c where career__c IN : career GROUP BY Career__c];
        }
        return resultAggregate;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Query ProgramEnrolment Records
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    queryprogramEnrolment
     * @param     Set<String> studentId,Set<String> academicProgram,Set<String> career,Set<String> studentCareerNumber
     * @return    List<hed__Program_Enrollment__c>
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static List < hed__Program_Enrollment__c > queryprogramEnrolment(ProgramEnrolmentServiceWrapper wrapper) {
        //Get programEnrollment and order by effective date and effective sequence
        if (Schema.sObjectType.hed__Program_Enrollment__c.fields.Academic_Program__c.isUpdateable()) {
            return [SELECT Id, Institution__r.Name, hed__Contact__r.Student_ID__c, Academic_Program__c, Career__c, Student_Career_Number__c FROM hed__Program_Enrollment__c WHERE Institution__r.Name = 'RMITU'
                AND hed__Contact__r.Student_ID__c IN: wrapper.studentId AND Academic_Program__c IN: wrapper.academicProgram AND Career__c IN: wrapper.acdemicCareer AND Student_Career_Number__c IN: wrapper.studentCareerNumber ORDER BY Effective_Date__c, Effective_Sequence__c DESC
            ];
        }
        return null;
    }
}