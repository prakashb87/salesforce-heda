/********************************************************************************************
Purpose: View Project Funding Detail- Budget and expenses view helper class
History:
Created by Ankit Bhagat  on 10/10/2018 for RPORW-135 
Modified By Subhajit for comments addition and for RPORW-282 Budget code added on 10/12/2018
*********************************************************************************************/
public class ProjectFundingDetailHelper {
    
    
    /*****************************************************************************************
    // JIRA No      :  RPORW-135
    // SPRINT       :  SPRINT-5
    // Purpose      :  View Project Funding Detail- expenses view
    // Parameters   :  String ProjectId
    // Developer    :  Ankit Bhagat 
    // Created Date :  10/10/2018                 
    //************************************************************************************/ 
    @AuraEnabled    
    public static ProjectFundingWrapper.ProjectFundingExpensesTable getProjectFundingTable(String projectId, string selectedYear){
        
        List<ProjectSAPFunding__c> projectFundingList = new List<ProjectSAPFunding__c>();
        ProjectFundingWrapper.ProjectFundingExpensesTable projectFundingExpensesTable  = new ProjectFundingWrapper.ProjectFundingExpensesTable();
        List<ProjectFundingWrapper.ProjectFundingExpensesDetails> projectFundingExpensesList = new List<ProjectFundingWrapper.ProjectFundingExpensesDetails>();
        decimal totalIncome = 0.0 ;
        decimal totalSalariesOncosts = 0.0 ;
        decimal salariesOncosts = 0.0 ;
        decimal totalOtherOperatingExpenses = 0.0 ;
        decimal totalcapital = 0.0;
        decimal totalFundingExpense = 0.0 ;
        decimal remainingBalance = 0.0 ;
        datetime lastRefreshedDate ;
        
         
       AggregateResult[] fundingAggregateResultLst;
       
       if(selectedYear != null && !selectedYear.equalsIgnoreCase('All'))
       {
	    if (!Schema.sObjectType.ProjectSAPFunding__c.fields.ProjectCode__c.isAccessible()){
                system.debug('ProjectCode__c ==> is not accessible');
        }
       	fundingAggregateResultLst= [Select CostElementDescription__c, sum(Income__c) Income__c, sum(Salaries_and_Oncosts__c) Salaries_and_Oncosts__c,
                                        sum(Other_Operating_Expenses__c) Other_Operating_Expenses__c, sum(Capital__c) Capital__c, sum(Total_Expense__c) Total_Expense__c, sum(Amount__c) Amount__c
                                         From ProjectSAPFunding__c where ProjectCode__c= :projectId 
                                         AND
                                         (Income__c <>0 or Salaries_and_Oncosts__c <>0 or Other_Operating_Expenses__c<>0 or Capital__c <>0 )
                                         AND fiscal_year__c =:integer.valueOf(selectedYear)
                                          group by CostElementDescription__c order by sum(Income__c) desc NULLS  last ];
       }
       else{
			if (!Schema.sObjectType.ProjectSAPFunding__c.fields.ProjectCode__c.isAccessible()){
					system.debug('ProjectCode__c ==> is not accessible');
			}
       		fundingAggregateResultLst= [Select CostElementDescription__c, sum(Income__c) Income__c, sum(Salaries_and_Oncosts__c) Salaries_and_Oncosts__c,
                                        sum(Other_Operating_Expenses__c) Other_Operating_Expenses__c, sum(Capital__c) Capital__c, sum(Total_Expense__c) Total_Expense__c, sum(Amount__c) Amount__c
                                         From ProjectSAPFunding__c where ProjectCode__c= :projectId AND
                                         (Income__c <>0 or Salaries_and_Oncosts__c <>0 or Other_Operating_Expenses__c<>0 or Capital__c <>0 )
                                         
                                          group by CostElementDescription__c order by sum(Income__c) desc NULLS  last ];
       }
       
        
		if (!Schema.sObjectType.ProjectSAPFunding__c.fields.Lastmodifieddate.isAccessible()){
                system.debug('Lastmodifieddate ==> is not accessible');
        }
		projectFundingList = [Select id,  Lastmodifieddate  From ProjectSAPFunding__c order by Lastmodifieddate desc limit 1];
		lastRefreshedDate=projectFundingList.size()==1?projectFundingList[0].Lastmodifieddate:null;
		system.debug(fundingAggregateResultLst);
		  
		
		for(AggregateResult aggFundingDetail: fundingAggregateResultLst){
		
			  if(decimal.valueOf(aggFundingDetail.get('Income__c')+'') <>0 ||decimal.valueOf(aggFundingDetail.get('Salaries_and_Oncosts__c')+'') <>0
               || decimal.valueOf(aggFundingDetail.get('Other_Operating_Expenses__c')+'') <>0||decimal.valueOf(aggFundingDetail.get('Capital__c')+'')<>0) {
     
      
                    ProjectFundingWrapper.ProjectFundingExpensesDetails fundingDetails = new ProjectFundingWrapper.ProjectFundingExpensesDetails(aggFundingDetail);
                    totalIncome+= fundingDetails.income;
                    totalSalariesOncosts+= fundingDetails.salariesOncosts;
                    salariesOncosts+=fundingDetails.salariesOncosts;
                    totalOtherOperatingExpenses+= fundingDetails.otherOperatingExpenses;
                    totalcapital+= fundingDetails.capital; 
                    projectFundingExpensesList.add(fundingDetails); 
               } 
		}
		projectFundingExpensesTable.projectFundingList = projectFundingExpensesList;
        projectFundingExpensesTable.totalIncome = totalIncome;
        projectFundingExpensesTable.totalSalariesOncosts = totalSalariesOncosts;
        projectFundingExpensesTable.salariesOncosts = salariesOncosts;
        projectFundingExpensesTable.totalOtherOperatingExpenses = totalOtherOperatingExpenses;
        projectFundingExpensesTable.totalcapital = totalcapital;
        projectFundingExpensesTable.totalFundingExpense = totalSalariesOncosts + totalOtherOperatingExpenses + totalcapital;
        projectFundingExpensesTable.remainingBalance = totalIncome - projectFundingExpensesTable.totalFundingExpense;
        projectFundingExpensesTable.lastRefreshedDate=lastRefreshedDate; 
		

        return projectFundingExpensesTable;
    }
    
    
     /*****************************************************************************************
    // JIRA No      :  RPORW-282
    // SPRINT       :  SPRINT-5
    // Purpose      :  View Project Funding Detail- Budget view
    // Parameters   :  String ProjectId
    // Developer    :  Subhajit 
    // Created Date :  10/12/2018                 
    //************************************************************************************/ 
    @AuraEnabled    
    public static  list<ProjectFundingWrapper.ProjectFundingBudgetDetails> getProjectFundingBudgetDetails(string projectId) {
    	
    	   list<ProjectFundingWrapper.ProjectFundingBudgetDetails> lstProjectFundingBudgetDetails  
            = new list<ProjectFundingWrapper.ProjectFundingBudgetDetails>();
               map<string,ProjectFundingWrapper.ProjectFundingBudgetDetails> mapProjectFundingBudgetWrapper 
            = new  map<string,ProjectFundingWrapper.ProjectFundingBudgetDetails>(); 
            mapProjectFundingBudgetWrapper = getProjectFundingDetailsMap(projectId);
    	  //get the final list of budget wrapper
            if(mapProjectFundingBudgetWrapper!=null) // Added in code review
            {
                for(string schemeId:mapProjectFundingBudgetWrapper.keyset()){
                	
                    if(mapProjectFundingBudgetWrapper.get(schemeId)!=null){
                        ProjectFundingWrapper.ProjectFundingBudgetDetails aProjectBudgetFunding 
                            = mapProjectFundingBudgetWrapper.get(schemeId);
                        
                        lstProjectFundingBudgetDetails.add(aProjectBudgetFunding);
                    }
                }
            }
          return  lstProjectFundingBudgetDetails;
    }
    /*****************************************************************************************
    // JIRA No      :  RPORW-282
    // SPRINT       :  SPRINT-5
    // Purpose      :  View Project Funding Detail- Budget view
    // Parameters   :  String ProjectId
    // Developer    :  Subhajit 
    // Created Date :  10/12/2018                 
    //************************************************************************************/ 
    
    public static  map<string,ProjectFundingWrapper.ProjectFundingBudgetDetails> getProjectFundingDetailsMap(string projectId){
        
        list<ProjectFundingWrapper.ProjectFundingBudgetDetails> lstProjectFundingBudgetDetails  
            = new list<ProjectFundingWrapper.ProjectFundingBudgetDetails>();
        list<ProjectFundingWrapper.projectFundingBudgetList> lstProjectFundingBudgetListView  
            = new list<ProjectFundingWrapper.projectFundingBudgetList> ();
        
        ProjectFundingWrapper.ProjectFundingBudgetDetails projectFundingBudgetDetails  
            = new ProjectFundingWrapper.ProjectFundingBudgetDetails();
        ProjectFundingWrapper.projectFundingBudgetList projectFundingBudgetListView  
            = new ProjectFundingWrapper.projectFundingBudgetList();
        
        map<string,ProjectFundingWrapper.ProjectFundingBudgetDetails> mapProjectFundingBudgetWrapper 
            = new  map<string,ProjectFundingWrapper.ProjectFundingBudgetDetails>(); 
        
        Integer totalFundingAppliedBudget = 0;
        Integer totalFundingReceivedBudget = 0 ;
		
		if (!Schema.sObjectType.Research_Project_Funding__c.fields.ResearcherPortalProject__c.isAccessible()){
                system.debug('ResearcherPortalProject__c==> is not accessible');
        }
        
        List<Research_Project_Funding__c> projectFundingBudgeList = [ SELECT id,  Amount_Applied__c, Project_Scheme__c,Amount_Received__c, Budget_Year__c, 
                                                                     Scheme_Description__c,ResearcherPortalProject__c 
                                                                     FROM Research_Project_Funding__c 
                                                                     WHERE ResearcherPortalProject__c =: projectId and Project_Scheme__c!=null order by Project_Scheme__c,Budget_Year__c asc];
        
       // if(projectFundingBudgeList!=null && !projectFundingBudgeList.isEmpty())// for list, size check not required.
        
            for(Research_Project_Funding__c eachRec : projectFundingBudgeList)
            {
                 
                    projectFundingBudgetDetails = new ProjectFundingWrapper.ProjectFundingBudgetDetails();
                    lstProjectFundingBudgetListView  
                        = new list<ProjectFundingWrapper.projectFundingBudgetList> ();
                    //create unique map id:
                    string mapUniqueId= string.valueof(eachRec.Project_Scheme__c)+eachRec.ResearcherPortalProject__c;
                    system.debug('####'+mapUniqueId);
                    //get the funding budget details for that scheme description
                    if(mapProjectFundingBudgetWrapper.get(mapUniqueId)!=null) 
                   {
                   	 projectFundingBudgetDetails= mapProjectFundingBudgetWrapper.get(mapUniqueId);
                   }
                    //Assign the schema description 
                    projectFundingBudgetDetails.fundingSchemeDescription=string.valueof(eachRec.Scheme_Description__c);
                    projectFundingBudgetDetails.projectId=eachRec.ResearcherPortalProject__c ;
                    
                    // get the total applied amount and add the current value to it.
                    totalFundingAppliedBudget =0;
                    if(projectFundingBudgetDetails.totalFundingAppliedBudget!=null)
                    {
                    	totalFundingAppliedBudget= projectFundingBudgetDetails.totalFundingAppliedBudget;
                    } 	
                    
                    totalFundingAppliedBudget+= integer.valueof(eachRec.Amount_Applied__c  !=null ? eachRec.Amount_Applied__c:0);
                    projectFundingBudgetDetails.totalFundingAppliedBudget= totalFundingAppliedBudget;                  
                    
                    // get the total applied amount and add the current value to it.
                    totalFundingReceivedBudget =0;
                  
                    if(projectFundingBudgetDetails.totalFundingReceivedBudget!=null)
                    {
                    	totalFundingReceivedBudget= projectFundingBudgetDetails.totalFundingReceivedBudget;
                    } 	
                    
                    totalFundingReceivedBudget+= integer.valueof(eachRec.Amount_Received__c  !=null ? eachRec.Amount_Received__c:0);
                  
                    projectFundingBudgetDetails.totalFundingReceivedBudget= totalFundingReceivedBudget;                  
                    
                    //get the budget list view for each year
                    if(projectFundingBudgetDetails.projectFundingBudgetList!=null)
                    {
                    	lstProjectFundingBudgetListView = projectFundingBudgetDetails.projectFundingBudgetList;
                    }	
                    
                    projectFundingBudgetListView = new ProjectFundingWrapper.projectFundingBudgetList(eachRec);
                    lstProjectFundingBudgetListView.add(projectFundingBudgetListView);
                    
                    projectFundingBudgetDetails.projectFundingBudgetList=lstProjectFundingBudgetListView;
                    
                    //assign to map (to add the value for next record if same fund scheme)
                    mapProjectFundingBudgetWrapper.put(mapUniqueId,projectFundingBudgetDetails);
               }
            
        
         
        return mapProjectFundingBudgetWrapper;
    } 
}