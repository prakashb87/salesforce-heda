/*******************************************
Purpose: Helper class of ProjectSAPFundingTrigger & To find record Ids to fill lookup fields
History:
Created by Khushman Deomurari on 10/10/2018 for RPORW-284: Integration - BI to SF
*******************************************/ 
public without sharing class ProjectSAPFundingTriggerHelper{

    /************************************************************************************
    // Purpose      :  To call method to populate lookups or any other method required to be called when Project_SAP_Funding__c records will be inserted
    // Parameters   :  List newRecords	   	
    // Developer    :  Khushman Deomurari
    // Created Date :  10/10/2018                 
    //***********************************************************************************/
    public static void onInsert(List<ProjectSAPFunding__c> newRecords){
        populateLookupsonNewRecords(newRecords);
    }

    /************************************************************************************
    // Purpose      :  To populate lookups for fields based on values of fields
    				   WBSPersonResponsibleENumber__c, Lead_Chief_Investigator_ENumber__c, ProjectCodeSAPWBSNum__c
    // Parameters   :  List newRecords	   	
    // Developer    :  Khushman Deomurari
    // Created Date :  10/10/2018                 
    //***********************************************************************************/
    public static void populateLookupsonNewRecords(List<ProjectSAPFunding__c> newRecords){
        System.debug('@@@newRecords==>'+JSON.serializePretty(newRecords));
        Set<String> eNumberValues=new Set<String>();
        Set<String> wbsValues=new Set<String>();
        Map<String, Set<ProjectSAPFunding__c>> eNumbersToFundingRecords=new Map<String, Set<ProjectSAPFunding__c>>();
        Map<String, Set<ProjectSAPFunding__c>> wbsToFundingRecords=new Map<String, Set<ProjectSAPFunding__c>>();
        
        for (ProjectSAPFunding__c rec: newRecords){
        	setUpValues(rec, eNumberValues, eNumbersToFundingRecords); //Moved below code to this method for PMD Fix
        	
        	/*
                if (rec.WBSPersonResponsibleENumber__c!=null){
                    eNumberValues.add(rec.WBSPersonResponsibleENumber__c);
                    eNumberValues.add('e'+rec.WBSPersonResponsibleENumber__c);
                    eNumberValues.add('E'+rec.WBSPersonResponsibleENumber__c);
                if (!eNumbersToFundingRecords.keySet().contains(rec.WBSPersonResponsibleENumber__c)){
                    eNumbersToFundingRecords.put(rec.WBSPersonResponsibleENumber__c, new Set<ProjectSAPFunding__c>());
                }
                    eNumbersToFundingRecords.get(rec.WBSPersonResponsibleENumber__c).add(rec);
            }
            
            if (rec.Lead_Chief_Investigator_ENumber__c!=null){
                eNumberValues.add(rec.Lead_Chief_Investigator_ENumber__c);
                eNumberValues.add('e'+rec.Lead_Chief_Investigator_ENumber__c);
                eNumberValues.add('E'+rec.Lead_Chief_Investigator_ENumber__c);
                if (!eNumbersToFundingRecords.keySet().contains(rec.Lead_Chief_Investigator_ENumber__c)){
                    eNumbersToFundingRecords.put(rec.Lead_Chief_Investigator_ENumber__c, new Set<ProjectSAPFunding__c>());
                }
                    eNumbersToFundingRecords.get(rec.Lead_Chief_Investigator_ENumber__c).add(rec);
            }*/

            if (rec.ProjectCodeSAPWBSNum__c!=null){
            	setUpWBSToFundingRecords(rec, wbsToFundingRecords, wbsValues); //Moved below code to this method for PMD Fix
            	/*
                wbsValues.add(rec.ProjectCodeSAPWBSNum__c);
                if (!wbsToFundingRecords.keySet().contains(rec.ProjectCodeSAPWBSNum__c)){
                    wbsToFundingRecords.put(rec.ProjectCodeSAPWBSNum__c, new Set<ProjectSAPFunding__c>());
                }
                    wbsToFundingRecords.get(rec.ProjectCodeSAPWBSNum__c).add(rec);
                */
            }
        }
        
        List<Contact> contacsFound=(List<Contact>)findRecords('Contact', 'Enumber__c', eNumberValues);
        List<ResearcherPortalProject__c> projectsFound=(List<ResearcherPortalProject__c>)findRecords('ResearcherPortalProject__c', 'SAP_WBS_NUM__c', wbsValues);
        system.debug('@@@@ProjectFound'+projectsFound);
        
        for (Contact contactRec: contacsFound){
        	
                
                Set<ProjectSAPFunding__c> fundingRecs=new Set<ProjectSAPFunding__c>();
				String searchStr=setupValues2(contactRec, fundingRecs, eNumbersToFundingRecords); //Moved below code to this method for PMD Fix
/*
                if (eNumbersToFundingRecords.keySet().contains(contactRec.Enumber__c)) {
                    searchStr=contactRec.Enumber__c;
                    fundingRecs.addAll(eNumbersToFundingRecords.get(contactRec.Enumber__c));
                }
                if (eNumbersToFundingRecords.keySet().contains(contactRec.Enumber__c.replace('e',''))){
                    searchStr=contactRec.Enumber__c.replace('e','');
                    fundingRecs.addAll(eNumbersToFundingRecords.get(contactRec.Enumber__c.replace('e','')));
                }
                if (eNumbersToFundingRecords.keySet().contains(contactRec.Enumber__c.replace('E',''))){
                    searchStr=contactRec.Enumber__c.replace('E','');
                    fundingRecs.addAll(eNumbersToFundingRecords.get(contactRec.Enumber__c.replace('E','')));
                }
*/
                    for (ProjectSAPFunding__c rec: fundingRecs){
						populateFields(rec, contactRec); //Moved below code to this method for PMD Fix
/*
                        if (rec.WBSPersonResponsibleENumber__c.replace('e','').replace('E','').equalsIgnoreCase(contactRec.Enumber__c.replace('e','').replace('E',''))){
                            rec.WBSPersonResponsibleCode__c=contactRec.id;
                        }
                        if (rec.Lead_Chief_Investigator_ENumber__c.replace('e','').replace('E','').equalsIgnoreCase(contactRec.Enumber__c.replace('e','').replace('E',''))){
                            rec.LeadChiefInvestigatorCode__c=contactRec.id;
                        }
*/                        
                    }
        }
            
            for (ResearcherPortalProject__c projectRec: projectsFound){
                if (wbsToFundingRecords.keySet().contains(projectRec.SAP_WBS_NUM__c)){
                    for (ProjectSAPFunding__c rec: wbsToFundingRecords.get(projectRec.SAP_WBS_NUM__c)){
                        rec.ProjectCode__c=projectRec.id;
                        
                    }
                }
            }
    }

	public static void setUpValues(ProjectSAPFunding__c rec, Set<String> eNumberValues, Map<String, Set<ProjectSAPFunding__c>> eNumbersToFundingRecords){
		
		
	            if (rec.WBSPersonResponsibleENumber__c!=null){
	                    eNumberValues.add(rec.WBSPersonResponsibleENumber__c);
	                    eNumberValues.add('e'+rec.WBSPersonResponsibleENumber__c);
	                    eNumberValues.add('E'+rec.WBSPersonResponsibleENumber__c);
	                if (!eNumbersToFundingRecords.keySet().contains(rec.WBSPersonResponsibleENumber__c)){
	                    eNumbersToFundingRecords.put(rec.WBSPersonResponsibleENumber__c, new Set<ProjectSAPFunding__c>());
	                }
	                    eNumbersToFundingRecords.get(rec.WBSPersonResponsibleENumber__c).add(rec);
	            }
	            
	            if (rec.Lead_Chief_Investigator_ENumber__c!=null){
	                eNumberValues.add(rec.Lead_Chief_Investigator_ENumber__c);
	                eNumberValues.add('e'+rec.Lead_Chief_Investigator_ENumber__c);
	                eNumberValues.add('E'+rec.Lead_Chief_Investigator_ENumber__c);
	                if (!eNumbersToFundingRecords.keySet().contains(rec.Lead_Chief_Investigator_ENumber__c)){
	                    eNumbersToFundingRecords.put(rec.Lead_Chief_Investigator_ENumber__c, new Set<ProjectSAPFunding__c>());
	                }
	                    eNumbersToFundingRecords.get(rec.Lead_Chief_Investigator_ENumber__c).add(rec);
	            }
	}

	//Khushman: Created for PMD Fix	
	public static String setupValues2(Contact contactRec, Set<ProjectSAPFunding__c> fundingRecs, Map<String, Set<ProjectSAPFunding__c>> eNumbersToFundingRecords){
				String searchStr=null;
                if (eNumbersToFundingRecords.keySet().contains(contactRec.Enumber__c)) {
                    searchStr=contactRec.Enumber__c;
                    fundingRecs.addAll(eNumbersToFundingRecords.get(contactRec.Enumber__c));
                }
                if (eNumbersToFundingRecords.keySet().contains(contactRec.Enumber__c.replace('e',''))){
                    searchStr=contactRec.Enumber__c.replace('e','');
                    fundingRecs.addAll(eNumbersToFundingRecords.get(contactRec.Enumber__c.replace('e','')));
                }
                if (eNumbersToFundingRecords.keySet().contains(contactRec.Enumber__c.replace('E',''))){
                    searchStr=contactRec.Enumber__c.replace('E','');
                    fundingRecs.addAll(eNumbersToFundingRecords.get(contactRec.Enumber__c.replace('E','')));
                }
                return searchStr;
	}

	//Khushman: Created for PMD Fix
	public static void setUpWBSToFundingRecords(ProjectSAPFunding__c rec, Map<String, Set<ProjectSAPFunding__c>> wbsToFundingRecords, Set<String> wbsValues){
                wbsValues.add(rec.ProjectCodeSAPWBSNum__c);
                if (!wbsToFundingRecords.keySet().contains(rec.ProjectCodeSAPWBSNum__c)){
                    wbsToFundingRecords.put(rec.ProjectCodeSAPWBSNum__c, new Set<ProjectSAPFunding__c>());
                }
                    wbsToFundingRecords.get(rec.ProjectCodeSAPWBSNum__c).add(rec);
	}
	
	//Khushman: Created for PMD FIx
	public static void populateFields(ProjectSAPFunding__c rec, Contact contactRec){
                        if (rec.WBSPersonResponsibleENumber__c.replace('e','').replace('E','').equalsIgnoreCase(contactRec.Enumber__c.replace('e','').replace('E',''))){
                            rec.WBSPersonResponsibleCode__c=contactRec.id;
                        }
                        if (rec.Lead_Chief_Investigator_ENumber__c.replace('e','').replace('E','').equalsIgnoreCase(contactRec.Enumber__c.replace('e','').replace('E',''))){
                            rec.LeadChiefInvestigatorCode__c=contactRec.id;
                        }
	}

    /************************************************************************************
    // Purpose      :  To able to call findRecords without need of creating Lists
    // Parameters   :  String sObjectName, String fieldName, Set values
    // Developer    :  Khushman Deomurari
    // Created Date :  10/10/2018                 
    //***********************************************************************************/
    public static List<sObject> findRecords(String sObjectName, String fieldName, Set<String> values){
        return findRecords(sObjectName, new List<String>{fieldName}, new List<String>(values));
    }

    /************************************************************************************
    // Purpose      :  Find records having particular values in particular fields
    // Parameters   :  String sObjectName, List fieldNames, List values   	
    // Developer    :  Khushman Deomurari
    // Created Date :  10/10/2018                 
    //***********************************************************************************/
    public static List<sObject> findRecords(String sObjectName, List<String> fieldNames, List<String> values){
        String valuesInStr=' IN (\''+ String.join(values, '\',\'') +'\')';
        String selectQuery='Select id,'+ String.join(fieldNames, ',') + ' from ' + sObjectName + ' where ' +
            String.join(fieldNames, valuesInStr+' OR ') + valuesInStr;
//        List<sObject> foundRecords=[Select id,:fieldNames from :sObjectName where :fieldNames IN :values];
       
        return Database.query(String.escapeSingleQuotes(selectQuery).replace('\\', '')); //Changed for PMD Fix
    }

}