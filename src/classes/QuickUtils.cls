/*******************************************
Purpose: To create different Utility Class to move simple frequent checks
History:
Created by Khushman Deomurari on 05/02/2019
*******************************************/

public with sharing class QuickUtils {

		public static Boolean isNotEmpty(List<Object> objList){
			return (objList!=null && !objList.isEmpty());
		}
		
		public static Object returnOnlyIfNotEmpty(List<Object> objList){
			if (!objList.isEmpty()){
				return objList;
			}
			return null;
		}

		public static String returnOnlyIfNotEmpty(String str){
			if (!String.isEmpty(str)){
				return str;
			}
			return null;
		}

		public static String returnOnlyIfLengthMoreThanZero(String str){
			if (str.length()>0){
				return str;
			}
			return null;
		}

/*	Not In Use as of now
		public static Object getFirstElseNull(List<Object> objList){
			if (objList.size()>0){
				return objList[0];
			}
			return null;
		}

	    public static void setIfNotNullAndEmpty(List<Object> objList, Object provideValTo) {
	    	if (objList!=null && objList.size()>0){
	    		provideValTo=objList;
	    		system.debug('setted:'+provideValTo);
	    	}
	    }

	    public static void setIfValIn(List<Object> objList, Object findThis, Object provideValTo) {
	    	if (objList!=null && objList.contains(findThis)){
	    		provideValTo=findThis;
	    	}
	    }
*/


//------------------------------------Divider----------------


}