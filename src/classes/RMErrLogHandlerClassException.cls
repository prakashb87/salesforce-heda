/*****************************************************************************************************
*** @Class             : RMErrLogHandlerClassException
*** @Author            : Avinash Machineni 
*** @Requirement       : Recording the Errors into a Custom Object 
*** @Created date      : 22/03/2018
*** @JIRA ID           : ECB-229
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to record the errors encountered, into a custom object
*****************************************************************************************************/

public with sharing class RMErrLogHandlerClassException extends Exception {
    
   //Creating Error Log Record
    public RMErrLog__c RMErrLogRecHandler(String userid,Exception ex, Exception errReason, Integer errNum, String errMessage, Datetime errTime, String errType){
        System.debug('RMErrLogRecHandler Method');
        RMErrLog__c err = new RMErrLog__c();
            err.Error_Cause__c = String.valueOf(errReason);
 			err.Error_Line_Number__c = errNum;
            err.Error_Message__c = errMessage;
            err.Error_Time__c = errTime;
            err.Error_Type__c = errType;
        try{
            insert err;
            if(Test.isRunningTest())
            {
                throw new DmlException();
            }
        }catch(exception e){
           System.debug('The following exception has occurred: ' + e.getMessage());
        }
			return err;
    }
}