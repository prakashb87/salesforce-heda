/**************************************************************
Purpose: To create Test data for RSTP Project
History:
Created by Subhajit on 10/24/2018
*************************************************************/
@isTest
public class RSTP_TestDataFactoryUtils {
   
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User as well Non Community data
    // Parameters   :  Map<String,Integer>, Map<String,String>      
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    public static Boolean testRSTPDataSetup(Map<String,Integer> allnumbersOfDataMap,Map<String,String> requiredInfoMap){
        
        Boolean sucessFlag= false;
        Integer numOfUsers = allnumbersOfDataMap.get('user'); 
        Integer numOfAccounts = allnumbersOfDataMap.get('account'); 
        Integer numOfContacts = allnumbersOfDataMap.get('contact');
        String typeOfUser = requiredInfoMap.get('userType');
        String profileName = requiredInfoMap.get('profile');
        String role= requiredInfoMap.get('role');
        
        if(typeOfUser.equalsIgnoreCase('CommunityUsers'))
        {
            List<Account> accts = createAccounts(numOfAccounts);
            List<Contact> cons = createContactWithAccount(numOfContacts,accts);
            List<User> usrs = createCommunityUsersWithProfile(numOfUsers,profileName,cons);
            System.assert(usrs!=null);
            if(usrs!=null)
            {
                sucessFlag =true;
            }
        }
        else //Admin or Other non community Users
        {
            List<Account> accts = createAccounts(numOfAccounts);
            List<Contact> cons = createContactWithAccount(numOfContacts,accts);
            List<User> usrs = createNonCommunityUsersWithProfile(numOfUsers,profileName,role);
            System.assert(usrs!=null);
            if(usrs!=null)
            {
                sucessFlag =true;
            }
        }
        
        return sucessFlag;
    }
  
    /************************************************************************************
    // Purpose      :  Creating test account data
    // Parameters   :  Integer      
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    public static List<Account> createAccounts(Integer numAccts) {
        List<Account> accts = new List<Account>();
         for(Integer i=1;i<=numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i);
            accts.add(a);
        }
        insert accts;
        System.assert(accts!=null);
        return accts;
    }
    
    /************************************************************************************
    // Purpose      :  Creating test contact data
    // Parameters   :  Integer, List<Account>       
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    public static List<Contact> createContactWithAccount(Integer numContacts, List<Account> accts) {
        List<Contact> contacts = new List<Contact>();
        for(Account eachAccount : accts)
        {
            for(Integer i=1;i<=numContacts;i++) {
                Contact contact = new Contact(
                                              FirstName ='Test',
                                              LastName ='Contact' + i,
                                              AccountId = eachAccount.Id,
                                              Email= 'Test.Contact'+i+'@rmit.edu.au'
                                             );
                contacts.add(contact);
            }
        }
        insert contacts;
        System.assert(contacts!=null);
        return contacts;
    }
  
    /************************************************************************************
    // Purpose      :  Creating Community users
    // Parameters   :  Integer, String, List<Contact>   
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    public static List<User> createCommunityUsersWithProfile(Integer numOfUsers,String profileName,List<Contact> contacts) {
        List<User> users = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name= :profileName];
        //for(Contact con:contacts)
        //{
            for(Integer i=1;i<=numOfUsers;i++) {
                User usr = new User(Alias = 'ulastnm'+i, 
                                    Email='test.communityUser'+i+'@rmit.edu.au.com', 
                                    EmailEncodingKey='UTF-8', 
                                    FirstName ='Test',
                                    LastName='communityUser'+i, 
                                    LanguageLocaleKey='en_US', 
                                    LocaleSidKey='en_US', 
                                    ProfileId = p.Id,
                                    ContactId = null, 
                                    TimeZoneSidKey='America/Los_Angeles', 
                                    UserName='test.communityUser'+i+'@test.com.testDev');
                users.add(usr);
            }
       // }
       for(Integer i=0;i<contacts.size();i++)
       {
           users[i].ContactId = contacts[i].Id;
       }
        insert users;
        System.assert(users!=null);
        return users;
    }
    
    /************************************************************************************
    // Purpose      :  Creating non Community users
    // Parameters   :  Integer, String      
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    public static List<User> createNonCommunityUsersWithProfile(Integer numOfUsers, String profileName,String role) {
    
        List<User> users = new List<User>();
        Id userRoleId;
        
        Profile p = [SELECT Id FROM Profile WHERE Name= :profileName];
        if(!String.IsEmpty(role))
           {
             userRoleId =[SELECT Id FROM UserRole WHERE Name=:role].Id;
           }  
        
            for(Integer i=1;i<=numOfUsers;i++) {
                User usr = new User(Alias = 'ulastnm'+i, 
                                    Email='test.nonCommunityUser'+i+'@rmit.edu.au.com', 
                                    EmailEncodingKey='UTF-8', 
                                    FirstName ='Test',
                                    LastName='nonCommunityUser'+i, 
                                    LanguageLocaleKey='en_US', 
                                    LocaleSidKey='en_US', 
                                    ProfileId = p.Id,
                                    UserRoleId = userRoleId,
                                    TimeZoneSidKey='America/Los_Angeles', 
                                    UserName='test.nonCommunityUser'+i+'@test.com.testDev');
                users.add(usr);
            }
       
        insert users;
        System.assert(users!=null);
        return users;
    }
    
    /************************************************************************************
    // Purpose      :  Creating non Research Project
    // Parameters   :  Integer,String,String        
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    public static List<ResearcherPortalProject__c> createResearchProj(Integer numOfproject, String recordtypeId,String access) {
        List<ResearcherPortalProject__c> projects = new List<ResearcherPortalProject__c>();
        for(Integer i=1;i<=numOfproject;i++) {
           
           ResearcherPortalProject__c proj = new ResearcherPortalProject__c();
           proj.Title__c ='Test project '+i; 
           proj.Start_Date__c =  system.today();
           proj.End_Date__c =  system.today() +2; 
           proj.Access__c =access; 
           proj.RecordTypeId=recordtypeId;  
           projects.add(proj); 
        }
        insert projects;
        System.assert(projects!=null);
        return projects;
        
    }
    
     /************************************************************************************
    // Purpose      :  Creating non Research Project
    // Parameters   :  Integer,String,String        
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    public static List<ResearcherPortalProject__c> createResearchProjNew(Integer numOfproject, String recordtypeId,Map<String,String> keyFieldApiToValueMap) {
        List<ResearcherPortalProject__c> projects = new List<ResearcherPortalProject__c>();
        for(Integer i=1;i<=numOfproject;i++) {
           
           ResearcherPortalProject__c proj = new ResearcherPortalProject__c();
           proj.Title__c ='Test project '+i; 
           proj.Start_Date__c =  system.today();
           proj.End_Date__c =  system.today() +2; 
           proj.Access__c =keyFieldApiToValueMap.get('Access__c'); 
           proj.Status__c =keyFieldApiToValueMap.get('Status__c');
           proj.RecordTypeId=recordtypeId;  
           projects.add(proj); 
        }
        insert projects;
        System.assert(projects!=null);
        return projects;
        
    }
   
    /************************************************************************************
    // Purpose      :  Creating Ethics test data
    // Parameters   :  Integer,String       
    // Developer    :  Subhajit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
     public static List<Ethics__c> createEthics(Integer numOfEthics, String recordtypeId) {
        List<Ethics__c> ethics = new List<Ethics__c>();
        for(Integer i=1;i<=numOfEthics;i++) {
           
           Ethics__c ethic = new Ethics__c();
           ethic.Ethics_Id__c='test';
           ethic.Application_Title__c ='Test Ethics '+i;
           ethic.Approved_Date__c = system.today();
           ethic.Expiry_Date__c=system.today() +2;  
           ethics.add(ethic); 
        }
        insert ethics;
        System.assert(ethics!=null);
        return ethics;
        
    }
    
     /************************************************************************************
    // Purpose      :  Creating SAP Funding test data
    // Parameters   :  Integer      
    // Developer    :  Shalu
    // Created Date :  12/05/2018                 
    //***********************************************************************************/
     public static List<ProjectSAPFunding__c> createSAPFunding(Integer numOfSAPFunding) {
        List<ProjectSAPFunding__c> fundings = new List<ProjectSAPFunding__c>();
        for(Integer i=1;i<=numOfSAPFunding;i++) {
           
           ProjectSAPFunding__c funding = new ProjectSAPFunding__c();
           funding.CostElementDescription__c = 'Test'+i;
           funding.ProjectCodeSAPWBSNum__c='100';
           funding.Lead_Chief_Investigator_ENumber__c ='100';
           funding.WBSPersonResponsibleENumber__c = '100'; 
           fundings.add(funding); 
        }
        System.assert(fundings!=null);
        return fundings;
        
    }
    
    /************************************************************************************
    // Purpose      :  Creating Research Project Funding test data
    // Parameters   :  Integer      
    // Developer    :  Ankit
    // Created Date :  15/04/2019                 
    //***********************************************************************************/
     public static List<Research_Project_Funding__c> createProjectFunding(Integer numOfFunding) {
        List<Research_Project_Funding__c> fundings = new List<Research_Project_Funding__c>();
        for(Integer i=1;i<=numOfFunding;i++) {
           
           Research_Project_Funding__c funding = new Research_Project_Funding__c();
           funding.Organisation_Ecode__c ='ARC';
           fundings.add(funding); 
        }
        System.assert(fundings!=null);
        return fundings;
        
    }
    
}