/****************************************************************
Purpose: Controller class for Research Contract functionality 
Created By: Subhajit on 12/10/18
****************************************************************/
public class ResearcherContractController {
    
      /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
    
     /*****************************************************************************************
    // JIRA NO      :  250
    // SPRINT       :  9
    // Purpose      :  Fetch all Researcher Contract records
    // Developer    :  Subhajit
    // Parameter    :  Void 
    // Created Date :  12/10/2018 
    //************************************************************************************/ 
    @AuraEnabled
    public static List<ResearcherContractWrapper> getAllResearcherContractRecsInfo(){
        
        List<ResearcherContractWrapper> contractRecs = new List<ResearcherContractWrapper>();
       
        try{
            
            contractRecs = ResearcherContractHelper.getResearcherContractRecords();
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }
        catch(Exception ex)
        {
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
            // CaptureExceptionLogDetailHelper.addResearchPortalErrorLog(ex);
        }
        System.debug('@@@@@ All contract Records==>'+ JSON.serializePretty(contractRecs));
        //return JSON.serializePretty(contractRecs);
        return contractRecs;
        
    }
    
        /************************************************************************************
    // Purpose      : To check current login user is community user or salesforce user
                   
    //***********************************************************************************/ 
    
	@AuraEnabled     
    public static Boolean checkInternalDualAccessUser(){
            Boolean isInternalDualAccessUser = ApexWithoutSharingUtils.checkInternalDualAccessUser();
            return isInternalDualAccessUser;
    } 
    
    /*****************************************************************************************
// JIRA No      :  RPORW-735 
// Purpose : This method used to get the picklist value 
// Discription:Using this method we are populating the picklist value of status from Contract
// Parameters : void
// Developer : Md Ali
// Created Date : 12/02/2019
//****************************************************************************************/  
    
    @AuraEnabled 
    public static List<String> getContractStatusValue(){
        
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = ResearchProjectContract__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }  
        system.debug('@@statuspickvalue'+pickListValuesList);
        return pickListValuesList;
    }
    
    /*****************************************************************************************
// JIRA No      :  RPORW-735 
// Purpose : This method used to get the picklist value 
// Discription:Using this method we are populating the picklist value of Research Type from Contract
// Parameters : void
// Developer : Md Ali
// Created Date : 12/02/2019
//****************************************************************************************/     
    @AuraEnabled 
    public static List<String> getContractResearchTypeValue(){
        
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = ResearchProjectContract__c.Activity_Type__c.getDescribe(); 
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }  
        system.debug('@@ResearchTypepickvalue'+pickListValuesList);
        return pickListValuesList;
    }
    /*
     @AuraEnabled 
    public static List<ResearcherContractWrapper> getProjectContractWrapperList(Id projectId){
        
       List<ResearcherContractWrapper> contractWrapperList = new List<ResearcherContractWrapper>();
       try{
             contractWrapperList = ResearcherContractHelper.getProjectContractsWrapper(projectId);
       } 
       catch(Exception ex) 
       {     
           //Exception handling Error Log captured :: Starts
           //ResearcherExceptionHandlingUtil.addExceptionLog(ex,new Map<String, String>{ERR_MAP_KEYPARAM1=>ResearcherExceptionHandlingUtil.getErrMethod(ex.getStackTraceString()),ERR_MAP_KEYPARAM2=>ResearcherExceptionHandlingUtil.getErrClassName(ex.getStackTraceString()),ERR_MAP_KEYPARAM3=>'View Ethics',ERR_MAP_KEYPARAM4=>'No'});
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
       return contractWrapperList ;
    }  */

    /*****************************************************************************************
   	 // JIRA No      :  RPORW-440 
	// Purpose : This method used to get contract detail page 
    // Parameters : id
    //  Discription:Using this method we are showing the contract detail page
    // Developer : Md Ali
    // Created Date : 21/02/2019
    //****************************************************************************************/ 
    @AuraEnabled 
    public static List<ResearcherContractWrapper> getContracts(Id contractId){
        
       List<ResearcherContractWrapper> contractWrapperList = new List<ResearcherContractWrapper>();
       try{
             contractWrapperList = ResearcherContractHelper.getContractsHelper(contractId);
       } 
       catch(Exception ex) 
       {               
           //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
           //Exception handling Error Log captured :: Ends
           System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
       } 
       return contractWrapperList ;
    }  
    
   /*****************************************************************************************
    // Purpose      :  This is a method to return the list of Milestones for a Contract in the Contract Detial page,889
    // Parameters   :  Id (Contract Id)
    // Developer    :  Ankit
    // Created Date :  26/02/2019                 
    //****************************************************************************************/
     @AuraEnabled 
    public static List<MilestonesDetailsWrapper.milestonesWrapper> getContractMilestonesList(Id contractId){ 
        
       List<MilestonesDetailsWrapper.milestonesWrapper> milestonesWrapperList = new List<MilestonesDetailsWrapper.milestonesWrapper>();
       try{
             milestonesWrapperList = ResearcherContractHelper.getContractMilestonesList(contractId);
       } 
       catch(Exception ex) 
       {               
           //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); 
           //Exception handling Error Log captured :: Ends
           System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
       } 
       return milestonesWrapperList ;
    }  
    
}