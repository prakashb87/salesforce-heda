@SuppressWarnings('PMD.TooManyFields,PMD.ExcessivePublicCount,PMD.VariableNamingConventions,PMD.MethodNamingConventions')
/*******************************************
Purpose: Researcher Contract Wrapper
Created by Subhajit on 12/10/18
*******************************************/
public class ResearcherContractWrapper {
	
    public static final string NO_ACCESS_ROLE =Label.RSTP_Role_No_Access;  

    @AuraEnabled public String contractCode;
    @AuraEnabled public String contractTitle;
    @AuraEnabled public String status;
    @AuraEnabled public String submittedDate;
    @AuraEnabled public String currentLocation;
    @AuraEnabled public String fullyExecutedDate;
    @AuraEnabled public String primaryContact;
    @AuraEnabled public String researchType;
    @AuraEnabled public String agreementType;
	@AuraEnabled public String preExecutionManager;//added by Ali
	@AuraEnabled public String riReviewer;//added by Ali
	@AuraEnabled public String storageReferenceNumber;//added by Ali
	@AuraEnabled public String linkedPrimaryOrganisationName;//added by Ali
	@AuraEnabled public id     contractId;//added by Ali
    @AuraEnabled public List<conDetailsWrapper> rmsMembers;
    @AuraEnabled public boolean isHyperlinkActive; // added by Ankit,1512
    
    
    public ResearcherContractWrapper()
    {
            this.contractCode='';
            this.contractTitle='';
            this.status='';
            this.submittedDate='';
            this.currentLocation='';
            this.fullyExecutedDate='';
            this.primaryContact='';
            this.researchType='';
            this.agreementType='';
            this.preExecutionManager='';
			this.riReviewer='';
			this.storageReferenceNumber='';
			this.linkedPrimaryOrganisationName='';
			this.isHyperlinkActive = false;  // added by Ankit,1512
          
    }
    
    public ResearcherContractWrapper(ResearchProjectContract__c contracts)
    {
            this.contractId = contracts.id;
            this.contractCode= contracts.Ecode__c;
            this.contractTitle=contracts.ContractTitle__c;
            this.status=contracts.Status__c;
            this.submittedDate=String.valueOf(contracts.Contract_Logged_Date__c);
            this.currentLocation=contracts.Current_Active_Action__c;
            this.fullyExecutedDate=String.valueOf(date.valueOf(contracts.Fully_Executed_Date__c));
            this.primaryContact=contracts.Contact_Preferred_Full_Name__c;
            this.researchType=contracts.Activity_Type__c;
            this.agreementType=contracts.Contract_Type__c;
            this.preExecutionManager=contracts.PreExecutionManager__c;
			this.riReviewer=contracts.RIReviewer__c;
			this.storageReferenceNumber=contracts.StorageReferenceNumber__c;
			this.linkedPrimaryOrganisationName=contracts.LInkedPrimaryOrganisationName__c;
        
        	
            rmsMembers=new List<conDetailsWrapper>();
            
            Set<Id> memberIds = new Set<Id>();  // added by Ankit,1512
            
            for (Researcher_Member_Sharing__c eachRms: contracts.Researcher_Portal_Member_Sharings__r){
                conDetailsWrapper conObj = new conDetailsWrapper();
                conObj.member=eachRms.User__r.name;
                conObj.role=eachRms.Position__c;
                
                rmsMembers.add(conObj);
                if(eachRms.AccessLevel__c != NO_ACCESS_ROLE) {
                	memberIds.add(eachRms.User__c);
                }
            }
            
            // added by Ankit,1512
            if ( memberIds.contains(UserInfo.getUserId())) 
            {
                isHyperlinkActive = true;
            } else
            {
            	isHyperlinkActive = false;
            }
            
          
    }//ResearcherContractWrapper Ends
    
    public class conDetailsWrapper{
            
            @AuraEnabled public String member;
            @AuraEnabled public String role;
            
        }//conDetailsWrapper Ends
    
}