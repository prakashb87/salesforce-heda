/*******************************************
Purpose: Capturing Exception Log from UI 
History:
Created by Md Ali on 12/14/18
*******************************************/
public with Sharing class ResearcherExceptionHandlingUtil {
  

    /*****************************************************************************************
  // JIRA NO      : 470/314/469/736
    // SPRINT       :  7
    // Purpose      :  capturing Exception Log from integration and portal user
  // Developer    :  Md Ali
  // Parameter    :  Void  
  // Created Date :  12/14/2018 
  //************************************************************************************/ 
    public static void addExceptionLog(Exception ex, Map<String,String> parameterList)
    {
        
        // Getting all parameterlist from argument
        String isIntegation = parameterList.get('isIntegation');
        
        //Check type of Exception source 
        if(isIntegation=='yes')
        {
            //Secenario 1: preparing error log of integration type
            prepareIntegrationRMErrorLogRecords(ex,parameterList);
        } 
        else
        {
            //Scenario 2 : preparing error log of portal User type
            preparePortaluserRMErrorLogRecords(ex,parameterList);
        }           
     } 
       /************************************************************************************
// Purpose      :  Creating method for the intergation user exception Log
// Developer    :  Md Ali
// Created Date :  12/14/2018                 
//***********************************************************************************/
     public static void prepareIntegrationRMErrorLogRecords(Exception ex, Map<String,String> parameterList)
    {   
        // Declation Block :
        List<RMErrLog__c> intgErrLogList = new List<RMErrLog__c>();
        RMErrLog__c intgErrLog = new RMErrLog__c();
        
        // Getting all parameterlist from argument
        /*String methodName = parameterList.get('methodName');
        String className = parameterList.get('className');*/
        String payLoad = parameterList.get('payLoad');
        String businessFunctionName =parameterList.get('businessFunctionName');
           
        //mapping the value to the respected fields
        intgErrLog.Error_Line_Number__c=ex.getLineNumber();
        intgErrLog.Error_Message__c = ex.getMessage();
        intgErrLog.Error_Type__c = ex.getTypeName();
        intgErrLog.Error_Cause__c=String.valueOf(ex.getCause());
        intgErrLog.Error_Time__c=system.now();
        intgErrLog.Class_Name__c=getErrClassName(ex.getStackTraceString());
        intgErrLog.Method_Name__c=getErrMethod(ex.getStackTraceString());
        intgErrLog.Error_Payload__c=payLoad;
        intgErrLog.Business_Function_Name__c=businessFunctionName;
        intgErrLog.Is_Integration_Error__c=true;
        intgErrLog.Logged_In_User__c=userInfo.getUserId();
        intgErrLog.Is_Admin_Notified__c=false;
        intgErrLog.Is_Case_Created_c__c=false;
        intgErrLogList.add(intgErrLog);
     
		if (Schema.sObjectType.RMErrLog__c.fields.Error_Type__c.isCreateable()) {
		  insert intgErrLogList;
		  system.debug('@@@@integrationErrorList'+intgErrLogList);
		}
        
        
    }
           /************************************************************************************
// Purpose      :  Creating method for the portal user exception Log
// Developer    :  Md Ali
// Created Date :  12/14/2018                 
//***********************************************************************************/
     public static void preparePortaluserRMErrorLogRecords(Exception ex, Map<String,String> parameterList)
    {
        // Declation Block :
        List<RMErrLog__c> portalErrLogList = new List<RMErrLog__c>();
        RMErrLog__c portalErrLog = new RMErrLog__c();
        
        // Getting all parameterlist from argument
        /*String methodName = parameterList.get('methodName');
        String className = parameterList.get('className');*/
        String businessFunctionName =parameterList.get('businessFunctionName');
      
        //mapping the value to the respected fields
        portalErrLog.Error_Line_Number__c=ex.getLineNumber();
        portalErrLog.Error_Message__c = ex.getMessage();
        portalErrLog.Error_Type__c = ex.getTypeName();
        portalErrLog.Error_Cause__c=String.valueOf(ex.getCause());
        portalErrLog.Error_Time__c=system.now();
        portalErrLog.Class_Name__c=getErrClassName(ex.getStackTraceString());
        portalErrLog.Method_Name__c=getErrMethod(ex.getStackTraceString());
        portalErrLog.Business_Function_Name__c=businessFunctionName;
        portalErrLog.Is_Integration_Error__c=false;
        portalErrLog.Logged_In_User__c=userInfo.getUserId();
        portalErrLog.Is_Admin_Notified__c=false;
        portalErrLog.Is_Case_Created_c__c=false;
        portalErrLogList.add(portalErrLog);
      
        insert portalErrLogList;
        system.debug('@@@@portalErrorList'+PortalErrLogList);
    }
       /************************************************************************************
// Purpose      :  Creating method to get class Name
// Developer    :  Md Ali
// Created Date :  12/14/2018                 
//***********************************************************************************/    
    public static String getErrClassName(String line)
    {
        if (line.startsWith('Class.')){   
            line = line.substringAfter('Class.');
        }
        return line.substringBefore(':').substringBeforeLast('.');
        
    } 
       /************************************************************************************
// Purpose      :  Creating method to get class Name
// Developer    :  Md Ali
// Created Date :  12/14/2018                 
//***********************************************************************************/        
    public static String getErrMethod(String line)
    {
        return line.substringBefore(':').substringAfterLast('.');
    }
        /************************************************************************************
// Purpose      :  Creating method to get class Name
// Developer    :  Md Ali
// Created Date :  01/04/2019                 
//***********************************************************************************/     
    public static void createForceExceptionForTestCoverage(Boolean isExceptionRequired){
        if(Test.isRunningTest() && isExceptionRequired)
        {
            Integer num =1/0;
            //String excptn = string.valueOf(num);
        }
    }
       /************************************************************************************
// Purpose      :  Creating method to pass BusinessFunctionName and IsIntegation
// Developer    :  Md Ali
// Created Date :  01/04/2019                 
//***********************************************************************************/       
    public static Map<String,String> getParamMap(){
        
        Map<String,String> paramMap = new Map<String,String>();
        List<RMErrorLogVariables__mdt>  rmErrorLogVariablesList = new List<RMErrorLogVariables__mdt>();
        if((Schema.sObjectType.RMErrorLogVariables__mdt.isAccessible()) && (Schema.sObjectType.RMErrorLogVariables__mdt.fields.BusinessFunctionName__c.isAccessible())){  //added by ali PMD voilation
        rmErrorLogVariablesList = [SELECT DeveloperName, BusinessFunctionName__c, IsIntegation__c
                                   FROM RMErrorLogVariables__mdt ];
        for(RMErrorLogVariables__mdt eachrecord : rmErrorLogVariablesList){
            
            
            //if(eachrecord.DeveloperName == CreateResearcherPortalProjectController.class.getName())
            //{
                paramMap.put(label.BusinessFunctionName,eachrecord.BusinessFunctionName__c);
                paramMap.put(label.IsIntegation,eachrecord.IsIntegation__c);
            //}             
        }  
        }
        return paramMap;
    }
    
}