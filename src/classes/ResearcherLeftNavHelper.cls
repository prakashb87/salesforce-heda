/*******************************************
Purpose: To create Helper class for Researcher Left Navigation
History:
Created by Ankit Bhagat on 09/11/2018
*******************************************/

public with sharing class ResearcherLeftNavHelper {
    
    /*****************************************************************************************
    // Purpose      :  Left Navigation Helper method
    // Developer    :  Ankit Bhagat 
    // Created Date :  09/11/2018                 
    //************************************************************************************/ 
    public static ResearcherLeftNavWrapper.leftNavWrapper getLeftNavWrapperDetails(){ 

        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        ResearcherLeftNavWrapper.leftNavWrapper leftNavWrapperDetails = new ResearcherLeftNavWrapper.leftNavWrapper();
            
        Id projectRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        Id ethicsRecordTypeId  = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
        Id contractRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ContractRecordtype).getRecordTypeId();
        Id publicationRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
                  
        Set<Id> recordTypeIdSet = new Set<Id>{projectRecordTypeId, ethicsRecordTypeId, contractRecordTypeId, publicationRecordTypeId};
        reseacherMemberSharingList = ResearcherMemberSharingUtils.geResearcherMemberSharingList(recordTypeIdSet, userinfo.getuserid());
        List<SignificantEvent__c> milestonesList = ApexWithoutSharingUtils.getMilestonesList(reseacherMemberSharingList);        

        integer countMilestones = 0;
        
       for(SignificantEvent__c eachMilestone : milestonesList){
                
            if(((eachMilestone.DueDate__c >= Date.today()  && eachMilestone.DueDate__c  <= Date.today().addDays(30)) 
                            &&   eachMilestone.IsAction__c == true && eachMilestone.ActualCompletionDate__c == null)  ||
                            (((eachMilestone.DueDate__c < Date.today())  && eachMilestone.IsAction__c == true 
                            && eachMilestone.ActualCompletionDate__c == null))) {
                countMilestones++ ;             
            }
        }   
        
        
        System.debug('@@@countMilestones'+countMilestones);
        leftNavWrapperDetails.milestoneCount = countMilestones; 
        System.debug('@@@leftNavWrapperDetails'+leftNavWrapperDetails);
        return leftNavWrapperDetails;
    }
    
    
}