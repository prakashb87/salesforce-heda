/*******************************************
Purpose: To create wrapper class for Researcher Left Navigation
History:
Created by Ankit Bhagat on 09/11/2018
*******************************************/

public class ResearcherLeftNavWrapper {
	
    
    /*****************************************************************************************
	// Purpose      :  Left Navigation Wrapper
	// Developer    :  Ankit Bhagat	
	// Created Date :  09/11/2018                 
	//************************************************************************************/ 
    public class leftNavWrapper{
        
        @AuraEnabled public integer milestoneCount;
        
        public leftNavWrapper(){
            
            milestoneCount  = 0;
            
        }
        
    }
    
}