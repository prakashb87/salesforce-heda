/*******************************************
Name: ResearcherNewUserUpdateRMSBatch 

Description : Batch class to update RMS records with User when there is a related Contact available.
If the contact is tied to a user then the same user will be updated in the RMS record.

Created by : Prakash B for RPORW-897
Modified by : Subhajit for fixing all 21 PMD issues

History:

02/19/2019   Updated the code as part of RPORW-938 to update the RMS with Contact in case it is not related to a Contact
but has an ENumber attached to it.
03/05/2019   fixed all new introduced PMD issues
*******************************************/ 


public class ResearcherNewUserUpdateRMSBatch implements Database.batchable<sObject>{
    
   
        
    public Database.QueryLocator start(Database.BatchableContext bc){
        
        String rmsList;
        
        /* Fetch all Research Member Sharing records where the user field is empty */ 
        
        rmsList = 'SELECT Id,Contact__c, User__c, Person_eCode__c FROM Researcher_Member_Sharing__c WHERE User__c = null AND AccessLevel__c != \'No Access\'';
        return Database.getQueryLocator(rmsList);
    }
       
    public void execute(Database.BatchableContext bc, List<Researcher_Member_Sharing__c> scope){ 
       List<Researcher_Member_Sharing__c> updateUserList = new List<Researcher_Member_Sharing__c>();
       
       ResearcherNewUserUpdateRMSBatchHelper helperClass= new ResearcherNewUserUpdateRMSBatchHelper();
       updateUserList= helperClass.executeRMSBatchRecord(scope);
       system.debug('@@update UserList'+updateUserList); 
        if(updateUserList.size() > 0 ){
        	Database.SaveResult[] srList = Database.update(updateUserList, false);
        	
		   // Iterate through each returned result
           for (Database.SaveResult sr : srList) {
		
		    if (sr.isSuccess()) {
		        // Operation was successful, so get the ID of the record that was processed
		        System.debug('Successfully inserted pub share ' + sr.getId());
		    }
		
		    else {
		        // Operation failed, so get all errors                
		        for(Database.Error err : sr.getErrors()) {                   
		
		            System.debug(err.getStatusCode() + ': ' + err.getMessage()); 
		         }
		      }
           }        
        } 
		

    }
    
    public void finish(Database.BatchableContext bc) { 
        ResearcherCalculateRMSShareBatch rmsBatch = new ResearcherCalculateRMSShareBatch ();
        Database.executeBatch(rmsBatch); 
    }
}