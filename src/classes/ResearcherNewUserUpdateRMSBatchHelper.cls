public with sharing class ResearcherNewUserUpdateRMSBatchHelper {
	
	Map<String, String> eCodeContactMap = new Map<String, String>();
    Map<String, String> eCodeUserMap = new Map<String, String>();
    Map<Id, Id> userContactMap = new Map<Id, Id>();
    Set<Id> contactId = new Set<Id>();
    Set<Id> eCodeContactId = new Set<Id>(); //RPORW-1726
    Set<String> eCodes = new Set<String>();
    Set<String> studentCodes = new Set<String>();
   //To find out the duplicates
   // map<string,set<Id>> eNumberContactDuplicateMap = new map<string,set<id>>();    
   // map<string,set<Id>> sNumberContactDuplicateMap = new map<string,set<id>>(); 
    
    public  List<Researcher_Member_Sharing__c> executeRMSBatchRecord(list<Researcher_Member_Sharing__c> scope){
    	
        List<Researcher_Member_Sharing__c> updateUserList = new List<Researcher_Member_Sharing__c>();
        
        populateRMSContactUserMap(scope);
        
        ResearcherNewUserUpdateRMSBatchUtil batchUtil = new ResearcherNewUserUpdateRMSBatchUtil(eCodeContactMap,eCodeUserMap,userContactMap);
        
        /* Loop through the scope again and update the User__c field on the RMS record by fetching it from the map */
        updateUserList = batchUtil.getUpdateUserList(scope);
          
        return updateUserList;
    }
	
	public  void populateRMSContactUserMap(list<Researcher_Member_Sharing__c> scope){
        /* Loop through the RMS records and get the contact Ids if available,
        if not get the ecode value if available to find the related Contact in the system
        */
       System.debug('@@@scope'+scope);
        for(Researcher_Member_Sharing__c rmsScope : scope){
            if(rmsScope.Contact__c != null){
                contactId.add(rmsScope.Contact__c);
                eCodeContactId.add(rmsScope.Contact__c);//RPORW-1726
            }
            else if(rmsScope.Contact__c == null && rmsScope.Person_eCode__c != null && rmsScope.Person_eCode__c != ''){
                eCodes.add('e'+rmsScope.Person_eCode__c);
                studentCodes.add(rmsScope.Person_eCode__c); 
            }
               
        }  
        populateUserContactMap();
     }  
    
    public void populateUserContactMap(){
    	
    	 /* Search for the contacts in Salesforce with the corresponding Enumber,
        if there is a related Contact available, add it to the contact set to search of user is available.
        */
        
        for(Contact con : [SELECT ID, Enumber__c, Student_ID__c FROM Contact WHERE Enumber__c IN: eCodes OR Student_ID__c IN: studentCodes]){
            eCodeContactMap = setECodeContactMap(con);
            contactId.add(con.Id);
        }
        
        system.debug('## eCodeContactMap : '+eCodeContactMap+' : '+contactId);
     
        /* Form a map of User ID with the related Contact ID */
        for( User usr : [SELECT ID,ProfileId, ContactID FROM User WHERE ContactID IN: contactId AND isActive = true]){
        	
         eCodeContactId.remove(usr.ContactID);	//RPORW-1726
        	
       	  // To check the Profile Access of the User as only 9 Profiles should be applicable
       	  Boolean isCurrentUserProfileAccessibleFlag = SobjectUtils.getProfileAccessCheckInfo(usr.ProfileId); 
       	  system.debug('## isCurrentUserProfileAccessibleFlag : '+isCurrentUserProfileAccessibleFlag+' : '+usr);
               if(isCurrentUserProfileAccessibleFlag ){
                 userContactMap.put(usr.ContactId, usr.Id);
                }
        }
        
        system.debug('## userContactMap : '+userContactMap);
        
       //RPORW-1726
       Map<string,Id> eCodeContactIdMap = getEcodeNumberForContact();
       
       system.debug('## eCodeContactIdMap : '+eCodeContactIdMap);
       
       system.debug('## FederationIdentifier : '+eCodes+' : '+studentCodes);
        
        //Removed AND UserroleId!=null  FROM Query RPORW-1726
        for(User usr : [SELECT ID,ProfileId, FederationIdentifier FROM User WHERE (FederationIdentifier IN: eCodes OR FederationIdentifier IN: studentCodes) AND  isActive=true ]){
           
		   //RPORW-1726
		   String eCodeNumber = usr.FederationIdentifier.toUpperCase();
		   userContactMap.put(eCodeContactIdMap.get(eCodeNumber), usr.Id);
		   
		   system.debug('### codeNumber : '+eCodeNumber+' : '+eCodeContactIdMap.get(eCodeNumber));
		   //RPORW-1726 Ends
		   
       	  // To check the Profile Access of the User as only 9 Profiles should be applicable
       	  Boolean isCurrentUserProfileAccessibleFlag = SobjectUtils.getProfileAccessCheckInfo(usr.ProfileId); 
       	  system.debug('## isCurrentUserProfileAccessibleFlag 2 : '+isCurrentUserProfileAccessibleFlag+' : '+usr);
               if(isCurrentUserProfileAccessibleFlag ){
                   eCodeUserMap.put(usr.FederationIdentifier.toUpperCase(), usr.Id);
                }           
        }
         system.debug('### userContactMap : '+userContactMap);//1726
        if(Test.isRunningTest()){
            userContactMap  = setTestData();
        }
    }
    
      
	//RPORW-1726
	private Map<String,Id> getEcodeNumberForContact(){
	    Map<string,Id> eCodeContactIdMap=new Map<string,id>();
       for(contact aContact:[select id,Enumber__c, Student_ID__c from Contact where Id=:eCodeContactId]){
        // eCodeContactIdMap.put(aContact.Enumber__c,aContact.id);
		 
		 if(aContact.Enumber__c!=null){
           eCodes.add(aContact.Enumber__c);
           eCodeContactIdMap.put(aContact.Enumber__c.toUpperCase(),aContact.id);
         } else if (acontact.Student_ID__c!=null){
           eCodeContactIdMap.put(('s'+ aContact.Student_ID__c).toUpperCase(),aContact.id);
           studentCodes.add(aContact.Student_ID__c);
         }
        
       }
	   system.debug('### eCodeContactIdMap : '+eCodeContactIdMap);
	   return eCodeContactIdMap;
	}
	//RPORW-1726 Ends
  
    
    public Map<String, String> setECodeContactMap(Contact con){
          set<id> contactIds= new set<id>();
          if(con.Enumber__c != null && con.Enumber__c != ''){
          	     eCodeContactMap.put(con.Enumber__c.toUpperCase() , con.Id);
          	   /*  
          	     if(eNumberContactDuplicateMap.get(con.Enumber__c)!=null){
          	     	contactIds=eNumberContactDuplicateMap.get(con.Enumber__c);          	     	
          	     }
          	  contactIds.add(con.Id);  
          	  eNumberContactDuplicateMap.put(con.Enumber__c,contactIds);*/
          }else if(con.Student_ID__c != null && con.Student_ID__c != ''){
                eCodeContactMap.put(con.Student_ID__c.toUpperCase() , con.Id);
              /*    if(sNumberContactDuplicateMap.get(con.Student_ID__c)!=null){
          	     	contactIds=sNumberContactDuplicateMap.get(con.Student_ID__c);          	     	
          	     }
          	  contactIds.add(con.Id);  
          	  sNumberContactDuplicateMap.put(con.Student_ID__c,contactIds);*/
          }
        
          return eCodeContactMap;
    } 
    
    public Map<Id, Id> setTestData(){
        
        Map<Id, Id> userContactMap = new Map<Id, Id>();
        List<Contact> cnt = new List<Contact>([SELECT ID FROM Contact WHERE FirstName = 'Test Batch' AND LastName = 'Schedule Class' LIMIT 1]);
        List<User> usr = new List<User>([SELECT ID,ContactID FROM User WHERE ContactID != null AND isActive = true LIMIT 1]);
        if(cnt.size() > 0 && usr.size() > 0){
            userContactMap.put(cnt[0].Id, usr[0].Id); 
        }  
        return userContactMap;
    }
	
    
}