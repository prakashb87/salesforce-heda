/*
@Developer : Gourav Bhardwaj
@Description : Util class for ResearcherNewUserUpdateRMSBatchHelper
*/
public with sharing class ResearcherNewUserUpdateRMSBatchUtil{
	Map<String, String> eCodeContactMap = new Map<String, String>();
	Map<String, String> eCodeUserMap = new Map<String, String>();
	Map<Id, Id> userContactMap = new Map<Id, Id>();
	
	public ResearcherNewUserUpdateRMSBatchUtil(Map<String, String> eCodeContactMap,Map<String, String> eCodeUserMap,Map<Id, Id> userContactMap){
		
		this.eCodeContactMap 	= eCodeContactMap;
		this.eCodeUserMap 		= eCodeUserMap;
		this.userContactMap 	= userContactMap;
	}
    
	public List<Researcher_Member_Sharing__c> getUpdateUserList( List<Researcher_Member_Sharing__c> scope){
    	system.debug('eCodeContactMap : '+eCodeContactMap+' : '+eCodeUserMap+' : '+userContactMap);
        List<Researcher_Member_Sharing__c> updateUserList = new List<Researcher_Member_Sharing__c>();
        
           for(Researcher_Member_Sharing__c rmsScope : scope){
        
                //RPORW-1775
                Researcher_Member_Sharing__c memberSharingRecordToUpdate = populateUserUpdateList(rmsScope);
                if(memberSharingRecordToUpdate!=null){
                        updateUserList.add(memberSharingRecordToUpdate);
                }
        } 
        
        return updateUserList;
    }
    
    public Researcher_Member_Sharing__c populateUserUpdateList(Researcher_Member_Sharing__c rmsScope) 
    {
    	system.debug('rmsScope1 '+rmsScope.Contact__c+' : '+rmsScope.Person_eCode__c);
    	system.debug('mapping '+userContactMap+' : '+eCodeContactMap+' : '+eCodeContactMap.size());
    	
    	List<Researcher_Member_Sharing__c> updateUserList = new List<Researcher_Member_Sharing__c>();
    	 if(rmsScope.Contact__c != null && userContactMap != null 
             			&& userContactMap.containsKey(rmsScope.Contact__c)){
             				
             	system.debug('inside if');			
                rmsScope.User__c = userContactMap.get(rmsScope.Contact__c);
				
				//updateUserList.add(rmsScope);   
                            
                /* 
                If the RMS doesnt have a contact but has an ECode, get the related Contact from the contactMap,
                Update the contact in RMS and search for that contact in UserMap,
                Incase its available, update the User field in RMS
                */    
            }else if(rmsScope.Contact__c == null && rmsScope.Person_eCode__c != null 
            			&& eCodeContactMap != null && eCodeContactMap.size() > 0){
            				
            				system.debug('inside ELSE');
                rmsScope = getRMSScope(rmsScope);
               // if(rmsScope != null){
                   // updateUserList.add(rmsScope);
               // }
            }
			system.debug('### rmsScope : '+rmsScope);
			return rmsScope;
    	
    }
    
    
    public Researcher_Member_Sharing__c getRMSScope(Researcher_Member_Sharing__c rmsScope){
        
        Boolean isUpdated = false;
        
        if(eCodeContactMap.containsKey('E'+rmsScope.Person_eCode__c)){
            rmsScope.Contact__c = eCodeContactMap.get('E'+rmsScope.Person_eCode__c);
            isUpdated = true;
        }else if(eCodeContactMap.containsKey(rmsScope.Person_eCode__c)){
            rmsScope.Contact__c = eCodeContactMap.get(rmsScope.Person_eCode__c);
            isUpdated = true;
        }
        
        if(userContactMap.containsKey(rmsScope.Contact__c)){
            rmsScope.User__c = userContactMap.get(rmsScope.Contact__c);
            isUpdated = true;
        }else if(ecodeUserMap.containsKey('E'+rmsScope.Person_eCode__c)){
            rmsScope.User__c = ecodeUserMap.get('E'+rmsScope.Person_eCode__c);
            isUpdated = true;
        }else if(ecodeUserMap.containsKey(rmsScope.Person_eCode__c)){
            rmsScope.User__c = ecodeUserMap.get(rmsScope.Person_eCode__c);
            isUpdated = true;
        }
        
        system.debug('## rmsScope : '+rmsScope);
        
        if(isUpdated){     
            return rmsScope;
        }
        return null;
    } 
    
}