@isTest
private with sharing class ResearcherPortalContentSearchTest {
    private static final String CONTENT_TYPE = 'TestContentType';
    private static final String CONTENT_NAME = 'TestContent';
    private static final String SITE_NAME = 'TestSite';

    static cms__Content__c testContent;

    static void buildTestContent() {
        // Normally we wouldn't touch any cms__ objects, even in a test
        // Here we need a valid cms__Content__c ID to insert an OCMS_Search__c record
        testContent = new cms__Content__c(
                cms__Name__c = CONTENT_NAME,
                cms__Site_Name__c = SITE_NAME
        );

        insert testContent;
    }

    static ContentBundle buildContentBundle(Id originId, Integer importance) {
        ContentBundle bundle = new ContentBundle();
        bundle.originId = originId;

        bundle.contentType = new ContentTypeBundle();
        bundle.contentType.name = CONTENT_TYPE;

        AttributeBundle.ContentAttribute importanceAttribute = new AttributeBundle.ContentAttribute();
        importanceAttribute.name = 'importance';
        importanceAttribute.value = String.valueOf(importance);
        bundle.contentAttributes = new Map<String, AttributeBundle.ContentAttribute[]>();
        bundle.contentAttributes.put('en_US', new AttributeBundle.ContentAttribute[] { importanceAttribute });

        return bundle;
    }

    static testmethod void testupsertSearchObjectsForContent() {
        buildTestContent();

        ContentBundle bundle;
        OCMS_Search__c[] searchRecords;

        // Test insert
        bundle = buildContentBundle(testContent.Id, 50);

        ResearcherPortalContentSearch.upsertSearchObjectsForContent(new ContentBundle[] { bundle });

        searchRecords = [SELECT Content__c, Importance__c, Content_Type__c FROM OCMS_Search__c];

        System.assertEquals(1, searchRecords.size(), '1 record should be created');
        System.assertEquals(testContent.Id, searchRecords[0].Content__c);
        System.assertEquals(50, searchRecords[0].Importance__c);
        //System.assertEquals(CONTENT_TYPE, searchRecords[0].Content_Type__c);

        // Test update
        bundle = buildContentBundle(testContent.Id, 10);

        ResearcherPortalContentSearch.upsertSearchObjectsForContent(new ContentBundle[] { bundle });

        searchRecords = [SELECT Content__c, Importance__c, Content_Type__c FROM OCMS_Search__c];

        System.assertEquals(1, searchRecords.size(), 'Existing record should be updated');
        System.assertEquals(testContent.Id, searchRecords[0].Content__c);
        System.assertEquals(10, searchRecords[0].Importance__c, 'Importance should be updated');
        //System.assertEquals(CONTENT_TYPE, searchRecords[0].Content_Type__c);
    }
}