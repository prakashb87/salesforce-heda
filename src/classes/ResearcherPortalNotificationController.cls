public with sharing class ResearcherPortalNotificationController {

    /**
     * Sets the specified notification content to a viewed state for the logged in user.
     *
     * @param String originId
     *
     * @return JSON String of type Map<String,SocialBundle>
     */
    @AuraEnabled
    public static String setViewed(String originId) {
        SocialAPIRequest socialRequest = new SocialAPIRequest();

        socialRequest.listParameters.put('originIds', new List<String>{originId});
        socialRequest.parameters.put('type', 'Content');
        socialRequest.requestFlags.put('noData', false);

        Map<String, String> parameters = new Map<String, String>();
        parameters.put('service', 'SocialAPI');
        parameters.put('action', 'view');
        parameters.put('request', json.serialize(socialRequest));
        parameters.put('sname', 'Researcherportal'); // Replace site_name with the name of your OrchestraCMS site
        parameters.put('apiVersion', '5.0');

        String response = cms.ServiceEndPoint.doActionApex(parameters);

        JSONMessage.APIResponse apiResponse = (JSONMessage.APIResponse) json.deserialize(response, JSONMessage.APIResponse.class);

        System.Debug(response); // Outputs response and Map<String,SocialBundle>
        System.Debug(apiResponse.message); // Outputs no message on success
        return response;
    }
}