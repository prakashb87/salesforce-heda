public without sharing class ResearcherPortalSupportCaseHelperUtil {
    
    public static void createJunctionObjects(ResearcherPortalSupportCaseWrapper caseWrapper,Case theCase){
        //Creating Projects linked to Case - RPORW-1423
        
        createProjectJunctionRecord(caseWrapper,theCase);
        
        //creating Contracts linked to case -RPORW-1427 
        if(caseWrapper.caseJunction.contractsSelected!=null && caseWrapper.caseJunction.contractsSelected.size()>0)
        {
            createContractJunctionRecord(caseWrapper.caseJunction.contractsSelected,theCase);
        }
        //creating ethics linked to case - 
        if(caseWrapper.caseJunction.ethicsSelected!=null && caseWrapper.caseJunction.ethicsSelected.size()>0)
        {
            createEthicsJunctionRecord(caseWrapper.caseJunction.ethicsSelected,theCase);
        }
        //creating milestones linked to case -
        if(caseWrapper.caseJunction.milestonesSelected!=null && caseWrapper.caseJunction.milestonesSelected.size()>0)
        {
            createMilestoneJunctionRecord(caseWrapper.caseJunction.milestonesSelected,theCase);
        }
        //creating publication linked to case -
        if(caseWrapper.caseJunction.publicationsSelected!=null && caseWrapper.caseJunction.publicationsSelected.size()>0)
        {
            createPublicationJunctionRecord(caseWrapper.caseJunction.publicationsSelected,theCase);
        }
        
    }
    /*@Author : Gourav Bhardwaj
@Story : RPORW-1423
@Description : Method to create Project record related to a case from the UI
*/
    private static void createProjectJunctionRecord(ResearcherPortalSupportCaseWrapper caseWrapper, Case caseRecord){
        if(caseRecord!=null && caseWrapper.caseJunction.projectSelected!=null && 
           caseWrapper.caseJunction.projectSelected.size()>0 && caseWrapper.caseJunction.projectSelected[0]!=null ){
               system.debug(caseWrapper.caseJunction.projectSelected[0] +' : ' +caseRecord.Id);
               
               Researcher_Project_Case__c projectCase = new Researcher_Project_Case__c(
                   Project_Case__c = caseRecord.Id,
                   Researcher_Project__c =  caseWrapper.caseJunction.projectSelected[0]
               );
               if (!Schema.sObjectType.Researcher_Project_Case__c.isCreateable()){
                   System.debug('Error: Insufficient Access on Researcher_Project_Case__c');
                   //return null;
               }
               system.debug(projectCase);
               insert projectCase;
           }
    }
    
    /*@Author : Md Ali
@Story : RPORW-1427
@Description : Method to create Contract record related to a case from the UI
*/
    public static void createContractJunctionRecord(List<Id> contractIds, Case caseRecord){
        if(contractIds!=null && contractIds.size()>0 && caseRecord!=null){
            system.debug(contractIds +' : ' +caseRecord.Id);
            
            List<Contracts_Case__c> contractCaseList = new List<Contracts_Case__c>();
            for(Id contractId : contractIds){
                Contracts_Case__c contactCase = new Contracts_Case__c();
                contactCase.Contract_Case__c=caseRecord.Id;
                contactCase.Research_Contract__c=contractId; 
                contractCaseList.add(contactCase);
            }
            
            if (!Schema.sObjectType.Contracts_Case__c.isCreateable()){
                System.debug('Error: Insufficient Access on Contracts_Case__c');
                //return null;
            }
            system.debug(contractCaseList);
            insert contractCaseList;
        }
    }
    
    /*@Author : Md Ali
@Story : RPORW-1427
@Description : Method to create ethics record related to a case from the UI
*/
    public static void createEthicsJunctionRecord(List<Id> ethicsIds, Case caseRecord){
        if(ethicsIds!=null && ethicsIds.size()>0 && caseRecord!=null){
            system.debug(ethicsIds +' : ' +caseRecord.Id);
            
            List<Ethics_Case__c> ethicsCaseList = new List<Ethics_Case__c>();
            for(Id ethicsId : ethicsIds){
                Ethics_Case__c ethicsCase = new Ethics_Case__c();
                ethicsCase.Ethics_Case__c=caseRecord.Id;
                ethicsCase.Ethics_Record__c=ethicsId; 
                ethicsCaseList.add(ethicsCase);
            }
            
            if (!Schema.sObjectType.Ethics_Case__c.isCreateable()){
                System.debug('Error: Insufficient Access on Contracts_Case__c');
                //return null;
            }
            system.debug(ethicsCaseList);
            insert ethicsCaseList;
        }
    }
    /*@Author : Md Ali
@Story : RPORW-1427
@Description : Method to create publication record related to a case from the UI
*/
    public static void createPublicationJunctionRecord(List<Id> publicationIds, Case caseRecord){
        if(publicationIds!=null && publicationIds.size()>0 && caseRecord!=null){
            system.debug(publicationIds +' : ' +caseRecord.Id);
            
            List<Publications_Case__c> publicationCaseList = new List<Publications_Case__c>();
            for(Id publicationId : publicationIds){
                Publications_Case__c publicationCase = new Publications_Case__c();
                publicationCase.Publication_Case__c=caseRecord.Id;
                publicationCase.Publications_Record__c=publicationId; 
                publicationCaseList.add(publicationCase);
            }
            
            if (!Schema.sObjectType.Publications_Case__c.isCreateable()){
                System.debug('Error: Insufficient Access on Contracts_Case__c');
                //return null;
            }
            system.debug(publicationCaseList);
            insert publicationCaseList;
        }
    }
    /*@Author : Md Ali
	@Story : RPORW-1427
	@Description : Method to create milestone record related to a case from the UI
	*/
    public static void createMilestoneJunctionRecord(List<Id> milestonesIds, Case caseRecord){
        if(milestonesIds!=null && milestonesIds.size()>0 && caseRecord!=null){
            system.debug(milestonesIds +' : ' +caseRecord.Id);
            
            List<Milestones_Case__c> milestonesCaseList = new List<Milestones_Case__c>();
            for(Id milestonesId : milestonesIds){
                Milestones_Case__c milestonesCase = new Milestones_Case__c();
                milestonesCase.Milestone_Case__c=caseRecord.Id;
                milestonesCase.Milestones_Record__c=milestonesId; 
                milestonesCaseList.add(milestonesCase);
            }
            system.debug('@@milestonesCaseList'+milestonesCaseList);
            if (!Schema.sObjectType.Milestones_Case__c.isCreateable()){
                System.debug('Error: Insufficient Access on Contracts_Case__c');
                //return null;
            }
            system.debug(milestonesCaseList);
            insert milestonesCaseList;
        }
    }
    
}