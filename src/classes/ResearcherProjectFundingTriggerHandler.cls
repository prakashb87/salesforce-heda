/**************************************************************
Purpose: Handler class of ResearcherPojectFunding Trigger
History:
Created by Ankit on 12/04/2019
*************************************************************/
public with Sharing class ResearcherProjectFundingTriggerHandler {
    
    
    /*****************************************************************************************
    // JIRA No      :  RPORW-1041
    // SPRINT       :  SPRINT X5
    // Purpose      :  get funding data from the trigger.new
    // Parameters   :  list of Research_Project_Funding__c 
    // Developer    :  Ankit
    // Created Date :  04/03/2019                 
    //****************************************************************************************/  
    public static void getFundingDataforOrganisationECodeForUpsert(list<Research_Project_Funding__c> newFundingList, Map<id,Research_Project_Funding__c> oldFundingMap) 
    {  
        Boolean isAfter = true;
        System.debug('@@@@newFundingList : '+newFundingList);
        Set<Id> projectIds = new Set<Id>();     
        List<ResearcherPortalProject__c> lstResearcherPortalProject = new List<ResearcherPortalProject__c>();
        for(Research_Project_Funding__c eachFunding :newFundingList)
        {
            //Insert Block
            if(Trigger.isInsert && eachFunding.Organisation_Ecode__c != null) {
                projectIds.add(eachFunding.ResearcherPortalProject__c);  
            }
            else if(Trigger.isUpdate && eachFunding.Organisation_Ecode__c != oldFundingMap.get(eachFunding.id).Organisation_Ecode__c ){
                projectIds.add(eachFunding.ResearcherPortalProject__c);
            }            
        }

        callProjectUpdateMethod(projectIds);
    } 

    
        /*****************************************************************************************
    // JIRA No      :  RPORW-1041
    // SPRINT       :  SPRINT X5
    // Purpose      :  get funding data from the trigger.new
    // Parameters   :  list of Research_Project_Funding__c 
    // Developer    :  Ankit
    // Created Date :  04/03/2019                 
    //****************************************************************************************/  
    public static void getFundingDataforOrganisationECodeForDelete(list<Research_Project_Funding__c> fundingList) 
    {  
        Boolean isAfter = true;
        System.debug('@@@@fundingList'+fundingList);
        Set<Id> projectIds = new Set<Id>();     
        List<ResearcherPortalProject__c> lstResearcherPortalProject = new List<ResearcherPortalProject__c>();
        for(Research_Project_Funding__c eachFunding :fundingList)
        {
            projectIds.add(eachFunding.ResearcherPortalProject__c);
                        
        }

        callProjectUpdateMethod(projectIds);
    } 

    /*****************************************************************************************
    // JIRA No      :  RPORW-1041
    // SPRINT       :  SPRINT X5
    // Purpose      :  get funding data from the trigger.new
    // Parameters   :  list of Research_Project_Funding__c 
    // Developer    :  Ankit
    // Created Date :  04/03/2019                 
    //****************************************************************************************/  
    public static void callProjectUpdateMethod(Set<Id> projectIds) 
    { 
        List<ResearcherPortalProject__c> lstResearcherPortalProject = new List<ResearcherPortalProject__c>();
        if(Schema.sObjectType.ResearcherPortalProject__c.fields.Project_Type__c.isAccessible()) {
        lstResearcherPortalProject = [Select id, recordtype.name,recordtypeid, name, Access__c, ls_Funded__c, 
                                          ls_Confidential__c,Project_Type__c, Group_Status__c 
                                          FROM ResearcherPortalProject__c
                                          where recordtypeId =: Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.ResearchMasterProject_RecorType).getRecordTypeId()
                                          AND Id IN : projectIds];
        }                                
        System.debug('@@@@lstResearcherPortalProject'+lstResearcherPortalProject);
        
        ResearcherPortalProjectTriggerHandler.getProjectListToUpsert(projectIds, lstResearcherPortalProject); 
    
    }

    
   }