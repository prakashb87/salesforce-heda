/*
@Author : Gourav Bhardwaj
@Description : Util class to handle logic for Searc Text in Projects Member, Title, Summary, Keyword fields.
*/
public class ResearcherPublicProjectsTextSearchUtil {
	//@Story : RPROW-1303
    //@Description : Method to check if the words searched is present in the passed member's name
    //@Author : Gourav Bhardwaj
    public static Boolean isWordsInSentencePresentInProjectMember(String memberName,List<Researcher_Member_Sharing__c> membersInProject){
        Boolean isSearchTextPresent = false;
        //Itterating each member in the Project and then checking if passed text is present in the name
        for(Researcher_Member_Sharing__c members : membersInProject){
            system.debug('## members.Contact__r : '+members.Contact__C);
            if(members.Contact__c!=null && members.Contact__r.Name.containsIgnoreCase(memberName)){
                isSearchTextPresent = true;
                break;
            }
            
        }//for Ends 
        
    	return isSearchTextPresent;
    }//isWordsInSentencePresentInProjectMember Ends
    
    //RPROW-1303 : Checking if word in a sentence is present in the Project
    //@Author : Gourav Bhardwaj
    //@Description : Method to check if words passes from the search text is present in the Project's Title, Keyword or Summary
    public static ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper isWordsInSentencePresentInProject(ResearcherPortalProject__c project,List<String> searchedWords){
    	ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapper = new ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper();
    	Integer searchOrder = 0;
		Boolean isSearchTextPresent = false;
		
		ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapperForTitle 	= isWordInSentencePresentInTitle(searchedWords,project);
		ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapperForKeyword 	= isWordInSentencePresentInKeyword(searchedWords,project);
		ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapperForSummary 	= isWordInSentencePresentInSummary(searchedWords,project);
		
        system.debug('## searchedWords : '+searchedWords);
        system.debug('## project : '+project);
        system.debug('## filteredRecordWrapperForTitle : '+filteredRecordWrapperForTitle);
        system.debug('## filteredRecordWrapperForKeyword : '+filteredRecordWrapperForKeyword);
        system.debug('## filteredRecordWrapperForSummary : '+filteredRecordWrapperForSummary);
        
       
		if(filteredRecordWrapperForTitle.searchOrder!=0){
			searchOrder = filteredRecordWrapperForTitle.searchOrder;
		}else 
		if(filteredRecordWrapperForKeyword.searchOrder!=0){
			searchOrder = filteredRecordWrapperForKeyword.searchOrder;
		}else 
		if(filteredRecordWrapperForSummary.searchOrder!=0){
			searchOrder = filteredRecordWrapperForSummary.searchOrder;
		}
		
		//Check if Al Search Words are present in any one of the Fields
		if(filteredRecordWrapperForTitle.isSearchTextPresent 
			|| filteredRecordWrapperForKeyword.isSearchTextPresent 
			|| filteredRecordWrapperForSummary.isSearchTextPresent)
		{
			isSearchTextPresent = true;
			
		}
		
		filteredRecordWrapper.isSearchTextPresent = isSearchTextPresent;
		filteredRecordWrapper.searchOrder         = searchOrder;
		
    	return filteredRecordWrapper;
    }// isWordsInSentencePresentInProject Ends
    
	public static ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper isWordInSentencePresentInTitle(List<String> searchedWords,ResearcherPortalProject__c project){
		Boolean areWordsPresentInTitle = true;
		ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapper = new ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper();
    	Integer searchOrder = 1;
		
		for(String word : searchedWords){
			if(String.isBlank(project.Title__c)){
                areWordsPresentInTitle = false;
				break;
			}
            system.debug(project.Title__c+' : '+word);
			if(!project.Title__c.containsIgnoreCase(word)){
				areWordsPresentInTitle 	= false;
				searchOrder				= 0;
				break;
			}
		}
		
		filteredRecordWrapper.searchOrder         = searchOrder;
        filteredRecordWrapper.isSearchTextPresent = areWordsPresentInTitle;
		
		return filteredRecordWrapper;
	}
	public static ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper isWordInSentencePresentInKeyword(List<String> searchedWords,ResearcherPortalProject__c project){
		Boolean areWordsPresentInKeyword = true;
		ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapper = new ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper();
    	Integer searchOrder = 2;
		
        system.debug('## searchedWords : '+searchedWords);
        system.debug('## project.Keywords__c : '+project.Keywords__c);
        
        if(String.isBlank(project.Keywords__c)){
                filteredRecordWrapper.searchOrder         = 0;
        		filteredRecordWrapper.isSearchTextPresent = false;
				return filteredRecordWrapper;
			}
        
		for(String word : searchedWords){
			if(!isWordPresentInKeywords(word,project.Keywords__c))
            {
                areWordsPresentInKeyword 	= false;
				searchOrder					= 0;
				break;
            }
		}
		
		filteredRecordWrapper.searchOrder         = searchOrder;
        filteredRecordWrapper.isSearchTextPresent = areWordsPresentInKeyword;
		system.debug('## filteredRecordWrapper : '+filteredRecordWrapper);
		return filteredRecordWrapper;
	}
    //Method to check if all search words are present in keywords of the project
    private static Boolean isWordPresentInKeywords(String word, String projectKeywords){
        List<String> projectKeywordsList = new List<String>();
        Boolean isSearchTextPresent = false;
        projectKeywordsList = projectKeywords.split(';');
        system.debug('## projectKeywordsList : '+projectKeywordsList);
        
        for(String projectkeyword : projectKeywordsList){
            if(projectkeyword.containsIgnoreCase(word)){
                isSearchTextPresent = true;
                break;
             }
        }
        system.debug('## isSearchTextPresent : '+isSearchTextPresent);
        return isSearchTextPresent;
    }
	public static ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper isWordInSentencePresentInSummary(List<String> searchedWords,ResearcherPortalProject__c project){
		Boolean areWordsPresentInSummary = true;
		ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapper = new ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper();
    	Integer searchOrder = 3;
		
		for(String word : searchedWords){
			if(String.isBlank(project.Description__c)){
				searchOrder					= 0;//RPORW-1603
                areWordsPresentInSummary 	= false;
				break;
			}
			if(!project.Description__c.containsIgnoreCase(word)){
				searchOrder					= 0;
				areWordsPresentInSummary 	= false;
				break;
			}
		}
		
		filteredRecordWrapper.searchOrder         = searchOrder;
		filteredRecordWrapper.isSearchTextPresent = areWordsPresentInSummary;
		
		return filteredRecordWrapper;
	}
	
 }