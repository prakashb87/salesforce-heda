/*
*@Author        : Md Ali
*@Description   : Class created to handle Attach file in the Email.
*@Story         : RPORW-1459
*@Created Date  : 28-5-2019
*/
public without sharing class ResearcherSupportCaseEmailTemplateHelper{

    /*
    *@Author        : Md Ali
    *@Description   : Method created to retrieve File attached for a case
    */
    public static void sendResearchSupportEmailNotification(Id caseID){
    
       system.debug('$$$'+caseId);
    
        list<Case> caseList = new List<Case>();
        
        if(Schema.sObjectType.Case.isAccessible()){
            System.debug('Not Accessible');
        }
        caseList = [SELECT ID,ResearcherCaseRoutedToEmail__c,Subject,CreatedDate,createdById,contactId,Researcher_Location__c,Contact.Name,Contact.Email,ResearcherCaseTopic__c,ResearcherCaseSubTopic__c,
                    CaseNumber,Description FROM Case WHERE ID =: caseID];
        
        
             //Checking Accessibility of EmailTemplate Object
        if((Schema.sObjectType.EmailTemplate.isAccessible()) && (Schema.sObjectType.EmailTemplate.fields.id.isAccessible())){
            System.debug('Not Accessible');
        }  //Checking Accessibility of EmailTemplate Ends
         EmailTemplate emailTemplate                         = new EmailTemplate();
    	 emailTemplate=[Select id from EmailTemplate where name = 'Researcher Support Case VfEmail Template' limit 1];
     
       SendEmail(caseList,emailTemplate);
        //if(!contentDocumentids.. Ends
        
        
    }//Method sendEmail Ends
    
    public static void sendEmail(list<Case> caseList, EmailTemplate emailTemplate  ){
    	
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        List<Messaging.SingleEmailMessage> singleEmailList  = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage singleEmail            = new Messaging.SingleEmailMessage();        
       
        List<String> emailAddress   = new list<string>();
    	string owdAddressId=[SELECT ID FROM OrgWideEmailAddress WHERE DisplayName = 'No Reply Research Support'].ID;
        //Checking Accessibility of Case Object
       system.debug('$$$'+caseList);        
        if( caseList != null && caseList.size() > 0){

                    Case emailCase = caseList[0];
             fileAttachments = getFileAttachments(emailCase.Id);
            emailAddress.add(emailCase.ResearcherCaseRoutedToEmail__c);
            singleEmail.setToAddresses(emailAddress);
            singleEmail.setTemplateId(emailTemplate.id);
            singleEmail.setWhatId(emailCase.Id);
            singleEmail.setSaveAsActivity(false);
                     singleEmail.setOrgWideEmailAddressId(owdAddressId);

            if(fileAttachments.size() > 0){
                singleEmail.setFileAttachments(fileAttachments);
            }

                     if(emailCase.ContactId != null && emailCase.Contact.Email != null && emailCase.Contact.Email != ''){
                           singleEmail.setTargetObjectId(emailCase.contactId);
                     }else{
               singleEmail.setTargetObjectId(emailCase.createdById);
                     }

            // Step 6. Add your singleEmail to the master list
            singleEmailList.add(singleEmail);
            
            // Step 7: Send all singleEmailList in the master list
            Messaging.sendEmail(singleEmailList); 
         }    
    	
    }
    public static List<Messaging.Emailfileattachment> getFileAttachments(Id caseID){

         List<id> contentDocumentids = new List<id>();
         List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
         //Quering ContentDocumentLink to get the attached File Id of the Case
         for(ContentDocumentLink contentDocumentLink : [SELECT LinkedEntityid, ContentDocumentid FROM ContentDocumentLink WHERE LinkedEntityid =:caseID]){
             contentDocumentids.add(contentDocumentLink.ContentDocumentid);
         }
         //For Ends

         //Checking if List is not Empty
        if(!contentDocumentids.isEmpty()){
            //Quering ContentVersion of the attached File id collected in the previous Code
            for ( ContentVersion contentVersion : [SELECT title,PathOnClient, FileExtension, FileType,versiondata FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentids])
            {
                Blob documentBody = contentVersion.versiondata;
                Messaging.Emailfileattachment emailFileAttachment   = new Messaging.Emailfileattachment();
                emailFileAttachment.setFileName(contentVersion.title+'.'+contentVersion.FileExtension);
                //emailFileAttachment.contenttype = contentVersion.FileType;
                emailFileAttachment.setBody(documentBody);

                fileAttachments.add(emailFileAttachment);
                system.debug('body : '+documentBody+'-----------'+contentVersion.title);
            }
        }

        return fileAttachments;
    }



}//Class ResearcherSupportCaseEmailTemplateHelper Ends
