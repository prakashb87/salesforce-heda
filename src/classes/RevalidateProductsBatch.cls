@SuppressWarnings('PMD.AvoidGlobalModifier')
global class RevalidateProductsBatch implements Database.Batchable<Id>, Database.Stateful {
	
	string m_query = null;
    list<id> m_pcIds = null;

    string getSfdc15Id(Id id) {
        return String.valueOf(id).substring(0, 15);
    }
	
	global RevalidateProductsBatch(list<id> pcIds) {
		
		system.debug('RevalidateProductsBatch created');        
        if (pcIds != null) { 
            m_pcIds = pcIds;
        }
	}
	global Iterable<Id> start(Database.BatchableContext ctx) {
        
        system.debug('RevalidateProductsBatch >> start');
        return m_pcIds;
    }

    global void execute(Database.BatchableContext BC, list<Id> pcIds) {

        system.debug('RevalidateProductsBatch.execute (pcIds) >> ' + pcIds);
        cscfga.ProductConfigurationBulkActions.revalidateConfigurations(new set<Id> { pcIds[0] });
    }
    
    global void finish(Database.BatchableContext BC) {
    	
        system.debug('RevalidateProductsBatch >> finish');
    }
		
}