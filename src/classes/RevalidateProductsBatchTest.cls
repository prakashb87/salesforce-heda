@isTest(seeAllData=false)
private class RevalidateProductsBatchTest {
	
	static cscfga__Product_Configuration__c config;
	
    @TestSetup
	static void setupTestData() {
        
        Account accDepartment = TestUtility.createTestAccount(true
            , 'Department Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        
        Account accAdministrative = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
 		List<String> lstParams = new List<String>{'Test', 'Con'};
 		Contact con = TestUtility.createTestContact(true
            ,lstParams
            , accAdministrative.Id);

        User u = TestUtility.createUser('Partner Community User', false);
        u.ContactId = con.Id;
        u.Username = 'communityuser@unittest.com';
        insert u;
        Opportunity opp = TestUtility.createTestOpportunity(true, 'Test Opportunity', accDepartment.Id);
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute Definition'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0 
            , cscfga__is_significant__c = true);
        insert attrDef;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;
        
        config = new cscfga__Product_Configuration__c(Name = 'Test config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id
            , cscfga__Product_Family__c = 'Test Family');  
        insert config;

    }
    
    /*static testmethod void testRevalidateProductsBatch() {
        
        setupTestData();
 		Test.StartTest();
        List<Id> lstpc=new  List<Id>{config.Id};
        RevalidateProductsBatch bc=new RevalidateProductsBatch(lstpc);
        Database.executeBatch(bc, 1);
        Test.StopTest();
        
    }*/
	
}