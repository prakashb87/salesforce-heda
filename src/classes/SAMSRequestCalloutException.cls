/**
 * @description Custom Exception for any problems with SAMS callouts
 * This can include failing to recieve a valid response from IPaaS or 
 * sams and also any unintended responses.
 */
public class SAMSRequestCalloutException extends Exception {}