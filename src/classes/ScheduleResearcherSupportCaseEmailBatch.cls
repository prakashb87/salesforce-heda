/*******************************************
Name: ScheduleResearcherSupportCaseEmailSendBatch

Description : Scheduler class to schedule to batch class to send the support email case notification.


History:
*******************************************/

public without sharing class ScheduleResearcherSupportCaseEmailBatch implements Schedulable {
   public void execute(SchedulableContext sc) {
   
       ResearcherSupportCaseEmailSendBatch caseSupportEmailNotifcationBatch = new   ResearcherSupportCaseEmailSendBatch();
       Database.executeBatch(caseSupportEmailNotifcationBatch ); 
       
   }
}