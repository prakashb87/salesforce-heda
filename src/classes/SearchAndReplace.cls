public class SearchAndReplace implements Database.Batchable<sObject>{ 
    SearchAndReplace_Helper helperMethod;
    public List <hed__Affiliation__c> aff = new List<hed__Affiliation__c>();
    
    public SearchAndReplace(){
        helperMethod = new SearchAndReplace_Helper();
    }

    public Database.QueryLocator start(Database.BatchableContext BC){ 
      
		String aff = System.Label.Affiliation_type;
        return Database.getQueryLocator(aff);
   }    
	public void execute(Database.BatchableContext BC,List <hed__Affiliation__c> scope )
	{
        helperMethod.updateMethod(scope); 
	}
	public void finish(Database.BatchableContext BC){ 
		system.debug('SearchAndReplace/finish');    
   } 
			
}