/*
  *****************************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         : 16/06/2018 
  * @description  : Batch Class for sending Embedded Credential AutoPurchase Error records to 21CC Support team
  ******************************************************************************************************************
*/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class SendAutoPurchaseErrorEmailBatch implements Database.Batchable<sObject>,Database.Stateful
{
    global string finalstr='';
    global List<RMErrLog__c> errLogListToUpdate = new List<RMErrLog__c>();
    List<RMErrLog__c> errLogListBatchList = new List<RMErrLog__c>();
    
	global SendAutoPurchaseErrorEmailBatch( List<RMErrLog__c> errorLogItems ) 
    {  
        errLogListBatchList = errorLogItems;
        
    }
    global List<RMErrLog__c> start( Database.BatchableContext bc ) 
    { 
        System.debug('RRRR:SendAutoPurchaseErrorEmailBatch - Inside start()');       
        return errLogListBatchList;
	}
    global void execute(Database.BatchableContext bc, List<RMErrLog__c> scope) 
    {
        System.debug('RRRR:SendAutoPurchaseErrorEmailBatch -Inside Execute()' + scope.size());        
        try
        {
          
		    for(RMErrLog__c rmErrorLogObj:scope) 
		    {    
		    	if( rmErrorLogObj != null) 
		        {   
                    String failedRecordId;
                    String failedStudentId;
                    String failedCredentialName;
                    
                    if(!String.isBlank(rmErrorLogObj.Record_Id__c))
                    {
                        failedRecordId = rmErrorLogObj.Record_Id__c;
                    }
                    else
                    {
                        failedRecordId ='';
                    }
                    if( failedRecordId != '')
                    {
                        failedStudentId = failedRecordId.substringBefore('&');
						failedStudentId = failedStudentId.substringafter('=');
                                                
                        failedCredentialName = failedRecordId.substringAfter('&');
                        failedCredentialName = failedCredentialName.substringAfter('=');
                    }
                     
                     string recordString = '"'+rmErrorLogObj.Business_Function_Name__c+'","'+rmErrorLogObj.Error_Type__c+'","'+rmErrorLogObj.Error_Message__c+'","'+rmErrorLogObj.CreatedDate +'","'+failedStudentId+'","'+failedCredentialName+'"\n';
                     finalstr = finalstr +recordString;  
                    
                     rmErrorLogObj.Is_Admin_Notified__c = true;
                     errLogListToUpdate.add(rmErrorLogObj);
                }
            }
        }
        catch( Exception Ex)
        {
            System.debug('Exception in SendAutoPurchaseErrorEmailBatch:execute()' + Ex.getMessage()); 
        }
          
    }
    global void finish(Database.BatchableContext bc)
    {
        
        System.debug('RRRR:SendAutoPurchaseErrorEmailBatch -Inside finish()');
        String autoPurchseSupportEmail = ConfigDataMapController.getCustomSettingValue('AutoPurchaseSupportEmail');
        String autoPurchaseErrorEmailSubject = ConfigDataMapController.getCustomSettingValue('AutoPurchaseErrorEmailSubject');
        String autoPurchaseErrorFileName = ConfigDataMapController.getCustomSettingValue('AutoPurchaseErrorFileName');
        
         
	    EmailTemplate templateId = [SELECT id FROM EmailTemplate WHERE developerName = 'AutoPurchaseErrorReportEmail'];
    	String owEmailAddressId = generateOWEmailId(); 
        String loggedInUserId = UserInfo.getUserId();
        
	    
        
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        
        String header = 'Business Function Name, Error Type , Error Message, Created Date,FailedStudent Id,Failed Credential Name\n';
        finalstr = header + finalstr ;
        System.debug('RRRR:Final String in finish() =' + finalstr);
        
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= autoPurchaseErrorFileName;
        csvAttc.setFileName(csvname);
        
        csvAttc.setBody(csvBlob);
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new list<string> {autoPurchseSupportEmail};
        String subject = autoPurchaseErrorEmailSubject;
        email.setTemplateID(templateId.id);
        email.setToAddresses(toAddresses);
        email.setOrgWideEmailAddressId(owEmailAddressId);
        email.setTargetObjectId(loggedInUserId);						
 		email.setTreatTargetObjectAsRecipient(false);
        
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
        
        
        if( !errLogListToUpdate.isEmpty())
        {
            update errLogListToUpdate;
        }
        
    }
    global static String generateOWEmailId()
    {
        string owEmailAddressId;
		OrgWideEmailAddress owea = [SELECT Address,DisplayName FROM OrgWideEmailAddress where DisplayName = 'Embedded Credential Error Report' LIMIT 1];
		if ( owea != null ) 
		{
	    	owEmailAddressId = owea.Id;
		}
		else
        {
            owEmailAddressId ='';
        }
        return owEmailAddressId;
    }
    
}