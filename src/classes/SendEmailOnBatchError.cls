public class SendEmailOnBatchError {
    
    public static void sendEmailOnBatch(AsyncApexJob a,String settingName){
        RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues(settingName);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = csRmito.BatchFinishEmailAddresses__c.split(',');
        mail.setToAddresses(toAddresses);
        mail.setSubject('Match Merge Batch ' + a.Status);
        mail.setPlainTextBody('Records processed ' + a.TotalJobItems +   'with '+ a.NumberOfErrors + ' failures.');    
        if(a.NumberOfErrors > 0 && csRmito.EnableSendEmailBatchFinish__c){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
	public static void sendEmailOnException(DmlException e,String settingName){
        RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues(settingName);
        if(csRmito.EnableSendEmailBatchException__c == true){
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String[] toaddresses = csRmito.BatchExceptionEmailAddresses__c.split(',');
            email.setToAddresses(toaddresses);
            email.setSubject(System.Label.Batch_exception_email_subject);
            String finalMessage = System.Label.Batch_exception_email_string +'\n'+ e.getCause()+'\n'+e.getMessage()+'\n'+e.getStackTraceString();
            email.setHtmlBody(finalMessage);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }
}