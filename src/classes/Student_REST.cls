/*********************************************************************************
 *** @TestClassName     : Student_REST
 *** @Author            : Anupam Singhal 
 *** @Requirement       : Getting records from external system through json structure
                          and upsert records into salesforce object(Contact and addresses) 
 *** @Created date      : 25/05/2018
 *** @Modified by       : Anupam Singhal
 *** @Modified date     : 17/09/2018
 **********************************************************************************/
@RestResource(urlMapping='/HEDA/v1/Student')
@SuppressWarnings('PMD.TooManyFields,PMD.ExcessivePublicCount,PMD.VariableNamingConventions,PMD.MethodNamingConventions,PMD.AvoidGlobalModifier')
global with sharing class Student_REST {
    
    public String studentId;
    public String DateOfBirth;
    public String PlaceOfBirth;
    public String CountryOfBirth;
    public String StateOfBirth;
    public String DateOfDeath;
    
    public List<PersonalDetail> PersonalDetail;
    public List<Name> Name;
    public List<Address> Address;
    public List<Email> Email;
    public List<Phone> Phone;
    public List<AccountFlag> AccountFlag;
    
    public class PersonalDetail {
        public String studentId;
        public String gender;
        public String effectiveDate;
        public String maritalStatus;
        public String maritalStatusDate;
        public String highestEducationLevel;
        public String fullTimeStudent;
        public String languageCode;
        public String alternativestudentId;
    }
    
    public class Name {
        public String studentId;
        public String fullName;
        public String prefix;
        public String lastName;
        public String firstName;
        public String middleName;
        public String effectiveStatus;
        public String preferredFirstName;
        public String effectiveDate;
        public String displayName;
        public String nameType;
    }
    
    public class Address {
        public String studentId;
        public String addressType;
        public String effectiveDate;
        public String country;
        public String addressLine1;
        public String addressLine2;
        public String addressLine3;
        public String addressLine4;
        public String city;
        public String state;
        public String postalCode;
    }
    
    public class Email {
        public String studentId;
        public String emailType;
        public String emailAddress;
        public String preferedEmailIndicator;
    }
    
    public class Phone {
        public String studentId;
        public String phoneType;
        public String countryCode;
        public String phoneNumber;
        public String preferredPhoneIndicator;
    }

	@SuppressWarnings('PMD.TooManyFields,PMD.ExcessivePublicCount')
    public class AccountFlag {
        public String studentId;
        public String accountFlagDateTime;
        public String updatedBy;
        public String institution;
        public String flagCode;
        public String flagReason;
        public String flagActiveTerm;
        public String flagActiveDate;
        public String flagEndTerm;
        public String flagEndDate;
        public String positiveFlag;
        public String flagReference;
        public String departmentId;
        public String positionNumber;
        public String contact;
        public String contactId;
        public String currencyCode;
        public String amount;
        public String flaggedByMethod;
        public String flaggedByPerson;
        public String flaggedByPersonId;
        public String flaggedByProcess;
        public String unflagProcess;
        public String comments;
    }
    
    
    @HttpPost
    global static Map<String,String> parse() {
        //Variable Declaration
        Map<String,String> responsetoSend = new Map<String,String>();
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;  
        
        Map<String, Student_Rest> mapEmailPhoneStudent = new Map<String, Student_Rest>();
        
        List<hed__Address__c> allAddressesJSON = new List<hed__Address__c>();
       
        Map<String,Id> studentAndContactId = new Map<String,Id>();        
        
        try{
            // Request String
            String reqStr = '[' + request.requestBody.toString() +']';
            
            response.addHeader('Content-Type','application/json');
            List<Student_REST> postStringStu = (List<Student_REST>) System.JSON.deserialize(reqStr, List<Student_REST>.class);
            List<Contact> allContactJSON = new List<Contact>();
            
            Map<String, Student_Rest> mapIdStudent = Student_REST_Parse_Helper.getMepIdStudent(postStringStu, mapEmailPhoneStudent, allContactJSON);
            map<String, Id> mapContactStudent = new Map<string, ID>();
            map<String, Id> mapContactToUpdate = new Map<string, ID>();
			set<String> contactmatchingKey= new set<String>();
            for(Contact mapConUp:[SELECT ID,Student_ID__c,  SamsContactMatching__c 
            						FROM Contact 
            						WHERE Student_ID__c IN :mapIdStudent.keySet()]){
                                        system.debug('contact rec:'+mapConUp);
            	studentAndContactId.put(mapConUp.Student_ID__c,mapConUp.Id);
            	contactmatchingKey.add(mapConUp.SamsContactMatching__c)	;				
            }
            for(Contact mapCon:[SELECT ID , SamsContactMatching__c
            					FROM Contact 
            					WHERE SamsContactMatching__c IN : mapEmailPhoneStudent.keySet() 
            					AND Student_ID__c NOT IN : mapIdStudent.keySet()]){
            	if(!contactmatchingKey.contains(mapCon.SamsContactMatching__c)) {    
            		mapContactStudent.put(mapCon.SamsContactMatching__c,mapCon.Id);
            	}
          
            }
            
            mapContactToUpdate=Student_REST_Parse_Helper.processWithProspectAffiliationOnly(mapContactStudent);
            Student_REST_Parse_Helper.studentAndContactId=studentAndContactId;
            Student_REST_Parse_Helper.mapEmailPhoneStudent=mapEmailPhoneStudent;
            Student_REST_Parse_Helper.upsertAndUpdateContacts(allContactJSON, mapContactToUpdate);
			List<Contact> recentlyInsertedContacts = Student_REST_Parse_Helper.getRecentlyInsertedContacts(mapIdStudent);

            Set<Id> contactIds = new Set<id>();
            
            for(Contact mapCons: recentlyInsertedContacts){
                contactIds.add(mapCons.Id);
            }
            //Method call to insert address.
            for(Student_REST srn : mapIdStudent.values()){
                //allAddressesJSON.add(allAddress(srn,recentlyInsertedContacts));
                allAddressesJSON = allAddress(srn,recentlyInsertedContacts);                                
            }
            
            // Query to get all Address of related contacts (if any).
			List <hed__Address__c> existingAddress = Student_REST_Parse_Helper.getExistingAddresses(contactIds);
			Student_REST_Parse_Helper.upsertAddresses(existingAddress, allAddressesJSON);
            
        }catch(Exception excpt){
            responsetoSend.put('message', 'Oops!Student was not well defined, amigo.');
            responsetoSend.put('status', '404');
            
            //If exception occurs then return this message to IPASS
            return responsetoSend;
            
        }
        responsetoSend.put('message', 'success');
        responsetoSend.put('status', '200');
        return responsetoSend;
    }
    
    // Method to extract all contacts from Request.
    public static Contact allStudent(Student_REST postString){        
        Contact extractedContact = new Contact();       
       // extractedContact.External_Unique_ID__c = 'CN-ST:' + mapIdStudent.keySet();
		Student_REST_Helper.handleGeneralDetails(postString, extractedContact);
        
        // Parsing Personal Details
        /*for(Student_REST.PersonalDetail sr_personalDetail: postString.PersonalDetail){
            extractedContact.hed__Gender__c = sr_personalDetail.gender;
        }*/
        
        List<Date> personalEffDateList = new List<Date>();
        Map<Date, Student_Rest.PersonalDetail> mapOfPersonalEffDate = new Map<Date, Student_Rest.PersonalDetail>();
        Student_REST_Helper.handleGeneralDetails(postString, extractedContact);
        Student_REST_Helper.setEffectiveDatesFromPersonalDetails(postString, personalEffDateList, mapOfPersonalEffDate);
        Student_REST_Handler.handlePersonalDetails(personalEffDateList, mapOfPersonalEffDate, extractedContact);
        
        List<Date> effectiveDateList = new List<Date>();
        List<Date> effectiveDateListPRI = new List<Date>();
        List<Date> effectiveDateListPREF = new List<Date>();
        Map<Date, Student_REST.Name> mapOfdateandName = new Map<Date, Student_REST.Name>();
        Map<Date, Student_REST.Name> mapOfdatePREFandName = new Map<Date, Student_REST.Name>();
        Map<Date, Student_REST.Name> mapOfdatePRIandName = new Map<Date, Student_REST.Name>();
        
        // Parsing Name
        if(postString.Name != null){
            for(Student_REST.Name srn : postString.Name){
                extractedContact.Id = NULL;
                Student_REST_Helper.updateDateList(srn, effectiveDateList, mapOfdateandName);
                Student_REST_Helper.updateDateListPRI(srn, effectiveDateListPRI, mapOfdatePRIandName);
                Student_REST_Helper.updateDateListPRF(srn, effectiveDateListPREF, mapOfdatePREFandName);
            }
        }    
        if(effectiveDateList.size() > 0){
        	Student_REST_Helper.sortDateLists(effectiveDateList, effectiveDateListPRI, effectiveDateListPREF);	
            Integer listSizePRI;
            Integer listSize = effectiveDateList.size()-1;
            if(effectiveDateListPRI.size() > 0)
            {
                listSizePRI = effectiveDateListPRI.size()-1;
                extractedContact.Student_ID__c = mapOfdateandName.get(effectiveDateList[listSize]).studentId;
				Student_REST_Helper.handleEffectiveDatePRI(effectiveDateListPRI, mapOfdatePRIandName, extractedContact);
            }
            Student_REST_Helper.handleEffectiveDatePREF(effectiveDateListPREF, mapOfdatePREFandName, extractedContact);
            extractedContact.Effective_Date__c = Date.ValueOF(mapOfdateandName.get(effectiveDateList[listSize]).effectiveDate);            
            
            // Since JSON File has effectiveStatus for Contact is A for Active and I for Inactive whereas in Contact it is Active or Inactive
            Student_REST_Helper.setStatusAsPerEffectiveStatus(mapOfdateandName.get(effectiveDateList[listSize]), extractedContact);
        }
        
        // Parsing Emails
		Student_REST_Handler.handleEmails(postString.Email, extractedContact);

        try{
            // Parsing Phone
            if(postString.Phone != null){
				Student_REST_Handler.handlePhones(postString.Phone, extractedContact);
            }
        }catch(Exception excpt){
            System.debug('No phone for this Student');
            
        }

      	Student_REST_Handler.handleAccountFlags(postString.AccountFlag, extractedContact);

        return extractedContact;
    }
    
    //Method to Extract all addresses from request
   public static List<hed__Address__c> allAddress(Student_Rest postString, List<Contact> contactRecords){

        List<hed__Address__c> returningAddressList =  new List<hed__Address__c>();
        
        Map<String,Id> studentAndContactId = new Map<String,Id>();
        
        List<Date> effectiveDateAddressHome = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressHome = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressMail = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressMail = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressEduAgent = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressEduAgent = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressAuRes = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressAuRes = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressBusn = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressBusn = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressWorkplace = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressWorkplace = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressBill = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressBill = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressFax = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressFax = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressMobile = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressMobile = new Map<Date, Student_REST.Address>();
        
        List<Date> effectiveDateAddressOther = new List<Date>();
        Map<Date, Student_REST.Address> mapOfdateandAddressOther = new Map<Date, Student_REST.Address>();
        
        
        Map<String, List<Date>> typeEffectiveDateAddressMap=new Map<String, List<Date>>(); 
        Map<String, Map<Date, Student_REST.Address>> typeDateAddressMap=new Map<String, Map<Date, Student_REST.Address>>(); 
        
        typeEffectiveDateAddressMap.put('HOME', effectiveDateAddressHome);
        typeDateAddressMap.put('HOME', mapOfdateandAddressHome);
        typeEffectiveDateAddressMap.put('MAIL', effectiveDateAddressMail);
        typeDateAddressMap.put('MAIL', mapOfdateandAddressMail);
        typeEffectiveDateAddressMap.put('AGNT', effectiveDateAddressEduAgent);
        typeDateAddressMap.put('AGNT', mapOfdateandAddressEduAgent);
        typeEffectiveDateAddressMap.put('ARES', effectiveDateAddressAuRes);
        typeDateAddressMap.put('ARES', mapOfdateandAddressAuRes);
        typeEffectiveDateAddressMap.put('BUSN', effectiveDateAddressBusn);
        typeDateAddressMap.put('BUSN', mapOfdateandAddressBusn);
        typeEffectiveDateAddressMap.put('WKPL', effectiveDateAddressWorkplace);
        typeDateAddressMap.put('WKPL', mapOfdateandAddressWorkplace);
        typeEffectiveDateAddressMap.put('BILL', effectiveDateAddressBill);
        typeDateAddressMap.put('BILL', mapOfdateandAddressBill);
        typeEffectiveDateAddressMap.put('FAX', effectiveDateAddressHome);
        typeDateAddressMap.put('FAX', mapOfdateandAddressHome);
        typeEffectiveDateAddressMap.put('MOBL', effectiveDateAddressMobile);
        typeDateAddressMap.put('MOBL', mapOfdateandAddressMobile);
        typeEffectiveDateAddressMap.put('OTH', effectiveDateAddressOther);
        typeDateAddressMap.put('OTH', mapOfdateandAddressOther);
        
        if(postString.Address != null){
            for(Student_Rest.Address srnAddress: postString.Address){
				putAddressesIntoMap(srnAddress, typeEffectiveDateAddressMap, typeDateAddressMap);
            }
        }
        
        for(Contact mapCons: contactRecords){
            studentAndContactId.put(mapCons.Student_ID__c,mapCons.Id);    
        }
        
        //To Insert address of eash address type if getting them in the json
        Student_REST_Handler.studentAndContactId=studentAndContactId; 
        Student_REST_Handler.returningAddressList=returningAddressList;  
        Student_REST_Handler.addingExtractedAddress('Home',effectiveDateAddressHome,mapOfdateandAddressHome);
        Student_REST_Handler.addingExtractedAddress('Mailing',effectiveDateAddressMail,mapOfdateandAddressMail);
        Student_REST_Handler.addingExtractedAddress('Educational Agent',effectiveDateAddressEduAgent,mapOfdateandAddressEduAgent);
        Student_REST_Handler.addingExtractedAddress('Australian Residence',effectiveDateAddressAuRes,mapOfdateandAddressAuRes);
        Student_REST_Handler.addingExtractedAddress('Business',effectiveDateAddressBusn,mapOfdateandAddressBusn);
        Student_REST_Handler.addingExtractedAddress('Work Placement',effectiveDateAddressWorkplace,mapOfdateandAddressWorkplace);
        Student_REST_Handler.addingExtractedAddress('Billing',effectiveDateAddressBill,mapOfdateandAddressBill);
        Student_REST_Handler.addingExtractedAddress('Fax',effectiveDateAddressFax,mapOfdateandAddressFax);
        Student_REST_Handler.addingExtractedAddress('Mobile',effectiveDateAddressMobile,mapOfdateandAddressMobile);
        Student_REST_Handler.addingExtractedAddress('Other',effectiveDateAddressOther,mapOfdateandAddressOther);
     
        return returningAddressList;
    }
    
    private static void putAddressesIntoMap(Student_Rest.Address srnAddress, Map<String, List<Date>> typeEffectiveDateAddressMap, Map<String, Map<Date, Student_REST.Address>> typeDateAddressMap){
            	for (String addType: typeDateAddressMap.keySet()){
	                if(Student_REST_Parse_Helper.isEqualsAddressTypeWithPastDate(srnAddress, addType)){
	                    typeEffectiveDateAddressMap.get(addType).add(Date.valueOf(srnAddress.effectiveDate));
	                    typeDateAddressMap.get(addType).put(Date.valueOf(srnAddress.effectiveDate),srnAddress);
	                }
            	}
    }
    
    public static string formatMobilePhone(string strPhone){
    	string strResult=strPhone;
    	if(string.isNotBlank(strPhone)){
	    	Pattern p= Pattern.compile('[^0-9]');
	    	strResult=p.matcher(strPhone).replaceAll('');
    	}
    	return strResult;
    }
}