public with sharing class Student_REST_Parse_Helper {

	public static Map<String,Id> studentAndContactId;
	public static Map<String, Student_Rest> mapEmailPhoneStudent;

    public static Boolean isEqualsAddressTypeWithPastDate(Student_Rest.Address srnAddress, String addressType){
    	return (srnAddress.addressType == addressType && Date.valueOf(srnAddress.effectiveDate) <= System.today());
    }

	public static map<String, Id> processWithProspectAffiliationOnly(map<String, Id> mapContactStudent){
		 map<String, Id> mapContactToUpdate = new Map<string, ID>();
		 Id recTypeId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
            for(hed__Affiliation__c affiliation:[SELECT ID,hed__Contact__c,
            											 hed__Contact__r.SamsContactMatching__c
            									FROM hed__Affiliation__c 
            									WHERE hed__Contact__c IN: mapContactStudent.values() 
            									AND recordTypeId=:recTypeId]){
            	if(mapContactStudent.containsKey(affiliation.hed__Contact__r.SamsContactMatching__c)){
            		mapContactToUpdate.put(affiliation.hed__Contact__r.SamsContactMatching__c,affiliation.hed__Contact__c);
            	}			
            }
         return mapContactToUpdate;

	}
	public static Map<String, Student_Rest> getMepIdStudent(List<Student_REST> postStringStu, Map<String, Student_Rest> mapEmailPhoneStudent, List<Contact> allContactJSON){
			Map<String, Student_Rest> mapIdStudent = new Map<String, Student_Rest>();
            String emailPhone;
            // To store each student response with respect to Student ID
            for(Student_REST mapStudent: postStringStu){
                mapIdStudent.put(mapstudent.studentId, mapStudent);
               
            }
            Contact con;
            for(Student_REST srn : mapIdStudent.values()){
                // All Contact from JSON Request.
                con=Student_REST.allStudent(srn);
                emailPhone=Student_REST.formatMobilePhone(con.MobilePhone) + con.Email;
                mapEmailPhoneStudent.put(emailPhone.toLowerCase(), srn);
                allContactJSON.add(con);
                
            }           
			return mapIdStudent;
	}
	
	public static void upsertAddresses(List <hed__Address__c> existingAddress, List<hed__Address__c> allAddressesJSON){
            Map <String, hed__Address__c> keyAddressMap = new Map <String, hed__Address__c>();
	        List<hed__Address__c> addressToUpsert = new List<hed__Address__c>();
            
            for (hed__Address__c addCont : existingAddress){
                
                String keyAddress = addCont.hed__Parent_Contact__c + '_' + addCont.hed__Address_Type__c.touppercase() + '_' + addCont.hed__MailingStreet__c + '_' + addCont.hed__MailingPostalCode__c;
                //String NewKeyForAddr = addCont.hed__Parent_Contact__c+'_'+ addCont.hed__Address_Type__c.touppercase() + '_' + addCont.hed__MailingStreet__c + '_' + addCont.hed__MailingPostalCode__c;
                keyAddressMap.put(keyAddress,addCont);              
            }
            // Traverse in All Address and add to Upsert Address List.
            for (hed__Address__c allAdd : allAddressesJSON ){
                String keyAddressJSON = allAdd.hed__Parent_Contact__c + '_' + allAdd.hed__Address_Type__c.touppercase() + '_' + allAdd.hed__MailingStreet__c + '_' + allAdd.hed__MailingPostalCode__c;
                if (keyAddressMap.get(keyAddressJSON) != NULL){
                    allAdd.id = keyAddressMap.get(keyAddressJSON).id;
                }
                addressToUpsert.add(allAdd);            
            }
            
            if (!Schema.sObjectType.hed__Address__c.isCreateable() || !Schema.sObjectType.hed__Address__c.isUpdateable()) {
				throw new DMLException();
			}
            upsert addressToUpsert;
	}
	
	public static void upsertAndUpdateContacts(List<Contact> allContactJSON, map<String, Id> mapContactToUpdate){
            List<Contact> contactToUpdate = new List<Contact>();
            List<Contact> contactToUpsert = new List<Contact>();
            for(Contact conAll : allContactJSON){
               	if (studentAndContactId.get(conAll.Student_ID__c) != Null){
                        conAll.Id = studentAndContactId.get(conAll.Student_ID__c);  
                }
                String strkeys=ConAll.MobilePhone + ConAll.Email;
                if(mapContactToUpdate.containsKey(strKeys.toLowerCase())){
                	conAll.Id=mapContactToUpdate.get(strKeys.toLowerCase());
                	conAll.Student_ID__c=mapEmailPhoneStudent.get(strKeys.toLowerCase()).studentId;
                    contactToUpdate.add(conAll);
                }
                contactToUpsert.add(conAll);
             }

			updateUpsertContacts(contactToUpdate, contactToUpsert);
	}
	
	public static void updateUpsertContacts(List<Contact> contactToUpdate, List<Contact> contactToUpsert){
            if(!contactToUpdate.isEmpty()){
            	if (!Schema.sObjectType.Contact.isUpdateable()) {
            		throw new DMLException();
            	}
                	update contactToUpdate;
            }
             if(!contactToUpsert.isEmpty()){
            	if (!Schema.sObjectType.Contact.isUpdateable() || !Schema.sObjectType.Contact.isCreateable()) {
            		throw new DMLException();
            	}
                upsert contactToUpsert Student_ID__c;
            }
	}
	
	public static List<Contact> getRecentlyInsertedContacts(Map<String, Student_Rest>  mapIdStudent){
			if (!Schema.sObjectType.Contact.isAccessible()) {
            		throw new DMLException();
            }
            List<Contact> recentlyInsertedContacts = [SELECT ID,Student_ID__c FROM Contact WHERE Student_ID__c IN :mapIdStudent.keySet() ];
            return recentlyInsertedContacts;
	}
	
	public static List <hed__Address__c> getExistingAddresses(Set<Id> contactIds){
			if (!Schema.sObjectType.hed__Address__c.isAccessible()) {
				throw new DMLException();
			}
	        List <hed__Address__c> existingAddress = [Select id, hed__Parent_Contact__c, hed__Address_Type__c, Effective_Date__c, hed__MailingPostalCode__c, hed__MailingStreet__c FROM hed__Address__c Where hed__Parent_Contact__c IN :contactIds ];
	        return existingAddress;
	}

}