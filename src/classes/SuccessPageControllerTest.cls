@isTest
public  class SuccessPageControllerTest 
{
	 @TestSetup
    static void makeData()
    {
    	List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();
        Config_Data_Map__c configDataObj1 =  new Config_Data_Map__c();
        configDataObj1.Name = 'MyDashBoardUrl';
        configDataObj1.Config_Value__c = 'https://ecbdev2-rmitheda.cs31.force.com/MarketPlace/s/myproduct';
        configurations.add(configDataObj1);
        Config_Data_Map__c configDataObj2 =  new Config_Data_Map__c();
        configDataObj2.Name = 'KeepShopping';
        configDataObj2.Config_Value__c = 'https://rmit-dev10.aemnpe.route53.aws.rmit.edu.au/study-with-us/levels-of-study/microcredentials';
        configurations.add(configDataObj2);
        Config_Data_Map__c configDataObj3 =  new Config_Data_Map__c();
        configDataObj3.Name = 'GoogleTagManagerUrl';
        configDataObj3.Config_Value__c = 'https://www.googletagmanager.com/ns.html?id=GTM-KZSMQQR';
        configurations.add(configDataObj3);
        Config_Data_Map__c configDataObj4 =  new Config_Data_Map__c();
        configDataObj4.Name = 'googleTagManagerID';
        configDataObj4.Config_Value__c = 'GTM-KZSMQQR';
        configurations.add(configDataObj4);
        
        Config_Data_Map__c configDataObj5 =  new Config_Data_Map__c();
        configDataObj5.Name = 'SurveyURL';
        configDataObj5.Config_Value__c = 'https://rmit.au1.qualtrics.com/jfe/form/SV_9FC04H9QhPql2N7';
        configurations.add(configDataObj5);

        insert configurations;
    }
    @isTest
    static void registerCustomerSuccessTest() 
    {
    	try
    	{
    		SuccessPageController ss = new SuccessPageController();
    	}    	
    	catch(Exception ex)
    	{
    		system.assert(false);
    	}

    }
	
}