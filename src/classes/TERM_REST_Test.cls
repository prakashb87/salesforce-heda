/*********************************************************************************
*** @TestClassName     : TERM_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Location_Rest web service apex class.
*** @Created date      : 27/08/2018
**********************************************************************************/
@isTest
public class TERM_REST_Test {
static testMethod void myUnitTest() {
      Account acc = new Account();
      acc.Name = 'RMITU';
      insert acc;
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Term';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","academicCareer": "UGRD","term": "0053","description": "UGRD Semester 1 1986","shortDescription": "Sem 1 1986","termCategory": "1","termBeginDate": "1986-02-19","termEndDate": "1986-06-29","academicYear": "1986","holidaySchedule": "ACAD","weeksOfInstruction": 17,"termType": "ONS"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = TERM_REST.upsertTerm();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Term';
        
        //creating test data
        String JsonMsg = '{"institution": "","academicCareer": "","term": "","description": "UGRD Semester 1 1986","shortDescription": "Sem 1 1986","termCategory": "1","termBeginDate": "1986-02-19","termEndDate": "1986-06-29","academicYear": "1986","holidaySchedule": "ACAD","weeksOfInstruction": 17,"termType": "ONS"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = TERM_REST.upsertTerm();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    /*static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = TERM_REST.upsertTerm();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }*/
    static testMethod void updateMethod() {
        Term__c term = new Term__c();
        Account instiAc = New Account(Name = 'RMITU');
        insert instiAc;
        term.Institution__c = instiAc.Id;
        
        term.Academic_Career__c = 'UGRD';
        
        term.Term_Code__c = '0053';
        
        term.Short_Description__c = 'this is to cover the update';
        
        insert term;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Term';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","academicCareer": "UGRD","term": "0053","description": "UGRD Semester 1 1986","shortDescription": "Sem 1 1986","termCategory": "1","termBeginDate": "1986-02-19","termEndDate": "1986-06-29","academicYear": "1986","holidaySchedule": "ACAD","weeksOfInstruction": 17,"termType": "ONS"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = TERM_REST.upsertTerm();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}