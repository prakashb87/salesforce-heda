/*********************************************************************************
 *** @ClassName         : TERM_SESSION_REST
 *** @Author            : Shubham Singh 
 *** @Requirement       : 
 *** @Created date      : 23/08/2018
 *** @Modified by       : Dinesh Kumar 
 *** @modified date     : 24/10/2018
 **********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Term_Session')
global with sharing class TERM_SESSION_REST {
    public String institution;
    public String academicCareer;
    public String term;
    public String session;
    public String sessionBeginDate;
    public String sessionEndDate;
    public String firstDateToEnrol;
    public String lastDateToEnrol;
    public String openEnrolDate;
    public String sessionWeeksOfInstruction;
    public String censusDate;
    public String fullTimeGradedDate;
    public string effectiveDate;
    public string sessionTimePeriod;
    
    
    @HttpPost
    global static Map < String, String > upsertTermSession() {
        //to create map of effective date        
        map < String, Date > updateEffectiveDate = new map < String, Date > ();
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert hed__Term__c
        list < hed__Term__c > upsertTermSession = new list < hed__Term__c > ();
        //hed__Term__c
        hed__Term__c termSession;

        try {
            //request and response variable
            RestRequest request = RestContext.request;

            RestResponse response = RestContext.response;

            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
    	    System.debug('PostString from IPaaS ------------> '+reqStr);
            response.addHeader('Content-Type', 'application/json');

            //getting the data from JSON and Deserialize it
            List < TERM_SESSION_REST > postString = (List < TERM_SESSION_REST > ) System.JSON.deserialize(reqStr, List < TERM_SESSION_REST > .class);
            //get institution
            list < String > listInstitution = new list < String > ();
            //get Career
            list < String > listAcademicCareer = new list < String > ();
            //get Term code
            list < String > listTermCode = new list < String > ();
            //getting term session
            list < String > listSession = new list < String > ();

            if (postString != null) {
                for (TERM_SESSION_REST termRest: postString) {
                    if (termRest.institution != null && (termRest.institution).length() > 0 && termRest.academicCareer != null && (termRest.academicCareer).length() > 0 && termRest.term != null && (termRest.term).length() > 0 && termRest.session != null && (termRest.session).length() > 0) {
                        listInstitution.add(termRest.institution);
                        listAcademicCareer.add(termRest.academicCareer);
                        listTermCode.add(termRest.term);
                        listSession.add(termRest.session);
                        if (termRest.effectiveDate != null && (termRest.effectiveDate).length() > 0){
                            updateEffectiveDate.put(termRest.academicCareer + '' + termRest.term + '' + termRest.session+ '' + termRest.institution , Date.ValueOf(termRest.effectiveDate));
                        }
                    } else {
                        responsetoSend = throwError();
                        return responsetoSend;
                    }

                }
            }

            //getting the old term for update
            list < hed__Term__c > previousTermSession = new list < hed__Term__c > ();
            if (listInstitution != null && listInstitution.size() > 0 && listAcademicCareer != null && listAcademicCareer.size() > 0 && listTermCode != null && listTermCode.size() > 0 && listSession != null && listSession.size() > 0) {
                previousTermSession = [SELECT 
                                       id,
                                       Session__c,
                                       hed__Account__c, 
                                       Institution__c,
                                       hed__Account__r.Name,
                                       Academic_Career__c, 
                                       Term_Code__c,
                                       Term_Code__r.Term_Code__c,
                                       Name, 
                                       Session_Begin_Date__c, 
                                       Session_End_Date__c, 
                                       First_Date_to_Enroll__c, 
                                       Last_Date_to_Enroll__c, 
                                       Open_Enroll_Date__c, 
                                       Session_Weeks_Of_Instruction__c, 
                                       Census_Date__c, 
                                       Full__c
                    		       FROM hed__Term__c 
                                       WHERE hed__Account__r.Name =: listInstitution 
                                       AND Academic_Career__c =: listAcademicCareer 
                                       AND Term_Code__r.Term_Code__c  =: listTermCode 
                                       AND Session__c =: listSession
                ];
            }
			
            //get institution from account (as one record will be there from JSON)
            Account institution;
            if (listInstitution != null && listInstitution.size() > 0 && Schema.sObjectType.Account.fields.Name.isAccessible()){
             	institution = [SELECT 
                               id,
                               Name 
                               FROM Account 
                               WHERE Name =: listInstitution[0] 
                               LIMIT 1];   
            }
          
            //get term details
            list < term__c > listTerm = new list < term__c > ();
            if (institution != null){
                listTerm = [SELECT id,
                            Description__c 
                            FROM term__c 
                            WHERE Institution__r.Name =: institution.name 
                            AND Academic_Career__c =: listAcademicCareer 
                            AND Term_Code__c =: listTermCode 
                            LIMIT 1];
            }
                
            //map to update the term session
            Map < String, hed__Term__c > oldTermSessionMap = new Map < String, hed__Term__c > ();
            String uniqKey;
            for (hed__Term__c trm: previousTermSession) {
                if (trm != null & trm != null) {
                    //not using the name as name is coming from term description
                    uniqKey = trm.Academic_Career__c + '' + trm.Term_Code__r.Term_Code__c+ '' + trm.Session__c + '' + trm.hed__Account__r.Name ;
                    oldTermSessionMap.put(uniqKey, trm);
                }
            }
            //insert or update the term session
            for (TERM_SESSION_REST termSessionRest: postString) {
                
                String key = termSessionRest.academicCareer +''+ termSessionRest.term+''+ termSessionRest.session +''+ termSessionRest.institution;
                termSession = new hed__Term__c();
                termSession.Term_Session_Key__c = key;
                termSession.Name = listTerm != null && listTerm.size() > 0 ? listTerm[0].Description__c : null;
                termSession.Session__c  = termSessionRest.session;
                termSession.hed__Account__c = institution.ID;
                termSession.Academic_Career__c = termSessionRest.academicCareer;
                termSession.Term_Code__c = listTerm != null && listTerm.size() > 0 ? listTerm[0].ID : null;
                termSession.Session_Begin_Date__c = termSessionRest.sessionBeginDate != null && (termSessionRest.sessionBeginDate).length() > 0 ? Date.valueOf(termSessionRest.sessionBeginDate) : null;
                termSession.Session_End_Date__c = termSessionRest.sessionEndDate != null && (termSessionRest.sessionEndDate).length() > 0 ? Date.valueOf(termSessionRest.sessionEndDate) : null;
                termSession.First_Date_to_Enroll__c = termSessionRest.firstDateToEnrol != null && (termSessionRest.firstDateToEnrol).length() > 0 ? Date.valueOf(termSessionRest.firstDateToEnrol) : null;
                termSession.Last_Date_to_Enroll__c = termSessionRest.lastDateToEnrol != null && (termSessionRest.lastDateToEnrol).length() > 0 ? Date.valueOf(termSessionRest.lastDateToEnrol) : null;
                termSession.Open_Enroll_Date__c = termSessionRest.openEnrolDate != null && (termSessionRest.openEnrolDate).length() > 0 ? Date.valueOf(termSessionRest.openEnrolDate) : null;
                termSession.Session_Weeks_Of_Instruction__c = termSessionRest.sessionWeeksOfInstruction;
                termSession.Census_Date__c = termSessionRest.censusDate != null && (termSessionRest.censusDate).length() > 0 ? Date.valueOf(termSessionRest.censusDate) : null;
                termSession.Full__c = termSessionRest.fullTimeGradedDate != null && (termSessionRest.fullTimeGradedDate).length() > 0 ? Date.valueOf(termSessionRest.fullTimeGradedDate) : null;
                termSession.Description__c = listTerm != null && listTerm.size() > 0 ? listTerm[0].Description__c : null;
				termSession.Session_Time_Period__c = termSessionRest.sessionTimePeriod != null && termSessionRest.sessionTimePeriod.length() > 0? termSessionRest.sessionTimePeriod: null;
                if (oldTermSessionMap.size() > 0 && oldTermSessionMap.get(key) != null) {
                    termSession.ID = oldTermSessionMap.get(key).ID;
                }
                upsertTermSession.add(termSession);
            }
            //upsert term session
            if (upsertTermSession != null && upsertTermSession.size() > 0) {
                //upsert upsertTermSession Term_Session_Key__c;
                upsert upsertTermSession;        
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
		
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Term was not well defined, amigo');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}