/*********************************************************************************
*** @TestClassName     : TERM_SESSION_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Location_Rest web service apex class.
*** @Created date      : 27/08/2018
**********************************************************************************/
@isTest
public class TERM_SESSION_REST_Test {
    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Term_Session';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","academicCareer": "NONA","term": "0096","session": "RAS","sessionBeginDate": "2000-02-19","sessionEndDate": "2000-06-29","firstDateToEnrol": "1999-12-01","lastDateToEnrol": "","openEnrolDate": "1999-12-01","sessionWeeksOfInstruction": 17,"censusDate": "2000-03-31","fullTimeGradedDate": "2000-07-13"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = TERM_SESSION_REST.upsertTermSession();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Term_Session';
        
        //creating test data
        String JsonMsg = '{"institution": "","academicCareer": "","term": "0096","session": "RAS","sessionBeginDate": "2000-02-19","sessionEndDate": "2000-06-29","firstDateToEnrol": "1999-12-01","lastDateToEnrol": "","openEnrolDate": "1999-12-01","sessionWeeksOfInstruction": 17,"censusDate": "2000-03-31","fullTimeGradedDate": "2000-07-13"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = TERM_SESSION_REST.upsertTermSession();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = TERM_SESSION_REST.upsertTermSession();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        Account acc = new Account();
        acc.Name = 'RMITU';
        insert acc;
        
        hed__Term__c termSession = new hed__Term__c();
        //need to comment if changed to auto number;
        termSession.Name = '';
        
        termSession.hed__Account__c = acc.ID;
        termSession.Academic_Career__c = 'NONA';
        //termSession.Term_Code__c = '0096';
        termSession.Name = 'RAS';
        insert termSession;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Term_Session';
        
        //creating test data
        String JsonMsg = '{"institution": "RMITU","academicCareer": "NONA","term": "0096","session": "RAS","sessionBeginDate": "2000-02-19","sessionEndDate": "2000-06-29","firstDateToEnrol": "1999-12-01","lastDateToEnrol": "","openEnrolDate": "1999-12-01","sessionWeeksOfInstruction": 28,"censusDate": "2000-03-31","fullTimeGradedDate": "2000-07-13"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = TERM_SESSION_REST.upsertTermSession();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}