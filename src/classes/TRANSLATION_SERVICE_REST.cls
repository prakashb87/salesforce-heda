/*********************************************************************************
 *** @ClassName         : TRANSLATION_SERVICE_REST
 *** @Author            : Shubham Singh 
 *** @Requirement       : 
 *** @Created date      : 23/08/2018
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 23/08/2018
 **********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Translation_Service')
global with sharing class TRANSLATION_SERVICE_REST {

    public String category;
    public String value;
    public String effectiveDate;
    public String effectiveStatus;
    public String translatedValue;
    public String translatedShortValue;
    public String lastUpdated;
    public String lastUpdatedBy;

    @HttpPost
    global static Map < String, String > upsertTranslationService() {
        //to create map of effective date        
        map < String,Date > updateEffectiveDate = new map < String,Date >(); 
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Translation_Service__c
        list < Translation_Service__c > upsertTranslationService = new list < Translation_Service__c > ();
        //Translation_Service__c
        Translation_Service__c translationservice;

   try {

        //request and response variable
        RestRequest request = RestContext.request;

        RestResponse response = RestContext.response;

        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';

        response.addHeader('Content-Type', 'application/json');

        //getting the data from JSON and Deserialize it
        List < TRANSLATION_SERVICE_REST > postString = (List < TRANSLATION_SERVICE_REST > ) System.JSON.deserialize(reqStr, List < TRANSLATION_SERVICE_REST > .class);
        //getting translationservice Name
        list < String > updateCategory = new list < String > ();
        //getting translationservice Field to be translated
        list < String > updateValue = new list < String > ();
        if (postString != null) {
            for (TRANSLATION_SERVICE_REST translationservicerest: postString) {
                if (translationservicerest.category != null && (translationservicerest.category).length() > 0 && translationservicerest.value != null && (translationservicerest.value).length() > 0) {
                    updateCategory.add(translationservicerest.category);
                    updateValue.add(translationservicerest.value);
                    if(translationservicerest.effectiveDate != null && (translationservicerest.effectiveDate).length() > 0)
                    {
                        updateEffectiveDate.put(translationservicerest.category+''+translationservicerest.value,Date.ValueOf(translationservicerest.effectiveDate));
                    }
                } else {
                    //throw error
                    responsetoSend = throwError();
                    return responsetoSend;
                }

            }
        }

        //getting the old translationservice for update
        list < Translation_Service__c > previousTranslationService = new list < Translation_Service__c > ();
        if (updateCategory != null && updateCategory.size() > 0 && updateValue != null && updateValue.size() > 0) {
            previousTranslationService = [SELECT Id,Translation_Service_Name__c,Field_To_be_Translated__c, Effective_date__c, Effective_status__c, Translated_Value__c, Translated_short_value__c, Last_updated_date__c, Last_Updated_By__c
                FROM Translation_Service__c WHERE Translation_Service_Name__c =: updateCategory[0] AND Field_To_be_Translated__c =: updateValue[0]
            ];
        }
        system.debug('previousTranslationService'+previousTranslationService);
        //map to update the previous Translation service
        Map < String, Translation_Service__c > oldTranslationService = new Map < String, Translation_Service__c > ();        
        for (Translation_Service__c ts: previousTranslationService) {
            if(ts.Translation_Service_Name__c != null && ts.Field_To_be_Translated__c != null){
                String uniqKey = ts.Translation_Service_Name__c + '' + ts.Field_To_be_Translated__c;
                if (updateEffectiveDate != null && updateEffectiveDate.get(uniqKey) != null ) {
                    if(updateEffectiveDate.get(uniqKey) <= System.today() && ts.Effective_date__c <= updateEffectiveDate.get(uniqKey)){
                        oldTranslationService.put(uniqKey, ts);    
                    }else if(ts.Effective_date__c > updateEffectiveDate.get(uniqKey)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                    
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend;
                }
            }
        }
        system.debug('oldTranslationService'+oldTranslationService);
        //insert or update the translationservice
        for (TRANSLATION_SERVICE_REST translationserviceRest: postString) {
            String key = translationserviceRest.category + '' + translationserviceRest.value;
            translationservice = new Translation_Service__c();
            translationservice.Translation_Service_Name__c = translationserviceRest.category;
            translationservice.Field_To_be_Translated__c = translationserviceRest.value;
            translationservice.Effective_date__c = translationserviceRest.effectiveDate != null ? Date.valueOf(translationserviceRest.effectiveDate) : null;
            translationservice.Effective_status__c = translationserviceRest.effectiveStatus != null && translationserviceRest.effectiveStatus.length() > 0?translationserviceRest.effectiveStatus == 'A'?'Active':'Inactive':null;
            translationservice.Translated_Value__c = translationserviceRest.translatedValue;
            translationservice.Translated_short_value__c = translationserviceRest.translatedShortValue;
            translationservice.Last_updated_date__c = translationserviceRest.lastUpdated != null ? Date.valueOf(translationserviceRest.lastUpdated) : null;
            translationservice.Last_Updated_By__c = translationserviceRest.lastUpdatedBy;
            translationservice.TraslationService_key__c = translationserviceRest.category + '' + translationserviceRest.value;
            
             if (oldTranslationService.size() > 0 && oldTranslationService.get(translationserviceRest.category + '' + translationserviceRest.value) != null) 
             {
                    if(oldTranslationService.get(translationserviceRest.category + '' + translationserviceRest.value).Effective_date__c <= translationservice.Effective_date__c )
                    {
                        translationservice.ID = oldTranslationService.get(key).ID;
                    }
                    else
                    {
                        translationservice = null;
                    }
             }
             if(translationservice !=null)
             {
                upsertTranslationService.add(translationservice);
             }
        }
        //upsert translationservice
        if (upsertTranslationService != null && upsertTranslationService.size() > 0 && upsertTranslationService[0].Effective_date__c <= System.today()) {
            upsert upsertTranslationService TraslationService_key__c;
        }
        responsetoSend.put('message', 'OK');
        responsetoSend.put('status', '200');
        return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }

    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Translation Service was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}