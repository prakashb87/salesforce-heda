@isTest
public without sharing class TandCLinkageTest 
{
	
	 @TestSetup
    static void makeData()
    {
    	Id courseRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();   

    	Config_Data_Map__c csDataMapObj = new Config_Data_Map__c();
        csDataMapObj.Name = '21CCRecordId';
        csDataMapObj.Config_Value__c = courseRecordTypeId;
        insert csDataMapObj;

        Config_Data_Map__c csProductType = new Config_Data_Map__c();
        csProductType.Name = 'ProductType' ;
        csProductType.Config_Value__c = 'Micro Creds';
        insert csProductType;

    	Account accObj = new Account(name = 'test Account');
        insert accObj;
                
        hed__Course__c course = new hed__Course__c(status__c = 'enabled', Name = 'Test Course Name', hed__Account__c = accObj.id, RecordTypeId=courseRecordTypeId, Type__c = '21CC');
        insert course;

        Terms_and_Conditions__c termsObj = new Terms_and_Conditions__c(Active__c = true, Long_Description__c ='abcdefghijklmnopqrstuvwxyz', Title__c='TestT&C', Version__c=1.0,Product_Type__c ='Micro Creds');
        insert termsObj;
    }
    static testMethod void testPositiveScenario()
    {
    	TandCLinkageClass.updateTandC();
    	List<ProductTC__c> prodTermsCondList = new  List<ProductTC__c>();
    	prodTermsCondList = [SELECT id from ProductTC__c ];
    	system.assert(prodTermsCondList.size()>0);
    }

}