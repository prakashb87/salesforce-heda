/**************************************************************
Purpose    : Test class for the the ChatterFeedController
Created by : Prakash 13/03/2019
History    :
*************************************************************/
@isTest(SeeAllData = true)
public class TestChatterFeedController {
    
    public static testmethod void insertNewFeed(){
        
        ResearcherPortalProject__c rpp = new ResearcherPortalProject__c();
        rpp.Project_Ecode__c = 'TestChatter12345';
        rpp.Title__c = 'Test Chatter Feed';
        rpp.Status__c = 'Approved';
        Insert rpp;
        
        Id projectId = [SELECT ID FROM ResearcherPortalProject__c WHERE Project_Ecode__c = 'TestChatter12345'].Id;
        
        Test.startTest();
        
        ChatterFeedController.createFeedItem('Test Comment', projectId);
        
        Test.stopTest();    
        System.assertEquals(1,[SELECT COUNT() FROM FeedItem WHERE ParentId =: projectId]);
        
    }
    
    public static testmethod void insertNewFeedComment(){
        
        ResearcherPortalProject__c rpp = new ResearcherPortalProject__c();
        rpp.Project_Ecode__c = 'TestChatter12345';
        rpp.Title__c = 'Test Chatter Feed';
        rpp.Status__c = 'Approved';
        Insert rpp;
        
        Id projectId = [SELECT ID FROM ResearcherPortalProject__c WHERE Project_Ecode__c = 'TestChatter12345'].Id;
        
        Test.startTest();
        
        ChatterFeedController.createFeedItem('Test Comment', projectId);
        ID feedId = [SELECT ID FROM FeedItem WHERE ParentID =: projectId].Id;
        ChatterFeedController.createCommentForFeed(feedId, 'Test Feed Comment');
        
        Test.stopTest();    
        System.assertEquals(1,[SELECT COUNT() FROM FeedItem WHERE ParentId =: projectId]);
        
    }
    
    public static testmethod void loadExistingComments(){
        
        ResearcherPortalProject__c rpp = new ResearcherPortalProject__c();
        rpp.Project_Ecode__c = 'TestChatter12345';
        rpp.Title__c = 'Test Chatter Feed';
        rpp.Status__c = 'Approved';
        Insert rpp;
        
        Id projectId = [SELECT ID FROM ResearcherPortalProject__c WHERE Project_Ecode__c = 'TestChatter12345'].Id;
        
        ChatterFeedController.createFeedItem('Test Comment', projectId);
        ID feedId = [SELECT ID FROM FeedItem WHERE ParentID =: projectId].Id;
        ChatterFeedController.createCommentForFeed(feedId, 'Test Feed Comment');
        
        Test.startTest();
        
        ChatterFeedController.getLoggedInuserId();
        ChatterFeedController.getChatterComments(projectId);
        
        Test.stopTest();    
        System.assertEquals(1,[SELECT COUNT() FROM FeedItem WHERE ParentId =: projectId]);
        
    }
  
    public static testmethod void deleteFeedComments(){
              
        ResearcherPortalProject__c rpp = new ResearcherPortalProject__c();
        rpp.Project_Ecode__c = 'TestChatter12345';
        rpp.Title__c = 'Test Chatter Feed';
        rpp.Status__c = 'Approved';
        Insert rpp;
        System.assert(rpp!=null); 
        
        Id projectId = [SELECT ID FROM ResearcherPortalProject__c WHERE Project_Ecode__c = 'TestChatter12345'].Id;
        
        
        ChatterFeedController.createFeedItem('Test Comment', projectId);
        ID feedId = [SELECT ID FROM FeedItem WHERE ParentID =: projectId].Id;
        ChatterFeedController.createCommentForFeed(feedId, 'Test Feed Comment');
        ID feedcomId = [SELECT ID FROM feedcomment WHERE feeditemid=: feedId ].Id;
        System.debug('feedcomId'+feedcomId);
        
        
        ResearcherPortalProject__Feed projectFeed1 = [SELECT  Id,Body, CreatedById, CreatedDate,Type,(SELECT Id, CommentBody, CreatedDate, FeedItemId, InsertedById, LastEditDate,CreatedById
                                                                                                      FROM FeedComments) FROM ResearcherPortalProject__Feed WHERE Id =:feedId];
        system.debug('###projectFeed1:'+projectFeed1);
        Test.startTest();
        
       	ChatterFeedController.deleteChatterPost(feedcomId,'feedComment',projectFeed1.id);
    
        Test.stopTest(); 
        
        
    }
   
}