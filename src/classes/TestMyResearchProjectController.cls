/*******************************************
Purpose: Test class for Project list from Research Master and Research Portal
History:
Created by Esther Ethelbert on 21/09/2018
*******************************************/
@isTest
public class TestMyResearchProjectController {

    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
    
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
    // Purpose      :  Test functionality for Create ResearcherPortal Project Helper
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @isTest
    public static  void testResearcherPortalProjectUtilMethod (){

        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        system.runAs(usr)
           {

            Contact con = new Contact();
            con.FirstName='xyz';
            con.LastName='abc';
            con.hed__UniversityEmail__c='abc@rmit.edu.au';
            con.Enumber__c='test1';
            insert con;
            system.assert(con!=null);
               
            String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId();
            String access = 'Private';
            List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
            system.assert(projectObjList.size() > 0);
  
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch .Contact__c = con.Id;
            memberresearch.User__c = usr.Id;
            insert memberresearch;
            system.assert(memberResearch!=null);
            
            Ethics__c ethicObj = new Ethics__c();
            ethicObj.Approved_Date__c = date.newInstance(2018, 09, 28);
            ethicObj.Expiry_Date__c = date.newInstance(2018, 10, 15);
            ethicObj.Ethics_Id__c='100002138';   
            insert ethicObj;
            system.assert(ethicObj!=null);
            
            List<id> ethicId = new List<id>();
            ethicId.add(ethicObj.Id); 
            system.assert(ethicId!=null);
            
            List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
            
            ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'RMIT');
            ProjectDetailsWrapper.ContactDetail conExtendDetail = new ProjectDetailsWrapper.ContactDetail(con.id);
            ProjectDetailsWrapper.ContactDetail conExtend= new ProjectDetailsWrapper.ContactDetail(memberResearch );
            conDetailList.add(conDetail);
            conDetailList.add(conExtendDetail);
            conDetailList.add(conExtend);
            system.assert(conDetailList!=null);
            
            ProjectDetailsWrapper.ResearchProjectListView researchView = new ProjectDetailsWrapper.ResearchProjectListView(projectObjList[0]);
            
            ProjectDetailsWrapper.ResearchProjectDetailData wrapperListView1 = new ProjectDetailsWrapper.ResearchProjectDetailData(projectObjList[0]);
            
            
            
            String serailizedString = '[{"contactEmail":"ankit@rmit.edu.au","contactName":"Ankit Bhagat","memberRole":"Researcher"}]' ;
                    
            ProjectDetailsWrapper.ResearchProjectDetailData wrapperListView2 = new ProjectDetailsWrapper.ResearchProjectDetailData();
            wrapperListView2.groupStatus='Active';
            wrapperListView2.projectStartDate=Date.newInstance(2011, 3, 22);
            wrapperListView2.projectEndDate=Date.newInstance(2019, 05, 10);
            String str1 = JSON.serialize(wrapperListView2); 

            Test.StartTest();
            
            MyResearchProjectController.getMyResearchProjects();
            
            MyResearchProjectController.getProjectDetails(projectObjList[0].Id);
            
            MyResearchProjectController.searchContactResults('abc');
            
            MyResearchProjectController.getProjectEthicsWrapper(projectObjList[0].Id);       
            
            MyResearchProjectController.deleteEthicsProject(ethicObj.Id,projectObjList[0].Id);
            
            MyResearchProjectController.searchEthicswithTitle('xyz',true); 
            
            MyResearchProjectController.addMembers(serailizedString,projectObjList[0].Id);
            
            MyResearchProjectController.deleteResearcherMemberSharing(serailizedString,projectObjList[0].Id);
            
            MyResearchProjectController.addEthicsProjectRecords(ethicId ,projectObjList[0].Id );
            
            MyResearchProjectController.getProjectContractWrapperList(projectObjList[0].Id);
            
            MyResearchProjectController.updatestatusvalueList(str1,projectObjList[0].Id);   
            Test.StopTest();
            }
    }
    
    /************************************************************************************
    // Purpose      :  Negative Test functionality for Create ResearcherPortal Project Helper
    // Developer    :  Shalu
    // Created Date :  11/22/2018                 
    //***********************************************************************************/
    @isTest
    public static  void testResearcherPortalProjectUtilMethodNegative (){

        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        system.runAs(usr)
           {
            Boolean result = false;
            Contact con = new Contact();
            con.FirstName='xyz';
            con.LastName='';
            con.hed__UniversityEmail__c='abc@rmit.edu.au';
            con.Enumber__c='test1';
            
            try
            {
                insert con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
            String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId();
            String access = 'Private';
            List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
            system.assert(projectObjList.size() > 0);
  			ResearcherPortalProject__c researcherproject = new ResearcherPortalProject__c();
               
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
           
            try
            {
                memberResearch .Contact__c = con.Id;
                memberresearch.User__c = usr.Id;
                insert memberresearch;   
            }
            catch(Exception ex)
            {
                result =true;                   
               
            }  
            system.assert(result);
            Ethics__c ethicObj = new Ethics__c();
            ethicObj.Approved_Date__c = date.newInstance(2018, 09, 28);
            ethicObj.Expiry_Date__c = date.newInstance(2018, 10, 15);
            ethicObj.Ethics_Id__c=null;   
            //insert ethicObj;
            //system.assert(ethicObj!=null);
            
            List<id> ethicId ;
            //ethicId.add(ethicObj.Id); 
            system.assert(ethicId==null);
            
            List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
            
            ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'RMIT');
            ProjectDetailsWrapper.ContactDetail conExtendDetail = new ProjectDetailsWrapper.ContactDetail(con.id);
            ProjectDetailsWrapper.ContactDetail conExtend= new ProjectDetailsWrapper.ContactDetail(memberResearch );
            conDetailList.add(conDetail);
            conDetailList.add(conExtendDetail);
            conDetailList.add(conExtend);
            system.assert(conDetailList!=null);
            
            ProjectDetailsWrapper.ResearchProjectListView researchView = new ProjectDetailsWrapper.ResearchProjectListView(projectObjList[0]);
            
            ProjectDetailsWrapper.ResearchProjectDetailData wrapperListView1 = new ProjectDetailsWrapper.ResearchProjectDetailData(projectObjList[0]);
            
            
            
            String serailizedString = '[{"contactEmail":"ankit@rmit.edu.au","contactName":"Ankit Bhagat","memberRole":"Researcher"}]' ;
            //use case for exception handling   
            String serailizedString1 = '[{"contactEmail":"","contactName":"Ankit Bhagat","":"Researcher"}]' ;
            String exception1 = '[]';   
            Test.StartTest();
            
            MyResearchProjectController.getMyResearchProjects();
            
            MyResearchProjectController.getProjectDetails(researcherproject.Id);
            
            MyResearchProjectController.searchContactResults(exception1);
            
            MyResearchProjectController.getProjectEthicsWrapper(researcherproject.Id);       
            
            MyResearchProjectController.deleteEthicsProject(ethicObj.Id,researcherproject.Id);
            
            MyResearchProjectController.searchEthicswithTitle(exception1,true); 
            
            MyResearchProjectController.addMembers(serailizedString1,researcherproject.Id);
            
            MyResearchProjectController.deleteResearcherMemberSharing(serailizedString1,researcherproject.Id);
            
            MyResearchProjectController.addEthicsProjectRecords(null ,null );
           
            MyResearchProjectController.getProjectContractWrapperList(projectObjList[0].Id);
            Test.StopTest();
            }
    }
    
    @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {   
        	
        	String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
            String access = 'Private';
            List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
            system.assert(projectObjList.size() > 0);
            
            Test.startTest();
            MyResearchProjectController.isForceExceptionRequired=true;
            MyResearchProjectController.getMyResearchProjects();
            MyResearchProjectController.getProjectDetails(projectObjList[0].Id);
            MyResearchProjectController.searchContactResults(null);
            MyResearchProjectController.getProjectEthicsWrapper(null);       
            MyResearchProjectController.deleteEthicsProject(null,null);
            MyResearchProjectController.searchEthicswithTitle('except',true); 
            MyResearchProjectController.addMembers(null,null);
            MyResearchProjectController.deleteResearcherMemberSharing(null,null);
            MyResearchProjectController.addEthicsProjectRecords(null ,null );
            MyResearchProjectController.getProjectContractWrapperList(projectObjList[0].Id);
            Test.stopTest();
        }
    }
    
}