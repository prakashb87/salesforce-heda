@isTest
public class TestProgramEnrollmentTrigger_RSTP {
    //Testing all the four Events
    @isTest static void pEToContactUpdateAndAffiliation() {
        test.startTest();
        Account acc = new Account(Name = 'Test Account');
        insert acc;
//        Contact con = new Contact(lastname = 'Test Contact');
        Contact con = new Contact();
        con.FirstName='xyz';
        con.LastName='abc';
        con.hed__UniversityEmail__c='abc@rmit.edu.au';
        con.Enumber__c='e100';
        insert con;

        con.IsAcademicStaff__c=true;
        try {
        	update con;
        } catch (Exception e) {system.debug(con);}
        
        list < hed__Program_Enrollment__c > progList = new list < hed__Program_Enrollment__c > ();
        hed__Program_Enrollment__c triggerRecord = new hed__Program_Enrollment__c(hed__Contact__c = con.id, Program_Action__c = 'ACTV', Action_Date__c = Date.newInstance(2016, 12, 9), hed__Graduation_Year__c = '2015');
        triggerRecord.hed__Account__c = acc.id;
        progList.add(triggerRecord);

        //Inserting Researcher PE
        hed__Program_Enrollment__c triggerRecord1 = new hed__Program_Enrollment__c(hed__Contact__c = con.id, Career__c='RSCH', Program_Action__c = 'DR01', program_status__c='AC', Action_Date__c = Date.newInstance(2015, 10, 8), hed__Graduation_Year__c = '2010');
        triggerRecord1.hed__Account__c = acc.id;
        progList.add(triggerRecord1);


        try {
	        insert progList;

        triggerRecord1.Career__c='NonResearcher';
		update progList;

        triggerRecord.Program_Action__c = 'COMP';
        Update triggerRecord;
        System.assertNotEquals(con.Program_Action__c,'DISC');
        Delete triggerRecord;
        Delete triggerRecord1;
        Undelete triggerRecord1;
        } catch (Exception e) {system.debug(triggerRecord1);}
        test.stopTest();
    }

}