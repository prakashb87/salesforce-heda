/*******************************************
Purpose: Test class ProjectDetailsWrapper
History:
Created by Ankit Bhagat on 21/09/2018
*******************************************/
@isTest
public class TestProjectDetailsWrapper {

 @isTest
public static void myUnitTest(){
    
    
    Contact con = new Contact();
    con.FirstName='xyz';
    con.LastName='abc';
    con.hed__UniversityEmail__c='abc@rmit.edu.au';
    insert con;
    system.assert(con!=null);

    List<ResearcherPortalProject__c> researcherPortalList = new List<ResearcherPortalProject__c>();
    
    ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
    researchProject.Title__c = 'title1';
    researchProject.Access__c='Private';
    researchProject.Approach_to_impact__c='test1';
    researchProject.Discipline_area__c='0101 - PURE MATHEMATICS';
    researchProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
    researchProject.End_Date__c=date.newInstance(2018, 12, 14);
    researchProject.Impact_Category__c='Social';
    researchProject.Portal_Project_Status__c='Planning';
    researchProject.RecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId(); 
    insert researchProject;
    researcherPortalList.add(researchProject);
    system.assert(researchProject!=null);


    
    ResearcherPortalProject__c researchRMProject = new ResearcherPortalProject__c();
    researchRMProject.Title__c = 'title2';
    researchRMProject.Access__c='RMIT Public';
    researchRMProject.Approach_to_impact__c='test2';
    researchRMProject.Discipline_area__c='0101 - PURE MATHEMATICS';
    researchRMProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
    researchRMProject.End_Date__c=date.newInstance(2018, 12, 14);
    researchRMProject.Impact_Category__c='Social';
    researchRMProject.Portal_Project_Status__c='Planning';
    researchRMProject.RecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId(); 
    insert researchRMProject;
    researcherPortalList.add(researchRMProject);
    system.assert(researchRMProject!=null);

    Research_Project_Classification__c classification = new Research_Project_Classification__c();
    classification.For_Six_Digit_Detail__c ='123454- Applied Mathematics';
    classification.For_Four_Digit_Detail__c ='1234 - Test For Code';
    classification.ResearcherPortalProject__c = researchRMProject.Id;
    
    Research_Project_Classification__c classification2 = new Research_Project_Classification__c();
    classification2.For_Six_Digit_Detail__c ='109344- Applied Mathematics';
    classification2.For_Four_Digit_Detail__c ='0101 - Test For Code';
    classification2.ResearcherPortalProject__c = researchRMProject.Id;
    
    Research_Project_Classification__c classification3 = new Research_Project_Classification__c();
    classification3.For_Six_Digit_Detail__c ='099876- Applied Mathematics';
    classification3.For_Four_Digit_Detail__c ='0395 - Test For Code';
    classification3.ResearcherPortalProject__c = researchRMProject.Id;

    List<Research_Project_Classification__c> classificationList = new List<Research_Project_Classification__c> {classification,classification2,classification3};
    insert classificationList;


    ProjectDetailsWrapper.FieldOfResearchForProjectsWrapper forWrapper = new ProjectDetailsWrapper.FieldOfResearchForProjectsWrapper();//Gourav

    ProjectDetailsWrapper.AllResearchProjectPublicWrapper researcherWrapper = new ProjectDetailsWrapper.AllResearchProjectPublicWrapper();//Gourav
    List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper> allResearchProjectPublicWrapperList = researcherWrapper.getAllResearcherProjectWrapperList(researcherPortalList);

	ProjectDetailsWrapper.AllResearchProjectPublicWrapper researcherWrapper2 = new ProjectDetailsWrapper.AllResearchProjectPublicWrapper(researchRMProject,1,'aerospace');//Gourav

    forWrapper.getSortedListOfFieldOfResearch(classificationList,true);//Gourav
    forWrapper.getSortedListOfFieldOfResearch(classificationList,false);//Gourav
    
    Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
    memberResearch.Contact__c = con.Id;
    memberResearch.MemberRole__c = 'CI';
    memberResearch.Researcher_Portal_Project__c = researchProject.Id;
    insert memberResearch;
    system.assert(memberResearch!=null);
    
    Researcher_Member_Sharing__c rmMemberResearch = new Researcher_Member_Sharing__c();
    rmMemberResearch.Contact__c = con.Id;
    rmMemberResearch.MemberRole__c = 'CI';
    rmMemberResearch.Researcher_Portal_Project__c = researchRMProject.Id;
    insert rmMemberResearch;
    system.assert(rmMemberResearch!=null);
    
    Ethics__c ethicObj = new Ethics__c();
    ethicObj.Ethics_Id__c='TestEthics';
    ethicObj.Approved_Date__c = date.newInstance(2018, 09, 28);
    ethicObj.Expiry_Date__c = date.newInstance(2018, 10, 15);
    insert ethicObj;
    system.assert(ethicObj!=null);
    
    List<id> ethicId = new List<id>();
    ethicId.add(ethicObj.Id); 
    system.assert(ethicId!=null);
    
    List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
    
    ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'testschoolname');
    ProjectDetailsWrapper.ContactDetail conExtendDetail = new ProjectDetailsWrapper.ContactDetail(con.id);
    ProjectDetailsWrapper.ContactDetail conExtend= new ProjectDetailsWrapper.ContactDetail(memberResearch);
    conDetailList.add(conDetail);
    conDetailList.add(conExtendDetail);
    conDetailList.add(conExtend);
    system.assert(conDetailList!=null);
    
    ProjectDetailsWrapper.ResearchProjectListView researchView = new ProjectDetailsWrapper.ResearchProjectListView(researchProject);
    system.assert(researchView !=null);
    
    ProjectDetailsWrapper.ResearchProjectDetailData wrapperListView1 = new ProjectDetailsWrapper.ResearchProjectDetailData();
    system.assert(wrapperListView1 !=null);
    
    ProjectDetailsWrapper.ResearchProjectDetailData wrapperListView2 = new ProjectDetailsWrapper.ResearchProjectDetailData(researchProject);
    system.assert(wrapperListView2 !=null);
    
    ProjectDetailsWrapper.ResearchProjectPublicProjectListView researchPublicPrjView = new ProjectDetailsWrapper.ResearchProjectPublicProjectListView();
    
    list<Researcher_Member_Sharing__c> rsmRecs = new list<Researcher_Member_Sharing__c>();
    rsmRecs.add(memberResearch);
    ProjectDetailsWrapper.ResearchProjectPublicProjectListView researchProjectPublicProjectList 
        = new ProjectDetailsWrapper.ResearchProjectPublicProjectListView(researcherWrapper2
                                                                         ,rsmRecs
                                                                         ,new List<ProjectDetailsWrapper.FieldOfResearchForProjectsWrapper>{forWrapper});
    
    }
}