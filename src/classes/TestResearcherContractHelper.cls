/*******************************************
Purpose: Test class ResearcherContractHelper
History:
Created by Subhajit on 12/10/2018
*******************************************/
@isTest
public class TestResearcherContractHelper {
   
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Subhajit
// Created Date :  12/10/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',2);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',2);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers'); 
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
            
            // Custom setting data preparation//
            CustomPermissionToProfile__c cPTP = new CustomPermissionToProfile__c(
            isAccessible__c=true,
            SetupOwnerId= [SELECT id FROM Profile WHERE NAME ='RMIT Researcher Portal Plus Community User'].Id    
            );
            insert cPTP;
            System.assert(cPTP!= null,'Custom setting records created'); 
        }
    }    
    
    
    /************************************************************************************
// Purpose      :  Test functionality of positive for getResearcherContractRecords
// Developer    :  Subhajit
// Created Date :  12/10/2018                 
//***********************************************************************************/        
    @isTest
    public static void testGetResearcherContractRecordsPositiveMethod(){
        User userRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        User shareUserRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser2@test.com.testDev' LIMIT 1]; 
        
        System.assert(userRec!=null);

        System.runAs(userRec) {
            system.debug('####TEST!@####'+userRec);
            
            ResearchProjectContract__c newContractRec = new ResearchProjectContract__c();
            newContractRec.Ecode__c ='0000123456';
            newContractRec.ContractTitle__c='test Contract';
            newContractRec.Status__c='Active';
            newContractRec.Contract_Logged_Date__c = date.today().addDays(-2);
            newContractRec.Current_Active_Action__c='With Researcher (12/05/15)';
            newContractRec.Fully_Executed_Date__c = date.today().addDays(+1);
            newContractRec.Contact_Preferred_Full_Name__c ='Dr test researcher';
            newContractRec.Activity_Type__c='Contract Research';
            newContractRec.Contract_Type__c ='RA-Variation';
            insert newContractRec;
            System.debug('@@@@@newContractRec==>'+JSON.serializePretty(newContractRec));
            System.assert(newContractRec!=null);
            
            ResearcherPortalProject__c portalProject = new ResearcherPortalProject__c();
            portalProject.Status__c='Idea';
            portalProject.Access__c='Members';
            portalProject.Approach_to_impact__c='test1';
            portalProject.Discipline_area__c='';
            portalProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            portalProject.End_Date__c=date.newInstance(2018, 12, 14);
            portalProject.Impact_Category__c='Social';
            portalProject.Title__c='projectTest1';
            portalProject.RecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
            insert portalProject; 
            system.assert(portalProject!=null);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics';
            ethicTest1.Application_Title__c ='Test Ethics';
            ethicTest1.Approved_Date__c = system.today();
            ethicTest1.Expiry_Date__c=system.today() +2;  
            insert ethicTest1;     
            system.assert(ethicTest1!=null);
            
            ProjectContract__c contract = new ProjectContract__c();
            contract.Research_Project_Contract__c = newContractRec.Id;
            contract.Researcher_Portal_Project__c = portalProject.Id;
            insert contract;
                        
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c contractMemberSharing = new Researcher_Member_Sharing__c();
            contractMemberSharing.ResearchProjectContract__c = newContractRec.Id;
            //contractMemberSharing.User__c = userRec.id;
            contractMemberSharing.Contact__c =ShareUserRec.ContactId;
            //contractMemberSharing.Researcher_Portal_Project__c =portalProject.Id;
            //contractMemberSharing.Ethics__c =ethicTest1.id;
            contractMemberSharing.Currently_Linked_Flag__c =true;
            contractMemberSharing.Position__c ='Chief Investigator';
            //contractMemberSharing.AccessLevel__c ='Read';
            //contractMemberSharing.MemberRole__c ='CI';
            contractMemberSharing.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project Contract').getRecordTypeId();
            memberResearchList.add(contractMemberSharing);
            System.debug('@@userRec.ContactId==>'+json.serializePretty(userRec.ContactId));

            insert memberResearchList;
            Researcher_Member_Sharing__c rms =[SELECT Id, User__c FROM Researcher_Member_Sharing__c WHERE Id =: memberResearchList[0].id];
            System.debug('@@User__c==>'+json.serializePretty(rms));

            System.assert(memberResearchList!=null);
            System.debug('@@memberResearchList==>'+memberResearchList);
                                    
            List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
            SignificantEvent__c milestoneProject = new SignificantEvent__c();
            milestoneProject.DueDate__c = date.today().addDays(45);
            milestoneProject.IsAction__c = true;
            milestoneProject.ResearchProjectContract__c = newContractRec.id;
            milestoneProject.EventDescription__c = 'Test';
            milestoneProject.ActualCompletionDate__c  = date.today().addDays(10);
            milestoneProject.RecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getRecordTypeId();
            milestonesList.add(milestoneProject);                                   
            
            insert milestonesList;           
            System.Assert(milestonesList!=null);
            
            List<ResearcherContractWrapper> contractWrapperList= ResearcherContractHelper.getProjectContractsWrapper(portalProject.Id);
			System.assert(contractWrapperList!=null);
            //List<ResearcherContractWrapper> contractRecsWrappers= ResearcherContractHelper.getResearcherContractRecords();
            //System.assert(contractRecsWrappers!=null);
            List<ResearcherContractWrapper> contractWrapperList1 = ResearcherContractHelper.getContractsHelper(newContractRec.Id);
            System.assert(contractWrapperList1!=null);
            
            List<MilestonesDetailsWrapper.milestonesWrapper> milestonesWrapperList = ResearcherContractHelper.getContractMilestonesList(newContractRec.Id);
			System.assert(milestonesWrapperList!=null);
        } 
        
        System.runAs(ShareUserRec) {
            List<ResearcherContractWrapper> contractRecsWrappers= ResearcherContractHelper.getResearcherContractRecords();
            System.assert(contractRecsWrappers!=null);            
        }
        
    }
    /************************************************************************************
// Purpose      :  Test functionality of negative for getResearcherContractRecords
// Developer    :  Subhajit
// Created Date :  12/10/2018                 
//***********************************************************************************/    
    
    @isTest
    public static void testGetResearcherContractRecordsNegativeMethod(){
        User userRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(userRec!=null);
        System.runAs(userRec) {
            
            ResearchProjectContract__c newContractRec = new ResearchProjectContract__c();
            newContractRec.Ecode__c ='0000123456';
            newContractRec.ContractTitle__c='test Contract';
            newContractRec.Status__c='Completed';
            newContractRec.Contract_Logged_Date__c = date.today().addDays(-2);
            newContractRec.Current_Active_Action__c='With Researcher (12/05/15)';
            newContractRec.Fully_Executed_Date__c = date.today().addDays(+1);
            newContractRec.Contact_Preferred_Full_Name__c ='Dr test researcher';
            newContractRec.Activity_Type__c='Contract Research';
            newContractRec.Contract_Type__c ='RA-Variation';
            newContractRec.PreExecutionManager__c='preTest';
            newContractRec.RIReviewer__c='RRI tetsing';
            newContractRec.StorageReferenceNumber__c='Refrence number';
            newContractRec.LInkedPrimaryOrganisationName__c='primary name';
            insert newContractRec;
            System.debug('@@@@@newContractRec==>'+JSON.serializePretty(newContractRec));
            System.assert(newContractRec!=null);
            List<ResearcherContractWrapper> contractRecsWrappers= ResearcherContractHelper.getResearcherContractRecords();
            System.assert(contractRecsWrappers!=null);
            //ResearcherContractHelper.getProjectContractDetailHelper(newContractRec.Id);
              

        }  
    }
}