/*******************************************
Purpose: Test class ResearcherContractWrapper
History:
Created by Subhajit on 12/10/2018
*******************************************/
@isTest
public class TestResearcherContractWrapper {

    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Subhajit
    // Created Date :  12/10/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
              
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
               
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
       
    }    
    /************************************************************************************
    // Purpose      :  Test functionality of positive for ResearcherContractWrapper
    // Developer    :  Subhajit
    // Created Date :  12/10/2018                 
    //***********************************************************************************/    
    @isTest
    public static void testGResearcherContractWrapperPositiveMethod(){
        User userRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(userRec!=null);
        
        
        System.runAs(userRec) {
            ResearchProjectContract__c newContractRec = new ResearchProjectContract__c(
                Ecode__c ='0000123456',
                ContractTitle__c='test Contract',
                Status__c='Completed',
                Contract_Logged_Date__c = system.today()-2,
                Current_Active_Action__c='With Researcher (12/05/15)',
                Fully_Executed_Date__c = system.today()+1,
                Contact_Preferred_Full_Name__c ='Dr test researcher',
                Activity_Type__c='Contract Research',
                Contract_Type__c ='RA-Variation'
            );
            insert newContractRec;
            System.assert(newContractRec!=null);
            
            Researcher_Member_Sharing__c memberSharing = new Researcher_Member_Sharing__c();
            memberSharing.Position__c = 'Student';
            memberSharing.ResearchProjectContract__c = newContractRec.Id; 
            memberSharing.User__c=userRec.Id;
            memberSharing.MemberRole__c='CI';
            memberSharing.Contact__c=userRec.ContactId;
            //memberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project Contract').getRecordTypeId(); 
            insert memberSharing;   
            System.assert(memberSharing!=null);
            
            
            
            ResearcherContractWrapper contractRecsWrapper= new ResearcherContractWrapper();
            System.assert(contractRecsWrapper!=null);
            
            contractRecsWrapper= new ResearcherContractWrapper(newContractRec);
            System.assert(contractRecsWrapper!=null);
            
            ResearcherContractWrapper.conDetailsWrapper conDetailContarctWrapper = new ResearcherContractWrapper.conDetailsWrapper();
            System.assert(conDetailContarctWrapper!=null);
            
            
        }  
    }
    
    /************************************************************************************
    // Purpose      :  Test functionality of negative for ResearcherContractWrapper
    // Developer    :  Subhajit
    // Created Date :  12/10/2018                 
    //***********************************************************************************/        
    @isTest
    public static void testResearcherContractWrapperNegativeMethod(){
        User userRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.assert(userRec!=null);
        
        ResearchProjectContract__c newContractRec = new ResearchProjectContract__c(
            Ecode__c ='0000123456',
            ContractTitle__c='test Contract',
            Status__c='Completed',
            Contract_Logged_Date__c = system.today()-2,
            Current_Active_Action__c='With Researcher (12/05/15)',
            Fully_Executed_Date__c = system.today()+1,
            Contact_Preferred_Full_Name__c ='Dr test researcher',
            Activity_Type__c='Contract Research',
            Contract_Type__c ='RA-Variation'
        );
        insert newContractRec;
        System.assert(newContractRec!=null);  
        
        System.runAs(userRec) {
            
            ResearcherContractWrapper contractRecsWrapper= new ResearcherContractWrapper();
            System.assert(contractRecsWrapper!=null);
            
            contractRecsWrapper= new ResearcherContractWrapper(newContractRec);
            System.assert(contractRecsWrapper!=null);
        }  
    }


}