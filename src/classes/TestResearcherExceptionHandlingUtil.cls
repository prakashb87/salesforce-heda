/*******************************************
Purpose: Test class ResearcherExceptionHandlingUtil
History:
Created by Md Ali on 12/14/2018
*******************************************/
@isTest
public class TestResearcherExceptionHandlingUtil {
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Md Ali
// Created Date :  12/14/2018                 
//***********************************************************************************/
  @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
    }    
/************************************************************************************
// Purpose      :  Positive test functionality for ResearcherExceptionHandlingUtil
// Developer    :  Md Ali
// Created Date :  12/14/2018                 
//***********************************************************************************/   
    @isTest
    public static void testPositiveUseCaseForResearcherExceptionHandlingUtilMethods(){
        User userObject =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(userObject!=null);
        
        System.runAs(userObject) 
        {
            //for integrationErr Error
            Map<String,String> parameterList = new Map<String,String>();
            parameterlist.put('isIntegation','Yes');
            parameterlist.put('businessFunctionName','checkingErroLog');
            
            Contact con = [SELECT id,firstname FROM contact WHERE lastname='Contact1'];
            con.LastName='';
            con.FirstName='Md';
            try{
                update con;    
                System.assert(con!=null);
            }
            catch(Exception ex){
                ResearcherExceptionHandlingUtil.addExceptionLog(ex,parameterList); 
                //ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(true);
				ResearcherExceptionHandlingUtil.getParamMap();
                
            }
            
        }    
    }  
/************************************************************************************
// Purpose      :  Positive test functionality for ResearcherExceptionHandlingUtil
// Developer    :  Md Ali
// Created Date :  12/14/2018                 
//***********************************************************************************/   
@isTest
    public static void testNegativeUseCaseForResearcherExceptionHandlingUtilMethods(){
        User userObject =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(userObject!=null);
        
        System.runAs(userObject) 
        {
            //for portalUser Error
            Map<String,String> parameterList = new Map<String,String>();
            parameterlist.put('isIntegation','No');
            parameterlist.put('businessFunctionName','checkingErroLog');
            
            Contact con = [SELECT id,firstname FROM contact WHERE lastname='Contact1'];
            con.LastName='';
            con.FirstName='ali';
            try{
                update con;    
                System.assert(con!=null);
            }
            catch(Exception ex){
                ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); 
            }
            
        }    
    }  
}