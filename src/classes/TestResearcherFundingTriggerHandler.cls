/*******************************************
Purpose: Test class for ResearcherProjectFundingTriggerHandler
History:
Created by Ankit Bhagat on 15/04/2019
*******************************************/
@isTest
public class TestResearcherFundingTriggerHandler {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
        
    /************************************************************************************
    // Purpose      :  Test functionality of positive for Trigger of ResearcherPortalProjectTriggerHandler
    // Developer   ResearcherPortalProjectTriggerHandler :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/        
    @isTest
    public static void reasearchProjectTriggerHandlerPositiveMethod(){

        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];           
        System.runAs(usr)
        {
            
            String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
            String projectRecordTypeId1=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId();
            String access = 'Private';
            String access1 ='Members';
            String status = 'Pending';
          
            Map<String,String> keyFieldApiToValueMap = new Map<String,String>();
            keyFieldApiToValueMap.put('Access__c',access);
            keyFieldApiToValueMap.put('Status__c',status);
            
            Map<String,String> keyFieldApiToValueMap1 = new Map<String,String>();
            keyFieldApiToValueMap.put('Access__c',access1);
            keyFieldApiToValueMap.put('Status__c',status);
            
            List<ResearcherPortalProject__c> newresearchProjectList = RSTP_TestDataFactoryUtils.createResearchProjNew(50, projectRecordTypeId1,keyFieldApiToValueMap);
            system.assert(newresearchProjectList.size() > 0);
    
            List<ResearcherPortalProject__c> oldresearchProjectList = RSTP_TestDataFactoryUtils.createResearchProjNew(100, projectRecordTypeId, keyFieldApiToValueMap1);
            system.assert(oldresearchProjectList.size() > 0);
       
            List<Research_Project_Funding__c> projectFundingList = RSTP_TestDataFactoryUtils.createProjectFunding(5);
            system.assert(projectFundingList.size() > 0);
            
            List<Research_Project_Funding__c> projectFundingNewList = RSTP_TestDataFactoryUtils.createProjectFunding(5);
            system.assert(projectFundingNewList.size() > 0); 
            
            Test.StartTest();
            ResearcherPortalProjectTriggerHandler.changeStatusForRMProj(newresearchProjectList );
            ResearcherPortalProjectTriggerHandler.assignProjectAccessProject(newresearchProjectList ,oldresearchProjectList );
            
            Set<Id> projectIdSet =  new Set<Id>();
            for(ResearcherPortalProject__c nSearchProjList : newresearchProjectList)
            {
                projectIdSet.add(nSearchProjList.Id);
            }
            Map<id,ResearcherPortalProject__c> oldRecMap = new Map<id,ResearcherPortalProject__c>();
            List<ResearcherPortalProject__c> oldRecs = [SELECT Status__c,Portal_Project_Status__c,Access__c,Group_Status__c,ls_Closed_Off__c
                                                        FROM ResearcherPortalProject__c 
                                                        WHERE Id IN: projectIdSet];
            for(ResearcherPortalProject__c oldrec : oldRecs)
            {
                oldRecMap.put(oldrec.Id,oldrec);
            }
            for(ResearcherPortalProject__c rec:newresearchProjectList)
            {
                rec.Status__c = 'Pending Agreement Negotiation';
            }
            update newresearchProjectList;
             
            for(ResearcherPortalProject__c rec:oldresearchProjectList)
            {
                rec.Group_Status__c = 'Active';
                rec.ls_Funded__c = true;
                rec.ls_Confidential__c = false;
                rec.Project_Type__c = 'Competitive Research';               
                    
            }
            update oldresearchProjectList;
                
            for(Research_Project_Funding__c eachFunding:projectFundingList){
              eachFunding.ResearcherPortalProject__c = oldresearchProjectList[0].id;
            }   
            insert projectFundingList;
            
            System.debug('@@@projectFundingNewList1'+projectFundingNewList);   
            for(Research_Project_Funding__c eachFunding: projectFundingNewList){
              eachFunding.ResearcherPortalProject__c = oldresearchProjectList[0].id;
              eachFunding.Organisation_Ecode__c ='ABC';

            } 
            insert projectFundingNewList;
            
            for(Research_Project_Funding__c eachFunding: projectFundingNewList){
              eachFunding.Organisation_Ecode__c ='abd';

            } 
            update projectFundingNewList;   
                    
            Map<id,Research_Project_Funding__c> oldRecFundingMap = new Map<id,Research_Project_Funding__c>();                                           
            for(Research_Project_Funding__c projectFunding : projectFundingNewList)
            {
                oldRecFundingMap.put(projectFunding.Id,projectFunding);
            }
              //ResearcherProjectFundingTriggerHandler.getFundingDataforOrganisationECodeForUpsert(projectFundingList,oldRecFundingMap);

            Test.StopTest();
       }
    }
     /************************************************************************************
    // Purpose      :  Test functionality of Negative for Trigger of ResearcherPortalProjectTriggerHandler
    // Developer   ResearcherPortalProjectTriggerHandler :  Ankit
    // Created Date :  12/05/2018                 
    //***********************************************************************************/        
    @isTest
    public static void reasearchProjectTriggerHandlerNegativeMethod(){

        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];           
        System.runAs(usr)
        {
        Boolean result = false;
        Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
        con.hed__UniversityEmail__c='raj@rmit.edu.au';
        
        List<Research_Project_Funding__c> projectFundingList = RSTP_TestDataFactoryUtils.createProjectFunding(5);
        system.assert(projectFundingList.size() > 0);

        con.lastname='';
        //update con; 
         try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
            String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
            String access = 'Private';
            List<ResearcherPortalProject__c> newresearchProjectList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
            system.assert(newresearchProjectList .size() > 0);
            List<ResearcherPortalProject__c> oldresearchProjectList = RSTP_TestDataFactoryUtils.createResearchProj(2, projectRecordTypeId, access);
            system.assert(oldresearchProjectList.size() > 0);
       
       
           List<Research_Project_Funding__c> projectFundingNewList = RSTP_TestDataFactoryUtils.createProjectFunding(5);
            system.assert(projectFundingNewList.size() > 0);
 
            for(Research_Project_Funding__c eachFunding:projectFundingNewList){
              eachFunding.ResearcherPortalProject__c = oldresearchProjectList[0].id;
              eachFunding.Organisation_Ecode__c ='ABC';

            } 
            
            insert projectFundingNewList;
            
            delete projectFundingNewList;
       
       }
    }
}