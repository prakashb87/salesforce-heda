/*******************************************
Purpose: Test class ResearcherLeftNavWrapper
History:
Created by Ankit Bhagat on 11/10/2018
*******************************************/
@isTest
public class TestResearcherLeftNavHelper {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
    
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',2);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',2);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
    // Purpose      :  Test functionality for View MyEthicsList Controller
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void researcherLeftNavWrapperMethod(){

       User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        User shareUserRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser2@test.com.testDev' LIMIT 1]; 
        
        
        System.runAs(shareUserRec) {            
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            System.Assert(ethicTest1.Application_Title__c == 'Test');
            
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            Publication__c publication = new Publication__c();
            publication.Publication_Title__c = 'Test';
            publication.Publication_Id__c = 123;
            insert publication;
            System.Assert(publication.Publication_Title__c == 'Test');   
            
            ResearchProjectContract__c contract = new ResearchProjectContract__c();
            contract.ContractTitle__c='test1';
            contract.StartDate__c=date.today();
            contract.EndDate__c=date.today().addDays(10);
            insert contract;
            System.Assert(contract.ContractTitle__c== 'test1');   
 
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c ethicsMemberSharing = new Researcher_Member_Sharing__c();
            ethicsMemberSharing.Ethics__c = ethicTest1.id;
            ethicsMemberSharing.Contact__c = ShareUserRec.ContactId;
            ethicsMemberSharing.User__c   = ShareUserRec.id;
            ethicsMemberSharing.Currently_Linked_Flag__c=true;
            ethicsMemberSharing.Position__c ='Chief Investigator';
            //ethicsMemberSharing.AccessLevel__c='RMIT Public';
            ethicsMemberSharing.RecordTypeId= Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId();
            memberResearchList.add(ethicsMemberSharing);
            
            Researcher_Member_Sharing__c projectMemberSharing = new Researcher_Member_Sharing__c();
            projectMemberSharing.Researcher_Portal_Project__c = researchProject.id;
            projectMemberSharing.Contact__c =ShareUserRec.ContactId;
            projectMemberSharing.User__c   = ShareUserRec.id;
            projectMemberSharing.Currently_Linked_Flag__c=true;
            projectMemberSharing.Position__c ='Chief Investigator';
            //projectMemberSharing.AccessLevel__c='RMIT Public';
            projectMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project').getRecordTypeId();
            memberResearchList.add(projectMemberSharing);
            
            Researcher_Member_Sharing__c publicationMemberSharing = new Researcher_Member_Sharing__c();
            publicationMemberSharing.Publication__c = publication.id;
            publicationMemberSharing.Contact__c =ShareUserRec.ContactId;
            publicationMemberSharing.User__c   = ShareUserRec.id;
            publicationMemberSharing.Currently_Linked_Flag__c=true;
            publicationMemberSharing.Position__c ='Student';
            //publicationMemberSharing.AccessLevel__c='RMIT Public';
            publicationMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Publications').getRecordTypeId();
            memberResearchList.add(publicationMemberSharing);
            
            Researcher_Member_Sharing__c contractMemberSharing = new Researcher_Member_Sharing__c();
            contractMemberSharing.ResearchProjectContract__c=contract.Id;
            contractMemberSharing.Contact__c =ShareUserRec.ContactId;
            contractMemberSharing.User__c= ShareUserRec.Id;
            //contractMemberSharing.AccessLevel__c='RMIT Public';
            contractMemberSharing.Currently_Linked_Flag__c=true;
            contractMemberSharing.Position__c ='Chief Investigator';
            
            contractMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project Contract').getRecordTypeId();
            memberResearchList.add(contractMemberSharing);           
            insert memberResearchList;
            
            Researcher_Member_Sharing__c rms =[SELECT Id,Contact__c,RecordTypeId, User__c,AccessLevel__c ,Currently_Linked_Flag__c FROM Researcher_Member_Sharing__c WHERE Id =: memberResearchList[0].id];
            System.debug('@@User__c==>'+json.serializePretty(rms));
            
            Id projectRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get('Research Project').getRecordTypeId();
            Id ethicsRecordTypeId  = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId();
            Id contractRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get('Contracts').getRecordTypeId();
            Id publicationRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get('Publications').getRecordTypeId();
            
            List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
            SignificantEvent__c milestoneEthics = new SignificantEvent__c();
            milestoneEthics.DueDate__c = date.today().addDays(15);
            milestoneEthics.IsAction__c = true;
            milestoneEthics.Ethics__c = ethicTest1.id;
            milestoneEthics.EventDescription__c = 'Test';
            milestoneEthics.RecordTypeId = ethicsRecordTypeId;
            milestonesList.add(milestoneEthics);  
            
            SignificantEvent__c milestoneProject = new SignificantEvent__c();
            milestoneProject.DueDate__c = date.today().addDays(45);
            milestoneProject.IsAction__c = true;
            milestoneProject.ResearchProject__c = researchProject.id;
            milestoneProject.EventDescription__c = 'Test';
            milestoneProject.ActualCompletionDate__c  = date.today().addDays(10);
            milestoneProject.RecordTypeId = projectRecordTypeId;
            milestonesList.add(milestoneProject);  
            
            SignificantEvent__c milestonePublication = new SignificantEvent__c();
            milestonePublication.DueDate__c = date.today().addDays(45);
            milestonePublication.IsAction__c = true;
            milestonePublication.Publication__c = publication.id;
            milestonePublication.EventDescription__c = 'Test';
            milestonePublication.ActualCompletionDate__c  = date.today().addDays(10);
            milestonePublication.RecordTypeId = publicationRecordTypeId ;
            milestonesList.add(milestonePublication);          
            
            insert milestonesList;           
            System.Assert(milestonesList.size()== 3);
            
            
            ResearcherLeftNavWrapper.leftNavWrapper  wrapper = new ResearcherLeftNavWrapper.leftNavWrapper();
            wrapper.milestoneCount  = 3;                                
            System.runAs(ShareUserRec) {
            ResearcherLeftNavHelper.getLeftNavWrapperDetails();
            }
        
        }
    }
         
    /************************************************************************************
    // Purpose      :  Test functionality for View MyEthicsList Controller
    // Developer    :  Ankit
    // Created Date :  12/05/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void researcherLeftNavWrapperNegativeMethod(){

        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {            
            
         
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c = 'Test1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            
        
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicTest1.id;
            memberResearch.User__c   = u.id;
            memberResearch.Currently_Linked_Flag__c=true;
            memberResearch.AccessLevel__c='RMIT Public';
            memberResearch.MemberRole__c='CI';
            memberResearchList.add(memberResearch);
            
          
            insert memberResearchList;
               
            
            List<SignificantEvent__c> milestonesList = new List<SignificantEvent__c>();
           
                SignificantEvent__c milestoneEthics = new SignificantEvent__c();
                milestoneEthics.DueDate__c = date.today().addDays(-15);
                milestoneEthics.IsAction__c = true;
                milestoneEthics.Ethics__c = ethicTest1.id;
                milestoneEthics.EventDescription__c = 'Test';
                milestonesList.add(milestoneEthics);  
            
            insert milestonesList;
            System.Assert(milestonesList.size()== 1);
            
            
            ResearcherLeftNavWrapper.leftNavWrapper  wrapper = new ResearcherLeftNavWrapper.leftNavWrapper();
            wrapper.milestoneCount  = 1;
            
            ResearcherLeftNavHelper.getLeftNavWrapperDetails();
            
        
        }
    }
    
 

}