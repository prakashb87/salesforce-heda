/*******************************************
Name: TestScheduleNewUserUpdateRMSBatch  

Description : Test class for the scheduler class to update the new users on the RMS records.
              
Created by : Prakash B for RPORW-897

History:
*******************************************/

@istest(seeAllData = false)
public class TestScheduleNewUserUpdateRMSBatch  {

    @Testsetup static void setupTestData(){
      
      ResearcherPortalProject__c rpp = new ResearcherPortalProject__c();
      rpp.Title__c = 'Test Batch';
      rpp.Access__c = 'Members';
      rpp.Project_Ecode__c = '1112222Test333444';
      Insert rpp;
      
      List<Contact> contactList = new List<Contact>();
      
      Contact testContact = new Contact();
      testContact.FirstName = 'Test Batch';
      testContact.LastName = 'Schedule Class';
      contactList.add(testContact);
      
      Contact testContact1 = new Contact();
      testContact1.FirstName = 'Test Batch2';
      testContact1.LastName = 'Schedule Class';
      testContact1.Enumber__c = 'e1243456';
      contactList.add(testContact1);
      
      Contact testContact2 = new Contact();
      testContact2.FirstName = 'Test Batch3';
      testContact2.LastName = 'Schedule Class';
      testContact2.Student_ID__c= '987654';
      contactList.add(testContact2);
      
      Insert contactList;
      
      List<Researcher_Member_Sharing__c> rmsList = new List<Researcher_Member_Sharing__c>();
      
      
      Researcher_Member_Sharing__c rms = new Researcher_Member_Sharing__c();
      rms.Researcher_Portal_Project__c = rpp.Id;
      rms.Currently_Linked_Flag__c = true;
      rms.Contact__c = testContact.Id;
      rms.Position__c = 'Chief Investigator';
      rmsList.add(rms);
      
      Researcher_Member_Sharing__c rms1 = new Researcher_Member_Sharing__c();
      rms1.Researcher_Portal_Project__c = rpp.Id;
      rms1.Currently_Linked_Flag__c = true;
      rms1.Person_eCode__c = '1243456';
      rms1.Position__c = 'Chief Investigator';
      rmsList.add(rms1);
      
      Researcher_Member_Sharing__c rms2 = new Researcher_Member_Sharing__c();
      rms2.Researcher_Portal_Project__c = rpp.Id;
      rms2.Currently_Linked_Flag__c = true;
      rms2.Person_eCode__c = '987654';
      rms2.Position__c = 'Chief Investigator';
      rmsList.add(rms2);
      
      Insert rmsList;
    
    }

   static testmethod void testNewUserRMSUpdate() {
   
       Test.startTest();
       
          Integer currentYear = System.today().year();
          Integer currentMonth = System.today().month();
          Integer nextDay = System.today().day() + 1;
          
          String scheduleDate = '0 0 0 15 5 ? 2030';// + nextDay + ' ' + currentMonth + ' ? ' + currentYear + '\'';
          System.debug('scheduleDate' + scheduleDate);
          
          String jobId = System.schedule('SchedulerResearcherNewUserUpdateRMSBatch', scheduleDate , new SchedulerResearcherNewUserUpdateRMSBatch());
    
          CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
    
          System.assertEquals(0, ct.TimesTriggered);
    
         
       Test.stopTest();
   
   }
}