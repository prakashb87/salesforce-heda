/*******************************************
Purpose: Test class SobjectUtility
History:
Created by Ankit Bhagat on 21/09/2018
*******************************************/
@isTest
public class TestSobjectUtils {

     /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
    // Purpose      :  Test functionality 
    // Created Date :  11/13/2018                 
    //***********************************************************************************/
    @isTest
    public static  void testGetselectOptions(){
        
        ResearcherPortalProject__c researchPortal = new ResearcherPortalProject__c();
        researchPortal.Title__c = 'researchtest';
        insert researchPortal;
        system.assert(researchPortal.Title__c == 'researchtest');
        
        SobjectUtils.getselectOptions(researchPortal,'Access__c');
        
    }
    /************************************************************************************
    // Purpose      :  Test functionality 
    // Created Date :  11/13/2018                 
    //***********************************************************************************/
    @isTest
    public static  void testFetchUser(){
        
      User usr = SobjectUtils.fetchUser();
      System.assert(usr!= null,'User record is fetched');  
    }
    
    /************************************************************************************
    // Purpose      :  Test functionality
    // Created Date :  11/13/2018                 
    //***********************************************************************************/
    @isTest
    public static  void testGetProfileAccessCheckInfo(){
        
        // Custom setting data preparation//
        CustomPermissionToProfile__c cPTP = new CustomPermissionToProfile__c(
        isAccessible__c=true,
        SetupOwnerId= [SELECT id FROM Profile WHERE NAME ='RMIT Researcher Portal Plus Community User'].Id    
        );
        insert cPTP;
        System.assert(cPTP!= null,'Custom setting records created');
        
        Boolean accessFlag = SobjectUtils.getProfileAccessCheckInfo(userInfo.getProfileId());
        System.assert(accessFlag== false,'System Adminstrator has false Access');
        
    }
    
    /************************************************************************************
    // Purpose      :  Test functionality
    // Created Date :  11/13/2018                 
    //***********************************************************************************/
    @isTest
    public static  void testCheckOrgType(){
        
        Boolean isSandbox = SobjectUtils.checkOrgType();
        if(isSandbox)
        {
            System.assert(isSandbox== true,'This is a Sandbox');  
        }
        else
        {
            System.assert(isSandbox== false,'This is a Production org');  
        }
           
    }
    
    /************************************************************************************
    // Purpose      :  Test functionality
    // Created Date :  11/13/2018                 
    //***********************************************************************************/
    @isTest
    public static  void testNegativeGetProfileAccessCheckInfo(){
        
        // Custom setting data preparation//
        CustomPermissionToProfile__c cPTP = new CustomPermissionToProfile__c(); 
        insert cPTP;
        System.assert(cPTP!= null,'Custom setting records created');
        
        Boolean accessFlag = SobjectUtils.getProfileAccessCheckInfo(userInfo.getProfileId());
        System.assert(accessFlag== false,'System Adminstrator has false Access');
        
    }
}