/*******************************************
Purpose: Test class ViewMyEthicsListController
History:
Created by Ankit Bhagat on 24/09/2018
*******************************************/
@isTest
public class TestViewMyEthicsListController {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    
    /************************************************************************************
    // Purpose      :  Test functionality for View MyEthicsList Controller
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void viewMyEthicsListUtilMethod(){

        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {
            
            List<Ethics__c> ethicsList = new List<Ethics__c>();
            for(integer i = 0; i < 5; i++){
                
                Ethics__c ethicTest = new Ethics__c();
                ethicTest .Ethics_Id__c='TestEthics'+i;
                ethicTest.Application_Title__c = 'Test';
                ethicTest.Application_Type__c = 'HUMAN = Human Ethics';
                ethicTest.Status__c = 'Amended';
                ethicTest.Approved_Date__c = date.today();
                ethicTest.Expiry_Date__c   = date.today().addDays(10);
                ethicsList.add(ethicTest);  
            }
            insert ethicsList;
            System.Assert(ethicsList.size()== 5);
            
            /*Ethics__c ethicObj = new Ethics__c();
            ethicObj.Ethics_Id__c='TestEthics';
            insert ethicObj ;
            List<id> ethicsId = new List<id>();
            ethicsId.add(ethicObj.id);
            system.assert(ethicsId!=null);*/
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='EthicsTest1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
             
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicsList[0].id;
            memberResearch.User__c   = u.id;            
            memberResearchList.add(memberResearch);
            
            Researcher_Member_Sharing__c memberResearch1 = new Researcher_Member_Sharing__c();
            memberResearch1.Ethics__c = ethicTest1.id;
            memberResearch1.User__c   = u.id;
            memberResearchList.add(memberResearch1);
                        
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            List<Ethics_Researcher_Portal_Project__c>  ethicResearchList = new List<Ethics_Researcher_Portal_Project__c>();
            for(integer i = 0; i < 5; i++){
                
                Ethics_Researcher_Portal_Project__c ethicResearch = new Ethics_Researcher_Portal_Project__c();
                ethicResearch.Name='Test';
                ethicResearch.Ethics__c = ethicsList[0].id; 
                ethicResearch.Researcher_Portal_Project__c = researchProject.Id;
                ethicResearchList.add(ethicResearch);
            }        
            
            insert ethicResearchList;
            System.Assert(ethicResearchList.size() == 5);
            
            Ethics_Researcher_Portal_Project__c ethicResearch1 = new Ethics_Researcher_Portal_Project__c();
            ethicResearch1.Name='Test';
            ethicResearch1.Ethics__c = ethicTest1.id; 
            ethicResearch1.Researcher_Portal_Project__c = researchProject.Id;
            ethicResearchList.add(ethicResearch1);
            
            
            EthicsDetailWrapper.ethicsWrapper  wrapper= new EthicsDetailWrapper.ethicsWrapper ();
            wrapper.ethicsTitle  ='Test';
            wrapper.type         ='abc';
            wrapper.status       ='Amended';  
            wrapper.projectTitle ='abc';
            wrapper.projectId    = researchProject.id;
            wrapper.approvedDate =  date.newInstance(2018, 10, 15);
            wrapper.expiryDate   =  date.newInstance(2018, 10, 28);
            wrapper.ethicsId = ethicsList[0].id;
            
            EthicsDetailWrapper.ethicsWrapper  wrapper3 = new EthicsDetailWrapper.ethicsWrapper ();
            EthicsDetailWrapper.ethicsWrapper  wrapper1 = new EthicsDetailWrapper.ethicsWrapper (ethicsList[0]);
            EthicsDetailWrapper.ethicsWrapper wrapper4 = new EthicsDetailWrapper.ethicsWrapper (ethicResearch1);
            //EthicsDetailWrapper.ethicsWrapper  wrapper2 = new EthicsDetailWrapper.ethicsWrapper (Ethic_Researchlist[0]);
            
            ViewMyEthicsListController.getEthicsList();
            ViewMyEthicsListController.searchtEthicsList('Test1');
            
            ViewMyEthicsHelper.getProjectEthicsWrapperHelper(researchProject.id);
            ViewMyEthicsHelper.deleteEthicsProjectHelper(ethicTest1.id,researchProject.Id);
            ViewMyEthicsHelper.getEthicsSearchWrapperList('testethics');
            ViewMyEthicsHelper.searchEthicswithTitleHelper('Test',true);
            ViewMyEthicsHelper.searchEthicswithTitleHelper('Test',false);
           
           
            List<Id> ethicsIdList= new List<Id>() ;             
            for(Ethics__c li: [SELECT Id FROM Ethics__c WHERE Ethics_Id__c LIKE 'TestEthics%'])
                {                                               
                ethicsIdList.add(li.id);
                }           
            ViewMyEthicsHelper.addEthicsProjectRecord(ethicsIdList,researchProject.Id);                      
        }
    }
    
   
    @isTest
    public static void viewMyEthicsListUtilMethod1(){
     
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {
            
       List<Ethics__c> ethicsList = new List<Ethics__c>();
            for(integer i = 0; i < 5; i++){
                
                Ethics__c ethicTest = new Ethics__c();
                ethicTest.Ethics_Id__c='TestEthics'+i;
                ethicTest.Application_Title__c = 'Test';
                ethicTest.Application_Type__c = 'HUMAN = Human Ethics';
                ethicTest.Status__c = 'Amended';
                ethicTest.Approved_Date__c = date.today();
                ethicTest.Expiry_Date__c   = date.today().addDays(10);
                ethicsList.add(ethicTest);  
            }
            insert ethicsList;
            System.Assert(ethicsList.size()== 5);
            
           /* Ethics__c ethicObj = new Ethics__c();
            ethicObj.Ethics_Id__c='TestEthics';
            insert ethicObj ;
            List<id> ethicsId = new List<id>();
            ethicsId.add(ethicObj.id);
            system.assert(ethicsId!=null);*/
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='EthicsTest2';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            system.assert(ethicTest1!=null);
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicsList[0].id;
            memberResearch.User__c   = u.id;
            memberResearch.Position__c='Chief Investigator';
            memberResearchList.add(memberResearch);
            
            Researcher_Member_Sharing__c memberResearch1 = new Researcher_Member_Sharing__c();
            memberResearch1.Ethics__c = ethicTest1.id;
            memberResearch1.User__c   = u.id;
            memberResearch1.Position__c='Chief Investigator and Contract Manager';
            memberResearchList.add(memberResearch1);
            insert memberResearchList;
            system.assert(memberResearchList!=null);
            
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            List<Ethics_Researcher_Portal_Project__c>  ethicResearchList = new List<Ethics_Researcher_Portal_Project__c>();
            for(integer i = 0; i < 5; i++){
                
                Ethics_Researcher_Portal_Project__c ethicResearch = new Ethics_Researcher_Portal_Project__c();
                ethicResearch.Name='Test';
                ethicResearch.Ethics__c = ethicsList[0].id; 
                ethicResearch.Researcher_Portal_Project__c = researchProject.Id;
                ethicResearchList.add(ethicResearch);
            }        
            
            insert ethicResearchList;
            System.Assert(ethicResearchList.size() == 5);
            
            Ethics_Researcher_Portal_Project__c ethicResearch1 = new Ethics_Researcher_Portal_Project__c();
            ethicResearch1.Name='Test';
            ethicResearch1.Ethics__c = ethicTest1.id; 
            ethicResearch1.Researcher_Portal_Project__c = researchProject.Id;
            ethicResearchList.add(ethicResearch1);
            
            
            EthicsDetailWrapper.ethicsWrapper  wrapper= new EthicsDetailWrapper.ethicsWrapper ();
            wrapper.ethicsTitle  ='Test';
            wrapper.type         ='abc';
            wrapper.status       ='Amended';  
            wrapper.projectTitle ='abc';
            wrapper.projectId    = researchProject.id;
            wrapper.approvedDate = date.newInstance(2018, 10, 15);
            wrapper.expiryDate   = date.newInstance(2018, 10, 28);
            wrapper.ethicsId = ethicsList[0].id;
            
            EthicsDetailWrapper.ethicsWrapper  wrapper3 = new EthicsDetailWrapper.ethicsWrapper ();
            EthicsDetailWrapper.ethicsWrapper  wrapper1 = new EthicsDetailWrapper.ethicsWrapper (ethicsList[0]);
            
            ViewMyEthicsListController.getEthicsList();
            ViewMyEthicsListController.searchtEthicsList('test');
            
            
        }
    }
    
     /************************************************************************************
    // Purpose      : Negative Test functionality for View MyEthicsList Controller
    // Developer    :  Shalu
    // Created Date :  11/23/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void viewMyEthicsListUtilMethodNegative(){

        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {
            Boolean result = false;
  
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10); 
            try
            {  
                insert ethicTest1;
            }
            catch(DmlException ex)
            {
                result =true;                   
            }
            System.Assert(result);
                        
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
          
            ViewMyEthicsListController.getEthicsList();
            ViewMyEthicsListController.searchtEthicsList('/test');     
          
      
        }
    }
    @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(u!=null);
        
        System.runAs(u) {     
            
            ViewMyEthicsListController.isForceExceptionRequired=true;
            ViewMyEthicsListController.getEthicsList();
            ViewMyEthicsListController.searchtEthicsList('/test');  
            
        }
    }
    
}