@isTest
private class TestbatchRMProjectsAccessUpdate {

static testmethod void test() {

// Create test Projects to be updated
// by the batch job.
    
        String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
        String access = 'RMIT Public';
        List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(50, projectRecordTypeId, access);
        system.assert(projectObjList.size() > 0);
         


        Test.startTest();
        batchRMProjectsAccessUpdate c = new batchRMProjectsAccessUpdate();
        Database.executeBatch(c);
        Test.stopTest();
        
}
}