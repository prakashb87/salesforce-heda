/********************************************************************************************************/
/**Author : Capgemini                                  Date : 14/02/2019     ****************************/               
/* TThe dispatcher is responsible for making sure all of the applicable methods on your trigger handler
are called, depending on the current trigger context.                    ****************************/ 

public without sharing class TriggerDispatcher {	
    
    public static void runTriggerLogic(TriggerHandler handler){
        // Detect the current trigger context and fire the relevant methods on the trigger handler:
        if(trigger.isBefore ){
            beforeTiggerlogic(handler);
        }
        if(trigger.isAfter ){
            afterTiggerlogic(handler);
        }  
    }
    public static void beforeTiggerlogic(TriggerHandler handler){
        if (trigger.isInsert){
            handler.beforeInsert(trigger.new);
        }else if (trigger.isUpdate){
            handler.beforeUpdate(trigger.newMap, trigger.oldMap);
        }else if (trigger.isDelete){
            handler.beforeDelete(trigger.oldMap);
        }
    }  
    
    public static void afterTiggerlogic(TriggerHandler handler){
        if (trigger.isInsert){
            handler.afterInsert(Trigger.newMap);
        }else if (trigger.isUpdate){
            handler.afterUpdate(trigger.newMap, trigger.oldMap);
        }else if (trigger.isDelete){
            handler.afterDelete(trigger.oldMap);
        }else if (trigger.isUndelete){
            handler.afterUndelete(trigger.oldMap);
        }
    }
}