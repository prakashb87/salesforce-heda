/*********************************************************************************
 *** @TestClass  		: UpdateContactandPEValuesOnContact_Test
 *** @Author		    : Shubham Singh
 *** @Requirement     	: To Test UpdateContactBatch and PEValuesOnContactBatch
 *** @Created date    	: 17/11/2017
 **********************************************************************************/
/*****************************************************************************************
 *** @About Class
 *** This class is a Test class for Batches UpdateContactBatch and PEValuesOnContactBatch.
 *****************************************************************************************/
@isTest
public class UpdateContactandPEValuesOnContact_Test {
    //Testing the value of NumberOfActiveProgramEnrollment on Contact  
    static testMethod void testMethod1() {
        List < hed__Program_Enrollment__c > lstPE = new List < hed__Program_Enrollment__c > ();
        List < Contact > lstCon = new List < Contact > ();
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        for (Integer i = 0; i < 10; i++) {
            Contact con = new Contact();
            con.LastName = 'TestContact';
            lstCon.add(con);
        }
        insert lstCon;
        for (Integer i = 0; i < 10; i++) {
            hed__Program_Enrollment__c pe = new hed__Program_Enrollment__c();
            pe.hed__Account__c = acc.id;
            pe.hed__Contact__c = lstCon[i].id;
            pe.Action_Date__c = Date.newInstance(2015, 10, 8);
            pe.hed__Graduation_Year__c = '201' + i;
            pe.Program_Action__c = 'ACTV';
            lstPE.add(pe);
        }

        insert lstPE;
        Test.startTest();
        UpdateContactBatch upCon = new UpdateContactBatch();
        DataBase.executeBatch(upCon);
        Test.stopTest();
        System.assertNotEquals(lstCon[0].Number_of_active_program_enrollments__c, -1);
        System.assertNotEquals(lstCon[0].Number_of_active_program_enrollments__c, 0);
    }
    //Testing Values of ProgramAction,ActionDate,GraduationYear,Program values on Contact
    static testMethod void testMethod2() {
        List < hed__Program_Enrollment__c > lstPE = new List < hed__Program_Enrollment__c > ();
        List < Contact > lstCon = new List < Contact > ();
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        for (Integer i = 0; i < 10; i++) {
            Contact con = new Contact();
            con.LastName = 'TestContact';
            lstCon.add(con);
        }
        insert lstCon;
        for (Integer i = 0; i < 10; i++) {
            hed__Program_Enrollment__c pe = new hed__Program_Enrollment__c();
            pe.hed__Account__c = acc.id;
            pe.hed__Contact__c = lstCon[i].id;
            pe.Action_Date__c = Date.newInstance(2015, 10, 8);
            pe.hed__Graduation_Year__c = '201' + i;
            lstPE.add(pe);
        }

        insert lstPE;

        test.startTest();
        PEValuesOnContactBatch peCon = new PEValuesOnContactBatch();
        DataBase.executeBatch(peCon);
        test.stopTest();
        System.assertNotEquals(lstCon[0].Program__c, 'TestAccount1');
        System.assertNotEquals(lstCon[0].Action_Date__c, Date.newInstance(2015, 11, 8));
        System.assertNotEquals(lstCon[0].Class_Year__c, '2011');
        System.assertNotEquals(lstCon[0].Program_Action__c, 'DISC');
    }
}