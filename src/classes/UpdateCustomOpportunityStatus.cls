/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 27/05/2018     ****************************/               
/***   This class is using to change the status of custom opportunity to 'Closed Won'    ****************/ 
/********************************************************************************************************/

public without sharing class UpdateCustomOpportunityStatus
{
	public static void changeCustomopportunityStatus( id basketId )
    {
        cscfga__Product_Basket__c productBasketObj = new cscfga__Product_Basket__c();
        productBasketObj = [SELECT id,cscfga__Opportunity__c,cscfga__Opportunity__r.Name,cscfga__Opportunity__r.OwnerId from cscfga__Product_Basket__c where id = :basketId LIMIT 1];
        
        List <Custom_Opportunity__c> customOppList = new List<Custom_Opportunity__c>();
        customOppList = [SELECT id,Stage__c from Custom_Opportunity__c where Opportunity__c =: productBasketObj.cscfga__Opportunity__c];
        if( customOppList.isEmpty())
        {
            createCustomopportunity( productBasketObj );
        }
        else
        {
            updateCustomOpportunity( customOppList );
        }                 
    }
    public static void createCustomopportunity( cscfga__Product_Basket__c productBasketObj )
    {   
        system.debug('RRRR: Inside createCustomopportunity()');
        Custom_Opportunity__c customOppObj = new Custom_Opportunity__c(); 
        customOppObj.Opportunity__c = productBasketObj.cscfga__Opportunity__c;
        customOppObj.Name = 'Custom_' + productBasketObj.cscfga__Opportunity__r.Name;
        //customOppObj.Stage__c = 'Closed Won';
        customOppObj.OwnerId = productBasketObj.cscfga__Opportunity__r.OwnerId;
        insert customOppObj;
    }
    public static void updateCustomOpportunity( List<Custom_Opportunity__c> customOppList)
    {
        system.debug('RRRR: Inside updateCustomOpportunity()');
        List <Custom_Opportunity__c> custoppListToUpdate = new List<Custom_Opportunity__c>();
        for( Custom_Opportunity__c custOppObj : customOppList )
        {
            custOppObj.Stage__c = 'Closed Won';
            custoppListToUpdate.add( custOppObj );
        }
        if( !custoppListToUpdate.isEmpty())
        {
            update custoppListToUpdate;
        }
    }
}