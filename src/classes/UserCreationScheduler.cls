/*****************************************************************************************************
 *** @Class             : UserCreationScheduler
 *** @Author            : Rishabh Anand
 *** @Requirement       : scheduler to schedule User Creation
 *** @Created date      : 31/05/2018
 *** @JIRA ID           : ECB-3249
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used as a scheduler to schedule User Creation
 *****************************************************************************************************/ 
 
public without sharing class  UserCreationScheduler implements Schedulable{
    
    public void execute(SchedulableContext SC) { 
        
       		        	
			
        	BatchJobUserCreation userCreation = new BatchJobUserCreation();
        	//ECB - 4087 :UAT- Community user creation batch job is not converting contacts to community users
        	Database.executebatch(userCreation,50);

       
    }

}