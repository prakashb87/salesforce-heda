/***************************************************************************************************************
 * ECB-4584 Maria Luisa Esmero 26/10/2018
 * Description: Inserts a User Registration record of the Contact, and sends an activation email to the Contact.
 * Parameter  : Id of a Contact
 * Sample script to run using Developer Console: 
 *    UserRegistrationEmail userReg = new UserRegistrationEmail(enter your contactId e.g. '003p000000OX3YnAAL');
 *    userReg.userRegistrationEmail();
 * PMD Checks: Removed object permissions for guest user
 **************************************************************************************************************/
public without sharing class UserRegistrationEmail {
    private id contactId;
    private datetime registerDtTime = datetime.now();
    private string registrationId;
    private id userRegId;
    
    //  Constructor    
    public UserRegistrationEmail(id usercontactId) 
    {
        contactId = usercontactId;
        if (contactId == null){throw new ArgumentException('Contact Id is null');}
    }
    
    //  Main
    public boolean userRegistrationEmail() 
    {           
        registrationId = generateHmacSHA256Signature();
        system.debug('Start processing account activation email of Contact Id ' +contactId +' with registration Id ' +registrationId);  
        boolean successfulemail = false;
        userRegId = insertRegistration();        
        If (userRegId != null) 
        {
           system.debug('Successful creation of User Registration record with Id ::: '+userRegId +'. Sending email');
           successfulemail = sendActivationEmail();        
           if (successfulemail == true) 
           {
              system.debug('Successful email send for Contact Id '+contactid +' on ' +registerDtTime);
           }
           else 
           {
              system.debug('Unable to send email. Deleting User Registration record with Id ::: '+userRegId);
           }        
         }
         system.debug('Successful Email result ::: '+successfulemail);
         return successfulemail;
    } 
    
    //  Generate the Registration Id by hashing the Contact Id + Datetime stamp
    private string generateHmacSHA256Signature()
    {
        Blob hmacData=Crypto.generateMac('HmacSHA256', Blob.valueOf(contactId), Blob.valueOf(registerDtTime.format()));
        return EncodingUtil.convertToHex(hmacData);
    }
    
    // Insert the User Registration record
    private id insertRegistration() 
    {
           User_Registration__c ureg = new User_Registration__c (
               Contact__c = contactid,
               Source__c = (ConfigDataMapController.getCustomSettingValue('MarketPlaceSource')),
               Registered_on__c = registerDtTime,
               Expiry__c = registerDtTime.addhours(Integer.valueOf(ConfigDataMapController.getCustomSettingValue('Hours'))),
               Registration_Link__c = (ConfigDataMapController.getCustomSettingValue('RMITUserRegistrationLink')),
               Registration_Id__c = registrationId,
               Activated__c = false,
               Activated_on__c = null);
          Database.insert(ureg);
          if( ureg !=null)
          {
              userRegId = ureg.id;
              system.debug('Successful insert of User Registration record with Id ::: '+userRegId);
          }
          else
             {
              system.debug('Unable to insert User Registration record with Id ::: '+ureg.id);
             }                 
          return userRegId;
    }
    
    // Send the activation email to the user
    private boolean sendActivationEmail() 
    {  
        boolean success = false;
        string emailException;
        string emailTemplate = ConfigDataMapController.getCustomSettingValue('UserRegistrationTemplate');
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(emailTemplate);
        system.debug('Email TemplateId ::: ' +emailTemplate);
        email.setTargetObjectId(contactId);
        system.debug('Email TargetObjectId ::: ' +contactId);
        email.setWhatId(userRegId);
        system.debug('Email WhatId ::: ' +userRegId);
        email.setReplyTo(ConfigDataMapController.getCustomSettingValue('RMITNoReplyEmail'));     
        email.setSenderDisplayName(ConfigDataMapController.getCustomSettingValue('RMITNoReplySender')); 
        email.setSaveAsActivity(true);      
        try
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            system.debug('Successful send of email to Target Object Id ::: '+email.getTargetObjectId());
            success = true;
        } 
        catch(exception ex) 
        {
            success = false;
            emailException = ex.getMessage();
            system.debug('Unable to send email. Exception message :::'+ex.getMessage()); 
            deleteRegistration();
        }
        If (success == false){throw new ArgumentException('Unable to send email. ' +emailException);}
        return success; 
    }

    // Delete the User Registration record if the send email was unsuccessful
    private void deleteRegistration() 
    {
        User_Registration__c failRegEmail;
        if(Schema.sObjectType.User_Registration__c.isAccessible()){
            failRegEmail = [SELECT Id FROM User_Registration__c WHERE id=:userRegId LIMIT 1];
        }
        system.debug('Deleting User Registration record :::'+userRegId); 
        if( failRegEmail != null )
        {
           Database.delete(failRegEmail);
        }
    }
}