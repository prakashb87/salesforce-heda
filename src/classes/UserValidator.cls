/*
 * @description This interface is used to define the methods that are used to validate user details.
 */
public interface UserValidator {
    
    /**
     * @description Returns true if email is valid
     */
    boolean isEmailValid(String email);
    
    
    /**
     * @description Returns true if first name is valid
     */
    boolean isFirstNameValid(String firstName);
    
    
    /**
     * @description Returns true if first name is valid
     */
    boolean isLastNameValid(String lastName);
    
    
    /**
     * @description Returns true if phone number is valid
     */
    boolean isPhoneNumberValid(String phoneNumber);
    
}