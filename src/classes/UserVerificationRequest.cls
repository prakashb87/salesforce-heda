/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 29/10/2018     ****************************/
/***   @JIRA ID: ECB-4585                       *********************************************************/
/***   This is request class for PassportIdpUserVerificationReq              ****************************/
/********************************************************************************************************/
public with sharing class UserVerificationRequest 
{
	public String email;
	public List<Isverified> isverified;

	public class Isverified 
	{
		public String add;
	}	
	public static UserVerificationRequest parse(String json) 
	{
		return (UserVerificationRequest) System.JSON.deserialize(json, UserVerificationRequest.class);
	}
}