/*******************************************
Purpose: To create the class for Milestones Details
History:
Created by Ankit Bhagat on 30/10/2018
*******************************************/

public without sharing class ViewMyMilestonesController {

        
    /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
    
    /************************************************************************************
    // JIRA No      :  RPORW-138          
    // SPRINT       :  SPRINT-5
    // Purpose      :  getting Milestones Wrapper
    // Parameters   :  Void         
    // Developer    :  Ankit
    // Created Date :  31/10/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static String getMyMilestonesListWrapper(){ 
        
        MilestonesDetailsWrapper.milestonesListWithNotificationWrapper milestonewrapper = new MilestonesDetailsWrapper.milestonesListWithNotificationWrapper();
      
       try{
           milestonewrapper = ViewMyMilestonesHelper.getMilestonesWrapperList();
           
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
        
        catch(Exception ex){
            
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage()+ex.getLineNumber());
        }
       System.debug('@@@milestonewrapper'+JSON.serialize(milestonewrapper));
       return JSON.serialize(milestonewrapper);

      }
    
       /************************************************************************************
    // Purpose      : To check current login user is community user or salesforce user
                   
    //***********************************************************************************/ 
      
   @AuraEnabled     
    public static Boolean checkInternalDualAccessUser(){
            Boolean isInternalDualAccessUser = ApexWithoutSharingUtils.checkInternalDualAccessUser();
            return isInternalDualAccessUser;
    } 
    
    
    /************************************************************************************
    // JIRA No      :  RPORW-313          
    // SPRINT       :  SPRINT-10
    // Purpose      :  getting Milestones Record Types 
    // Parameters   :  Void         
    // Developer    :  Ankit
    // Created Date :  16/01/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static List<String> getMyMilestonesRecordTypeList(){ 
        
       List<String> recordTypeList = new List<String>();    
     
       try{
           recordTypeList = ViewMyMilestonesHelper.getRecordTypeNames();
          
        }
        catch(Exception ex){
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage()+ex.getLineNumber());
        }
       System.debug('@@@recordTypeList'+recordTypeList);
       return recordTypeList;

      }      
}