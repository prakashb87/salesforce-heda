/*******************************************
Purpose: To create Helper class for Milestones Details
History:
Created by Ankit Bhagat on 30/10/2018
added  new method for 1592
********************************************************/

public without Sharing class ViewMyMilestonesHelper {
    
    /*****************************************************************************************
	// Purpose      :  Viewing Milestones
	// Developer    :  Ankit Bhagat	
	// Created Date :  30/10/2018                 
	//************************************************************************************/ 
	
    public static final string NO_ACCESS_ROLE = label.RSTP_Role_No_Access;

	public static MilestonesDetailsWrapper.milestonesListWithNotificationWrapper getMilestonesWrapperList(){ 

		
		MilestonesDetailsWrapper.milestonesListWithNotificationWrapper milestonewrapper;
	
 		List<Researcher_Member_Sharing__c> reseacherMemberSharingList = getReseacherMemberSharingList();
		
		List<SignificantEvent__c> milestonesList=getMilestonesList(reseacherMemberSharingList);

		List<MilestonesDetailsWrapper.milestonesWrapper> milestonesWrapperList = new List<MilestonesDetailsWrapper.milestonesWrapper>();
		integer countMilestones = getMilestonesCount(milestonesList, milestonesWrapperList);

        milestonewrapper = new MilestonesDetailsWrapper.milestonesListWithNotificationWrapper();
        milestonewrapper.totalMilestones = countMilestones;
        milestonewrapper.milestoneWrapperList = milestonesWrapperList;		
        			 
        System.debug('@@@milestonewrapperdetails'+milestonewrapper);

        return milestonewrapper;
    }   
    
    // Get the milestone wrapper list for the project detail pages- 1592 story
    public static List<MilestonesDetailsWrapper.milestonesWrapper>  getMilestonesWrapperListByProjectIdSet(set<Id> projectIdSet){
    	
    	Id projectRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
       
    	List<MilestonesDetailsWrapper.milestonesWrapper> milestonesWrapperList = new List<MilestonesDetailsWrapper.milestonesWrapper>();
    	
         if((Schema.sObjectType.SignificantEvent__c.isAccessible())&&(Schema.sObjectType.SignificantEvent__c.fields.ActualCompletionDate__c.isAccessible())){//PMD fix by Ali
    	List<SignificantEvent__c> milestonesList = [Select id, ActualCompletionDate__c, DueDate__c, Ethics__c, Ethics__r.Application_Title__c,  EventDescription__c,IsAction__c,
                                              IsActionDue__c, Publication__c, Publication__r.Publication_Title__c, ResearchProject__c, ResearchProject__r.Title__c,
                                              ResearchProjectContract__c, ResearchProjectContract__r.ContractTitle__c,
                                              SignificantEventType__c, Status__c , RecordTypeId
                                              From SignificantEvent__c 
                                              where ResearchProject__c IN:projectIdSet                                              
                                              ORDER BY Status__c ASC, DueDate__c ASC NULLS LAST];
        
        getMilestonesCount(milestonesList, milestonesWrapperList); //Same method used to get the milestone wrapper
        }
    	return milestonesWrapperList;
    }
    
    
    public static Map<Id, String> getRecordTypeNameFromRecordTypeId(){
        
        Id projectRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        Id ethicsRecordTypeId  = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
		Id contractRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getRecordTypeId();
        Id publicationRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
                
        String projectRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getName();
        String ethicsRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getName();
        String contractRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getName();
        String publicationRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getName();
 
        Map<Id, String> recordTypeIdVsName = new Map<Id, String> { projectRecordTypeId => projectRecordTypeName , ethicsRecordTypeId => ethicsRecordTypeName ,
                                                                   contractRecordTypeId => contractRecordTypeName , publicationRecordTypeId => publicationRecordTypeName };        
        System.debug('@@@recordTypeIdVsName'+recordTypeIdVsName);
        return recordTypeIdVsName;
                                                                       
     }
    
    // Return List of RecordTypes,313, Ankit
     public static List<String> getRecordTypeNames(){
                        
        String projectRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getName();
        String ethicsRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getName();
        String contractRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getName();
        String publicationRecordTypeName = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getName();
 
        List<String> recordTypeIdVsNameList = new List<String> {  projectRecordTypeName , ethicsRecordTypeName ,
                                                                  contractRecordTypeName ,publicationRecordTypeName };        
        System.debug('@@@recordTypeIdVsNameList'+recordTypeIdVsNameList);
        return recordTypeIdVsNameList;
                                                                       
     }

	public static List<Researcher_Member_Sharing__c> getReseacherMemberSharingList(){
		List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
		Id projectRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        Id ethicsRecordTypeId  = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
		Id contractRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ContractRecordtype).getRecordTypeId();
        Id publicationRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
        
		Set<Id> recordTypeIdSet = new Set<Id>{projectRecordTypeId, ethicsRecordTypeId, contractRecordTypeId, publicationRecordTypeId};
	    
	    reseacherMemberSharingList = ResearcherMemberSharingUtils.geResearcherMemberSharingList(recordTypeIdSet, userinfo.getUserId());
		return reseacherMemberSharingList;
	}
    public static List<SignificantEvent__c> getMilestonesList(List<Researcher_Member_Sharing__c> reseacherMemberSharingList){
        Set<Id> ethicsIdSet      = new Set<Id>();
        Set<Id> projectIdSet     = new Set<Id>();
        Set<Id> publicationIdSet = new Set<Id>();
        Set<Id> contractIdSet    = new Set<Id>();

        System.debug('@@@reseacherMemberSharingList'+reseacherMemberSharingList);
		for(Researcher_Member_Sharing__c eachmemberSharing :reseacherMemberSharingList){
            if(isEligibleToShareEthics(eachmemberSharing)){
	            ethicsIdSet.add(eachmemberSharing.Ethics__c);
            } 
            if(!String.isBlank(eachmemberSharing.Researcher_Portal_Project__c))
            {
                projectIdSet.add(eachmemberSharing.Researcher_Portal_Project__c);
            }  
            
            if(!String.isBlank(eachmemberSharing.Publication__c)) {
                publicationIdSet.add(eachmemberSharing.Publication__c);
            }
            if(!String.isBlank(eachmemberSharing.ResearchProjectContract__c)) {
                contractIdSet.add(eachmemberSharing.ResearchProjectContract__c);
            }  

		} 
        
       checkAccessToSignificantEvent();
       List<SignificantEvent__c> milestonesList = [Select id, ActualCompletionDate__c, DueDate__c, Ethics__c, Ethics__r.Application_Title__c,  EventDescription__c,IsAction__c,
		                                      IsActionDue__c, Publication__c, Publication__r.Publication_Title__c, ResearchProject__c, ResearchProject__r.Title__c,
                                              ResearchProjectContract__c, ResearchProjectContract__r.ContractTitle__c,
											  SignificantEventType__c, Status__c , RecordTypeId
                                              From SignificantEvent__c 
                                              where Ethics__c IN:ethicsIdSet
											  OR ResearchProject__c IN:projectIdSet
											  OR Publication__c IN:publicationIdSet
											  OR ResearchProjectContract__c IN:contractIdSet
											  ORDER BY Status__c ASC, DueDate__c ASC NULLS LAST]; // Sorting on Status Added, 533, Ankit
         
        System.debug('@@@milestonesList==>'+milestonesList);
		return milestonesList;
    }

	public static void checkAccessToSignificantEvent(){
		if(!Schema.sObjectType.SignificantEvent__c.isAccessible()){   
			System.debug('Check PMD');
			throw new DMLException();
		}
	}
	
	public static Boolean isEligibleToShareEthics(Researcher_Member_Sharing__c eachmemberSharing) {
		return (!String.isBlank(eachmemberSharing.Ethics__c) && eachmemberSharing.RM_Member__c == true); 
	}

	public static Integer getMilestonesCount(List<SignificantEvent__c> milestonesList, List<MilestonesDetailsWrapper.milestonesWrapper> milestonesWrapperList){

		integer countMilestones = 0;
        Map<Id, String> milestonesRecordTypeIdVsName = getRecordTypeNameFromRecordTypeId();        
        
		if(milestonesList!= null && milestonesList.size()>0)
		{
	     MilestonesDetailsWrapper.milestonesListWithNotificationWrapper milestonewrapper = new MilestonesDetailsWrapper.milestonesListWithNotificationWrapper();
	     for(SignificantEvent__c eachMilestone : milestonesList){
			
			MilestonesDetailsWrapper.milestonesWrapper wrapper = new MilestonesDetailsWrapper.milestonesWrapper(eachMilestone);
            wrapper.type = milestonesRecordTypeIdVsName.get(eachMilestone.RecordTypeId);
			setWrapperNameAsperType(wrapper, eachMilestone);            
            
			if(isMilestoneDueWithin30Days(eachMilestone)) {
				wrapper.IsDueWithin30Days = true;
				countMilestones++ ; 
			}   
			else	
			if((eachMilestone.DueDate__c < Date.today())  && eachMilestone.IsAction__c == true && eachMilestone.ActualCompletionDate__c == null) {
				wrapper.IsOverDue = true;
                countMilestones++ ;		  		
			}
			
			milestonesWrapperList.add(wrapper);
		 }	
        }
		return countMilestones;
	}
	
	public static void setWrapperNameAsperType(MilestonesDetailsWrapper.milestonesWrapper wrapper, SignificantEvent__c eachMilestone){
            if(wrapper.type == Label.RSTP_ResearchProjectRecordtype) {
                wrapper.name = eachMilestone.ResearchProject__r.Title__c;
                wrapper.milestoneTypeRecordId = eachMilestone.ResearchProject__c;  //added by Ankit,1512
            } else 
            if(wrapper.type == Label.RSTP_EthicsRecordtype) {
                wrapper.name = eachMilestone.Ethics__r.Application_Title__c;
                wrapper.milestoneTypeRecordId = eachMilestone.Ethics__c;   //added by Ankit,1512
            } else 
            if(wrapper.type == Label.RSTP_MilestoneContractRecordtype) {
                wrapper.name = eachMilestone.ResearchProjectContract__r.ContractTitle__c;
                wrapper.milestoneTypeRecordId = eachMilestone.ResearchProjectContract__c;   //added by Ankit,1512
            } else 
            if(wrapper.type == Label.RSTP_PublicationRecordtype) {
                wrapper.name = eachMilestone.Publication__r.Publication_Title__c;
                wrapper.milestoneTypeRecordId = eachMilestone.Publication__c;   //added by Ankit,1512
            } 
	}
	
	public static Boolean isMilestoneDueWithin30Days(SignificantEvent__c eachMilestone){
		return ((eachMilestone.DueDate__c >= Date.today()  && eachMilestone.DueDate__c  <= Date.today().addDays(30))  
               		&&   eachMilestone.IsAction__c == true && eachMilestone.ActualCompletionDate__c == null);
	}
}