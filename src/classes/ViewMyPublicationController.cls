/*******************************************
Purpose: To create the class for Publication Details
History:
Created by Ankit Bhagat on 30/10/2018
*******************************************/

public class ViewMyPublicationController {

      /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
    
    /************************************************************************************
    // JIRA No      :  RPORW-247          
    // SPRINT       :  SPRINT-6
    // Purpose      :  getting Publication Wrapper
    // Parameters   :  Void         
    // Developer    :  Ankit
    // Created Date :  31/10/2018                 
    //***********************************************************************************/
    @AuraEnabled    
    public static String getPublicationListWrapper(){ 
        
       PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper publicationwrapper = new PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper();
      
       try{
           publicationwrapper = ViewMyPublicationHelper.getPublicationWrapperList();
          
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
        
        catch(Exception ex) {
            
            //Exception handling Error Log captured :: Starts
           	ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage());
            
        }
        System.debug('@@@publicationwrapper'+JSON.serialize(publicationwrapper));
        return JSON.serialize(publicationwrapper);    

      }
      
         
    /************************************************************************************
    // Purpose      : To check current login user is community user or salesforce user
                   
    //***********************************************************************************/ 
    
	@AuraEnabled     
    public static Boolean checkInternalDualAccessUser(){
            Boolean isInternalDualAccessUser = ApexWithoutSharingUtils.checkInternalDualAccessUser();
            return isInternalDualAccessUser;
    } 
 
    /*****************************************************************************************
     // JIRA No      :  RPORW-734 
	// Purpose : This method used to get the picklist value 
	// Discription:Using this method we are populating the picklist value of status from publication
    // Parameters : void
    // Developer : Md Ali
    // Created Date : 04/02/2019
  //****************************************************************************************/  


    @AuraEnabled 
    public static List<String> getPublicationStatusValue(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Publication__c.Audit_Result__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }  
        system.debug('@@pickvalue'+pickListValuesList);
        return pickListValuesList;
    }
   

/*****************************************************************************************
   	 // JIRA No      :  RPORW-734 
	// Purpose : This method used to get picklist value
    // Parameters : Void
    // // Discription:Using this method we are populating the picklist value of category from publication
    // Developer : Md Ali
    // Created Date : 04/02/2019
    //****************************************************************************************/  
    
    @AuraEnabled 
    public static List<String> getPublicationCategoryValue(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Publication__c.Publication_Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        system.debug('@@Categorypickvalue'+pickListValuesList);
        return pickListValuesList;
    }
 /*****************************************************************************************
   	 // JIRA No      :  RPORW-894 
	// Purpose : This method used to get publication detail page 
    // Parameters : Void
    //  Discription:Using this method we are showing the publication detail page
    // Developer : Md Ali
    // Created Date : 19/02/2019
    //****************************************************************************************/ 
    
    @AuraEnabled
    public static PublicationDetailsWrapper.publicationWrapper getPublicationDetails(id publicationId){
        
        PublicationDetailsWrapper.publicationWrapper publicationDetailsData = new PublicationDetailsWrapper.publicationWrapper();
        
        try{
            publicationDetailsData= ViewMyPublicationHelper.getPublicationDetailsHelper(publicationId);
        }
        catch(Exception ex) {
            
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            system.debug('Exception Occured =>'+ex.getMessage());
            
        }
        System.debug('@@@publicationDetailsData'+JSON.serialize(publicationDetailsData));
        return publicationDetailsData;
        
    }
    
}