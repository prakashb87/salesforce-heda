/**
 * This test class is for use with the OrchestraCMS API Resources classes.
 * This test class does not assert for any correctness, it only exercises the properties in the classes.
 *
 * Created by mwannamaker on 2017-09-06.
 */
@IsTest
private class ZTest_APIResources {
    // Tests APIRequest.cls
    static testMethod void testAPIRequest() {

        // Create an API request.
        APIRequest apiRequest = new APIRequest();

        // Run getters against the parameters we need, setters will be covered as they're on the same line.
        Id objectId = apiRequest.objectId;
        String transactionId = apiRequest.transactionId;

        // Check the APIRequests single method, we can reuse the objectId variable as it's the right type.
        objectId = apiRequest.getID();

        // Set the requestFlags to null to trigger the conditional check when we run the getter next.
        apiRequest.requestFlags = null;
        Map<String, Boolean> requestFlags = apiRequest.requestFlags;

        // Set the parameters to null to trigger the conditional check when we run the getter next.
        apiRequest.parameters = null;
        Map<String, String> parameters = apiRequest.parameters;

        // Set the listParameters to null to trigger the conditional check when we run the getter next.
        apiRequest.listParameters = null;
        Map<String, List<String>> listParameters = apiRequest.listParameters;

        // Set the mapParameters to null to trigger the conditional check when we run getter next.
        apiRequest.mapParameters = null;
        Map<String, Map<String, String>> mapParameters = apiRequest.mapParameters;

        System.assertNotEquals(apiRequest, null);
    }

    // Tests AttributeBundle.cls
    static testMethod void testAttributeBundle() {

        // Create a JSONMessage.APIResponse.
        AttributeBundle.ContentAttribute contentAttribute = new AttributeBundle.ContentAttribute();

        // The AttributeBundle settings and getters are on the same line.
        // This will help save writing test coverage for code coverage.

        // We need to get the AttributeBundle specific fields.
        String languageCode = contentAttribute.languageCode;
        Boolean simple = contentAttribute.simple;

        // Now let's get the AttributeBundle.ContentAttribute specific fields.
        String name = contentAttribute.name;
        String value = contentAttribute.value;
        String valueType = contentAttribute.valueType;

        // Create an AttributeBundle.ContentMetaAttribute.
        AttributeBundle.ContentMetaAttribute contentMetaAttribute = new AttributeBundle.ContentMetaAttribute();

        // Let's get the AttributeBundle.ContentMetaAttribute specific fields.
        String metaContentName = contentMetaAttribute.metaContentName;
        String metaContentTitle = contentMetaAttribute.metaContentTitle;
        String metaContentDescription = contentMetaAttribute.metaContentDescription;

        System.assertNotEquals(null, contentAttribute);
    }

    // Tests ContentFilteringAPIRequest.cls
    static testMethod void testContentFilteringAPIRequest() {

        // We only need the ContentFilteringAPIRequest constructor for complete code coverage.
        ContentFilteringAPIRequest contentFilteringAPI = new ContentFilteringAPIRequest();

        System.assertNotEquals(null, contentFilteringAPI);
    }

    // Tests ContentLayoutBundle.cls
    static testMethod void testContentLayoutBundle() {

        // Create a ContentLayoutBundle.
        ContentLayoutBundle contentLayoutBundle = new ContentLayoutBundle();

        // We need to use a getter or setter for the layoutAttributes field.
        // This will complete necessary code coverage for this class.
        Map<String, List<AttributeBundle.ContentAttribute>> layoutAttributes = contentLayoutBundle.layoutAttributes;

        System.assertNotEquals(null, contentLayoutBundle);
    }

    // Tests ContentAPIRequest.cls
    static testMethod void testContentAPIRequest() {

        // Create a ContentAPIRequest.
        ContentAPIRequest contentAPI = new ContentAPIRequest();

        // We need to use a getter or setter for the bundle field.
        // This will complete necessary code coverage for this class.
        ContentBundle bundle = contentAPI.bundle;

        System.assertNotEquals(null, contentAPI);
    }

    // Tests Filter.cls
    static testMethod void testFilter() {

        Filter f = new Filter();

        System.assertNotEquals(null, f);
    }

    // Tests FilterItem.cls
    static testMethod void testFilterItem() {

        FilterItem f = new FilterItem();

        System.assertNotEquals(null, f);
    }

    // Tests FilteringAPIRequest.cls
    static testMethod void testFilteringAPIRequest() {

        // We only need the FilteringAPIRequest constructor for complete code coverage.
        FilteringAPIRequest filteringAPI = new FilteringAPIRequest();

        System.assertNotEquals(null, filteringAPI);
    }

    // Tests FilteringBundle.cls
    static testMethod void testFilteringBundle() {

        FilteringBundle fBundle = new FilteringBundle();

        System.assertNotEquals(null, fBundle);
    }

    // Tests FolderAPIRequest.cls
    static testMethod void testFolderAPIRequest() {

        // We only need the FolderAPIRequest constructor for complete code coverage.
        FolderAPIRequest folderAPI = new FolderAPIRequest();

        System.assertNotEquals(null, folderAPI);
    }

    // Tests FolderItemBundle.cls
    static testMethod void testFolderItemBundle() {

        FolderItemBundle fBundle = new FolderItemBundle();

        System.assertNotEquals(null, fBundle);
    }

    // Tests LanguageAPIRequest.cls
    static testMethod void testLanguageAPIRequest() {

        // We only need the LanguageAPIRequest constructor for complete code coverage.
        LanguageAPIRequest languageAPI = new LanguageAPIRequest();

        System.assertNotEquals(null, languageAPI);
    }

    // Tests LanguageBundle.cls
    static testMethod void testLanguageBundle() {

        LanguageBundle lBundle = new LanguageBundle();

        System.assertNotEquals(null, lBundle);
    }

    // Tests JSONMessage.cls
    static testMethod void testJSONMessage() {

        // We only need the JSONMessage.MessageError constructor for code coverage of this subclass.
        JSONMessage.MessageError messageError = new JSONMessage.MessageError();

        // Create a JSONMessage.APIResponse.
        JSONMessage.APIResponse apiResponse = new JSONMessage.APIResponse();

        // We need to set a mock response object to utilize the getResponseAs method.
        // We'll use the UserBundle class to make a mock one for remaining code coverage due to its simplicity.
        apiResponse.responseObject = '{"userId":"'+UserInfo.getUserId()+'"}';
        Object apiResponseAs = apiResponse.getResponseAs(UserBundle.class);

        System.assertNotEquals(null, apiResponseAs);
    }

    // Tests ProfileAPIRequest.cls
    static testMethod void testProfileAPIRequest() {

        // We only need the ProfileAPIRequest constructor for complete code coverage.
        ProfileAPIRequest profileAPI = new ProfileAPIRequest();

        System.assertNotEquals(null, profileAPI);
    }

    // Tests ProfileBundle.cls
    static testMethod void testProfileBundle() {

        // Create a ProfileBundle.
        ProfileBundle profileBundle = new ProfileBundle();

        // The ProfileBundle has protected setters so we need to use gets, but they're on the same line anyway.
        // This will help save writing test coverage for code coverage.

        // We need to get the ProfileBundle fields.
        String profileId = profileBundle.profileId;
        String profileName = profileBundle.profileName;
        String licenseType = profileBundle.licenseType;

        Map<String, Boolean> permissions = profileBundle.permissions;
        Map<Id, Map<String, Boolean>> permissionsContentType = profileBundle.permissions_content_type;
        Map<Id, Map<String, Boolean>> permissionsLibrary = profileBundle.permissions_library;

        List<UserBundle> users = profileBundle.users;
        Boolean activated = profileBundle.activated;
        Boolean licenseExpired = profileBundle.licenseExpired;

        System.assertNotEquals(null, profileBundle);
    }

    // Tests RenderResultBundle.cls
    static testMethod void testRenderResultBundle() {

        // We only need to check RenderResultBundle contentRemaining for complete code coverage.
        Boolean renderResultBundle = new RenderResultBundle().contentRemaining;

        System.assertNotEquals(null, renderResultBundle);
    }

    // Tests SiteAPIRequest.cls
    static testMethod void testSiteAPIRequest() {

        // We only need the SiteAPIRequest constructor for complete code coverage.
        SiteAPIRequest siteAPI = new SiteAPIRequest();

        System.assertNotEquals(null, siteAPI);
    }

    // Tests SiteBundle.cls
    static testMethod void testSiteBundle() {

        SiteBundle sBundle = new SiteBundle();

        System.assertNotEquals(null, sBundle);
    }

    // Tests SocialAPIRequest.cls
    static testMethod void testSocialAPIRequest() {

        // We only need the SocialAPIRequest constructor for complete code coverage.
        SocialAPIRequest socialAPI = new SocialAPIRequest();

        System.assertNotEquals(null, socialAPI);
    }

    // Tests SocialActivity.cls
    static testMethod void testSocialActivity() {

        // We only need the SocialActivity constructor for complete code coverage.
        SocialActivity sActivity = new SocialActivity();

        System.assertNotEquals(null, sActivity);
    }

    // Tests SocialBundle.cls
    static testMethod void testSocialBundle() {

        SocialBundle sBundle = new SocialBundle();

        System.assertNotEquals(null, sBundle);
    }

    // Tests TargetingAPIRequest.cls
    static testMethod void testTargetingAPIRequest() {

        // We only need the TargetingAPIRequest constructor for complete code coverage.
        TargetingAPIRequest targetingAPI = new TargetingAPIRequest();

        System.assertNotEquals(null, targetingAPI);
    }

    // Tests TaxonomyAPIRequest.cls
    static testMethod void testTaxonomyAPIRequest() {

        // We only need the TaxonomyAPIRequest constructor for complete code coverage.
        TaxonomyAPIRequest taxonomyAPI = new TaxonomyAPIRequest();

        System.assertNotEquals(null, taxonomyAPI);
    }
    
    // Tests TranslationGroupBundle
    static testMethod void testTranslationGroupBundle() {

        TranslationGroupBundle translationGroup = new TranslationGroupBundle();

        System.assertNotEquals(null, translationGroup);
    }

}