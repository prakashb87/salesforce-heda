@RestResource(urlMapping='/csapi_BasketExtension/AddProduct')
global class csapi_RestBasketExtension_AddProduct {
    
    @HttpPost
    global static csapi_BasketRequestWrapper.AddProductResponse doPost(csapi_BasketRequestWrapper.AddProductRequest csapi_request) {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        Exception ee = null;
        csapi_BasketRequestWrapper.AddProductResponse csapi_response;
        try {
            csapi_response = csapi_BasketExtension.addProduct(csapi_request);
        } catch(Exception e ) {
            ee = e;
        } finally {
            if(ee != null) 
            {
                system.debug('ee >> ' + ee.getMessage());
            }
        }
                
        return csapi_response;
    }
    
}