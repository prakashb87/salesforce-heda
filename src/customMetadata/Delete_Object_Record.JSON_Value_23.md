<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>JSON Value 23</label>
    <protected>false</protected>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Academic_Career__c</value>
    </values>
    <values>
        <field>JSON_Field_Name__c</field>
        <value xsi:type="xsd:string">academicCareer</value>
    </values>
    <values>
        <field>JSON_Object_Name__c</field>
        <value xsi:type="xsd:string">TermSession</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">hed__Term__c</value>
    </values>
</CustomMetadata>
