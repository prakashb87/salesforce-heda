<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>JSON Value 52</label>
    <protected>false</protected>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Academic_Plan__c</value>
    </values>
    <values>
        <field>JSON_Field_Name__c</field>
        <value xsi:type="xsd:string">academicPlan</value>
    </values>
    <values>
        <field>JSON_Object_Name__c</field>
        <value xsi:type="xsd:string">AcademicPlan</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">Plan__c</value>
    </values>
</CustomMetadata>
