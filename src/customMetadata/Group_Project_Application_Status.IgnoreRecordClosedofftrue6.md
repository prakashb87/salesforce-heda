<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>IgnoreRecordClosedofftrue6</label>
    <protected>false</protected>
    <values>
        <field>IsCLosedOff__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Portal_Project_Status__c</field>
        <value xsi:type="xsd:string">Ignore</value>
    </values>
    <values>
        <field>backendApplicationStatus__c</field>
        <value xsi:type="xsd:string">RA License</value>
    </values>
</CustomMetadata>
