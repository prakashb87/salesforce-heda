<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Member Ethics Read</label>
    <protected>false</protected>
    <values>
        <field>Access_Level__c</field>
        <value xsi:type="xsd:string">Read</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">Ethics</value>
    </values>
    <values>
        <field>SF_Role__c</field>
        <value xsi:type="xsd:string">Member</value>
    </values>
</CustomMetadata>
