<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Finding Funding8</label>
    <protected>false</protected>
    <values>
        <field>Case_Record_Type__c</field>
        <value xsi:type="xsd:string">Researcher Portal Support Case</value>
    </values>
    <values>
        <field>EmailAddress__c</field>
        <value xsi:type="xsd:string">dsccontracts@rmit.edu.au</value>
    </values>
    <values>
        <field>QueueName__c</field>
        <value xsi:type="xsd:string">RSTP Enable Capability Platform</value>
    </values>
    <values>
        <field>Subtopic__c</field>
        <value xsi:type="xsd:string">Support services within College of Design and Social Context regarding developing contracts</value>
    </values>
    <values>
        <field>Test_Email_Address__c</field>
        <value xsi:type="xsd:string">research.support.test@rmit.edu.au</value>
    </values>
    <values>
        <field>Topic__c</field>
        <value xsi:type="xsd:string">Finding Funding</value>
    </values>
</CustomMetadata>
