<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object stores all the researcher publications</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Audit_Result__c</fullName>
        <externalId>false</externalId>
        <label>Audit Result</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Eligible - Commissioned Report</fullName>
                    <default>false</default>
                    <label>Eligible - Commissioned Report</label>
                </value>
                <value>
                    <fullName>Eligible - RMIT affiliation1</fullName>
                    <default>false</default>
                    <label>Eligible - RMIT affiliation1</label>
                </value>
                <value>
                    <fullName>Eligible - creative work approved by DHR/FoR review panel</fullName>
                    <default>false</default>
                    <label>Eligible - creative work approved by DHR/FoR review panel</label>
                </value>
                <value>
                    <fullName>Eligible - creative work recorded prior to FoR panels</fullName>
                    <default>false</default>
                    <label>Eligible - creative work recorded prior to FoR panels</label>
                </value>
                <value>
                    <fullName>Eligible - future journal</fullName>
                    <default>false</default>
                    <label>Eligible - future journal</label>
                </value>
                <value>
                    <fullName>Eligible - in press</fullName>
                    <default>false</default>
                    <label>Eligible - in press</label>
                </value>
                <value>
                    <fullName>Eligible - meets HERDC guidelines (but missed HERDC reporting period)</fullName>
                    <default>false</default>
                    <label>Eligible - meets HERDC guidelines (but missed HERDC reporting period)</label>
                </value>
                <value>
                    <fullName>Eligible - meets HERDC guidelines year changed</fullName>
                    <default>false</default>
                    <label>Eligible - meets HERDC guidelines year changed</label>
                </value>
                <value>
                    <fullName>Eligible - no RMIT affiliation</fullName>
                    <default>false</default>
                    <label>Eligible - no RMIT affiliation</label>
                </value>
                <value>
                    <fullName>In progress - College/ADR Review</fullName>
                    <default>false</default>
                    <label>In progress - College/ADR Review</label>
                </value>
                <value>
                    <fullName>In progress - School Review</fullName>
                    <default>false</default>
                    <label>In progress - School Review</label>
                </value>
                <value>
                    <fullName>In progress - awaiting information requested from author</fullName>
                    <default>false</default>
                    <label>In progress - awaiting information requested from author</label>
                </value>
                <value>
                    <fullName>In progress - awaiting library hold request</fullName>
                    <default>false</default>
                    <label>In progress - awaiting library hold request</label>
                </value>
                <value>
                    <fullName>In progress - data entry stage 1 completed</fullName>
                    <default>false</default>
                    <label>In progress - data entry stage 1 completed</label>
                </value>
                <value>
                    <fullName>In progress - in press (awaiting publication)</fullName>
                    <default>false</default>
                    <label>In progress - in press (awaiting publication)</label>
                </value>
                <value>
                    <fullName>In progress - library request pending</fullName>
                    <default>false</default>
                    <label>In progress - library request pending</label>
                </value>
                <value>
                    <fullName>In progress - publication to be purchased by RMIT library</fullName>
                    <default>false</default>
                    <label>In progress - publication to be purchased by RMIT library</label>
                </value>
                <value>
                    <fullName>Not eligible - book review</fullName>
                    <default>false</default>
                    <label>Not eligible - book review</label>
                </value>
                <value>
                    <fullName>Not eligible - brief communication, commentary</fullName>
                    <default>false</default>
                    <label>Not eligible - brief communication, commentary</label>
                </value>
                <value>
                    <fullName>Not eligible - output ruled ineligible by School/College</fullName>
                    <default>false</default>
                    <label>Not eligible - output ruled ineligible by School/College</label>
                </value>
                <value>
                    <fullName>Publication submitted</fullName>
                    <default>false</default>
                    <label>Publication submitted</label>
                </value>
                <value>
                    <fullName>See audit result for Parent Portfolio record</fullName>
                    <default>false</default>
                    <label>See audit result for Parent Portfolio record</label>
                </value>
                <value>
                    <fullName>Eligible - RMIT affiliation</fullName>
                    <default>false</default>
                    <label>Eligible - RMIT affiliation</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Conference_Location__c</fullName>
        <externalId>false</externalId>
        <label>Conference Location</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Conference_Name__c</fullName>
        <externalId>false</externalId>
        <label>Conference Name</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Copyright__c</fullName>
        <description>Story the copy right information from RRI. Created part of 983</description>
        <externalId>false</externalId>
        <label>Copyright</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Creative_Work_Extent__c</fullName>
        <externalId>false</externalId>
        <label>Creative Work Extent</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Creative_Work_Type__c</fullName>
        <externalId>false</externalId>
        <label>Creative Work Type</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Publication Abstract</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>131070</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Digital_Object_Identifier__c</fullName>
        <externalId>false</externalId>
        <label>Digital Object Identifier</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Edition__c</fullName>
        <description>Publication Edition</description>
        <externalId>false</externalId>
        <label>Edition</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Editors__c</fullName>
        <externalId>false</externalId>
        <label>Editors</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>End_Page__c</fullName>
        <externalId>false</externalId>
        <label>End Page</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Event_Date__c</fullName>
        <externalId>false</externalId>
        <label>Event Date</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>ISSN__c</fullName>
        <description>ISSN (identification number for a journal)</description>
        <externalId>false</externalId>
        <label>ISSN</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Portfolio_Component__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Portfolio Component</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Portfolio__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is_Portfolio</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Issue_Number__c</fullName>
        <externalId>false</externalId>
        <label>Issue Number</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Keywords__c</fullName>
        <externalId>false</externalId>
        <label>Keywords</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Medium__c</fullName>
        <externalId>false</externalId>
        <label>Medium</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Outlet__c</fullName>
        <externalId>false</externalId>
        <label>Outlet</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Page_Total__c</fullName>
        <description>Total Number of Pages</description>
        <externalId>false</externalId>
        <label>Page Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parent_Portfolio__c</fullName>
        <externalId>false</externalId>
        <label>Parent Portfolio</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Place_Published__c</fullName>
        <description>Place of Publication</description>
        <externalId>false</externalId>
        <label>Place Published</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publication_Category__c</fullName>
        <externalId>false</externalId>
        <label>Publication Category</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Book</fullName>
                    <default>false</default>
                    <label>Book</label>
                </value>
                <value>
                    <fullName>Book Chapter</fullName>
                    <default>false</default>
                    <label>Book Chapter</label>
                </value>
                <value>
                    <fullName>Journal Article</fullName>
                    <default>false</default>
                    <label>Journal Article</label>
                </value>
                <value>
                    <fullName>Conference Publication</fullName>
                    <default>false</default>
                    <label>Conference Publication</label>
                </value>
                <value>
                    <fullName>Creative work or commissioned report</fullName>
                    <default>false</default>
                    <label>Creative work or commissioned report</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Publication_Code__c</fullName>
        <externalId>false</externalId>
        <label>Publication Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>A1</fullName>
                    <default>false</default>
                    <label>A1</label>
                </value>
                <value>
                    <fullName>B - Book Chapter</fullName>
                    <default>false</default>
                    <label>B - Book Chapter</label>
                </value>
                <value>
                    <fullName>C1 - Journal Article</fullName>
                    <default>false</default>
                    <label>C1 - Journal Article</label>
                </value>
                <value>
                    <fullName>E1 - Conference Publication</fullName>
                    <default>false</default>
                    <label>E1 - Conference Publication</label>
                </value>
                <value>
                    <fullName>O - Other</fullName>
                    <default>false</default>
                    <label>O - Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Publication_Ecode__c</fullName>
        <description>Research Master Reference Number (ecode in the front end)</description>
        <externalId>false</externalId>
        <label>Publication Ecode</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publication_Id__c</fullName>
        <description>use this External Id to do the update of publication</description>
        <externalId>true</externalId>
        <label>Publication Id</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Publication_Title__c</fullName>
        <externalId>false</externalId>
        <label>Publication Title</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Publisher__c</fullName>
        <description>Publisher (Publisher Name)</description>
        <externalId>false</externalId>
        <label>Publisher</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RM_Created_Date__c</fullName>
        <externalId>false</externalId>
        <label>RM Created Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>RM_LastModified_Date__c</fullName>
        <externalId>false</externalId>
        <label>RM Last Modified Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Repository_Deposited_Date__c</fullName>
        <description>Date Deposited in Library Repository</description>
        <externalId>false</externalId>
        <label>Repository Deposited Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SJR_Percentile__c</fullName>
        <description>Store the Percentile from RRI. New field created part of 983 story</description>
        <externalId>false</externalId>
        <label>SJR Percentile</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Scimago_Journal_Quartile__c</fullName>
        <externalId>false</externalId>
        <label>Scimago_Journal_Quartile</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Scopus_Identifier__c</fullName>
        <externalId>false</externalId>
        <label>Scopus Identifier</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Start_Page__c</fullName>
        <description>Start Page - Number of the first page</description>
        <externalId>false</externalId>
        <label>Start Page</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Volume__c</fullName>
        <externalId>false</externalId>
        <label>Volume</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Web_of_Science_Identifier__c</fullName>
        <externalId>false</externalId>
        <label>Web of Science Identifier</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Year_Published__c</fullName>
        <externalId>false</externalId>
        <label>Year Published</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ls_Current__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>ls Current</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ls_Eligible__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>ls Eligible</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Researcher Publications</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Year_Published__c</columns>
        <columns>Outlet__c</columns>
        <columns>Publication_Category__c</columns>
        <columns>Publication_Title__c</columns>
        <columns>Audit_Result__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Publications</fullName>
        <columns>NAME</columns>
        <columns>Year_Published__c</columns>
        <columns>Publication_Category__c</columns>
        <columns>Publication_Title__c</columns>
        <columns>Outlet__c</columns>
        <columns>Publication_Id__c</columns>
        <filterScope>Everything</filterScope>
        <label>Publications</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>PUB-{0000000}</displayFormat>
        <label>Publication Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Publications</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>Year_Published__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Publication_Category__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Publication_Title__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Outlet__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Publication_Id__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
