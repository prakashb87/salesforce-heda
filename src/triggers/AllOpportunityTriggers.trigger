trigger AllOpportunityTriggers on Opportunity (before update, before insert) {
    
    if(Trigger.isBefore){
       if(Trigger.isUpdate) {
        	AllOpportunityTriggersHandler.handleBeforeUpdate(trigger.newMap, trigger.oldMap);
    	}
    	if(Trigger.isInsert){
        	AllOpportunityTriggersHandler.handleBeforeInsert(trigger.new); 
    	}
    }
}