trigger AllProductBasketTriggers on cscfga__Product_Basket__c (before insert) {
    
    if(Trigger.isBefore && Trigger.isInsert) {
        AllProductBasketTriggersHandler.handleBeforeInsert(trigger.new);
    }
}