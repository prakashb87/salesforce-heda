/*********************************************************************************
 *** @Trigger           : ContactTrigger
 *** @Author            : SF HEDA Team
 *** @Requirement       : To update values Of Contact
 *** @Created date      : 06/09/2018
 **********************************************************************************/
trigger ContactTrigger on Contact (before insert, before update,After insert, After Update) 
{
    public List<Id> conIds = new List<Id>();
    public Static Boolean newContact_check;
    List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
    String MyProflieName = PROFILE[0].Name;
    ContactTriggerHandler objContactTriggerHandler =new ContactTriggerHandler();
    //Work on insert of Contact  
    //if(MyProflieName != 'System Administrator' && MyProflieName != 'Data Migration')
	//{  
	    if(trigger.isInsert && trigger.isBefore)
	    {           
	        objContactTriggerHandler.onBeforeInsert(trigger.new);
	        objContactTriggerHandler.latestContactEmailDetails(trigger.new);//RM-1641
            objContactTriggerHandler.setContactIds(null,trigger.new);//RM-1576 - This methods calls a createAdministrativeAccount class which creates Account records irrespective of the Current users Profile and Role.
	       	objContactTriggerHandler.UpdateOptOutFields(null,trigger.new);
            //To update the Email field based on the preferred email
            objContactTriggerHandler.prefferredEmail(trigger.new);
	    }
	    
	    //Work on update of Contact
	    if(trigger.isUpdate && trigger.isBefore)
	    { 
	        objContactTriggerHandler.latestContactEmailDetails(trigger.new);//RM-1707
            objContactTriggerHandler.OnBeforeUpdate(trigger.oldMap,trigger.new);
            objContactTriggerHandler.setContactIds(trigger.oldmap,trigger.new);//RM-1576 - This methods calls a createAdministrativeAccount class which creates Account records irrespective of the Current users Profile and Role.
            objContactTriggerHandler.UpdateOptOutFields(trigger.oldmap,trigger.new);
            objContactTriggerHandler.prefferredEmail(trigger.new);
            CreateAdministrativeAccount.updateAdministrativeAccount(trigger.new);
	        /*for (Contact conObj: Trigger.new) {
	            if (conObj.Assessed_risk_level__c != trigger.oldMap.get(conObj.Id).Assessed_risk_level__c|| 
	                conObj.First_in_family_at_university__c != trigger.oldMap.get(conObj.Id).First_in_family_at_university__c ||
	                conObj.Early_highschool_leaver__c != trigger.oldMap.get(conObj.Id).Early_highschool_leaver__c ||
	                conObj.Previous_low_GPA__c != trigger.oldMap.get(conObj.Id).Previous_low_GPA__c ||
	                conObj.Employed_fulltime_or_time_poor__c != trigger.oldMap.get(conObj.Id).Employed_fulltime_or_time_poor__c ||
	                conObj.Unemployed_or_unskilled__c != trigger.oldMap.get(conObj.Id).Unemployed_or_unskilled__c ||
	                conObj.Course_engagement_level__c != trigger.oldMap.get(conObj.Id).Course_engagement_level__c ||
	                conObj.Lives_20km_or_further_from_RMIT__c != trigger.oldMap.get(conObj.Id).Lives_20km_or_further_from_RMIT__c) {
	                //objContactTriggerHandler.OnBeforeUpdate(trigger.oldMap,trigger.new);
	            }
	        } 
*/
	    }
	    //Work on Insert of Contact
	   /* if (trigger.isInsert && trigger.isBefore){
	       //objContactTriggerHandler.createAdministrativeAccount(trigger.new); Commentted as Part of RM-1576
	       
	    }*/
	        //Work on Update of Contact
	      /*if (trigger.isUpdate && trigger.isBefore){
	        for (Contact con: Trigger.new) {
	         // if (   con.Lastname != trigger.oldMap.get(con.Id).Lastname) {
	              //objContactTriggerHandler.createAdministrativeAccount(trigger.new);Commentted as Part of RM-1576
	              //objContactTriggerHandler.setContactIds(trigger.oldmap);//RM-1576 - This methods calls a createAdministrativeAccount class which creates Account records irrespective of the Current users Profile and Role.
	           //}
	            
	            if (   con.hed__Deceased__c != trigger.oldMap.get(con.Id).hed__Deceased__c) {
	                objContactTriggerHandler.UpdateOptOutFields(trigger.oldmap,trigger.new);
	            }
	        }
	    }
*/
	    //To update the Emai field based on the preferred email
	    /*if((trigger.isInsert || trigger.isUpdate) && trigger.isBefore){
	        objContactTriggerHandler.prefferredEmail(trigger.new);
	    }*/
	    //RM-916 - Update an old Affiliation for a staff contact and create a new one
	    if(trigger.isUpdate && trigger.isAfter){
	        objContactTriggerHandler.updateAffiliation(Trigger.new,trigger.oldMap);
	        //objContactTriggerHandler.latestContactEmailDetails(trigger.new);//RM-1641
	        //Removed as a part of RM-985 conclusion
	        /*for(Contact conIdloop: Trigger.new){
	            if(trigger.oldMap.get(conIdloop.Id).hed__Primary_Organization__c != conIdloop.hed__Primary_Organization__c){
	                objContactTriggerHandler.updateAffAccount(Trigger.new);
	            }
	        }*/
	    }
	
	    
        /*if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter){
	        if(ContactTriggerFlag.startTrigger() == True && (MyProflieName != 'System Administrator' && MyProflieName != 'Data Migration')){
	            ContactTriggerHandler.createUpdateAffiliation(trigger.new);    
	        }
	        
        }*/
	    
        /*if(trigger.isUpdate && trigger.isBefore){
                CreateAdministrativeAccount.updateAdministrativeAccount(trigger.new);    
	   
        }*/ 
	//}
     if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter){
        ContactTriggerHandler.updateResearcherAffiliations(trigger.newMap, trigger.oldMap);
    }
}