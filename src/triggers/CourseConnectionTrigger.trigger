trigger CourseConnectionTrigger on hed__Course_Enrollment__c (before delete, after update) 
{
    
    if(trigger.isBefore){
        if(trigger.isDelete){			
            for(hed__Course_Enrollment__c CourseConnectionObj : trigger.old)
            {	
				if(CourseConnectionObj.studentid__c == null)
				{
					CourseConnectionObj.adderror('Course Connection Cannot be deleted');
				}
            }
        }
    }
    //PS:16/10/2018
    if(trigger.isUpdate){
        CourseEnrollmentTriggerHandler.doAfterUpdate(Trigger.new, Trigger.OldMap);
    }
        
}