trigger chatter_answers_question_escalation_to_case_trigger on Question (after update) {
	Set<Id> priorityChangedQuestionIds=new Set<Id>();
	for (Question q: Trigger.new) {
		if (q.Priority == 'high' && (q.Cases == null || q.Cases.size() == 0) && Trigger.oldMap.get(q.id).Priority != 'high') {
			priorityChangedQuestionIds.add(q.Id);
		}
	}
	
	List<Question> prirityChangedQuestions=new List<Question>();
	if (priorityChangedQuestionIds.size()>0){	
		if (Schema.sObjectType.Question.IsAccessible()){	
			prirityChangedQuestions=[select Id, Title, Body, CommunityId, createdById, createdBy.AccountId, createdBy.ContactId from Question where Id IN :priorityChangedQuestionIds];
		}
		
		List<Case> newCases=new List<Case>();
		for (Question q: prirityChangedQuestions){
			newCases.add(new Case(Origin='Chatter Answers', OwnerId=q.CreatedById, QuestionId=q.Id, CommunityId=q.CommunityId, Subject=q.Title, Description=q.Body, AccountId=q.CreatedBy.AccountId, ContactId=q.CreatedBy.ContactId));
		}
		
		if (Schema.sObjectType.Case.IsCreateable()){
			insert newCases;
		}
	}
}