<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Contract_Expiry_Reminder_Email</fullName>
        <description>Send Contract Expiry Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <field>Other_RMIT_Contact__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>global.partners@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Others/Contract_expiry_Reminder_Email</template>
    </alerts>
    <rules>
        <fullName>Contract - Owner Expiration Notice</fullName>
        <active>true</active>
        <description>Create a new task assign to owner based on the Owner Expiration Notice value</description>
        <formula>AND( !ISPICKVAL(Owner_Expiration_Notice__c, &apos;&apos;),Owner_Expiration_Notice_Date_Formula__c &gt; TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Contract_Expiry_Reminder_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.Owner_Expiration_Notice_Date_Formula__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
