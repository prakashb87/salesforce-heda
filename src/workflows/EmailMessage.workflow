<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateCaseStatus</fullName>
        <field>Status</field>
        <literalValue>Email Received</literalValue>
        <name>UpdateCaseStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Owner_to_RMITO_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RMITO_HEd_Success_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update the Owner to RMITO Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Owner_to_RMITO_Support_Queues</fullName>
        <description>This will update the owner of the case and return it to the queue</description>
        <field>OwnerId</field>
        <lookupValue>RMIT_Online_Support_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update the Owner to RMITO Support Queues</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Status Update on Email Message</fullName>
        <actions>
            <name>UpdateCaseStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Case Status Update on Email Message</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMITO HEd Success Case Owner Update on Email Message</fullName>
        <actions>
            <name>Update_the_Owner_to_RMITO_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>RMITO HEd Success</value>
        </criteriaItems>
        <description>Case Owner will be assigned to RMITO HEd Success Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RMITO Support Request Case Owner Update on Email Message</fullName>
        <actions>
            <name>Update_the_Owner_to_RMITO_Support_Queues</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>RMIT Support Request Case</value>
        </criteriaItems>
        <description>Case Owner will be assigned to RMITO Support Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
