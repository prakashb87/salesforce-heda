<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Form_1_GPLR_Request_form</fullName>
        <ccEmails>global.partners@rmit.edu.au</ccEmails>
        <description>Form 1: GPLR Request form</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Form_1_GPLR_Request_form</template>
    </alerts>
    <alerts>
        <fullName>Form_2_International_Visit_Request_form</fullName>
        <ccEmails>global.partners@rmit.edu.au</ccEmails>
        <description>Form 2: International Visit Request form</description>
        <protected>false</protected>
        <recipients>
            <field>RMIT_Staff_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Others/Form_2_International_Visit_Request_form</template>
    </alerts>
    <alerts>
        <fullName>NotifyAppleLeads_Join_RMIT_s_Swift_App_Development_course</fullName>
        <description>NotifyAppleLeads-Join RMIT’s Swift App Development course</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing/Join_RMIT_s_Swift_App_Development_course</template>
    </alerts>
    <alerts>
        <fullName>NotifyAppleLeads_apple_introduction_to_swift</fullName>
        <description>NotifyAppleLeads-apple-introduction-to-swift</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing/RMIT_s_Summer_school_program_Introduction_to_Swift</template>
    </alerts>
    <alerts>
        <fullName>NotifyAppleLeads_apple_ios_app_development_with_swift_online</fullName>
        <description>NotifyAppleLeads-apple-ios-app-development-with-swift-online</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing/Apple_Template_apple_ios_app_development_with_swift_online</template>
    </alerts>
    <alerts>
        <fullName>NotifyAppleLeads_apple_rmit_digital_solution_design</fullName>
        <description>NotifyAppleLeads-apple-rmit-digital-solution-design</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing/Digital_Solution_Design_RMIT_Activator</template>
    </alerts>
    <alerts>
        <fullName>NotifyAppleLeads_apple_rmit_ios_swift_brand</fullName>
        <description>NotifyAppleLeads-apple-rmit-ios-swift-brand</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing/Learn_App_Development_with_Swift_at_RMIT</template>
    </alerts>
    <alerts>
        <fullName>NotifyAppleLeads_apple_rmit_swift_teachers_scholarships</fullName>
        <description>NotifyAppleLeads-apple-rmit-swift-teachers-scholarships</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing/RMIT_Swift_Teacher_Scholarships_applications_open_20_November_2017</template>
    </alerts>
    <alerts>
        <fullName>RMITO_first_response_Blockchain</fullName>
        <description>RMITO first response - Blockchain</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing@rmitonline.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>RMITO_Templates/First_response_email_Blockchain</template>
    </alerts>
    <alerts>
        <fullName>RMITO_first_response_Swift</fullName>
        <description>RMITO first response - Swift</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing@rmitonline.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>RMITO_Templates/First_response_email_Swift</template>
    </alerts>
    <alerts>
        <fullName>SendCareerAdvisorsAck</fullName>
        <description>SendCareerAdvisorsAck</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing/CareerAdvisorAcknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_the_new_IBD_lead_owner_To_C_E</fullName>
        <ccEmails>adam.rowland@rmit.edu.au</ccEmails>
        <description>Send email to the new IBD lead owner (To C&amp;E)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>industryconnect@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IBD_Email_Template_Folder/Portal_Enquiry_to_new_Lead_Owner</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_the_new_IBD_lead_owner_excluding_C_E</fullName>
        <description>Send email to the new IBD lead owner (excluding C&amp;E)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>industryconnect@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IBD_Email_Template_Folder/Portal_Enquiry_to_new_Lead_Owner</template>
    </alerts>
    <alerts>
        <fullName>Send_thank_you_email_to_Lead</fullName>
        <description>Send thank you email to Lead</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Others/Thank_you_email_to_Enquiries</template>
    </alerts>
    <alerts>
        <fullName>Template_Submission_auto_response_Form_1</fullName>
        <description>Template Submission auto response - Form 1 GRLP</description>
        <protected>false</protected>
        <recipients>
            <field>RMIT_Staff_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>global.partners@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Template_Submission_auto_response_Form_1_GPLR</template>
    </alerts>
    <alerts>
        <fullName>Template_Submission_auto_response_Form_2</fullName>
        <description>Template Submission auto response - Form 2 International Visit Request Form</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>global.partners@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Others/Template_Submission_auto_response_Form_2_International_Visit_Request_Form</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lead_Owner</fullName>
        <description>Lead Owner Update to Queue (&quot;Student Triage&quot;)</description>
        <field>OwnerId</field>
        <lookupValue>Student_Triage</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Update</fullName>
        <description>Lead statuse update to new when Lead source is Email or Webform</description>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Lead Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Company_Field</fullName>
        <description>Update company field with first name and last name</description>
        <field>Company</field>
        <formula>IF(RecordTypeId == $Label.Prospective_Student,FirstName+&apos; &apos;+LastName+&apos; &apos;+ $Label.ProspectiveStudent,
	IF(RecordTypeId == $Label.Activator_Registration,FirstName +&apos; &apos;+ LastName +&apos; &apos;+ $Label.Activator,FirstName +&apos; &apos;+ LastName +&apos; &apos;+ $Label.Online_Prospective_Student))</formula>
        <name>Update Company Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Lead_Classification_Status</fullName>
        <field>Classification_Status__c</field>
        <literalValue>Activator Program Opt Out</literalValue>
        <name>update Lead Classification Status = Acti</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Apple Lead Creation-apple-introduction-to-swift</fullName>
        <actions>
            <name>NotifyAppleLeads_apple_introduction_to_swift</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Apple_course__c</field>
            <operation>equals</operation>
            <value>apple-introduction-to-swift</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Apple Lead Creation-apple-ios-app-development-with-swift-online</fullName>
        <actions>
            <name>NotifyAppleLeads_apple_ios_app_development_with_swift_online</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Apple_course__c</field>
            <operation>equals</operation>
            <value>apple-ios-app-development-with-swift-online</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Apple Lead Creation-apple-rmit-digital-solution-design</fullName>
        <actions>
            <name>NotifyAppleLeads_apple_rmit_digital_solution_design</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Apple_course__c</field>
            <operation>equals</operation>
            <value>apple-digital-solution-design</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Apple Lead Creation-apple-rmit-ios-swift-brand</fullName>
        <actions>
            <name>NotifyAppleLeads_apple_rmit_ios_swift_brand</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Apple_course__c</field>
            <operation>equals</operation>
            <value>apple-rmit-ios-swift-brand</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Apple Lead Creation-apple-rmit-swift-teachers-scholarships</fullName>
        <actions>
            <name>NotifyAppleLeads_apple_rmit_swift_teachers_scholarships</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Apple_course__c</field>
            <operation>equals</operation>
            <value>apple-rmit-swift-teachers-scholarships</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Apple Lead Creation-apple-swift-app-development-sc</fullName>
        <actions>
            <name>NotifyAppleLeads_Join_RMIT_s_Swift_App_Development_course</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Apple_course__c</field>
            <operation>equals</operation>
            <value>apple-swift-app-development-sc</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Auto Response%3A Industry Portal Form Submission</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>IBD Leads</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Industry Portal</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CareerAdvisorAcknowledgementRule</fullName>
        <actions>
            <name>SendCareerAdvisorsAck</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Event_date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Company Update</fullName>
        <actions>
            <name>Update_Company_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Activator Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student - RMIT Online</value>
        </criteriaItems>
        <description>Update the company field in lead</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead %3A update Classification Status</fullName>
        <actions>
            <name>update_Lead_Classification_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Owner and Lead Status update when Lead Source is Email or Webform</fullName>
        <actions>
            <name>Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND( 2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Webform</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify new Lead Owner when Owner is changed to C%26E</fullName>
        <actions>
            <name>Send_email_to_the_new_IBD_lead_owner_To_C_E</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.DeveloperName = &apos;Business_Lead&apos;,     ISPICKVAL(RMIT_Relationship_Alignment__c, &apos;Industry Engagement&apos;),    	ISCHANGED(OwnerId), CONTAINS(Lead_Owner_Email_Address__c, &apos;careers@rmit.edu.au&apos;)  			)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify new Lead Owner when Owner is changed to someone but not C%26E</fullName>
        <actions>
            <name>Send_email_to_the_new_IBD_lead_owner_excluding_C_E</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.DeveloperName = &apos;Business_Lead&apos;,  ISPICKVAL(RMIT_Relationship_Alignment__c, &apos;Industry Engagement&apos;),  ISCHANGED(OwnerId), NOT(CONTAINS(Lead_Owner_Email_Address__c, &apos;careers@rmit.edu.au&apos;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Thank you email to lead</fullName>
        <actions>
            <name>Send_thank_you_email_to_Lead</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospective Student</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Contact_Method__c</field>
            <operation>equals</operation>
            <value>Webform</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
