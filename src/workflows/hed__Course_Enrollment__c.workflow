<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Course_Connection_Product_Line_Update</fullName>
        <field>Product_Line__c</field>
        <formula>hed__Course_Offering__r.hed__Course__r.Product_Line__c</formula>
        <name>Course Connection Product Line Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Product Line Update</fullName>
        <actions>
            <name>Course_Connection_Product_Line_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To be used in sharing rule to provide edit access to RMITO</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
